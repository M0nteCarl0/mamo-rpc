//[]------------------------------------------------------------------------[]
// RRP Abstract Calibrator
//
//
// Created by GSh.
//[]------------------------------------------------------------------------[]

#ifndef __R_RRPCALIB_H
#define	__R_RRPCALIB_H

#ifdef	__R_RRPCALIB_BUILD_DLL
#if ! defined (_WIN64)
#define	IRRPCALIB_EXPORT __declspec(dllexport)
#else
#define IRRPCALIB_EXPORT
#endif
#define	INITGUID
#else
#define IRRPCALIB_EXPORT __declspec(dllimport)
#endif

//[]------------------------------------------------------------------------[]
//
    DECLARE_INTERFACE_ (IRRPRepairer, IUnknown)
    {
	typedef enum
	{
	    II_ImageSizeX   = 1,
	    II_ImageSizeY   = 2,

	}   ImageInfoId	 ;

	typedef union ImageInfo
	{
	    _I32	i32Value ;
	    _U32	u32Value ;
	    float	r32Value ;
	    void  *	pValue   ;
	    char  *	sbValue  ;
	    WCHAR *	swValue  ;

	}   ImageInfo	 ;

	typedef enum
	{
	    Done	    =   0,
//	    Filter0	    =	1,
//	    Filter1	    =	2,
//	    Filter2	    =	3,
//	    Filter3	    =	4,
//	    Filter4	    =	5,
//	    Filter5	    =	6,
//	    Filter6	    =	7,
//	    Filter7	    =	8,

	    Capturing	    = 254,
	    Preparing	    = 255,

	}   Phase	 ;

    STDMETHOD_ (DWord,	   GetImageInfo)(ImageInfoId	   ,
					 ImageInfo *	   )   PURE;
    //
    //...............................................................
    //
    STDMETHOD_ (DWord,	 StartRepairing)(int	 nUAnode   ,
					 int	 nIAnode   ,
					 int	 nActiveBeg,
					 int	 nActiveEnd,
					 int	 nDCBeg	   ,
					 int	 nDCEnd	   ,
					 DWord	 dFlags	   ,		// 0x0000 : Full
					 DWord	 dParam	   ,		// HIBYTE(HIWORD()) : 12_Bad
									// LOBYTE(HIWORD()) : 12_Insert
									// HIBYTE(LOWORD()) : 23_Bad
									// LOBYTE(LOWORD()) : 23_Insert
					 TCHAR * pPath	   ) PURE;

    STDMETHOD_ (void,	CancelRepairing)()		     PURE;

    STDMETHOD_ (DWord,	       PushData)(const 
					 void  * pSrcBuf   ,
					 DWord   dLength   ) PURE;

    STDMETHOD_ (Phase,  PeekImageStatus)(int *   pStep     = NULL ,
					 int *   pStepNum  = NULL )
							     PURE;

    STDMETHOD_ (DWord,	PeekImageResult)()		     PURE;

    STDMETHOD_ (int,	      GrabImage)(void *  pDstBuf   ,
					 DWord   dDstPitch ,
					 int     nFromRow  ,
					 int     nNumRows  ) PURE;

    STDMETHOD_ (int,	    GetSrcSizeY)()		     PURE;
    };

    DECLARE_INTERFACE_ (IRRPCalibrator, IUnknown)
    {
/*
    STDMETHOD_ (GResult,     CreateRepairer)(IRRPRepairer *&
						    pIRepairer	,
					     int    nBufSizeX	,
					     int    nBufSizeY	,
					     DWord  dBufPitch	) PURE;

    CreateAccumulator (...)

    

*/
    STDMETHOD_ (GResult,     Select)(LPCTSTR	    pBasePath	,
				     int	    nUAnode	) PURE;

    STDMETHOD_ (GResult,     Repair)(const void *   pImageBuf   ,
				     int	    nImageSizeX ,
				     int	    nImageSizeY ,
				     DWord	    dImagePitch ) PURE;
    };
//
//[]------------------------------------------------------------------------[]
    
//[]------------------------------------------------------------------------[]
//
extern "C"
{
    IRRPCALIB_EXPORT GResult WINAPI CreateRRPRepairer	(IRRPRepairer    *&,
							 LPCTSTR pClassName);

    IRRPCALIB_EXPORT GResult WINAPI CreateRRPCalibrator	(IRRPRepairer    *&,
							 LPCTSTR pClassName);

    IRRPCALIB_EXPORT GResult WINAPI Accumulate (TCHAR * pFileName  ,
						TCHAR * pInpPath   );

    IRRPCALIB_EXPORT GResult WINAPI Calibrate  (int	nUA	   ,
						int	nActiveBeg ,
						int	nActiveEnd ,
						int	nDCBeg     ,
						int	nDCEnd     ,
						int	kX	   ,
						int	kY	   ,
						TCHAR * pPath      );
}
//
//[]------------------------------------------------------------------------[]

#endif//__R_RRPCALIB_H

