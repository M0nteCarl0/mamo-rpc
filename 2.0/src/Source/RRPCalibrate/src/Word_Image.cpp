//-----------------------------------------------------------------------------
//    Novikov  V
//    date 28.01.2005
//    ver. 1.00
//-----------------------------------------------------------------------------
#include "Word_Image.h"
#include "Matr_Int.h"
#include "CCD_ImgFile.h"
#include <math.h>

//-----------------------------------------------------------------------------
//   IMAGE      IMAGE      IMAGE      IMAGE      IMAGE      IMAGE      IMAGE    
//-----------------------------------------------------------------------------
Word_Image::Word_Image ():m_Addr (NULL), m_Nx(0), m_Ny(0)
{
   m_FlgAlias = false;
};
//-----------------------------------------------------------------------------
Word_Image::Word_Image (int Nx, int Ny)
{
   m_FlgAlias = false;
   Create (Nx, Ny);
};
//-----------------------------------------------------------------------------
Word_Image::Word_Image (int Nx, int Ny, WORD *Addr)
{
   m_FlgAlias = false;
   Create (Nx, Ny);
   
   int J = 0;
   for (int y = 0; y < Ny; y++){
      for (int x = 0; x < Nx; x++){
         m_Addr[y][x] =  Addr[J++];
      }
   }
};
//-----------------------------------------------------------------------------
Word_Image::~Word_Image ()
{
   Erase ();
};
//-----------------------------------------------------------------------------
void Word_Image::Create (int Nx, int Ny, WORD *Addr)
{
   if (Nx <= 0 || Ny <= 0){
      return;
   }

   m_Nx = Nx;
   m_Ny = Ny;
   
   int y;

   m_Addr = new WORD* [Ny];
   for (y = 0; y < Ny; y++)
      m_Addr [y] = NULL;

   if (Addr){
      m_Addr[0] = Addr;
      m_FlgAlias = true;
   }
   else{
      m_Addr[0] = new WORD [Nx*Ny];
      m_FlgAlias = false;
   }

   for (y = 0; y < Ny; y++){
      m_Addr[y] = m_Addr[0] + Nx * y;
   }
};
//-----------------------------------------------------------------------------
void Word_Image::Erase ()
{
   if (m_Addr){
      if (m_Addr[0] && !m_FlgAlias){
         delete [] m_Addr [0];
                                                               //printf ("It is delete of image array\n");
      }
      delete [] m_Addr;
   }
   m_Addr = NULL;
   m_Nx = m_Ny = 0;
};
//-----------------------------------------------------------------------------
void Word_Image::Clear ()
{
   if (m_Addr){
      if (m_Addr[0])
         memset (m_Addr[0], 0, sizeof (WORD) * m_Nx * m_Ny);
   }
};
//-----------------------------------------------------------------------------
bool Word_Image::Check ()
{
   if (m_Addr == NULL || m_Addr[0] == NULL) 
      return false;
   else
      return true;

};
//-----------------------------------------------------------------------------
void Word_Image::Copy (Word_Image *Img)
{
   Img->Check ();

   if (m_Addr == NULL || m_Nx != Img->m_Nx || m_Ny != Img->m_Ny){
      Erase  ();
      Create (Img->m_Nx, Img->m_Ny);
   }
   memcpy (m_Addr[0], Img->m_Addr[0], sizeof (WORD) * m_Nx * m_Ny);
};
//-----------------------------------------------------------------------------
void Word_Image::CopyZoom (int K, Word_Image *Img)
{
   Img->Check ();

   int Nx = Img->m_Nx / K;
   int Ny = Img->m_Ny / K;

   Erase  ();
   Create (Nx, Ny);

   for (int y = 0; y < Ny; y++){
      int Y = y * K;
      int X = 0;
      for (int x = 0; x < Nx; x++){
         m_Addr [y][x] = Img->m_Addr [Y][X];
         X += K;
      }
   }
};
//-----------------------------------------------------------------------------
void Word_Image::Invert ()
{
   Check ();

   for (int y = 0; y < m_Ny; y++){
      for (int x = 0; x < m_Nx; x++){
         int V = m_Addr[y][x];
         V = 0xFFFF - V;
         m_Addr[y][x] = (WORD) V;
      }
   }
};
//-----------------------------------------------------------------------------
void Word_Image::Subtract (Word_Image *Img)
{
   Img->Check ();
   Check ();

   if (m_Nx != Img->m_Nx || m_Ny != Img->m_Ny)
      return;

   for (int y = 0; y < m_Ny; y++){
      for (int x = 0; x < m_Nx; x++){
         m_Addr[y][x] = m_Addr[y][x] - Img->m_Addr[y][x];
      }
   }
};
//-----------------------------------------------------------------------------
void Word_Image::SubtractAbs (Word_Image *Img)
{
   Img->Check ();
   Check ();

   if (m_Nx != Img->m_Nx || m_Ny != Img->m_Ny)
      return;

   for (int y = 0; y < m_Ny; y++){
      for (int x = 0; x < m_Nx; x++){
         m_Addr[y][x] = abs ((int) m_Addr[y][x] - (int) Img->m_Addr[y][x]);
      }
   }
};
//-----------------------------------------------------------------------------
void Word_Image::Add (Word_Image *Img)
{
   Img->Check ();
   Check ();

   if (m_Nx != Img->m_Nx || m_Ny != Img->m_Ny)
      return;

   for (int y = 0; y < m_Ny; y++){
      for (int x = 0; x < m_Nx; x++){
         m_Addr[y][x] = m_Addr[y][x] + Img->m_Addr[y][x];
      }
   }
};
//-----------------------------------------------------------------------------
void Word_Image::Add (WORD Val)
{
   Check ();

   for (int y = 0; y < m_Ny; y++){
      for (int x = 0; x < m_Nx; x++){
         m_Addr[y][x] = m_Addr[y][x]  +  Val;
      }
   }
};
//-----------------------------------------------------------------------------
void Word_Image::Multiply (double Val)
{
   Check ();

   if (Val == 0){
      memset (&m_Addr[0][0], 0, sizeof (WORD) * m_Nx * m_Ny);
   }
   else{
      for (int y = 0; y < m_Ny; y++){
         for (int x = 0; x < m_Nx; x++){
            m_Addr[y][x] = (WORD) ((double) m_Addr[y][x] * Val);
         }
      }
   }
};
//-----------------------------------------------------------------------------
void Word_Image::Binary (WORD Level)
{
   Check ();

   for (int y = 0; y < m_Ny; y++){
      for (int x = 0; x < m_Nx; x++){
         if (m_Addr[y][x] > Level)
            m_Addr[y][x] = PIX_FULL;
         else
            m_Addr[y][x] = PIX_EMPTY;
      }
   }
};
//-----------------------------------------------------------------------------
void Word_Image::Eros ()
{
   Check ();

   Word_Image * ImgTmp = new Word_Image;
   ImgTmp->Copy (this);

   this->Clear ();

   for (int y = 2; y < m_Ny-2; y++){
      for (int x = 2; x < m_Nx-2; x++){
         if (ImgTmp->m_Addr[y-1][x] != PIX_FULL || ImgTmp->m_Addr[y-2][x] != PIX_FULL   ||
             ImgTmp->m_Addr[y][x+1] != PIX_FULL || ImgTmp->m_Addr[y][x+2] != PIX_FULL   ||
             ImgTmp->m_Addr[y+1][x] != PIX_FULL || ImgTmp->m_Addr[y+2][x] != PIX_FULL   ||
             ImgTmp->m_Addr[y][x-1] != PIX_FULL || ImgTmp->m_Addr[y][x-2] != PIX_FULL){
            m_Addr[y][x] = PIX_EMPTY;
         }
         else{
            m_Addr[y][x] = PIX_FULL;
         }
      }
   }

   delete ImgTmp;
};
//-----------------------------------------------------------------------------
void Word_Image::CombineLabeles (int Mask1, int Mask2, int Y)
{
   for (int y = Y; y > 0; y--){
      bool FlgChange = false;
      for (int x = 0; x < m_Nx; x++){

         if (m_Addr [y][x] == Mask2){
            m_Addr [y][x] = Mask1;
            FlgChange = true;
         }

      }
      if (!FlgChange)
         break;
   }
};
//-----------------------------------------------------------------------------
int Word_Image::Labeling ()
{
   int Labeling = 0;
   int y, j;

   vector <int> LabeleVect;
   LabeleVect.clear ();
   
   for (y = 0; y < m_Ny; y++){
      m_Addr [y][0]      = PIX_EMPTY;
      m_Addr [y][m_Nx-1] = PIX_EMPTY;
   }
   for (int x = 0; x < m_Nx; x++){
      m_Addr [0][x]      = PIX_EMPTY;
      m_Addr [m_Ny-1][x] = PIX_EMPTY;
   }

   for (y = 1; y < m_Ny-1; y++){
      for (int x = 1; x < m_Nx-1; x++){
         
         int  Mask1 = 0;
         int  Mask2 = 0;
         int  *Mask = &Mask1;

         int  xBegin;
         int  xEnd  ;
         int  NewSpot = 0;    //Yes
         if (m_Addr [y][x] == PIX_FULL){
            xBegin  = x;
            xEnd    = m_Nx;
            for (j = xBegin; j < m_Nx; j++){
               if (m_Addr [y][j] == PIX_FULL){

                  int M = m_Addr [y-1][j-1];                              
                  if (M != PIX_EMPTY && M != PIX_FULL){
                     *Mask = M;                              
                     NewSpot++;
                  }
                  else{
                     M = m_Addr [y-1][j];                              
                     if (M != PIX_EMPTY && M != PIX_FULL){
                        *Mask = M;                              
                        NewSpot++;
                     }
                     else{
                        M = m_Addr [y-1][j+1];
                        if (M != PIX_EMPTY && M != PIX_FULL){
                           *Mask = M;
                           NewSpot++;
                        }
                     }
                  }

                  if (NewSpot == 1)
                     Mask = &Mask2;

                  if (NewSpot >= 2 && Mask1 != Mask2){
                     CombineLabeles (Mask1, Mask2, y-1);
                     NewSpot = 1;
                     LabeleVect [Mask2-1] = 0;
                  }

               }
               else{
                  xEnd = j;
                  break;
               }
            }
            if (!NewSpot){
               Labeling++;
               *Mask = Labeling;
               LabeleVect.push_back (*Mask);
            }
            else{
               Mask = &Mask1;
            }

            for (j = xBegin; j < xEnd; j++)
               m_Addr [y][j] = *Mask;

            x = xEnd;
         }
      }
   }

   int N = (int) LabeleVect.size ();
   int * LUT = new int [N];
   int J = 0;
   for (int i = 0; i < N; i++){
      if (LabeleVect[i])
         LUT [i] = ++J;
      else
         LUT [i] = 0;
   }
   for (y = 1; y < m_Ny-1; y++){
      for (int x = 1; x < m_Nx-1; x++){
         if (m_Addr[y][x] != PIX_EMPTY && m_Addr[y][x] != PIX_FULL){
           
            int V = m_Addr [y][x]-1;
            if (V == 0)
               V=V;
            if (V < N && V >= 0)
               m_Addr [y][x] = LUT [V];
         }

      }
   }
   delete [] LUT;

   return J;
};
//-----------------------------------------------------------------------------
void Word_Image::GetMeanRms (CRect *Rect, double *Mean, double *Rms)
{
   if (Rect->right > m_Nx || Rect->bottom > m_Ny)  return;

   int W = Rect->right  - Rect->left;
   int H = Rect->bottom - Rect->top;
   int Size = W * H;
   if (Size <= 0){
      *Mean = 0;
      *Rms = 0;
      return;
   }

   int y;
   double Summ = 0;
   for (y = Rect->top; y < Rect->bottom; y++){
      for (int x = Rect->left; x < Rect->right; x++){
         Summ += (double) m_Addr [y][x]; 
      }
   }
   double MeanA = Summ / (double) Size;
   *Mean = MeanA;

//---
   if (Rms == NULL) return;

   Summ = 0;
   for (y = Rect->top; y < Rect->bottom; y++){
      for (int x = Rect->left; x < Rect->right; x++){
         double V = (double) m_Addr [y][x] - MeanA;
         Summ += V*V;
      }
   }
   double RmsA = sqrt (Summ/(double)Size);
   *Rms  = RmsA;
};
//-----------------------------------------------------------------------------
void Word_Image::GetMeanRms (double *Mean, double *Rms)
{
   CRect Rect (0, 0, m_Nx, m_Ny);
   GetMeanRms (&Rect, Mean, Rms);
};
//-----------------------------------------------------------------------------
WORD Word_Image::GetMax ()
{
   Check ();

   WORD Max = 0;
   for (int y = 0; y < m_Ny; y++){
      for (int x = 0; x < m_Nx; x++){
         if (abs (m_Addr[y][x]) > Max)
            Max = abs (m_Addr[y][x]);
      }
   }
   return Max;
};
//-----------------------------------------------------------------------------
WORD Word_Image::GetMin ()
{
   Check ();

   WORD Min = 0;
   for (int y = 0; y < m_Ny; y++){
      for (int x = 0; x < m_Nx; x++){
         if (m_Addr[y][x] < Min)
            Min = m_Addr[y][x];
      }
   }
   return Min;
};
//-----------------------------------------------------------------------------
double Word_Image::GetSumm ()
{
   double Summ = 0;
   for (int y = 0; y < m_Ny; y++){
      for (int x = 0; x < m_Nx; x++){
         Summ += m_Addr[y][x];
      }
   }
   return Summ;
};
//-----------------------------------------------------------------------------
double Word_Image::GetSumm2 ()
{
   double Summ = 0;
   for (int y = 0; y < m_Ny; y++){
      for (int x = 0; x < m_Nx; x++){
         Summ += m_Addr[y][x] * m_Addr[y][x];
      }
   }
   return Summ;
};
//-----------------------------------------------------------------------------
double Word_Image::GetAverage ()
{
   Check ();

   double Summ = 0;
   double Average = 0;
   for (int y = 0; y < m_Ny; y++){
      for (int x = 0; x < m_Nx; x++){
            Summ += m_Addr[y][x];
      }
   }
   double Size = (double) (m_Nx * m_Ny);
   if (Size)
      Average = Summ / Size;

   return Average;
};
//-----------------------------------------------------------------------------
void Word_Image::PutImage (int Nx, int Ny, WORD *Addr)
{
   Erase ();
   Create (Nx, Ny);
   
   memcpy (m_Addr[0], Addr, sizeof (WORD) * Nx * Ny);
/* because it is WORD
   int J = 0;
   for (int y = 0; y < m_Ny; y++){
      for (int x = 0; x < m_Nx; x++){
          m_Addr[y][x] = Addr[J++];
      }
   }
*/
};
//-----------------------------------------------------------------------------
void Word_Image::Print ()
{
   for (int y = 0; y < m_Ny; y++){
      printf ("%p   ", m_Addr[y]);

      for (int x = 0; x < m_Nx; x++){
         printf ("%5.1f  ", m_Addr[y][x]);
      }
      printf ("\n");
   }
};
//-----------------------------------------------------------------------------
void Word_Image::FlipHor (Word_Image *ImgOut)
{
   if (!Check ()) return;

   Word_Image *Img;

   if (ImgOut == NULL){
      Img = this;
   }
   else{
      ImgOut->Erase ();
      ImgOut->Create (m_Nx, m_Ny);
      Img = ImgOut;
   }

   for (int y = 0; y < m_Ny; y++){
      for (int x = 0; x < m_Nx/2; x++){

         WORD Tmp = Img->m_Addr [y][m_Nx-1-x];

         Img->m_Addr [y][m_Nx-1-x] = m_Addr [y][x];
         m_Addr [y][x] = Tmp;
      }
   }
};
//-----------------------------------------------------------------------------
void Word_Image::FlipVer (Word_Image *ImgOut)
{
   if (!Check ()) return;

   Word_Image *Img;

   if (ImgOut == NULL){
      Img = this;
   }
   else{
      ImgOut->Erase ();
      ImgOut->Create (m_Nx, m_Ny);
      Img = ImgOut;
   }

   for (int y = 0; y < m_Ny/2; y++){
      for (int x = 0; x < m_Nx; x++){
         WORD Tmp = Img->m_Addr [m_Ny-1-y][x];
         Img->m_Addr [m_Ny-1-y][x] = m_Addr [y][x];
         m_Addr [y][x] = Tmp;
      }
   }
};
//-----------------------------------------------------------------------------
//    
//-----------------------------------------------------------------------------
void Word_Image::Smooth (int Kx, int Ky, Word_Image &ImgOut)
{
   if (!Check ()) return;
   if (!Kx || !Ky)      return;

   int Ny = m_Ny;
   int Nx = m_Nx;
   int Size = Kx * Ky;
   int Ky2 = Ky / 2;

   Matr_Int ImgSumm;
   ImgSumm.Create (Nx, Ny);
   ImgSumm.Clear ();

   int x;
   int y;
   for (x = 0; x < Nx; x++){
      int Summ = 0;
      for (y = 0; y < Ky; y++){
         Summ += m_Addr [y][x];
      }
      ImgSumm.m_Addr [Ky2][x] = Summ;
   }
//---
   for (y = 1; y < Ny-Ky; y++){
      for (x = 0; x < Nx; x++){

         int Summ = ImgSumm.m_Addr [y-1+Ky2][x];
         Summ -= m_Addr [y-1][x];
         Summ += m_Addr [y-1+Ky][x];

         ImgSumm.m_Addr [y+Ky2][x] = Summ;
      }
   }
//---
   for (y = 0; y < Ky2; y++){
      for (x = 0; x < Nx; x++){
         ImgSumm.m_Addr [y][x] = ImgSumm.m_Addr [Ky2][x];
      }
   }

   for (y = Ny-Ky; y < Ny; y++){
      for (x = 0; x < Nx; x++){
         ImgSumm.m_Addr [y][x] = ImgSumm.m_Addr [Ny-Ky-1+Ky2][x];
      }
   }
//---
   ImgOut.Erase ();
   ImgOut.Create (Nx, Ny);
   ImgOut.Clear ();

   for (y = 0; y < Ny; y++){
      for (x = 0; x < Nx; x++){
         ImgOut.m_Addr [y][x] = ImgSumm.m_Addr [y][x] / Size;
      }
   }
};
//-----------------------------------------------------------------------------
void Word_Image::ChangePixels (int OriginalPix, int DublicatePix)      
//       if DublicatePix > 0 ����������� ������� ������ �� OriginalPix
//       if DublicatePix < 0 ����������� ������� �����  �� OriginalPix
{
   if (!Check ()) return;

   int Nx = m_Nx;
   int Ny = m_Ny;

   if (OriginalPix + DublicatePix >= Nx || OriginalPix - DublicatePix < 0) return;

   if (DublicatePix > 0){

      for (int y = 0; y < Ny; y++){

         for (int x = OriginalPix+1; x <= OriginalPix + DublicatePix; x++){

            m_Addr [y][x] = m_Addr [y][OriginalPix];
         }
      }
   }
   else{
      for (int y = 0; y < Ny; y++){

         for (int x = OriginalPix-1; x >= OriginalPix + DublicatePix; x--){

            m_Addr [y][x] = m_Addr [y][OriginalPix];
         }
      }
   }
};
//-----------------------------------------------------------------------------
int * Word_Image::ProfileX ()
{
   if (!Check ()) return 0;

   int *Profile = new int [m_Nx];
   
   memset (Profile, 0, sizeof (int) * m_Nx);

   int x;
   for (x = 0; x < m_Nx; x++){
      for (int y = 0; y < m_Ny; y++){

         Profile [x] += (int) m_Addr [y][x];
      }
   }

   for (x = 0; x < m_Nx; x++){
      Profile [x] /= (int) m_Ny;
   }
   return Profile;
};
//-----------------------------------------------------------------------------
bool Word_Image::LoadImage (char *Name, int Y0, int Y1)
{
   if (Y0 > Y1)   return false;

   CCD_ImgFile F;

   int Ret = F.OpenRead  (Name);
   if (!Ret){ printf ("Error Open file %s \n", Name); /*getchar();*/ return false;}

   F.ReadHeader ();
   int Nx = F.m_FlrHead.ffWidth;
   int Ny = F.m_FlrHead.ffHeight;

   F.SetWidthHeight (Nx, Ny);
   F.SetOffset      (0, 0);

   if (Y1 > Ny)      Y1 = Ny;

   if (Y0 == 0 && Y1 == 0){
      Y0 = 0;
      Y1 = Ny;
   }

   int H = Y1 - Y0;

   Erase ();
   Create (Nx, H);

   Ret = F.ReadImage (GetAddress (), Y0, Y1);

   if (!Ret){ 
      printf ("Error read file %s \n", Name); /*getchar();*/
      return false;
   }
   else{
//    printf ("Load success %s \n", Name); 
   }
   F.Close ();

   return true;
};
//-----------------------------------------------------------------------------
bool Word_Image::WriteImage (char *Name)
{
   if (!Check ()) return false;   

   CCD_ImgFile F;

   bool Ret = F.OpenWrite  (Name);
   if (!Ret){return false;}

   F.SetWidthHeight (m_Nx, m_Ny);
   F.SetOffset      (0, 0);


   Ret = F.WriteImg (GetAddress ());
   if (!Ret){return false;}

   F.Close ();

   return true;
};
//-----------------------------------------------------------------------------
