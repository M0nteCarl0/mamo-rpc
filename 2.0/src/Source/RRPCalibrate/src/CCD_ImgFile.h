//-----------------------------------------------------------------------------
//    Novikov  V
//    Date 08.08.04
//    ver. 1.00
//-----------------------------------------------------------------------------
#if !defined(__CCD_IMGFILE_H__)
#define      __CCD_IMGFILE_H__

#ifdef CCD_AMPLCALIBR_OBJ
#define AMPLCALIBR_API
#else

#ifdef CCD_AMPLCALIBR_EXPORTS
#define AMPLCALIBR_API __declspec(dllexport)
#else
#define AMPLCALIBR_API __declspec(dllimport)
#endif

#endif

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#pragma warning(disable:4786)
#include <new>
#include <new.h>
using namespace std;

//-----------------------------------------------------------------------------
#define FLR_HEADER_MARKER      ((WORD) ('R' << 8) | 'F')
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
class CCD_ImgFile  
{
   protected:
		char   m_FileName [_MAX_PATH];
		HANDLE m_HF;

		int   m_K[25];
      int   m_Width;
		int   m_Height;
      int   m_OffsetW;
      int   m_OffsetH;
      int   m_FragmNum;

		double m_Mean;
      
      double m_RMS;

		int   m_HeightFragm;
      WORD  * m_Addr;

   public:

      CCD_ImgFile ();
      CCD_ImgFile (int Width, int Height);
		virtual ~CCD_ImgFile();

      void SetWidthHeight (int Width, int Height){ m_Width = Width; m_Height = Height;};
      void SetOffset      (int OffsetWidth, int OffsetHeight){ m_OffsetW = OffsetWidth; m_OffsetH = OffsetHeight;};
      void SetFragmNum    (int FragmNum){ m_FragmNum = FragmNum;};
      void SetMean        (double Mean) {m_Mean = Mean;};

      double GetMean ()    {return m_Mean;};
      double GetRMS        () {return m_RMS;};
      void   GetHeaderWidthHeight (int *Width, int *Height);

      void AllocMemory  ();
      void FreeMemory   ();
      WORD * GetAddrMemory (){return m_Addr;};

      bool OpenRead	   (char *Name);
      bool OpenRead	   ()          { return OpenRead (m_FileName);};
      bool OpenWrite    (char *Name);
      void Close        ();
      bool ReadHeader   ();

      bool ReadImage       (WORD *AddrImg, int YB, int YE);
      bool ReadImgFragm    (int FragCurr, WORD *AddrImg, int *Row); 
      bool ReadImgFragm    (int FragCurr, int *Row);
      bool ReadImg         (WORD *AddrImg);
      bool WriteImg        (WORD *AddrImg);

      bool MakeBrightness  ();
      bool MakeBrightness  (int OffsetW, int OffsetH, int W, int H);
      bool MakeRMS         ();
      bool MakeRMS         (int OffsetW, int OffsetH, int W, int H);

      void PrintImg      (WORD *AddrImg, int NX, int NY);
      void PrintImgFragm (int Rows);

   protected:
      _PNH _Old_new_handler;
      static int my_new_handler (size_t){
         throw std::bad_alloc();
         return 0;
      };
      typedef struct tagFLRFILEHEADER { 
			WORD	ffType;
			WORD	ffWidth;
			WORD	ffHeight;
			WORD	ffBpp;
			WORD	ffBitsOffset;
      } FLRFILEHEADER;
   public:   
		FLRFILEHEADER m_FlrHead;
};
//-----------------------------------------------------------------------------
void MakeMeanRms  (WORD *Addr, int Width, int Height, CRect *Rect, double *Mean, double *Rms);
void MakeMean     (WORD *Addr, int Width, int Height, CRect *Rect, double *Mean);
//-----------------------------------------------------------------------------

#endif
