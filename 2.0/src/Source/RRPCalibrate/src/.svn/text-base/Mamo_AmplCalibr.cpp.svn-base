//-----------------------------------------------------------------------------
//    Novikov
//    Date 21.10.10
//    Amplitude Calibration
//-----------------------------------------------------------------------------
#include "Mamo_AmplCalibr.h"

#include "Word_Image.h"
#include "CCD_ImgFile.h"

#include <stdio.h>
#include <math.h>
//-----------------------------------------------------------------------------
Mamo_AmplCalibr::Mamo_AmplCalibr () :  m_RectMean (0, 0, 1, 1), 
                                       m_Ampl_Max     (0xFFFF),
                                       m_AmplOffset   (1000),
                                       m_SizeMean     (1000)
{
   m_X_CCD1 = 2;
   m_X_CCD2 = 1540;        //1536;
   m_X_CCD3 = 3076;        //3072;
   
   m_X_Dublicate1 = 2;
   m_X_Dublicate2 = 3;
   m_X_Dublicate3 = 2;

   strcpy (m_NameFileLoaded, "");
   
   Clear ();
};
//-----------------------------------------------------------------------------
Mamo_AmplCalibr::~Mamo_AmplCalibr ()
{
   Erase ();
};
//-----------------------------------------------------------------------------
void Mamo_AmplCalibr::Clear ()
{
   m_Yimg0 = 0;
   m_Yimg1 = 0;
   m_Ydc0  = 0;
   m_Ydc1  = 0;
   m_Kx    = 0;
   m_Ky    = 0;

   m_NumImg = 0;

   m_ImgVect = NULL;

   m_Nx = m_Ny = 0;

   //m_Param = NULL;
   m_HFcof = 0;

   memset (&m_Header, 0, sizeof (FitHeader));
   m_NumAccumImage = 0;
};
//-----------------------------------------------------------------------------
void Mamo_AmplCalibr::Erase ()
{
   m_ImgAccumulate.Erase  ();

   EraseImgMap ();

   if (m_ImgVect)
      delete [] m_ImgVect;

   EraseCalibrParam ();

   EraseRepairImage ();
   
   EraseCalibrNameMap ();
};
//-----------------------------------------------------------------------------
void Mamo_AmplCalibr::EraseImgMap ()
{
   MAP_DOUB_WORDIMAGE::iterator It;
   for (It = m_ImgMap.begin (); It != m_ImgMap.end (); It++){
      
      delete It->second->Active;  
      delete It->second->Dc;
      
      if (It->second->Profile)
         delete It->second->Profile;

      delete It->second;
   }
   m_ImgMap.clear();
};
//-----------------------------------------------------------------------------
void Mamo_AmplCalibr::EraseCalibrNameMap ()
{
   MAP_INT_TXT::iterator It;
   for (It = m_CalibrNameMap.begin (); It != m_CalibrNameMap.end (); It++){
      delete [] It->second;
   }
   m_CalibrNameMap.clear();
};
//-----------------------------------------------------------------------------
char * Mamo_AmplCalibr::GeCalibrName (int HV)
{
   char *Txt = NULL;

   int N = (int) m_CalibrNameMap.size ();
   if (N == 0) return Txt;

   int *U = new int [N];

   int J = 0;
   MAP_INT_TXT::iterator It;
   for (It = m_CalibrNameMap.begin (); It != m_CalibrNameMap.end (); It++){
      U [J++] = It->first;
   }
   
   Sort (U, N);
   
   int i;
   for (i = 0; i < N; i++){
      if (HV < U[i]){
         break;
      }
   }

   int Voltage = 0;
   if (i == N){
      Voltage = U[N-1];
   }
   else if (i == 0){
      Voltage = U[0];
   }
   else{
      if (abs (U[i] - HV) < abs (U[i-1] - HV)){
         Voltage = U[i];
      }
      else{
         Voltage = U[i-1];
      }
   }

   delete [] U;

   for (It = m_CalibrNameMap.begin (); It != m_CalibrNameMap.end (); It++){
      if (It->first == Voltage){
         Txt = It->second;
      }
   }
   return Txt;
};
//-----------------------------------------------------------------------------
void Mamo_AmplCalibr::EraseRepairImage ()
{
   m_ImgActiveRepair.Erase ();
};
//-----------------------------------------------------------------------------
//       ACCUMULATE  ACCUMULATE  ACCUMULATE  ACCUMULATE  ACCUMULATE  
//-----------------------------------------------------------------------------
void Mamo_AmplCalibr::OpenAccumulate (int Nx, int Ny)
{
   m_NumAccumImage = 0;
   m_ImgAccumulate.Erase  ();
   m_ImgAccumulate.Create (Nx, Ny);
   m_ImgAccumulate.Clear  ();
};
//-----------------------------------------------------------------------------
int Mamo_AmplCalibr::Accumulate (WORD *Addr)
{
   if (!m_ImgAccumulate.Check ()) Return::ER;

   int Size = m_ImgAccumulate.m_Nx * m_ImgAccumulate.m_Ny;

   int *AddrAccum = m_ImgAccumulate.GetAddress ();
   
   
   for (int i = 0; i < Size; i++){
      AddrAccum [i] += (int) Addr [i];     
   }
   
   m_NumAccumImage++;

   return Return::OK;
};
//-----------------------------------------------------------------------------
int Mamo_AmplCalibr::SaveAccumulate (char *Name)
{
   if (!m_ImgAccumulate.Check ()) Return::ER;
//   if (!)                         Return::ER;
   
   Word_Image ImgOut;
   ImgOut.Create (m_ImgAccumulate.m_Nx, m_ImgAccumulate.m_Ny);
   WORD *AddrOut = ImgOut.GetAddress ();

   int Size = m_ImgAccumulate.m_Nx * m_ImgAccumulate.m_Ny;

   int *AddrAccum = m_ImgAccumulate.GetAddress ();

   for (int i = 0; i < Size; i++){
      int AmplOut = (double) AddrAccum [i] / (double) m_NumAccumImage;     

      if (AmplOut > m_Ampl_Max){
         AmplOut = m_Ampl_Max;
      }
      else {
         if (AmplOut < 0)
            AmplOut = 0;
      }
     
      AddrOut [i] = (WORD) AmplOut;
   }
   m_NumAccumImage = 0;

   ImgOut.WriteImage (Name);

   m_ImgAccumulate.Erase  ();

   return Return::OK;
};
//-----------------------------------------------------------------------------
//       CALIBRATE   CALIBRATE   CALIBRATE   CALIBRATE   CALIBRATE   
//-----------------------------------------------------------------------------
int Mamo_AmplCalibr::PreloadAllImages (char * ImgPath)
{
   int Ret = Return::OK;
   EraseImgMap ();

	HANDLE hFind;
	WIN32_FIND_DATA finddata;

	char strPath [_MAX_PATH];
   
   strcpy (strPath, ImgPath);
   strcat (strPath, "\\");
   strcat (strPath, "*.flr");

	hFind =  ::FindFirstFile (strPath, &finddata);

	BOOL bMore = (hFind != (HANDLE) -1);
	if(!bMore) {
		return Return::ER;
	}

	while (bMore) {

      strcpy (strPath, ImgPath);
      strcat (strPath, "\\");
      strcat (strPath, finddata.cFileName);

      TwoImage *TwoImg = new TwoImage;
      memset (TwoImg, 0, sizeof (TwoImage));

      TwoImg->Active = new Word_Image ();
      bool Ret = TwoImg->Active->LoadImage (strPath, m_Yimg0, m_Yimg1);
      if (!Ret) {Ret = Return::ER; goto _Error;}

      TwoImg->Dc = new Word_Image ();
      Ret = TwoImg->Dc->LoadImage (strPath, m_Ydc0, m_Ydc1);
      if (!Ret) {Ret = Return::ER; goto _Error;}

//GSh <<

printf ("c+%s\n", strPath);

//GSh >>


      m_RectMean.left   =  TwoImg->Active->m_Nx - m_SizeMean - 100;
      m_RectMean.top    = (TwoImg->Active->m_Ny - m_SizeMean)/2;
      m_RectMean.right  =  TwoImg->Active->m_Nx - 100;
      m_RectMean.bottom = (TwoImg->Active->m_Ny + m_SizeMean)/2;
      
      double Mean;
      TwoImg->Active->GetMeanRms (&m_RectMean, &Mean, 0);

      TwoImg->Mean = Mean;

      m_ImgMap [Mean] = TwoImg;

		bMore = ::FindNextFile (hFind, &finddata);
	}
_Error:

	::FindClose(hFind);

   if (m_ImgMap.size ()){
      m_Nx = m_ImgMap.begin ()->second->Active->m_Nx;
      m_Ny = m_ImgMap.begin ()->second->Active->m_Ny;
   }

   MAP_DOUB_WORDIMAGE::iterator It;
   for (It = m_ImgMap.begin (); It != m_ImgMap.end (); It++){
      if (It->second->Active->m_Nx != m_Nx) {Ret = Return::ER; EraseImgMap (); break;}
      if (It->second->Active->m_Ny != m_Ny) {Ret = Return::ER; EraseImgMap (); break;}
   }
   
	return Ret;
};
//-----------------------------------------------------------------------------
void Mamo_AmplCalibr::FirstMethod ()
{
   MAP_DOUB_WORDIMAGE::iterator It;
   for (It = m_ImgMap.begin (); It != m_ImgMap.end (); It++){
      
      Word_Image &ImgAct = *It->second->Active;
      Word_Image &ImgDc  = *It->second->Dc;

      ImgAct.ChangePixels (m_X_CCD1, -m_X_Dublicate1);
      ImgAct.ChangePixels (m_X_CCD2, -m_X_Dublicate2);
      ImgAct.ChangePixels (m_X_CCD3, -m_X_Dublicate3);

      ImgDc.ChangePixels (m_X_CCD1, -m_X_Dublicate1);
      ImgDc.ChangePixels (m_X_CCD2, -m_X_Dublicate2);
      ImgDc.ChangePixels (m_X_CCD3, -m_X_Dublicate3);


      int *Profile = ImgDc.ProfileX ();
      It->second->Profile = Profile;

      Word_Image *ImgOut = new Word_Image;
      ImgAct.Smooth (m_Kx, m_Ky, *ImgOut);

      delete It->second->Active;
      It->second->Active = ImgOut;
   }
 
};
//-----------------------------------------------------------------------------
void Mamo_AmplCalibr::CopyImgMap ()
{
   m_NumImg = (int) m_ImgMap.size ();
   m_ImgVect = new TwoImage [m_NumImg];

   int J = 0;
   MAP_DOUB_WORDIMAGE::iterator It;
   for (It = m_ImgMap.begin (); It != m_ImgMap.end (); It++){
      m_ImgVect [J].Active    = It->second->Active;   
      m_ImgVect [J].Dc        = It->second->Dc;   
      m_ImgVect [J].Profile   = It->second->Profile;   
      m_ImgVect [J].Mean      = It->second->Mean;   
      J++;
   }
};
//-----------------------------------------------------------------------------
int Mamo_AmplCalibr::Calibrate (float HV, char *NameOutCOF)
{
   FirstMethod ();
   CopyImgMap  ();

   int Ret;
   Ret = OpenWriteCOF (NameOutCOF);   if (Ret) return Ret;

   Ret = WriteHeaderCOF  (HV);        if (Ret) return Ret;

   FitParam *Param = new FitParam [m_Nx];

   double* X  = new double [m_NumImg];
	double* Y  = new double [m_NumImg];
	double* Er = new double [m_NumImg];

   int i;
   for (i = m_NumImg-1; i >= 0; i--){
      Y [i] = (double) m_ImgVect[i].Mean - (double) m_ImgVect[0].Mean;
   }

   for (i = 1; i < m_NumImg; i++){
      Er [i] = sqrt (Y[i]);
   }
   Er [0] = 10;                                                 // !!!


   double P0, P1, P2;

   for (int y = 0; y < m_Ny; y++){
      for (int x = 0; x < m_Nx; x++){

         for (int i = 0; i < m_NumImg; i++){

            X[i] = (double) m_ImgVect[i].Active->m_Addr [y][x];
            X[i] = X[i] - (double) m_ImgVect[i].Profile [x];
         }


         FitByPol_2 (X, Y, Er, m_NumImg, &P0, &P1, &P2);
         Param[x].Param[0]  = P0;
         Param[x].Param[1]  = P1;
         Param[x].Param[2]  = P2;

/*      
for (i = 0; i < m_NumImg; i++) {
   printf ("i = %d  X[i]=%7.0f Y[i]=%7.0f Er[i]=%7.0f\n", i, X[i], Y[i], Er[i]);
}
printf ("               P0=%7.0f P1=%7.0f P2=%7.0f\n", P0, P1, P2);
*/
      }

      Ret = WriteRowCOF (Param, m_Nx);
      if (Ret)    break;
   }
   
   delete [] X;
   delete [] Y;
   delete [] Er;
   
   delete [] Param;

   CloseCOF ();

   EraseImgMap ();

   return Ret;
};
//-----------------------------------------------------------------------------
//    REPAIR   REPAIR   REPAIR   REPAIR   REPAIR   REPAIR   REPAIR   REPAIR   
//-----------------------------------------------------------------------------
int Mamo_AmplCalibr::PreloadAllCalibrName (char * ImgPath)
{
   int Ret = Return::OK;
   EraseCalibrNameMap ();

	HANDLE hFind;
	WIN32_FIND_DATA finddata;

	char strPath [_MAX_PATH];
   
   strcpy (strPath, ImgPath);
   strcat (strPath, "\\");
   strcat (strPath, "*.CFF");

	hFind =  ::FindFirstFile (strPath, &finddata);

	BOOL bMore = (hFind != (HANDLE) -1);
	if(!bMore) {
		return Return::ER;
	}

	while (bMore) {

      strcpy (strPath, ImgPath);
      strcat (strPath, "\\");
      strcat (strPath, finddata.cFileName);
      
      int Ret = LoadCalibrFileHeader (strPath);
      if (Ret) {Ret = Return::ER; goto _Error;}
      
      int HV = m_Header.HV;
      char *Txt = new char [_MAX_PATH];
      strcpy (Txt, strPath);

      m_CalibrNameMap [HV] = Txt;

		bMore = ::FindNextFile (hFind, &finddata);
	}
_Error:

	::FindClose(hFind);
   
	return Ret;
};
//-----------------------------------------------------------------------------
int Mamo_AmplCalibr::LoadCalibrFile (int HV, int nWidth, int nActiveBeg, int nActiveEnd, int nDCBeg, int nDCEnd, char **NameCalibr)
{
   *NameCalibr = 0;

   char *Name = GeCalibrName (HV);
   if (Name == 0)  return Return::ER;

   int Ret = LoadCalibrFile (Name, nWidth, nActiveBeg, nActiveEnd, nDCBeg, nDCEnd);

   if (NameCalibr) 
      *NameCalibr = Name;

   return Ret;
};
//-----------------------------------------------------------------------------
int Mamo_AmplCalibr::LoadCalibrFile (char *Name, int nWidth, int nActiveBeg, int nActiveEnd,
							     int nDCBeg	   , int nDCEnd	   )
{
   int Ret = Return::OK;

   if (0 == strcmp (Name, m_NameFileLoaded))
   {
   return Ret;
   }
   
   Ret = OpenReadCOF  (Name);        if (Ret){ CloseCOF (); return Ret;}
   Ret = ReadHeaderCOF ();           if (Ret){ CloseCOF (); return Ret;}

   int Size = m_Header.W * m_Header.H;

   EraseCalibrParam ();

//    Ret = (NULL != (m_Param = (FitParam *) malloc (sizeof (FitParam) * Size))
// 
// 		 ? ReadRowCOF (m_Param, Size) : Return::ALLOC_ERROR);

    if ((Return::OK == Ret) && (nWidth	   == m_Header.W     )
			    && (nActiveBeg == m_Header.Yimg0 )
			    && (nActiveEnd == m_Header.Yimg1 )
			    && (nDCBeg	   == m_Header.Ydc0  )
			    && (nDCEnd	   == m_Header.Ydc1  ))
    {

    if (Return::OK == (Ret = m_Param.Alloc (Size)))
    {{
	void *	pThis = this;

    ::RaiseException  (EXCEPTION_ACCESS_VIOLATION, 0, 1, (ULONG_PTR *) & pThis);

    Ret = ReadRowCOF  (Size);
    }}

    }
    else
    {
    Ret = Return::CFF_ERROR ;
    }

   CloseCOF ();

   if (!Ret)
      strcpy (m_NameFileLoaded, Name);

   return Ret;
};
//-----------------------------------------------------------------------------
int Mamo_AmplCalibr::LoadCalibrFileHeader (char *Name)
{
   int Ret;
   Ret = OpenReadCOF  (Name);        if (Ret) return Ret;
   Ret = ReadHeaderCOF ();           if (Ret) return Ret;

   CloseCOF ();

   return Ret;
};
//-----------------------------------------------------------------------------
void Mamo_AmplCalibr::EraseCalibrParam ()
{
    m_Param.Delete ();
    strcpy (m_NameFileLoaded, "");
};
//-----------------------------------------------------------------------------
int Mamo_AmplCalibr::Repair (bool FlgEraseWP, bool FlgDublicate, WORD *AddrIn, int NxIn, int NyIn, WORD **AddrOut, int *NxOut, int *NyOut)
{
   *AddrOut = NULL;
   *NxOut   = 0;
   *NyOut   = 0;

   int Yimg0 = m_Header.Yimg0;
   int Yimg1 = m_Header.Yimg1;
   int Ydc0  = m_Header.Ydc0;
   int Ydc1  = m_Header.Ydc1;

   int H  = m_Header.H;
   int W  = m_Header.W;

   if (W != NxIn)       return Return::ER;

   if (Yimg1 > NyIn)    return Return::ER;
   if (Yimg0 > Yimg1)   return Return::ER;

   if (Ydc1 >  NyIn)    return Return::ER;
   if (Ydc0 >  Ydc1)    return Return::ER;

   EraseRepairImage   ();

   m_ImgActiveRepair.Create (W, H, AddrIn + Yimg0 * W);

   Word_Image ImgOut;
   WORD *AddrImgOut = new WORD [W * H];
   ImgOut.Create (W, H, AddrImgOut);

   if (FlgDublicate){
      m_ImgActiveRepair.ChangePixels (m_X_CCD1, -m_X_Dublicate1);
      m_ImgActiveRepair.ChangePixels (m_X_CCD2, -m_X_Dublicate2);
      m_ImgActiveRepair.ChangePixels (m_X_CCD3, -m_X_Dublicate3);
   }
   
   Word_Image ImgDc;

   int Hdc = Ydc1-Ydc0;
   ImgDc.Create     (W, Hdc, AddrIn + Ydc0 * W);
   
   if (FlgDublicate){
      ImgDc.ChangePixels (m_X_CCD1, -m_X_Dublicate1);
      ImgDc.ChangePixels (m_X_CCD2, -m_X_Dublicate2);
      ImgDc.ChangePixels (m_X_CCD3, -m_X_Dublicate3);
   }

//---
   if (FlgEraseWP) 
      CutOffWhitePoints (m_ImgActiveRepair, ImgOut);
   else
      ImgOut.Copy (&m_ImgActiveRepair);
//---   
   int *Profile = ImgDc.ProfileX ();

   float* fp;
   int J = 0;
   for (int y = 0; y < H; y++){
      for (int x = 0; x < W; x++){

         if (ImgOut.m_Addr[y][x] == m_Ampl_Max){
            AddrImgOut [J] = m_Ampl_Max;
            J++;
            continue;
         }

         float AmplIn  = (float) ImgOut.m_Addr[y][x] - (float) Profile [x];
         
//          float AmplOut =   m_Param[J].Param[0] * AmplIn * AmplIn + 
//                            m_Param[J].Param[1] * AmplIn + 
//                            m_Param[J].Param[2];
	 fp = m_Param [J] ->Param;

	 float AmplOut = fp [0] * AmplIn * AmplIn +
			 fp [1] * AmplIn +
			 fp [2];

         AmplOut += m_AmplOffset;

         if (AmplOut > m_Ampl_Max) 
            AmplOut = (float) m_Ampl_Max;
         else 
            if (AmplOut < 0)
               AmplOut = 0;

         AddrImgOut [J] = (WORD) AmplOut;

         J++;
      }
   }
   delete [] Profile;

   *NxOut = W;
   *NyOut = H;
   *AddrOut = ImgOut.GetAddress ();

   EraseRepairImage   ();

   return Return::OK;
};
//-----------------------------------------------------------------------------
double Mamo_AmplCalibr::Determ (double m00, double m01, double m02,
                                double m10, double m11, double m12,
                                double m20, double m21, double m22)
{
   double M00 =    m00 * ((double) (m11 * m22) - (double) (m21 * m12));
   double M10 =  - m10 * ((double) (m01 * m22) - (double) (m21 * m02));
   double M20 =    m20 * ((double) (m01 * m12) - (double) (m11 * m02));
   double Det =    M00 + M10 + M20;

//   printf ("M00: = %22.16e %22.16e %22.16e $ %22.16e\n", M00, m11 * m22, m21 * m12, m11 * m22 - m21 * m12);
//   printf ("M10: = %22.16e %22.16e %22.16e $ %22.16e\n", M10, m01 * m22, m21 * m02, m01 * m22 - m21 * m02);
//   printf ("M20: = %22.16e %22.16e %22.16e $ %22.16e\n", M20, m01 * m12, m11 * m02, m01 * m12 - m11 * m02);
//   printf ("Det: = %22.16e\n", Det);

   return Det;
};
//-----------------------------------------------------------------------------
void Mamo_AmplCalibr::FitByPol_2 (double *x, double *y, double *Er, int N, double *A, double*B, double *C)
{
   double YX2 = 0;
   double YX  = 0;
   double Y   = 0;
   double X4  = 0;
   double X3  = 0;
   double X2  = 0;
   double X   = 0;
   double S   = 0;

//   double X0 = x[0];

   for (int i = 0; i < N; i++){
/*
      double z = x[i] - X0; 

      YX2 += (y[i] * z*z);
      YX  += (y[i] * z);
      Y   += (y[i]);

      X4  += (z*z*z*z);
      X3  += (z*z*z);
      X2  += (z*z);
      X   += (z);

      S   += 1;
*/

      if (!Er[i]){
         *A = 0;
         *B = 0;
         *C = 0;
         return;
      }

      double V = y[i] * x[i];
      YX2 += (V * x[i])/Er[i];
      YX  += (V)/Er[i];
      Y   += (y[i])/Er[i];

      V = x[i]*x[i]; 
      X4  += (V * V)/Er[i];
      X3  += (V * x[i])/Er[i];
      X2  += (V)/Er[i];
      X   += (x [i])/Er[i];

      S   += 1/Er[i];

   }
//printf ("X4-0: = %22.16e %22.16e %22.16e %22.16e %22.16e \n", X4, X3, X2, X, S);
	
   double Det = Determ (X4, X3, X2, 
                        X3, X2, X,
                        X2, X,  S);

   double DetA= Determ (YX2, X3, X2, 
                         YX, X2, X,
                          Y,  X, S);

   double DetB= Determ (X4, YX2, X2, 
                        X3,  YX, X,
                        X2,   Y, S);

   double DetC= Determ (X4, X3, YX2, 
                        X3, X2,  YX,
                        X2, X,    Y);

   double a = 0, b = 0, c = 0;
   if (Det){
      a = DetA / Det;
      b = DetB / Det;
      c = DetC / Det;
   }
   *A = a;
   *B = b;
   *C = c;

//   *A = a;
//   *B = b - 2*a*X0;
//   *C = a*X0*X0 - (b)*X0 + c;


//printf ("A-B-C: = %e %e %e\n", *A, *B, *C);
};
//-----------------------------------------------------------------------------
void Mamo_AmplCalibr::Sort (int *Addr, int N)
{
   for (int i = 0; i < N; i++){
      for (int j = 0; j < N-1-i; j++){
         if (Addr[j] > Addr[j+1]){
            int Tmp = Addr[j+1];
            Addr[j+1]  = Addr[j];
            Addr[j]    = Tmp;
         }
      }
   }
};
//-----------------------------------------------------------------------------
int Mamo_AmplCalibr::MaxF (int *Addr, int N)
{
   int Max = 0;
   for (int i = 0; i < N; i++){
      if (Addr[i] > Max){
         Max = Addr[i];
      }
   }
   return Max;
};
//-----------------------------------------------------------------------------
int Mamo_AmplCalibr::MinF (int *Addr, int N)
{
   int Min = 0xFFFF;
   for (int i = 0; i < N; i++){
      if (Addr[i] < Min){
         Min = Addr[i];
      }
   }
   return Min;
};
//-----------------------------------------------------------------------------
void Mamo_AmplCalibr::CutOffWhitePoints (Word_Image &ImgIn, Word_Image &ImgOut)
{
   int T0 = clock ();

   int Nx = ImgIn.m_Nx;
   int Ny = ImgIn.m_Ny;

   const int K = 8;
   int Buff [K];

   double CA = 0.472;
   double CB = 182;

   int Point = 0;
   for (int y = 1; y < Ny-1; y++){
      for (int x = 1; x < Nx-1; x++){

         int Summ = 0;

         int yy = y-1;
         int xx = x-1;
         Buff [0] = ImgIn.m_Addr [yy][xx++];
         Buff [1] = ImgIn.m_Addr [yy][xx++];
         Buff [2] = ImgIn.m_Addr [yy][xx];
         yy = y;
         xx = x-1;
         Buff [3] = ImgIn.m_Addr [yy][xx];
         xx += 2;
         Buff [4] = ImgIn.m_Addr [yy][xx];
         yy = y+1;
         xx = x-1;
         Buff [5] = ImgIn.m_Addr [yy][xx++];
         Buff [6] = ImgIn.m_Addr [yy][xx++];
         Buff [7] = ImgIn.m_Addr [yy][xx];

         Summ += Buff[0]+Buff[1]+Buff[2]+Buff[3]+Buff[4]+Buff[5]+Buff[6]+Buff[7];
         Summ /= K;

         int MaxP = 0;
         int MinP = 0xFFFF;
         for (int i = 0; i < K; i++){
            if (Buff[i] > MaxP){
               MaxP = Buff[i];
            }
            if (Buff[i] < MinP){
               MinP = Buff[i];
            }
         }

         double RMS = CA * Summ + CB;
         
         int Rms = 0;
         if (RMS > 0)   Rms = sqrt (RMS);


         if (((ImgIn.m_Addr [y][x] > MaxP + Rms)) && Summ < 10000){
            ImgOut.m_Addr [y][x] = Summ;
            Point++;
         }
         else if (((ImgIn.m_Addr [y][x] < MinP - Rms)) && Summ < 10000){
            ImgOut.m_Addr [y][x] = Summ;
            Point++;
         }
         else{
            ImgOut.m_Addr [y][x] = ImgIn.m_Addr [y][x];
         }

      }
   }
   int T1 = clock ();
   printf ("Time = %d, Changed Pointd = %d\n", T1-T0, Point);

};
//-----------------------------------------------------------------------------
/*
void Mamo_AmplCalibr::CutOffWhitePoints (Word_Image &ImgIn, Word_Image &ImgOut)
{
   int Nx = ImgIn.m_Nx;
   int Ny = ImgIn.m_Ny;

//   ImgOut.Erase ();
//   ImgOut.Create (Nx, Ny);
//   ImgOut.Clear ();

   const K = 9;
   int Buff [K];

   double CA = 0.472;
   double CB = 182;

   int Point = 0;
   for (int y = 1; y < Ny-1; y++){
      for (int x = 1; x < Nx-1; x++){

         int J = 0;
         int Summ = 0;
         for (int yy = -1; yy <= 1; yy++){
            for (int xx = -1; xx <= 1; xx++){
               Buff [J] = ImgIn.m_Addr [y+yy][x+xx];
               Summ += Buff [J];
               J++;
            }
         }
         Summ /= K;

         Buff[4] = Buff[3];

         Sort (Buff, K);
         int MaxP = MaxF (Buff, K);
         int MinP = MinF (Buff, K);

         double RMS = CA * Summ + CB;
         
         int Rms = 0;
         if (RMS > 0)
            Rms = sqrt (RMS);


         if (((ImgIn.m_Addr [y][x] > MaxP + 1*Rms)) && Summ < 10000){
            ImgOut.m_Addr [y][x] = MaxP;
            Point++;
         }
         else if (((ImgIn.m_Addr [y][x] < MinP - 1*Rms)) && Summ < 10000){
            ImgOut.m_Addr [y][x] = MinP;
            Point++;
         }
         else{
            ImgOut.m_Addr [y][x] = ImgIn.m_Addr [y][x];
         }
      }
   }
   printf ("Changed Pointd = %d\n", Point);

};
*/
//--------------------------------------------------------------------------
//
//--------------------------------------------------------------------------
int  Mamo_AmplCalibr::OpenWriteCOF  (char *Name)
{
//   strcpy (FileName, Name);

   m_HFcof = CreateFile   (Name, GENERIC_READ | GENERIC_WRITE,
			    0,
			    NULL,
			    CREATE_ALWAYS,
			    FILE_ATTRIBUTE_NORMAL,
			    NULL);
   if (INVALID_HANDLE_VALUE == m_HFcof){
      return Return::OPEN_ER;
   }

   return Return::OK;
};
//--------------------------------------------------------------------------
int  Mamo_AmplCalibr::OpenReadCOF  (char *Name)
{
//   strcpy (FileName, Name);

   m_HFcof = CreateFile   (Name, GENERIC_READ,
			    0,
			    NULL,
			    OPEN_EXISTING,
			    FILE_ATTRIBUTE_NORMAL,
			    NULL);
   if (INVALID_HANDLE_VALUE == m_HFcof){
      return Return::OPEN_ER;
   }

   return Return::OK;
};
//--------------------------------------------------------------------------
void Mamo_AmplCalibr::CloseCOF ()
{
   if (m_HFcof){
      CloseHandle (m_HFcof);
      m_HFcof          = 0;
   }
};
//-----------------------------------------------------------------------------
int Mamo_AmplCalibr::WriteHeaderCOF (float HV)
{
   if (!m_HFcof)        return Return::ER;

   int Ret = Return::OK;

   m_Header.Key  = 0;
   m_Header.W        = m_Nx;
   m_Header.H        = m_Ny;
   m_Header.Yimg0    = m_Yimg0;
   m_Header.Yimg1    = m_Yimg1;
   m_Header.Ydc0     = m_Ydc0;
   m_Header.Ydc1     = m_Ydc1;
   m_Header.HV       = HV;

   unsigned long Rev;
   WriteFile (m_HFcof, &m_Header, sizeof (FitHeader), &Rev, NULL);

   if (Rev != sizeof (FitHeader)){ 
      Ret = Return::RW_ER; 
   }

   return Ret;
};
//-----------------------------------------------------------------------------
int Mamo_AmplCalibr::ReadHeaderCOF ()
{
   if (!m_HFcof)        return Return::ER;

   int Ret = Return::OK;

   unsigned long Rev;
   ReadFile (m_HFcof, &m_Header, sizeof (FitHeader), &Rev, NULL);

   if (Rev != sizeof (FitHeader)){ 
      Ret = Return::RW_ER; 
   }

   return Ret;
};
//-----------------------------------------------------------------------------
int Mamo_AmplCalibr::WriteRowCOF (FitParam *Param, int Len)
{
   if (!m_HFcof)        return Return::ER;

   int Ret = Return::OK;

   Len *= sizeof (FitParam);

   unsigned long Rev;
   WriteFile (m_HFcof, Param, Len, &Rev, NULL);
   
   if (Rev != Len){ 
      Ret = Return::RW_ER; 
   }

   return Ret;
};
//-----------------------------------------------------------------------------
#if FALSE

int Mamo_AmplCalibr::ReadRowCOF (FitParam *Param, int Len)
{
   if (!m_HFcof)        return Return::ER;

   int Ret = Return::OK;

   Len = sizeof(FitParam) * Len;
   
   unsigned long Rev;
   ReadFile (m_HFcof, Param, Len, &Rev, NULL);
   
   if (Rev != Len){ 
      Ret = Return::RW_ER; 
   }

   return Ret;
};

#else

int Mamo_AmplCalibr::ReadRowCOF (int Len)
{
#if TRUE

    if (!m_HFcof) 
    {
	return Return::ER;
    }

    if (m_Param.GetSize () < Len)
    {
	return Return::ER;
    }

    DWORD     dTemp ;
    DWORD     dRead ;
    
    for (int i = 0; 0 < Len; i ++)
    {
	    dRead = ((FIT_BLOCK_LEN < Len) ? FIT_BLOCK_LEN : Len) * sizeof (FitParam);

	if (! ::ReadFile (m_HFcof, m_Param [i * FIT_BLOCK_LEN], dRead, & dTemp, NULL) || (dRead != dTemp))
	{
	    return Return::RW_ER;	    
	}
	    Len  -= (dRead / sizeof (FitParam));
    }
	    return Return::OK;
    
/*
   Len = sizeof(FitParam) * Len;
   
   unsigned long Rev;
   ReadFile (m_HFcof, Param, Len, &Rev, NULL);
   
   if (Rev != Len){ 
      Ret = Return::RW_ER; 
   }

   return Ret;
*/

#else

    if (!m_HFcof)        return Return::ER;
    
    int Ret = Return::OK;
    
    //Len = sizeof(FitParam) * m_Param.size_;
    
    unsigned long Rev;

    int Len = 0;

    for (int i=0; i<m_Param.nbfcnt_; i++)
    {
	FitParam* Param = m_Param.GetBlock(i);
	    Len = m_Param.block_size_*sizeof(FitParam);
	ReadFile (m_HFcof, Param, Len, &Rev, NULL);
	if (Rev != Len){ 
	    if (i+1 == m_Param.nbfcnt_ 
		&& (m_Param.size_%m_Param.block_size_)*sizeof(FitParam) != Rev)
	    {
		Ret = Return::RW_ER; 
	    }
	}
    }
    
    return Ret;
#endif
};

#endif
//
//-----------------------------------------------------------------------------

