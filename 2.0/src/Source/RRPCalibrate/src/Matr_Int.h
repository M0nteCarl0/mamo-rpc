//----------------------------------------------------------------------------
//    Novikov
//    19.12.2006
//----------------------------------------------------------------------------
#ifndef  __MATR_INT_H
#define  __MATR_INT_H

#include "stdio.h"
#include "memory.h"
//----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
class Matr_Int
{
   public:
      int            **m_Addr;
      int              m_Nx;
      int              m_Ny;
      int              m_FlgPrint;

      Matr_Int ();
      Matr_Int (int Nx, int Ny);
      virtual ~Matr_Int ();

      void Copy   (Matr_Int * Obj);
      void Create (int Nx, int Ny);
      void Erase ();
      void Clear ();
      bool Check ();
   
      int * GetAddress (){return m_Addr[0];}

      void Write (FILE *hFile);
      void Save  (char *Name);
};
//-----------------------------------------------------------------------------
#endif
