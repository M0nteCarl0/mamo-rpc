//[]------------------------------------------------------------------------[]
// RRP Calibrator by VNovikov instance
//
//
// Created by GSh.
//[]------------------------------------------------------------------------[]

#ifndef _WIN32_WINNT
#define _WIN32_WINNT	0x0501	    // WindowsXP <= version
#endif

#include "Mamo_AmplCalibr.h"

#include "g_winbase.h"

#include "RRPCalibrate.h"
#include "adapt.h"


#define	 NV_EXTENSION	FALSE	    // Mamo_AmplCalibr::Repair (true , true , ...)
				    // Mamo_AmplCalibr::Repair (false, false, ...)

//[]--------------------------------------------------------------------------
//
static HANDLE createFile (HWND hParent, const TCHAR * pFilter, TCHAR * szTitle = NULL)
{
    HANDLE		hFile;
    TCHAR		szFileName [256];
    OPENFILENAME	ofn ;

    memset (szFileName, 0, sizeof (szFileName));
    memset (& ofn     , 0, sizeof (ofn	     ));

    hFile		= NULL;

    ofn.lStructSize     = sizeof(OPENFILENAME);
    ofn.hwndOwner	= hParent;

    ofn.lpstrFile	= szFileName;
    ofn.nMaxFile	= sizeof(szFileName);

    ofn.lpstrFileTitle	= szTitle;
    ofn.nMaxFileTitle	= MAX_PATH ;

    ofn.lpstrFilter     = pFilter;
    ofn.nFilterIndex    = 1;
    ofn.Flags		= OFN_PATHMUSTEXIST | OFN_CREATEPROMPT | OFN_HIDEREADONLY;
    ofn.lpstrDefExt     = pFilter;

    if (GetSaveFileName (& ofn))
    {
	if (INVALID_HANDLE_VALUE != 
	   (hFile = CreateFile	    (szFileName		    ,
				     GENERIC_WRITE	    ,
				     0 /*FILE_SHARE_READ*/  ,
				     NULL		    ,
				     CREATE_ALWAYS	    ,
				     FILE_ATTRIBUTE_NORMAL  ,
				     NULL		    )))
	{
	    return hFile;
	}
    }
	    return NULL ;
};
//
//[]--------------------------------------------------------------------------

//[]--------------------------------------------------------------------------
//
#define FLR_HEADER_MARKER      ((WORD) ('R' << 8) | 'F')

      typedef struct tagFLRFILEHEADER { 
			WORD	ffType;
			WORD	ffWidth;
			WORD	ffHeight;
			WORD	ffBpp;
			WORD	ffBitsOffset;
      } FLRFILEHEADER;

void SaveFLR (HANDLE f, void * pImageBuf, int iWidth, int iHeight)
{
	FLRFILEHEADER flrHead;
	DWORD		  t;

	flrHead.ffType	 = FLR_HEADER_MARKER;

	flrHead.ffWidth	 = iWidth  ;
	flrHead.ffHeight = iHeight ;

	flrHead.ffBpp	     = 16;
	flrHead.ffBitsOffset =  0;
    {
	::WriteFile (f, & flrHead, sizeof (flrHead)    , & t, NULL);
        ::WriteFile (f, pImageBuf, iWidth * iHeight * 2, & t, NULL);
    }
};

void SaveImage (HWND hParent, void * pImageBuf, int iWidth, int iHeight)
{
    if (pImageBuf)
    {{
	TCHAR   pFileName [256];

	HANDLE  f = createFile (hParent, "flr\0*.flr\0", pFileName);

	if     (f)
	{
	    SaveFLR	  (f, pImageBuf, iWidth, iHeight);

//    	    ::MessageBox  (hParent, "Save file done", "Report", MB_ICONINFORMATION | MB_OK);

	    ::CloseHandle (f);
	}
    }}
    else
    {
	    ::MessageBox (hParent, "Can not find ImageBuf", "Report", MB_ICONSTOP | MB_OK);
    }
};
//
//[]--------------------------------------------------------------------------

void CropImage (Word * pDstBuf, int DstPitch ,
				int DstX     ,
				int DstY     ,
		Word * pSrcBuf, int SrcPitch ,
				int SrcX     ,
				int SrcY     ,
				int SrcSizeX ,
				int SrcSizeY )
{
	pDstBuf = pDstBuf + ((DstY * DstPitch) + DstX);
	pSrcBuf = pSrcBuf + ((SrcY * SrcPitch) + SrcX);

    for (int i = 0; i < SrcSizeY; i ++, pDstBuf += DstPitch, pSrcBuf += SrcPitch)
    {
	memcpy (pDstBuf, pSrcBuf, SrcSizeX * 2);
    }
};

#define	IMAGE_RULE_WIDTH (1537		   * 1)
#define IMAGE_LINE_WIDTH (IMAGE_RULE_WIDTH * 3)
#define	RAW_IMAGE_SIZEX	 (IMAGE_LINE_WIDTH * 2)

//[]------------------------------------------------------------------------[]
//
class NVCalibrator : public Mamo_AmplCalibr
{
public :

		    NVCalibrator    ();
		   ~NVCalibrator    ();

static	GResult	    CreateInstance		(IRRPRepairer *&);

	friend
	class TXUnknown_Entry <NVCalibrator>;
	      TXUnknown_Entry <NVCalibrator>
						m_XUnknown	;

virtual	HRESULT	    XUnknown_QueryInterface	(REFIID, void **);

virtual	void	    XUnknown_FreeInterface	(void *);

	friend
	class IRRPRepairer_Entry;
	friend
	class TInterface_Entry <IRRPRepairer, NVCalibrator>;
	class IRRPRepairer_Entry : public
	      TInterface_Entry <IRRPRepairer, NVCalibrator>
	{
	STDMETHOD_ (DWord,	   GetImageInfo)(ImageInfoId uInfoId,
						 ImageInfo * pInfo  )
	{
	    return GetT () ->IRRPRepairer_GetImageInfo (uInfoId, pInfo);
	};
	//
	//..........................................................
	//
	STDMETHOD_ (DWord,	 StartRepairing)(int	 nUAnode    ,
						 int	 nIAnode    ,
						 int	 nActiveBeg ,
						 int	 nActiveEnd ,
						 int	 nDCBeg	    ,
						 int	 nDCEnd	    ,
						 DWord	 dFlags	    ,
						 DWord	 dParam	    ,
						 TCHAR * pPath	    )
	{
	    return GetT () ->IRRPRepairer_Start (nUAnode    ,
						 nIAnode    ,
						 nActiveBeg ,
						 nActiveEnd ,
						 nDCBeg	    ,
						 nDCEnd	    ,
						 dFlags	    ,
						 dParam	    ,
						 pPath	    );
	};
	STDMETHOD_ (void,	CancelRepairing)()
	{
		   GetT () ->IRRPRepairer_Cancel ();
	};
	STDMETHOD_ (DWord,	       PushData)(const 
						 void  * pSrcBuf    ,
						 DWord   dLength    )
	{
	    return GetT () ->IRRPRepairer_PushData (pSrcBuf, dLength);
	};
	STDMETHOD_ (Phase,	PeekImageStatus)(int *   pStep	    ,
						 int *   pStepNum   )
	{
	    return GetT () ->IRRPRepairer_PeekImageStatus (pStep    ,
							   pStepNum );
	};
	STDMETHOD_ (DWord,	PeekImageResult)()
	{
	    return GetT () ->IRRPRepairer_PeekImageResult ();
	};
	STDMETHOD_ (int,	      GrabImage)(void *	 pDstBuf    ,
						 DWord	 dDstPitch  ,
						 int	 nFromRow   ,
						 int	 nNumRows   )
	{
	    return GetT () ->IRRPRepairer_GrabImage     (pDstBuf    ,
							 dDstPitch  ,
							 nFromRow   ,
							 nNumRows   );
	};
	STDMETHOD_ (int,	    GetSrcSizeY)()
	{
	    return GetT () ->m_nCurSizeY;
	};
	}					 m_IRRPRepairer;

virtual	DWord	IRRPRepairer_GetImageInfo	(IRRPRepairer::ImageInfoId, 
						 IRRPRepairer::ImageInfo *);

virtual	DWord	IRRPRepairer_Start		(int, int, int, int, int, int, DWord, DWord, TCHAR  *);

virtual void	IRRPRepairer_Cancel		();

virtual	DWord	IRRPRepairer_PushData		(const void *, DWord);

virtual	IRRPRepairer::Phase
		IRRPRepairer_PeekImageStatus    (int *, int *);

	DWord	IRRPRepairer_PeekImageResult	();

virtual	int	IRRPRepairer_GrabImage		(void *, DWord, int, int);

virtual	GResult		RepairProc		(int	 nUAnode ,
						 int	 nIAnode ,
						 DWord	 dFlags  ,
						 DWord	 dParam  ,
						 TCHAR * pPath	 ,
						 HANDLE  hResume );

static  GResult	GAPI	RepairProc		(int, void **	, HANDLE);

static	DWORD		LoadCalibrFileException (NVCalibrator *	  ,
						 PEXCEPTION_RECORD);

	typedef enum
	{
	    Calc_Cancel	    = -1,
	    Calc_Done	    =  0,
	    Calc_Process    =  1,
	    Calc_Capture    =  2,
	    Calc_Prepare    =  3,

	}   CalcPhase	;

	void		SetCalcPhase		(IRRPRepairer::Phase ph)
			{
			    
			};
protected :

	    int		    m_nMaxSizeX	    ;
	    int		    m_nMaxSizeY	    ;

	    int		    m_nSrcSizeX	    ;
	    Word *	    m_pSrcBuf	    ;

	    int		    m_nCurSizeY	    ;

	    TInterlocked <int>
			    m_nCalcPhase    ;
	    HANDLE	    m_hCalcThread   ;

	    GResult	    m_dOutResult    ;
	    TInterlocked <int>
			    m_nOutPhase	    ;

	    Word *	    m_pOutBuf	    ;
	    int		    m_nOutPitch	    ;
	    int		    m_nOutSizeX	    ;
	    int		    m_nOutSizeY	    ;

	    int		    m_nActiveBeg    ;
	    int		    m_nActiveEnd    ;
	    int		    m_nDCBeg	    ;
	    int		    m_nDCEnd	    ;

	    TCHAR	    m_pCalcBaseDir [MAX_PATH];
};
//
//[]------------------------------------------------------------------------[]
//
HRESULT NVCalibrator::XUnknown_QueryInterface (REFIID riid, void **)
{
    HRESULT Result = ERROR_NOT_SUPPORTED;

    return Result;
};

void NVCalibrator::XUnknown_FreeInterface (void * pi)
{
    if (pi == & m_XUnknown)
    {
    {{
printf ("VNCalibrator::delete ()\n");

	delete this;
    }}
    }
};

GResult NVCalibrator::CreateInstance (IRRPRepairer *& pIRRPRepairer)
{
    NVCalibrator * o = new NVCalibrator ();

    o ->m_XUnknown.Init (o);

	 pIRRPRepairer =
    o ->m_IRRPRepairer
		  .Init (o, IID_NULL, & o ->m_XUnknown);

    o ->m_XUnknown.Release ();

    return ERROR_SUCCESS;
};

static Bool GetProgramFilesDir (LPTSTR pPath, int nSize)
{
    HKEY    hKey  = NULL ;
    DWORD   dSize = nSize * sizeof (TCHAR);

    if (ERROR_SUCCESS != ::RegOpenKeyEx	    (HKEY_LOCAL_MACHINE ,
					  _T("SOFTWARE\\Microsoft\\Windows\\CurrentVersion"),
					     0, KEY_READ, & hKey))
    {	return False;}

    if (ERROR_SUCCESS == ::RegQueryValueEx  (hKey	       ,
					  _T("ProgramFilesDir"),
					     NULL, NULL, (LPBYTE) pPath, & dSize))
    {	::RegCloseKey (hKey); return True ;}

	::RegCloseKey (hKey); return False;
};

NVCalibrator::NVCalibrator ()
{
    m_nSrcSizeX	     = ((RAW_IMAGE_SIZEX / 4) * 4) / 2;
    m_nMaxSizeX	     = (m_nSrcSizeX + 20);
    m_nMaxSizeY	     = 6999;

    m_pSrcBuf	     = (Word *) malloc ((m_nMaxSizeY + 1) * m_nMaxSizeX * 2);

    m_nCurSizeY	     = 0;

    m_nCalcPhase     = Calc_Cancel ;
    m_hCalcThread    = NULL	   ;

    m_dOutResult     = ERROR_SUCCESS	 ;
    m_nOutPhase	     = IRRPRepairer::Done;

    m_pOutBuf	     = NULL ;
    m_nOutSizeX	     =
    m_nOutSizeY	     = 0;

    m_nActiveBeg     = 
    m_nActiveEnd     = 0;
    m_nDCBeg	     =
    m_nDCEnd	     = 0;

  * m_pCalcBaseDir   = 0;

    GetProgramFilesDir (m_pCalcBaseDir, sizeof (m_pCalcBaseDir)/sizeof (TCHAR));
    ::strcat (m_pCalcBaseDir, _T("\\RoentgenProm\\UrpHF Next\\NVCalibrate"));
};

NVCalibrator::~NVCalibrator ()
{
    free (m_pSrcBuf);
};
//
//[]------------------------------------------------------------------------[]
//
DWord NVCalibrator::IRRPRepairer_GetImageInfo (IRRPRepairer::ImageInfoId id,
					       IRRPRepairer::ImageInfo * ii)
{
    switch (id)
    {
    case IRRPRepairer::II_ImageSizeX :
	ii ->i32Value = m_nOutSizeX;		// (((RAW_IMAGE_LINEX * 3 * 2) / 4) * 4) / 2;
	return ERROR_SUCCESS;

    case IRRPRepairer::II_ImageSizeY :
	ii ->i32Value =	m_nOutSizeY;		//6000;
	return ERROR_SUCCESS;
    };
	return ERROR_NOT_SUPPORTED;
};

DWord NVCalibrator::IRRPRepairer_Start (int nUAnode, int nIAnode, int nActiveBeg, 
								  int nActiveEnd,
								  int nDCBeg	,
								  int nDCEnd	,
					DWord dFlags, DWord dParam, TCHAR * pPath)
{
    void * pParam [6] = {this, & nUAnode, & nIAnode, & dFlags, & dParam, (void *) pPath};

    m_nCurSizeY	 = 0;

    m_nActiveBeg = nActiveBeg;
    m_nActiveEnd = nActiveEnd;
    m_nDCBeg	 = nDCBeg    ;
    m_nDCEnd	 = nDCEnd    ;

    m_nCalcPhase.Exchange (Calc_Prepare);

    return CreateThread <GThread> 
	  (& m_hCalcThread, NVCalibrator::RepairProc, 6, pParam, (256 * 1024));
};

void NVCalibrator::IRRPRepairer_Cancel ()
{
    m_nCalcPhase.Exchange (Calc_Cancel);

    if (m_hCalcThread)
    {
	::QueueUserAPC (m_hCalcThread, NULL, 0, NULL, NULL);
    }
    return;
};

DWord NVCalibrator::IRRPRepairer_PushData (const void * pRawBuf,
						 DWord	dLength)
{
    if  (Calc_Capture != m_nCalcPhase.GetValue ())
    {
	return 0;
    }
    if	(0	      == dLength)
    {
	if (Calc_Capture == m_nCalcPhase.CompareExchange (Calc_Process, Calc_Capture))
	{
	    ::QueueUserAPC (m_hCalcThread, NULL, 0, NULL, NULL);

printf ("Push %4d rows completed, Start Processing ...\n", m_nCurSizeY);
	}
	return 0;
    }

    int	    i;
    DWord   dResult = 0;
    Word *  pSrcBuf = (Word *)(((Word *) m_pSrcBuf) + (m_nSrcSizeX * m_nCurSizeY));

    for (; (m_nMaxSizeY > m_nCurSizeY) && (RAW_IMAGE_SIZEX <= dLength);)
    {
	for (i = 0; i < IMAGE_RULE_WIDTH; i ++)
	{
	    pSrcBuf [i + (IMAGE_RULE_WIDTH * 1)    ] = * (((const Word *) pRawBuf) + 0);
	    pSrcBuf [i + (IMAGE_RULE_WIDTH * 0)    ] = * (((const Word *) pRawBuf) + 1);
	    pSrcBuf [    (IMAGE_RULE_WIDTH * 3) - i] = * (((const Word *) pRawBuf) + 2);

	    ((const Word *&) pRawBuf)  += 3;
	}
	    pSrcBuf += m_nSrcSizeX    ;
	    dResult += RAW_IMAGE_SIZEX;
	    dLength -= RAW_IMAGE_SIZEX;

		    ++ m_nCurSizeY    ;

//	if (/*m_nMaxSizeY*/6872  == ++ m_nCurSizeY)
//	{
//	if (Calc_Capture == m_nCalcPhase.CompareExchange (Calc_Process, Calc_Capture))
//	{
//	    ::QueueUserAPC (m_hCalcThread, NULL, 0, NULL, NULL);
//	}
//	}
    }

//printf ("Push %4d rows\n", m_nCurSizeY);

	return dResult;
};

IRRPRepairer::Phase NVCalibrator::IRRPRepairer_PeekImageStatus 
						  (int * pStep    ,
						   int * pStepNum )
{
    int	iPhase	;

    if (pStep	) { * pStep    = 0;}
    if (pStepNum) { * pStepNum = 0;}

    if (IRRPRepairer::Done == (iPhase = m_nOutPhase.GetValue ()))
    {{
	DWord dExitCode ;

    if (m_hCalcThread)
    {
	::GetExitCodeThread (m_hCalcThread, & dExitCode);

	if (STILL_ACTIVE == dExitCode)
	{
	    goto lExit;
	}

	::CloseHandle 
       (m_hCalcThread);
        m_hCalcThread = NULL;

	m_dOutResult = dExitCode;
    }
    }}

lExit :
    return (IRRPRepairer::Phase) iPhase;
};

DWord NVCalibrator::IRRPRepairer_PeekImageResult ()
{
    return IRRPRepairer::Done == IRRPRepairer_PeekImageStatus (NULL, NULL) ? m_dOutResult : ERROR_IO_PENDING;
};

int NVCalibrator::IRRPRepairer_GrabImage (void * pDstBuf   ,
					  DWord  dDstPitch ,
					  int	 nFromRow  ,
					  int	 nNumRows  )
{
    int	       nMove = 0;

    if (NULL == m_pOutBuf)
    {	return nMove;}

    Word *  pSrcBuf = m_pOutBuf + (m_nOutPitch * nFromRow);

    for (; (nFromRow < m_nOutSizeY) && (0 < nNumRows);)
    {
	memmove (pDstBuf, pSrcBuf, m_nOutSizeX * 2);

	nMove	 ++;
	nFromRow ++;
	nNumRows --;

      ((Byte *&) pDstBuf) += (0 <= ((int) dDstPitch) ? dDstPitch : (m_nOutSizeX * 2));
	pSrcBuf		  += (m_nOutPitch);
    }
	return nMove;
};

DWORD NVCalibrator::LoadCalibrFileException (NVCalibrator * pThis, PEXCEPTION_RECORD r)
{
    if ((EXCEPTION_ACCESS_VIOLATION	     ==		r ->ExceptionCode	    )
    &&  (1				     ==		r ->NumberParameters	    )
    &&	((void *)((Mamo_AmplCalibr *) pThis) == (void *) r ->ExceptionInformation [0]))
    {
	pThis ->m_nCalcPhase.CompareExchange (NVCalibrator::Calc_Capture,
					      NVCalibrator::Calc_Prepare);
	pThis ->m_nOutPhase .Exchange	     (IRRPRepairer::Capturing);

	return EXCEPTION_CONTINUE_EXECUTION ;
    }
	return EXCEPTION_CONTINUE_SEARCH    ;
};

GResult	NVCalibrator::RepairProc (int	  nUAnode,
				  int	  nIAnode,
				  DWord   dFlags ,
				  DWord	  dParam ,
				  TCHAR * pPath	 , HANDLE hResume)
{
    int	    p_iFieldType =	    // 0x00 : Full
				    // 0x01 : 21x30 (4376 x 6250)
				    // 0x02 : 18x24 (3750 x 5000)
				    // 0x03 : 12x12 (2500 x 2500)
	    (int )((dFlags & 0x0003) >> 0);

    _U32    p_uRPMask	 =	    // 0x80 : Enable repairing
				    // 0x01 : Do adaptive call
				    // 0x02 : Do clear white pixels
				    // 0x04 : Do duplicate pixels
	    (_U32)((dFlags & 0x8700) >> 8);

    int	    p_i12_Bad	 = HIBYTE(HIWORD(dParam));
    int	    p_i12_Insert = LOBYTE(HIWORD(dParam));
    int	    p_i23_Bad	 = HIBYTE(LOWORD(dParam));
    int	    p_i23_Insert = LOBYTE(LOWORD(dParam));

    TCHAR 	pBaseDir [MAX_PATH];
    strcpy     (pBaseDir, pPath);

		 ::SetEvent (hResume);		    // Resume creator...

    if (m_pOutBuf)
    {
	m_pOutBuf = NULL;
    }

    SetDublicateParam (0, 1537 + p_i12_Bad,
			  3074 + p_i23_Bad,
		       0,	 p_i12_Bad,
				 p_i23_Bad);

    Word * pOutImage	    = NULL;
    int	   nOutImageSizeX   = 0	  ;
    int	   nOutImageSizeY   = 0	  ;

    if (p_uRPMask & 0x80)
    {{
	    if (Return::OK != PreloadAllCalibrName (pBaseDir))
	    {
		return ERROR_FILE_NOT_FOUND;
	    }
		char * pCOFFileName;

	    {{
		int LoadRet;

		__try
		{
		    LoadRet = LoadCalibrFile (nUAnode, m_nSrcSizeX  ,
						       m_nActiveBeg ,
						       m_nActiveEnd ,
						       m_nDCBeg	    ,
						       m_nDCEnd	    , & pCOFFileName);

		} __except (LoadCalibrFileException (this, (GetExceptionInformation ()) ->ExceptionRecord))
		{}
//
// Inside exception set :
//
//	    m_nOutPhase .Exchange	 (IRRPRepairer::Capturing);

		    printf ("\n====\nNVCalibrator::Done Load {%s}!!!\n====\n", pCOFFileName);

		if (LoadRet != Return::OK)
		{
		if (LoadRet == Return::CFF_ERROR)
		{
		    printf ("\t...CCF file fomat error\n");

		    return ERROR_BAD_FORMAT;
		}
		    return ERROR_NOT_ENOUGH_MEMORY;
		}
	    }}
//
// Inside exception set :
//
//	    m_nCalcPhase.CompareExchange (Calc_Capture, Calc_Prepare);

	    while (Calc_Capture == m_nCalcPhase.GetValue ())		// Wait done input image
	    {
		printf (".");

		::SleepEx (200, True);
	    }

	    if (Calc_Cancel  == m_nCalcPhase.GetValue ())
	    {
		EraseCalibrParam ();

		return (GResult) -1;
	    }

	//	SaveImage (NULL, m_pSrcBuf, m_nSrcSizeX, m_nSrcSizeY);
	//
	    if (Return::OK != Repair ((p_uRPMask & 0x02) ? true : false, // Clear white pixels
				      (p_uRPMask & 0x04) ? true : false, // Do Duplicate 
			      (Word *) m_pSrcBuf	,
				       m_nSrcSizeX      ,		 // Rewrite (const void *) !!!
				       m_nCurSizeY      ,
				     & pOutImage        ,
				     & nOutImageSizeX   ,
				     & nOutImageSizeY   ))
	    {
		printf ("\n====\nNVCalibrator::Done Calc Fail\n====\n");

		EraseCalibrParam ();

		return (GResult) -1;
	    }
		EraseCalibrParam ();

		printf ("\n===\nNVCalibrator::Done Calc %d:%d\n====\n", nOutImageSizeX, nOutImageSizeY);
    }}
    else
    {{
	    m_nCalcPhase.CompareExchange (Calc_Capture, Calc_Prepare);

	    m_nOutPhase .Exchange	 (IRRPRepairer::Capturing);

	    while (Calc_Capture == m_nCalcPhase.GetValue ())		// Wait done input image
	    {
		::SleepEx (200, True);
	    }

		nOutImageSizeX	= m_nSrcSizeX ;
		nOutImageSizeY  = m_nCurSizeY ;
		pOutImage	= new WORD [nOutImageSizeX * nOutImageSizeY];

		memcpy (pOutImage, m_pSrcBuf, nOutImageSizeX * nOutImageSizeY * sizeof (Word));
    }}
//
//............................................................................
//
    if (pOutImage)
    {
// Center src line <<<
//
//  if (4 < m_nCurSizeY) memset ((Byte *) pOutImage + ((m_nSrcSizeX * 2) * ((m_nCurSizeY / 2) - 2)), 0x8000, m_nSrcSizeX * 2 * 4);
//
// Center src >>>

	m_pOutBuf   = m_pSrcBuf	;

    {{

 	struct LinePos
	{
	    int	    nBase    ;	// Base position
	    int	    nLeftBd  ;	// Bad  pixels
	    int	    nLeftGd  ;	// Good pixels
	};

//////////////////////////////////////

	    int	    Begin  = 0;

	    int	    Good1  = (/*RAW_IMAGE_LINEX * 1*/ 1537) + p_i12_Bad;
	    int	    nBad1  = p_i12_Bad	  ;
	    int	    nAdd1  = p_i12_Insert ;

	    int	    Good2  = (/*RAW_IMAGE_LINEX * 2*/ 3074) + p_i23_Bad;
	    int	    nBad2  = p_i23_Bad	  ;
	    int	    nAdd2  = p_i23_Insert ;

/////////////////////////////////////

	    int	    SrcOffset  = Begin;

	    int	    DstPitch   = ((nOutImageSizeX - SrcOffset + nAdd1 + nAdd2) / 2) * 2;
	    int	    DstOffset  = 0;

// Clear dst image <<<
//
//	memset	  (m_pOutBuf  , 0x0000, DstPitch * nOutImageSizeY * 2);
//
// Clear dst image >>>

	CropImage (m_pOutBuf  , DstPitch		,
			        DstOffset		,
				0			,
		     pOutImage, nOutImageSizeX		,
				SrcOffset		,
				0			,
				Good1 - SrcOffset	,
				nOutImageSizeY		);

		DstOffset = DstOffset + (Good1 - SrcOffset + nAdd1);
		SrcOffset = SrcOffset + (Good1 - SrcOffset	  );

	CropImage (m_pOutBuf  , DstPitch		,
				DstOffset		,
				0			,
		     pOutImage, nOutImageSizeX		,
				SrcOffset		,
				0			,
				Good2 - SrcOffset	,
				nOutImageSizeY		);

		DstOffset = DstOffset + (Good2 - SrcOffset + nAdd2);
		SrcOffset = SrcOffset + (Good2 - SrcOffset	  );

	CropImage (m_pOutBuf  , DstPitch		,
				DstOffset		,
				0			,
		     pOutImage, nOutImageSizeX		,
				SrcOffset		,
				0			,
				nOutImageSizeX - SrcOffset,
				nOutImageSizeY		);


	    m_nOutPitch = DstPitch	 ;

	    m_nOutSizeX = DstPitch	 ;
	    m_nOutSizeY = nOutImageSizeY ;

	    printf ("\n\n%08X:%d, Image %d (%d) x %d\n\n", dFlags, p_iFieldType, m_nOutSizeX, m_nOutPitch, m_nOutSizeY);

	if (p_uRPMask & 0x01)
	{{
	    printf ("Beg   interpolate\n");

	    if (0 < nBad1 + nAdd1)
	    {
		printf ("\tadd_interp :: for %d (%d)\n", Good1 - Begin +     0 - nBad1, nBad1 + nAdd1);

		add_interp (m_pOutBuf, Good1 - Begin +     0 - nBad1,
						       nBad1 + nAdd1, m_nOutSizeX, m_nOutSizeY);
	    }

	    if (0 < nBad2 + nAdd2)
	    {
		printf ("\tadd_interp :: for %d (%d)\n", Good2 - Begin + nAdd1 - nBad2, nBad2 + nAdd2);

		add_interp (m_pOutBuf, Good2 - Begin + nAdd1 - nBad2,
						       nBad2 + nAdd2, m_nOutSizeX, m_nOutSizeY);
	    }

	    printf ("End   interpolate\n");
	}}

//	    m_nOutPitch = DstPitch	 ;
//
//	if (0 == p_iFieldType)	    // Full
//	{
//	    m_nOutSizeX = DstPitch	 ;
//	    m_nOutSizeY = nOutImageSizeY ;
//	}
//	else
	if (1 == p_iFieldType)	    // 21x30 (4376 x 6250)
	{
	    if (m_nOutSizeX > 4376)
		m_nOutSizeX = 4376;
	    if (m_nOutSizeY / 2 > 6250 / 2)
		m_nOutSizeY = 6250;
	}
	else
	if (2 == p_iFieldType)	    // 18x24 (3750 x 5000)
	{
	    if (m_nOutSizeX > 3750)
		m_nOutSizeX = 3750;
	    if (m_nOutSizeY / 2 > 5000 / 2)
		m_nOutSizeY = 5000;
	}
	else
	if (3 == p_iFieldType)	    // 12x12 (2500 x 2500)
	{
	    if (m_nOutSizeX > 2500)
		m_nOutSizeX = 2500;
	    if (m_nOutSizeY / 2 > 2500 / 2)
		m_nOutSizeY = 2500;
	}
	    m_pOutBuf  += ( ( ((nOutImageSizeY / 2) - (m_nOutSizeY / 2)) * DstPitch) + (DstPitch - m_nOutSizeX));

	    printf ("\n\n%08X:%d, Out : %d (%d) x %d", dFlags, p_iFieldType, m_nOutSizeX, m_nOutPitch, m_nOutSizeY);

	    printf (" with Beg : %d\n\n", (nOutImageSizeY / 2) - (m_nOutSizeY / 2));
    }}

	delete pOutImage;
	       pOutImage = NULL;
    }

    if (Calc_Cancel == m_nCalcPhase.GetValue ())
    {
	return (GResult) 1;
    }
	return ERROR_SUCCESS;
};

GResult GAPI NVCalibrator::RepairProc (int nArgs, void ** pArgs, HANDLE hResume)
{
    NVCalibrator *  pThis =
   (NVCalibrator *)(pArgs [0]);

    pThis ->m_nOutPhase.Exchange (IRRPRepairer::Preparing);

    GResult dResult	  = pThis ->RepairProc 
			(* (int  *)(pArgs [1]),
			 * (int  *)(pArgs [2]),
			 * (DWord*)(pArgs [3]),
			 * (DWord*)(pArgs [4]),
			   (TCHAR*)(pArgs [5]), hResume	 );
    
    pThis ->m_nOutPhase.Exchange (IRRPRepairer::Done     );

    return  dResult;
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
IRRPCALIB_EXPORT GResult WINAPI	CreateRRPRepairer	(IRRPRepairer	 *&
								 pInterface,
							 LPCTSTR pClassName)
{
    if ((NULL == pClassName) || (False))
    {
	return NVCalibrator::CreateInstance (pInterface);
    }
	return (GResult) ERROR_NOT_SUPPORTED;
};

IRRPCALIB_EXPORT GResult WINAPI	CreateRRPCalibrator	(IRRPRepairer	 *&
								 pInterface,
							 LPCTSTR pClassName)
{
	return (GResult) ERROR_NOT_SUPPORTED;
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
IRRPCALIB_EXPORT GResult WINAPI Accumulate (TCHAR * pFileName, TCHAR * pPath)
{
    int	    n	     =     0;
    Bool    bCreated = False;

    Mamo_AmplCalibr    Calibr ;
    Word_Image	       ImgIn  ;

    TCHAR   pName [MAX_PATH];

    HANDLE	    hf;
    WIN32_FIND_DATA fi;

    strcpy (pName, pPath);
    strcat (pName, _T("\\*.*"));

    for (hf = ::FindFirstFile (pName, & fi); hf && (hf != INVALID_HANDLE_VALUE);)
    {
    if  (0 ==  (fi.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
    {
	strcpy (pName, pPath	   );
	strcat (pName, _T("\\")	   );
	strcat (pName, fi.cFileName);

	if (! ImgIn.LoadImage (pName))
	{   n = -1; goto lFail;}

//GSh <<

printf ("a+%s\n", pName);

//GSh >>

	if (! bCreated)
	{
	    bCreated = True ;

	    Calibr.OpenAccumulate (ImgIn.m_Nx, ImgIn.m_Ny);
	}
	    Calibr.Accumulate	  (ImgIn.GetAddress    ());

	    ImgIn.Erase ();

	    n ++;
    }
	if (::FindNextFile (hf, & fi))
	{
	    continue;
	}

lFail :
	    ::FindClose	   (hf);

	    break   ;
    }

    if (0 < n)
    {
	if (! Calibr.SaveAccumulate (pFileName))
	{/*  n = -1;*/}
    }
	    return (0 < n) ? ERROR_SUCCESS : -1;
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
IRRPCALIB_EXPORT GResult WINAPI Calibrate (int	   nUA	      ,
					   int	   nActiveBeg ,
					   int	   nActiveEnd ,
					   int	   nDCBeg     ,
					   int	   nDCEnd     ,
					   int	   kX	      ,
					   int	   kY	      ,
					   TCHAR * pPath      )
{
    Mamo_AmplCalibr  Calibr ;
    Word_Image	     Img    ;

    Calibr.SetField	    (nActiveBeg ,
			     nActiveEnd ,
			     nDCBeg	, 
			     nDCEnd	);

    Calibr.SetSmoothCoeff   (kX, kY	);

    if (0 != Calibr.PreloadAllImages (pPath))
    {	
	return -1;
    }
    
    TCHAR     pFileName [MAX_PATH];

    wsprintf (pFileName, "%s\\..\\%d.cff", pPath, nUA);

    if (0 != Calibr.Calibrate (nUA, pFileName))
    {	
	return -2;
    }

	return ERROR_SUCCESS;
};
//
//[]------------------------------------------------------------------------[]

#ifdef _WIN32

BOOL APIENTRY DllMain (HINSTANCE hInstance, DWORD dReason, LPVOID)
{
    switch (dReason)
    {
    case DLL_PROCESS_ATTACH :

	if (! GWinInit (hInstance))
	{
	    return FALSE;
	}
	break;

    case DLL_PROCESS_DETACH :

	      GWinDone ();
	break;
    };
	return TRUE;
};

#endif
//
//[]------------------------------------------------------------------------[]
