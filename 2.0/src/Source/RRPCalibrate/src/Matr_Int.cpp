//----------------------------------------------------------------------------
//    Novikov
//    19.12.2006
//----------------------------------------------------------------------------
//#include "stdafx.h"
#include "Matr_Int.h"

//-----------------------------------------------------------------------------
Matr_Int::Matr_Int ():m_Addr (NULL), m_Nx(0), m_Ny(0)
{
   m_FlgPrint = 0;
};
//-----------------------------------------------------------------------------
Matr_Int::Matr_Int (int Nx, int Ny)
{
   Create (Nx, Ny);
   m_FlgPrint = 0;
};
//-----------------------------------------------------------------------------
Matr_Int::~Matr_Int ()
{
   Erase ();
};
//-----------------------------------------------------------------------------
void Matr_Int::Copy (Matr_Int * Obj)
//   *this = *Obj
{
   Erase ();
 
   if (Obj->Check ()){
      Create (Obj->m_Nx, Obj->m_Ny);

      m_FlgPrint  = Obj->m_FlgPrint;

      memcpy (m_Addr [0], Obj->m_Addr [0], m_Nx * m_Ny * sizeof (int));
   }
};
//-----------------------------------------------------------------------------
void Matr_Int::Create (int Nx, int Ny)
{
   if (Nx <= 0 || Ny <= 0){
      return;
   }

   m_Nx = Nx;
   m_Ny = Ny;
   
   m_Addr = new int* [Ny];
   int y;
   for (y = 0; y < Ny; y++)
      m_Addr [y] = NULL;

   m_Addr[0] = new int [Nx*Ny];

   for (y = 0; y < Ny; y++){
      m_Addr[y] = m_Addr[0] + Nx * y;
   }
};
//-----------------------------------------------------------------------------
void Matr_Int::Erase ()
{
   if (m_Addr){
      if (m_Addr[0]){
         delete [] m_Addr [0];
      }
      delete [] m_Addr;
   }
   m_Addr = NULL;
   m_Nx = m_Ny = 0;
};
//-----------------------------------------------------------------------------
void Matr_Int::Clear ()
{
   if (m_Addr){
      if (m_Addr[0])
         memset (m_Addr[0], 0, sizeof (int) * m_Nx * m_Ny);
   }
};
//-----------------------------------------------------------------------------
bool Matr_Int::Check ()
{
   if (m_Addr == NULL || m_Addr[0] == NULL) 
      return false;
   else
      return true;
};
//-----------------------------------------------------------------------------
void Matr_Int::Write (FILE *hFile)
{
   if (!Check ()) return;

   for (int y = 0; y < m_Ny; y++){
//      fprintf (hFile, "[%3d]    ", y);
      for (int x = 0; x < m_Nx; x++){
//         fprintf (hFile, "%3d  ", m_Addr[y][x]);
         fprintf (hFile, "%3d \t %3d \t %7d  \n", x, y, m_Addr[y][x]);
      }
//      fprintf (hFile, "\n");
   }
   fprintf (hFile, "\n");
};
//----------------------------------------------------------------------------
void Matr_Int::Save (char *Name)
{
   if (!m_FlgPrint) return;

   FILE *hFile = fopen (Name, "w");
   Write (hFile);
   fclose (hFile);
};
//----------------------------------------------------------------------------
