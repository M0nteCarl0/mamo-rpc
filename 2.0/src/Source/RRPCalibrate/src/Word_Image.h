//-----------------------------------------------------------------------------
//    Novikov  V
//    date 28.01.2005
//    ver. 1.00
//-----------------------------------------------------------------------------
#ifndef __WORD_IMAGE__
#define __WORD_IMAGE__

#include "stdafx.h"
//-----------------------------------------------------------------------------
#pragma warning(disable:4786)
#include <vector>
using namespace std ;
//-----------------------------------------------------------------------------
class Word_Image
{
   public:
      enum {PIX_EMPTY = 0x0000, PIX_FULL = 0x7FFF};

      WORD   **m_Addr;
      int      m_Nx;
      int      m_Ny;
      bool     m_FlgAlias;
      
      Word_Image ();
      Word_Image (int Nx, int Ny);
      Word_Image (int Nx, int Ny, WORD *Addr);
      virtual ~Word_Image ();

      void Create (int Nx, int Ny, WORD *Addr = NULL);
      void Erase ();
      void Clear ();
      bool Check ();

      void Copy      (Word_Image *Img);
      void CopyZoom  (int K, Word_Image *Img);
      void Invert    ();
      void Subtract     (Word_Image *Img);
      void SubtractAbs  (Word_Image *Img);
      void Add       (Word_Image *Img);
      void Add       (WORD Val);
      void Multiply  (double Val);
      void Binary    (WORD Level);
      void Eros      ();
      void CombineLabeles  (int Mask1, int Mask2, int Y);
      int  Labeling        ();

      void GetMeanRms (CRect *Rect, double *Mean, double *Rms);
      void GetMeanRms (double *Mean, double *Rms);
      WORD GetMax     ();
      WORD GetMin     ();
      double GetSumm    ();
      double GetSumm2   ();
      double GetAverage ();
      
      void GetImageNormMax (int *Nx, int *Ny, WORD *Addr);
      void PutImage (int  Nx, int  Ny, WORD *Addr);

      void Print ();

      WORD * GetAddress (){return m_Addr[0];}

      void FlipHor (Word_Image *ImgOut = NULL);
      void FlipVer (Word_Image *ImgOut = NULL);

//--- MAMMO  MAMMO  MAMMO  MAMMO  MAMMO  MAMMO  
      void Smooth (int Kx, int Ky, Word_Image &ImgOut);
      void ChangePixels (int OriginalPix, int DublicatePix);
                                             //       if DublicatePix > 0 ����������� ������� ������ �� OriginalPix
                                             //       if DublicatePix < 0 ����������� ������� �����  �� OriginalPix
      int * ProfileX ();

      bool LoadImage    (char *Name, int Y0 = 0, int Y1 = 0);
      bool WriteImage   (char *Name);
};
//-----------------------------------------------------------------------------
#endif