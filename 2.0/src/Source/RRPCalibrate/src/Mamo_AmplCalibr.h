//-----------------------------------------------------------------------------
//    Novikov
//    Date 21.10.10
//    Amplitude Calibration
//-----------------------------------------------------------------------------
#ifndef __MAMO_AMPLCALIBR__
#define __MAMO_AMPLCALIBR__
//-----------------------------------------------------------------------------

#include "stdafx.h"

#include "Word_Image.h"
#include "Matr_Int.h"

//-----------------------------------------------------------------------------
#pragma warning(disable:4786)
#include <map>
using namespace std;
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
class Mamo_AmplCalibr
{
   public:
      enum Return {OK=0, ER=1, OPEN_ER=2, RW_ER=3, CFF_ERROR=98, ALLOC_ERROR=99, UNKNOWN_ERROR=100}; 
      enum {NUM_POL = 3};

                                                                     // ��������� ����� ����������
      struct FitHeader{
         int      Key;                                               // ������ ��������� �����������
         int      W;                                                 // ������ ��������� �����������
         int      H;                                                 // ������ �� Y ��������� ����������� 
         int      Yimg0;                                             // �����  �� Y ��������� �����������   
         int      Yimg1;                                             // ������ �� Y ��������� �����������
         int      Ydc0;                                              // �����  �� Y ��������� �����������
         int      Ydc1;                                              // ������� ����������
         float    HV;
         void Clear () {memset (this, 0, sizeof (FitHeader));};
      };
      FitHeader  m_Header;
                                                                              //---------------------------------------------------------------
   protected:
      struct TwoImage{
         Word_Image * Active;
         Word_Image * Dc;
         int        * Profile;
         double       Mean;
      };
  
      typedef map  <double, TwoImage*>  MAP_DOUB_WORDIMAGE;
      MAP_DOUB_WORDIMAGE   m_ImgMap;
      TwoImage    * m_ImgVect;
//---

      struct FitParam{
         float   Param[NUM_POL];
      };

#define	FIT_BLOCK_NUM	8
#define	FIT_BLOCK_LEN	(96 * 1024 * 1024 / sizeof (FitParam))

	struct FtParams
	{
	    int		m_cArray    ;
	    FitParam *	m_pArray [FIT_BLOCK_NUM];


			FtParams () { m_cArray = 0; memset (m_pArray, 0, sizeof (m_pArray));};

		       ~FtParams () { Delete ();};


	    void	Delete	 ()
			{
			    for (; 0 < m_cArray;)
			    {
				free  (m_pArray [-- m_cArray]);
				       m_pArray [   m_cArray] = NULL;
			    }
			};

	    Return	Alloc	 (int  nLength)
			{
			    if  ((FIT_BLOCK_NUM * FIT_BLOCK_LEN) < nLength)
			    {
				    return Return::ER;
			    }

			    for (; 0 < nLength;)
			    {
				if (NULL == (m_pArray [m_cArray ++] = (FitParam *) malloc (FIT_BLOCK_LEN * sizeof (FitParam))))
				{
				    m_cArray --;

				    return Return::ALLOC_ERROR;
				}
				    nLength  -= FIT_BLOCK_LEN;
			    }
				    return Return::OK;
			};

	    int		GetSize	  () const
			{
			    return m_cArray * FIT_BLOCK_LEN;
			};


	    FitParam *	operator [] (int i)
			{
			    return m_pArray [i / FIT_BLOCK_LEN] + (i % FIT_BLOCK_LEN);
			};


/*
	    FitParam **m_Param;
	    FtParams() : m_Param(NULL), size_(0), nbfcnt_(0), block_size_(0) {};
	    void Alloc(size_t size) 
	    {
		const int len = 1024*1024*96;
		size_ = size;
		if (sizeof(FitParam)*size_ > len)
		{
		    nbfcnt_ = sizeof(FitParam)*size_ / len + 1;
		    block_size_ = len/sizeof(FitParam);
		}
		else
		{
		    nbfcnt_ = 1;
		}
		m_Param = new FitParam *[nbfcnt_];
		for (int i = 0; i<nbfcnt_; i++)
		{
		    m_Param[i] = (FitParam*) malloc(block_size_*sizeof(FitParam));
		}
	    }
	    ~FtParams()
	    {
		Delete();
	    }

	    void Delete()
	    {
		if (m_Param)
		{
		    for (int i = 0; i<nbfcnt_; i++)
		    {
			free(m_Param[i]);
		    }
		    delete[] m_Param;
		    m_Param = NULL;
		}
	    }

	    float* operator[] (int i)
	    {
		int j = i%block_size_;
		int k = i/block_size_;
		return m_Param[k][j].Param;
	    }

	    FitParam* GetBlock(int i)
	    {
		if (i >= nbfcnt_)
		{
		    return NULL;
		}
		return m_Param[i];
	    }

	    size_t size_;
	    size_t block_size_;
	    int nbfcnt_;
*/
	};

      //FitParam *m_Param;
	FtParams m_Param;	
      char m_NameFileLoaded [_MAX_PATH];

  		HANDLE m_HFcof;
//---
      int const m_Ampl_Max;      // 0xFFFF
      int const m_AmplOffset;    // ��������� ��������� ����� ������� �����������
      int const m_SizeMean;      // ������������� ��� ���������� �������� �������� ��������� �����������

      int m_Nx;         // size W of Active Image
      int m_Ny;         // size H of Active Image

      int m_Yimg0;                 
      int m_Yimg1;
      int m_Ydc0 ;
      int m_Ydc1 ;
      int m_Kx   ;
      int m_Ky   ;
      CRect  m_RectMean;

      int m_X_CCD1;
      int m_X_CCD2;
      int m_X_CCD3;
      int m_X_Dublicate1;
      int m_X_Dublicate2;
      int m_X_Dublicate3;

      int  m_NumImg;
                                                                        // REPAIR   REPAIR   REPAIR      
      typedef map  <int, char*>  MAP_INT_TXT;
      MAP_INT_TXT   m_CalibrNameMap;

      Word_Image m_ImgActiveRepair;
      Matr_Int   m_ImgAccumulate;
      int        m_NumAccumImage;
                                                                              //---------------------------------------------------------------
   public:
      Mamo_AmplCalibr ();
      virtual ~Mamo_AmplCalibr ();

                                                                        // ����� ����������� Y = 6450, X = 4608 pix
      void SetField (int Yimg0,                                         // ������ �� Y ��������� �����������
                     int Yimg1,                                         // �����  �� Y ��������� �����������   
                     int Ydc0,                                          // ������ �� Y ��������� �����������
                     int Ydc1)                                          // �����  �� Y ��������� �����������
      {
                           m_Yimg0  = Yimg0;
                           m_Yimg1  = Yimg1;
                           m_Ydc0   = Ydc0 ; 
                           m_Ydc1   = Ydc1 ; 
      };
      void SetSmoothCoeff     (int Kx,  int Ky) {m_Kx = Kx;  m_Ky = Ky;};
      void SetDublicateParam  (int X_CCD1,                                       // 1-� ���-��, ������ ���-� ������ ������� (���� - �����)
                               int X_CCD2,
                               int X_CCD3,
                               int X_Dublicate1,                                 // 1-� ���-��, ���. ������ ����� ���������  (���� - ������)
                               int X_Dublicate2,
                               int X_Dublicate3){
                                                   m_X_CCD1          = X_CCD1      ;
                                                   m_X_CCD2          = X_CCD2      ;
                                                   m_X_CCD3          = X_CCD3      ;
                                                   m_X_Dublicate1    = X_Dublicate1;
                                                   m_X_Dublicate2    = X_Dublicate2;
                                                   m_X_Dublicate3    = X_Dublicate3;};





                                                                        // ACCUMULATE
      void  OpenAccumulate    (int Nx, int Ny);
      int   Accumulate        (WORD *Addr);
      int   SaveAccumulate    (char *Name);


      int   PreloadAllImages (char * ImgPath);                          // ��������� � ������ ��� ����� *.FLR �� ���-� ImgPath
      int   Calibrate        (float HV, char *NameOutCOF);              // �������������� � �������� ���� ���������� NameOutCOF, ���������� HV


      int   PreloadAllCalibrName (char * CffPath);
      int   LoadCalibrFile       (int HV, int iWidth, int iActiveBeg, int iActiveEnd, int iDCBeg, int iDCEnd, char **NameCalibr);
      int   LoadCalibrFile       (char *Name, int, int, int, int, int);       // ��������� � ������ ������������� ����
      int   LoadCalibrFileHeader (char *Name);
      int   Repair (bool FlgEraseWP, bool FlgDublicate, WORD *AddrIn, int NxIn, int NyIn, WORD **AddrOut, int *NxOut, int *NyOut);
                                                                              // ��������� ����� �����������, �������� ������ ��� ����������� �����������
                                                                              //  ���:
                                                                              // WORD *AddrIn, int NxIn, int NyIn    - ����� � ������ � ����-� ������ �����������
                                                                              // WORD *AddrOut, int NxOut, int NyOut - ����� � ������ � ����-� ������������ �����������
                                                                              

      void  EraseCalibrParam  ();                                             // ������� �� ������ ������ �����-� ����������

                                                                              //---------------------------------------------------------------
   protected:

      void SetRectMean     (CRect &Rect)     {m_RectMean = Rect;};
      void Clear ();

      void Erase              ();
      void EraseImgMap        ();
      void EraseCalibrNameMap ();
      void EraseRepairImage   ();                                             // ������� �� ������ ����������� �����������

      char * GeCalibrName (int HV);

      void  FirstMethod ();
      void  CopyImgMap  ();

      double Determ (double m00, double m01, double m02,
                     double m10, double m11, double m12,
                     double m20, double m21, double m22);
      void FitByPol_2 (double *x, double *y, double *Er, int N, double *A, double*B, double *C);

      void Sort (int *Addr, int N);
      int  MaxF (int *Addr, int N);
      int  MinF (int *Addr, int N);
      void CutOffWhitePoints (Word_Image &ImgIn, Word_Image &ImgOut);

      int  OpenWriteCOF    (char *Name);
      int  OpenReadCOF     (char *Name);
      void CloseCOF        ();
      int  WriteHeaderCOF  (float HV);
      int  ReadHeaderCOF   ();
      int  WriteRowCOF     (FitParam *Param, int Len);
//    int  ReadRowCOF      (FitParam *Param, int Len);
      int  ReadRowCOF	   (int Len);
};
//-----------------------------------------------------------------------------
#endif
