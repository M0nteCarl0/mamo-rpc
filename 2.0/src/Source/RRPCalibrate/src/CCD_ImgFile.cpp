//-----------------------------------------------------------------------------
//    Novikov  V
//    Date 08.08.04
//    ver. 1.00
//-----------------------------------------------------------------------------
#include "stdafx.h"

#include "CCD_ImgFile.h"
#include <stdio.h>
#include <math.h>
//-----------------------------------------------------------------------------
CCD_ImgFile::CCD_ImgFile ()
{
   _Old_new_handler = _set_new_handler (my_new_handler);

	m_Width       = 1;
	m_Height      = 1;
   m_OffsetW     = 0;
   m_OffsetH     = 0;

   m_FragmNum    = 1;
	m_Mean  = 1;
	m_HeightFragm = 0;
   m_Addr        = NULL;
   m_HF          = 0;
};
//-----------------------------------------------------------------------------
CCD_ImgFile::CCD_ImgFile (int Width, int Height)
{
   _Old_new_handler = _set_new_handler (my_new_handler);

	m_Width       = Width;
	m_Height      = Height;
   m_OffsetW     = 0;
   m_OffsetH     = 0;

   m_FragmNum    = 1;
	m_Mean  = 1;
	m_HeightFragm = 0;
   m_Addr        = NULL;
   m_HF          = 0;
};
//-----------------------------------------------------------------------------
CCD_ImgFile::~CCD_ImgFile ()
{
   Close ();
   FreeMemory ();

   _set_new_handler(_Old_new_handler);
};
//--------------------------------------------------------------------------
void CCD_ImgFile::AllocMemory ()
{
   FreeMemory ();
   m_HeightFragm = m_Height/m_FragmNum;
   m_HeightFragm = m_HeightFragm + (m_Height % m_FragmNum);
   m_Addr = new WORD [m_Width * m_HeightFragm];
};
//--------------------------------------------------------------------------
void CCD_ImgFile::FreeMemory ()
{
   if (m_Addr){
      delete [] m_Addr;
      m_Addr = NULL;
   	m_HeightFragm = 0;
   }
};
//--------------------------------------------------------------------------
bool  CCD_ImgFile::OpenRead	(char *Name)
{
   strcpy (m_FileName, Name);
   m_HF = CreateFile   (m_FileName, GENERIC_READ,
			    0,
			    NULL,
			    OPEN_EXISTING,
			    FILE_ATTRIBUTE_NORMAL,
			    NULL);
   if (INVALID_HANDLE_VALUE == m_HF){
      return false;
   }
   return true;
};
//--------------------------------------------------------------------------
bool  CCD_ImgFile::OpenWrite  (char *Name)
{
   strcpy (m_FileName, Name);
   m_HF = CreateFile   (m_FileName, GENERIC_READ | GENERIC_WRITE,
			    0,
			    NULL,
			    CREATE_ALWAYS,
			    FILE_ATTRIBUTE_NORMAL,
			    NULL);
   if (INVALID_HANDLE_VALUE == m_HF){
      return false;
   }
   return true;
};
//--------------------------------------------------------------------------
void CCD_ImgFile::Close ()
{
   if (m_HF){
      CloseHandle (m_HF);
      m_HF          = 0;
   }
};
//--------------------------------------------------------------------------
bool CCD_ImgFile::ReadHeader ()
{
   SetFilePointer (m_HF, 0, NULL, FILE_BEGIN);

   unsigned long   Rev;
   ReadFile (m_HF, &m_FlrHead,  sizeof (m_FlrHead), &Rev, NULL);

   if (Rev               == sizeof (m_FlrHead)    &&
        m_FlrHead.ffType == FLR_HEADER_MARKER)
         return true;
   else
         return false;
};
//--------------------------------------------------------------------------
void CCD_ImgFile::GetHeaderWidthHeight (int *Width, int *Height)
{
   *Width   = m_FlrHead.ffWidth;
   *Height  = m_FlrHead.ffHeight;
};
//-----------------------------------------------------------------------------
bool CCD_ImgFile::ReadImage (WORD *AddrImg, int YB, int YE)
{
   bool Ret = true;

   if (!ReadHeader ()){ 
      Close (); 
      return false; 
   }

   int Len = m_Width * sizeof (WORD);

   SetFilePointer (m_HF, YB * Len , NULL, FILE_CURRENT);

   unsigned long   Rev;

   for (int y = 0; y < YE-YB; y++){

      BOOL R = ReadFile (m_HF, AddrImg, Len, &Rev, NULL);

      if (Rev != (unsigned long)Len){
         Close ();
         Ret = false; 
         break;
      }
      AddrImg += m_Width;
   }

   return Ret;
};
//-----------------------------------------------------------------------------
bool CCD_ImgFile::ReadImgFragm (int FragCurr, WORD *AddrImg, int *Row)
{
   bool Ret = true;
   int FirstPixel;
   unsigned long   Rev;
   int Len;
   int y;
   int FragRow    ;
   int FragRowCurr;
   WORD * AddrTmp = NULL;
   
   *Row = 0;

   if (!m_FragmNum || FragCurr >= m_FragmNum || m_FragmNum > m_Height)  {Ret = false; goto _Error;}

   if (!ReadHeader ()){ Close (); Ret = false; goto _Error;}

   if (m_FlrHead.ffWidth < (m_Width+m_OffsetW) || m_FlrHead.ffHeight < (m_Height+m_OffsetH)) {Close (); Ret = false; goto _Error;}

   FragRow     = m_Height/m_FragmNum;
   FragRowCurr = FragRow;

   if (FragCurr == (m_FragmNum - 1))
      FragRowCurr = FragRow + m_Height % m_FragmNum;

   FirstPixel = (m_OffsetH + FragRow * FragCurr) * m_FlrHead.ffWidth + m_OffsetW;

   SetFilePointer (m_HF, FirstPixel * sizeof (WORD) , NULL, FILE_CURRENT);

   Rev;
   Len = m_Width * sizeof (WORD);

   AddrTmp = AddrImg;
   for (y = 0; y < FragRowCurr; y++){
      BOOL R = ReadFile (m_HF, AddrTmp, Len, &Rev, NULL);
      if (Rev != (unsigned long)Len){
         Close ();
         Ret = false; goto _Error;
      }
      AddrTmp += m_Width;
      SetFilePointer (m_HF, (m_FlrHead.ffWidth-m_Width)*sizeof (WORD) , NULL, FILE_CURRENT);
   }

   *Row = FragRowCurr;

_Error:
/*
      AddrTmp = AddrImg + (FragRowCurr-1)*m_Width;
      int HalfY = FragRowCurr/2;
      for (y = 0; y < HalfY; y++){
         for (int x = 0; x < m_Width; x++){
            WORD V = AddrImg [x];
            AddrImg [x] = AddrTmp [x];
            AddrTmp [x] = V;
         }
         AddrImg += m_Width;
         AddrTmp -= m_Width;
      }
*/
   return Ret;
};

//-----------------------------------------------------------------------------
bool CCD_ImgFile::ReadImgFragm (int FragCurr, int *Row)
{ 
   bool Ret = ReadImgFragm (FragCurr, m_Addr, Row);

   return Ret;
};
//-----------------------------------------------------------------------------
bool CCD_ImgFile::ReadImg (WORD *AddrImg)
{
   int Row;
   for (int i = 0; i < (int) m_FragmNum; i++){
      bool Ret = ReadImgFragm (i, AddrImg, &Row);
      if (!Ret)   return false;
//?????      AddrImg += m_Width * Row;
   }

   return true;
};
//-----------------------------------------------------------------------------
bool CCD_ImgFile::WriteImg (WORD *AddrImg)
{
   SetFilePointer (m_HF, 0, NULL, FILE_BEGIN);
   unsigned long   Rev;

   FLRFILEHEADER Head;
	Head.ffType    = FLR_HEADER_MARKER;
	Head.ffWidth   = m_Width;
	Head.ffHeight  = m_Height;
	Head.ffBpp     = 16;
	Head.ffBitsOffset = 0;

	DWORD dwDataLen = Head.ffWidth*Head.ffHeight*(Head.ffBpp>>3);

   WriteFile (m_HF, &Head, sizeof (FLRFILEHEADER), &Rev, NULL);
   if (Rev != sizeof (FLRFILEHEADER))      return false;

/*   
   WORD * AddrTmpT = AddrImg;
   WORD * AddrTmpD = AddrImg + (m_Height-1)*m_Width;
   int HalfY = m_Height/2;
   for (int y = 0; y < HalfY; y++){
      for (int x = 0; x < m_Width; x++){
         WORD V = AddrTmpT [x];
         AddrTmpT [x] = AddrTmpD [x];
         AddrTmpD [x] = V;
      }
      AddrTmpT += m_Width;
      AddrTmpD -= m_Width;
   }
*/

   WriteFile (m_HF, AddrImg, m_Width*m_Height*sizeof (WORD), &Rev, NULL);
   if (Rev != m_Width*m_Height*sizeof (WORD))      return false;

   return true;
};
//-----------------------------------------------------------------------------
bool CCD_ImgFile::MakeBrightness ()
{
   return MakeBrightness (m_FlrHead.ffWidth/4, m_FlrHead.ffHeight/4, m_FlrHead.ffWidth/2, m_FlrHead.ffHeight/2);
};
//-----------------------------------------------------------------------------
bool CCD_ImgFile::MakeBrightness (int OffsetW, int OffsetH, int W, int H)
{
   m_Mean = 0;
                                 // Store m_Width, m_Height, OffsetWidth, OffsetHeight
   int WidthTmp  = m_Width;
   int HeightTmp = m_Height;

   SetWidthHeight (W, H);

   int OffsetWidthTmp  = m_OffsetW;
   int OffsetHeightTmp = m_OffsetH;
   
   SetOffset      (OffsetW, OffsetH);

   int Size = m_Width * m_Height;
   if (!Size)  return false;

   WORD *Addr = new WORD [Size];

   if (!ReadImg (Addr)){ Close (); return false;}
   
   for (int i = 0; i < Size; i++){
      m_Mean += (double) Addr[i]; 
   }
   delete [] Addr;

   m_Mean = m_Mean / (double) Size;

                                 // Restore m_Width, m_Height, OffsetWidth, OffsetHeight
   SetWidthHeight (WidthTmp, HeightTmp);
   SetOffset      (OffsetWidthTmp, OffsetHeightTmp);

   return true;
};
//-----------------------------------------------------------------------------
bool CCD_ImgFile::MakeRMS ()
{
   return MakeRMS (m_FlrHead.ffWidth/2, m_FlrHead.ffHeight/2, m_FlrHead.ffWidth/10, m_FlrHead.ffHeight/10);
};
//-----------------------------------------------------------------------------
bool CCD_ImgFile::MakeRMS (int OffsetW, int OffsetH, int W, int H)
{
   m_Mean = 0;
                                 // Store m_Width, m_Height, OffsetWidth, OffsetHeight
   int i;
   int WidthTmp  = m_Width;
   int HeightTmp = m_Height;

   SetWidthHeight (W, H);

   int OffsetWidthTmp  = m_OffsetW;
   int OffsetHeightTmp = m_OffsetH;
   
   SetOffset      (OffsetW, OffsetH);

   int Size = m_Width * m_Height;
   if (!Size)  return false;

   WORD *Addr = new WORD [Size];

   if (!ReadImg (Addr)){ Close (); return false;}
   
   double Mean = 0;
   for (i = 0; i < Size; i++){
      Mean += (double) Addr[i]; 
   }

   Mean = Mean / (double) Size;

   m_RMS = 0;
   double V;
   for (i = 0; i < Size; i++){
      V = (double) Addr[i] - Mean; 
      m_RMS += V*V;
   }

   m_RMS = sqrt (m_RMS/(double) Size);

   delete [] Addr;
                                 // Restore m_Width, m_Height, OffsetWidth, OffsetHeight
   SetWidthHeight (WidthTmp, HeightTmp);
   SetOffset      (OffsetWidthTmp, OffsetHeightTmp);

   return true;
};
//-----------------------------------------------------------------------------
void CCD_ImgFile::PrintImg (WORD *AddrImg, int NX, int NY)
{
   for (int y = 0; y < NY; y++){
      for (int x = 0; x < NX; x++){
         printf ("%5u ", AddrImg [y*NX+x]);
      }
      printf ("\n");
   }
};
//-----------------------------------------------------------------------------
void CCD_ImgFile::PrintImgFragm (int Rows)
{
   if (Rows > m_HeightFragm)   return;
   printf ("Image Fragment: Width = %d, Height = %d\n", m_Width, Rows);
   PrintImg (m_Addr, m_Width, Rows);
};
//-----------------------------------------------------------------------------
// MakeMeanRms       MakeMeanRms       MakeMeanRms       MakeMeanRms
//-----------------------------------------------------------------------------
void MakeMeanRms (WORD *Addr, int Width, int Height, CRect *Rect, double *Mean, double *Rms)
{
   int W = Rect->right  - Rect->left;
   int H = Rect->bottom - Rect->top;
   int Size = W * H;
   if (Size <= 0){
      *Mean = 0;
      *Rms = 0;
      return;
   }

   double Summ = 0;
   int XY = Rect->top * Width + Rect->left;

   int y;
   for (y = 0; y < H; y++){
      for (int x = 0; x < W; x++){
         Summ += (double) Addr[XY + x]; 
      }
      XY += Width;
   }
   double MeanA = Summ / (double) Size;

   Summ = 0;
   XY = Rect->top * Width + Rect->left;
   for (y = 0; y < H; y++){
      for (int x = 0; x < W; x++){
         double V = (double) Addr[XY + x] - MeanA;
         Summ += V*V;
      }
      XY += Width;
   }
   double RmsA = sqrt (Summ/(double)Size);

   *Mean = MeanA;
   *Rms  = RmsA;
};
//-----------------------------------------------------------------------------
void MakeMean (WORD *Addr, int Width, int Height, CRect *Rect, double *Mean)
{
   int W = Rect->right  - Rect->left;
   int H = Rect->bottom - Rect->top;
   int Size = W * H;
   if (Size <= 0){
      *Mean = 0;
      return;
   }

   double Summ = 0;
   int XY = Rect->top * Width + Rect->left;

   for (int y = 0; y < H; y++){
      for (int x = 0; x < W; x++){
         Summ += (double) Addr[XY + x]; 
      }
      XY += Width;
   }
   double MeanA = Summ / (double) Size;

   *Mean = (double) MeanA;
};
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
