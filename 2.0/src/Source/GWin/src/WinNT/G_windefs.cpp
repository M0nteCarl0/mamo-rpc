//[]------------------------------------------------------------------------[]
// Global windows application definitions
//
//
// Created by GSh.
//[]------------------------------------------------------------------------[]

#include "g_windefs.h"
#include "g_stddefs.cpp"
#include "g_winbase.h"

//[]------------------------------------------------------------------------[]
//
GResult GAPI CreateWakeUp (IWakeUp *& pIWakeUp)
{
    GWakeUpPort * pWakeUpPort = 
    TCurrent () ->GetWakeUpPort ();

    if (pWakeUpPort)
    {
	return pWakeUpPort ->CreateWakeUp (pIWakeUp);
    }
	return TPool () ->CreateWakeUp	  (pIWakeUp);
};

GResult GAPI CreateIoCoWait (IIoCoWait *& pIIoCoWait)
{
/*
    GWakeUpPort * pWakeUpPort = 
    TCurrent () ->GetWakeUpPort ();

    if (pWakeUpPort)
    {
	return pWakeUpPort ->CreateIoCoWait (pIIoCoWait);
    }
*/
	return (pIIoCoWait = NULL, ERROR_NOT_SUPPORTED);
};
//
//[]------------------------------------------------------------------------[]
