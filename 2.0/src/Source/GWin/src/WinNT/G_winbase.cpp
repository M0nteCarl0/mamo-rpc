//[]------------------------------------------------------------------------[]
// Main GWin definitions
//
//
// Created by GSh.
//[]------------------------------------------------------------------------[]

#include "G_winbase.h"

#include <stdio.h>

//[]------------------------------------------------------------------------[]
// Kernel wrappers :
//
//
DWORD GAPI WaitForAny (int nHandles, ... /* Handles [nHandle], dTimeout */)
{
    va_list    args;
    va_start  (args, nHandles);

    DWORD      dResult  = (1 < nHandles)
			? ::WaitForMultipleObjects (nHandles, (HANDLE *) args,
						    False   ,
						 * (DWORD *)(((HANDLE *) args) + nHandles))
			: ::WaitForSingleObject    (	    * (HANDLE *) args,
						 * (DWORD *)(((HANDLE *) args) + 1));

    if ((WAIT_ABANDONED_0 <= dResult) && (dResult < WAIT_ABANDONED_0  + nHandles))
    {
	       dResult = dResult - WAIT_ABANDONED_0 + WAIT_OBJECT_0;
    }
	return dResult;
};

DWORD GAPI WaitForAnyAlertable (int nHandles, ... /* Handles [nHandle], dTimeout */)
{
    va_list    args;
    va_start  (args, nHandles);

    DWORD      dResult  = (1 < nHandles)
			? ::WaitForMultipleObjectsEx (nHandles, (HANDLE *) args,
						      False   ,
						   * (DWORD *)(((HANDLE *) args) + nHandles),
						      True    )
			: ::WaitForSingleObjectEx    (	      * (HANDLE *) args,
						   * (DWORD *)(((HANDLE *) args) + 1),
						      True    );

    if ((WAIT_ABANDONED_0 <= dResult) && (dResult < WAIT_ABANDONED_0  + nHandles))
    {
	       dResult = dResult - WAIT_ABANDONED_0 + WAIT_OBJECT_0;
    }
	return dResult;
};

DWORD GAPI WaitForAll (int nHandles, ... /* Handles [nHandle], dTimeout */)
{
    va_list    args;
    va_start  (args, nHandles);

    DWORD      dResult  = (1 < nHandles)
			? ::WaitForMultipleObjects (nHandles, (HANDLE *) args,
						    True    ,
						 * (DWORD *)(((HANDLE *) args) + nHandles))
			: ::WaitForSingleObject    (	    * (HANDLE *) args,
						 * (DWORD *)(((HANDLE *) args) + 1));

    if ((WAIT_ABANDONED_0 <= dResult) && (dResult < WAIT_ABANDONED_0  + nHandles))
    {
	       dResult = dResult - WAIT_ABANDONED_0 + WAIT_OBJECT_0;
    }
	return dResult;
};

DWORD GAPI WaitForAllAlertable (int nHandles, ... /* Handles [nHandle], dTimeout */)
{
    va_list    args;
    va_start  (args, nHandles);

    DWORD      dResult  = (1 < nHandles)
			? ::WaitForMultipleObjectsEx (nHandles, (HANDLE *) args,
						      True    ,
						   * (DWORD *)(((HANDLE *) args) + nHandles),
						      True    )
			: ::WaitForSingleObjectEx    (	      * (HANDLE *) args,
						   * (DWORD *)(((HANDLE *) args) + 1),
						      True    );

    if ((WAIT_ABANDONED_0 <= dResult) && (dResult < WAIT_ABANDONED_0  + nHandles))
    {
	       dResult = dResult - WAIT_ABANDONED_0 + WAIT_OBJECT_0;
    }
	return dResult;
};

HANDLE GAPI DuplicateHandle (HANDLE hSrcHandle)
{
    HANDLE hDupHandle = NULL;

    if	  (hSrcHandle)
    {
    ::DuplicateHandle (::GetCurrentProcess (),
		       hSrcHandle	     ,
		       ::GetCurrentProcess (),
		     & hDupHandle	     ,
		       0		     ,
		       False		     ,
		       DUPLICATE_SAME_ACCESS );
    }

    return hDupHandle;
};

#pragma pack (_M_PACK_VPTR)

    struct _userAPCParam
    {
	GUserAPCProc	pProc	;
	int		cArgs   ;
	void **		pArgs   ;
	HANDLE		hResume ;
    };

#pragma pack ()

static void CALLBACK thunk_APCProc (ULONG_PTR pParam)
{
//  pParam may be invalid, if hResume == NULL
//
//
    if (((_userAPCParam *) pParam) ->pProc  )
    {
       (((_userAPCParam *) pParam) ->pProc  )
       (((_userAPCParam *) pParam) ->cArgs  ,
	((_userAPCParam *) pParam) ->pArgs  ,
	((_userAPCParam *) pParam) ->hResume);
    }
    else
    if (((_userAPCParam *) pParam) ->hResume)
    {
	::SetEvent
       (((_userAPCParam *) pParam) ->hResume);
    }
};

static void CALLBACK thunk_APCNull (ULONG_PTR hResume)
{
    if ((HANDLE) hResume)
    {
	::SetEvent ((HANDLE) hResume);
    }
};

Bool GAPI QueueUserAPC (HANDLE hThread, GUserAPCProc pProc, int cArgs, void ** pArgs, HANDLE hResume)
{
    _userAPCParam   Param = {pProc, cArgs, pArgs, hResume};

    if ((pProc) ? ::QueueUserAPC (thunk_APCProc, hThread, (ULONG_PTR) & Param  )
		: ::QueueUserAPC (thunk_APCNull, hThread, (ULONG_PTR)   hResume))
    {
	if (hResume)
	{
//				::WaitForAny	      (1, hResume, INFINITE);
	while (WAIT_OBJECT_0 != ::WaitForAnyAlertable (1, hResume, INFINITE)) {}
	}

	return True ;
    }
	return False;
};

#pragma pack (_M_PACK_VPTR)

    struct _APCCoParam
    {
	GAPCCoProc	pProc	;
	GIrp *		pIrp	;
    };

#pragma pack ()

static void CALLBACK thunk_APCCoProc (ULONG_PTR pParam)
{
//  pParam is part of the pIrp stack !!!
//
//
    if (((_APCCoParam *) pParam) ->pProc )
    {
       (((_APCCoParam *) pParam) ->pProc )
       (((_APCCoParam *) pParam) ->pIrp  );
    }
    else
    {
	CompleteIrp

       (((_APCCoParam *) pParam) ->pIrp  );
    }
};

Bool GAPI QueueAPCCoProc (HANDLE hThread, GAPCCoProc pProc, GIrp * pIrp)
{
    _APCCoParam * pParam = (_APCCoParam *) pIrp ->alloc (sizeof _APCCoParam);

    pParam ->pProc = pProc ;
    pParam ->pIrp  = pIrp  ;

    return (Bool) ::QueueUserAPC (thunk_APCCoProc, hThread, (ULONG_PTR) pParam);
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
// Synchronization :
//
//
Bool GCriticalSection::Lock (void * pfLockContext, LockType t)
{
    (t != release) ? ::EnterCriticalSection ((CRITICAL_SECTION *) pfLockContext)
		   : ::LeaveCriticalSection ((CRITICAL_SECTION *) pfLockContext);
    return True;
};

Bool GSyncEvent::Lock (void * pfLockContext, LockType t)
{
    if (* (HANDLE *) pfLockContext)
    {
    return (t != release) ? (::WaitForSingleObject (* (HANDLE *) pfLockContext, INFINITE), True)
			  : (::SetEvent		   (* (HANDLE *) pfLockContext),	   True);
    }
    return True;
};

Bool GSemaphore::Lock (void * pfLockContext, LockType t)
{
    if (* (HANDLE *) pfLockContext)
    {
    return (t != release) ? (::WaitForSingleObject (* (HANDLE *) pfLockContext, INFINITE), True)
			  : (::ReleaseSemaphore	   (* (HANDLE *) pfLockContext, 1,  NULL), True);
    }
    return True;
};

Bool GMutex::Lock (void * pfLockContext, LockType t)
{
    if (* (HANDLE *) pfLockContext)
    {
    return (t != release) ? (::WaitForSingleObject (* (HANDLE *) pfLockContext , INFINITE), True)
			  : (::ReleaseMutex	   (* (HANDLE *) pfLockContext),	    True);
    }
    return True;
};
//
//[]------------------------------------------------------------------------[]
//
void GFifoThreadQueue::Lock (GThread::Entry * e)
{
    m_tLock.LockAcquire ();

	if (NULL == e)
	{
	    if (0 == -- m_cCurr)
	    {
		    m_tCurr = m_tQueue.extract_first ();

		if (m_tCurr)
		{
		    m_cCurr  ++;

    m_tLock.LockRelease ();

		    ::SetEvent (m_tCurr ->pThread ->GetResume ());

		    return;
		}
	    }
	}
	else
	{
	    if	   (m_tCurr == NULL)
	    {
		    m_tCurr = e;

		    m_cCurr  ++;
	    }
	    else
	    {
		if (m_tCurr ->pThread == TCurrent ())
		{
		    m_cCurr  ++;
		}
		else
		{
		    m_tQueue.append (e);

    m_tLock.LockRelease ();

		    ::WaitForAny  (1, e ->pThread ->GetResume (), INFINITE);

		    return;
		}
	    }
	}

    m_tLock.LockRelease ();
};

Bool GFifoThreadQueue::Lock (void * pfLockContext, LockType t)
{
    if (release == t)
    {
	((GFifoThreadQueue *) pfLockContext) ->Lock (NULL);

	return True;
    }

	GThread::Entry	      te;

	te.pThread = TCurrent ();

	((GFifoThreadQueue *) pfLockContext) ->Lock (& te);

	return True ;
};
//
//[]------------------------------------------------------------------------[]



//[]------------------------------------------------------------------------[]
//
class GRdWrThreadQueue : public GAccessLock
{
public :
		GRdWrThreadQueue (const GAccessLock & src) : m_tLock (src)
		{
		    m_tCurr = NULL;
		    m_cCurr = NULL;

		    Init (Lock, this);
		};

	       ~GRdWrThreadQueue () {};
protected :

	void	    Lock	(GThread::Entry *);
static	Bool	    Lock	(void *, LockType);

private :

	GAccessLock		m_tLock	    ; // Lock for (*)

	GThread::List		m_tRdQueue  ; // (*)
	GThread::List		m_tWrQueue  ; // (*)
	GThread::Entry *	m_tCurr	    ; // (*)
	int			m_cCurr	    ; // (*)
};
//
//[]------------------------------------------------------------------------[]


//[]------------------------------------------------------------------------[]
//
Bool GRdWrThreadQueue::Lock (void * pfLockContext, LockType t)
{
    return False;

};
//
//[]------------------------------------------------------------------------[]














//[]------------------------------------------------------------------------[]
//
Bool GCloseLock::Open  ()
{
    if (0 == m_cOpen.CompareExchange (1, 0))
    {
	m_cClose ++;

	m_rClose.Exchange (NULL);

	m_cOpen  ++;

	return True ;
    }
	return False;
};

GIrp * GAPI GCloseLock::Xxxx_Co_Close (GIrp * pIrp, void * pCloseLock, void *)
{
    {
	((GCloseLock *) pCloseLock) ->m_cOpen.Exchange (0);
    }
	return pIrp;
};

void GCloseLock::QueueForClose (GIrp * pIrp)
{
// GASSERT CloseAcquire () return True !!!
//
    {
	SetCurCoProc (pIrp, Xxxx_Co_Close, this, NULL);

	m_rClose.Exchange (pIrp);

	LockRelease  ();
    }
};

Bool GCloseLock::CloseAcquire ()
{
    return (2 == m_cOpen.CompareExchange (1, 2)) ? True : False;
};

Bool GCloseLock::LockAcquire ()
{
    if (2 == m_cOpen.GetValue ())
    {
	m_cClose  ++;

    if (NULL == (m_rClose.GetValue ()))
    {
	return True ;
    }
	LockRelease ();
    }
	return False;
};

void GCloseLock::LockRelease ()
{
    if (0 < -- m_cClose)
    {
	return ;
    }

    GIrp * rClose =  m_rClose.Exchange 
		    ((GIrp *) INVALID_PTR);

    if   ((rClose == (GIrp *) INVALID_PTR)
    ||    (rClose == (GIrp *) NULL	 ))
    {
	return ;
    }

// GASSERT rClose = valid closing GIrp *, Dispatch it
//
// Do FileHandle close on any othe closing inside GIrp_DpProc...
//
	SetCurRequest (rClose,
	CreateRequest <IoCo_Close> (rClose) ->Init ());

	DispatchIrp   (rClose);
};
//
//[]------------------------------------------------------------------------[]













HRESULT GAPI RegisterClassObject (void ** hRegister, REFCLSID clsid, IUnknown * pIUnknown)
{
    return ERROR_SUCCESS;
};



#if FALSE


Bool GRdWrThreadQueue::Lock (void * pfLockContext, LockType t)
{
    if (t == release  )
    {
    }
    else
    if (t == shared   )
    {
	
    }
    else
//  if (t == exclusive)
    {
    }
};














class GSharedResourcePtr
{
public

	void *		    Create	    (int
};

class GClassObject
{
public :


	Bool		    Open	    ();
};

Bool GInterfacePtr::Open ()
{
};






class GSyncBarer.
{
    GSyncBarrier


    if (bLockAcquire ())
    {
    }


	m_Semaphore.LockAcquire ();

    if (m_CloseLock.Open ())
    {
	// Create object code...

	m_Sempathore.Release	();

	return ...
    }
	








}
#endif









//[]------------------------------------------------------------------------[]
// Sync Event object
//
//
HANDLE GAPI OpenResume ()
{
    GThread *  tCurrent = TCurrent ();

    if (tCurrent)
    {
	return tCurrent ->GetResume ();
    }
	return ::CreateEvent (NULL, False, False, NULL);
};

void GAPI CloseResume (HANDLE hResume)
{
    GThread *  tCurrent = TCurrent ();

    if (tCurrent && (tCurrent ->GetResume () == hResume))
    {
	return ;
    }
    if (hResume)
    {
	::CloseHandle (hResume);
    }
};
/*
HANDLE GAPI TCurrentResume ()
{
    GThread * tCurrent = TCurrent ();

    return (tCurrent) ? tCurrent ->GetResume () : NULL;
};

DWord GAPI TCurrentId ()
{
    GThread * tCurrent = TCurrent ();

    return (tCurrent) ? tCurrent ->GetId () : ::GetCurrentThreadId ();
};
*/
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
// Windows heap allocator
//
//
void GHeapAllocator::Destroy (GHeapAllocator * pAlloc)
{
    if (pAlloc)
    {
	HANDLE  hHeap	    = pAlloc ->m_hHeap	   ;
	DWord   dHeapFlags  = pAlloc ->m_dHeapFlags;

	::HeapFree    (hHeap, dHeapFlags, pAlloc);
	::HeapDestroy (hHeap);
    }
};

GHeapAllocator * GHeapAllocator::Create (SIZE_T	szInit, SIZE_T szHigh, DWord dFlags)
{
    GHeapAllocator *	pAlloc = NULL;
    HANDLE		hHeap  = ::HeapCreate
    (dFlags & (HEAP_GENERATE_EXCEPTIONS | HEAP_NO_SERIALIZE), szInit, szHigh);

    if (hHeap )
    {
	    pAlloc = new (::HeapAlloc (hHeap, dFlags, sizeof (GHeapAllocator))) 
		     GHeapAllocator; 
	if (pAlloc)
	{
	    pAlloc ->m_dHeapFlags = dFlags;
	    pAlloc ->m_hHeap      = hHeap ;
	}
    }

    return  pAlloc;
};

void * GHeapAllocator::__alloc (size_t dSize)
{
    return (m_hHeap) ? ::HeapAlloc (m_hHeap, m_dHeapFlags, dSize) : NULL;
};

void * GHeapAllocator::__free (void * ptr)
{   
    return (ptr && m_hHeap) 
	   ? ((::HeapFree (m_hHeap, m_dHeapFlags, ptr)) ? NULL : ptr)
	   : ptr;
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
static Bool GAPI defDispIdle ()
{
    return False;
};

static void GAPI defWaitIdle (DWord dTimeout)
{
};

GException_Handler::GException_Handler (Bool (GAPI * pHandler)(void *, GException_Record &), void * pContext)
{
    this ->pHandler = pHandler ;
    this ->pContext = pContext ;

    GThread * th = GetCurThread ();

    pParent = th ->m_pTopExHandler;
	      th ->m_pTopExHandler = this;
};

GException_Handler::~GException_Handler ()
{
   (GetCurThread ()) ->m_pTopExHandler = pParent;
};

Bool GAPI RaiseException (DWord dExCode, int nExParams, void ** pExParams)
{
    GException_Record r = {dExCode, nExParams, pExParams};

    for (GException_Handler * p = GetCurThread () ->m_pTopExHandler; p; p = p ->pParent)
    {
    if ((p ->pHandler)(p ->pContext, r))
    {
	return True ;
    }
    }
	return False;
};

GThread::GThread (GThread::Type tType)
{
    m_pOldTls	    = ::TlsGetValue
		     (g_GWinModule.dTlsIndex);

    m_tHandle	    = ::DuplicateHandle
		     (::GetCurrentThread   ());

    m_tResume	    = ::CreateEvent (NULL, False, False, NULL);

    m_tId	    = ::GetCurrentThreadId ();
    m_tType	    = tType;

		      ::TlsSetValue 
		     (g_GWinModule.dTlsIndex, this);

    m_pCurApartment = NULL;
    m_pWakeUpPort   = NULL;
    m_pTopExHandler = NULL;
};

GThread::~GThread ()
{
		      ::TlsSetValue 
		     (g_GWinModule.dTlsIndex, m_pOldTls);
    if (m_tResume)
    {
	::CloseHandle (m_tResume);
    }
    if (m_tHandle)
    {
	::CloseHandle (m_tHandle);
    }
};

    struct _createThreadParams
    {
	HANDLE		hResume	      ; // Set this event if MainProc can normal work or do some work.
					// Not touch if MainThreadProc can not normal work.
					// Path ErrorCode throw thread exit code.
	GWinMainProc    pMainProc     ;
	int		nMainProcArgs ;
	void **		pMainProcArgs ;
    };

/*
DWORD GThread::WaitPerform (int nWait, const HANDLE * hWait, DWORD dTimeout)
{
    if (nWait)
    {
	return ::WaitForMultipleObjectsEx 
		(nWait, hWait, False, dTimeout, True);	// Enable Alertable !!!
    }
	return ::SleepEx	     (dTimeout, True);	// Enable Alertable !!!
};


void GThread::_doneProc ()
{
};
*/

GResult GThread::_threadProc (void * pParams)
{
    _createThreadParams *  tp =
   (_createThreadParams *) pParams   ;

    GResult dExitCode = ERROR_SUCCESS;

    if (m_tHandle && m_tResume)
    {
	if (tp ->pMainProc)
	{
	    dExitCode = 

	   (tp ->pMainProc)(tp ->nMainProcArgs,
			    tp ->pMainProcArgs,
			    tp ->hResume      );
	}
	else
	{						// Set Resume and return SUCCESS 
	    ::SetEvent	   (tp ->hResume      );	// if has not MainProc
	}
    }
    else
    {
	    dExitCode = ERROR_NO_SYSTEM_RESOURCES;
    }

    return  dExitCode;
};

GResult GThread::_create (HANDLE *	  pHandle	,
			  LPTHREAD_START_ROUTINE     
					  te		,
			  GWinMainProc	  pMainProc     ,
			  int		  nMainProcArgs	,
			  void **	  pMainProcArgs	,
			  SIZE_T	  dStackSize    ,
			  LPSECURITY_ATTRIBUTES
			    		  pSecAttributes)
{
    GResult	 dResult   =	ERROR_SUCCESS ;

    _createThreadParams tp = {::OpenResume () ,
				pMainProc     ,
				nMainProcArgs ,
				pMainProcArgs };
    if (pHandle) 
    {
	       * pHandle = NULL;
    }

    if (NULL ==	(tp.hResume))
    {	
	return ERROR_NO_SYSTEM_RESOURCES;
    }

    HANDLE	 hThread;
    DWORD	 dThread;

    if (NULL == (hThread = ::CreateThread ( pSecAttributes ,
					    dStackSize     ,
					    te		   ,
					  & tp		   ,
					    0		   ,
					  & dThread	   )))
    {
		 dResult    = ::GetLastError ();
    }
    else
    {
	if ((WAIT_OBJECT_0 == WaitForAny (2, tp.hResume, hThread, INFINITE))
	||  (WAIT_OBJECT_0 == WaitForAny (1, tp.hResume		,	 0)))
	{
	    if (pHandle)
	    {
	      * pHandle	    = hThread;
			      hThread = NULL;
	    }
	}
	else
	{
	    ::GetExitCodeThread (hThread, & dResult);

	    if (dResult	   == ERROR_SUCCESS)
	    {
		dResult	    = ERROR_OPERATION_ABORTED;
	    }
	}
	if (hThread)
	{
	    ::CloseHandle (hThread);
	}
    }
	    ::CloseResume (tp.hResume);

	return  dResult;
};
/*
GWakeUpPort * GThread::GetWakeUpPort ()
{
//  return TPool () ->GetWakeUpPort ();

    return NULL;
};

Bool GThread::PumpInputWork ()
{
    return False;
};

void GThread::WaitInputWork (DWord dTimeout)
{};
*/



GResult GThread::RunApartment (GApartment * pa, int nArgs, void ** pArgs, HANDLE hResume)
{
    GResult	 dResult;
    GApartment * pOldApp;

    pOldApp =  m_pCurApartment	   ;
	       m_pCurApartment = pa;

    pa ->m_pCoThread   = this;

    pa ->m_XUnknown  .Init    (pa);
    pa ->m_IApartment.Init    (pa, IID_IApartment, & pa ->m_XUnknown);
    pa ->m_XUnknown  .Release ();

    if (m_pWakeUpPort)
    {
	m_pWakeUpPort ->EnterWaitLoop ();
    }

	dResult = pa ->InitInstance (nArgs, pArgs, hResume);

    pa ->m_IApartment.Release ();

    if (ERROR_SUCCESS == dResult)
    {
	dResult = pa ->WorkLoop (NULL);
    }
    else
    {
		  pa ->WorkLoop (NULL);
    }
    
    if (m_pWakeUpPort)
    {
	m_pWakeUpPort ->LeaveWaitLoop ();
    }
		  pa ->Done ();

    pa ->m_pCoThread   = NULL;

	       m_pCurApartment = pOldApp;

    return dResult;
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
GWakeUpPort::GWakeUpPort (int		nWakeUpArray,
			  WakeUpEntry * eWakeUpArray)
{
    m_cRefCount	    .Init (0);

    m_tOwner	    = NULL;
    m_oCurrent	    = NULL;

    m_syncSent	    .Init (False);

    memset (eWakeUpArray, 0, sizeof (WakeUpEntry) * nWakeUpArray);

    for (int i = 0; i < nWakeUpArray; i ++)
    {
	m_freeList.append (eWakeUpArray + i);
    }
};

void GWakeUpPort::free (WakeUpEntry * e)
{
    TXList_Iterator <WakeUpEntry> I;

    if (m_oCurrent == e)
    {
	m_oCurrent  = NULL;
    }
	e ->m_pOwner	  = NULL ;

    if (e ->m_qList)
    {
	e ->m_qList ->extract (e);	// ??? Not
	e ->m_qList	  = NULL ;	// ??? Not
    }
	e ->m_hWaitFor	  = NULL	    ;
	e ->m_tWaitFor	    .Disactivate () ;
	e ->m_dPeriod	  = INFINITE	    ;

	e ->m_eOnReason   = IWakeUp::OnCancelled; 
	e ->m_pOnProc	  = NULL ;
	e ->m_pOnContext  = 
	e ->m_pOnArgument = NULL ;

    LockAcquire ();

	for (I  = m_freeList.head (); I.next ();)
	{
	    if (e < I.next ())
	    {
		break;
	    }
	    I <<= m_freeList.getnext (I.next ());
	};
		  m_freeList.insert  (I, e);

    LockRelease ();

	Release ();
};

GResult GWakeUpPort::CreateWakeUp (IWakeUp *& pIWakeUp)
{
    WakeUpEntry * e;

    LockAcquire ();

	pIWakeUp = e = new (m_freeList.extract_first ()) WakeUpEntry;

    LockRelease ();

    if (e)
    {
	AddRef  ();

	e ->m_cRefCount     .Init (1)	    ;
	e ->m_pOwner	  = this	    ;
	e ->m_qList	  = NULL	    ;

	e ->m_hWaitFor	  = NULL 	    ;
	e ->m_tWaitFor	    .Disactivate () ;
	e ->m_dPeriod	  = INFINITE	    ;

	e ->m_eOnReason   = IWakeUp::OnCancelled; 
	e ->m_pOnProc	  = NULL ;
	e ->m_pOnContext  = 
	e ->m_pOnArgument = NULL ;

	return ERROR_SUCCESS	 ;
    }
	return ERROR_NO_SYSTEM_RESOURCES;
};
//
//[]------------------------------------------------------------------------[]
//
void GAPI GWakeUpPort::Cancel_Co_Delay (GIrp * pIrp, GAccessLock *, void * pContext)
{
//  pPort ->LockAcquire (); was called !!!
//
	GWakeUpPort *  pPort =
       (GWakeUpPort *) pContext ;

	pPort ->m_oCoList.append (pPort ->m_tCoList.extract (pIrp));

    pPort ->LockRelease ();

    pPort ->Break ();
};

void GWakeUpPort::QueueIrpForComplete (GIrp * pIrp, DWord dTimeout, Bool bCancelable)
{
    TXList_Iterator <GIrp>		 I;

    DWord	dCurTick = GetTickCount ();

    SetCurRequest		 (pIrp,
    CreateRequest <DelayRequest> (pIrp) ->Init (dTimeout, dCurTick));

    LockAcquire ();

	if (bCancelable)
	{
	    if  (! SetCancelProc (pIrp, & m_PLock, Cancel_Co_Delay, this))
	    {
    LockRelease ();

		::QueueIrpForComplete (pIrp);

		return;
	    }
	}
	for (I  = m_tCoList.head (); I.next ();)
	{
	    if (dTimeout < GetCurRequest <DelayRequest> (I.next ()) ->m_tWaitFor.Relative (dCurTick))
	    {
		break;
	    }
	    I <<= m_tCoList.getnext (I.next ());
	}
		  m_tCoList.insert  (I, pIrp);

	if  ((m_tOwner ->GetId () != TCurrent () ->GetId ()) && (m_tCoList.first () == pIrp))
	{
        if   (0 == m_syncSent.CompareExchange (1, 0))
	{
	    QueueUserAPC (m_tOwner ->GetHandle (), NULL, 0, NULL, NULL);
	}
	}

    LockRelease ();
};
//
//[]------------------------------------------------------------------------[]
//
HRESULT STDMETHODCALLTYPE GWakeUpPort::WakeUpEntry::QueryInterface (REFIID iid, void ** pi)
{
    if (pi)
    {
	if (IID_IUnknown == iid)
	{
	    return ((* ((IUnknown **) pi) = this) ->AddRef (), ERROR_SUCCESS);
	}
	    return ERROR_NOT_SUPPORTED;
    }
	    return ERROR_INVALID_PARAMETER;
};

ULONG STDMETHODCALLTYPE GWakeUpPort::WakeUpEntry::AddRef ()
{
    return  ++ m_cRefCount;
};

ULONG STDMETHODCALLTYPE GWakeUpPort::WakeUpEntry::Release ()
{
    if (0 < -- m_cRefCount)
    {
	return m_cRefCount.GetValue ();
    }
	return (m_pOwner ->free (this), 0);
};

void GWakeUpPort::queue (WakeUpEntry * e	   ,
			 HANDLE	       hWaitFor	   ,
			 IWakeUp::CallBack 
				       pOnProc	   ,
			 void *	       pOnContext  ,
			 void *	       pOnArgument ,
			 DWord	       dDueTime    ,
			 DWord	       dPeriod     )
{
    DWord	  dCurTick,
		  dRelTick;

    TXList_Iterator 
   <WakeUpEntry>	 I;
    WakeUpEntry::List *	 L;

    if (m_oCurrent == e)
    {
	m_oCurrent  = NULL;
    }

    if (e ->m_qList)
    {
	e ->m_qList ->extract (e);
	e ->m_qList	  = NULL ;
    }
	e ->m_eOnReason	  = IWakeUp::OnCancelled;
	e ->m_pOnProc     = pOnProc	  ;
	e ->m_pOnContext  = pOnContext	  ;
	e ->m_pOnArgument = pOnArgument	  ;

    if (NULL		 != 
       (e ->m_hWaitFor	  =	hWaitFor ))
    {
	e ->m_qList   = L = & m_hWaitList ;
    }
    else
    {
	e ->m_qList   = L = & m_tWaitList ;
    }
	e ->m_dPeriod     = dPeriod	  ;

	e ->m_tWaitFor	   .Disactivate ();

    if ((dDueTime != INFINITE)
    ||  (dPeriod  != INFINITE))
    {
	dCurTick = GetTickCount ();

	e ->m_tWaitFor	   .Activate ((dDueTime != INFINITE) ? dDueTime : dPeriod, dCurTick);

	dRelTick = e ->m_tWaitFor.Relative (dCurTick);

	for (I  = L ->head (); I.next ();)
	{
	    if (dRelTick < I.next () ->m_tWaitFor.Relative (dCurTick))
	    {
		break;
	    }

	    I <<= L ->getnext (I.next ());
	}
	    L ->insert (I, e);
    }
    else
    {
	    L ->append (   e);
    }
};

void GAPI GWakeUpPort::ex_queue	(ExCallEntry * te)
{
    te ->pEntry ->m_pOwner ->queue 
   (te ->pEntry,	    (HANDLE) te ->pExArgs [0],
		 (IWakeUp::CallBack) te ->pExArgs [1],
    				     te ->pExArgs [2],
				     te ->pExArgs [3],
			     (DWord) te ->pExArgs [4],
			     (DWord) te ->pExArgs [5]);
};

void STDMETHODCALLTYPE GWakeUpPort::WakeUpEntry::QueueWaitFor
		       (HANDLE		  hWaitFor    ,
			IWakeUp::CallBack pOnProc     ,
			void *		  pOnContext  ,
			void *		  pOnArgument ,
			DWord		  dDueTime    ,
			DWord		  dPeriod     )
{
    if (m_pOwner ->m_tOwner ->GetId () == TCurrent () ->GetId ())
    {
	m_pOwner ->queue (this,		  hWaitFor    ,
					  pOnProc     ,
					  pOnContext  ,
					  pOnArgument ,
					  dDueTime    ,
					  dPeriod     );
	return;
    }

    ExCallEntry		      te ;

    te.pThread	   = TCurrent () ;
    te.pEntry	   = this	 ;
    te.pExProc	   = ex_queue    ;
    te.pExArgs [0] = hWaitFor	 ;
    te.pExArgs [1] = pOnProc	 ;
    te.pExArgs [2] = pOnContext	 ;
    te.pExArgs [3] = pOnArgument ;
    te.pExArgs [4] = (void *)
		     dDueTime	 ;
    te.pExArgs [5] = (void *)
		     dPeriod	 ;

    m_pOwner ->ex_call	   (& te);
};

void GWakeUpPort::cancel (WakeUpEntry * e)
{
    if (m_oCurrent == e)
    {
	m_oCurrent  = NULL;
    }    
    if (e ->m_qList)
    {
	e ->m_qList ->extract (e) ;
	e ->m_qList = NULL	  ;

	e ->m_eOnReason  = WakeUpEntry::OnCancelled;

       (e ->m_pOnProc)(e, e ->m_eOnReason  ,
			  e ->m_pOnContext ,
			  e ->m_pOnArgument);
    }
};

void GAPI GWakeUpPort::ex_cancel (ExCallEntry * te)
{
    te ->pEntry ->m_pOwner ->cancel (te ->pEntry);
};

void STDMETHODCALLTYPE GWakeUpPort::WakeUpEntry::CancelWaitFor (Bool)
{
    if (m_pOwner ->m_tOwner ->GetId () == TCurrent () ->GetId ())
    {
	m_pOwner ->cancel (this);

	return;
    }

    ExCallEntry		      te ;

    te.pThread	    = TCurrent ();
    te.pEntry	    = this	 ;
    te.pExProc	    = ex_cancel  ;

    m_pOwner ->ex_call	   (& te);
};

static Bool GAPI XxxxIoCo_WakeUp (IWakeUp * pIWakeUp, IWakeUp::Reason reason, void *, void * o)
{
    if (reason == IWakeUp::OnTimeout)
    {
    if (! HasOverlappedIoCompleted ((GOverlapped *) o))
    {
	return True;
    }
    }
//  else
//  if (reason == IWakeUp::OnOccured)
//  {}
//  else
//  if (reason == IWakeUp::OnCancel )
//  {}
	return (((GOverlapped *) o) ->CallIoCoProc (), False);
};

/*
HANDLE STDMETHODCALLTYPE GWakeUpPort::WakeUpEntry::GetIoCoHandle ()
{
    return m_hWaitFor;
};

void STDMETHODCALLTYPE GWakeUpPort::WakeUpEntry::QueueIoCoWait (GOverlapped & o)
{
    HANDLE  hWaitFor = NULL	;
    DWORD   dPeriod  = INFINITE ;

    if (((ULONG_PTR) 1) <  ((ULONG_PTR) o.hEvent))
    {
	hWaitFor = (HANDLE)((ULONG_PTR) o.hEvent & (~((ULONG_PTR) 1)));
    }
    else
    {
	dPeriod  = 100;
    }

    if (m_pOwner ->m_tOwner ->GetId () == TCurrent () ->GetId ())
    {
	m_pOwner ->queue (this,	hWaitFor ,
			 XxxxIoCo_WakeUp ,
				NULL	 ,
			      & o	 ,
				INFINITE ,
				dPeriod  );
	return;
    }

    ExCallEntry		      te ;

    te.pThread	   = TCurrent () ;
    te.pEntry	   = this	 ;
    te.pExProc	   = ex_queue	 ;
    te.pExArgs [0] = hWaitFor	 ;
    te.pExArgs [1] = 
		 XxxxIoCo_WakeUp ;
    te.pExArgs [2] = NULL	 ;
    te.pExArgs [3] = & o	 ;
    te.pExArgs [4] = (void *)
		     INFINITE	 ;
    te.pExArgs [5] = (void *)
		     dPeriod	 ;

    m_pOwner ->ex_call	   (& te);
};
*/
void GAPI GWakeUpPort::ex_call (ExCallEntry * te)
{
    if (m_PLock.NotInited ())
    {
	::RaiseException (0x80000000, 0, 0, NULL);
    }

    LockAcquire ();

	m_syncList.append (te);
    
	if (0 == m_syncSent.CompareExchange (1, 0))
	{
	    QueueUserAPC (m_tOwner ->GetHandle (), NULL, 0, NULL, NULL);
	}

    LockRelease ();

	    WaitForAny   (1, te ->pThread ->GetResume (), INFINITE);
};

DWord GAPI GWakeUpPort::wait (WaitProc pWait, int nWait, const HANDLE * hWait, DWord dTimeout)
{
    if (pWait)
    {
	return (pWait)(nWait, hWait, dTimeout);
    }
    if (nWait)
    {
	return ::WaitForMultipleObjectsEx 

	        (nWait, hWait, False, dTimeout, True);	// Enable Alertable !!!
    }

    if (0 ==   ::SleepEx	     (dTimeout, True))	// Enable Alertable !!!
    {
	return WAIT_TIMEOUT;
    }
	return WAIT_IO_COMPLETION;
};
//
//[]------------------------------------------------------------------------[]
//
static int getwaitindex (int n, DWord dWaitResult)
{
	 if (					    (dWaitResult < (WAIT_OBJECT_0    + n)))
    {
	return (int) dWaitResult - WAIT_OBJECT_0;
    }
    else if ((WAIT_ABANDONED_0   <= dWaitResult) && (dWaitResult < (WAIT_ABANDONED_0 + n)))
    {
	return (int) dWaitResult - WAIT_ABANDONED_0;
    }
    else if ((WAIT_IO_COMPLETION == dWaitResult))   // Return n index in any case
    {
	return	n;
    }
    else if ((WAIT_TIMEOUT	 == dWaitResult))
    {
	return  n;
    }
    else if ((WAIT_FAILED	 == dWaitResult))
    {
	return  n;
    }
	return	n;
};

void GWakeUpPort::WaitWakeUp (WaitProc pWait, DWord dTimeout)
{
    int		      i, j, k;
    DWord	      t, u;

    TXList_Iterator 
   <WakeUpEntry>      I;

    TXList_Iterator
   <GIrp>	      J;

    int		      nWait ;
    HANDLE	      hWait [MAXIMUM_WAIT_OBJECTS];

    for (nWait = 0, I = m_hWaitList.head (); I.next ();)
    {
	 hWait [nWait ++] = I.next () ->m_hWaitFor;

	 I <<= m_hWaitList.getnext (I.next ());
    };

    if  (0 < dTimeout)
    {
	t = GetTickCount ();

	if  (     m_hWaitList.last  ()
	&&  ((u = m_hWaitList.first () ->m_tWaitFor.Relative (t)) < dTimeout))
	{
	     dTimeout = u;
	}
	if  (     m_tWaitList.last  ()
	&&  ((u = m_tWaitList.first () ->m_tWaitFor.Relative (t)) < dTimeout))
	{
	     dTimeout = u;
	}

    LockAcquire ();

	if  (	  m_tCoList.last   ()
	&&  ((u = GetCurRequest <DelayRequest> (m_tCoList.first ()) ->m_tWaitFor.Relative (t)) < dTimeout))
	{
	     dTimeout = u;
	}

    LockRelease ();
    }

	if (nWait > (k  = (i = getwaitindex (nWait - 0,
				wait (pWait, nWait - 0, hWait + 0, dTimeout)))))
    {
	    I = m_hWaitList.head ();

	    for (j = 0; j < i; j ++, I <<= m_hWaitList.getnext (I.next ()));

						 I.next () ->m_eOnReason = WakeUpEntry::OnOccured;
	    m_oList.append (m_hWaitList.extract (I	 ));

    for (; ++ k < nWait;)
    {
	if (nWait > (k += (i = getwaitindex (nWait - k,
				wait (NULL,  nWait - k, hWait + k, 0)))))
	{
	    for (j = 0; j < i; j ++, I <<= m_hWaitList.getnext (I.next ()));

						 I.next () ->m_eOnReason = WakeUpEntry::OnOccured;
	    m_oList.append (m_hWaitList.extract (I	 ));
	}
    }
    }
	t = GetTickCount ();

    for (I = m_hWaitList.head (); I.next () && (0 == I.next () ->m_tWaitFor.Relative (t));)
    {
						 I.next () ->m_eOnReason = WakeUpEntry::OnTimeout;
	    m_oList.append (m_hWaitList.extract (I	 ));
    }

    for (I = m_tWaitList.head (); I.next () && (0 == I.next () ->m_tWaitFor.Relative (t));)
    {
						 I.next () ->m_eOnReason = WakeUpEntry::OnTimeout;
	    m_oList.append (m_tWaitList.extract (I	 ));
    }

    {{
	union
	{
	    ExCallEntry *  te	    ;
	    GIrp *	   pIrp	    ;
	};

	GThread::List	   syncList ;

    LockAcquire ();

	  m_syncSent.Exchange (0);

	    syncList.append   (m_syncList);

	for (J = m_tCoList.head   (); J.next () && (0 == GetCurRequest <DelayRequest>
						   (J.next ()) ->m_tWaitFor.Relative (t));)
	{
	    ClrCancelProc  (J.next (), & m_PLock); 

	    m_oCoList.append (m_tCoList.extract	   (J	    ));
	}

    LockRelease ();

	while (NULL != (te   = (ExCallEntry *) syncList.extract_first ()))
	{
	    (te ->pExProc)(te);

	    ::SetEvent (te ->pThread ->GetResume ());
	}
    }}
};

Bool GWakeUpPort::PumpWakeUp ()
{
    if (NULL !=   (m_oCurrent = m_oList.extract_first ()))
    {
	if       ((m_oCurrent ->m_pOnProc)(m_oCurrent		      ,
					   m_oCurrent ->m_eOnReason   ,
					   m_oCurrent ->m_pOnContext  ,
					   m_oCurrent ->m_pOnArgument )
	      &&   m_oCurrent)
	{
				    queue (m_oCurrent,  
					   m_oCurrent ->m_hWaitFor    ,
					   m_oCurrent ->m_pOnProc     ,
					   m_oCurrent ->m_pOnContext  ,
					   m_oCurrent ->m_pOnArgument ,
					   INFINITE		      ,
					   m_oCurrent ->m_dPeriod     );
	}
	    return True ;
    }
    else
    {{
	GIrp * pIrp = m_oCoList.extract_first ();

	if    (pIrp)
	{
	    ::QueueIrpForComplete (pIrp);

	    return True ;
	}
    }}
	    return False;
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
GApartment::~GApartment ()
{};

HRESULT GApartment::XUnknown_QueryInterface (REFIID iid, void ** pi)
{
    if (NULL == pi)
    {
	return ERROR_INVALID_PARAMETER;
    }
	* pi  = NULL;

    if (iid  == IID_IUnknown)
    {
	if (0 <  m_XUnknown  .GetRefCount ())
	return  (m_XUnknown  .AddRef (), * pi = & m_XUnknown,	ERROR_SUCCESS);

	return  (m_XUnknown  .Init (this),			ERROR_SUCCESS);
    }
    else
    if (iid  == IID_IApartment)
    {
	if (0 <  m_IApartment.GetRefCount ())
	return  (m_IApartment.AddRef (), * pi = & m_IApartment, ERROR_SUCCESS);

	if (0 == m_XUnknown  .GetRefCount ())
		 m_XUnknown  .Init (this);

	return  (* pi = m_IApartment.Init (this, IID_IApartment, & m_XUnknown), 
		 m_XUnknown  .Release (),		        ERROR_SUCCESS);
    }
	return ERROR_NOT_SUPPORTED;
};

void GApartment::XUnknown_FreeInterface (void * pi)
{
    if (pi == & m_XUnknown  )
    {}
    else
    if (pi == & m_IApartment)
    {}
};

GIrp * GApartment::GetIrpForComplete ()
{
    GIrp *  pIrp = NULL;

    if	   (m_bCoCur)
    {
	if (m_pCoCur)
	{
		     pIrp = m_pCoCur;
			    m_pCoCur = NULL;
	}
	else
	{
    LockAcquire ();

	if (NULL == (pIrp = m_qCoQueue.extract_first ()))
	{
	    m_bCoCur  = 
	    m_bCoBusy = False;
	}

    LockRelease ();
	}
    }
	return pIrp;
};

GIrp * GAPI GApartment::Co_RecvCoProcIrp (GIrp * pIrp, void * pa, void *)
{
    ((GApartment *) pa) ->RecvCoProcIrp (pIrp);

    return NULL;
};

GIrp * GAPI GApartment::Co_RecvCoProcIrp2 (GIrp * pIrp, void * pa, void *)
{
    GThread * th = TCurrent ();

    th ->m_pCurApartment  = (GApartment *) pa;

    ((GApartment *) pa) ->m_bCoCur = True;

	CompleteIrp (pIrp);

		     pIrp = 
    ((GApartment *) pa) ->GetIrpForComplete ();

    if (pIrp)
    {
	SetCurCoProc	 (pIrp, Co_RecvCoProcIrp2, pa, NULL);

	TPool () ->QueueIrpForComplete (GThread::Work, pIrp);
    }

    th ->m_pCurApartment  = (GApartment *) NULL;
    
    return NULL;
};

void GApartment::SendCoProcIrp (GIrp * pIrp)
{
    if (m_pCoThread)
    {
    if (m_pCoThread ->GetId () == TCurrent () ->GetId ())
    {
	RecvCoProcIrp	 (pIrp);

	return;
    }
	SetCurCoProc	 (pIrp, Co_RecvCoProcIrp    , this, NULL);

	::QueueAPCCoProc (m_pCoThread ->GetHandle (), NULL, pIrp);
    }
    else
    {
	SetCurCoProc	 (pIrp, Co_RecvCoProcIrp2   , this, NULL);

	TPool () ->QueueIrpForComplete (GThread::Work, pIrp);
    }
};

void GApartment::IApartment_QueueIrpForComplete (GIrp * pIrp, DWord dTimeout)
{
    if (0 < dTimeout)
    {{								// ?????????????? no, Remove !!! and Update !!!
	GWakeUpPort * pWakeUpPort = 
		      TCurrent () ->GetWakeUpPort ();
    if (pWakeUpPort)
    {
	pWakeUpPort ->QueueIrpForComplete (pIrp, dTimeout);
    }
	return;
    }}

    LockAcquire ();

	pIrp = (m_bCoBusy) ? (m_qCoQueue.append (pIrp), NULL)
			   : (m_bCoBusy = True	      , pIrp);

    LockRelease ();

    if (pIrp)
    {
	SendCoProcIrp (pIrp);
    }
};

Bool GApartment::PumpInputWork ()
{
    GWakeUpPort * pWakeUpPort = TCurrent () ->GetWakeUpPort ();

    GIrp *	  pIrp	      = NULL ;
    Bool	  bResult     = False;

    if (pWakeUpPort)
    {
	bResult = pWakeUpPort ->PumpWakeUp ();
    }
    if (NULL   != (pIrp = GetIrpForComplete ()))
    {
	bResult = (CompleteIrp (pIrp), True);
    }
	return bResult;
};

void GApartment::WaitInputWork (DWord dTimeout)
{
    if (m_bInsideWait)
    {
//	return;
    }
	m_bInsideWait = True ;

    GWakeUpPort * pWakeUpPort = TCurrent () ->GetWakeUpPort ();
/*
printf ("\t\t\t\t<<GWaitIo...::Wait");
*/
    if (pWakeUpPort)
    {
	pWakeUpPort ->WaitWakeUp (NULL, dTimeout);
    }   
    else
    {
	SleepEx (dTimeout, True);
    }
/*
printf (">>\n");
*/
	m_bInsideWait = False;
};

void GApartment::Work ()
{
//printf ("<<%08X::Work>>\n", TCurrent () ->GetId ());
};

GResult GApartment::WorkLoop (LONG volatile * pExitFlag)
{
    LONG volatile *  pOrgExitFlag = NULL;

    if (pExitFlag == NULL)
    {
	pExitFlag  = (LONG volatile *) m_XUnknown.GetRefCountPtr ();
    }
    else
    {
	pOrgExitFlag = m_pExitFlag;
		       m_pExitFlag = pExitFlag;
    }

#if FALSE

    while (True)
    {
	do {} while (PumpInputWork ());

	Work ();

	if (0 >= * pExitFlag)
	{    
	    break;
	}

	WaitInputWork (INFINITE);
    };

#else

    while (0 < * pExitFlag)
    {
	if (PumpInputWork ())
	{
//	    Work ();

	    continue;
	}
	    Work ();

	WaitInputWork (INFINITE);
    }

#endif


printf ("\t  Break>> :\n\n");

	m_pExitFlag = pOrgExitFlag;

    return ERROR_SUCCESS;
};

GResult GApartment::InitInstance (int, void **, HANDLE)
{
    return ERROR_SUCCESS;
};

GResult GApartment::MainProc (int, void **, HANDLE)
{
    return ERROR_SUCCESS;
};

void GApartment::Done ()
{
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
GWinMsgApartment::~GWinMsgApartment ()
{
    if (m_hCoWnd)
    {
				    ::SetWindowLongPtr

       (m_hCoWnd, GWLP_WNDPROC, (LONG_PTR) m_pCoWndProcDef);

	m_pCoWndProcDef = NULL;

	::DestroyWindow (m_hCoWnd);

	m_hCoWnd        = NULL;
    }
};

HWND GWinMsgApartment::GetCoHWND ()
{
    if (m_hCoWnd)
    {
	return m_hCoWnd;
    }
	m_hCoWnd  = ::CreateWindowEx	(WS_EX_NOACTIVATE,
					_T("Static")	 ,
					 NULL		 ,
					 WS_POPUP	 ,
					 0		 ,
					 0		 ,
					 0		 ,
					 0		 ,
					 NULL		 ,
					 NULL		 ,
		    ::GetModuleHandle	(NULL)		 ,
					 NULL		 );
    if (m_hCoWnd)
    {
	m_pCoWndProcDef = (WNDPROC) ::SetWindowLongPtr 

       (m_hCoWnd, GWLP_WNDPROC, (LONG_PTR) GWinMsgApartment::CoWndProc);
    }
	return m_hCoWnd;
};

LRESULT WINAPI GWinMsgApartment::CoWndProc (HWND hWnd, UINT uMsg, WPARAM wParam,
								  LPARAM lParam)
{
    GWinMsgApartment * pa =
   (GWinMsgApartment *) TCurrent () ->GetCurApartment ();

    if ((uMsg == WM_NULL) && ((GWinMsgApartment *) lParam == pa))
    {
	pa ->RecvCoProcIrp ((GIrp *) wParam);

	return 0L;
    }
	return ::CallWindowProc (pa ->m_pCoWndProcDef, hWnd, uMsg, wParam, lParam);
};

void GWinMsgApartment::SendCoProcIrp (GIrp * pIrp)
{
// GASSERT (m_pCoThread != NULL)
//
    if (m_pCoThread)
    {
#if TRUE

	GApartment::SendCoProcIrp (pIrp);

#else
    if (m_pCoThread ->GetId () == TCurrent () ->GetId ())
    {
	RecvCoProcIrp (pIrp);

	return;
    }
    else if (m_hCoWnd)
    {
	::PostMessage (m_hCoWnd, WM_NULL, (WPARAM) pIrp, (LPARAM) this);
    }
    else
    {
	GApartment::SendCoProcIrp (pIrp);
    }
#endif
    }
    else
    {
	::RaiseException (0x80000000, 0, 0, NULL);  // Not entry to this point !!!
    }
};

Bool GWinMsgApartment::PreprocMessage (MSG * msg)
{
    return False;
};


GResult GWinMsgApartment::WorkLoop (LONG volatile * pExitFlag)
{
    int	  bInsideDisp = m_bInsideDisp;
			m_bInsideDisp = 0;

    GResult   dResult =	GApartment::WorkLoop (pExitFlag);

	m_bInsideDisp =	  bInsideDisp;

    return    dResult;
};

#define	TRACE_PumpInputWork	0

#if TRACE_PumpInputWork

static int cnt = 0;
static int ent = 0;

#endif

Bool GWinMsgApartment::PumpInputWork ()
{
	MSG    msg  ;

    if (m_bInsideDisp ++ == 0)
    {
#if TRACE_PumpInputWork
void * p; _asm { mov p,esp}
printf ("\t<<Peek (%08X:%1d)\n", p, ++ ent);
#endif
    if (    ::PeekMessage      (& msg, NULL, 0, 0, PM_REMOVE))
    {
#if TRACE_PumpInputWork
printf ("\t  Peek>> :\n" ,	    -- ent);
printf ("<<MSG :\t  %08X\t\t[%08X,%08X,%08X]...(%5d)\n", msg.hwnd, msg.message, msg.wParam, msg.lParam, cnt ++);
if (::GetCapture ())
printf ("Capture : %08X\n", ::GetCapture ());
#endif
	if (  PreprocMessage   (& msg))
	{
#if TRACE_PumpInputWork
printf ("  MSG by PreprocMessage .......................................(%5d)>>\n\n", cnt - 1);
#endif
	}
	else
	{
	    ::TranslateMessage (& msg);
	    ::DispatchMessage  (& msg);
#if TRACE_PumpInputWork
printf ("  MSG by DispatchMessage ......................................(%5d)>>\n\n", cnt - 1);
#endif
	}

	m_bInsideDisp --;

	GApartment::PumpInputWork ();
	return True;
    }
#if TRACE_PumpInputWork
printf ("",			    -- ent);
#endif
    }
    else
    {
///////////////////////////////////

    }

#if TRACE_PumpInputWork
printf ("\t  Idle <<\n");

	m_bInsideDisp --;

    if (GApartment::PumpInputWork ())
    {
printf ("\t  >> Idle\n");
	return True ;
    }
printf ("\t  >> Idle\n");
	return False;
#else
	m_bInsideDisp --;

	return
	GApartment::PumpInputWork ();
#endif
};

DWord GAPI GWinMsgApartment::WaitWakeUpProc (int nWait, const HANDLE * hWait, DWord dTimeout)
{
    DWord  dWaitResult = ::MsgWaitForMultipleObjectsEx 

			 (nWait, hWait, dTimeout,
			  QS_ALLINPUT
			| QS_ALLPOSTMESSAGE
			| 0			,	    // Wakeup by any event in the queue
			  MWMO_ALERTABLE
    			| MWMO_INPUTAVAILABLE		    // Alertable & No wait if has input !!!
			| 0			);

    if (nWait && (WAIT_OBJECT_0 + nWait == dWaitResult))
    {
	   dWaitResult = ::WaitForMultipleObjectsEx

			 (nWait, hWait, False, 0, True);    // Enable Alertable !!!
    }

    return dWaitResult;
};

#define	TRACE_WaitInputWork	0

void GWinMsgApartment::WaitInputWork (DWord dTimeout)
{
    if (m_bInsideWait)
    {
#if TRACE_WaitInputWork
printf ("(r)");
#endif
	return;
    }
	m_bInsideWait = True ;

    GWakeUpPort * pWakeUpPort = TCurrent () ->GetWakeUpPort ();

#if TRACE_WaitInputWork
printf ("\t\t\t\t<<GWinMsg...::Wait (%d)", m_bInsideDisp);
#endif

    if (pWakeUpPort)
    {
	pWakeUpPort ->WaitWakeUp ((0 == m_bInsideDisp) ? WaitWakeUpProc : NULL, dTimeout);
    }
    else
    {
	WaitWakeUpProc (0, NULL, dTimeout);
    }

#if TRACE_WaitInputWork
printf (">>\n");
#endif

	m_bInsideWait = False;
};
//
//[]------------------------------------------------------------------------[]

/*
//..............
//
static void GWaitIoThread::RequestForCo (GApartment *, GIrp * pIrp)
{
    if (m_pCoCur.CompareExchange (pIrp, NULL) == NULL)
    {
    ::QueueUserAPC (GetHandle (), NULL, 0, NULL, NULL);
    }
};
//
//..............
//
static void GWinMsgThread::RequestForCo (GApartment *, GIrp * pIrp)
{
    if (m_pCoCur.CompareExchange (pIrp, NULL) == NULL)
    {
    ::PostMessage (m_hCoWnd, WM_NULL, (WPARAM) 0, (LPARAM) this);
    }
};
//
//..............
//
static void GThreadPool::SetCoSignal (GApartment * pApartment, m_pCoCur)
{
    SetCurCoProc (m_pCoCur,
    ...

    m_pITPool ->QueueIrpForComplete (GThreadBase::WorkIo, m_pCoCur);
};
//
//..............



static void GThreadPool::QueueIrpForDelay (GApartment *, GIrp *, DWord dTimeout)
{
};


static void GWaitIoThread::QueueIrpForDelay (GApartment *, GIrp *, DWord dTimeout)
{
};
*/

//[]------------------------------------------------------------------------[]
//
#ifdef	THREAD_PRIORITY_DEBUG
#define	THREAD_PRIORITY_IO	    THREAD_PRIORITY_BELOW_NORMAL
#define	THREAD_PRIORITY_WORK	    THREAD_PRIORITY_LOWEST
#define	THREAD_PRIORITY_FREE	    THREAD_PRIORITY_LOWEST
#else
#define	THREAD_PRIORITY_IO	    THREAD_PRIORITY_HIGHEST
#define	THREAD_PRIORITY_WORK	    THREAD_PRIORITY_NORMAL
#define	THREAD_PRIORITY_FREE	    THREAD_PRIORITY_NORMAL
#endif

class GThreadPool
{
	friend GResult	GAPI RunApartment   (GApartment *, int nArgs, void **, HANDLE);

public :

	GThreadPool (IThreadPool *& pIThreadPool) : m_portlock  (), m_PortLock  (m_portlock ),
						    m_qwiolock  (), m_qWIoLock  (m_qwiolock ),
						    m_qciolock  (), m_qCIoLock  (m_qciolock ),
						    m_qworklock (), m_qWorkLock (m_qworklock)
	{
	// GASSERT called inside _createGWinModule with 0 cLoadCount and body has been cleared to 0.
	//
	      SYSTEM_INFO	      sysInfo  ;
	    ::GetSystemInfo	   (& sysInfo) ;

#if TRUE
	    m_tCIoMax = (int) sysInfo.dwNumberOfProcessors;
#else
	    m_tCIoMax = 2;
#endif
	    m_XUnknown   .Init	  (this);
	    pIThreadPool = 
	    m_IThreadPool.Init	  (this, IID_IThreadPool, & m_XUnknown);

	    m_XUnknown	 .Release ();
	};

       ~GThreadPool ()
	{
	// GASSERT called inside _destroyGWinModule whit 0 cLoadCount
	//
	};

	void	    DoneAllThreads  ();

protected :
#ifdef _MSC_BUG_C2248
public :
#endif

	HRESULT	    XUnknown_QueryInterface	    (REFIID, void **);

	void	    XUnknown_FreeInterface	    (void *);

	GResult	    IThreadPool_QueueWorkItem	    (GThread::Type, 
						     GWinMainProc ,
						     int, void ** );

	void	    IThreadPool_QueueIrpForComplete (GThread::Type, GIrp *);

	GResult	    IThreadPool_CreateWakeUp	    (IWakeUp    *&);

	GResult	    IThreadPool_CreateIoCoPort	    (IIoCoPort  *&, HANDLE);

	void	    IIoCoPort_QueueIoCoStatus	    (GOverlapped &);

protected :

	GResult	    RunApartment		    (GApartment *, int, void **, HANDLE);

#pragma pack (_M_PACK_VPTR)

	struct WorkEntry : public GThread::Entry
	{
	    GIrp *	    pIrp ;
	};

	typedef	TXList <WorkEntry ,
		TSLink <WorkEntry , 0> > WorkList ;

#pragma pack ()
//
//[]------------------------------------------------------------------------[]
// Members :
//
	GSpinCountCriticalSection   m_portlock ;
	GFifoThreadQueue	    m_PortLock ;	// Lock for (*)
	TInterlockedPtr <HANDLE>    m_hDone    ;

	void	    DoneThread	     (HANDLE);
//
// WaitIo Thread -----------------------------------------------------------
//
	class WIoThread : public GThread
	{
	protected :

	      WIoThread () : GThread (WaitIo) {};
	};

	void	    WIoProc	    (HANDLE);
static	DWORD GAPI  WIoProc	    (int, void **, HANDLE);

	Bool	    CreateWIoThread ();

	GIrp *	    WIoQueueIrp	    (GIrp *);
	GResult	    WIoQueueWItem   (GWinMainProc, int, void **);

	GSpinCountCriticalSection   m_qwiolock	    ;
	GFifoThreadQueue	    m_qWIoLock	    ;	// Lock for (WIo*)
	GIrp_List		    m_qWIoQueue	    ;	// (WIo*)

//	int			    m_tWIoMax	    ;	// Only one, Not Used
	TInterlocked <int>	    m_tWIoCur	    ;	// (*)

	TInterlocked <int>	    m_cWIoRequests  ;	// Queued Irp count

	TInterlockedPtr <GWakeUpPort *>
				    m_pWIoPort	    ;	// (*)
//
// WorkIo Thread -----------------------------------------------------------
//
	class CIoThread : public GThread
	{
	protected :

	      CIoThread () : GThread (WorkIo) {};
	};

	void 	    CIoProc	    (HANDLE);
static	DWORD GAPI  CIoProc	    (int, void **, HANDLE);

	Bool	    CreateCIoThread (Bool);

	GIrp *	    CIoQueueIrp	    (GIrp *, Bool);
	GResult	    CIoQueueWItem   (GWinMainProc, int, void **);

	GSpinCountCriticalSection   m_qciolock	    ;
	GFifoThreadQueue	    m_qCIoLock	    ;	//  Lock for (CIo*)
	GIrp_List		    m_qCIoQueue	    ;	// (CIo*)

	int			    m_tCIoMax	    ;
	TInterlocked <int>	    m_tCIoCur	    ;	// (*)
	TInterlocked <int>	    m_tCIoWaiting   ;

	TInterlocked <int>	    m_cCIoRequests  ;	// Queued Irp count

	HANDLE			    m_hCIoPort	    ;	// (*)
//
// Work Thread -------------------------------------------------------------
//
	class WorkThread : public GThread
	{
	protected :

	      WorkThread () : GThread (Work) {};
	};

	void 	    WorkProc	    (GIrp *	 , HANDLE);
static	DWORD GAPI  WorkProc	    (int, void **, HANDLE);

	GIrp *	    WorkQueueIrp    (GIrp *);
	GResult	    WorkQueueWItem  (GWinMainProc, int, void **);

	GSpinCountCriticalSection   m_qworklock	    ;
	GFifoThreadQueue	    m_qWorkLock	    ;	//  Lock for (Work*)
	GIrp_List		    m_qWorkQueue    ;	// (Work*)

//	int			    m_tWorkMax	    ;	// Unlimited Not Used
	TInterlocked <int>	    m_tWorkCur	    ;	// (Work*)

	TInterlocked <int>	    m_cWorkRequests ;	// Queued Irp count

	WorkList		    m_qWorkReady    ;	// (Work*)
//
//--------------------------------------------------------------------------
//
	friend
	class TXUnknown_Entry <GThreadPool>;
	      TXUnknown_Entry <GThreadPool>
				    m_XUnknown	    ;
	friend
	class IThreadPool_Entry;
	friend
	class TInterface_Entry <IThreadPool, GThreadPool>;
	class IThreadPool_Entry : public TInterface_Entry <IThreadPool, GThreadPool>
	{
	STDMETHOD_ (GResult,	  QueueWorkItem)(GThread::Type	tType	    ,
						 GWinMainProc	pWorkProc   ,
						 int		nWorkArgs   ,
						 void **	pWorkArgs   )
	{
	    return GetT () ->IThreadPool_QueueWorkItem (tType,  pWorkProc   ,
								nWorkArgs   ,
								pWorkArgs   );
	};
	STDMETHOD_ (void,   QueueIrpForComplete)(GThread::Type	tType	    ,
						 GIrp *		pIrp	    )
	{
	    GetT () ->IThreadPool_QueueIrpForComplete (tType, pIrp);
	};
	STDMETHOD_ (GResult,       CreateWakeUp)(IWakeUp *&     pIWakeUp    )
	{
	    return GetT () ->IThreadPool_CreateWakeUp (pIWakeUp);
	};
	STDMETHOD_ (GResult,     CreateIoCoPort)(IIoCoPort *&	pIIoCoPort  ,
						 HANDLE		hFileHandle )
	{
	    return GetT () ->IThreadPool_CreateIoCoPort (pIIoCoPort, hFileHandle);
	};
	}			    m_IThreadPool   ;

	friend
	class IIoCoPort_Entry;

	friend
	class TInterface_Entry <IIoCoPort, GThreadPool>;
	class IIoCoPort_Entry : public TInterface_Entry <IIoCoPort, GThreadPool>
	{
	public :
	STDMETHOD_ (void,     QueueIoCoStatus)(GOverlapped & o)
	{
	    GetT () ->IIoCoPort_QueueIoCoStatus (o);
	};
	}			    m_IIoCoPort	    ;
};
//
//[]------------------------------------------------------------------------[]
//
void GThreadPool::DoneThread (HANDLE hNext)
{
	HANDLE		  hPrev;

    if (NULL	      != (hPrev = m_hDone.Exchange (hNext)))
    {
	::WaitForAny  (1, hPrev, INFINITE);

	::CloseHandle (	  hPrev);
    }
};

void GThreadPool::DoneAllThreads ()
{
// GASSERT TCurrent () is User's external thread
//
    if (TCurrent () 
    && (TCurrent () ->GetType () != GThread::User))
    {
	return;
    }

// Wait completion all queued & running requests
//
    while (True)
    {
	if (1 < (m_XUnknown.GetRefCount	  () +

	       ((m_pWIoPort	.GetValue ()) ?
		 m_pWIoPort	.GetValue () ->
			    GetRefCount   ()  : 0) +

		 m_cWIoRequests .GetValue () +
	         m_cCIoRequests .GetValue () +
	         m_cWorkRequests.GetValue ()))
	{
	    ::Sleep (10);

	    continue;
	}
	    break   ;
    }

    WorkEntry   *   te;
    GWakeUpPort	*   wp;

    if (NULL != (wp = m_pWIoPort.Exchange (NULL)))
    {
	wp ->Break ();
    }

    if (0 < m_tCIoCur)
    {
	::PostQueuedCompletionStatus (m_hCIoPort, 0, (ULONG_PTR) this, NULL);
    }

// Wait completion all running threads
//
    while (True)
    {
	m_qWorkLock.LockAcquire ();

	    for (; NULL != (te = m_qWorkReady.extract_first ());)
	    {
		SetEvent   (te ->pThread ->GetResume ());
	    }

	m_qWorkLock.LockRelease ();

	if (0 < (m_tWIoCur .GetValue () +
		 m_tCIoCur .GetValue () +
		 m_tWorkCur.GetValue ()))
	{
	    ::Sleep (10);

	    continue;
	}
	    break   ;
    }
	DoneThread (NULL);
};
//
//[]------------------------------------------------------------------------[]
//
void GThreadPool::WIoProc (HANDLE hResume)
{
    GIrp * pIrp ;
    Bool   Loop ;

    TSharedWakeUpPort <> WIoPort (m_qWIoLock);

//  __try
    {
		WIoPort.EnterWaitLoop (); 

		m_pWIoPort.Exchange (& WIoPort);

	::SetEvent (hResume);

	while (True)
	{
	    do
	    {
		    Loop = WIoPort.PumpWakeUp ();

		m_qWIoLock.LockAcquire ();

		    pIrp = m_qWIoQueue.extract_first ();

		m_qWIoLock.LockRelease ();

		if (pIrp)
		{
		    CompleteIrp (pIrp);

		    m_cWIoRequests --;
	    
		    Loop = True;
		}

	    } while (Loop);

	    if (m_pWIoPort)
	    {
		WIoPort.WaitWakeUp  (NULL, INFINITE);

		continue;
	    }
		break;
        };
		WIoPort.LeaveWaitLoop ();
    }
//  __except (EXCEPTION_EXECUTE_HANDLER)
//  {}
};

DWORD GAPI GThreadPool::WIoProc (int, void ** pArgs, HANDLE hResume)
{
    GThreadPool * pThis = (GThreadPool *) pArgs [0];

			TCurrent () ->SetBasePriority (THREAD_PRIORITY_IO);

    pThis ->m_tWIoCur ++;

    pThis ->WIoProc    (hResume);

    pThis ->DoneThread (::DuplicateHandle (TCurrent () ->GetHandle ()));

    pThis ->m_tWIoCur --;

    return ERROR_SUCCESS;
};

Bool GThreadPool::CreateWIoThread ()
{
	if (m_pWIoPort.GetValue ())
	{
	    return True;
	}

    m_PortLock.LockAcquire ();

	if (m_pWIoPort.GetValue ())
	{}
	else
	{{
	    void * pArgs [1] = {this};

	    ::CreateThread <WIoThread> (NULL, WIoProc, 1, pArgs, (256 * 1024));
	}}

    m_PortLock.LockRelease ();

    return (m_pWIoPort.GetValue ()) ? True : False;
};

GIrp * GThreadPool::WIoQueueIrp (GIrp * pIrp)
{
    if (CreateWIoThread ())
    {
	    m_cWIoRequests ++;

	m_qWIoLock.LockAcquire ();

	    if (m_qWIoQueue.last () || (GThread::WaitIo != TCurrent () ->GetType ()))
	    {
		pIrp = (m_qWIoQueue.append (pIrp), NULL);
	    }

	m_qWIoLock.LockRelease ();

	if (pIrp)
	{
	    CompleteIrp (pIrp);

	    m_cWIoRequests --;
	}
	else
	{
	    m_pWIoPort.GetValue () ->Break ();
	}

	return NULL;
    }
	return pIrp;
};

GResult GThreadPool::WIoQueueWItem (GWinMainProc pMainProc,
				    int		 nMainArgs,
				    void **	 pMainArgs)
{
    return ERROR_NOT_SUPPORTED;
};
//
//[]------------------------------------------------------------------------[]
//
void GThreadPool::CIoProc (HANDLE hResume)
{
    CIoThread *	    tCurrent =
   (CIoThread *)    TCurrent	 ();

    DWord	    dTransfered    ;
    const void  *   pCompletionKey ;
    union
    {
    GOverlapped *   pOverlapped    ;
    GIrp *	    pIrp	   ;
    };
    DWord	    dResult	   ;

//  __try
    {
			    m_tCIoWaiting ++;

	::SetEvent (hResume);

	while (True)
	{
	    dTransfered	    =    0 ;
	    pCompletionKey  =    0 ;
	    pOverlapped	    = NULL ;

	    dResult	    = ::GetQueuedCompletionStatus ( m_hCIoPort	   ,
							  & dTransfered	   ,
					  (PULONG_PTR	) & pCompletionKey ,
					  (OVERLAPPED **) & pOverlapped	   ,
							    INFINITE	   )
					 ? ERROR_SUCCESS  : ::GetLastError();

	    if	   (pOverlapped	   != NULL)
	    {
		if (pCompletionKey != this) // This is external IoCompletion packet
		{			    //
		    pOverlapped ->CallIoCoProc (dResult, (void *) dTransfered);
		}
		else			    // This is self pool's Completion packet
	{				    //
					    // Assume dTransfered as some CtrlCode
					    //
		if      (0 == dTransfered)
		{			    // pOverlapped has been queued with GThread::WorkIo
			{		    //
			    CompleteIrp (pIrp);

			    m_cCIoRequests --;
			}
		}
		else if (1 == dTransfered)
		{			    // pOverlapped has been queued with GThread::Work
					    //
		    if  (0 < -- m_tCIoWaiting)
		    {
			tCurrent ->SetBasePriority (THREAD_PRIORITY_WORK);

			while (pIrp)
			{
			    CompleteIrp (pIrp);

			    m_cCIoRequests --;

			m_qCIoLock.LockAcquire ();

			    pIrp = m_qCIoQueue.extract_first ();

			m_qCIoLock.LockRelease ();

			};

			tCurrent ->SetBasePriority (THREAD_PRIORITY_IO);
		    }
		    else
		    {
			m_qCIoLock.LockAcquire ();

				   m_qCIoQueue.insert (pIrp);

			m_qCIoLock.LockRelease ();
		    }
			    m_tCIoWaiting ++;
		}
	}
	    }
	    else			    // Break packet
	    {				    //
			    m_tCIoWaiting --;
		break;
	    }
	};
    }
//  __except (EXCEPTION_EXECUTE_HANDLER)
//  {}
};

DWORD GAPI GThreadPool::CIoProc (int, void ** pArgs, HANDLE hResume)
{
    GThreadPool * pThis = (GThreadPool *) pArgs [0];

			TCurrent () ->SetBasePriority (THREAD_PRIORITY_IO);

	       pThis ->m_tCIoCur ++;

    pThis ->CIoProc    (hResume);

    pThis ->DoneThread (::DuplicateHandle (TCurrent () ->GetHandle ()));

    if (0 < -- pThis ->m_tCIoCur)
    {					    // Post break for next thread
    ::PostQueuedCompletionStatus 

   (pThis ->m_hCIoPort, 0, (ULONG_PTR) pThis, NULL);
    }

	return ERROR_SUCCESS;
};

Bool GThreadPool::CreateCIoThread (Bool bQueueIoFlag)
{
    if (bQueueIoFlag)
    {
	if ((m_tCIoCur.GetValue () <  m_tCIoMax		        )
	&&  (m_tCIoCur.GetValue () <  m_cCIoRequests.GetValue ()))
	{}
	else
	{   return True;}
    }
    else
    {
	if (0 < m_tCIoCur.GetValue ())
	{   return True;}
    }

    m_PortLock.LockAcquire ();

	if (m_tCIoCur.GetValue () < m_tCIoMax)
	{
	    if (m_hCIoPort == NULL)
	    {
		m_hCIoPort = ::CreateIoCompletionPort (INVALID_HANDLE_VALUE ,
						       NULL, NULL, m_tCIoMax);
	    }
	    if (m_hCIoPort)
	    {{
		void * pArgs [1] = {this};

		::CreateThread <CIoThread> (NULL, CIoProc, 1, pArgs, (256 * 1024));
	    }}
	}

    m_PortLock.LockRelease ();

    return m_tCIoCur.GetValue () ? True : False;
};

GIrp * GThreadPool::CIoQueueIrp (GIrp * pIrp, Bool bWork)
{
    if (CreateCIoThread (False))
    {
	m_cCIoRequests ++;

	CreateCIoThread (True );

    if (::PostQueuedCompletionStatus (m_hCIoPort,  (bWork) ? 1 : 0,
				     (ULONG_PTR   ) this ,
				     (OVERLAPPED *) pIrp ))
    {
	return NULL;
    }
	m_cCIoRequests --;
    }
	return pIrp;
};

GResult GThreadPool::CIoQueueWItem (GWinMainProc pMainProc,
				    int		 nMainArgs,
				    void **	 pMainArgs)
{
    return ERROR_NOT_SUPPORTED;
};
//
//[]------------------------------------------------------------------------[]
//
void GThreadPool::WorkProc (GIrp * pIrp, HANDLE hResume)
{
    WorkEntry		  te;

    te.pThread = TCurrent ();
    te.pIrp    = pIrp	    ;

//  __try
    {
	    ::SetEvent  (hResume);

	while (te.pIrp)
	{
	    CompleteIrp (te.pIrp);

			 te.pIrp = NULL;

	    m_cWorkRequests --;

	    m_qWorkLock.LockAcquire  ();

		m_qWorkReady.insert  (& te);

	    m_qWorkLock.LockRelease  ();

		WaitForAny (1, te.pThread ->GetResume (), 30000);

	    m_qWorkLock.LockAcquire  ();

		m_qWorkReady.extract (& te);

	    m_qWorkLock.LockRelease  ();

		WaitForAny (1, te.pThread ->GetResume (),     0);
	};
    }
//  __except (EXCEPTION_EXECUTE_HANDLER)
//  {}
};

DWORD GAPI GThreadPool::WorkProc (int, void ** pArgs, HANDLE hResume)
{
    GThreadPool * pThis	= (GThreadPool *) pArgs [0];

			TCurrent () ->SetBasePriority (THREAD_PRIORITY_FREE);

    pThis ->m_tWorkCur ++;

    pThis ->WorkProc   ((GIrp *) pArgs [1], hResume);

    pThis ->DoneThread (::DuplicateHandle (TCurrent () ->GetHandle ()));
    
    pThis ->m_tWorkCur --;

    return ERROR_SUCCESS;
};

GIrp * GThreadPool::WorkQueueIrp (GIrp * pIrp)
{
    WorkEntry * te;

	    m_cWorkRequests ++;

    m_qWorkLock.LockAcquire ();

	    te = m_qWorkReady.extract_first ();

	if (te)
	{
			te ->pIrp = pIrp;

	    ::SetEvent (te ->pThread ->GetResume ());

    m_qWorkLock.LockRelease ();

	    pIrp = NULL;
	}
	else
	{
    m_qWorkLock.LockRelease ();
	
    m_PortLock.LockAcquire  ();
    {{	
	void * pArgs [2]   = {this, pIrp};

	if (ERROR_SUCCESS != ::CreateThread <WorkThread> (NULL, WorkProc, 2, pArgs, (256 * 1024)))
	{
	    m_cWorkRequests --;
	}
	else
	{
	    pIrp = NULL;
	}
    }}
    m_PortLock.LockRelease  ();
	}

    return  pIrp;
};

GResult GThreadPool::WorkQueueWItem (GWinMainProc pMainProc,
				     int	  nMainArgs,
				     void **	  pMainArgs)
{
    return ERROR_NOT_SUPPORTED;
};
//
//[]------------------------------------------------------------------------[]
//
HRESULT GThreadPool::XUnknown_QueryInterface (REFIID iid, void ** pi)
{
    if (iid  == IID_IThreadPool)
    {
	// Unsafe, this method call only once 

	return (* pi = m_IThreadPool.Init (this, IID_IThreadPool, & m_XUnknown), ERROR_SUCCESS);
    }
	return ERROR_NOT_SUPPORTED;
};

void GThreadPool::XUnknown_FreeInterface (void * pi)
{
/*
    if	    (pi == & m_IThreadPool)
    {}    
    else if (pi == & m_IIoCoPort  )
    {}
    else if (pi == & m_XUnknown	  )
    {}
*/
};



GResult GThreadPool::IThreadPool_QueueWorkItem (GThread::Type tType	,
						GWinMainProc  pWorkProc ,
						int	      nWorkArgs ,
						void **	      pWorkArgs )
{
    switch (tType)
    {
    case GThread::WaitIo :
	return WIoQueueWItem  (pWorkProc, nWorkArgs, pWorkArgs);

    case GThread::WorkIo :
	return CIoQueueWItem  (pWorkProc, nWorkArgs, pWorkArgs);

//  case GThread::Work	 :
//	return CIoQueueWItem  (pWorkProc, nWorkArgs, pWorkArgs, True );

    default :
//  case GThread::Work	 :
//  case GThread::User	 :
	return WorkQueueWItem (pWorkProc, nWorkArgs, pWorkArgs);

    }
	return ERROR_SUCCESS;
};

void GThreadPool::IThreadPool_QueueIrpForComplete (GThread::Type tType ,
						   GIrp *	 pIrp  )
{
    switch (tType)
    {
    case GThread::WaitIo :
	pIrp = WIoQueueIrp  (pIrp);
	break;

    case GThread::WorkIo :
	pIrp = CIoQueueIrp  (pIrp, False);
	break;

    case GThread::Work :
	pIrp = CIoQueueIrp  (pIrp, True );
	break;
	
    default :
//  case GThread::Work :
//  case GThread::User :
	pIrp = WorkQueueIrp (pIrp);
	break;
    };
};

GResult GThreadPool::IThreadPool_CreateWakeUp (IWakeUp *& pIWakeUp)
{
    if (CreateWIoThread ())
    {
	return m_pWIoPort.GetValue () ->CreateWakeUp (pIWakeUp);
    }
	return ERROR_NO_SYSTEM_RESOURCES;
};

GResult GThreadPool::IThreadPool_CreateIoCoPort (IIoCoPort *& pIIoCoPort, HANDLE hFileHandle)
{
	GResult Result = ERROR_NO_SYSTEM_RESOURCES;

	pIIoCoPort     = NULL;

    if (CreateCIoThread (False))
    {
	if (ERROR_SUCCESS == (Result = (::CreateIoCompletionPort
				       (hFileHandle, m_hCIoPort , (ULONG_PTR) NULL, 0))

				      ? ERROR_SUCCESS : ::GetLastError ()))
	{
	   (pIIoCoPort = & m_IIoCoPort) ->AddRef ();
	}
    }
	return Result ;
};

void GThreadPool::IIoCoPort_QueueIoCoStatus (GOverlapped & o)
{
    if (::PostQueuedCompletionStatus (m_hCIoPort, 0, (ULONG_PTR) NULL, & o))
    {}
    else
    {
	::RaiseException (0x80000000, 0, 0, NULL);
    }
};

/*
static GIrp * CoDoneApartment (GIrp * pIrp, void * pa, void *)
{
    if (0 < ((GApartment *) pa) ->GetRefCount ())
    {
	
    }

    ((GApartment *) pa) ->Done ();
};
*/

/*
GResult GThreadPool::RunApartment (GApartment * pa, int nArgs, void ** pArgs, HANDLE hResume)
{
    GResult	dResult;

    dResult =  pa ->Main (nArgs, pArgs, hResume);

    return dResult;
};
*/




GResult GAPI RunApartment (GApartment * pa, int nArgs, void ** pArgs, HANDLE hResume)
{
    GResult	   dResult;
    GThread * th = TCurrent ();

    if (th && pa)
    {
	if (GThread::User == th ->GetType ())
	{
	    dResult	   = th ->RunApartment 

			    (pa, nArgs, pArgs, hResume);
	}
	else
	{
//	    return Run
	}
    }
    else
    {
	    dResult = ERROR_INVALID_PARAMETER;
    }

    return  dResult;
};

GResult GAPI QueryIApartment (IApartment *&)
{
//    if (

    return ERROR_SUCCESS;
};
//
//[]------------------------------------------------------------------------[]








































































































//[]------------------------------------------------------------------------[]
//
void GAPI QueueIrpForComplete (GIrp * pIrp, DWord dTimeout)
{
    GThread * th = TCurrent ();

    if (th && th ->GetCurApartment ())
    {
	      th ->GetCurApartment () ->IApartment_QueueIrpForComplete (pIrp, dTimeout);
    }
    else
    {
	TPool () ->QueueIrpForComplete (GThread::Work, pIrp/*, dTimeout*/);
    }

/*
    IApartment * pIApartment = TCurrent () ->QueryIApartment ();

    GApartment * pApartment = TCurrent () ->GetCurApartment ();

    if (pApartment)
    {
	pApartment ->QueueIrpForComplete (pIrp);
    }

#if FALSE

	GThread::Type	 tType = TCurrent () ? TCurrent () ->GetType () 
					     : GThread::User ;

    if (bForceQueue || (GThread::Work != tType))
    {
	TPool () ->QueueIrpForComplete ((CoType) GThread::Work, pIrp);
    }
    else
    {
	CompleteIrp (pIrp);
    }
#endif
*/
};

/*
void GIrp_DispatchQueue::DispatchPacket (GIrp * pIrp, Bool bForceQueue)
{
	GThread::Type	 tType = TCurrent () ? TCurrent () ->GetType () 
					     : GThread::User ;

    if (bForceQueue || (GThread::Work != tType))
    {
	TPool () ->QueueIrpForComplete (pIrp, (CoType) GThread::Work);
    }
	CompleteIrp (pIrp);
};
*/
//
//[]------------------------------------------------------------------------[]














//[]------------------------------------------------------------------------[]
/*
GResult	GAPI CreateEventWakeUp (IWakeUp *& pIWakeUp,
				Bool bNotification ,
				Bool bInitState	   )
{
    GResult	 dResult;
    HANDLE	 hEvent ;

    if (NULL ==		 (hEvent = ::CreateEvent (NULL, bNotification, bInitState, NULL)))
    {
	return ::GetLastError ();
    }
    if (ERROR_SUCCESS != 
       (dResult	       = (TCurrent () ->GetCurApartment () ->CreateWakeUp (pIWakeUp))))
    {
	::CloseHandle	 (hEvent);

	return dResult;
    }
	return ERROR_SUCCESS;
};

void GAPI DestroyEventWakeUp (IWakeUp * pIWakeUp)
{
    if (pIWakeUp)
    {
    if (pIWakeUp ->GetHandle ())
    {
	::CloseHandle

       (pIWakeUp ->GetHandle ());
    }
	pIWakeUp ->Release   ();
    }
};
*/
//[]------------------------------------------------------------------------[]
















/*

//[]------------------------------------------------------------------------[]
//
class GIoWorkThread : public GThread
{
public :

	DWORD		WaitPerform	(int, const HANDLE *, DWORD dTimeout);
};

class GWinMsgThread : public GIoWorkThread
{
public :

	DWORD		WaitPerform	(int, const HANDLE *, DWORD dTimeout);
};
//
//[]------------------------------------------------------------------------[]

*/














//[]------------------------------------------------------------------------[]
// GWinModule declaration for any (.Exe or .Dll) instance
//
//
#pragma pack (_M_PACK_VPTR)

    struct GWinModuleFile
    {
	TInterlocked <int>	    cLoadCount  ;
	DWORD			    dTlsIndex   ;

	HANDLE			    hFile	;

	GThreadPool		    ThreadPool	;
	IThreadPool *		  pIThreadPool  ; 

	void *  operator new    (size_t, void * ptr) { return ptr;};
	void	operator delete (void *, void *) {};
	void    operator delete (void *	       ) {};

	GWinModuleFile (HANDLE h) : cLoadCount (1),
				    dTlsIndex  (::TlsAlloc ()),
				    hFile      (h),
				    ThreadPool (pIThreadPool )
	{};

       ~GWinModuleFile ()
	{
	    if (-1 != (int) dTlsIndex)
	    {
		::TlsFree  (dTlsIndex);
	    }
	}
    };

#pragma pack ()
//
//[]------------------------------------------------------------------------[]
//
    DECLSPEC_ALIGN (_M_CACH_SIZE)

    GWinModuleArea  g_GWinModuleArea = {NULL};

static void _destroyGWinModule (GWinModuleFile * pFile)
{
    if (-- pFile ->cLoadCount > 0)
    {
	return;
    }

    HANDLE  hFile    = pFile ->hFile	   ;
		       pFile ->hFile = NULL;

	       delete  pFile;

    ::UnmapViewOfFile (pFile);

    ::CloseHandle     (hFile);
};

static Bool _createGWinModule (WCHAR * pFileName, HMODULE hExeDll)
{
    HANDLE	     hFile = NULL ;
    GWinModuleFile * pFile = NULL ;
    DWord	     dSize = (DWord) roundup (sizeof (GWinModuleFile), _M_PAGE_SIZE);

	if (NULL != (hFile = ::CreateFileMappingW 
		    (INVALID_HANDLE_VALUE	    ,
		     NULL			    ,
		     PAGE_READWRITE | SEC_COMMIT    ,
		     0, dSize, pFileName	    )))
    {{

	    Bool     bShutBeInit = (ERROR_ALREADY_EXISTS == ::GetLastError ()) ? False : True;

	if (NULL != (pFile = (GWinModuleFile *) 
			     ::MapViewOfFile
		    (hFile			    ,
		     FILE_MAP_WRITE | FILE_MAP_READ ,
		     0, 0, 0			    )))
	{
	    if	    (bShutBeInit)
	    {
    		memset (pFile, 0, dSize);
		new    (pFile) GWinModuleFile (hFile);

		if ((LONG) pFile ->dTlsIndex == -1)
		{
		    _destroyGWinModule (pFile);

		    return False;
		}
	    }
	    else
	    {
		pFile ->cLoadCount ++ ;

		::CloseHandle  (hFile);
	    }

	    g_GWinModule.dTlsIndex    =  pFile ->dTlsIndex    ;
	    g_GWinModule.pParent      =  pFile		      ;
	    g_GWinModule.hExeDll      = (hExeDll) 
				       ? hExeDll : ::GetModuleHandle (NULL);
	   (g_GWinModule.pIThreadPool =  pFile ->pIThreadPool) ->AddRef ( );

	    return True ;
	}
    }}

	if (pFile)
	{
	    ::UnmapViewOfFile (pFile);
	}
	if (hFile)
	{
	    ::CloseHandle     (hFile);
	}
	    return False;
};

static void _buildProcessName (WCHAR * pBuf, const WCHAR * pPostFix)
{
    ::wsprintfW (pBuf, L"%s_%08X", pPostFix, ::GetCurrentProcessId ());
};

void GAPI GWinDone ()
{
	if (-- g_GWinModule.cInitCount > 0)
	{
	    return;
	}

    HANDLE  hGWin     ;
    WCHAR   pStrW [32];

	    _buildProcessName  (pStrW, L"GWinL");

    if (NULL != (hGWin = ::CreateMutexW (NULL, True, pStrW)))
    {
	    GWinModuleFile *  pFile =
	   (GWinModuleFile *)

	    g_GWinModule.pParent.Exchange (NULL);

	if (g_GWinModule.cInitCount.GetValue   () == 0)
	{
	    if (g_GWinModule.hExeDll == ::GetModuleHandle (NULL))
	    {
	    pFile ->ThreadPool.DoneAllThreads  ();
	    }
	    g_GWinModule.dTlsIndex    = 0   ;
	    g_GWinModule.hExeDll      = NULL;
	    g_GWinModule.pIThreadPool ->Release();
	    g_GWinModule.pIThreadPool = NULL;

	    _destroyGWinModule		  (pFile);
	}
	else
	{
	    g_GWinModule.pParent.Exchange (pFile);
	}
	    ::ReleaseMutex (hGWin);

	    ::CloseHandle  (hGWin);
    }
};

Bool GAPI GWinInit (HMODULE hModule)
{
	    g_GWinModule.cInitCount ++;

	if (g_GWinModule.pParent.GetValue () != NULL)
	{
	    return True ;
	}

    HANDLE  hGWin     ;
    WCHAR   pStrW [32];

	    _buildProcessName (pStrW, L"GWinL");

    if (NULL != (hGWin = ::CreateMutexW (NULL, True, pStrW)))
    {
	if (g_GWinModule.pParent.GetValue () == NULL)
	{
	    _buildProcessName (pStrW, L"GWinF");

	    _createGWinModule (pStrW, hModule );
	}

        ::ReleaseMutex  (hGWin);

        ::CloseHandle	(hGWin);

	if (g_GWinModule.pParent.GetValue () != NULL)
	{
	    return True ;
	}
    }
	    g_GWinModule.cInitCount --;

	    return False;
};
//
//[]------------------------------------------------------------------------[]





























#if FALSE

//[]------------------------------------------------------------------------[]
//
Bool GAPI GWaitIoThread::dispIdle ()
{
    GWaitIoThread * th = (GWaitIoThread *) TCurrent ();

    if (th ->m_WakeUpPort.PeekWakeUp (NULL))
    {
	th ->m_WakeUpPort.ExecWakeUp ();

	return True;
    }

	GIrp *  pIrp =  th ->GetIrpForComplete ();

    if (NULL == pIrp)
    {
		pIrp = (th ->GetCurApartment ()) 

		      ? th ->GetCurApartment () ->GetIrpForComplete () 
		      : NULL;
    }
    if (NULL != pIrp)
    {
	CompleteIrp (pIrp);

	return True ;
    }
	return False;
};

void GAPI GWaitIoThread::waitIdle (DWord dTimeout)
{
printf ("\t\t\t\t<<GWaitIo...::Wait");

    ((GWaitIoThread *) TCurrent ()) ->m_WakeUpPort.WaitWakeUp (NULL, dTimeout);

printf (">>\n");
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
DWord GAPI GWinMsgThread::waitFor (int nWait, const HANDLE * hWait, DWord dTimeout)
{
    DWord  dWaitResult = ::MsgWaitForMultipleObjectsEx 

			 (nWait, hWait, dTimeout,
			  QS_ALLINPUT
			| QS_ALLPOSTMESSAGE
			| 0			,	    // Wakeup by any event in the queue
			  MWMO_ALERTABLE
    			| MWMO_INPUTAVAILABLE		    // Alertable & No wait if has input !!!
			| 0			);

    if (nWait && (WAIT_OBJECT_0 + nWait == dWaitResult))
    {
	   dWaitResult = ::WaitForMultipleObjectsEx

			 (nWait, hWait, False, 0, True);    // Enable Alertable !!!
    }

    return dWaitResult;
};

Bool GAPI GWinMsgThread::dispIdle ()
{
    GWinMsgThread * th = (GWinMsgThread *) TCurrent ();

    if (th ->m_WakeUpPort.PeekWakeUp (waitFor))
    {
	th ->m_WakeUpPort.ExecWakeUp ();
    }

	GIrp *  pIrp =  th ->GetIrpForComplete ();

    if (NULL == pIrp)
    {
		pIrp = (th ->GetCurApartment ()) 

		      ? th ->GetCurApartment () ->GetIrpForComplete () 
		      : NULL;
    }
    if (NULL != pIrp)
    {
	CompleteIrp (pIrp);

	return True ;
    }
	return False;
};

void GAPI GWinMsgThread::waitIdle (DWord dTimeout)
{
printf ("\t\t\t\t<<GWinMsg...::Wait");

    ((GWinMsgThread *) TCurrent) ->m_WakeUpPort.WaitWakeUp (waitFor, dTimeout);

printf (">>\n");
};
//
//[]------------------------------------------------------------------------[]
//
GWinMsgThread::~GWinMsgThread ()
{
    if (m_hCoWnd)
    {
					::SetWindowLongPtr

       (m_hCoWnd, GWLP_WNDPROC, (LONG_PTR) m_pCoWndProcDef);

	m_pCoWndProcDef = NULL;

	::DestroyWindow (m_hCoWnd);

	m_hCoWnd        = NULL;
    }
};

HWND GWinMsgThread::GetCoHWND ()
{
    if (m_hCoWnd)
    {
	return m_hCoWnd;
    }
	m_hCoWnd  = ::CreateWindowEx	(WS_EX_NOACTIVATE,
					_T("Static")	 ,
					 NULL		 ,
					 WS_POPUP	 ,
					 0		 ,
					 0		 ,
					 0		 ,
					 0		 ,
					 NULL		 ,
					 NULL		 ,
		    ::GetModuleHandle	(NULL)		 ,
					 NULL		 );
    if (m_hCoWnd)
    {
	m_pCoWndProcDef = (WNDPROC) ::SetWindowLongPtr 

       (m_hCoWnd, GWLP_WNDPROC, (LONG_PTR) GWinMsgThread::_CoWndProc);

    }
	return m_hCoWnd;
};

LRESULT GWinMsgThread::CoWndProcDef (HWND hWnd, UINT uMsg, WPARAM wParam,
							   LPARAM lParam)
{
    return ::CallWindowProc (m_pCoWndProcDef, hWnd, uMsg, wParam, lParam);
};

LRESULT GWinMsgThread::CoWndProc (HWND hWnd, UINT uMsg, WPARAM wParam,
							LPARAM lParam)
{
/*
    if ((uMsg == WM_NULL) && (lParam == (LPARAM) this))
    {
	if (RunIdle ())

	while (RunIdle ())
	{

	}

	GIrp * pIrp

	while (NULL !=  (pIrp = GetIrpForComplete ())
	{
	    CompleteIrp (pIrp);

	    WaitWakeUp ();
	}
    }}
*/








#if FALSE


    if ((uMsg == WM_NULL) && (lParam == (LPARAM) this))
    {
	while  (m_pCoCur)
	{
	    GWaitIoApartment::CompletePacket ();

	    if (1 == WaitProc (0, NULL, 0))
	    {
	    if (m_pCoCur)
	    {
		::PostMessage (hWnd, WM_NULL, (WPARAM) 0, (LPARAM) this);
	    }
		break;
	    }
	}

	return 0L;
    }
	return CoWndProcDef (hWnd, uMsg, wParam, lParam);
#else

    if ((uMsg == WM_NULL) && (lParam == (LPARAM) this))
    {
    }

#endif
	return 0L;
};

LRESULT WINAPI GWinMsgThread::_CoWndProc (HWND hWnd, UINT uMsg, WPARAM wParam,
								LPARAM lParam)
{
    return ((GWinMsgThread *) TCurrent ()) ->

	    CoWndProc (hWnd, uMsg, wParam, lParam);
};

#if FALSE

Bool GWindow::On_WM_EnterIdle (GWinMsg &)
{
    GWinMsgThread::WaitRequest ();

    return m.DispatchComplete  ();
};


#endif
//
//[]------------------------------------------------------------------------[]

#endif


/*
static Bool GAPI Xxxx_Co_QueueForDelay (IWakeUp * pIWakeUp, IWakeUp::Reason reason, void * pContext, void * pIrp)
{
    if (IWakeUp::OnTimeout == reason)
    {
	pIWakeUp ->Destroy ();

	((GApartment *) pContext) ->QueueIrpForComplete ((GIrp *) pIrp);

	((GApartment *) pContext) ->Release ();
    }
	return False;
};

void GAPI QueueForDelay (GIrp * pIrp, DWord dTimeout)
{
    IWakeUp *	    pIWakeUp;

    if (ERROR_SUCCESS == TCurrent () ->GetCurApartment () ->
			 CreateWakeUp   (pIWakeUp, NULL))
    {
				 TCurrent () ->GetCurApartment () ->AddRef ();
	pIWakeUp ->QueueForWait (Xxxx_Co_QueueForDelay, 
				 TCurrent () ->GetCurApartment (), pIrp, dTimeout);
    }
    else
    {
	::Sleep (0);

	CompleteIrp (pIrp);
    }
};
*/

#if FALSE

void GApartment::EmitCoSignal ()
{
    ::QueueUserAPC (m_pCoThread ->GetHandle (), NULL, 0, NULL, NULL);
};

void GWinMsgApartment::EmitCoSignal ()
{
    ::PostMessage  (m_hCoWnd, WM_NULL, (WPARAM) 0, (LPARAM) this);
};




GResult GWaitIoApartment::Run (Bool (GAPI * pConditionProc)(GResult * pResult), void * pConditionContext)
{
    GIrp *     pIrp    = NULL;
    GResult    dResult = ERROR_SUCCESS;

    while ((pCondProc)(pCondContext, & dResult))
    {
	if (m_WakeUpPort.PeekWakeUp (NULL))
	{
	    m_WakeUpPort.ExecWakeUp ();

	    continue;
	}

	if (DispatchCoQueue ())
	{
	    continue;
	}
	    WaitWakeUp ();
    }
}

#endif


#if FALSE

//
//[]------------------------------------------------------------------------[]
//
static Bool   GAPI	  Xxxx_Co_Timeout (IWakeUp *, IWakeUp::Reason reason, void * pContext , 
									      void * pArgument)
{
    SetCancelProc (   (GIrp	  *) pArgument,
		   & ((GApartment *) pContext ) ->m_qCoLock, NULL, NULL);

		     ((GApartment *) pContext ) ->QueueIrpForComplete ((GIrp *) pArgument);
    return False;
};

static void   GAPI Cancel_Xxxx_Co_Timeout (GIrp * pIrp, GAccessLock * pLock,  void * pArgument)
{
    ((IWakeUp	 *) pArgument) ->Cancel  ();

    pLock ->LockRelease ();
};

static GIrp * GAPI Co_Timeout (GIrp * pIrp, void * pContext ,
					    void * pArgument)
{
    ((IWakeUp    *) pArgument) ->Release ();

    ((GApartment *) pContext ) ->Release ();

    return pIrp;
};

void GApartment::QueueIrpForComplete (GIrp * pIrp, DWord dTimeout)
{
    if (0 < dTimeout)
    {{
	GetWakeUpPort () ->QueueIrpForComplete (pIrp, dTimeout, True);

	return;
/*
	    IWakeUp *      pIWakeUp;

    if (ERROR_SUCCESS == CreateWakeUp (pIWakeUp))
    {
    m_qCoLock.LockAcquire ();

	if (SetCancelProc (pIrp, & m_qCoLock, Cancel_Xxxx_Co_Timeout, pIWakeUp))
	{
						  this ->AddRef ();

	    SetCurCoProc  (pIrp, Co_Timeout     , this, pIWakeUp );

	    pIWakeUp ->QueueWaitFor
			  (NULL, Xxxx_Co_Timeout, this, pIrp, dTimeout);

    m_qCoLock.LockRelease ();

	    return;
	}
    m_qCoLock.LockRelease ();

	    pIWakeUp ->Release ();
    }
*/
    }}
	QueueIrpForComplete (pIrp);
};















void GApartment::CompletePacket ()
{
    if (TCurrent () ->GetId () != m_pCoThread ->GetId ())
    {
    if (m_qCoEvent.CompareExchange (1, 0) == 0)
    {
	::QueueUserAPC (m_pCoThread ->GetHandle (), NULL, 0, NULL, NULL);
    }
	return;
    }

    if (m_pCoCur)
    {
    ::CompleteIrp (m_pCoCur);

    if (0 ==    -- m_cCoCur)
    {
    LockAcquire ();

	m_qCoEvent.Exchange	   (

	(NULL !=  (m_pCoCur = m_qCoQueue.extract_first ())) ? 1 : 0);

	if	  (m_pCoCur)
	{
		   m_cCoCur ++;
	}

    LockRelease ();
    }
    }
};

GResult GApartment::Run ()
{
    m_pCoThread = TCurrent ();

    while (True)
    {
	if (m_pCoCur)
	{
	    CompletePacket ();

	    continue;
	}
	    ::SleepEx (INFINITE, True);
    }

    return ERROR_SUCCESS;
};

#if FALSE

Bool GThread::PumpInputWork ()
{
    if (m_pCurApartment)
    {
    }
};


void GThread::WaitInputWork (DWord dTimeout)
{
};

void GWinMsgThread::WaitInputWork (DWord dTimeout)
{
};


#endif
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//

GWakeUpPort * GWaitIoApartment::GetWakeUpPort ()
{
    return & m_WakeUpPort;
};

GResult GWaitIoApartment::CreateWakeUp (IWakeUp *& pIWakeUp)
{
    return m_WakeUpPort.CreateWakeUp   (pIWakeUp  );
};

GResult GWaitIoApartment::CreateIoCoWait (IIoCoWait *& pIIoCoWait)
{
    return m_WakeUpPort.CreateIoCoWait (pIIoCoWait);
};

GResult GWaitIoApartment::Run ()
{
//???  m_WakeUpPort.InitWait ();

    m_pCoThread = TCurrent ();

    while (True)
    {
	if (m_WakeUpPort.PeekWakeUp (NULL))
	{
	    m_WakeUpPort.ExecWakeUp ();

	    continue;
	}

	if (m_pCoCur)
	{
	    CompletePacket  ();

	    continue;
	}

	if (0 < GetRefCount ())
	{
printf ("\t\t\t\t<<Wait");

	    m_WakeUpPort.WaitWakeUp (NULL, INFINITE);

printf (">>\n");

	    continue;
	}
	    break;
    };

//???  m_WakeUpPort.DoneWait ();

    return ERROR_SUCCESS;
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//


void GWinMsgApartment::CompletePacket ()
{
    if (NULL == m_hCoWnd)
    {
	GWaitIoApartment::CompletePacket ();

	return;
    };

    if (TCurrent () ->GetId () != m_pCoThread ->GetId ())
    {
    if (m_qCoEvent.CompareExchange (0, 1) == 0)
    {
	::PostMessage (m_hCoWnd, WM_NULL, (WPARAM) 0, (LPARAM) this);
    }
	return;
    }
	m_qCoEvent.Exchange	   (   1);

	::SendMessage (m_hCoWnd, WM_NULL, (WPARAM) 0, (LPARAM) this);
};











//
//[]------------------------------------------------------------------------[]
//
GIrp * GSharedApartment::Co_CompletePacket (GIrp * pIrp, void *)
{
    TCurrent () ->SetCurApartment (this);

    while (m_pCoCur)
    {
    ::CompleteIrp (m_pCoCur);

    if (0 ==    -- m_cCoCur)
    {
    LockAcquire ();

		   m_pCoCur = m_qCoQueue.extract_first ();

	if 	  (m_pCoCur)
	{
		   m_cCoCur ++;
	}

    LockRelease ();

	if	  (m_pCoCur)
	{
	    CompletePacket ();
	}
	    break;
    }
    };

    TCurrent () ->SetCurApartment (NULL);

    return NULL;
};

void GSharedApartment::CompletePacket ()
{
    SetCurCoProc <GSharedApartment, void *>
		 (m_pCoCur, & GSharedApartment::Co_CompletePacket, this, NULL);

    m_pITPool ->QueueIrpForComplete (GThreadBase::WorkIo, m_pCoCur);
};
//
//[]------------------------------------------------------------------------[]


//[]------------------------------------------------------------------------[]
//
Bool GWaitIoApartment::PumpInputWork ()
{
    Bool    bResult = False;

    GIrp *  pIrp    = NULL ;

	bResult	  = 
	m_WakeUpPort.PumpWakeUp ();

    if (NULL	 == (pIrp = GetIrpForComplete ()))
    {
	CompleteIrp (pIrp);
	
	bResult	  =  True ;
    }

    if (! bResult)
    {
	m_WakeUpPort.WaitWakeUp (NULL, 0);

	bResult	  = 
	m_WakeUpPort.PumpWakeUp ();
    }
	return bResult;
};

void GWaitIoApartment::WaitInputWork (DWord dTimeout)
{
printf ("\t\t\t\t<<GWaitIo...::Wait");

//  m_WakeUpPort.WaitWakeUp (NULL, dTimeout);

printf (">>\n");
};
//
//[]------------------------------------------------------------------------[]

#endif

