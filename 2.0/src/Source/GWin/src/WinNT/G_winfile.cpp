//[]------------------------------------------------------------------------[]
// GWin file io subsystem definitions
//
//
// Created by GSh.
//[]------------------------------------------------------------------------[]

#include "g_winfile.h"

#include <stdio.h>


//[]------------------------------------------------------------------------[]
//
/*
Bool GAPI GrabOverlappedResult (HANDLE hFile, GOverlapped & o, Bool bWait)
{
    DWord   dResult	= ERROR_IO_INCOMPLETE;
    DWord   dResultInfo = 0;

lRepeat :

    if (ERROR_IO_INCOMPLETE ==
       (dResult		     = (::GetOverlappedResult 
			       (hFile, & o, & dResultInfo, 
			       (sizeof (ULONG_PTR) < (ULONG_PTR) o.hEvent) ? bWait : False))

			      ? ERROR_SUCCESS : ::GetLastError ()))
    {
	if (bWait)
	{
	    ::Sleep (10);

	    goto lRepeat;
	}
	    return False;
    }
	    o.SetIoCoResult (dResult, (void *) dResultInfo);

	    return True ;
};

void GAPI GetOverlappedResult (HANDLE hFile, GOverlapped & o)
{
};

*/

Bool GAPI GrabOverlappedResult (HANDLE hFile, GOverlapped & o, Bool bWait)
{
    DWord   dResult	= ERROR_IO_INCOMPLETE;
    DWord   dResultInfo = 0;

lRepeat :

    if (ERROR_IO_INCOMPLETE ==
       (dResult		     = (::GetOverlappedResult 
			       (hFile, & o, & dResultInfo, 
			       (sizeof (ULONG_PTR) < (ULONG_PTR) o.hEvent) ? bWait : False))

			      ? ERROR_SUCCESS : ::GetLastError ()))
    {
	if (bWait)
	{
	    ::Sleep (10);

	    goto lRepeat;
	}
	    return False;
    }
	    o.SetIoCoResult (dResult, (void *) dResultInfo);

	    return True ;
};

GOverlapped::IoStatus GAPI PerformIo (GOverlapped & o		,
				      IIoCoWait *   pIIoCoWait	,
				      IIoCoPort *   pIIoCoPort	,

				      DWord	    dResult	,
				      void *	    pResultInfo	)
{
           o.StartIo   (0, 0, pIIoCoWait, pIIoCoPort);

    return o.GetIoCoStatus (dResult, pResultInfo);
};

GOverlapped::IoStatus GAPI WriteFile (HANDLE	    hFile       ,
				      DWord	    dOffset     ,
				      DWord	    dOffsetHigh ,
				      const void *  pBuf        ,
				      DWord	    dBufSize    ,
				      GOverlapped & o	        ,
				      IIoCoWait *   pIIoCoWait	,
//
//				      IIoCoPoint *  pIIoCoPoint	,
//				      HANDLE	    hIIoCoWait	,
				      IIoCoPort *   pIIoCoPort  )
{
    DWord   dResult	= 0;
    DWord   dResultInfo = 0;

    Bool bSQ = o.StartIo (dOffset, dOffsetHigh, pIIoCoWait, pIIoCoPort);

    dResult  = (::WriteFile (hFile, pBuf, dBufSize, & dResultInfo, & o))
			   ? ERROR_SUCCESS : ::GetLastError ();

    if (IsIoSysQueued (dResult, bSQ))
    {
	return GOverlapped::IoSysQueued;
    }
	return o.GetIoCoStatus (dResult, (void *) dResultInfo);
};

    struct SyncIoCoWait : public IIoCoWait
    {
	   SyncIoCoWait  (HANDLE hIoCoEvent) : m_hIoCoEvent (hIoCoEvent) {};

	STDMETHOD  (	     QueryInterface)(REFIID, void **)
	{ return ERROR_NOT_SUPPORTED;};

	STDMETHOD_ (ULONG,	     AddRef)()
	{ return 1;}

	STDMETHOD_ (ULONG,	    Release)()
	{ return 1;};

	STDMETHOD_ (HANDLE,   GetIoCoHandle)()
	{ return m_hIoCoEvent;};

	STDMETHOD_ (void,     QueueIoCoWait)(GOverlapped &)
	{};

	HANDLE	 m_hIoCoEvent;
    };

GResult GAPI WriteFile (HANDLE       hFile	 ,
			DWord        dOffset     ,
			DWord        dOffsetHigh ,
			const void * pBuf	 ,
			DWord        dBufSize    ,
			DWord *      pTransfered ,
			HANDLE	     hIoCoEvent  )
{
    GOverlapped  o ;
    SyncIoCoWait w (hIoCoEvent);

    if (GOverlapped::IoCompleted < ::WriteFile (hFile, dOffset, dOffsetHigh,
						       pBuf   , dBufSize   , o, & w, NULL))
    {
	GrabOverlappedResult (hFile, o, True);
    }
    if (pTransfered)
    {
      * pTransfered = 
       (DWord) o.GetIoCoResultInfo ();

	return o.GetIoCoResult     ();
    }
	return 
	(dBufSize  == (DWord) o.GetIoCoResultInfo ()) ?   ERROR_SUCCESS
						      : ((ERROR_SUCCESS == o.GetIoCoResult ()) ? ERROR_WRITE_FAULT : o.GetIoCoResult ());
};

GOverlapped::IoStatus GAPI ReadFile (HANDLE	   hFile       ,
				     DWord	   dOffset     ,
				     DWord	   dOffsetHigh ,
				     void *	   pBuf	       ,
				     DWord	   dBufSize    ,
				     GOverlapped & o	       ,
				     IIoCoWait *   pIIoCoWait  ,
				     IIoCoPort *   pIIoCoPort  )
{
    DWord   dResult	= 0;
    DWord   dResultInfo = 0;

    Bool bSQ = o.StartIo (dOffset, dOffsetHigh, pIIoCoWait, pIIoCoPort);

    dResult  = (::ReadFile (hFile, pBuf, dBufSize, & dResultInfo, & o))
			  ? ERROR_SUCCESS : ::GetLastError ();

    if (IsIoSysQueued (dResult, bSQ))
    {
	return GOverlapped::IoSysQueued;
    }
	return o.GetIoCoStatus (dResult, (void *) dResultInfo);
};

GResult GAPI ReadFile (HANDLE    hFile	     ,
		       DWord	 dOffset     ,
		       DWord	 dOffsetHigh ,
		       void  *   pBuf	     ,
		       DWord	 dBufSize    ,
		       DWord *   pTransfered ,
		       HANDLE    hCoEvent    )
{
    GOverlapped  o ;
    SyncIoCoWait w (hCoEvent);

    if (GOverlapped::IoCompleted < ::ReadFile (hFile, dOffset, dOffsetHigh,
						      pBuf   , dBufSize   , o, & w, NULL))
    {
	GrabOverlappedResult (hFile, o, True);
    }
    if (pTransfered)
    {
      * pTransfered = 
       (DWord) o.GetIoCoResultInfo ();

	return o.GetIoCoResult     ();
    }
	return 
	(dBufSize  == (DWord) o.GetIoCoResultInfo ()) ?   ERROR_SUCCESS
						      : ((ERROR_SUCCESS == o.GetIoCoResult ()) ? ERROR_READ_FAULT : o.GetIoCoResult ());
};

GOverlapped::IoStatus GAPI DeviceIoControl (HANDLE	  hFile	      ,
					    DWord	  dCtrlCode   ,
					    void *	  pInpBuf     ,
					    DWord	  dInpBufSize ,
					    void *	  pOutBuf     ,
					    DWord	  dOutBufSize ,
					    GOverlapped & o	      ,
					    IIoCoWait *	  pIIoCoWait  ,
					    IIoCoPort *	  pIIoCoPort  )
{
    DWord   dResult	= 0;
    DWord   dResultInfo = 0;

    Bool bSQ = o.StartIo (0, 0, pIIoCoWait, pIIoCoPort);

    dResult  = (::DeviceIoControl (hFile, dCtrlCode, pOutBuf, dOutBufSize,
						     pInpBuf, dInpBufSize, 
						   & dResultInfo, & o))
				 ? ERROR_SUCCESS : ::GetLastError ();

    if (IsIoSysQueued (dResult, bSQ))
    {
	return GOverlapped::IoSysQueued;
    }
	return o.GetIoCoStatus (dResult, (void *) dResultInfo);
};

GOverlapped::IoStatus GAPI LockFile (HANDLE	   hFile       ,
				     DWord	   dOffset     ,
				     DWord	   dOffsetHigh ,
				     DWord	   dLength     ,
				     DWord	   dLengthHigh ,
				     Bool	   bExclusive  ,
				     GOverlapped & o	       ,
				     IIoCoWait *   pIIoCoWait  ,
				     IIoCoPort *   pIIoCoPort  )
{
    DWord   dResult	= 0;

    Bool bSQ = o.StartIo (dOffset, dOffsetHigh, pIIoCoWait, pIIoCoPort);

    dResult  = (::LockFileEx (hFile  , (bExclusive) ? LOCKFILE_EXCLUSIVE_LOCK : 0, 0,
			      dLength,  dLengthHigh, & o))

			    ? ERROR_SUCCESS : ::GetLastError ();

    if (IsIoSysQueued (dResult, bSQ))
    {
	return GOverlapped::IoSysQueued;
    }
	return o.GetIoCoStatus (dResult, NULL);
};

GResult GAPI LockFile (HANDLE	hFile	    ,
		       DWord	dOffset	    ,
		       DWord	dOffsetHigh ,
		       DWord	dLength	    ,
		       DWord	dLengthHigh ,
		       Bool	bExclusive  ,
		       HANDLE	hIoCoEvent  )
{
    GOverlapped  o ;
    SyncIoCoWait w (hIoCoEvent);

    if (GOverlapped::IoCompleted < ::LockFile (hFile, dOffset, dOffsetHigh,
						      dLength, dLengthHigh, bExclusive, o, & w, NULL))
    {
	GrabOverlappedResult (hFile, o, True);
    }
	return o.GetIoCoResult ();
};

GOverlapped::IoStatus GAPI ConnectNamedPipe (HANDLE	   hFile      ,
					     GOverlapped & o	      ,
					     IIoCoWait *   pIIoCoWait ,
					     IIoCoPort *   pIIoCoPort )
{
    DWord   dResult	= 0;

    Bool bSQ = o.StartIo (0, 0, pIIoCoWait, pIIoCoPort);

    dResult  = (::ConnectNamedPipe (hFile, & o)) ? ERROR_SUCCESS : ::GetLastError ();

    if (IsIoSysQueued (dResult, bSQ))
    {
	return GOverlapped::IoSysQueued;
    }
    if (ERROR_PIPE_CONNECTED == dResult)
    {
    dResult  = ERROR_SUCCESS;
    }
	return o.GetIoCoStatus (dResult, NULL);
};
//
//[]------------------------------------------------------------------------[]





class GBindingEntry
{
public :

//	typedef GIrp *	   (GAPI *     DispatchProc)(GIrp *, GApartment *, void *,

//	typedef	GIrp *	   (GAPI *     DispatchProc)(GIrp *, void *, const void *);
};











//[]------------------------------------------------------------------------[]
//
    DECLARE_INTERFACE  (XBindingCallBack)
    {
    DECLARE_INTERFACE  (XInstance)
    {
    STDMETHOD_ (void,		    Release)(GIrp *) PURE;

    STDMETHOD_ (GIrp *,		     Invoke)(GIrp *) PURE;
    };

    STDMETHOD_ (XInstance *,	    Acquire)(GIrp *, GAccessLock    * pCancelLock    ,
						     GIrp_CancelProc  pCancelProc    ,
						     void *	      pCancelContext ) PURE;
    };
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
class GBindingCallBack : public XBindingCallBack
{
public :

private :

	class BindingEntry : public XBindingCallBack::XInstance
	{
	typedef TSLink <BindingEntry, sizeof (XBindingCallBack::XInstance)> Link;
	typedef TXList <BindingEntry,
		TSLink <BindingEntry, sizeof (XBindingCallBack::XInstance)> > List;
	};

};
//
//[]------------------------------------------------------------------------[]







class XBindingEntry
{
/*
	friend	XDispatchCallBack * GAPI CreateXDispatchCallBack (


	m_

*/

};
/*

    DECLARE_INTERFACE_ (XDispatchCallBack, IUnknown)
    {
    STDMETHOD_ (void,		    Connect)(XHandle **, void *
    };

    DECLARE_INTERFACE  (XHandle)
    {
    STDMETHOD_ (void,		    Destroy)() PURE;

    };

    DECLARE_INTERFACE_ (XService, IUnknown)
    {
    STDMETHOD_ (void,	CreateXDispatchCallBack)
    };


    void	RegisterXS

Service :

    CreateXDispatchCallBack (...);


Client :

    XDispatch * pXDispatch = CreateXDispatcher <Client>
			  (& Client::On_RRPService, this);

    


    m_Console.Connect	  (


    m_Console.Connect	  (  pXDispatch, NULL, 



*/


/*

void Client::On_QueryInterface (









	typedef	GIrp *	   (T::*       TDispatchProc)(GIrp *,  const void *);

	typedef	GIrp * 	   (GAPI *     XDispatchProc (GIrp *,  const void *, void *)

	friend	XDispatch * GAPI   CreateXDispatcher (XDispatchProc, void *);




    Connect (XDispatch *, 


GResult Server::Connect (XDispatchCallBackHandle **, XDispatchCallBack *,  XDispatchHandle *&, XDispatch *, const void * pRequestId,
				   GIrp_DpProc


Client :

	if (ERROR_SUCCESS == Connect (& pXDispatchCallBack,	pXDispatch	  , QueryXSourcer (pIRRPUrp, IID_IRRPUrpStatus),
				      & pXDispatchCallBack,

	if (ERROR_SUCCESS == pXDispatch ->Connect (& pXDispatchHandle, pIRRPUrp, IID_IRRPUrpStatus))
	{
	    ...
	}


	if (pXDispatchHandle)
	{
	    pXDispatchHandle ->Close ();
	}



	XDispatchSignal (
*/


//[]------------------------------------------------------------------------[]
//
class XSignalSourcer
{
public :

//	typedef	

//	friend	XDispatchHandle *

};



//
//[]------------------------------------------------------------------------[]



class GObjectHandle
{
public :

	typedef	void	(GAPI *	    CloseProc)(GObjectHandle &);

protected :

	void *		    GetObject	    () const { return m_pObject; };

private :

	void *		    m_pObject	;
	CloseProc	    m_pCloseProc;
};

template <typename T = void *> class TObjectHandle : public GObjectHandle
{
public :

	T 		    GetObject	    () const { return (T) GetObject ();};

private :
};

/*
IUnknown * SomeObjectProc ()
{
    TObjectHandle <IUnknown *>	hIUnknown   ;
    TObjectHandle <>		hSome	    ;

    return hIUnknown.GetObject ();
};
*/


























#if FALSE

class XReply_Entry : public XReply
{
public :

    STDMETHOD_ (void,		    OnReply)(GIrp *, XReply::Request  );

    STDMETHOD_ (XReply::Request,  OnRequest)(GIrp *,
				       const void *  pRequestId = NULL);

	void		    Bind	    (XReply::DispatchProc, void *);

protected :

#pragma pack (_M_PACK_VPTR)

	struct Request_Entry
	{
	    typedef TSLink <Request_Entry, 0>	SLink;
	    typedef TXList <Request_Entry,
		    TSLink <Request_Entry, 0> >	SList;

		    SLink	    m_QLink	 ;
		    XReply *	    m_pXReply	 ;
	      const void   *	    m_pRequestId ;

	       Request_Entry (XReply * pXReply	  ,
			const void   * pRequestId ) :

		    m_QLink  ()
	       {
		   (m_pXReply	    = pXReply	 ) ->AddRef ();
		    m_pRequestId    = pRequestId ;
	       };
	};

#pragma pack ()

	GIrp *		    OnDispatchReply (GIrp *, Request_Entry *);

protected :

	TInterlocked <ULONG>	    m_cRefCount	 ;
	TInterlocked <ULONG>	    m_cRefCount2 ;
	XDispatch *		    m_pXDispatch ;
	Request_Entry::SList	    m_QList	 ;
	void *			    m_oDst	 ;
	FOO_PMF			    m_tDstProc	 ;
	GIrp *		    (GAPI * m_pDstProc	 )(GIrp *, void *, const void *);
};

XReply::Request STDMETHODCALLTYPE XReply_Entry::OnRequest (GIrp * pIrp, const void * pRequestId)
{
    Request_Entry * rq = NULL;

    if (pIrp == NULL)
    {
	pIrp  = CreateIrp ();
    }
	rq    = new (pIrp, (Request_Entry **) NULL) Request_Entry (this, pRequestId);

        m_QList.append  (rq);

	return (XReply::Request) rq;
};

GIrp * XReply_Entry::OnDispatchReply (GIrp * pIrp, Request_Entry * rq)
{
	rq   = (rq) ? m_QList.extract (rq) : NULL;

	pIrp = (m_pDstProc)(pIrp, m_oDst, (rq) ? rq ->m_pRequestId : NULL);

    if (rq)
    {
	Release ();
    }

	return pIrp;
};

void STDMETHODCALLTYPE XReply_Entry::OnReply (GIrp * pIrp, XReply::Request hRequest)
{
    SetCurCoProc <XReply_Entry, Request_Entry *> 
   (pIrp, & XReply_Entry::OnDispatchReply, this, (Request_Entry *) hRequest);

   (m_pXDispatch) ->QueueIrpForComplete (pIrp);
};

void XReply_Entry::Bind (XReply::DispatchProc pDstProc, void * pContext)
{
};

void GAPI XReply_OnReply (GIrp * pIrp, XReply::Request hRequest)
{
    if (hRequest)
    {
//	InvokeXReplyXReply_Dispatch_hRequest
    }
	CompleteIrp (pIrp);
};
//
//[]------------------------------------------------------------------------[]


//[]------------------------------------------------------------------------[]
//
class GIrp_Dispatcher
{
protected :

	Bool		LockAcquire	    (Bool bExclusive = True) 
					    { 
						return m_DLock.LockAcquire (bExclusive);
					    };
	void		LockRelease	    () {       m_DLock.LockRelease ();};

virtual	void		QueueIrpForComplete (GIrp * pIrp, Bool bForceQueue);

virtual	void		QueueIrpForDispatch (GIrp * pIrp);

protected :


static	GIrp * GAPI	CompletePacket	    (GIrp *, void *, void *);

	void		CompleteNextPacket  ();

protected :

	IIoCoPort *	    m_pIIoCoPort ;

	GSpinCountCriticalSection   
			    m_DLock	 ;  // Lock for (*)

	GIrp_List	    m_qCoQueue	 ;  // (*)
	GIrp *		    m_pCoCur 	 ;  // (*)
};

/*
GIrp * GAPI CompletePacket (GIrp * pIrp, void * pIApartment, void *)
{
    ((GIrp_Dispatcher *) pIApartment) ->QueueIrpForComplete (pIrp, False);

    ((IApartment *) pIApartment) ->Release ();

    return NULL;
};

*/

void GIrp_Dispatcher::CompleteNextPacket ()
{
    LockAcquire ();

	m_pCoCur = m_qCoQueue.extract_first ();

    LockRelease ();

    if (m_pCoCur)
    {
	::QueueIrpForComplete (m_pCoCur, True);
    }
};

void GIrp_Dispatcher::QueueIrpForComplete (GIrp * pIrp, Bool bForceQueue)
{
    LockAcquire ();

	if (m_pCoCur && (m_pCoCur != pIrp))
	{
		pIrp =  (m_qCoQueue.append (pIrp), NULL);
	}
	else
	{
	    m_pCoCur =	 pIrp;
	}

    LockRelease ();

    if (pIrp)
    {
	::QueueIrpForComplete (pIrp);
    }
};


#endif

/*

//
//[]------------------------------------------------------------------------[]
//
DWord GWaitIoApartment::Run ()
{
    
};


//
//[]------------------------------------------------------------------------[]
//
//
//[]------------------------------------------------------------------------[]
//
//
//[]------------------------------------------------------------------------[]
//







void GAPI QueueIrpForComplete (GThread * pDst, GThread::Type tDst, GIrp * pIrp, Bool bForceQueue)
{
    if (pDst)
    {


    if (GThread::thUser == TCurrent () ->GetType ())
    {
	
    }
    else
    {
	g_WinProcess.m_pThreadPool ->QueueIrpForComplete (tType, pIrp, bForceQueue);
    }


    switch (thCur ->GetType ())
    {
	case GThread::thUser :

	    break;

	case GThread::thFree :
    }

    if (thCur ->GetType () == GThreadTypethWorkIoGThread_WorkIo

    if (thCur ->
    

    if (TCurrent () ->GetType ())
    {
    }
};

void GAPI QueueIrpForComplete (GIrp * pIrp, Bool bForceQueue)
{
	GThread::Type	 tType = TCurrent () ? TCurrent () ->GetType () 
					     : GThread::User ;

    if (bForceQueue || (GThread::Work != tType))
    {
	TPool () ->QueueIrpForComplete ((CoType) GThread::Work, pIrp);
    }
    else
    {
	CompleteIrp (pIrp);
    }
};













*/







/*
protected :

	Bool		LockAcquire	    (Bool bExclusive = True) 
					    { 
						return m_DLock.LockAcquire (bExclusive);
					    };
	void		LockRelease	    () {       m_DLock.LockRelease ();};

	void		CreateXReplyHandler (GIrp * pIrp);

#ifdef _MSC_BUG_C2248
public :
#endif

virtual	HRESULT	       XUnknown_QueryInterface	    (REFIID, void **);

virtual	void	       XUnknown_FreeInterface	    (void *);

virtual	void	      XDispatch_QueueIrpForComplete (GIrp *, Bool bForceQueue);

#ifdef _MSC_BUG_C2248
protected :
#endif

*/




//[]------------------------------------------------------------------------[]
//


//
//[]------------------------------------------------------------------------[]


//[]------------------------------------------------------------------------[]
//




/*
    DECLARE_INTERFACE  (IDispatchCallBack)
    {    
    STDMETHOD_ (void,		OnDispatch)(GIrp *) PURE;
    };
*/


//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//



    interface IIpcPort;

    DECLARE_INTERFACE_ (IIpcCallBack, IUnknown)
    {
    STDMETHOD_	(void,		    OnRecv)(IIpcPort * pIpcSource ,
					    GResult    dIoResult  ,
				      const void *     pRequestId ,
				      const void *     pData      ,
					    DWord      dDataLen   ) PURE;
    };

    DECLARE_INTERFACE_ (IIpcPort, IUnknown)
    {
    STDMETHOD_	(void,		    Cancel)(IIpcCallBack *	  ) PURE;

    STDMETHOD_	(void,		      Send)(IIpcCallBack *	  ,
				      const void *    pRequestId  ,
				      const void *    pSendBuf    ,
					    DWord     dSendBufLen ) PURE;
    };

//    interface ISerialCallBack;
    




//[]------------------------------------------------------------------------[]
//
GIrp * GSerialPort::Co_Send (GIrp * pSendIrp, void *)
{
    GResult   dResult	  = pSendIrp ->GetCurResult	();
    DWord     dResultInfo = (DWord)
			    pSendIrp ->GetCurResultInfo ();
    GIrp_List XferList;

	      XferList.append (GetCurRequest
			      <SendRequest > (pSendIrp) ->XferList);

    m_SendQueue.append (pSendIrp);

			pSendIrp = (PerformXfer (), NULL);
    {{
	GIrp * pIrp;

	while (NULL != (pIrp = XferList.extract_first ()))
	{
	    if (dResultInfo >= GetCurRequest <XferRequest> (pIrp) ->dSendBufLen)
	    {
		pIrp ->SetCurResultInfo (dResult, 
		      (void *) GetCurRequest <XferRequest> (pIrp) ->dSendBufLen);

		dResultInfo -= GetCurRequest <XferRequest> (pIrp) ->dSendBufLen;
	    }
	    else
	    {
		pIrp ->SetCurResultInfo (dResult, (void *) dResultInfo);

		dResultInfo  = 0;
	    }
		CompleteIrp (pIrp);
	};
    }}

	return pSendIrp;
};

void GSerialPort::BuildSendRequest (GIrp * pSendIrp, void * pBuf, DWord dBufSize)
{
    SetCurRequest (pSendIrp,
    CreateRequest 
    <SendRequest> (pSendIrp) ->Init (pBuf, dBufSize));
};

void GSerialPort::QueueForSend (GIrp * pSendIrp, void * pBuf, DWord dBufSize)
{
    BuildSendRequest   (pSendIrp, pBuf, dBufSize);

    m_SendQueue.append (pSendIrp);

    PerformXfer ();
};
//
//[]------------------------------------------------------------------------[]
//
GIrp * GSerialPort::Co_Recv (GIrp * pRecvIrp, void *)
{
    TXList_Iterator <GIrp> I;

    DWord	  t  = 0    ;
    RecvRequest * rq = GetCurRequest
   <RecvRequest> (pRecvIrp) ;

    if ((ISerial::streamraw == rq ->tDataType)
    ||	(ISerial::streamoob == rq ->tDataType)
    ||	(ISerial::stream    == rq ->tDataType))
    {
	if (0 !=   (t = rq ->dRecvSeqNo - m_dRecvAckNo))
	{
	    for (I = m_RecvQueue.head (); I.next ();)
	    {
		if (t < (GetCurRequest <RecvRequest> (I.next ()) ->dRecvSeqNo - m_dRecvAckNo))
		{
		    break;
		}
		I <<= m_RecvQueue.getnext (I.next ());
	    };
		      m_RecvQueue.insert  (I, pRecvIrp);

	    return NULL;
	}
    }

    do
    {
	SetCurRequest (pRecvIrp		 ,
	CreateRequest <ISerial::IoCo_Recv>
		      (pRecvIrp		 ) ->Init (pRecvIrp ->GetCurResult (),
						  (void *) m_dRecvAckNo	     ,
						   NULL			     ,
						   NULL			     ,
						   rq ->tDataType	     ,
						   rq ->pRecvBuf , 0	    ));

// ???	if ((ERROR_SUCCESS == pRecvIrp ->GetCurResult ())
//	&&  (ISerial::eof  != rq       ->tDataType      ))
	{
	GetCurRequest <ISerial::IoCo_Recv>
		      (pRecvIrp) ->dRecvBufLen = 
		      (DWord   )	 pRecvIrp ->GetCurResultInfo ();
	}
	    pRecvIrp = (DispatchIrp (pRecvIrp), NULL);

	    m_dRecvAckNo ++;

	if (m_RecvQueue.last () && (m_dRecvAckNo == GetCurRequest <RecvRequest> (m_RecvQueue.first ()) ->dRecvSeqNo))
	{
		  rq = GetCurRequest <RecvRequest> 
	   (pRecvIrp = m_RecvQueue.extract_first ());
	}

    } while (pRecvIrp);

	return NULL;
};

GIrp * GSerialPort::XxxxCo_Recv (GIrp * pRecvIrp, void *)
{
#if TRUE

#else

    if	   (m_pApartment == GetCur_Apartment ())
    {
	if (m_pApartment ->m_pCoCur == pRecvIrp)
	{
	    Force Queue !!!
	}
	else
	{
	    Execute
	}
    }
    else
    {
	    Queue
    }

1)  GetCur_Apartment () != m_p







    return (m_pPortApartment ->QueueIrpForComplete (pIrp, False), NULL);

#endif
    return NULL;
};

GIrp * GSerialPort::Co_RecvLoop (GIrp * pRecvIrp, void *)
{
    RecvRequest * rq = GetCurRequest
   <RecvRequest> (pRecvIrp);

#if FALSE

    if (ERROR_SUCCESS == pRecvIrp ->GetCurResult ())
    {
	SetCurCoProc <GSerialPort, void *>
		     (pRecvIrp, & GSerialPort::Co_RecvLoop, this, NULL);

	SetCurCoProc <GSerialPort, void *>
		     (pRecvIrp, & GSerialPort::Co_Recv	  , this, NULL);

	SetCurCoProc <GSerialPort, void *>
		     (pRecvIrp, & GSerialPort::XxxxCo_Recv, this, NULL);



	rq ->dRecvSeqNo = m_dRecvSeqNo ++ ;
	rq ->tDataType  = ISerial::eof	  ;

	pRecvIrp = Io_RecvProc  (pRecvIrp);
    }
	return pRecvIrp;

#else

    if (m_fCloseLock.LockAcquire ())
    {
	SetCurCoProc <GSerialPort, void *>
		     (pRecvIrp, & GSerialPort::Co_RecvLoop, this, NULL);

	SetCurCoProc <GSerialPort, void *> 
		     (pRecvIrp, & GSerialPort::Co_Recv	  , this, NULL);

	rq ->dRecvSeqNo = m_dRecvSeqNo ++ ;
	rq ->tDataType  = ISerial::eof	  ;

	pRecvIrp = Io_RecvProc  (pRecvIrp);

	m_fCloseLock.LockRelease ();
    }
    else
    {
	pRecvIrp ->SetResultInfo (ERROR_INVALID_HANDLE, NULL);
    }

#endif

	return pRecvIrp;
};

void GSerialPort::BuildRecvRequest (GIrp * pRecvIrp, void * pBuf, DWord dBufSize)
{
    SetCurRequest (pRecvIrp,
    CreateRequest 
    <RecvRequest> (pRecvIrp) ->Init (pBuf, dBufSize));
};

void GSerialPort::QueueForRecv (GIrp * pRecvIrp, void * pBuf, DWord dBufSize)
{
    BuildRecvRequest (pRecvIrp, pBuf, dBufSize);

    SetCurCoProc     <GSerialPort, void *>
		     (pRecvIrp, GSerialPort::Co_RecvLoop, this, NULL);

    CompleteIrp	     (pRecvIrp);
};
//
//[]------------------------------------------------------------------------[]
//
GIrp * GSerialPort::PrepareXfer ()
{
	GIrp *  pSendIrp;
	GIrp *  pIrp    ;

    if	      (NULL != (pSendIrp = m_SendQueue.first ()))
    {
	while (NULL != (pIrp     = m_XferQueue.extract_first ()))
	{
	    if (GetCurRequest <SendRequest> (pSendIrp) ->dSendBufSize
	    >=  GetCurRequest <XferRequest> (pIrp    ) ->dSendBufLen )
	    {
		memcpy 
	       (GetCurRequest <SendRequest> (pSendIrp) ->pSendBuf    ,
		GetCurRequest <XferRequest> (pIrp    ) ->pSendBuf    ,
		GetCurRequest <SendRequest> (pSendIrp) ->dSendBufLen =
		GetCurRequest <XferRequest> (pIrp    ) ->dSendBufLen );

		GetCurRequest <SendRequest> (pSendIrp) ->XferList.append (pIrp);

		break;
	    }
	    else
	    {
		pIrp ->SetResultInfo (ERROR_INSUFFICIENT_BUFFER, NULL);

		QueueIrpForComplete  (pIrp, True);
	    }
	};
    }
	return (pSendIrp) ? ((GetCurRequest <SendRequest> (pSendIrp) ->XferList.last ())
			  ?  m_SendQueue.extract 	  (pSendIrp)
			  :  NULL)
			  :  NULL;
};

void GSerialPort::PerformXfer ()
{
	GIrp *	 pSendIrp;

    if (NULL != (pSendIrp = PrepareXfer ()))
    {
	if (m_fCloseLock.LockAcquire ())
	{
	    SetCurCoProc <GSerialPort, void *>
			(pSendIrp, GSerialPort::Co_Send, this, NULL);

	    pSendIrp = Io_SendProc (pSendIrp);

	    m_fCloseLock.LockRelease ();
	}
	else
	{
	    pSendIrp ->SetResultInfo (ERROR_INVALID_HANDLE, NULL);
	}
	if (pSendIrp)
	{
	    CompleteIrp (pSendIrp);
	}
	    PerformXfer ();
    }
};
//
//[]------------------------------------------------------------------------[]
//
HRESULT GSerialPort::XUnknown_QueryInterface (REFIID iid, void ** pi)
{
    if (iid == IID_ISerial)
    {
	((ISerial *)(* pi = & m_ISerial)) ->AddRef ();

	return ERROR_SUCCESS;
    }
	return ERROR_NOT_SUPPORTED;
};

void GSerialPort::XUnknown_FreeInterface (void * pi)
{};
//
//[]------------------------------------------------------------------------[]
//
void GSerialPort::ISerial_Listen (GIrp *, XDispatchCallBack *,
				  const void *	,
				  const ISerial::BusAddress *)
{};

void GSerialPort::ISerial_Connect (GIrp *, XDispatchCallBack *,
				   const void *	, 
				   const ISerial::BusAddress *,
				   const void *	, DWord )
{};

void GSerialPort::ISerial_Disconnect  (GIrp *, XDispatchCallBack *,
		    		       const void *)
{};

void GSerialPort::ISerial_Xfer (GIrp *  pIrp	    ,
				XDispatchCallBack *
					pICallBack  ,
			  const void *	pRequestId  ,
			  const ISerial::BusAddress *
					pAddress    ,
			  const void *  pSendBuf    ,
				DWord   dSendBufLen )
{
    GIrp *	    pSendIrp = NULL;

    pIrp = (pIrp) ? pIrp : CreateIrp ();

    SetCurRequest
    <XferRequest>  (pIrp,
    CreateRequest  
    <XferRequest>  (pIrp) ->Init (pICallBack ,
				  pRequestId ,
				  pAddress   ,
				  pSendBuf   ,
				  dSendBufLen), XferRequest::Destroy);
    m_XferQueue.append (pIrp);

    PerformXfer ();
};
//
//[]------------------------------------------------------------------------[]


//[]------------------------------------------------------------------------[]
//
void GSerialIoDevice::BuildSendRequest (GIrp * pSendIrp, void * pBuf, DWord dBufSize)
{
    SetCurRequest (pSendIrp,
    CreateRequest 
    <SendRequest> (pSendIrp) ->Init (pBuf, dBufSize));
};
//
//[]------------------------------------------------------------------------[]
//
void GSerialIoDevice::BuildRecvRequest (GIrp * pRecvIrp, void * pBuf, DWord dBufSize)
{
    SetCurRequest (pRecvIrp,
    CreateRequest 
    <RecvRequest> (pRecvIrp) ->Init (pBuf, dBufSize));
};
//
//[]------------------------------------------------------------------------[]
//
GResult GSerialIoDevice::CreateIoCoHandler (XIoCoHandler *& hIoCoHandler)
{
    return ERROR_NOT_SUPPORTED;
};
//
//[]------------------------------------------------------------------------[]
//
GIrp * GSerialIoDevice::Co_Close (GIrp * pIrp, void *)
{
	return pIrp;
};

GIrp * GSerialIoDevice::Xxxx_Dp_Close (GIrp * pIrp, GIrp_DpContext, void *)
{
    if (False)
    {
	return pIrp;
    }
	pIrp ->SetResultInfo (ERROR_SUCCESS, NULL);

	return pIrp;
};

void GSerialIoDevice::QueueForClose (GIrp * pIrp)
{
    if (m_fCloseLock.CloseAcquire ())
    {
	SetCurCoProc <GSerialIoDevice, void *>
       (pIrp, & GSerialIoDevice::Co_Close, this, NULL);

	SetCurDpProc <GSerialIoDevice, void *>
       (pIrp, & GSerialIoDevice::Xxxx_Dp_Close, this, NULL);

	m_fCloseLock.QueueForClose (pIrp);

	return;
    }
	pIrp ->SetResultInfo (ERROR_SUCCESS, NULL);

	CompleteIrp (pIrp);
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
/*
static Bool GAPI XxxxIoCo_WakeUp (IWakeUp * pIWakeUp, IWakeUp::Reason reason, void *, void * o)
{
    if (reason == IWakeUp::OnTimeout)
    {
    if (! HasOverlappedIoCompleted ((GOverlapped *) o))
    {
	return True;
    }
    }
//  else
//  if (reason == IWakeUp::OnOccured)
//  {}
//  else
//  if (reason == IWakeUp::OnCancel )
//  {}
    if (! ((GOverlapped *) o) ->GrabIoCoResult ())
    {
    }
	return (((GOverlapped *) o) ->CallIoCoProc (), False);
};

static void GAPI XxxxIoCo_Send (GOverlapped & o, GIoDevice * pIoDevice, GIrp * pIrp)
{
    if (pIoDevice ->GetOverlappedResult (* o))
    {
	pIrp ->SetCurResultInfo (o.GetIoCoResult     (),
				 o.GetIoCoResultInfo ());
    }
    else
    {
	::RaiseException (0x80000000, 0, 0, NULL);
    }
};

Bool GIoDevice::GetOverlappedResult (GOverlapped & o)
{
    if (! o.GrabIoCoResult ())
    {
    if (m_fCloseLock.LockAcquire ())
    {
	if (! GetOverlappedResult (m_hFile, o, False))
	{

	}
	m_fCloseLock.LockRelease ();
    }
    }
};

GIrp * GIoDevice::Io_Send ()
{
    if (m_fCloseLock.LockAcquire ())
    {
	oStatus = ::WriteFile (m_hFile, 0, 0, pBuf	    ,
					      dBufSize	    ,
					      o.SetIoCoProc
			      (XxxxIoCo_Send, this, pIrp)   ,

		    HEVENT ????

					      m_pIoCoPort   );
	m_fCloseLock.LockRelease ();

	if (GOverlapped::IoSysQueued == oStatus)
	{}
	else
	{
	}
    }
    else
    {
	pIrp ->SetCurResultInfo (ERROR_INVALID_HANDLE);
    }
	return pIrp;
};

*/

void GAPI GSerialFile::XxxxIoCo_Send (GOverlapped &, GSerialFile * pThis, GIrp * pSendIrp)
{
    SendRequest * rq = GetCurRequest
   <SendRequest> (pSendIrp);

    if (! rq ->o.GrabIoCoResult ())
    {
	if (pThis ->m_fCloseLock.LockAcquire ())
	{
	    if (! GrabOverlappedResult (pThis ->m_hFile, rq ->o, False))
	    {
		::RaiseException (0x80000000, 0, 0, NULL);
	    }
	    pThis ->m_fCloseLock.LockRelease ();

	    pSendIrp ->SetCurResultInfo (rq ->o.GetIoCoResult     (),
					 rq ->o.GetIoCoResultInfo ());
	}
	else
	{
	    pSendIrp ->SetCurResultInfo (ERROR_INVALID_HANDLE, NULL);
	}
    }
    else
    {
	    pSendIrp ->SetCurResultInfo (rq ->o.GetIoCoResult     (),
					 rq ->o.GetIoCoResultInfo ());
    }
	    CompleteIrp (pSendIrp);
};

GIrp * GSerialFile::Io_SendProc (GIrp * pSendIrp)
{
    SendRequest * rq = GetCurRequest
   <SendRequest> (pSendIrp);

    GOverlapped::IoStatus   oStatus = GOverlapped::IoCompleted;

    if (m_fCloseLock.LockAcquire ())
    {
/*
	oStatus = ::WriteFile (m_hFile, 0, 0, rq ->pSendBuf	,
					      rq ->dSendBufLen	,
					      rq ->o.SetIoCoProc
	 ((GOverlapped::IoCoProc) XxxxIoCo_Send, this, pSendIrp),
					      rq ->pIIoCoWakeUp	,
					      m_pIIoCoPort	);
*/
	m_fCloseLock.LockRelease ();

	if (GOverlapped::IoSysQueued == oStatus)
	{}
	else
	{
	    if (! rq ->o.QueueIoCoStatus (oStatus, False))
	    {
		  rq ->o.CallIoCoProc    ();
	    }
	}
	pSendIrp = NULL;
    }
    else
    {
	pSendIrp ->SetCurResultInfo (ERROR_INVALID_HANDLE);
    }
	return pSendIrp;
};

/*
Bool GAPI GSerialFile::XxxxIoCo_Wait (IWakeUp * pIWakeUp, IWakeUp::Reason reason, GSerialFile *, void * o)
{
    if (reason == IWakeUp::OnTimeout)
    {
    if (! HasOverlappedIoCompleted ((GOverlapped *) o))
    {
	return True;
    }
    }
//  else
//  if (reason == IWakeUp::OnOccured)
//  {}
//  else
//  if (reason == IWakeUp::OnCancel )
//  {}
	return (((GOverlapped *) o) ->CallIoCoProc (), False);
};

void GAPI GSerialFile::XxxxIoCo_Send (GOverlapped &, GSerialFile * pThis, GIrp * pSendIrp)
{
};

GIrp * GSerialFile::Io_SendProc (GIrp * pSendIrp)
{
    SendRequest * rq = GetCurRequest
   <SendRequest> (pSendIrp);

    GOverlapped::IoStatus   oStatus;

    if (m_fCloseLock.LockAcquire ())
    {
	oStatus = ::WriteFile (m_hFile, 0, 0, rq ->pSendBuf	,
					      rq ->dSendBufLen	,
					      rq ->o.SetIoCoProc
	 ((GOverlapped::IoCoProc) XxxxIoCo_Send, this, pSendIrp),
					      rq ->pIIoCoWakeUp	,
					      m_pIIoCoPort	);

	m_fCloseLock.LockRelease ();

	if (GOverlapped::IoSysQueued == oStatus)
	{}
	else
	{
	    if (! rq ->o.QueueIoCoStatus (
	}	




#if FAST_CALL
	else
	if (GOverlapped::IoPending   == oStatus)
	{
	    rq ->pIIoCoWakeUp ->QueueIoCoStatus (NULL		,
						 XxxxIoCo_Send	,
						 this, & rq ->o	,
						 INFINITE	,
						 10		);
	}
#else
	else
	{
	    rq ->pIIoCoWakeUp ->QueueIoCoStatus	(NULL		,
						 XxxxIoCo_Send	,
						 this, & rq ->o	,
						 INFINITE	,
						 10		);
	}
#endif


	    rq ->pIIoCoWakeUp ->Queue

	    if (! rq ->o.QueueIoCoStatus (oStatus, False))
	    {
		  rq ->o.CallIoCoProc    ();
	    }
	}
	pSendIrp = NULL;
    }
    else
    {
	pSendIrp ->SetCurResultInfo (ERROR_INVALID_HANDLE);
    }
	return pSendIrp;
};


GIrp * Class::XxxxIoCo_Recv (GIrp *, DpContext * pDpContext)
{

};


GIrp * Class::SomeDpProc (GIrp * pIrp, DpContext * pDpContext)
{
    if (pIrp ->Cancelled ())
    {
	return pIrp;
    }    

    switch (pDpContext ->uPhase)
    {
	case 0 :
	    SetCurRequest (pIrp)
	    CreateRequest (....);

	    SetCurCoProc <Class, DpContext *>
	   (pIrp, & Class::XxxIoCo_Recv, this, pDpContext);


    };
};


void Class::SomeDispatcher (GIrp * pIrp)
{
    DpContext * pDpContext = new (pIrp, (DpContext *) NULL) DpContext (...);

    SetCurDpProc <Class, DpContext *>
   (pIrp, & Class::DispatchProc, this, pDpContext);

    SetCurDpContext (pIrp, 

    DispatchIrp (pIrp);        
}   



   (m_pWinPipeAcceptor = new (pIrp, (GWinPipeAcceptor *) NULL) GWinPipeAcceptor ()) ->



*/




//
//[]------------------------------------------------------------------------[]
//
void GAPI GSerialFile::XxxxIoCo_Recv (GOverlapped &, GSerialFile * pThis, GIrp * pRecvIrp)
{
    RecvRequest * rq = GetCurRequest
   <RecvRequest> (pRecvIrp);

    if (! rq ->o.GrabIoCoResult ())
    {
	if (pThis ->m_fCloseLock.LockAcquire ())
	{
	    if (! GrabOverlappedResult (pThis ->m_hFile, rq ->o, False))
	    {
		::RaiseException (0x80000000, 0, 0, NULL);
	    }

	    pThis ->m_fCloseLock.LockRelease ();

	    pRecvIrp ->SetCurResultInfo (rq ->o.GetIoCoResult     (),
					 rq ->o.GetIoCoResultInfo ());
	}
	else
	{
	    pRecvIrp ->SetCurResultInfo (ERROR_INVALID_HANDLE, NULL);
	}
    }
    else
    {
	    pRecvIrp ->SetCurResultInfo (rq ->o.GetIoCoResult	  (),
					 rq ->o.GetIoCoResultInfo ());
    }
	    CompleteIrp (pRecvIrp);
};

GIrp * GSerialFile::Io_RecvProc (GIrp * pRecvIrp)
{
    RecvRequest * rq = GetCurRequest
   <RecvRequest> (pRecvIrp);

    GOverlapped::IoStatus   oStatus = GOverlapped::IoCompleted;

    if (m_fCloseLock.LockAcquire ())
    {
/*
	oStatus = ::ReadFile (m_hFile, 0, 0, rq ->pRecvBuf     ,
					     rq ->dRecvBufSize ,
					     rq ->o.SetIoCoProc
	((GOverlapped::IoCoProc) XxxxIoCo_Recv, this, pRecvIrp),
					     rq ->pIIoCoWakeUp ,
					     m_pIIoCoPort      );
*/
	m_fCloseLock.LockRelease ();

	if (GOverlapped::IoSysQueued == oStatus)
	{}
	else
	{
	    if (! rq ->o.QueueIoCoStatus (oStatus, False))
	    {
		  rq ->o.CallIoCoProc ();
	    }
	}

	pRecvIrp = NULL;
    }
    else
    {
	pRecvIrp ->SetCurResultInfo (ERROR_INVALID_HANDLE);
    }
	return pRecvIrp;
};
//
//[]------------------------------------------------------------------------[]
//
GIrp * GSerialFile::Xxxx_Dp_Close (GIrp * pIrp, GIrp_DpContext, void *)
{
    if (False)
    {
	return pIrp;
    }
	::CloseHandle (m_hFile);

		       m_hFile = NULL;

	pIrp ->SetCurResultInfo (ERROR_SUCCESS, NULL);

	return pIrp;
};
//
//[]------------------------------------------------------------------------[]










#if FALSE

//[]------------------------------------------------------------------------[]
//
class GSerialBus : protected GSerialPort
{
public :

virtual	HRESULT	    XUnknown_QueryInterface (REFIID, void **);

virtual void	    XUnknown_FreeInterface  (void *);

virtual	void	    ISerial_Xfer	    (GIrp *	  pIrp	      ,
				       const ISerial::BusAddress * 
							  pAddress    ,
				       const void *	  pSendBuf    ,
					     DWord	  dSendBufLne );

	void	    ISerialBus_Listen	    (GIrp *	  pIrp	      ,
					     ISerialCallBack *	      ,
				       const void *	  pRequestId  ,
				       const ISerial::BusAddress *    );

	void	    ISerialBus_XferConnect  (GIrp *	  pIrp	      ,
					     ISerialCallBack *	      ,
				       const void *	  pRequestId  ,
				       const ISerial::BusAddress *    ,
				       const void *	  pSendBuf    ,
					     DWord	  dSendBufLen ,
					     Bool	  bBuffered   );
protected :

virtual	GResult	    BuildXferConnectPack    (GStreamBuf	&	      ,
				       const void *	  pRequestId  ,
				       const ISerial::BusAddress *    ,
				       const void *	  pSendBuf    ,
					     DWord	  dSendBufLen );
protected :
/*
	class ISerialBus_Entry : public TInterface_Entry <ISerialBus, GSerialBus>
	{
	public :

	STDMETHOD_ (void,	   Xfer)(GIrp *	      pIrp	  ,
				   const BusAddress * pAddress	  ,
				   const void *	      pSendBuf	  ,
					 DWord	      dSendBufLen )
	{
	};

	STDMETHOD_ (void,	 Listen)(GIrp *	      pIrp	  ,
					 ISerialCallBack *
						      pICallBack  ,
				   const void *	      pRequestId  ,
				   const BusAddress * pSrcAddress )
	{
	};

	STDMETHOD_ (void,   XferConnect)(GIrp *	      pIrp	  ,
					 ISerialCallBack *	  
						      pICallBack  ,
				   const void *	      pRequestId  ,
				   const BusAddress * pDstAddress ,
				   const void *	      pSendBuf    ,
					 DWord	      dSendBufLen ,
					 Bool	      bBuffered	  )
	{
	};
	}			m_ISerialBus;
*/
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
GResult GSerialBus::BuildXferConnectPack (GStreamBuf & s	  ,
				   const void *	      pRequestId  ,
				   const ISerial::BusAddress *
						      pDstAddress ,
				   const void *	      pSendBuf	  ,
					 DWord	      dSendBufLen )
{
    return ERROR_NOT_SUPPORTED;
};

void GSerialBus::ISerialBus_XferConnect (GIrp *	      pIrp	  ,
					 ISerialCallBack *
						      pICallBack  ,
				   const void *	      pRequestId  ,
				   const ISerial::BusAddress * 
						      pDstAddress ,
				   const void *	      pSendBuf    ,
					 DWord	      dSendBufLen ,
					 Bool	      bBuffered   )
{
    TStreamBuf <1024>	m_Request;

    pIrp = (pIrp) ? pIrp : CreateXferIrp ();
};
//
//[]------------------------------------------------------------------------[]

#endif



















//[]------------------------------------------------------------------------[]
//
#pragma pack (_M_PACK_VPTR)

#define	IoCo_RecvRequest_CtrlCode	0x0003

	struct IoCo_RecvRequest : public IoCo_Request
	{
	    ISerial *		pSrcISerial ;
	    const ISerial::BusAddress *
				pSrcAddress ;
	    ISerial::DataType	tDataType   ;
	    const void *	pDataBuf    ;
	    DWord		dDataBufLen ;

	    IoCo_RecvRequest * Init (GResult	dIoCoResult     ,
				     void *	pIoCoResultInfo ,
				     ISerial *	pSrcISerial	,
			       const ISerial::BusAddress *
						pSrcAddress	,
				     ISerial::DataType
						tDataType	,
			       const void *	pDataBuf	,
				     DWord	dDataBufLen	)
	    {
		IoCo_Request::Init (IoCo_RecvRequest_CtrlCode, dIoCoResult, pIoCoResultInfo);

	    if((this ->pSrcISerial = pSrcISerial) != NULL)
	    {	this ->pSrcISerial ->AddRef    ();}

		this ->pSrcAddress = pSrcAddress ;
		this ->tDataType   = tDataType	 ;
		this ->pDataBuf	   = pDataBuf    ;
		this ->dDataBufLen = dDataBufLen ;

		return this;
	    };

	    static void GAPI	    Destroy (IoCo_RecvRequest * pThis)
			    {
				if (pThis ->pSrcISerial)
				{
				    pThis ->pSrcISerial ->Release ();
				    pThis ->pSrcISerial = NULL	    ;
				}
			    };
	};

#pragma pack ()
//
//[]------------------------------------------------------------------------[]



/*
    DECLARE_INTERFACE_ (IRpcPort, IUnknown)
    {
    STDMETHOD_ (void,	  ListenFor)(ISerialCallBack *	      ,
			       const void *
				     GDataPack *  p
    };


#pragma pack (_M_PACK_VPTR)


#pragma pack ()
*/

/*
    


    DECLARE_INTERFACE_ (ISerialCallBack, IUnknown)
    {
    STDMETHOD_ (GIrp *,      OnRecv)(GIrp *	pIrp	    ,
				     DWord	dIoResult   ,
			       const ISerial::BusAddress *  ,
				     ISerial::DataType	    ,
				     void *	pData	    ,
				     DWord	dDataLen    ) PURE;
    };
*/


//[]------------------------------------------------------------------------[]
//

    DECLARE_INTERFACE_ (IRpc, IUnknown)
    {
//    STDMETHOD_ (void,	    Register
    };


#pragma pack (_M_PACK_VPTR)



#pragma pack ()


class GRpcPort
{
public :


#pragma pack (_M_PACK_VPTR)

	struct BindingEntry
	{
/*
	    typedef TSLink <BindingEntry, 0>	Link ;

	    typedef TXList <BindingEntry,
*/		

	};

#pragma pack ()


protected :

virtual	void		On_SerialRecv	(GIrp *, GApartment *);

	void	   Xxxx_On_SerialRecv	(GIrp *, GApartment *);
};


/*


void OnSomeConnect (...)
{
}

void SomeConnect (XDispatchCallBack * pICallBack, ISerial * pITransport)
{
    TSerialBusAddress <32> Address;
    CreateAddress (Address, "Tcp\\CLSIID_Roent"Roent Device");

    RegisterClass (NULL, "Tcp\\192.168.0.254:5005", GWinSock::

    RegisterClass (NULL, "Tcp\\RoentDevice\\", ..."

    if (ERROR_SUCCESS == RegisterResource 
			(new GWinSock::TcpConnection ("192.168.0.103:5005"));


    QueryInterface (GetClassId (


    if (ERROR_SUCCESS == GSerialPort::CreateInstance (& m_pISerial))
    {
	m_pISerial ->Connect (CreateIrp (), 
			      m_pICallBack, NULL, NULL, NULL, NULL)
    }


    QueryInterface ("tcp\\192.168.0.103:5000", IID_


    RegisterClass  (...






Run Client :


1)  RegisterClass  (GWinSock::, ... for creation connection, CLSID_TcpConnection, new )

    GWinSockConnector	Connector.


    new TcpConnection () ->SetV4Address ("192.168.0.10",

    RegisterClass  (new V4TcpConnection








*/
//
//[]------------------------------------------------------------------------[]


#pragma pack (_M_PACK_VPTR)
/*
#   define  On_RecvRequest_CtrlCode	0x0004

    struct  On_RecvRequest : public IoDp_Request
    {
	ISerial *	    pSrcSerial  ;
	const ISerial::BusAddress *
			    pSrcAddress ;
	ISerial::DataType   tDataType   ;
	const void *	    pRecvBuf    ;
	DWord		    dRecvBufLen ;

	    On_RecvRequest *	Init (GResult	dIoResult     ,
				      void *	pIoResultInfo ,
				const ISerial::BusAddress *
						pSrcAddress   ,
				      ISerial::DataType
						tDataType     ,
				const void *	pRecvBuf      ,
				      DWord	dRecvBufLen   )
	    {
		IoDp_Request::Init (On_RecvRequest_CtrlCode, dIoResult, pIoResultInfo);

		this ->pSrcSerial  = NULL	 ;
		this ->pSrcAddress = pSrcAddress ;
		this ->tDataType   = tDataType	 ;
		this ->pRecvBuf	   = pRecvBuf    ;
		this ->dRecvBufLen = dRecvBufLen ;

		return this;
	    };
    };


    struct On_RpcRequest : public IoDp_Request
    {
    };
*/
#pragma pack ()

//[]------------------------------------------------------------------------[]
//
void GRpcPort::Xxxx_On_SerialRecv (GIrp * pIrp, GApartment *)
{
/*
    On_RecvRequest * rq = GetCurRequest
   <On_RecvRequest> (pIrp);

    if (On_RecvRequest_CtrlCode == rq ->GetCtrlCode ())
    {
    }
*/
};
//
//[]------------------------------------------------------------------------[]

/*

void SomeClass::ISerialCallBack_OnDispatch (GIrp * pIrp)
{
};

GIrp * GSerialReceiver::XxxxIoDp_OnDispatch (GIrp * pIrp, GIrp_DpContext, void * hRequest)
{
    LockAcquire (false);



    LockRelease ();
};

SomeProc ()
{
    void *		t;
    TStreamBuf <512>	s;

    
    t = s._PutSizeMarkerW (	 );
	s._PutBigEndian   (iid   );
	s._PutBigEndianD  (0x8024);
	s._PutBigEndianD  (0	 );

	::_PutBigEndianW  (t, 0, (Byte)(s.Tail () - (Byte *) t));


	pISerialPort ->Xfer (NULL, 

			     s.Head (), s.ForGetSize ());
};






*/
































//[]------------------------------------------------------------------------[]
//
class GIpcPort : public TXUnknown_Entry <GIpcPort>
{
public :

virtual	HRESULT	    XUnknown_QueryInterface (REFIID, void **);

virtual void	    XUnknown_FreeInterface  (void *);

virtual	void	    IIpcPort_Cancel	(IIpcCallBack * pIRecv	   );

virtual	void	    IIpcPort_Send	(IIpcCallBack * pIRecv	   ,
					 const void *	pRequestId ,
					 const void *	pSendBuf   ,
					       DWord	dSendBufLen);
protected :

#pragma pack (_M_PACK_VPTR)

	struct CallBack_Entry
	{
	    typedef TSLink <CallBack_Entry, 0>	 Link ;
	    typedef TXList <CallBack_Entry,
		    TSLink <CallBack_Entry, 0> > List ;

	    Link		qLink	  ;
	    const void *	pRequestId;
	};

#pragma pack ()


	void		LockAcquire	    () { m_PLock.LockAcquire ();};

	void		LockRelease	    () { m_PLock.LockRelease ();};

	class IIpcPort_Entry : public TInterface_Entry <IIpcPort, GIpcPort>
	{			    	
	public :
	STDMETHOD_ (void,    Cancel)(IIpcCallBack * pIRecv)
	{
	    GetT () ->IIpcPort_Cancel (pIRecv);
	};
	STDMETHOD_ (void,      Send)(IIpcCallBack * pIRecv,
			       const void *    pRequestId ,
			       const void *    pSendBuf   ,
			    	     DWord     dSendBufLen)
	{
	    GetT () ->IIpcPort_Send (pIRecv, pRequestId, pSendBuf, dSendBufLen);
	};
	}			m_IIpcPort  ;

protected :

	GAccessLock		m_PLock	    ;

	CallBack_Entry *	m_pParent   ;
	CallBack_Entry::List	m_qList	    ;

	GStreamBuf		m_SendBuf   ;
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
void GIpcPort::IIpcPort_Send (IIpcCallBack * pIRecv	,
			      const void *   pRequestId ,
			      const void *   pSendBuf   ,
			      DWord	     dSendBufLen)
{
    if (pIRecv)
    {
	
    }
};
//
//[]------------------------------------------------------------------------[]









//[]------------------------------------------------------------------------[]
//

//
//[]------------------------------------------------------------------------[]



/*
    1) Load GWinSock param for build GWinSock object.

    2) Build GWinSock instance.

    3) Register GWinSock::XUnknown as some ClassId(GSerialClass)::InstnaceId. Id from server.


    ... in some time user begin work with device, He:

    1) Enum all devices records, and select DeviceId,ClassId(GSerialClass)::InstanceId for connection.

       CreateInterface (IRpcPort, )

       pIRpcPort ->Connect (...) GWinSock::XUnknown...

       pIRpcPort ->CreateInterface (ClassId(G


    IRpcRoentDevice

       pIRpcRoentDeivceRegisterInterface



   


    IRpcPort *	    pIRpcPort ;

    if (ERROR_SUCCESS == QueryInterface (& pIRpcPort, CLSID_GRpcPort, * pInstanceId, * IID_IRpcPort, (void **) & pIRpcPort))
    {
	pIRpcPort ->QueryInterface (IID_IRoentSerivce, (void **) & pR_IRoentService);

	pIRpcPort ->QueryInterface (IID_IRoentDevice, (void **) & pIRoentDevice);

	pIRpcPort ->RegisterClientBuilder (CLSID_GSerialPort, new GSerialPortBuilder (....))

	pIRpcPort ->CreateServerObject   (CLSID_GSerialPort, IID_ISerialPort, & pISerialPort, IID_ISerialPort,
    }


    CreateInstance     (REFIID &&

    pISerial ->Connect (pIrp, I



*/








class GSerialBuf
{
public :

//	void	    Send	    (
};

class GSerialReceiver
{
public :

	




	typedef	void (GAPI *   RecvProc)(GIrp *	   pIrp	       ,
					 void *    pContext    ,
				   const void *    pRequestId  ,
				         GResult   dIoResult   ,
				   const void *    pRecvBuf    ,
					 DWord	   dRecvBufLen ,
					 DWord	   dFlags      );

	void		       Recv	(GIrp *	   pIrp	       ,
					 RecvProc  pRecvProc   ,
					 void *	   pContext    ,
				   const void *    pRequestId  ,
					 ISerial * pISerial    ,
				   const ISerial::BusAddress * ,
				   const void *	   pSendBuf    ,
					 DWord	   dSendBufLen ,
					 DWord	   dFlags      );
protected :

#pragma pack (_M_PACK_VPTR)

	struct RecvRequest : public GIrp_Request
	{
	};

#pragma pack ()


virtual	void		     OnRecv	(GIrp *);

static	void GAPI	     OnRecv	(GIrp *, void *, const void *);

virtual	GIrp *	    XxxxIoDp_OnRecv	(GIrp *, GIrp_DpContext, XReplyHandler *);




/*
virtual	HRESULT	       XUnknown_QueryInterface	    (REFIID, void **);

virtual	void	       XUnknown_FreeInterface	    (void *);
*/
protected :

//virtual	void	    CreateIReceiver



/*
	TXUnknown_Entry <GSerialReceiver>

			    m_XUnknown	 ;

	class ISerialReceiver_Entry : public TInterface_Entry <ISerialReceiver, GSerialReceiver>
	{
	STDMETHOD_ (void,	 XxxxIo_DpRecvReady)(GIrp * pIrp);

	}		    m_IReceiver	 ;
*/
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
void GSerialReceiver::OnRecv (GIrp * pIrp)
{
};

void GAPI GSerialReceiver::OnRecv (GIrp * pIrp, void * pRecv, const void *)
{
    ((GSerialReceiver *) pRecv) ->OnRecv (pIrp);
};

GIrp * GSerialReceiver::XxxxIoDp_OnRecv (GIrp * pIrp, GIrp_DpContext, XReplyHandler * pXReply)
{
    pXReply ->Invoke (pIrp);

    return NULL;
};

/*
void GSerialReceiver::Recv  (GIrp *    pIrp	   ,
			     RecvProc  pRecvProc   ,
			     void *    pContext    ,
		       const void *    pRequestId  ,
			     ISerial * pISerial    ,
		       const ISerial::BusAddress * ,
		       const void *    pSendBuf    ,
			     DWord     dSendBufLen ,
			     DWord     dFlags      )
{
    


    SetCurDpProc    <GSerialReceiver, XReplyHandler *>
		    (pIrp, & GSerialReceiver::XxxxIoDp_OnRecv, this,
    CreateXReplyHandler
		    (pIrp, NULL, OnRecv, this, NULL));
};
*/
//
//[]------------------------------------------------------------------------[]

/*

void SomeProc (GReplyPort *, const void * pBuf, DWord dBufSize)
{
    GIrp * pIrp = CreateIrp ();

    SetCurDpProc     (pIrp, XxxxIoDp_OnSocket, 
    CreateDpHandler  (pIrp, GetCur_Apartment (), 


    Xfer  (pIrp, pBuf, dBufSize
};











*/


//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
class XSerialPort
{
public :

protected :

virtual	void		XxxxIoDp_RequestToSend	(GIrp * pIrp);

virtual	void		XxxxIoDp_DataReady	(GIrp * pIrp);


protected :

	GStreamBuf	    m_;
};
//
//[]------------------------------------------------------------------------[]


//[]------------------------------------------------------------------------[]
//
    interface		ISerialBus;

/*
    DECLARE_INTERFACE_	(ISerialReceiver, IUnknown)
    {
    STDMETHOD_	(void,	   DataReadyForRecv)(GIrp *	      pIrp     ,
				   const void *	      pBuf     ,
					 DWord	      dBufSize ,
    };

    DECLARE_INTERFACE_	(ISerial, IUnknown)
    {
    STDMETHOD_	(void,		   Xfer)(GIrp *	      pIrp     ,
					 ISerialBus * pReply   ,
				   const void *	      pBuf     ,
					 DWord	      dBufSize ,
					 DWord	      dFlags   ) PURE;
    };
*/

    DECLARE_INTERFACE_ (XSerial, IUnknown)
    {
	typedef enum
	{
	    streamraw	    = 0,
	    streamoob	    = 1,
	    stream	    = 2,
	    packet	    = 3,
	    packetpartial   = 4,

	}	DataType;

    DECLARE_INTERFACE_ (XSend  , IUnknown)
    {
    STDMETHOD_	(void,		 CancelXfer)(XReplyHandler *	   ) PURE;

    STDMETHOD_	(void,		       Xfer)(XReplyHandler *	   ,
					     GIrp *	pSndIrp	   ,
				       const void *	pSndBuf	   ,
					     DWord	dSndBufLen ,
					     DWord	dSndFlags  ) PURE;
    };

#pragma pack (_M_PACK_VPTR)

	struct Send_Request : public GIrp_Request
	{

	    Send_Request *  Init (XReply *  pXReply	,
				  void   *  pSndBuf	,
				  DWord	    dSndBufSize	)
	    {
		return this;
	    };
	};

	struct Recv_Request : public GIrp_Request
	{
	    GIrp_ReplyHandler *	hReply	   ;
	    void *		pBuf	   ;
	    DWord		dBufSize   ;

	    Recv_Request *  Init (GIrp_ReplyHandler * 
					    hReply  ,
				  void *    pBuf    ,
				  DWord	    dBufSize)
	    {
		GIrp_Request::Init (0);

		this ->hReply	= hReply   ;
		this ->pBuf	= pBuf	   ;
		this ->dBufSize = dBufSize ;

		return this;
	    };
	};

#pragma pack ()

    STDMETHOD_	(void,	       QueueForSend)(GIrp_ReplyHandler *
							pXReply    ,
					     void *	pSndBuf	   ,
					     DWord	dSndBufSize) PURE;  // Connect

    STDMETHOD_	(void,	       QueueForRecv)(GIrp_ReplyHandler *
							XReply	   ,
					     void *     pRcvBuf	   ,
					     DWord	dRcvBufSize) PURE; // Accept or Listen
    };

    template <typename T>
    class TISerial_Entry : public TInterface_Entry <XSerial, T>
    {
    public :
		 XDispatch *	GetXDispatch () const { return NULL;};

    STDMETHOD_	(void,	       QueueForSend)(XReply *	pXReply    ,
					     GIrp *	pIrp	   ,
					     void *	pBuf	   ,
					     DWord	dBufSize   )
    {
	SetCurRequest (pIrp,
	CreateRequest
       <Send_Request> (pIrp) ->Init  (pXReply, pBuf, dBufSize));

	SetCurCoProc  
	<T, void *>   (pIrp, & T::ISerial_QueueForSend, this, NULL);
		    
	GetXDispatch  () ->QueueIrpForComplete (pIrp, False);
    };

    STDMETHOD_	(void,	       QueueForRecv)(XReply * 	pXReply	 ,
					     GIrp *	pIrp	 ,
					     void *     pBuf	 ,
					     DWord	dBufSize )
    {
	SetCurRequest (pIrp,
	CreateRequest
       <Recv_Request> (pIrp) ->Init (pXReply, pBuf, dBufSize));

	SetCurCoProc
       <T, void *>    (pIRp, & T::ISerial_QueueForRecv, this, NULL);

	GetXDispatch  () ->QueueIrpForComplete (pIrp, False);
    };
    };


//
//[]------------------------------------------------------------------------[]

#if FALSE

    template <typename I>
    interface TXProxy_Entry : public I
    {
//	   TInterface_Entry () : m_cRefCount (0), m_oDst (NULL), m_pIUnknown (NULL) {};

	STDMETHOD  (	     QueryInterface)(REFIID riid, void ** pi)
	{
	    return m_pIUnknown ->QueryInterface (riid, pi);
	};

	STDMETHOD_ (ULONG,	     AddRef)()
	{
	    return  ++ m_cRefCount;
	};

	STDMETHOD_ (ULONG,	    Release)()
	{
	    if (0 < -- m_cRefCount)
	    {
		return m_cRefCount.GetValue ();
	    }
		GIUnknown_Entry * 
		pIUnknown = m_pIUnknown		;
			    m_pIUnknown = NULL	;
			    m_oDst	= NULL	;

		pIUnknown ->FreeInterface_Entry (this);

		pIUnknown ->Release ();

		return 0;
	};

//	I *	Init (T * oDst, GIUnknown_Entry * pIUnknown)
//	{
//	    m_oDst = oDst; ++ m_cRefCount; ++ (m_pIUnknown = pIUnknown) ->m_cRefCount; return this;
//	};

	    TInterlocked <ULONG>    m_cRefCount	;
	    TInterlocked <ULONG>    m_cRefCount2;
	    GIUnknown_Entry *	    m_pIUnknown ;
//	    LRESULT 
//	   (STDMETHODCALLTYPE *	    m_pInvokeProc)(TInterface_Entry <IUnknown> &, void * pDst

//	    void *		    m_oDst	;
//	    FOO_PMF		    m_tDst
    };


    template <typename T>
    void STDMETHODCALLTYPE	TISerial_QueueForSend	(TXProxy_Entry <ISerial> * 
								  pX	     ,
							 XReply * pXReply    ,
							 GIrp *   pSndIrp    ,
							 void *   pSndBuf    ,
							 DWord    dSndBufSize)
    {
	SetCurRequest	       (pSndIrp,
	CreateRequest
       <ISerial::Send_Request> (pSndIrp) ->Init (pXReply    , 
						 pSndIrp    , 
						 pSndBuf    ,
						 pSndBufSize));

    };

#endif




/*
    template <typenaclass T>
    GFORCEINLINE T *	alloc (GIrp * pIrp, const T * = NULL)
    {
	return (T *) pIrp ->alloc (sizeof (T));
    };
*/

//[]------------------------------------------------------------------------[]
//

#if FALSE


class GSerialReceiver
{
protected :
			GSerialReceiver ();

		       ~GSerialReceiver ();

virtual	HRESULT		QueryInterface		(REFIID, void **);

protected :

virtual	void		IXDispatch_Invoke	(GIrp *);

virtual	void		IRecv_OnRecv		(GIrp *, const GDataPack * ,
							 const GDataPack * );

//	TIUnknown_Entry 
//	<GSerialReceiver>		m_IUnknown ;

//	DECLARE_IUNKNOWN_ENTRY	(GSerialReceiver)   m_IUnknown  ;
//
//	DECLARE_INTERFACE_ENTRY	(GSerialReceiver, ISerial::IRecv, TInterface_Entry <GSerialReceiver, ISerial::IRecv>)
//						    m_IRecv	;
//

protected :

	XDispatch *	    m_pIXDispatch ;
};


HRESULT GSerialReceiver::QueryInterface (REFIID iid, void ** pi)
{
    return ERROR_SUCCESS;
};

void GSerialReceiver::IRecv_OnRecv (GIrp * pIrp, const GDataPack * pDataId ,
						 const GDataPack * pData   )
{
/*
    switch (GetCurRequest <GIrp_Request> (pIrp) ->CtrlCode)
    {
	case :

	    memcpy (... pIrp, pCurRequest ..., ...);



	case :
    };
*/
#if FALSE

0)
    if (...)
    {
	CompleteIrp	(pIrp, NULL);
    }
    else
    if (...)
    {
	
    }
    else
    {
    LockAcquire ();

	m_XDtrQueue.append (pIrp);

    LockRelease ();
    }

1)
    SetReleaseCoProc	(pIrp, & m_IUnknown);

    SetCurCoProc  <GSerialReceiver>
			(pIrp, ...);

    m_pIXDispatch ->QueueIrpForComplete (pIrp, False);

1)
    SetReleaseCoProc	(pIrp, & m_IUnknown);

    SetCurRequest	(
    CreateRequest <...> (pIrp, ...));

    SetCurCoProc  <GSerialReceiver>
			(pIrp, ...);

    m_pIXDispatch ->QueueIrpForComplete (pIrp, False);

#endif
};
//
//[]------------------------------------------------------------------------[]


#endif







//[]------------------------------------------------------------------------[]
//
class GFileHandle
{
public :

		GResult			Close	()
		{
		    return (m_pIIo ->pCloseProc)(* this);
		};

		GOverlapped::IoStatus	Write
				       (DWord	dOffset	    ,
					DWord	dOffsetHigh ,
				  const void *	pBuf	    ,
					DWord	dBufSize    ,
					GOverlapped & o	    )
		{
		    return (m_pIIo ->pWriteProc)(* this, dOffset, dOffsetHigh, pBuf, dBufSize, o);
		};

		GOverlapped::IoStatus	Read
				       (DWord	dOffset	    ,
					DWord	dOffsetHigh ,
				        void *	pBuf	    ,
					DWord	dBufSize    ,
					GOverlapped & o	    )
		{
		    return (m_pIIo ->pReadProc )(* this, dOffset, dOffsetHigh, pBuf, dBufSize, o);
		};

		GOverlapped::IoStatus	IoControl
				       (DWord	dCtrlCode   ,
					void *	pInpBuf	    ,
					DWord	dInpBufSize ,
					void *	pOutBuf	    ,
					DWord	dOutBufSize ,
					GOverlapped & o	    )
		{
		    return (m_pIIo ->pIoCtrlProc)(* this, dCtrlCode, pInpBuf, dInpBufSize, pOutBuf, dOutBufSize, o);
		};

		GOverlapped::IoStatus   Lock
				       (DWord	dOffset	    ,
					DWord	dOffsetHigh ,
					DWord	dLength	    ,
					DWord	dLengthHigh ,
					Bool	bExclusive  ,
					GOverlapped & o	    )
		{
		    return (m_pIIo ->pLockProc )(* this, dOffset, dOffsetHigh, dLength, dLengthHigh, bExclusive, o);
		};

protected :

	typedef	GResult		       (GAPI * CloseProc)
				       (GFileHandle &	    );

	typedef GOverlapped::IoStatus  (GAPI * WriteProc)
				       (GFileHandle &	    ,
					DWord   dOffset     ,
					DWord   dOffsetHigh ,
				  const void *  pBuf	    ,
					DWord   dBufSize    ,
					GOverlapped &	    );

	typedef GOverlapped::IoStatus  (GAPI *  ReadProc)
				       (GFileHandle &	    ,
				        DWord   dOffset     ,
					DWord   dOffsetHigh ,
					void *  pBuf	    ,
					DWord   dBufSize    ,
					GOverlapped &	    );

	typedef GOverlapped::IoStatus  (GAPI *	IoCtrlProc)
				       (GFileHandle &	    ,
					DWord	dCtrlCode   ,
					void *	pInpBuf	    ,
					DWord	dInpBufSize ,
					void *	pOutBuf	    ,
					DWord	dOutBufSize ,
					GOverlapped &	    );

	typedef GOverlapped::IoStatus  (GAPI *  LockProc)
				       (GFileHandle &	    ,
				        DWord   dOffset     ,
					DWord   dOffsetHigh ,
					DWord   dLength     ,
					DWord   dLengthHigh ,
					Bool    bExclusive  ,
					GOverlapped &	    );

#pragma pack (_M_PACK_VPTR)

	typedef struct IIo_tag
	{
	    CloseProc	pCloseProc  ;
	    WriteProc	pWriteProc  ;
	    ReadProc	pReadProc   ;
	    IoCtrlProc	pIoCtrlProc ;
	    LockProc	pLockProc   ;

	}   IIo ;

#pragma pack ()

		GFileHandle (const IIo * pIIo) : m_pIIo (pIIo) {};

private :

static	GResult		      GAPI close     (GFileHandle &);

static	GOverlapped::IoStatus GAPI write     (GFileHandle &,
					      DWord , DWord,
					const void *, DWord, GOverlapped &);

static	GOverlapped::IoStatus GAPI read	     (GFileHandle &,
					      DWord , DWord,
					      void *, DWord, GOverlapped &);

static	GOverlapped::IoStatus GAPI ioControl (GFileHandle &,
					      DWord ,
					      void *, DWord,
					      void *, DWord, GOverlapped &);

static	GOverlapped::IoStatus GAPI lock	     (GFileHandle &,
					      DWord , DWord, 
					      DWord , DWord, Bool, GOverlapped &);
private :

	const IIo *	    m_pIIo	 ;
	IIoCoPort *	    m_pIIoCoPort ;
	void	  *	    m_hFile	 ;
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
/*
GResult GAPI GFileHandle::Close (GFileHandle &)
{
    return ERROR_CALL_NOT_IMPLEMENTED;
};

GOverlapped::IoStatus GAPI GFileHandle::Write (GFileHandle & h	 ,
					       DWord  dOffset	 ,
					       DWord  dOffsetHigh,
					 const void *		 ,
					       DWord		 ,
					       GOverlapped & o	 )
{
	   o.StartIo	   (dOffset, dOffsetHigh, h.);

    return o.GetIoCoStatus (ERROR_CALL_NOT_IMPLEMENTED, NULL);
};

GOverlapped::IoStatus GAPI GFileHandle::Read  (GFileHandle &,
					       DWord , DWord,
					       void *, DWord, GOverlapped & o)
{};

GOverlapped::IoStatus GAPI GFileHandle::Lock  (GFileHandle &,
					       DWord , DWord,
					       DWord , DWord, Bool, GOverlapped &)
{};
*/
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
    class GIoDeviceHandle;

#define	IIO_MJ_CLOSE	0
#define	IIO_MJ_WRITE	1
#define	IIO_MJ_READ	2
#define	IIO_MJ_IOCTRL	3

#define IIO_MJ_SEND	4
#define	IIO_MJ_RECV	5

/*
    DECLARE_INTERFACE	(IIoBase)
    {
	STDMETHOD_ (GResult,	      QueryInterface)(REFIID iid, void ** pi) PURE;

	STDMETHOD_ (void,		       Close)() PURE;
    };
*/

    DECLARE_INTERFACE	(IIoBase)
    {
#pragma pack (_M_PACK_VPTR)

	struct IoRequest
	{
	      DWord		dCtrlId	    ;
	      DWord		dResult	    ;
	      GOverlapped *	o	    ;

	      IoRequest	       (DWord CtrlId, GOverlapped * po) :

				dCtrlId (CtrlId),
				dResult (0     ), o (po)
	      {};
	};

	struct WrRequest : public IoRequest
	{
	      DWord		dOffset	    ;
	      DWord		dOffsetHigh ;
	const void *		pBuf	    ;
	      DWord		dBufSize    ;

	      WrRequest	       (DWord  dOffset	   ,
				DWord  dOffsetHigh ,
			  const void * pBuf	   ,
				DWord  dBufSize	   ,
				GOverlapped * o	   ) : IoRequest (IIO_MJ_WRITE, o)
	      {
		   this ->dOffset     = dOffset	    ;
		   this ->dOffsetHigh = dOffsetHigh ;
		   this ->pBuf	      = pBuf	    ;
		   this ->dBufSize    = dBufSize    ;
	      }
	};

	struct RdRequest : public IoRequest
	{
	      DWord		dOffset	    ;
	      DWord		dOffsetHigh ;
	      void *		pBuf	    ;
	      DWord		dBufSize    ;

	      RdRequest	       (DWord  dOffset	   ,
				DWord  dOffsetHigh ,
				void * pBuf	   ,
				DWord  dBufSize	   ,
				GOverlapped * o	   ) : IoRequest (IIO_MJ_READ, o)
	      {
		   this ->dOffset     = dOffset	    ;
		   this ->dOffsetHigh = dOffsetHigh ;
		   this ->pBuf	      = pBuf	    ;
		   this ->dBufSize    = dBufSize    ;
	      }
	};

	struct IoXferRequest : public IoRequest
	{
	      DWord		dOffset	    ;
	      DWord		dOffsetHigh ;
	      void *		pBufAddr    ;
	      DWord		pBufAddrSize;
	      void *		pBuf	    ;
	      DWord		dBufSize    ;
	};

	struct IoCtrlRequest : public IoRequest
	{
	      DWord		dIoCtrlCode ;
	      void *		pInpBuf	    ;
	      DWord		dInpBufSize ;
	      void *		pOutBuf	    ;
	      DWord		dOutBufSize ;
	};

#pragma pack ()

	STDMETHOD_ (void,		       Close)() PURE;

	STDMETHOD_ (LRESULT,	   DispatchIoRequest)(GIoDeviceHandle *, IoRequest *) PURE;

	static Bool		     GAPI GrabOverlappedResult
						     (GIoDeviceHandle *, GOverlapped &);

	static GOverlapped::IoStatus GAPI Write	     (GIoDeviceHandle *, DWord	dOffset	    ,
									 DWord  dOffsetHigh ,
								   const void * pBuf	    ,
									 DWord	dBufSize    ,
									 GOverlapped *	    );

	static GOverlapped::IoStatus GAPI Send	     (GIoDeviceHandle *,
								   const void * pBusAddr    ,
									 DWord	dBusAddrSize,
								   const void *	pBuf	    ,
									 DWord  dBufSize    ,
									 GOverlapped *	    );

	static GOverlapped::IoStatus GAPI Read	     (GIoDeviceHandle *, DWord	dOffset	    ,
									 DWord	dOffsetHigh ,
									 void * pBuf	    ,
									 DWord	dBufSize    ,
									 GOverlapped *	    );
	static GOverlapped::IoStatus GAPI Recv	     (GIoDeviceHandle *,
									 void * pBusAddr    ,
									 DWord  dBusAddrSize,
									 void *	pBuf	    ,
									 DWord	dBufSize    ,
									 GOverlapped *	    );
	static GOverlapped::IoStatus GAPI DeviceIoControl 
						     (GIoDeviceHandle *, DWord	dIoCtrlCode ,
									 void *	pInpBuf	    ,
									 DWord	dInpBuf	    ,
									 void *	pOutBuf	    ,
									 DWord	dOutBufSize ,
									 GOverlapped *	    );
    };

class GIoDeviceHandle
{
public :

	typedef void (GAPI *	  CloseProc)(GIoDeviceHandle & );

			GIoDeviceHandle	    () { Init ();};

			GIoDeviceHandle	    (GIoDeviceHandle &);

		       ~GIoDeviceHandle	    () { Close ();};

	GIoDeviceHandle &   operator =	    (GIoDeviceHandle & h)
			    {
				AssignHandle (h.m_hIoDevice  ,
					      h.m_pIIoCoPort ,
					      h.m_pCloseProc );

					      h.Init ();

				return * this;
			    };

	void		    Close	    ()
			    {
				if (m_pCloseProc)
				{
				   (m_pCloseProc)(* this);
				}
			    };

	void *		    GetHandle	    () const { return m_hIoDevice  ;};

	IIoCoPort *	    GetIIoCoPort    () const { return m_pIIoCoPort ;};

protected :

	void		    Init	    ()
			    {
				m_hIoDevice  = NULL ;
			        m_pIIoCoPort = NULL ;
				m_pCloseProc = CloseHandle;
			    };

	GResult		    AssignHandle    (void *	 hIoDevice  ,
					     IIoCoPort * pIIoCoPort ,
					     CloseProc	 pCloseProc )
			    {
				m_hIoDevice  = hIoDevice  ;
				m_pIIoCoPort = pIIoCoPort ;

			    if (m_hIoDevice 
			    && (m_hIoDevice != INVALID_HANDLE_VALUE))
			    {
				m_pCloseProc = pCloseProc ;

				return ERROR_SUCCESS;
			    }
				m_hIoDevice  = NULL	  ;
				m_pCloseProc = CloseHandle;

				return ERROR_INVALID_HANDLE;
			    };

static	void GAPI	    CloseHandle	    (GIoDeviceHandle &);

protected :

	void *			m_hIoDevice  ;
	IIoCoPort *		m_pIIoCoPort ;
	CloseProc		m_pCloseProc ;
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
class GWinFileHandle : public GIoDeviceHandle
{
public :

	GResult		    AssignHandle    (HANDLE hFile, IIoCoPort * pIIoCoPort)
			    {
				return GIoDeviceHandle::AssignHandle ((void *) hFile, pIIoCoPort, 
								      GWinFileHandle::CloseHandle);
			    };
protected :

static	void GAPI	    CloseHandle	(GIoDeviceHandle &);
};
//
//[]------------------------------------------------------------------------[]


//[]------------------------------------------------------------------------[]
//
void GIoDeviceHandle::CloseHandle (GIoDeviceHandle & hDevice)
{
    if (hDevice.m_pIIoCoPort)
    {
	hDevice.m_pIIoCoPort ->Release ();

	hDevice.m_pIIoCoPort = NULL;
    }
    if (hDevice.m_hIoDevice)
    {
	hDevice.m_hIoDevice  = NULL;
    }
};

void GWinFileHandle::CloseHandle (GIoDeviceHandle & hDevice)
{
    HANDLE hFile = (HANDLE) hDevice.GetHandle ();

    GIoDeviceHandle::CloseHandle (hDevice);

    if (hFile)
    {
	::CloseHandle (hFile);
    }
};
//
//[]------------------------------------------------------------------------[]


//[]------------------------------------------------------------------------[]
//
#if FALSE

void GWinFileHandle::IoInterface::Close (GIoDeviceHandle & h)
{
    GIoDeviceHandle GWinFileHandle f = h;

/*
    if (h.m_pIIoCoPort)
    {
	h.m_pIIoCoPort ->Release ();
	h.m_pIIoCoPort = NULL;
    }

    if (h.m_hIoDevice)
    {
	::CloseHandle ((HANDLE) h.m_hIoDevice);
	h.m_hIoDevice  = NULL;
    }
	h.m_pIIo       = NULL;
*/
};

#endif
//
//[]------------------------------------------------------------------------[]


//[]------------------------------------------------------------------------[]
//
GOverlapped::IoStatus GAPI WriteFile (GWinFileHandle & hFile	    ,
				      DWord	       dOffset	    ,
				      DWord	       dOffsetHight ,
				      const void *     pBuf	    ,
				      DWord	       dBufSize	    ,
				      GOverlapped &    o	    )
{
    return GOverlapped::IoCompleted;
};
//
//[]------------------------------------------------------------------------[]

    DECLARE_INTERFACE_	(IXSerial, IUnknown)
    {
/*
    STDMETHOD_	(void,		Connect)(Request      hRequest	  ,
					 DispatchProc pDspProc	  ,
					 void *	      pDspContext ,
					 ISerial

					 int	      cDspArgs	  ,
				   const void *
				   const      *	      pDspArgs	  ) PURE;
*/
    STDMETHOD_	(void,		   Xfer)(
				   const void *	      pBuf	  ,
					 DWord	      dBufSize	  ) PURE;

/*
	typedef void   (STDAPICALLTYPE * RecvCallBack
					(GIrp *	      pIrp	  ,
					 void *	      pContext	  ,
					 GResult      dIoResult	  ,
				   const GDataPack *  pBusAddr	  ,
					 DataType     tBuf	  ,
					 Byte **      pBufHead	  ,
					 Byte *	      pBufTail	  );
*/

/*
    STDMETHOD_	(Request,	Connect)(GIrp *	      pIrp	  ,
					 Request      hParent	  ,
					 ReplyProc    pRplProc	  ,
					 void *	      pRplContext ,
				   const void *	      pRplId	  ) PURE;
*/
/*
	typedef	void   (STDAPICALLTYPE * DispatchProc		  )
					(GIrp *	      pIrp	  ,
					 void *	      pDspContext ,
					 int	      cDspArgs	  ,
				   const void *
				   const      *       pDspArgs	  );
*/
    };


void SomeCall ()
{
/*
    ITrheadPool ->Invoke (IThreadPool ->CreateRequest 
			 (::CreateIrp (), NULL, OnConnect, this),
			 GSerialPort::Connect, m_oDsp, 0 NULL);
*/
};


/*
GIrp * OnRequestToSend (GIrp * pIrp, void *, void *, GIrp_DpParam & pDpParam)
{
    if (...)
    {
	if (ERROR_SUCCESS == (GResult) pDpParam.wParam))
	{
	    
	}
	else
	{
	    pIrp ->SetResultCode (pParam);
	}

	return NULL;
    }
    
	return pDpParam.DispatchDefault (pIrp);
};



void SomeAccept (ISerial * pISerial, GIrp * pIrp)
{
    CreateDpRequest 

	        SetCurDpProc (pIrp, OnRequestToSend, NULL, NULL);

    pISerial ->RequestToSend (pIrp, NULL, malloc (4096), 4096);

    
};


*/








#if FALSE

    DECLARE_INTERFACE_	(ISerialBus, IUnknown)
    {

#pragma pack (_M_PACK_VPTR)

	struct GIrp_RequestToSendIoDpParam
	{
	    XSerial *	    pXSerial ;
	    void *	    pBuf     ;
	    DWord	    dBufSize ;
	};

#pragma pack ()

    STDMETHOD_	(void,	  RequestToSend)(GIrp *	     pIrp     ,
				   const GDataPack * pBusAddr ,
					 void *	     pBuf     ,
					 DWord	     dBufSize ) PURE;	// Connect request

#pragma pack (_M_PACK_VPTR)

	struct GIrp_RequestToRecvIoDpParam
	{
	    XSerial *	    pXSerial ;
	    void *	    pBuf     ;
	    DWord	    dBufSize ;
	};

#pragma pack ()

    STDMETHOD_	(void,	  RequestToRecv)(GIrp *	     pIrp     ,
				   const GDataPack * pBusAddr ,
					 void *	     pBuf     ,
					 DWord	     dBufSize ) PURE;	// Accept  request
    };

#endif

class GSerial
{
};


void SomeCall (XSerial * pXSerial, void * pBuf, DWord dBufLen)
{
    GIrp * pIrp = ::CreateIrp ();

};

/*
    DECLARE_INTERFACE_	(ISerial, IUnknown)
    {
    // ISerial methods
    //
    STDMETHOD_	(void,	  QueueXfer)(GIrp * pIrp, void * pInpBuf,
						  DWord	 dInpBuf,
					    const void * pOutBuf,
						  DWord  dOutBuf) PURE;
    };










*/

//
//[]------------------------------------------------------------------------[]
















//[]------------------------------------------------------------------------[]
//
class GClient
{
public :

	void		    Create	(GIrp *);

};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
class GWinFileIoDevice //: public GIoDevice
{
protected :

	GIrp *	       IoDp_Close	(GIrp *, void *, GIrp_DpContext *);

protected :

	GWinFileHandle		m_hWinFile  ;
};

GIrp * GWinFileIoDevice::IoDp_Close (GIrp * pIrp, void *, GIrp_DpContext *)
{
    m_hWinFile.Close ();

    return pIrp;
};

class GSerialWinFileIoDevice : public GWinFileIoDevice
{
public :

virtual	void		Connect		(GIrp *);

};

void GSerialWinFileIoDevice::Connect (GIrp * pIrp)
{
};
//
//[]------------------------------------------------------------------------[]

#if FALSE


/*
	GOverlapped::IoStatus Close	(GOverlapped &	o	    ,
					 IWaitFor  *	pIoCoWait   ,
					 IIoCoPort *	pIoCoPort   );

virtual	Bool	     GrabIoCoResult	(HIODEVICE	hIoDevice   ,
					 GOverlapped &	o	    ,
					 Bool		bWait	    );

virtual	GOverlapped::IoStatus Write	(HIODEVICE	hIoDevice   ,
					 DWord		dOffset	    ,
					 DWord		dOffsetHigh ,
					 const void *	pOutBuf	    ,
					 DWord		dOutBufSize ,
					 GOverlapped &	o	    ,
					 IWaitFor  *	pIoCoWait   ,
					 IIoCoPort *	pIoCoPort   ) = NULL;

virtual	GOverlapped::IoStatus Read	(HIODEVICE	hIoDevice   ,
					 DWord		dOffset	    ,
					 DWord		dOffsetHigh ,
					 void *		pInpBuf	    ,
					 DWord		dInpBufSize ,
					 GOverlapped &  o	    ,
					 IWaitFor  *	pIoCoWait   ,
					 IIoCoPort *	pIoCoPort   ) = NULL;

virtual	GOverlapped::IoStatus DeviceIoControl	
					(HIODEVICE	hIoDevice   ,
					 DWord		dIoCtrlCode ,
    					 void *		pOutBuf	    ,	// Write part
					 DWord		dOutBufSize ,
					 void *		pInpBuf	    ,	// Read  part
					 DWord		dInpBufSize ,
					 GOverlapped &	o	    ,
					 IWaitFor  *	pIoCoWait   ,
					 IIoCoPort *	pIoCoPort   ) = NULL;
*/

class GWinFile : public GIoDevice
{
public :

	Bool	     GrabIoCoResult	(HIODEVICE	hIoDevice   ,
					 GOverlapped &	o	    ,
					 Bool		bWait	    );

	GOverlapped::IoStatus Write	(HIODEVICE	hIoDevice   ,
					 DWord		dOffset	    ,
					 DWord		dOffsetHigh ,
					 const void *	pOutBuf	    ,
					 DWord		dOutBufSize ,
					 GOverlapped &	o	    ,
					 IWaitFor  *	pIoCoWait   ,
					 IIoCoPort *	pIoCoPort   );

	GOverlapped::IoStatus Read	(HIODEVICE	hIoDevice   ,
					 DWord		dOffset	    ,
					 DWord		dOffsetHigh ,
					 void *		pInpBuf	    ,
					 DWord		dInpBufSize ,
					 GOverlapped &  o	    ,
					 IWaitFor  *	pIoCoWait   ,
					 IIoCoPort *	pIoCoPort   );

	GOverlapped::IoStatus DeviceIoControl	
					(HIODEVICE	hIoDevice   ,
					 DWord		dIoCtrlCode ,
    					 void *		pOutBuf	    ,	// Write part
					 DWord		dOutBufSize ,
					 void *		pInpBuf	    ,	// Read  part
					 DWord		dInpBufSize ,
					 GOverlapped &	o	    ,
					 IWaitFor  *	pIoCoWait   ,
					 IIoCoPort *	pIoCoPort   );

protected :

//	GWinFileHandle		m_hWinFile	;
};

Bool GWinFile::GrabIoCoResult (HIODEVICE     hIoDevice,
			       GOverlapped & o	      ,
			       Bool	     bWait    )
{
    if (! o.GrabIoCoResult ())
    {
	if (m_hCloseLock.LockAcquire ())
	{
//	    WaitOverlappedResult (m_hWinFile.GetHandle (), o);

	    m_hCloseLock.LockRelease ();
	}
	else
	{
	    o.SetIoCoResult (ERROR_INVALID_HANDLE, NULL);
	}
    }

    return False;
};

GOverlapped::IoStatus GWinFile::Write (HIODEVICE     hIoDevice	 ,
				       DWord	     dOffset	 ,
				       DWord	     dOffsetHigh ,
				       const void *  pOutBuf	 ,
				       DWord	     dOutBufSize ,
				       GOverlapped & o		 ,
				       IWaitFor  *   pIoCoWait   ,
				       IIoCoPort *   pIoCoPort	 )
{
    GOverlapped::IoStatus oStatus;

    if (m_hCloseLock.LockAcquire ())
    {
	oStatus = ::WriteFile    (hIoDevice		 ,
				  dOffset, dOffsetHigh   ,
				  pOutBuf, dOutBufSize   ,
				  o, pIoCoWait, pIoCoPort);

	m_hCloseLock.LockRelease ();
    }
    else
    {
	oStatus = ::PerformIo	 (o, pIoCoWait, pIoCoPort, ERROR_INVALID_HANDLE, NULL);
    }
	return oStatus;
};

GOverlapped::IoStatus GWinFile::Read (HIODEVICE	    hIoDevice   ,
				      DWord	    dOffset     ,
				      DWord	    dOffsetHigh ,
				      void *	    pInpBuf     ,
				      DWord	    dInpBufSize ,
				      GOverlapped & o	        ,
				      IWaitFor  *   pIoCoWait	,
				      IIoCoPort *   pIoCoPort	)
{
    GOverlapped::IoStatus oStatus;

    if (m_hCloseLock.LockAcquire ())
    {
	oStatus = ::ReadFile (hIoDevice		     ,
			      dOffset, dOffsetHigh   ,
			      pInpBuf, dInpBufSize   ,
			      o, pIoCoWait, pIoCoPort);

	m_hCloseLock.LockRelease ();
    }
    else
    {
	oStatus = ::PerformIo (o, pIoCoWait, pIoCoPort, ERROR_INVALID_HANDLE, NULL);
    }
	return oStatus;
};

GOverlapped::IoStatus GWinFile::DeviceIoControl 
			       (HIODEVICE     hIoDevice	  ,
				DWord	      dIoCtrlCode ,
    				void *	      pOutBuf	  ,
				DWord	      dOutBufSize ,
				void *	      pInpBuf	  ,
				DWord	      dInpBufSize ,
				GOverlapped & o		  ,
				IWaitFor  *   pIoCoWait   ,
				IIoCoPort *   pIoCoPort	  )
{
    GOverlapped::IoStatus oStatus;

    if (m_hCloseLock.LockAcquire ())
    {
	oStatus = ::DeviceIoControl (hIoDevice		    ,
				     dIoCtrlCode	    ,
				     pOutBuf, dOutBufSize   ,
				     pInpBuf, dInpBufSize   ,
				     o, pIoCoWait, pIoCoPort);

	m_hCloseLock.LockRelease ();
    }
    else
    {
	oStatus = ::PerformIo (o, pIoCoWait, pIoCoPort, ERROR_INVALID_HANDLE, NULL);
    }
	return oStatus;
};


#endif
//
//[]------------------------------------------------------------------------[]


#define	GIRPM_IOSERIAL_CONNECT	    0xC010

#define	GIRPM_NOTIFY		    0xC000

#if FALSE

Bool GAPI OnDispatchIrp (GIrp_Msg & m)
{
    if (GIRPM_IOSERIAL_CONNECT == m.uMsg)
    {
	if (m.DispatchDefault ())
	{
	}
/*
	    QueueIrpForComplete (m.


	    CompleteIrp (m.pIrp);

*/
    }

    return m.DispatchComplete ();
};

#endif




//[]------------------------------------------------------------------------[]
//
    DECLARE_INTERFACE_	(IIoSerialRecv, IUnknown)
    {
    STDMETHOD_ (void,	   Recv)(GIrp *, void *	      pInpBuf	  ,
					 DWord	      dInpBufSize ) PURE;
    };

    DECLARE_INTERFACE_	(IIoSerialSend, IUnknown)
    {
    STDMETHOD_ (void,	   Send)(GIrp *, const void * pOutBuf	  ,
					 DWord	      dOutBufSize ,
					 void *	      pInpBuf	  ,
					 DWord	      dInpBufSize ) PURE;
    };

    DECLARE_INTERFACE_	(IIoSerialPort, IUnknown)
    {
    // IIoSerial methods
    //
    STDMETHOD_	(void,	 Create)(GIrp *, const void * pParam     ,
					 DWord	      dParamSize ,
					 const void * pOutBuf	 ,
					 DWord	      dOutBufSize,
					 void *	      pInpBuf	 ,
					 DWord	      dInpBufSize) PURE;
    };

//
//[]------------------------------------------------------------------------[]




/*

void ServiceManger


void ServiceManager::StartService (GService * pService)
{
    GIrp * pIrp = CreateIrp ();

    pService ->RegisterServiceEntry (this, new ServiceEntry);

    SetCurDpProc <ServiceManger>
};


Bool SomeService_Control (GIrp * pIrp, void *)
{
    RESPONSE_IRPM		(
};

class GApplicatationService : public GServiceManager
{
};

int WinMain ()
{
    if (GWinInit ())
    {
	{{
// 1) Run Thread model

	    GAppService	pApplication;

//
//

// Start SomeService :
//
	    GService *  pService = new SomeService  (...);
	    GIrp *	pIrp	 = CreateIrp ();

	    SetCurCoProc (pIrp, CompleteRun, NULL, pService);

	    pService ->Start (pIrp);

//
//	    ...
//

// Start SomeService2 :
//
			pService = new SomeService2 (...);
			pIrp	 = CreateIrp ();

	    SetCurCoProc (pIrp, CompleteRun, NULL, pService);

	    SetCurDpProc (pIrp, SomeService_Control

	    pService ->Start (pIrp);

	if (UserInterface)
	{

// Start SomeClient GUI
//
	    if (SThreadModel)
	    {
		Application.Close (PrimaryThread <TWinMsgThread <GWinMsgLoop> > (GWinMain, NULL, NULL));

		Application.Close ();
	    }
	    else
	    if (MThreadModel)
	    {
		CreateThread  <TWinMsgThread <GWinMsgLoop> > (GWinMain, NULL, NULL);
	    }
	}
	else
	{
	    if (SThreadModel)
	    {
	    }
	    else
	    if (MThreadModel)
	    {
	    }
	}
	}}

	GWinDone ();
    }
};


*/







































namespace GSerialLine
{
	typedef enum
	{
	    input	= 1,
	    output	= 2,
	    hduplex	= 3,
	    fduplex	= 7

	}   FlowType ;

	typedef enum
	{
	    stream	= 0,
	    packet	   ,

	}   DataType ;
};



/*
    DECLARE_GINTERFACE_	(ISerial, IUnknown)
    {
    // ISerial methods
    //
    STDMETHOD_	(void,	       Send)(GIrp * pIrp, GStream *);
    STDMETHOD_	(void,	       Recv)(GIrp * pIrp, GStream *);
    };


    DECLARE_INTERFACE_	(ISerialBus, IUnknown)
    {
    // ISerialBus methods
    //
    };

    DECLARE_INTERFACE_	(ISerialPort, IUnknown)
    {
    // ISerialPort methods
    //

#pragma pack (_M_PACK_VPTR)

	typedef struct Address_tag
	{
	    Address_tag	*   pAddrHighLo	;
	    

	}   Address ;

#pragma pack ()

    STDMETHOD_	(void,	    Connect)(GIrp * pIrp, const Address *, XSerial **);

    STDMETHOD_	(void,	    Connect)(GIrp * pIrp);
    };

    DECLARE_INTERFACE_	(ISerialReceiver, IUnknown)
    {
    // ISerialReceiver
    //
    STDMETHOD_	(XSerial *,	GetXSerial)() PURE;

    STDMETHOD_	(void,		    OnRecv)(const void * pBuf, DWord dBufSize) PURE;
    };
*/


    DECLARE_INTERFACE_	(IRoentParser, IUnknown)
    {
    STDMETHOD_	(void,		     Parse)(DWord dParamId, const void * pValue,
								  DWord  dValue) PURE;
    };




/*
    DECLARE_GINTERFACE_	(ISerialPort, IUnknown)
    {
    // ISerialPort methods
    //
    STDMETHOD_	(void,	       Send)(GIrp * pIrp, ISerialPort * pIInp,
						  ISerialPort * pIOut) PURE;
    };



    DECLARE_GINTERFACE_	(I485Device, IUnknown)
    {
    // I485Device methods
    //
    };


    DECLARE_GINTERFACE_	(IUrpDevice, IUnknown)
    {
    // IUrpDevice methods
    //
    STDMETHOD_	(int,	 GetAnodeIC)(int
    STDMETHOD_	(int,	 SetAnodeIC)(int
    };


DWord GetRegValue (ISerialPort * pISerial, DWord dParam)
{
    


};



*/







/*
void GAPI GFtdiFile::IoCo_RecvProc (void * pThis, GIrp * pIrp, GOverlapped * o)
{
    if (! o ->HasIoCoResult ())
    {
	o ->
    }


    if (STATUS_SUCCESS == o ->Internal)
    {
    }


    CurStatus <GIrp_Status> (pIrp) ->SetStatusInfoPtr
			    (o ->IoCoResult (), o ->IoCoResultInfo ());
};

void GFtdiFile::Io_RecvProc (GIrp * pIrp)
{
    RecvRequest * rq = CurRequest
   <RecvRequest> (pIrp);

    if (GOverlapped::IoSysQueued > ::FT_W32_ReadFile
		      ((FT_HANDLE) m_hWinFile.GetHandle ()	,
				   rq ->pBufTail		,
				  (rq ->pBufBase + rq ->dBufSize)
						 - rq ->pBufTail,
				   rq ->o.SetIoCoProc
				  (IoCo_RecvProc, NULL, pIrp)   ,
				   rq ->hCoEvent		,
				   m_hWinFile.GetIIoCoPort ()))
    {
	QueueOverlappedResult (rq ->o);
    }
};
*/










//[]------------------------------------------------------------------------[]
//
/*
tmpleate <class P = , class 
class SerialTransport
{
};
*/




/*
GIrp * GSerialFile::Io_Recv (GIrp * pIrp)
{
    RecvRequest * rq = CurRequest 
   <RecvRequest> (pIrp);

    if (m_fCloseLock.LockAcquire ())
    {
	rq ->o.SetIoCoProc (IoCo_RecvProc, this, pIrp);

	Io_RecvProc (rq);

	m_fCloseLock.LockRelease ();

	return NULL;
    }
	CurStatus <GIrp_Status> (pIrp) ->SetStatusInfoPtr
				 (ERROR_HANDLE_EOF, NULL);

	return pIrp;
};


GIrp * GSerialFile::IoCo_Eof  (GIrp * pIrp, void *)
{
    return m_fCloseLock.Close (pIrp);
};

void GAPI GSerialFile::IoCo_Release (GOverlapped *, void * pIrp)
{
    CompleteIrp ((GIrp *) pIrp);
};

GIrp * GSerialFile::IoCo_Close (GIrp * pIrp, void *)
{
    RecvRequest * rq = CurRequest 
   <RecvRequest> (pIrp);

    m_hWinFile.Close ();

    rq ->o.IoAddRef  ();
    rq ->o.IoRelease (IoCo_Release, pIrp);

    return NULL;
};


    SetCurCoProc  <GSerialFile, void *>
		  (pIrp, & GSerialFile::IoCo_Close   , this, NULL);

    SetCurCoProc  <GSerialFile, void *>
		  (pIrp, & GSerialFile::IoCo_Eof     , this, NULL);

    SetCurCoProc  <GSerialFile, void *>
		  (pIrp, & GSerialFile::IoCo_RecvLoop, this, NULL);

    CompleteIrp	  (pIrp);
*/

/*
GIrp * GAPI GSerialFile::IoCo_CallBackLoop (GIrp * pIrp, void *, void * rp)
{
    if (ERROR_SUCCESS == pIrp ->CurResult ())
    {
    }
};


	void * rp      = CreateRequest 
			<RecvCallBackParam> (pIrp) ->Init (tData, rq ->pBufHead,
								  rq ->pBufTail);
	SetCurCoProc   (pIrp, IoCo_CallBackLoop, NULL, rp);

	CompleteIrp    (pIrp);
*/


//
//[]------------------------------------------------------------------------[]

/*
	void GAPI   Transact	(GIrp * pIrp, ISerial * pInpSerail,
					      GStream * pInpStream,
					      ISerial * pOutSerial,
					      GStream * pOutStream);
*/


#if FALSE

class GSerialTransport
{
public :
/*
virtual	void *	    RegisterBindingEntry    (
*/

protected :

static	void GAPI   Io_RecvCallBack	  (GIrp *   pIrp	,
					   void *   pContext    ,
				     const GSerialPort::Addr *
						    pAddr	,
					   GSerialPort::DataType
						    tData	,
					   Byte *&  pBufHead    ,
					   Byte *&  pBufTail    );
};




class GTransactionPort
{
public :

virtual	void	    Send	(GIrp * pIrp, GStream * pInpStream,
					      GStream * pOutStream);


protected :

	GSpinCountCriticalSection   m_PLock ; // Lock for (*)
};
#endif






































/*
virtual	void	    OnIoCo_RecvOOB	(GIrp *	    pIrp      ,
					 Byte *&    pBufHead  ,
					 Byte *&    pBufTail  );

virtual	void	    OnIoCo_Recv		(GIrp *	    pIrp      ,
				   const GSerialFile::Addr *
						    pSrcAddr  ,
					 Bool	    bPacket   ,
					 Byte *&    pBufHead  ,
					 Byte *&    pBufTail  );

static	void GAPI   OnIoCo_Recv	  (	 GIrp *	    pIrp      ,
					 void *	    pContext  ,
				   const GSerialFile::Addr *
						    pSrcAddr  ,
				         GSerialFile::DataType,
					 Byte *&    pBufHead  ,
					 Byte *&    pBufTail  );
protected :

	GSpinCountCriticalSection
			    m_PLock	   ;

*/
//	BindingEntry	    m_BindingEntry ;






/*
#pragma pack (_M_PACK_VPTR)

	struct BindingEntry
	{
	    TSLink <BindingEntry, 0>	SLink	  ;
	    TXList <BindingEntry,
	    TSLink <BindingEntry, 0> >	SList	  ;

	    TInterlocked <ULONG>	cRefCount ;

	    void	       (GAPI *	pReleaseProc )(BindingEntry *);
	    IoCo_RecvCallBack  (GAPI *	pGetRecvProc )(BindingEntry *,
						       const void   *,
							     void  **);
	    void	AddRef	()
			{
				     ++ cRefCount ;
			};

	    void	Release ()
			{
			    if (0 <  -- cRefCount)
			    {
				return;
			    }
			    if (pReleaseProc)
			    {
			       (pReleaseProc)(this);
			    }
			};
	};

#pragma pack ()
*/

//[]------------------------------------------------------------------------[]
//
namespace GP_Protocol
{
#pragma pack (_M_PACK_BYTE)

	typedef struct
	{
	    _U16	uHeaderVer  ;	// +  0 - Bigendian 'GP' code
//	    _U16	u

	}   PacketHeader ;

#pragma pack ()
}
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
#define	  G5_FLAG_FST	    0x01	// First message packet
#define	  G5_FLAG_LST	    0x02	// Last  message packet
#define	  G5_FLAG_PCS	    0x04	// Valid uPacketCS field

namespace G5_Protocol
{
#pragma pack (_M_PACK_BYTE)

	typedef	struct
	{
	    _U32	uSecNoSof   ;	// +  0 - Bigendian head seqN of the received data
	    _U32	uSecNoEof   ;	// +  4 - Bigendian tail seqN of the received data

	}   AckPacket ;

	typedef struct
	{
	    _U16	uHeaderVer  ;	// +  0 - Bigendian 'G5' code
	    _U16	uHeaderCS   ;	// +  2 - Bigendian Header check sum if not 0
	    _U8		uHeaderSize ;	// +  4 - Sizeof header in _U32 words
	    _U8		uFlags	    ;	// +  5 -
	    _U16	uPacketCS   ;	// +  6 - Raw data check sum if G5_FLAG_PCS set
	    _U16	uPacketSize ;	// +  8 - Bigendian Packet size in bytes
//	    _U16	uMsgId	    ;	// + 10 -
//	    _U32	uSrcAddr    ;	// + 12 -
//	    _U32	uDstAddr    ;	// + 16 -

	union
	{
	    _U32	uMsgSize    ;	// + 20 - if ( G5_FLAG_FST) Bigendian message size in bytes
	    _U32	uSecNo	    ;	// + 20 - if (!G5_FLAG_FST) Bigendian packet offset inside the message in bytes
	};
					// + 24	- 
//	    AcqPacket	[(uHeaderSize * sizeof (_U32) - sizeof (PacketHeader)) 
//						      / sizeof (AckPacket   )];
	}   PacketHeader ;
					//	- Raw data.
#pragma pack ()

	typedef enum
	{
	    Success	= 0,
	    Underflow	= 1,
	    BadCS	= 2,
	    BadFrame	= 3,

	}   TranslateCode ;


//	    void	    ParsePacket	    (Word
//
//	    void	    ParseStream

	    TranslateCode   ProcessPacket   (Word  &	wHeaderSize ,
					     Bool	bPacket	    ,
					     Byte *&	pBufHead    ,
					     Byte *&	pBufTail    ,
					     Bool	bNoBadError = True);
}
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
namespace G5_Protocol
{

static _U16 CalcCS (_U16 curcs, const Byte * pBufHead,
				const Byte * pBufTail)
{
    DWord  cs	      =  curcs;
    size_t tWordCount =	 pBufTail - pBufHead;

    for (; 1 < tWordCount; tWordCount -= 2, pBufHead += 2)
    {
	cs += _GetBigEndianW (pBufHead, 0);
    }
    if (   0 < tWordCount)
    {
	cs += (((Word) * (pBufHead + 0)) << 8);
    }
	cs  = ((DWord)(HIWORD(cs))) + ((DWord)(LOWORD(cs)));
	cs += ((DWord)(HIWORD(cs)));

	return (_U16) cs;
};

static TranslateCode HasHeader (Word & wHeaderSize, Byte *& pBufHead, Byte *& pBufTail)
{
    if (sizeof (PacketHeader) > (pBufTail - pBufHead))
    {
	return Underflow;
    }
    if (('G' != * pBufHead) || ('5' != * (pBufHead + 1)))
    {
	return BadFrame;
    }

    PacketHeader *   pHeader =
   (PacketHeader *&) pBufHead;

		    wHeaderSize	 = ((Word) pHeader ->uHeaderSize) * sizeof (_U32);

    if ((pBufHead + wHeaderSize) > pBufTail)
    {
	return Underflow;
    }

    if ((pHeader ->uHeaderCS) && (0 != ~CalcCS (0, pBufHead,
						   pBufHead + wHeaderSize)))
    {
	return BadCS   ;
    }
	return Success ;
};

static TranslateCode HasPacket (Bool bPacket, Word  & wHeaderSize,
					      Byte *& pBufHead	 ,
					      Byte *& pBufTail	 )
{
    TranslateCode tCode = HasHeader (wHeaderSize, pBufHead, pBufTail);

    if (Success < tCode)
    {
	return tCode   ;
    }
	Word   wPacketSize = _GetBigEndianW (pBufHead, offsetof (PacketHeader, uPacketSize));

    if ((pBufTail - pBufHead) < wPacketSize)
    {
	return bPacket ? BadFrame : Underflow;
    }

    PacketHeader *   pHeader =
   (PacketHeader *&) pBufHead;

    if ((pHeader ->uFlags & G5_FLAG_PCS) && (0 != ~CalcCS (0, pBufHead + ((Word) pHeader ->uHeaderSize) * sizeof (_U32),
							      pBufHead + wPacketSize)))
    {
	return BadCS   ;
    }
	return Success ;
};

TranslateCode ProcessPacket (Word  & wHeaderSize,
			     Bool    bPacket    ,
			     Byte *& pBufHead   ,
			     Byte *& pBufTail   ,
			     Bool    bNoBadError)
{
    TranslateCode    tCode;

    for (;Underflow < (tCode = HasPacket (bPacket, wHeaderSize, pBufHead, pBufTail));)
    {
	if (bNoBadError)
	{
	    if (bPacket)
	    {
		pBufHead = pBufTail ;
	    }
	    else
	    {
		pBufHead ++;
	    }
		continue;
	}
    }
	return  tCode;
};

}
//
//[]------------------------------------------------------------------------[]

#if FALSE

//[]------------------------------------------------------------------------[]
//
class GTransactionPort
{
public :


	typedef	void (GAPI *	IoCo_DispRecvProc)(void *  pContext ,
						   GIrp *  pIrp	    ,
						   Bool    bMessage ,
						   Byte *& pBufHead ,
						   Byte *& pBufTail );

	struct BindingEntry
	{
	    void	       (GAPI *	m_pReleaseProc)(BindingEntry *	 );

	    BindingEntry *     (GAPI *	m_pGetEntryFor)(BindingEntry *	 ,
						  const void *  pKey	 );

	    void	       (GAPI *	m_pProcessRecv)(BindingEntry *	 ,
						        GIrp *  pIrp	 ,
							Bool    bMessage ,
							Byte *& pBufHead ,
							Byte *	pBufTail );

	    void		AddRef	()
				{
				    ++ m_cRefCount ;
				};

	    void		Release ()
				{
				    if (0 < m_cRefCount)
				    {
					return;
				    }
				       (m_pReleaseProc)(this);
				};



	    TSLink <BindingEntry, 0>	SLink	;
	    TXList <BindingEntry,
	    TSLink <BindingEntry, 0> >	SList	;


	    GSerialFile::Addr	m_pAddr		   ;
	    IoCo_DispRecvProc	m_pDispProc	   ;
	    void *		m_pDispProcContext ;


	    TInterlocked
	   <ULONG>	    m_cRefCount	;
	};


protected :

virtual		    IoCo_DispRecvProc

		Get_IoCo_DispRecvProc	  (
				     const GSerialPort::Addr *	,
					   void *&  pContext	);

virtual	void	    IoCo_DispRecvOOB	  (GIrp *   pIrp	,
					   Byte *&  pBufHead	,
					   Byte *&  pBufTail	);

static	void GAPI   IoCo_RecvCallBack	  (GIrp *   pIrp	,
					   void *   pContext    ,
				     const GSerialPort::Addr *
						    pAddr	,
					   GSerialPort::DataType
						    tData	,
					   Byte *&  pBufHead    ,
					   Byte *&  pBufTail    );
protected :

	GSpinCountCriticalSection	m_PLock	;
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//

//
//[]------------------------------------------------------------------------[]


#endif




//[]------------------------------------------------------------------------[]
//
class IUnknown_Class : public IUnknown
{
public :
	STDMETHOD    (	     QueryInterface)(REFIID iid, void ** pi);
	STDMETHOD_   (ULONG,	     AddRef)();
	STDMETHOD_   (ULONG,	    Release)();

private :

	TInterlocked <ULONG>	m_cRefCount ;
	void *	    (GAPI *	m_pDsp)(int iMethod, void * pContext  ,
						     void * pArgument );
	void *			m_oDsp	    ;
	FOO_PMF			m_tDsp	    ;
};

//[]------------------------------------------------------------------------[]

HRESULT IUnknown_Class::QueryInterface (REFIID iid, void ** pi)
{

    return ERROR_SUCCESS;
};

ULONG IUnknown_Class::AddRef ()
{
    return  ++ m_cRefCount;
};

ULONG IUnknown_Class::Release ()
{
    if (1 < -- m_cRefCount)
    {
	return m_cRefCount.GetValue ();
    }
	return 0;
};


/*

    if (ERROR_SUCCESS == (dResult = pISerialPort ->Transact
			 (pBuf, dOutBufSize  , 
			  pBuf, sizeof (pBuf))))
    {
	pISerialPort ->Relase ();

	pISerialPort = NULL	;
    }
*/

IUnknown * SomeProc ()
{
    return new IUnknown_Class;
};


#if FALSE
class GWinFile
{
public :

private :


//	STDMETHOD_
/*
static	void GAPI	ISerialPort_Send    (GWinFile *, const void * pBuf     ,
							 DWord	      dBufSize ,
							 GOverlapped &	       );
*/

protected :

//	void		Write		(const void *


protected :

	    GWinFileHandle		m_hWinFile  ;

private :
};
#endif



















//[]------------------------------------------------------------------------[]
//
class GConnector
{
public :
/*
	typedef	enum
	{
	    input	= 1,
	    output	   ,
	    duplex	   ,

	}	 BoundType ;

	typedef enum
	{
	    stream	= 0,
	    packet	   ,

	}	StreamType ;
*/
	typedef enum
	{
	    closed	= 0,	// -> creating	| starting		| ready
	    failed	= 1,	// -> closed
	    stopped	= 3,	// -> closed
	    creating	= 2,	// -> failed	| starting		| ready
	    stopping	= 4,	// -> stopped
	    starting    = 5,	// ->					| ready
	    paused	= 6,	// ->		  stopping		| ready
	    ready	= 7,	// -> 		  stopping  | paused

	}	     State ;


public :

//	const void *	GetId		() const { return m_pUserId; };

//	BoundType	GetBoundType	() const { return m_tBound ; };

//	StreamType	GetStreamType	() const { return m_tStream; };

//	State		GetState	() const { return m_tState.GetValue ();};

protected :

	    GConnector	()
			{
/*
			    m_pUserId	= pUserId ;
			    m_tBound	= duplex  ;
			    m_tStream	= stream  ;

			    m_tState	.Init (closed);
*/
			};
protected :

	GSerialLine::FlowType	    m_tFlowType	 ;
	GSerialLine::DataType	    m_tDataType	 ;

	GCloseLock		    m_fCloseLock ;
};
/*
    DECLARE_GINTERFACE_ (IService, IUnknown)
    {
    // IService
    //
    STDMETHOD_	(void,			 Listen)(GOverlapped &)		PURE;

/*
    STDMETHOD_	(void,	    QueueIrpForComplete)(GThread::Type, GIrp *)	PURE;
    STDMETHOD_	(HWAKEUP,	   CreateWakeUp)()			PURE;
    STDMETHOD_	(HWTIMER,	    CreateTimer)()			PURE;
    STDMETHOD_	(GResult,	 CreateIoCoPort)(IIoCoPort **)		PURE;
*/
//    };

/*

void * CreateSessionIrp (GOverlapped &)
{
    


};



*/

class GNetworkAcceptor
{
protected :

#ifdef _MSC_BUG_C2248
public :
#endif

#pragma pack (_M_PACK_VPTR)

	struct AcceptRequest : public GIrp_Request
	{
	    AcceptRequest * Init	()
	    {
		GIrp_Request::Init (0x0000);
    
		return this;
	    };
	};

	struct AcceptReply : public GIrp_Request
	{
	    AcceptReply	*   Init	()
	    {
		GIrp_Request::Init (0x0000);

		return this;
	    };
	};

#pragma pack ()

protected :
//
//----------------------------------------------------------------------------
//
virtual	void		Io_ListenProc	(GIrp *) = NULL;

	GIrp *		Io_ListenLoop	(GIrp *, void *);

virtual	GIrp *		Dp_Listen	(GIrp *, XReply *, GIrp_DpContext *);
//
//----------------------------------------------------------------------------
//
	GCloseLock	    m_fCloseLock ;
};

/*
GIrp * GAPI GNetworkAcceptor::IoCo_Close (GIrp * pIrp, void *, void *)
{
    AcceptRequest * rq = CurRequest
   <AcceptRequest> (pIrp);

	rq ->hWinFile.Close ();

	return NULL;
};
*/

GIrp * GNetworkAcceptor::Dp_Listen (GIrp * pIrp, XReply * hReply, GIrp_DpContext * dc)
{
    return (hReply ->OnReply (pIrp, NULL), NULL);
};

GIrp * GNetworkAcceptor::Io_ListenLoop (GIrp * pIrp, void *)
{
    AcceptRequest * rq	 = GetCurRequest
   <AcceptRequest> (pIrp);

//	     rq ->hWinFile.Close ();

    if (m_fCloseLock.LockAcquire ())
    {
	SetCurCoProc  <GNetworkAcceptor, void *>
		      (pIrp, & GNetworkAcceptor::Io_ListenLoop, this, NULL);

	Io_ListenProc (pIrp);

	m_fCloseLock.LockRelease ();

	return NULL;
    }

	CurStatus <GIrp_Status> (pIrp) ->SetStatusInfoPtr
				(ERROR_HANDLE_EOF,  NULL);
        return pIrp;
};

/*
void GNetworkAcceptor::Listen (GIrp * pIrp)
{
    SetCurCoProc <GNetworkAcceptor, void *>
		 (pIrp, & GNetworkAcceptor::Io_ListenLoop, this, NULL);

    CompleteIrp  (pIrp);
};
*/
//
//[]------------------------------------------------------------------------[]


//[]------------------------------------------------------------------------[]
//
void GWinPipe::CreatePipeName (LPTSTR pPipeName, LPCSTR pName)
{
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
class GWinPipeAcceptor : public GNetworkAcceptor
{
public :
		    GWinPipeAcceptor	();
//		   ~GWinPipeAcceptor	();

	GResult		Create		(GSerialLine::FlowType
						     tFlowType	   ,
					 Bool	     bPacket	   ,
					 LPCTSTR     pName	   ,
					 DWord	     dWrBufSize = 0,  // _M_PAGE_SIZE default
					 DWord	     dRdBufSize = 0); // _M_PAGE_SIZE default

	void 		QueueForListen	(XReplyHandler *, Bool bRepeat);

protected :

	GResult		CreateInstance  (HANDLE &, Bool bFirstInstance = False);

	void		Close		();

private :

// For (1300 > _MSC_VER) compatible !!! ???
//
	struct ParentAcceptRequest : public GNetworkAcceptor::AcceptRequest {};
	struct ParentAcceptReply   : public GNetworkAcceptor::AcceptReply   {};
//
// For (1300 > _MSC_VER) compatible !!! ???

#ifdef _MSC_BUG_C2248
public :
#endif

#pragma pack (_M_PACK_VPTR)

	struct AcceptRequest : public ParentAcceptRequest
	{
	    GWinFileHandle	hWinFile ;

	    HANDLE		hCoEvent ;
	    GOverlapped		o	 ;

	    AcceptRequest * Init	()
			    {
				this ->hWinFile.AssignHandle (NULL, NULL);
				this ->hCoEvent	    = NULL;
				this ->o.Init ();

				return this;
			    };

	static void GAPI    Destroy	(void * pThis)
			    {
				if (((AcceptRequest *) pThis) ->hCoEvent)
				{
				    ::CloseHandle
				   (((AcceptRequest *) pThis) ->hCoEvent);
				}
			    };
	};

	struct AcceptReply : public ParentAcceptReply
	{
	    GWinFileHandle	hWinPipe  ;

	    GResult		dIoResult ;

	    AcceptReply *   Init	(GResult	  dIoResult ,
					 GWinFileHandle & hWinPipe  )
			    {
				this ->dIoResult = dIoResult ;
				this ->hWinPipe	 = hWinPipe  ;

				return this;
			    };

	static void GAPI    Destroy     (void * pThis)
			    {
				((AcceptReply *) pThis) ->hWinPipe.Close ();
			    };
	};

#pragma pack ()
//
//----------------------------------------------------------------------------
//

protected :

	GIrp *		XxxxIoDp_Listen (GIrp *, GIrp_DpContext, GIrp_ReplyHandler * rp);

	void		XxxxIoCo_Listen	(GIrp *);

static	void GAPI	XxxxIoCo_Listen	(GOverlapped &, void * pThis, void * pIrp)
			{
			    ((GWinPipeAcceptor *) pThis) ->XxxxIoCo_Listen  ((GIrp *) pIrp);
			};

	void		Io_ListenProc	(GIrp *);

	GIrp *		Io_ListenLoop	(GIrp *, void *);

static	void GAPI	Io_CancelListen (GIrp *, GAccessLock *, void *);

private :

	HANDLE			m_hPipe		 ;
	
	DWord			m_dPipeOpenMode  ;
	DWord			m_dPipeMode	 ;

	DWord			m_dPipeWrBufSize ;
	DWord			m_dPipeRdBufSize ;

	TCHAR			m_pPipeName [MAX_PATH];
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
#if    (0x0501 <= WINVER)
#ifndef FILE_FLAG_FIRST_PIPE_INSTANCE
#define	FILE_FLAG_FIRST_PIPE_INSTANCE	0x00080000
#endif
#else
#define	FILE_FLAG_FIRST_PIPE_INSTANCE	0x00000000
#endif

GWinPipeAcceptor::GWinPipeAcceptor ()
{
    m_hPipe	     = NULL;
    m_dPipeOpenMode  =
    m_dPipeMode	     = 0;

    m_dPipeWrBufSize = 0;
    m_dPipeRdBufSize = _M_PAGE_SIZE;

  * m_pPipeName	     = 0;
};

GResult GWinPipeAcceptor::Create (GSerialLine::FlowType
					      tFlowType	 ,
				  Bool	      bPacket	 ,
				  LPCTSTR     pName	 ,
				  DWord	      dWrBufSize ,
				  DWord	      dRdBufSize )
{
    if (! m_fCloseLock.Open ())
    {
    return ERROR_ALREADY_EXISTS;
    }

    switch (tFlowType)
    {
	case GSerialLine::input :
	    m_dPipeOpenMode  = PIPE_ACCESS_INBOUND  ;
	    break;

	case GSerialLine::output :
	    m_dPipeOpenMode  = PIPE_ACCESS_OUTBOUND ;
	    break;

	case GSerialLine::hduplex :
	case GSerialLine::fduplex :
	default :
	    m_dPipeOpenMode  = PIPE_ACCESS_DUPLEX   ;
	    break;
    };
	    m_dPipeOpenMode |= FILE_FLAG_OVERLAPPED ;

	    m_dPipeMode	     = (bPacket) ? (PIPE_TYPE_MESSAGE | PIPE_READMODE_MESSAGE)
					 : (PIPE_TYPE_BYTE    | PIPE_READMODE_BYTE   );

	    m_dPipeWrBufSize = (_M_PAGE_SIZE < dWrBufSize) ? dWrBufSize : _M_PAGE_SIZE;
	    m_dPipeRdBufSize = (_M_PAGE_SIZE < dRdBufSize) ? dRdBufSize : _M_PAGE_SIZE;

	    GWinPipe::CreatePipeName 
	   (m_pPipeName	     , pName);

    return ERROR_SUCCESS;
};

GResult GWinPipeAcceptor::CreateInstance (HANDLE & hPipe, Bool bFirstInstance)
{
	hPipe = ::CreateNamedPipe   (m_pPipeName      ,
 (bFirstInstance ? FILE_FLAG_FIRST_PIPE_INSTANCE : 0) |
				     m_dPipeOpenMode  ,
				     m_dPipeMode      ,
			     PIPE_UNLIMITED_INSTANCES ,
				     m_dPipeWrBufSize ,
				     m_dPipeRdBufSize ,
			     NMPWAIT_USE_DEFAULT_WAIT ,
						NULL );

    if (hPipe == INVALID_HANDLE_VALUE)
    {
	hPipe  = NULL ;

	return ::GetLastError ();
    }
	return ERROR_SUCCESS;
};

/*
//1--------
//
GIrp * GAPI XDispatch_QueueIrpForCompleteCoProc (GIrp * pIrp, void *, (void *) pXDispatch)
{
    ((XDispatch *) pXDispatch) ->QueueIrpForCompleteCoProc (pIrp, False);

    return NULL;
};
//
//1--------
*/

GIrp * GWinPipeAcceptor::XxxxIoDp_Listen (GIrp * pIrp, GIrp_DpContext, GIrp_ReplyHandler * rp)
{
    return NULL;
/*
//1--------
//
			GetTCurrentXDispatch () ->AddRef ();
    SetCurCoProc (pIrp, XDispatch_QueueIrpForCompleteCoProc,
			NULL,
			GetTCurrentXDispatch ());
    rq ->OnReply (pIrp);


    XReply_OnReply (rq

    rp ->pXReply ->OnReply (pIrp, rq);

    return NULL;
//
//1--------

//2--------
//
    rp ->pXReply ->QueueIrpForComplete (pIrp, 





    XReplyPort () ->CreateReply (pIrp, ...


//
//2--------
*/
};

void GWinPipeAcceptor::XxxxIoCo_Listen (GIrp * pIrp)
{
    AcceptRequest * rq = GetCurRequest
   <AcceptRequest> (pIrp);

    if (  ! rq ->o.GrabIoCoResult ())
    {
	if (m_fCloseLock.LockAcquire ())
	{
	    GrabOverlappedResult      (
	    rq ->hWinFile.GetHandle (),
	    rq ->o	      , False );

	    m_fCloseLock.LockRelease ();
	}
	else
	{
	    GrabOverlappedResult (
	    NULL, 
	    rq ->o	     , False);
	}
    }

    SetCurRequest (pIrp,
    CreateRequest
   <AcceptReply>  (pIrp) ->Init (rq ->o.GetIoCoResult (), rq ->hWinFile), AcceptReply::Destroy);

    DispatchIrp	  (pIrp);
};

void GAPI GWinPipeAcceptor::Io_CancelListen (GIrp * pIrp, GAccessLock * pCancelLock, void *)
{
    AcceptRequest * rq = GetCurRequest
   <AcceptRequest> (pIrp);

    pCancelLock ->LockRelease ();

    rq ->hWinFile.Close ();
};

void GWinPipeAcceptor::Io_ListenProc (GIrp * pIrp)
{
    AcceptRequest * rq = GetCurRequest
   <AcceptRequest> (pIrp);

    GOverlapped::IoStatus oStatus ;
    GResult		  dResult ;

			  rq ->o.SetIoCoProc 
			 (XxxxIoCo_Listen, this, pIrp);
    HANDLE  hPipe;

    if (ERROR_SUCCESS == 
       (dResult	       =  CreateInstance (hPipe, False)))
    {
	if (True/*SetCancelProc (pIrp, NULL, Io_CancelListen, NULL)*/)
	{
	    oStatus    =  ::ConnectNamedPipe 
			 (rq ->hWinFile.GetHandle    (),
			  rq ->o		       ,
			  NULL			       ,
			  rq ->hWinFile.GetIIoCoPort ());
	}
	else
	{
	    oStatus    =  ::PerformIo
			 (rq ->o		       ,
			  NULL			       ,
			  rq ->hWinFile.GetIIoCoPort (),
			  ERROR_OPERATION_ABORTED, NULL);
	}
    }
    else
    {
	    oStatus    =  ::PerformIo
			 (rq ->o		       ,
			  NULL			       ,
			  rq ->hWinFile.GetIIoCoPort (),
			  dResult, NULL		       );
    }

    if (GOverlapped::IoSysQueued == oStatus)
    {
	return;
    }
	rq ->o.QueueIoCoStatus (oStatus, True);
};


GIrp * GWinPipeAcceptor::Io_ListenLoop (GIrp * pIrp, void *)
{
    AcceptRequest * rq	 = GetCurRequest
   <AcceptRequest> (pIrp);

    if (m_fCloseLock.LockAcquire ())
    {
	SetCurCoProc  <GWinPipeAcceptor, void *>
		      (pIrp, & GNetworkAcceptor::Io_ListenLoop, this, NULL);

/*
	GetCurXDispatchRequest (
//	SetCurCoProc  (pIrp, XDispatch
//
//
*/

	Io_ListenProc (pIrp);

	m_fCloseLock.LockRelease ();

	return NULL;
    }

	CurStatus <GIrp_Status> (pIrp) ->SetStatusInfoPtr
				(ERROR_HANDLE_EOF,  NULL);
        return pIrp;
};

/*
GIrp * ThisClass::OnWinPipeAcceptorReply (GIrp * pIrp, const void *)
{
    GASSERT (0x0000 == GetCurRequest <GIrp_Request> (pIrp) ->GetCtrlCode ())

    GWinPipeAcceptor::AcceptReply * rp = GetCurRequest
   <GWinPipeAcceptor::AcceptReply> (pIrp);

    if (ERROR_SUCCESS == rp ->dIoResult)
    {{
	CreateRequest <GIrp_Request>
	....
	pIrp = .....


	new GSerialPort <GWinPipe> (...);

	pIrp = NULL;
    }}
    else
    {
	if (pIrp.Cancelled ())
	{
	}
    }

    return pIrp;
};

{
//---------------------------------

    m_WinPipeAcceptor.Create ("\\\\.\\pipe\\RoentDevice, ...);

    m_WinPipeAcceptor.QueueForListen 
   (m_hPipeAcceptor = CreateReplyHandler <ThisClass> (CreateIrp (), & m_WinPipeAcceptor, ThisClass::OnWinPipeAcceptorReply, this));

    ...

    if (m_hAcceptor)
    {
	m_hAcceptor ->Close (ERROR_CANCELED);
	m_hAcceptor = NULL;
    }
//---------------------------------

    m_WinSockAcceptor.Create ("tcp\\192.168.0.1:5002"

    m_WinSockAcceptor.QueueForListen
   (m_hSockAcceptor = CreateReplyHandler <ThisClass> (CreateIrp (), ThisClass::OnWinSockAcceptorReply, this));

    ...

    if (m_hSockAcceptor)
    {
	m_hSockAcceptor ->Close (ERROR_CANCELLED);
	m_hSockAcceptor = NULL;
    }
//---------------------------------

    m_FtdiPort.Create ("19200:8,n,2");


    pIrp = CreateIrp ();

    SetCurDpProc       (pIrp, ThisClass::XxxxIoDp_FtdiReply, this,
    CreateReplyHandler <ThisClass>
		       (pIrp, GetCur_IApartment (), ThisClass::OnFtdiReply, 










    SetCurDpProc       <ThisClass, GIrpReplyHandler *>
		       (CreateIrp, & This


    CreateDpProc       <ThisClass>

    CreateReplyHandler <ThisClass> 
   (pIrp = CreateIrp (), ThisClass::OnFtdiPortReply, ThisClass::OnDpProc(ThisClass::) this);

    CreateReplyDpHandler <ThisClass>
   (pIrp = CreateIrp (), ThisClass


    m_FtdiPort.QueueForCreate

 








}
*/


void GWinPipeAcceptor::QueueForListen (XReplyHandler * hReply, Bool bRepeat)
{
    GIrp_ReplyHandler *  rp   = 
   (GIrp_ReplyHandler *) hReply;

    GIrp * pIrp = CreateIrp ();

    SetCurRequest  (pIrp,
    CreateRequest
   <AcceptRequest> (pIrp) ->Init (), AcceptRequest::Destroy);

    SetCurDpProc   <GWinPipeAcceptor, GIrp_ReplyHandler *> 
		   (pIrp, & GWinPipeAcceptor::XxxxIoDp_Listen, this, rp);

    SetCurCoProc   <GWinPipeAcceptor, void *>
		   (pIrp, & GWinPipeAcceptor::Io_ListenLoop, this, NULL);

    CompleteIrp	   (pIrp);

//  GetTCurrentXDispatch () ->CompleteNextIrp (pIrp);
//
//
};

/*
void GWinPipeAcceptor::QueueForClose (XReply::Request hReply)
{
    GIrp_ReplyHandler *  rp   =
   (GIrp_ReplyHandler *) hReply;

    GIrp *		 pIrp = (rp) ? rp ->pIrp.Exchange (NULL) : NULL;

    if (NULL == pIrp)
    {
	return;
    }
};
*/

/*
GWinPipeAcceptor * SomeCreator (GIrp * pIrp)
{
    return new (pIrp, (GWinPipeAcceptor **) NULL) GWinPipeAcceptor ();
};
*/
//
//[]------------------------------------------------------------------------[]


/*
Bool ThisService::Dp_PipeListen (GIrp_Msg & m)
{
    GIrp * pIrp;

    if (DPIRP_WINPIPE_ACCEPTED == m.uMsg)
    {
	pIrp = m.ExtractDispIrp ();

	QueueIrpForComplete	(pIrp, ...);

	return True;
    }
	return m.DispatchDefault ();
};

Bool ThisService::Dp_TcpListen (GIrp_Msg & m)
{
    GIrp * pIrp;

    if (DPIRP_WINSOCK_ACCEPTED == m.uMsg)
    {
	pIrp = m.ExtractDispIrp ();

	QueueIrpForComplete	(pIrp);

	return True;
    }
	reutrn m.DispatchDefault ();
};

GResult ThisService::Start (GIrp * pIrp, int argc, const void * const * argv)
{
    GIrp *   pIrp;
//...
	     pIrp = CreateIrp ();

			    pIUnknown ->AddRef ();
    SetReleaseCoProc (pIrp, pIUnknown);

    SetCurDpProc <ThisObject>
	    (pIrp, & ThisService::Dp_PipeListen, this);

   (m_pWinPipeAcceptor = new (pIrp, (GWinPipeAcceptor *) NULL) GWinPipeAcceptor ()) ->
    Listen  (pIrp, _T("RoentDeviceService"));

//...
	     pIrp = CreateIrp ();

			    pIUnknown ->AddRef ();
    SetReleaseCoProc (pIrp, pIUnknown);

    SetCurDpProc <ThisObject>
	    (pIrp, & ThisService::Dp_TcpListen, this);

   (m_pTcpSockAccpetor = new (pIrp, (GTcpWinSockAcceptor *) NULL) GTcpWinSockAcceptor ()) ->
    Listen  (pIrp, "127.0.0.1:50005");
};


*/







class SomeService
{
public :

	void		Close	    (GIrp *);

	GIrp *		Co_StartService	    (GIrp *, TInterlockedPtr <GCloseLock *> *);

	void		StartService	    (GIrp *);

	void		StopServiceRequest  (IUnknown *);


protected :

	TInterlockedPtr
       <GCloseLock *>	    m_pCloseLock;
};


void SomeService::StopServiceRequest (IUnknown * pIUnknown)
{
// (2)
//
    GCloseLock * clock;

    if ((NULL != (clock = m_pCloseLock.Exchange (NULL)))
    &&		  clock ->CloseAcquire ())
    {{
	GIrp * pIrp = ::CreateIrp   ();

	if (pIUnknown)
	{
				      pIUnknown ->AddRef ();

	    SetReleaseCoProc   (pIrp, pIUnknown);
	}
	    clock ->QueueForClose (pIrp);
    }}
};

GIrp * SomeService::Co_StartService (GIrp * pIrp, TInterlockedPtr <GCloseLock *> * clockPtr)
{
    GCloseLock * clock;

    if (NULL != (clock = clockPtr ->Exchange (NULL)))
    {
    }

    return pIrp;
};

void SomeService::StartService (GIrp * pIrp)
{
// (1)
//
    m_pCloseLock = new (pIrp, (GCloseLock **) NULL) GCloseLock;

    SetCurCoProc <SomeService, TInterlockedPtr <GCloseLock *> *>
		 (pIrp, & SomeService::Co_StartService, this, & m_pCloseLock);
};

#if FALSE

void StopSerivce (GIrp * pIrp)
{
};

void * SomeListen (GIrp * pIrp, GWinPipeAcceptor * pAcceptor)
{
//    SetCurCoProc <SomeEndPoint, void *>


/*
    pAcceptor ->Listen (NULL, NULL, NULL,
			GSerialLine::input | GSerialLine::output | GSerialLine::fduplex, GSerialLine::packet, 
			_T("\\\\.\\pipe\\RoentDeviceService"), 0, 0);
*/
    return NULL;
};

#endif

//[]------------------------------------------------------------------------[]
/*
GIrp * GWinPipeAcceptor::ListenCoProc (GIrp * pIrp, void *)
{
    if (pIrp ->Cancelled ())
    {
	CurStatus <GIrp_Status> (pIrp) ->SetStatus (ERROR_CANCELLED);
    }

    if (ERROR_SUCCESS == 
	CurStatus <GIrp_Status> (pIrp))
    {
//	...
//
    }
    else
    {
	m_hWinPipe.Close ();
    }
	return pIrp;
};

static void GAPI Cancel_ConnectNamedPipe (GIrp * pIrp, GAccessLock * pLock, void * pWinPipe)
{
    GWinFileHandle  hWinPipe = * (GWinFileHandle *) pWinPipe;

    pLock ->LockRelease ();

	hWinPipe.Close  ();
};

static void GAPI IoCo_ConnectNamedPipe (GOverlapped * o, void *, GIrp * pIrp, Bool)
{
    DefAccessLock ().LockAcquire ();

	ClearCancelProc (pIrp);

    DefAccessLock ().LockRelease ();

	CurStatus <GIrp_Status> (pIrp) ->SetStatus (o ->CoResult ());

	CompleteIrp (pIrp);
};

static GIrp * GAPI queueIrpForComplete (GIrp * pIrp, GIrp_DispatchQueue * pQueue, void *)
{
    if (pQueue)
    {
	pQueue ->QueueIrpForDispatch (pIrp);

	return NULL;
    }
	return pIrp;
};

void GWinPipeAcceptor::ListenPerform (GIrp * pIrp)
{
	GResult		  dResult;

    if (ERROR_SUCCESS == (dResult = CreateInstance (True)))
    {
	SetCurCoProc  <GWinPipeAcceptor, void *>
		      (pIrp, &  GWinPipeAcceptor::ListenCoProc, this, NULL);

	SetCurCoProc  (pIrp, (GIrp_CoProc) queueIrpForComplete, TCurrentCoQueue ()NULL, (void *) CoWork);

    DefAccessLock ().LockAcquire ();

	SetCancelProc (pIrp, & DefAccessLock (),   Cancel_ConnectNamedPipe, & m_hWinPipe);

    DefAccessLock ().LockRelease ();

	ConnectNamedPipe (m_hWinPipe.GetHandle	  () ,
			  m_oListen .SetIoCoProc  (IoCo_ConnectNamedPipe, NULL, pIrp),
			  NULL			     ,
			  m_hWinPipe.GetIIoCoPort ());
	return;
    }
	CurStatus <GIrp_Status> (pIrp) ->SetStatus (dResult);

	CompleteIrp (pIrp);
};
*/
//[]------------------------------------------------------------------------[]




    struct GIrp_Accept_Status : public GIrp_Status
    {


    };

/*

GIrp * BioScanService::CoPipeAccept (GIrp * pIrp, void *)
{
    m_WinPipeAcceptor.Listen (pIrp,
};

void BioScanService::StartPipeAccept (GIrp * pIrp)
{
    if (pIrp == NULL)
    {
	pIrp =  new (NULL) GIrp ();
    }

    SetCurCoProc <BioScanSerivce, void *>
		 (pIrp, 
};


void BioScanService::StartProc (GIrp * pIrp)
{
    GIrp * pIrp = new (NULL) GIrp ();

    SetCurCoProc <BioScanService, void *>
		 (pIrp, & BioScanService::CoPipeAccept, this, NULL;


    m_WinPipeAcceptor.Listen (


    SetDispProc  (pIrp) ->Init ()
    {
    }


    SetCurStatus <GIrp_Status> (pIrp
    CreateStatus <GIrp_Status> (pIrp) ->Init ());

    SetCurCoProc (



    a.Listen (pIrp);

};

    



*/



























//[]------------------------------------------------------------------------[]
//

/*
void GAPI SomeCoHandler (void *  pContext, ISerial::CoRequest & CoRequest);
{
    if (CoRequest.Status == ISerial::Ready)
    {

	return
    }


    switch (CoRequest.StatusISerial::CoReason)
    {
	case ISerial::LineStatus :
	    break;

	case ISerial::
    };
};

void SomeProc ()
{
    GWinPipeAcceptor * m_pWinPipeAcceptor = new GWinPipeAcceptor (...,

    ISerial * pISerial = NULL;;

    if (ERROR_SUCCESS == GWinPipe::CreateInstance (& pISerial))
    {
	pISerial ->RegisterCoHandler (SomeCoHandler, this);
    } 


    if (ERROR_SUCCESS == GWinPipeAcceptor::CreateInstance (& pISerial))
    {
    }
};



*/





/*

    DECLARE_GINTERFACE_ (ISerial, IUnknown)
    {
	typedef	enum
	{
	    closed	= 0,

	    RequestToSend

	}	     Status    ;

	typedef struct
	{
	    Status   m_Status  ;

	    GResult  dIoResult ;

	}	     CoRequest ;

	typedef void (GAPI * RecvProc )(void * pContext, DWord );
//	typedef void (GAPI * 

	typedef	void (GAPI * CoHandler)(void * pContext, CoRequest);

    // ISerialPort methods
    //	
    STDMETHOD_   (GResult,     RegisterCoHandler)(	CoHandler,
						 void * pContext );
    };
*/

/*

    DECLARE_GINTERFACE_ (IOutStream, IUnknown)
    {
    // IOutStream methods
    //
    STDMETHOD_	(void,			CleanUp)()			PURE;
    STDMETHOD_	(void,	    QueueIrpForComplete)(GThread::Type, GIrp *)	PURE;
    STDMETHOD_	(HWAKEUP,	   CreateWakeUp)()			PURE;
    STDMETHOD_	(HWTIMER,	    CreateTimer)()			PURE;
    STDMETHOD_	(GResult,	 CreateIoCoPort)(IIoCoPort **)		PURE;
    };

    DECLARE_GINTERFACE_ (IInpStream, IUnknown)
    {
    // IInpStream methods
    //
    STDMETHOD_	(void,			CleanUp)()			PURE;
    STDMETHOD_	(void,	    QueueIrpForComplete)(GThread::Type, GIrp *)	PURE;
    STDMETHOD_	(HWAKEUP,	   CreateWakeUp)()			PURE;
    STDMETHOD_	(HWTIMER,	    CreateTimer)()			PURE;
    STDMETHOD_	(GResult,	 CreateIoCoPort)(IIoCoPort **)		PURE;
    };
*/
//
//[]------------------------------------------------------------------------[]









#if FALSE

#define	GWINFILE_FLAG_OVERLAPPED	0x00000001

#define	GWINFILE_FLAG_STREAM		0x0000
#define	GWINFILE_FLAG_MESSAGE		0x0004

class GWinFileHandle
{
public :

	typedef enum OverlappedMode_tag
	{
	    Sync		= 0,
	    Overlapped		   ,
	    OverlappedEx

	} OverlappedMode;

			GWinFileHandle	();
//
// Valid only this scheme Create/CloseHandle
//
// ____|-Create-|_______________________________
//
// _____________________|---Close---|___________
//
// |-Write-|-Write-|-Write-|-Write-|-Write-|-Wri
//
// |-Read-|-Read-|-Read-|-Read-|-Read-|-Read-|-R


	void		AttachHandle	(HANDLE, IIoCoPort * );

	HANDLE		DetachHandle	(	 IIoCoPort *&);

virtual	void		Dummy		(GOverlapped &			  ,
					 GIoCoProc	pIoCoPorc	  ,
					 void *		pIoCoProcContext  ,
					 void *		pIoCoProcArgument ,
					 HANDLE		hEvent		  ,
					 HWAKEUP	hWakeUp		  );

virtual Bool		GetCompletionStatus
					(GOverlapped &, DWord * pIoResult     = NULL,
							DWord * pIoResultInfo = NULL);
protected :

	Bool		AcquireCloseLock    ();

	void		ReleaseCloseLock    ();


	Bool		StartIo		(GOverlapped & o, HANDLE);

	Bool		IsQueuedIo	(		  Bool	  bQueued     ,
							  DWord	  dResult     );

	void		QueueIo		(GOverlapped & o, Bool	  bQueued     ,
							  DWord	  dResult     ,
							  DWord	  dTransfered ,
							  HWAKEUP hWakeUp     );

static	void GAPI	WakeUpIoCoProc	    (HWAKEUP, GWakeUpReason, void *, void *);

protected :

	    HANDLE		        m_hFile	     ;
	    IIoCoPort *		        m_pIIoCoPort ;

	    TInterlockedPtr <HANDLE>    m_hClose     ;
	    TInterlocked    <int>       m_cClose     ;
	    TInterlocked    <int>	m_cIoPending ;
};


GWinFileHandle::GWinFileHandle ()
{
    m_hFile	 = NULL;
    m_pIIoCoPort = NULL;

    m_hClose	 .Init (INVALID_PTR);
    m_cClose	 .Init (0);
    m_cIoPending .Init (0);
};

Bool GWinFileHandle::AcquireCloseLock ()
{
	HANDLE   hClose;

        m_cClose ++;

    if ((hClose = m_hClose.GetValue ()) == NULL)
    {
	return 1;
    }
    if ((hClose)		 == INVALID_PTR)
    {
	m_cClose --;
    }
    else
    {
	ReleaseCloseLock ();
    }
	return 0;
};

void GWinFileHandle::ReleaseCloseLock ()
{
    if (-- m_cClose == 0)
    {
	m_pIIoCoPort = NULL;
	m_hFile	     = NULL;

	::SetEvent     (m_hClose);
    }
};

void GWinFileHandle::AttachHandle (HANDLE hFile, IIoCoPort * pIIoCoPort)
{
// GASSERT this has not attached handle
//
	m_hFile	     = hFile ;
	m_pIIoCoPort = pIIoCoPort;

	m_cClose ++;
	m_hClose      .Exchange (NULL);
};

HANDLE GWinFileHandle::DetachHandle (IIoCoPort *& pIIoCoPort)
{
// GASSERT previouse AttachHandle was called
//
    HANDLE  hResume ;

    HANDLE  hClose	= m_hClose.Exchange
	   (hResume	= ::OpenResume () );
    HANDLE  hFile	= m_hFile	   ;
	    pIIoCoPort  = m_pIIoCoPort	   ;

	ReleaseCloseLock ();

	WaitForAny (1, hResume, INFINITE);

    if (hResume != (hClose = m_hClose.CompareExchange
			      (INVALID_PTR, hResume)))
    {
	::SetEvent (hClose);
    }
			  ::CloseResume (hResume);
    return  hFile;
};
//
//[]------------------------------------------------------------------------[]











//[]------------------------------------------------------------------------[]
//
class GConnector
{
public :

	typedef enum State_tag
	{
	    closed	= 0,	// -> creating	| starting		| ready
	    failed	= 1,	// -> closed
	    stopped	= 3,	// -> closed
	    creating	= 2,	// -> failed	| starting		| ready
	    stopping	= 4,	// -> stopped
	    starting    = 5,	// ->					| ready
	    paused	= 6,	// ->		  stopping		| ready
	    ready	= 7,	// -> 		  stopping  | paused

	}	     State ;
};

class GNetworkAcceptor : public GConnector
{
public :
		    GNetworkAcceptor	(const void *	    ,
					 int   nInstancesMax) :


	typedef GIrp *	(GAPI * Co_AcceptCallBack)(void *  pContext ,
						   GIrp *	    ,
						   GWinFileHandle   )


	void		Listen		(GIrp *









//			m_pAcceptingIrp (NULL  ),
			m_State	        (closed) {};

	void		Cancel		();

	State		ExchangeState	(State s)	   { return m_State.Exchange (s);};

	State	 CompareExchangeState	(State s, State c) { return m_State.CompareExchange (s, c);};

private :
	    GIrp_CancelPtr 		m_pAcceptingIrp	;
	    TInterlocked <State >	m_State		;

protected :
	    TInterlocked <int>		m_nInstances	;
	    int				m_nInstancesMax	;
};







//[]------------------------------------------------------------------------[]
//
class GWinPipeAcceptor //: public GNetworkAcceptor
{
public :

      GWinPipeAcceptor	(const void *,
			 int nMaxInstances = PIPE_UNLIMITED_INSTANCES);

	    GResult	Create	    (LPCTSTR	pName	      ,
				     DWord	dWrBufSize = 0,	    // _M_PAGE_SIZE default
				     DWord	dRdBufSize = 0)	    // _M_PAGE_SIZE default
	    {
			Create	    (		pName	      ,
						dWrBufSize    ,	    // nOutBufferSize
						dRdBufSize    ,	    // nInpBufferSize

				  ((0			      |
				      PIPE_ACCESS_DUPLEX      |
			    //	      PIPE_ACCESS_INBOUND     |
			    //	      PIPE_ACCESS_OUTBOUND    |
				    0)			      |
				   (0			      |
			    //	      FILE_FLAG_WRITE_THROUGH |
				    0)			      |
				   (0			      |
			    //	      WRITE_DAC		      |
			    //	      WRITE_OWNER	      |
			    //	      ACCESS_SYSTEM_SECURITY  |
				    0)),			    // dOpenMode

				  ((0			      |
				      PIPE_TYPE_BYTE	      |	    // with PIPE_READMODE_BYTE
			    //	      PIPE_TYPE_MESSAGE	      |	    // with PIPE_READMODE_BYTE || PIPE_READMODE_MESSAGE
				    0)			      |
				   (0			      |
				      PIPE_READMODE_BYTE      |	    // for PIPE_TYPE_MESSAGE || PIPE_TYPE_BYTE
			    //	      PIPE_READMODE_MESSAGE   |	    // for PIPE_TYPE_MESSAGE
				    0)),			    // dPipeMode

				      1000, NULL);		    // WaitNamedPipe server timeout, SecAttrib
	    };

	    GResult	Create	    (LPCTSTR    pName	      ,
				     DWord	dWrBufSize    ,
				     DWord	dRdBufSize    ,
				     DWord	dOpenMode     ,
				     DWord	dPipeMode     ,
				     DWord	dDefTimeout   ,
				     PSECURITY_ATTRIBUTES     );
protected :

	    void	Bind	    (LPCTSTR    pName	      ,
				     DWord	dWrBufSize    ,
				     DWord	dRdBufSize    ,
				     DWord	dOpenMode     ,
				     DWord	dPipeMode     ,
				     DWord	dDefTimeout   ,
				     PSECURITY_ATTRIBUTES
						pSecAttrib    )
	    {
		::lstrcpyn 
	       (m_pName, pName, sizeof (m_pName)/sizeof (TCHAR));

		m_dWrBufSize    = (_M_PAGE_SIZE < dWrBufSize) ? dWrBufSize : _M_PAGE_SIZE;
		m_dRdBufSize    = (_M_PAGE_SIZE < dRdBufSize) ? dRdBufSize : _M_PAGE_SIZE;

		m_dOpenMode     = dOpenMode  | FILE_FLAG_OVERLAPPED ;
		m_dPipeMode     = dPipeMode  | PIPE_WAIT	    ;
		m_dDefTimeout   = dDefTimeout			    ;
		m_pSecAttrib	= pSecAttrib			    ;
	    };

//virtual	void		GetCreationParams    (CreationParams & 

public :
/*
virtual	GResult		Create	    (LPCTSTR	pName	      ,
				     );
*/

	    void	Accept	    (GIrp *	pAcceptingIrp ,
				     LPCTSTR	pName	      ,
				     DWord	dWrBufSize    ,
				     DWord	dRdBufSize    ,
				     DWord	dOpenMode     ,
				     DWord	dPipeMode     ,
				     DWord	dDefTimeout   );
/*

void SomeProc ()
{
    if (ERROR_SUCCESS == GWinPipeAcceptor.Create (...))
    {
    }
};


*/
protected :

	GResult		CreateInstance	    (Bool	bFirstInstance,
					     PSECURITY_ATTRIBUTES     );

	void	      IoCoAccept    (		      Bool bSync, DWord  dIoCoStatus	 ,
								  GIrp *		 );

static	void	GAPI  IoCoAccept    (GOverlapped & o, Bool bSync, DWord  dIoCoStatus	 ,
								  DWord  dIoCoStatusInfo ,
								  void * pIoCoContext	 ,
								  void * pIoCoArgument   )
			{
			    ((GWinPipeAcceptor *) pIoCoContext) ->IoCoAccept
			    (bSync, dIoCoStatus, (GIrp *) pIoCoArgument);
			};

private :
//	    TInterlockedPtr <GIrp *>	m_pAccepingIrp	;
//	    GWinFileHandle		m_PipeHandle	;

	    DWord			m_dWrBufSize	;
	    DWord			m_dRdBufSize	;

	    DWord			m_dOpenMode	;
	    DWord			m_dPipeMode	;

	    DWord			m_dDefTimeout	;
	    PSECURITY_ATTRIBUTES	m_pSecAttrib	;

	    GOverlapped			m_oAccepting	;

	    TCHAR			m_pName [MAX_PATH];
};
//
//[]------------------------------------------------------------------------[]


//[]------------------------------------------------------------------------[]
//
GWinPipeAcceptor::GWinPipeAcceptor (const void * pId, int nInstancesMax) :

		  GNetworkAcceptor (pId, nInstancesMax)
{
//  Bind (NULL, 
};

#if    (_WIN32_WINXP <= _WIN32_WINNT)

#ifndef FILE_FLAG_FIRST_PIPE_INSTANCE
#define	FILE_FLAG_FIRST_PIPE_INSTANCE	0x00080000
#endif
#else
#define	FILE_FLAG_FIRST_PIPE_INSTANCE	0x00000000
#endif

/*
    SECURITY_ATTRIBUTES *
		pSecAttr  = NULL;
    SECURITY_ATTRIBUTES	    sa	;
    union
    {
    SECURITY_DESCRIPTOR	    saDcr;
    Byte		    saDcrBuf [SECURITY_DESCRIPTOR_MIN_LENGTH];
    };

    if (pSecAttr == NULL)
    {
						    // Set NULL DACL to allow anyone to connect
						    // to the pipe.
	sa.nLength		      = sizeof (sa);
	sa.lpSecurityDescriptor	      = & saDcr;
	::InitializeSecurityDescriptor (& saDcr, SECURITY_DESCRIPTOR_REVISION);
	::SetSecurityDescriptorDacl    (& saDcr, True, NULL, False);
	sa.bInheritHandle	      = False;

	pSecAttr = & sa ;
    }
*/
GResult GWinPipeAcceptor::CreateInstance (Bool		       bFirstInstance, 
					  PSECURITY_ATTRIBUTES pSecAttrib    )
{
    LPCTSTR	pPipeName = NULL;
    HANDLE	hPipe     = NULL;

    if (INVALID_HANDLE_VALUE == 
       (hPipe		      = ::CreateNamedPipe
				(   pPipeName	 ,
				  m_dOpenMode | ((bFirstInstance) 
				? FILE_FLAG_FIRST_PIPE_INSTANCE 
				: 0		),
				  m_dPipeMode    ,
				  m_nInstancesMax,
				  m_dWrBufSize   ,
				  m_dRdBufSize   ,
				  m_dDefTimeout  ,
				    pSecAttrib	 )))
    {
	return ::GetLastError ();
    }

    if (bFirstInstance 
    && (ERROR_ALREADY_EXISTS == ::GetLastError ()))
    {
	::CloseHandle (hPipe);

	return ERROR_ALREADY_EXISTS;
    }
/*
	GResult	Result;

    if (ERROR_SUCCESS != 
       (Result	       = GWinFileHandle::Create (hPipe, GWinFileHandle::OverlappedEx)))
    {
	::CloseHandle (hPipe);
    }
	return  Result;
*/
	return ERROR_SUCCESS;
};

GResult GWinPipeAcceptor::Create (LPCTSTR   pName	,
				  DWord	    dWrBufSize	,
				  DWord	    dRdBufSize	,
				  DWord	    dOpenMode	,
				  DWord	    dPipeMode	,
				  DWord	    dDefTimeout ,
				  PSECURITY_ATTRIBUTES
					    pSecAttrib  )
{
    ::lstrcpyn 
   (m_pName, pName, sizeof (m_pName)/sizeof (TCHAR));

    m_dWrBufSize    = (_M_PAGE_SIZE < dWrBufSize) ? dWrBufSize : _M_PAGE_SIZE;
    m_dRdBufSize    = (_M_PAGE_SIZE < dRdBufSize) ? dRdBufSize : _M_PAGE_SIZE;

    m_dOpenMode     = dOpenMode  | FILE_FLAG_OVERLAPPED ;
    m_dPipeMode     = dPipeMode  | PIPE_WAIT		;
    m_dDefTimeout   = dDefTimeout			;

    return  CreateInstance (True, pSecAttrib);
};
//
//[]------------------------------------------------------------------------[]

#endif
































class GSerialDevice
{
public :

    typedef void (GAPI *    SendCoProc	 )	(void * pHiContext,
						 void * pArgument , GResult,  DWord dTransfered);

    typedef void (GAPI *    SendProc     )	(void * pLoContext, void * pBuf, DWord dBufSize,
							SendCoProc,
						 void * pArgument );

    typedef void (GAPI *    RequestToSend)	(void * pHiContext, SendProc, void * pLoContext);

    typedef void (GAPI *    RecvCoProc	 )	(void * pHiContext,
						 void * pArgument , GResult,  DWord dTransfered);

    typedef void (GAPI *    RecvProc     )	(void * pLoContext, void * pBuf, DWord dBufSize,
							RecvCoProc,
						 void * pArgument );

    typedef void (GAPI *    RequestToRecv)	(void * pHiContext, RecvProc, void * pLoContext);

    typedef void (GAPI *    CloseCoProc	 )	(void * pHiContext);

public :

virtual		void	    Start	(void * pHiContext, RequestToSend,
							    RequestToRecv, DWord dFlags) = NULL;
virtual		void	    Close	() = NULL;

protected :

virtual	void		PerformSend	(		    void * pBuf, DWord  dBufSize,
						SendCoProc  pSendCoProc, void * pArguemnt) = NULL;

static	void GAPI	PerformSend	(void * pLoContext, void * pBuf, DWord  dBufSize,
						SendCoProc  pSendCoProc, void * pArgument)
			{
			    ((GSerialDevice *) pLoContext) ->PerformSend (pBuf, dBufSize, pSendCoProc, pArgument);
			};
protected :

	    void *		m_pHiContext  ;
	    RequestToSend	m_pToSendProc ;
	    RequestToRecv	m_pToRecvProc ;
};

void GSerialDevice::Start (void * pHiContext, RequestToSend pToSendProc,
					      RequestToRecv pToRecvProc, DWord dFlags)
{
    m_pHiContext  = pHiContext;

    m_pToSendProc = pToSendProc;
    m_pToRecvProc = pToRecvProc;

    if (m_pToSendProc)
    {
       (m_pToSendProc)(m_pHiContext, PerformSend, this);
    }
    if (m_pToRecvProc)
    {
//     (m_pToRecvProc)(m_pHiContext, PerformRecv, this);
    }
};

/*
void GSerialDevice::PerformSend (	    void * pBuf, DWord  dBufSize ,
				 SendCoProc pSendCoProc, void * pArgument)
{
    (pSendCoProc)(m_pHiContext, pArgument, ERROR_NOT_SUPPORTED, 0);
};
*/

class GWinFile_SerialDevice : public GSerialDevice
{
protected :

	void		PerformSend	(	     void * pBuf, DWord  dBufSize,
					 SendCoProc  pSendCoProc, void * pArguemnt);

protected :

	    HANDLE		m_hFile	    ;
	    GOverlapped		m_oSend	    ;
};







#if FALSE



    DECLARE_GINTERFACE_ (ISerial, IUnknown)
    {
    // ISerial methods
    //
    STDMETHOD_	(void,			   Recv)(...) PURE;

    STDMETHOD_	(void,			CleanUp)()			PURE;
    STDMETHOD_	(void,	    QueueIrpForComplete)(GThread::Type, GIrp *)	PURE;
    STDMETHOD_	(HWAKEUP,	   CreateWakeUp)()			PURE;
    STDMETHOD_	(HWTIMER,	    CreateTimer)()			PURE;
    STDMETHOD_	(GResult,	 CreateIoCoPort)(IIoCoPort **)		PURE;
    };
/*
    DECLARE_GINTERFACE_ (ISerial, IUnknown)
    {
    // ITPool methods
    //
    STDMETHOD_	(void,			CleanUp)()			PURE;
    STDMETHOD_	(void,	    QueueIrpForComplete)(GThread::Type, GIrp *)	PURE;
    STDMETHOD_	(HWAKEUP,	   CreateWakeUp)()			PURE;
    STDMETHOD_	(HWTIMER,	    CreateTimer)()			PURE;
    STDMETHOD_	(GResult,	 CreateIoCoPort)(IIoCoPort **)		PURE;
    };
*/

class GSerialPort
{
public :

	void			Send)(GIrp * pIrp, void *	pAddr,
						      size_t	dAddr,
						      GStream * 

	void			Recv		(GIrp *	pIrp

protected :

	void			CoSend		(GResult, 

virtual	void			PerformSend	(void * pBuf, DWord dBufSize

};

/*
class GSerialFile
{
};

template <typename T> class TSerialPort : public GSerialPort
{
protected :

	T			m_Transport ;
};

class GWinPipe : public TSerialPort <GWinFile>
{
};

class GComPort : public TSerialPort <GWinFile>
{
};

class GTcpSock : public TSerialPort <GWinSock>


class GUdpSock : public TSerialPort <GWinSock>

*/
/*
    Create


*/







//[]------------------------------------------------------------------------[]
/*
void GOverlapped::CallCoProc (CoProcReason Reason, DWord  dResult     ,
						   void * pResultInfo )
{
    pCoResult	    = dResult;
    


			     pCoResultInfo   = pResultInfo ;
    if (ERROR_IO_PENDING == (dCoResult	     = dResult ))
    {
	if (pCoProc)
	{
	    pCoProc)( = 
	}
    }


    GIoCoProc IoCoProc  = pIoCoProc	   ;
			  pIoCoProc = NULL ;
    if (  pIoPending)
    {
	* pIoPending --;
    }
    if (pCoProc)
    {  
       (



       (IoCoProc)(* this, bSync, dIoStatus	   ,
				 dIoStatusInfo     ,
				 pIoCoProcContext  ,
				 pIoCoProcArgument );

    }
};
*/
//[]------------------------------------------------------------------------[]
//


//
//[]------------------------------------------------------------------------[]
//
Bool GWinFileHandle::GetCompletionStatus (GOverlapped & o, DWord * pIoResult	,
							   DWord * pIoTransfered)
{
    DWord   dResult	   ;
    DWord   dTransfered = 0;

    dResult = (::GetOverlappedResult (NULL, & o, & dTransfered, False)) ? ERROR_SUCCESS
									: ::GetLastError ();
    if (pIoResult)
    { * pIoResult = dResult;}

    if (pIoTransfered)
    { * pIoTransfered = dTransfered;};

    return (dResult != ERROR_IO_INCOMPLETE) ? True : False;
};

void GWinFileHandle::WakeUpIoCoProc (HWAKEUP hWakeUp, GWakeUpReason, void * pWinFile, void * po)
{
    DWord   dResult	;
    DWord   dTransfered ;

    ((GWinFile *) pWinFile) ->GetCompletionStatus (* (GOverlapped *) po, & dResult, & dTransfered);

    ((GOverlapped *) po) ->CallCoProc (GOverlapped::CompletePending, dResult, dTransfered);
};

Bool GWinFileHandle::StartIo (GOverlapped & o, HANDLE hEvent)
{
//  AcquireCloseLock () must be successed
//
     * (o.pIoPending = & m_cIoPending) ++;

    if (hEvent)
    {
	o.hEvent     = (HANDLE)((ULONG_PTR) hEvent | ((m_pIIoCoPort) ? 1 : 0));

	return False;
    }
    if (m_pIIoCoPort == NULL)
    {
	return False;
    }
	return True ;
};

Bool GWinFileHandle::IsQueuedIo	(Bool	bQueued ,
				 DWord	dResult )
{
//  AcquireCloseLock () must be successed
//
    if (bQueued && ((ERROR_SUCCESS    == dResult)
		||  (ERROR_IO_PENDING == dResult)))
    {
	m_pIIoCoPort ->AddRefForPendingIo ();

	return True ;
    }
	return False;
};

void GWinFileHandle::QueueIo (GOverlapped & o, Bool    bQueued     ,
					       DWord   dResult     ,
					       DWord   dTransfered ,
					       HWAKEUP hWakeUp     )
{
    if (bQueued)
    {
	    return;	// Pending By IoCoPort wait
    }	

    if (ERROR_IO_PENDING == dResult)
    {
	if (hWakeUp && o.hEvent)
	{
	    QueueWakeUp  (hWakeUp, GWinFile::WakeUpIoCoProc, this, & o, 
				   (HANDLE) ((ULONG_PTR) o.hEvent & (~((ULONG_PTR) 1))), INFINITE);

	    return;	// Pending By WakeUp port Wait
	}
	    return;	// Pending By Unknown wait
    }
	    o.CompleteIo (dResult, dTransfered, True);
};

void GWinFileHandle::Dummy (GOverlapped & o		    ,
			    GIoCoProc     pIoCoProc	    ,
			    void *	  pIoCoProcContext  ,
			    void *	  pIoCoProcArgument ,
			    HANDLE	  hEvent	    ,
			    HWAKEUP	  hWakeUp	    )
{
    Bool    bQueued	= False ;
    DWord   dResult	= ERROR_INVALID_HANDLE;

    o.Init (0, 0, pIoCoProc, pIoCoProcContext ,
			     pIoCoProcArgument);

    if (AcquireCloseLock ())
    {
	if (bQueued = StartIo (o, hEvent))
	{
	    dResult = m_pIIoCoPort ->QueueIoCoStatus (this, o);
	}
	else
	{
	    dResult = ERROR_SUCCESS;
	}
	    bQueued = IsQueuedIo (bQueued, dResult); 

	if (hEvent)
	{
	    ::SetEvent (hEvent);
	}

	ReleaseCloseLock ();
    }
		      QueueIo (o, bQueued, dResult, 0, hWakeUp);
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//


void GWinFile::Write (DWord	    dOffset	      ,
		      DWord	    dOffsetHigh	      ,
		      const void *  pBuf	      ,
		      DWord	    dBufSize	      ,
		      GOverlapped & o		      ,
		      GIoCoProc     pIoCoProc	      ,
		      void *	    pIoCoProcContext  ,
		      void *	    pIoCoProcArgument ,
		      HANDLE	    hEvent	      ,
		      HWAKEUP	    hWakeUp	      )
{
    Bool    bQueued     = False ;
    DWord   dResult	= ERROR_INVALID_HANDLE;
    DWord   dTransfered = 0	;

    o.Init (dOffset, dOffsetHigh, pIoCoProc	   ,
				  pIoCoProcContext ,
				  pIoCoProcArgument);
    if (AcquireCloseLock ())
    {
	bQueued = StartIo (o, hEvent);

	dResult = (::WriteFile (m_hFile, pBuf, dBufSize, & dTransfered, & o)) ? ERROR_SUCCESS
									      : ::GetLastError ();
	bQueued = IsQueuedIo   (bQueued, dResult);

	ReleaseCloseLock ();
    }
		  QueueIo (o,   bQueued, dResult, dTransfered, hWakeUp);
};

/* Sync call version

GResult GWinFile::Write (const void * pBuf, DWord dBufSize)
{
	GTimeout      o
	GOverlapped   o		 ;
	GResult	      Status	 ;
	DWord	      StatusInfo ;

	HANDLE	      hCoEvent = ::CreateResume ();

    do
    {
	Write	    (0, 0, pBuf, dBufSize, o, NULL, NULL, NULL, hCoEvent, NULL)

	WaitForAny  (1, hCoEvent, dTimeout);

	GetCoStatus (o, & Status, & StatusInfo);

	((Byte *&) pBuf	    ) += StatusInfo;
		   dBufSize   -= StatusInfo;

    } while ((ERROR_SUCCESS == Status) && (0 < dBufSize));

				 ::CloseResume  ();
};

GResult GWinFile::Write (DWord	      dOffset	  ,
			 DWord	      dOffsetHigh ,
			 const void * pBuf	  ,
			 DWord	      dBufSize	  )
{
	GOverlapped   o		 ;
	GResult	      Status	 ;
	DWord	      StatusInfo ;

	HANDLE	      hCoEvent = ::CreateResume ();

    do
    {
	Write	    (dOffset, dOffsetHight,
		     pBuf   , dBufSize    , o, NULL, NULL, NULL, hCoEvent, NULL)

	WaitForAny  (1, hCoEvent, INFINITE);

	GetCoStatus (o, & Status, & StatusInfo);

	((Byte *&) pBuf	    ) += StatusInfo;
		   dBufSize   -= StatusInfo;

    } while ((ERROR_SUCCESS == Status) && (0 < dBufSize));

				 ::CloseResume  ();
};
*/


void GWinFile::Read  (DWord	    dOffset	      ,
		      DWord	    dOffsetHigh	      ,
		      void *	    pBuf	      ,
		      DWord	    dBufSize	      ,
		      GOverlapped & o		      ,
		      GIoCoProc     pIoCoProc	      ,
		      void *	    pIoCoProcContext  ,
		      void *	    pIoCoProcArgument ,
		      HANDLE	    hEvent	      ,
		      HWAKEUP	    hWakeUp	      )
{
    Bool    bQueued     = False ;
    DWord   dResult	= ERROR_INVALID_HANDLE;
    DWord   dTransfered = 0	;

    o.Init (dOffset, dOffsetHigh, pIoCoProc	   ,
				  pIoCoProcContext ,
				  pIoCoProcArgument);
    if (AcquireCloseLock ())
    {
	bQueued = StartIo (o, hEvent);

	dResult = (::ReadFile (m_hFile, pBuf, dBufSize, & dTransfered, & o)) ? ERROR_SUCCESS
									     : ::GetLastError ();
	bQueued = IsQueuedIo  (bQueued, dResult);

	ReleaseCloseLock ();
    }
		  QueueIo (o,  bQueued, dResult, dTransfered, hWakeUp);
};

void GWinFile::DeviceIoControl	(DWord		dOffset		  ,
				 DWord		dOffsetHigh	  ,
				 DWord		dCtrlCode	  ,
				 void *		pOutBuf		  ,
				 DWord		dOutBufSize	  ,
				 void *		pInpBuf		  ,
				 DWord		dInpBufSize	  ,
				 GOverlapped &	o		  ,
				 GIoCoProc	pIoCoProc	  ,
				 void *		pIoCoProcContext  ,
				 void *		pIoCoProcArgument ,
				 HANDLE		hEvent		  ,
				 HWAKEUP	hWakeUp		  )
{
    Bool    bQueued     = False ;
    DWord   dResult	= ERROR_INVALID_HANDLE;
    DWord   dTransfered = 0	;

    o.Init (dOffset, dOffsetHigh, pIoCoProc	   ,
				  pIoCoProcContext ,
				  pIoCoProcArgument);
    if (AcquireCloseLock ())
    {
	bQueued = StartIo (o, hEvent);

	dResult = (::DeviceIoControl (m_hFile, dCtrlCode, pInpBuf, dInpBufSize,
							  pOutBuf, dOutBufSize, 
							& dTransfered	      , & o)) ? ERROR_SUCCESS
										      : ::GetLastError ();
	bQueued = IsQueuedIo (bQueued, dResult);

	ReleaseCloseLock ();
    }
		  QueueIo (o, bQueued, dResult, dTransfered, hWakeUp);
};

void GWinFile::ConnectNamedPipe	(GOverlapped &	o		  ,
				 GIoCoProc	pIoCoProc	  ,
				 void *		pIoCoProcContext  ,
				 void *		pIoCoProcArgument ,
				 HANDLE		hEvent		  ,
				 HWAKEUP	hWakeUp		  )
{
    Bool    bQueued     = False ;
    DWord   dResult	= ERROR_INVALID_HANDLE;

    o.Init (0, 0, pIoCoProc	   ,
		  pIoCoProcContext ,
		  pIoCoProcArgument);

    if (AcquireCloseLock ())
    {
	bQueued = StartIo (o, hEvent);

	dResult	= (::ConnectNamedPipe (m_hFile, & o)) ? ERROR_SUCCESS
						      : ::GetLastError ();
	bQueued = IsQueuedIo (bQueued, dResult);

    if (dResult == ERROR_PIPE_CONNECTED)
    {
	dResult =  ERROR_SUCCESS;
    }
	ReleaseCloseLock ();
    }
		  QueueIo (o, bQueued, dResult, 0, hWakeUp);
};
//
//[]------------------------------------------------------------------------[]

#define	NULL_SECURITY_ATTRIBUTES    ((LPSECURITY_ATTRIBUTES) sizeof (void *))


/*
class GWinPipeAcceptor : public GNetworkAcceptor
{
protected :

	GResult		Create	    (LPCTSTR	pName		,
				     LPSECURITY_ATTRIBUTES
						pSecAttribute	,
				     DWord	dWrBufSize	,
				     DWord	dRdBufSize	,
				     DWord	dOpenMode	,
				     DWord	dPipeMode	,
				     DWord	dDefTimeout	);
};
*/

class GWinSockAcceptor : public GNetworkAcceptor
{
protected :

//	GResult		Create	    (LCPSTR
};



class GWinPipe
{
public :

static	void		Accept	    (GNetworkAcceptor &	
						NetworkAcceptor	,
				     GIrp *	pAcceptingIrp	,
				     LPCTSTR	pName		,
				     LPSECURITY_ATTRIBUTES
						pSecAttribute = NULL,
				     DWord	dWrBufSize    = 0   ,
				     DWord	dRdBufSize    = 0   )
			{
			    GWinPipe::Accept 
				    (		NetworkAcceptor ,
						pAcceptingIrp	,
						pName		,
						pSecAttribute	,
						dWrBufSize	,
						dRdBufSize	,
				    ((0				|   // dOpenMode
				      PIPE_ACCESS_DUPLEX	|
		//		      PIPE_ACCESS_INBOUND	|
		//		      PIPE_ACCESS_OUTBOUND	|
				     0)				|   // One of the ...
		//		      FILE_FLAG_WRITE_THROUGH	|
		//		      WRITE_DAC			|
		//		      WRITE_OWNER		|
		//		      ACCESS_SYSTEM_SECURITY	|
				     0),			    // Combined 

				    ((0				|   // dPipeMode
				      PIPE_TYPE_BYTE		|   // with PIPE_READMODE_BYTE
		//		      PIPE_TYPE_MESSAGE		|   // with PIPE_READMODE_BYTE || PIPE_READMODE_MESSAGE
				     0)				|   // One of the ...
				    (0				|
				      PIPE_READMODE_BYTE	|   // for PIPE_TYPE_MESSAGE || PIPE_TYPE_BYTE
		//		      PIPE_READMODE_MESSAGE	|   // for PIPE_TYPE_MESSAGE
				     0))			,   // One fo the ...

				      NMPWAIT_USE_DEFAULT_WAIT	);  // dDefTimeout WaitNamedPipe server timeout
			};

static	void		Accept	    (GNetworkAcceptor &
						NetworkAcceptor ,
				     GIrp *	pAcceptingIrp	,
				     LPCTSTR	pName		,
				     LPSECURITY_ATTRIBUTES
						pSecAttribute	,
				     DWord	dWrBufSize	,
				     DWord	dRdBufSize	,
				     DWord	dOpenMode	,
				     DWord	dPipeMode	,
				     DWord	dDefTimeout	);
protected :

	    GWinFile			m_WinFile	;
};
//
//[]------------------------------------------------------------------------[]
//


//[]------------------------------------------------------------------------[]
//
    struct GIrp_AcceptPipeStatus : public GIrp_Status
    {
	GIrp_AcceptPipeStatus *	Init	()
	{
	    return this;
	};
    };

/*
class GSerialPort
{
};

class GNetworkPort : public GInterfaceObject
{

protected :

	GBindingHandler		m_BinHandler;
};


void GNetworkDevice

void Accept (GIrp * pIrp)
{
    GIrp_AcceptPipeRequest * pCurRequest = GetCurRequest
   <GIrp_AcceptPipeRequest> (pIrp);

    if (NULL == pCurRequest ->hPipe)
    {
    if (::CreateNamedPipe (........);
    }
};

void DispatchIrp (GIrp * pIrp, GIrp_DspProc pDspProc	    , 
			       void *	    pDspProcContext )
{
    if (pIrp)
    {
	if (pDspProc)
	{
	   (pDspProc)(pIrp, pDspProcContext);
	}
	else
	{
	    CompleteIrp (pIrp);
	}
    }
};

void RunAccepting (GIrp * pIrp)
{
    CreateRequest <GIrp_AcceptPipeRequest> (pIrp) ->Init (pName...);

    DispatchIrp  (pIrp, Accept);
};

void ...
{
    CreateStatus  <GIrp_Status> (pIrp) ->SetStatusInfoPtr (ERROR_SUCCESS, NULL);

    GWinSock::Create (& , AF_INET, IPROTO_TCP, "127.0.0.1:5001", ...);

    DispatchIrp	     (
};

GHandle CreateIoHandle ()
{
    GIrp * pIrp = ::CreateIrp (...);

    

};


*/






    struct GIrp_AcceptPipeRequest : public GIrp_Request
    {
	GNetworkAcceptor *	pAcceptor	;
	LPCTSTR			pName		;
	SECURITY_ATTRIBUTES *	pSecAttribute	;

	DWord			dWrBufSize	;
	DWord			dRdBufSize	;

	DWord			dOpenMode	;
	DWord			dPipeMode	;
	DWord			dDefTimeout	;

	GIrp_AcceptPipeRequest * Init	(GNetworkAcceptor *
						    pAcceptor	 ,
					 LPCTSTR    pName	 ,
					 LPSECURITY_ATTRIBUTES
						    pSecAttribute,
					 DWord	    dWrBufSize	 ,
					 DWord	    dRdBufSize	 ,
					 DWord	    dOpenMode	 ,
					 DWord	    dPipeMode	 ,
					 DWord	    dDefTimeout	 )
	{
	    this ->pAcceptor	 = pAcceptor	;
	    this ->pName	 = pName	;
	    this ->pSecAttribute = pSecAttribute;
	    this ->dWrBufSize	 = dWrBufSize	;
	    this ->dRdBufSize	 = dRdBufSize	;
	    this ->dOpenMode	 = dOpenMode	;
	    this ->dPipeMode	 = dPipeMode	;
	    this ->dDefTimeout	 = dDefTimeout	;

	    return this;
	};

	GResult			CreateNamedPipe (HANDLE & hPipe, Bool bFirstInstance);
    };

    struct GIrp_AcceptPipeDispRequest : public GIrp_DispRequest
    {
	GIrp_AcceptPipeRequest *
				pRequest    ;
	HANDLE			hPipe	    ;

	HWAKEUP			wAccepting  ;
	HANDLE			hAccepting  ;
	OVERLAPPED		oAccepting  ;

	GIrp_AcceptPipeDispRequest * Init (GIrp_AcceptPipeRequest * pRequest)
	{
	    this ->pRequest	= pRequest  ;
		   hPipe	= NULL	    ;
		   wAccepting	= NULL	    ;
		   hAccepting	= NULL	    ;
	oAccepting.Internal	=
	oAccepting.InternalHigh	= 0	    ;
	oAccepting.Offset	=
	oAccepting.OffsetHigh	= 0	    ;
	oAccepting.hEvent	= NULL	    ;

	    return this;
	};
    };

GResult GIrp_AcceptPipeRequest::CreateNamedPipe (HANDLE & hPipe, Bool bFirstInstance)
{
    SECURITY_ATTRIBUTES *
	    pSecAttribute = NULL;
    SECURITY_ATTRIBUTES	    sa	;
    union
    {
    SECURITY_DESCRIPTOR	    saDcr;
    Byte		    saDcrBuf [SECURITY_DESCRIPTOR_MIN_LENGTH];
    };
    TCHAR   pName	   [128];
    hPipe		  = NULL;

	pSecAttribute  = this ->pSecAttribute;

    if (pSecAttribute == NULL_SECURITY_ATTRIBUTES)
    {
						    // Set NULL DACL to allow anyone to connect
						    // to the pipe.
	::InitializeSecurityDescriptor (& saDcr, SECURITY_DESCRIPTOR_REVISION);
	::SetSecurityDescriptorDacl    (& saDcr, True, NULL, False);

	sa.nLength		      = sizeof (sa);
	sa.lpSecurityDescriptor	      = & saDcr	   ;
	sa.bInheritHandle	      = False	   ;

	pSecAttribute  = & sa ;
    }

	strcpy (pName, _T("\\\\.\\pipe\\"));

    if (	       this ->pName)
    {
	strcat (pName, this ->pName);
    }

    if (INVALID_HANDLE_VALUE == 
       (hPipe		      = ::CreateNamedPipe
				(pName		,
			  this ->dOpenMode	,
			  this ->dPipeMode	,
		      PIPE_UNLIMITED_INSTANCES  ,
			  this ->dWrBufSize	,
			  this ->dRdBufSize	,
			  this ->dDefTimeout	,
				 pSecAttribute	)))
    {
	hPipe		      = NULL;

	return ::GetLastError ();
    }

    if (bFirstInstance 

    && (ERROR_ALREADY_EXISTS == ::GetLastError ()))
    {
	::CloseHandle (hPipe);

	hPipe		      = NULL;

	return ERROR_ALREADY_EXISTS ;
    }
	return ERROR_SUCCESS;
};

/*
void GAPI OnAccept_WakeUp (HWAKEUP, GWakeUpReason Reason    ,
				    void *	  pContext  ,
				    void *	  pArgument )
{
};
*/

static void GAPI DispatchAcceptPipe (GIrp * pIrp, void * pInitParam)
{
    GIrp_AcceptPipeDispRequest * pDispRequest;

    if (pInitParam)
    {
       (pDispRequest = CreateDispatchRequest <GIrp_AcceptPipeDispRequest> (pIrp)) ->Init 
		      (GetCurRequest	     <GIrp_AcceptPipeRequest    > (pIrp));

	pDispRequest ->wAccepting = TPool ()   ->CreateWakeUp ();
	pDispRequest ->hAccepting = ::CreateEvent (NULL, False, False, NULL);

	GetCurStatus <GIrp_Status> (pIrp) ->SetStatus
		      (pDispRequest ->pRequest ->CreateNamedPipe (pDispRequest ->hPipe, True));

//	pStatus ->SetStatus   (CreateInstance (pRequest, pStatus ->hPipe, True));
//	pStatus ->wAccepting = TPool () ->CreateWakeUp ();
//	pStatus ->hAccepting = ::CreateEvent (NULL, True, False, NULL);

	return;
    }
	pDispRequest = GetCurRequest <GIrp_AcceptPipeDispRequest> (pIrp);

//	RegisterWakeUp (
//
//  if (pStatus ->wAccepting == NULL)
    {
//	pStatusEx 
    }
};

static GIrp * GAPI Complete_AcceptPipe (GIrp * pIrp, void *, void *)
{
    GetCurRequest <GIrp_AcceptPipeRequest> (pIrp) ->pAcceptor ->ExchangeState
    (GNetworkAcceptor::closed);

    TPool () ->Release ();

    return pIrp;
};

void GWinPipe::Accept (GNetworkAcceptor & Acceptor	,
		       GIrp *		  pIrp		,
		       LPCTSTR		  pName		,
		       LPSECURITY_ATTRIBUTES
					  pSecAttribute ,
		       DWord		  dWrBufSize	,
		       DWord		  dRdBufSize	,
		       DWord		  dOpenMode	,
		       DWord		  dPipeMode	,
		       DWord		  dDefTimeout	)
{
// GASSERT (NULL != pIrp)
//
    if (GNetworkAcceptor::closed  != Acceptor.CompareExchangeState 
       (GNetworkAcceptor::creating,
	GNetworkAcceptor::closed  ))
    {
	GetCurStatus <GIrp_Status> (pIrp) ->SetStatus (ERROR_BUSY);

	CompleteIrp  (pIrp);

	return;
    }

    CreateRequest <GIrp_AcceptPipeRequest> (pIrp) ->Init (& Acceptor	    ,
							    pName	    ,
							    pSecAttribute   ,
	       (_M_PAGE_SIZE > dWrBufSize) ? _M_PAGE_SIZE : dWrBufSize	    ,
	       (_M_PAGE_SIZE > dRdBufSize) ? _M_PAGE_SIZE : dRdBufSize	    ,
				     FILE_FLAG_OVERLAPPED | dOpenMode	    ,
						PIPE_WAIT | dPipeMode	    ,
							    dDefTimeout	    );
//  TPool ->QueueIrpForDispatch (pIrp, Accept);

//  CreateStatus  <GIrp_AcceptPipeStatus>  (pIrp) ->Init
// (GetCurRequest <GIrp_AcceptPipeRequest> (pIrp));

//  CreateBackProc     (pIrp, Complete_AcceptPipe,


/*
			TPool () ->AddRef ();

    CreateBackProc     (pIrp, Complete_AcceptPipe, NULL, NULL);

    CreateDispatchProc (pIrp,	       AcceptPipe);

    CompleteIrp	       (pIrp);
*/
/*
    Bool    bQueued	= False ;
    DWord   dResult	= ERROR_INVALID_HANDLE;

    m_oAccepting.Init (0, 0, NULL, NULL, NULL);

//  SetCurStatusExPtr (ERROR_IO_PENDING, 

    if (AcquireCloseLock ())
    {
	bQueued = StartIo (m_oAccepting, m_hAccepting);

	dResult = (::ConnectNamedPipe (m_hFile, & m_oAccepting)) ? ERROR_SUCCESS
								 : ::GetLastError ();
	bQueued = IsQueuedIo (bQueued, dResult);

	ReleaseCloseLock ();
    }
		  QueueIo (m_oAccepting, bQueued, dResult, 0, NULL);
*/
};
//
//[]------------------------------------------------------------------------[]


/*
    TCHAR * l_pName = alloc <TCHAR *> (pIrp, sizeof (TCHAR) * (strlen (pName) + 1));
		      strcpy (l_pName, pName);
*/


//[]------------------------------------------------------------------------[]
//

/*

{
    GIrp * pIrp = CreateIrp ();

    GWinPipe::Accept (pIrp, & m_IoCompletionPack, 'PIPE', 4096, 4096);
    while (closed == m_IoCompletionPack.GetState ())
    {
	Sleep (
    }
}


*/



void GWinPipeAcceptor::IoCoAccept (Bool bSync, DWord dIoCoStatus, GIrp * pIrp)
{
/*
    IIoCoPort   * pIIoCoPort;
    HANDLE	  hPipe	       = CloseHandle (pIIoCoPort);

    IIoComplete * pIIoComplete = GetCurRequest <GIrp_IoRequest> (pIrp) ->
		  pIIoComplete;

    if (ERROR_SUCCESS == dIoCoStatus)
    {
	if (pIIoComplete)
	{
	    GetCurStatus <GIrp_AcceptingStatus> (pIrp) ->
	    SetAcceptingStatus (dIoCoStatus, 
			       (void *) hPipe, (void *) pIIoCoPort);
	    hPipe      = NULL;
	    pIIoCoPort = NULL;
	}
	else
	{
	    GetCurStatus <GIrp_AcceptingStatus> (pIrp) ->
	    SetAcceptingStatus (ERROR_INVALID_PARAMETER, 
			       (void *) NULL, (void *) NULL);
	}
    }
    else
    {
	    GetCurStatus <GIrp_AcceptingStatus> (pIrp) ->
	    SetAcceptingStatus (dIoCoStatus,
			       (void *)  NULL, (void *) NULL	  );
    }

    if (hPipe)
    {
	::CloseHandle (hPipe);
    }
    if (pIIoCoPort)
    {
	pIIoCoPort ->Release ();
    }

    if (pIIoComplete)
    {
	pIIoComplete ->OnIoComplete (pIrp);
	pIIoComplete ->Release      ();

	return;
    }
	TPool () ->QueueIrpForComplete (GThread::WorkIo, pIrp);
*/
};
//
//[]------------------------------------------------------------------------[]






/*
    GIrp * pIrp = CreateIrp  (...);

    CreateDispatchRequest <> (pIrp);

    m_WinPipeAcceptor.Create (... );
    m_WinPipeAcceptor.Accept (pIrp);


*/




class GOutStream
{
};

class GInpStream
{
};

/*
    struct GBindingHandler;
    struct GMsg		  ;

    typedef Bool (* GBindingProc)(GBindingHandler * oDsp, GWinMsg &);

    struct GMsg
    {
	void *&		pHead	;
	void *		pTail	;
	LRESULT		lResult	;
	UINT		fResult	;
	UINT		uMsg	;
	WPARAM		wParam	;
	LPARAM		lParam	;

	void *		hKey	;
	GWinMsgProc *	pDefProc;
    };


    tcp\\127.0.0.1:5012	    [/T=...],[/B
    udp\\
    winpipe\\


    struct GBindingHandler
    {
	TSLink <GBindingHandler, 0>	SLink ;

	void **				hDsp  ;

	GWinMsgDispatchProc		pDsp  ;
	void *				oDsp  ;
	CPP_PMF				tDsp  ;

	TSLink <GBindingEntry, 0>   SLink   ;
	TXList <GBindingEntry,
	TSLink <GBindingEntry, 0> > SList   ;
	GBindingEntry *		    pParent ;

	void **			    pKey    ;

	void		    RegisterBindingEntry	(GBindingEntry &);
    };

class GBindingHandler
{
};
*/
//
//[]------------------------------------------------------------------------[]

#endif









#if FALSE
/*
class GSerialFile
{
public :

	GWinFileCloseLock &	GetCloseLock () { return m_hWinFileLock;};

protected :

virtual	GOverlapped::IoStatus	Send	(const  void *  pBuf     ,
					 DWord		dBufSize ,
					 GOverlapped &  o        ,
					 HANDLE		hCoEvent );

virtual	GOverlapped::IoStatus	Recv	(	void *  pBuf     ,
					 DWord		dBufSize ,
					 GOverlapped &  o	 ,
					 HANDLE		hCoEvent );

virtual GOverlapped::IoStatus GetOverlappedResult
				    (GOverlapped &  o, Bool bWait);

protected :

	    GWinFileCloseLock	m_hWinFileLock	;   // Close lock for (*)
	    GWinFileHandle	m_hWinFile	;   // (*)
};

GOverlapped::IoStatus GSerialFile::Send (const  void * pBuf     ,
					 DWord	       dBufSize ,
					 GOverlapped & o        ,
					 HANDLE	       hCoEvent )
{
    GOverlapped::IoStatus IoStatus;

    if (m_hWinFileLock.LockAcquire ())
    {
	IoStatus = WriteFile (m_hWinFile.GetHandle (),
			      0, 0, pBuf, dBufSize, o, hCoEvent, m_hWinFile.GetIIoCoPort ());

	m_hWinFileLock.LockRelease ();
    }
    else
    {
	IoStatus = DeadIo (0, 0, o, hCoEvent, NULL, ERROR_OPERATION_ABORTED, NULL);
    }
	return IoStatus;
};

GOverlapped::IoStatus GSerialFile::Recv (void	     * pBuf     ,
					 DWord	       dBufSize ,
					 GOverlapped & o        ,
					 HANDLE	       hCoEvent )
{
    GOverlapped::IoStatus IoStatus;

    if (m_hWinFileLock.LockAcquire ())
    {
	IoStatus = ReadFile (m_hWinFile.GetHandle (),
			     0, 0, pBuf, dBufSize, o, hCoEvent, m_hWinFile.GetIIoCoPort ());

	m_hWinFileLock.LockRelease ();
    }
    else
    {
	IoStatus = DeadIo (0, 0, o, hCoEvent, NULL, ERROR_OPERATION_ABORTED, NULL);
    }
	return IoStatus;
};


class GSerialFile
{
public :

	void	    Send	(GIrp *, const void * 
						pOutBuf	    ,
					 DWord	dOutBufSize ,
					 HANDLE hCoEvent    );

	void	    Recv	(GIrp *, void * pInpBuf	    ,
					 DWord	dOutBufSize ,
					 HANDLE hCoEvent     );

	void	    Read	(GIrp *, void * pInpBuf	    ,
					 DWord	dOutBufSize ,
					 HANDLE hCoEvent    );

protected :

#pragma pack (_M_PACK_VPTR)

	struct WrRdRequest : public GIrp_Request
	{
	    void * 	    pBuf     ;
	    DWord	    dBufSize ;
	    GOverlapped	    o	     ;
	    HANDLE	    hCoEvent ;

	    WrRdRequest *	Init	(void * pBuf, DWord dBufSize, HANDLE hCoEvent)
				{
				    this ->pBuf	    = pBuf     ;
				    this ->dBufSize = dBufSize ;
				    this ->o.Init  ();
				    this ->hCoEvent = hCoEvent ;

				    return this;
				};
	};

#pragma pack ()

static	GIrp *	  GAPI SendCoProc   (GIrp *, GSerialFile *, void *);

virtual void	       CoProc_Send  (GOverlapped *);

virtual	GOverlapped::IoStatus Send  (const  void *  pBuf     ,
				     DWord	    dBufSize ,
				     GOverlapped &  o	     ,
				     HANDLE	    hCoEvent );

virtual	void	       CoProc_Recv  (GOverlapped *);

virtual	GOverlapped::IoStatus Recv  (	    void *  pBuf     ,
				     DWord	    dBufSize ,
				     GOverlapped &  o	     ,
				     HANDLE	    hCoEvent );
protected :
	
	    GWinFileCloseLock	m_hWinFileLock	;   // Close lock for (*)
	    GWinFileHandle	m_hWinFile	;   // (*)
};

GIrp * GAPI GSerialFile::SendCoProc (GIrp * pIrp, GSerialFile * pThis, void *)
{
    WrRdRequest * rq = CurRequest
   <WrRdRequest> (pIrp);


    return pIrp;
};

void GSerialFile::CoProc_Send (GOverlapped * o)
{

};

void GSerialFile::Send (GIrp * pIrp, const void * pBuf	   ,
				     DWord	  dBufSize ,
				     HANDLE	  hCoEvent )
{
	WrRdRequest *	     rq;

    if (m_hWinFileLock.LockAcquire ())
    {
	SetCurRequest (pIrp, rq =
	CreateRequest <WrRdRequest> (pIrp) ->Init ((void *) pBuf, dBufSize, hCoEvent));

	SetCurCoProc  (pIrp, (GIrp_CoProc) GSerialFile::SendCoProc, this, NULL);

//	o.SetCoProc 

	m_hWinFileLock.LockRelease ();
    }
};

void GSerialFile::Recv (GIrp * pIrp, void *  pBuf     ,
				     DWord   dBufSize ,
				     HANDLE  hCoEvent )
{
};
*/
#endif



#if FALSE

GIrp * GSerialPort::IoCo_SendLoop (GIrp * pIrp, void *)
{
#if TRUE
    {
	SendRequest * rq = 
	CurRequest <SendRequest> (pIrp);

	m_cSendLock.Exchange	 (2);

	Io_Send	     (rq ->pBufHead, rq ->pBufTail - rq ->pBufHead,
		      rq ->o.SetIoCoProc (IoCo_Send, pIrp), rq ->hCoEvent);
    }
	return NULL;

#else

    if (ERROR_SUCCESS   == CurStatus  <GIrp_Status> (pIrp))
    {{
	SendRequest * rq = CurRequest <SendRequest> (pIrp);

    if (rq ->pBufHead < rq ->pBufTail)
    {
	Io_Send		(rq ->pBufHead, rq ->pBufTail - rq ->pBufHead,
			 rq ->o.SetIoCoProc (IoCo_Send, this, pIrp), rq ->hCoEvent);
	return NULL;
    }
    }}

	return pIrp;
#endif
};






    

    if (rq ->pCallBack)
    {
	pIrp =	 (rq ->pCallBack)(pIrp, rq ->pContext ,
					rq ->pBufHead ,
					rq ->pBufTail );
    }
    else
    {
	    if (0 <	((GSerialPort *) pContext) ->m_cSendLock.GetValue ())
	{
	    if (0 == -- ((GSerialPort *) pContext) ->m_cSendLock	    )
	    {
		rq ->pContext  = pContext;
		rq ->pCallBack = IoCo_SendCallBack ;


		

	    }
	    return;
	}
	else
	{
	    rq ->pBufHead = rq ->pBufTail;
	}
    }

    if (pIrp)
    {
	CompleteIrp (pIrp);
    }

#endif

/*
#pragma pack (_M_PACK_BYTE)
					// For StreamType::packet only
	typedef struct			//
	{
	struct
	{
	    _U8	    uProtocol	   ;	// +  0 - Packet type + Header length in bytes
					//
					// bPackType : ppllllll	- 
					//
					//	       00000100 - Stream data block
					//	       01000100 - Stream OOB data block
					//	       10xxxxxx - Packet data with AF_XXX (Internet style field)
					//	       11xxxxxx - Unknown (some data)
	    _U8	    uPacketSize [3];	// +  3 - Bigendian full packet size

//	    _U8		uDataType    

	};

	}   PacketHeader ;

#pragma pack ()
*/

/*
void GSerial::IoCo_Send (Bool)
{
    if (ERROR_SUCCESS == m_oSend.IoCoResult ())
    {
    }
};



void GSerial::Send (GIrp * pIrp, const GDataPack * pDstKey    ,
				 GStream *	   pStreamOut ,
				 GStream *	   pStreamInp ,
				 ISerial::Priority tPriority  )
{
};







//
//[]------------------------------------------------------------------------[]
//
static _U16 CalcCS (_U16 curcs, const Byte * pBufHead,
				const Byte * pBufTail)
{
    DWord  cs	      =  curcs;
    size_t tWordCount =	 pBufTail - pBufHead;

    for (; 1 < tWordCount; tWordCount -= 2, pBufHead += 2)
    {
	cs += _GetBigEndianW (pBufHead, 0);
    }
    if (   0 < tWordCount)
    {
	cs += (((Word) * (pBufHead + 0)) << 8);
    }
	cs  = ((DWord)(HIWORD(cs))) + ((DWord)(LOWORD(cs)));
	cs += ((DWord)(HIWORD(cs)));

	return (_U16) cs;
};

GIrp * GSerial::IoCo_RecvPacket (GIrp * pIrp, Byte *& pBufHead,
					      Byte *  pBufTail)
{
	Word   wPacketSize = _GetBigEndianW (pBufHead, offsetof (ISerial::PacketHeader, uPacketSize));

    if ((pBufTail - pBufHead) < wPacketSize)
    {
	return pIrp;
    }
	m_pIoCo_RecvProc = & GSerial::IoCo_RecvHeader;

    {
	ISerial::PacketHeader *   pHeader =
       (ISerial::PacketHeader *&) pBufHead;

	if (pHeader ->uFlags & G5_FLAG_PCS)
	{
	    if (0 != ~CalcCS (0, pBufHead + ((Word) pHeader ->uHeaderSize) * sizeof (_U32),
				 pBufHead + wPacketSize))
	    {
		pBufHead += wPacketSize;

		return (this ->* m_pIoCo_RecvProc)(pIrp, pBufHead, pBufTail);
	    }
	}
    }
	    //	QueueIrp to the dispatch Queue


	return NULL;
};

GIrp * GSerial::IoCo_RecvHeader (GIrp * pIrp, Byte *& pBufHead,
					      Byte *  pBufTail)
{
    for (; sizeof (ISerial::PacketHeader) <= (pBufTail - pBufHead);)
    {
	if (('G' == * pBufHead) && ('5' == * (pBufHead + 1)))
	{{
	    ISerial::PacketHeader *   pHeader =
	   (ISerial::PacketHeader *&) pBufHead;

	    Word    wHeaderSize	  = ((Word) pHeader ->uHeaderSize) * sizeof (_U32);

	    if (pHeader ->uHeaderCS)
	    {
		if ((pBufHead + wHeaderSize) <= pBufTail)
		{
		    if (0 != ~CalcCS (0, pBufHead, pBufHead + wHeaderSize))
		    {
			break;
		    }
		}
		else
		{	break;}
	    }

	return (this ->* (m_pIoCo_RecvProc = & GSerial::IoCo_RecvPacket))(pIrp, pBufHead, pBufTail);

	}}
	    pBufHead ++;
    }
        return pIrp;
};

GIrp * GAPI GSerial::IoCo_Recv (GIrp * pIrp, void *  pContext ,
					     Byte *& pBufHead ,
					     Byte *  pBufTail )
{
    return (((GSerial *) pContext) ->* 
	    ((GSerial *) pContext) ->m_pIoCo_RecvProc)(pIrp, pBufHead, pBufTail);
};

void GSerial::Recv (GIrp * pIrp, void *	pBuf, DWord dBufSize)
{
//  m_pPort ->Recv (pIrp, IoCo_Recv, this, pBuf, dBufSize);
};
*/














//[]------------------------------------------------------------------------[]
//
    DECLARE_INTERFACE_ (IService, IUnknown)
    {
	typedef enum
	{
	    closed	= 0,	// -> creating	| starting		| ready
	    failed	= 1,	// -> closed
	    stopped	= 3,	// -> closed
	    creating	= 2,	// -> failed	| starting		| ready
	    stopping	= 4,	// -> stopped
	    starting    = 5,	// ->					| ready
	    paused	= 6,	// ->		  stopping		| ready
	    ready	= 7,	// -> 		  stopping  | paused

	}   Status ;

    STDMETHOD_	(void,	QueueForQueryStatus)(GIrp *, DWord dFlags) PURE;
    };

    DECLARE_INTERFACE_ (IServiceControl, IUnknown)
    {
    STDMETHOD_	(void,		  Start)(GIrp *) PURE;

    STDMETHOD_	(void,		   Stop)(GIrp *) PURE;
    };

/*
    if (ERROR_SUCCESS == GWinSockConnector::CreateInstance (IID_ISerivceControl, & pIServCtrl))
    {{
	pISerialCtrl ->


	pIServCtrl ->Start (CreateIrp (),
    }}



*/




#if FALSE

class GService
{
public :


protected :

	Bool		LockAcquire	    (Bool bExclusive = True) 
					    { 
						return m_qLock.LockAcquire (bExclusive);
					    };
	void		LockRelease	    () {       m_qLock.LockRelease ();};

#pragma pack (_M_PACK_VPTR)

	struct QueryStatus_Entry
	{
	typedef TSLink <QueryStatus_Entry, 0>   Link;
	typedef TXList <QueryStatus_Entry,
		TSLink <QueryStatus_Entry, 0> > List;

	    Link	    m_QLink	;
	    GIrp *	    m_hKey	;
	    GIrp *	    m_pIrp	;
	    Bool	    m_bDuty	;
	    DWord	    m_dFlags	;
	};

#pragma pack ()

public :

	GIrp *		Co_UpdateQueryStatus		(GIrp * pIrp, QueryStatus_Entry *);

	void		UpdateQueryStatus		(GIrp * pIrp);

static	void GAPI	CancelQueryStatus		(GIrp * pIrp, GAccessLock *, void *);

	void		IService_QueueForQueryStatus	(GIrp * pIrp, DWord);

virtual	void		IServiceControl_Start		(GIrp * pIrp);

static	GIrp * GAPI	IServiceControl_Start		(GIrp * pIrp, void *, void *);

virtual	void		IServiceControl_Stop		(GIrp * pIrp);

protected :

	GAccessLock	    m_qLock		; // Lock for (*), Apartment's lock

	QueryStatus_Entry::List
			    m_qStatusRequest	; // (*)
//
//----------------------------------------------------------------------------
//
protected :

	TXUnknown_Entry	<GApartment>

			    m_XUnknown	 ;


	class IService_Entry : public TInterface_Entry <IService, GService>
	{
	STDMETHOD_ (void,   QueueForQueryStatus)(GIrp * pIrp, DWord dFlags)
	{
	    GetT () ->IService_QueueForQueryStatus (pIrp, dFlags);
	};

	}		    m_IService	 ;
    
	class IServiceControl_Entry : public TInterface_Entry <IServiceControl, GService>
	{
	STDMETHOD_ (void,	Start)(GIrp * pIrp);

	STDMETHOD_ (void,	 Stop)(GIrp * pIrp);

	}		    m_IServiceControl ;
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
GIrp * GService::Co_UpdateQueryStatus (GIrp * pIrp, QueryStatus_Entry * e)
{
    LockAcquire ();

    LockRelease ();

    return pIrp;
};

void GService::UpdateQueryStatus (GIrp * pIrp)
{
};

void GAPI GService::CancelQueryStatus (GIrp * pIrp, GAccessLock *, void * pService)
{
//  LockAcquire () wass called !!!

};

void GService::IService_QueueForQueryStatus (GIrp * pIrp, DWord dFlags)
{
    QueryStatus_Entry * e =
   (QueryStatus_Entry *) pIrp ->alloc (sizeof (QueryStatus_Entry));

    e ->m_QLink	 .unlink ();
    e ->m_hKey	 = pIrp	   ;
    e ->m_pIrp	 = NULL	   ;
    e ->m_bDuty  = True	   ;
    e ->m_dFlags = dFlags  ;

			      m_IService.AddRef ();

    SetReleaseCoProc (pIrp, & m_IService);

    LockAcquire ();

	if (SetCancelProc (pIrp, & m_qLock, CancelQueryStatus, this))
	{
	    m_qStatusRequest.append (e);
	}
	else
	{
    LockRelease ();

	    CompleteIrp (pIrp);

	    return;
	}

    LockRelease ();

    SetCurCoProc        <GService, QueryStatus_Entry *>
			(pIrp,  Co_UpdateQueryStatus, this, e);

    UpdateQueryStatus   (pIrp);
};
//
//[]------------------------------------------------------------------------[]
//
void GService::IServiceControl_Start (GIrp * pIrp)
{};

void GService::IServiceControl_Entry::Start (GIrp * pIrp)
{};
//
//[]------------------------------------------------------------------------[]

class GClientManager : public GService
{

protected :

	GResult		On_CtrlHandler	    (UINT, WPARAM, LPARAM);
};

GResult GClientManager::On_CtrlHandler (UINT uCtrlCode, WPARAM wParam,
						        LPARAM lParam)
{
    return ERROR_SUCCESS;
};


//[]------------------------------------------------------------------------[]
//
#define DISPATCH_IRPM(msg,Method)		    \
    if  (msg == m.uMsg)				    \
    {						    \
	return Method (m);			    \
    }

#define	DISPATCH_IRPM_WPARAM(msg,mwp,Method)	    \
    if ((msg == m.uMsg  )			    \
    &&  (mwp == m.wParam))			    \
    {						    \
	return Method (m);			    \
    }

#define DISPATCH_IRPM_WPARAM_LO(msg,mwp,Method)	    \
    if ((msg == m.uMsg		)		    \
	(mwp == LOWORD (m.wParam)))		    \
    {						    \
	return Method (m);			    \
    }

#define	DISPATCH_IRPM_WPARAM_HI(msg,mwp,Method)	    \
    if ((msg == m.uMsg		)		    \
	(mwp == HIWORD (m.wParam)))		    \
    {						    \
	return Method (m);			    \
    }

#define RESPONSE_IRPM(msg,Method)		    \
    if  (msg == m.uMsg)				    \
    {						    \
	return Method (m);			    \
    }

#define	RESPONSE_IRPM_WPARAM(msg,mwp,Method)	    \
    if ((msg == m.uMsg  )			    \
    &&  (mwp == m.wParam))			    \
    {						    \
	return Method (m);			    \
    }

#define RESONSE_IRPM_WPARAM_LO(msg,mwp,Method)	    \
    if ((msg == m.uMsg		)		    \
	(mwp == LOWORD (m.wParam)))		    \
    {						    \
	return Method (m);			    \
    }

#define	RESPONSE_IRPM_WPARAM_HI(msg,mwp,Method)	    \
    if ((msg == m.uMsg		)		    \
	(mwp == HIWORD (m.wParam)))		    \
    {						    \
	return Method (m);			    \
    }

#define	IRPM_SERVICE_CTRL	0x0001


/*
Bool GParentWindow::OnWinMessage (GWinMsg & m);
Bool GParentWindow::DispatchWinMsg (GWinMsg & m)
{
    switch (m.uType)
    {
    case GWinMsg::Internal :

	DISPATCH_GM	(GM_RegisterCtrlNotify,	On_GM_RegisterCtrlNotify)

	return GWindow::DispatchWinMsg (m);

    case GWinMsg::WndProc :

	DISPATCH_WM	(WM_NOTIFY,		On_WM_Notify	    )
	DISPATCH_WM	(WM_COMMAND,		On_WM_Notify	    )
	DISPATCH_WM	(WM_WINDOWPOSCHANGED,	On_WM_PosChanged    )
	DISPATCH_WM	(WM_ACTIVATE,		On_WM_Activate	    )


	RESPONSE_WM	(WM_NOTIFY,		On_WM_CtrlNotify    )
	RESPONSE_WM	(WM_COMMAND,		On_WM_CtrlNotify    )
	RESPONSE_WM	(WM_WINDOWPOSCHANGED,	On_WM_PosChanged    )
	RESPONSE_WM	(WM_ACTIVATE,		On_WM_Activate	    )


	PROCESS_

	return GWindow::DispatchWinMsg (m);
    };
	return GWindow::DispatchWinMsg (m);
};
*/
//
//[]------------------------------------------------------------------------[]


//[]------------------------------------------------------------------------[]
//
/*
class GService
{
public :
};
*/
//
//[]------------------------------------------------------------------------[]

#endif















//[]------------------------------------------------------------------------[]
//
class XNotificationPort
{
public :
			XNotificationPort ()
			{
//			    m_pIDispatch = 
//			    CreateXDispatcher (XNotificationPort::Xxxx_On_Notify, this);
			};

		       ~XNotificationPort ()
			{
			if (m_pIDispatch)

			    m_pIDispatch ->Release ();
			};

	XNotify *	CreateNotifyEntry	(XDispatchCallBack *     ,
						 const void * pRequestId );

	GIrp *		SendNotify		(GIrp * pIrp)
			{
			    return m_pIDispatch ->Invoke (pIrp);
			};
protected :

#pragma pack (_M_PACK_VPTR)

	class XNotifyEntry : public XNotify
	{
	typedef TSLink <XNotifyEntry, sizeof (XNotify)>   Link ;
	typedef TXList <XNotifyEntry,
		TSLink <XNotifyEntry, sizeof (XNotify)> > List ;

		void *  operator new    (size_t s);
		void    operator delete (void * p);

			XNotifyEntry	(XNotificationPort * pOwner    ,
					 XDispatchCallBack * pICallBack,
					 const void *	     pRequestId)
			{
			     m_pOwner	  = pOwner    ;

			    (m_pICallBack = pICallBack) ->AddRef ();
			     m_pRequestId = pRequestId;
			};

		       ~XNotifyEntry	()
			{
			     m_pICallBack ->Release ();
			};

	    Link		m_qLink	      ;
	    XNotificationPort *	m_pOwner      ;

	    XDispatchCallBack *	m_pICallBack  ;
	    const void *	m_pRequestId  ;

	STDMETHOD_  (void,	    Destroy)();

	STDMETHOD_  (void,	    Refresh)();

	friend class XNotificationPort	;
	};

#pragma pack ()

	friend class XNotifyEntry	;

	Bool		    LockAcquire	    (Bool bExclusive = True) 
					    { 
						return m_xLock.LockAcquire (bExclusive);
					    };
	void		    LockRelease	    () {       m_xLock.LockRelease ();};

	GIrp *		    On_NotifyLoop   (GIrp *, void *);

	GIrp *		    On_Notify	    (GIrp *, void *);

static	GIrp * GAPI    Xxxx_On_Notify	    (GIrp *, GApartment *, void *);

protected :

	GAccessLock		m_xLock	     ;  // (*) Lock for

	XDispatch *		m_pIDispatch ;

	XNotifyEntry::List	m_SubscrList ;
	TXList_Iterator 
       <XNotifyEntry>		m_SubscrListI;
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
void * XNotificationPort::XNotifyEntry::operator new (size_t sz)
{
    return ::operator new (sz);
};

void XNotificationPort::XNotifyEntry::operator delete (void * p)
{
    ::operator delete (p);
};

void XNotificationPort::XNotifyEntry::Destroy ()
{
    (m_pOwner) ->m_SubscrList.extract (this);

    delete this;
};

void XNotificationPort::XNotifyEntry::Refresh ()
{};

GIrp * XNotificationPort::On_NotifyLoop (GIrp * pIrp, void *)
{
    XNotifyEntry * e = m_SubscrListI.next ();
		       m_SubscrListI <<= m_SubscrList.getnext 
		      (m_SubscrListI.next ());

    if (m_SubscrListI.next ())
    {
	SetCurCoProc <XNotificationPort, void *>
	     (pIrp, & XNotificationPort::On_NotifyLoop, this, (void *) True);
	
    }
    return e ->m_pICallBack ->Invoke (pIrp);
};

GIrp * XNotificationPort::On_Notify (GIrp * pIrp, void *)
{
	m_SubscrListI = m_SubscrList.head ();

    if (m_SubscrListI.next ())
    {
	SetCurCoProc <XNotificationPort, void *>
	     (pIrp, & XNotificationPort::On_NotifyLoop, this, (void *) True);
    }
	return pIrp;
/*
    XNotifyEntry * e;

    if (NULL == pArg)
    {
	m_SubscrListI	= m_SubscrList.head ();
    }

    if (NULL != (e = m_SubscrListI.next ()))
    {
	m_SubscrListI <<= m_SubscrList.getnext (m_SubscrListI.next ());

    if (m_SubscrListI.next ())
    {
	SetCurCoProc <XNotificationPort, void *>
	     (pIrp, & XNotificationPort::On_Notify, this, (void *) True);
    }
	if (NULL != (pIrp = e ->m_pICallBack ->Invoke (pIrp)))
	{
	CompleteIrp (pIrp);
	}
	return NULL;
    }

	return pIrp;
*/
};

GIrp * XNotificationPort::Xxxx_On_Notify (GIrp * pIrp, GApartment * pa, void * pThis)
{
    SetCurCoProc  <XNotificationPort, void *>
	  (pIrp, & XNotificationPort::On_Notify, (XNotificationPort *) pThis, NULL);
/*last
    return pIrp = (pa ->QueueIrpForComplete (pIrp), NULL);
*/
    return NULL;
};

XNotify * XNotificationPort::CreateNotifyEntry (XDispatchCallBack *
							     pICallBack ,
						const void * pRequestId )
{
    XNotifyEntry * e = new XNotifyEntry (this, pICallBack, pRequestId);

    if (e)
    {
	m_SubscrList.append (e);
    }
	return e;
};
//
//[]------------------------------------------------------------------------[]




#include <stdio.h>

#include "g_winsock.h"

//[]------------------------------------------------------------------------[]
//
    DECLARE_INTERFACE_ (IIoDeviceConfig, IUnknown)
    {
#pragma pack (_M_PACK_VPTR)

	typedef struct
	{
	    GUID *	dcrId	    ;
	    DWord	dcrVersion  ;
	//
	//
	}		Descriptor  ;

#pragma pack ()

    STDMETHOD_ (void,     GetDescriptor)(GIrp *, Descriptor &);
    };

    void GAPI	      GetIoDeviceConfig (GIrp *, IIoDeviceConfig *, IIoDeviceConfig::Descriptor &);

    DECLARE_INTERFACE_ (IIoSerialLineConfig, IUnknown)
    {
    STDMETHOD_ (void,		 SetDCB)(GIrp *, DCB &) PURE;
    };
    
class GService
{
public :

#pragma pack (_M_PACK_VPTR)

	typedef struct
	{
	    void (GAPI *    pClose  )(GIrp *, void *);
	    void (GAPI *    pCreate )(GIrp *, void *);
	    void (GAPI *    pStop   )(GIrp *, void *);
	    void (GAPI *    pStart  )(GIrp *, void *);

	}	  EntryPoints ;

#pragma pack ()

	typedef enum
	{			//	      |	if (fail) | if (! fail)
				//
	    closed	= 0,	// ->		creating     creating
	    closing	= 1,	// -> pending	closed	     closed
	    creating	= 2,	// -> pending   closed	     created
	    created	= 3,	// ->		closing	     starting
	    stopping	= 4,	// -> pending   created	     created
	    starting	= 5,	// -> pending	stopped	     ready      (*)
	    ready	= 6,	// ->		stopping     stopping

	}		State ;

	typedef struct tag_Hanlde {} * Handle;

			GService ()
			{
			    m_CurState	    = closed;

			    m_pWkIrp	    = NULL;
			    m_pDnIrp	    = 
			    m_pUpIrp	    =
			    m_pUpIrpPre	    = NULL;

			    memset 
			 (& m_oPefEntries,    0, sizeof (m_oPefEntries));
			    m_pPefContext   = NULL;
			    m_dPefTimeout   = 1000;
			};

	State		GetCurState		() const { return m_CurState;};

public :

	void		QueueStart		(GIrp *, IUnknown * = NULL);

	void		QueueStop		(GIrp *);

protected :

#pragma pack (_M_PACK_VPTR)
/*
	struct Handle 
	{
	    typedef TSLink <Handle, 0>	    Link;
	    typedef TXList <Handle
		    TSLink <Handle, 0 > >   List;
	};
*/
#pragma pack ()

virtual	void		StateChanged		();

	void		SetCurState		(State newState);

	void		ChangeState		();

#pragma pack (_M_PACK_VPTR)

	struct StartRequest : public GIrp_Request
	{
	};

#pragma pack ()

	GIrp *		Co_Timeout		(GIrp *, void *);

	GIrp *		Co_Start		(GIrp *, void *);

	GIrp *		Co_Create		(GIrp *, void *);

	GIrp *		Co_Queue		(GIrp *, void *);

	void		StartQueue		(GIrp *);

protected :

	State			m_CurState	;

	GIrp *			m_pWkIrp	;

	GIrp *			m_pDnIrp	;
	GIrp_List		m_qDone		;

	GIrp *			m_pUpIrp	;
	GIrp *			m_pUpIrpPre	;

	GIrp_List 		m_qCancel	;
	GIrp_List		m_qOpen		;

	EntryPoints		m_oPefEntries	;
	void *			m_pPefContext	;

	DWord			m_dPefTimeout	;
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
void GService::StateChanged ()
{
};



void GService::SetCurState (State newState)
{
    State oldState = m_CurState;
		     m_CurState = newState;

    if (oldState !=  m_CurState)
    {
	StateChanged ();
    }
};

GIrp * GService::Co_Timeout (GIrp * pIrp, void *)
{
    if (pIrp ->Cancelled ())
    {	    return pIrp;}

	    ChangeState ();
	    return NULL;
};

GIrp * GService::Co_Start  (GIrp * pIrp, void * dStartTick)
{
    if (pIrp ->Cancelled ())
    {	    return pIrp;}

    if (ERROR_SUCCESS != pIrp ->GetCurResult ())
    {{
	DWord	dElapsedCount = (GetTickCount () - (DWord) dStartTick);
	DWord	dPefTimeout   = 
       (DWord)	pIrp ->GetCurResultInfo    ();

	if (dPefTimeout <  m_dPefTimeout)
	{
	    dPefTimeout =  m_dPefTimeout;
	}
	if (dPefTimeout >  dElapsedCount)
	{
	    SetCurCoProc <GService, void *>
	   (pIrp, & GService::Co_Timeout, this, NULL);
/*last
	    TCurrent () ->GetCurApartment () ->QueueIrpForComplete
  	   (pIrp, dPefTimeout - dElapsedCount);
*/
	    return NULL;
	}
	    ChangeState ();
	    return NULL;
    }}
	    SetCurState (ready);

	    ChangeState ();
	    return NULL;
};

GIrp * GService::Co_Create (GIrp * pIrp, void * dStartTick)
{
    if (pIrp ->Cancelled ())
    {	    return pIrp;}

    if (ERROR_SUCCESS != pIrp ->GetCurResult ())
    {{
	DWord	dElapsedCount = (GetTickCount () - (DWord) dStartTick);
	DWord	dPefTimeout   = 
       (DWord)	pIrp ->GetCurResultInfo    ();

	if (dPefTimeout <  m_dPefTimeout)
	{
	    dPefTimeout =  m_dPefTimeout;
	}
	if (dPefTimeout >  dElapsedCount)
	{
	    SetCurCoProc <GService, void *>
	   (pIrp, & GService::Co_Timeout, this, NULL);
/*last
	    TCurrent () ->GetCurApartment () ->QueueIrpForComplete
	   (pIrp, dPefTimeout - dElapsedCount);
*/
	    return NULL;
	}
	    ChangeState ();
	    return NULL;
    }}
	    SetCurState (created);

	    ChangeState ();
	    return NULL;
};

void GService::ChangeState ()
{
/*
    if (m_pWkIrp == NULL)
    {
	m_pWkIrp = (m_pDnIrp) ? & m_pDnIrp : ((m_pUpIrp) ? & m_pUpIrp : NULL);

    if (m_pWkIrp == NULL)
    {	return;}
    }
*/
    if (m_pWkIrp == m_pDnIrp)
    {
	switch (GetCurState ())
	{
	case ready  :

		SetCurState (stopping);

	    if (m_oPefEntries.pStop  )
	    {
	       (m_oPefEntries.pStop  )(m_pDnIrp, m_pPefContext);

		break;
	    }

	case stopping :

	    if (m_pUpIrp && m_oPefEntries.pStart)
	    {
	    }
		break;
	};
	return;
    }
    else
//  if (m_pWkIrp == m_pUpIrp)
    {
	switch (GetCurState ())
	{
	case closed   :
	case creating :

	    if (m_oPefEntries.pCreate)
	    {
		SetCurState (creating);

		SetCurCoProc <GService, void *>
	       (m_pUpIrp, & GService::Co_Create, this, (void *) GetTickCount ());

	       (m_oPefEntries.pCreate)(m_pUpIrp, m_pPefContext);

		break;
	    }
		SetCurState (created );

		ChangeState ();

		break;

	case created  :
	case starting :

	    if (m_oPefEntries.pStart )
	    {
		SetCurState (starting);

		SetCurCoProc <GService, void *>
	       (m_pUpIrp, & GService::Co_Create, this, (void *) GetTickCount ());

	       (m_oPefEntries.pStart )(m_pUpIrp, m_pPefContext);

		break;
	    }
		SetCurState (ready   );

		ChangeState ();
		break;

	case ready    :
/*
		if (m_pPendingIrp)
		{
		    CompleteIrp (m_pPendingIrp);
				 m_pPendingIrp = NULL;

		    ChangeState ();
		}
*/
	default :
		break;

		break;
	};
	return;
    }
	::RaiseException (0x80000000, 0, 0, NULL);  // No Entry to this point !!!
};

GIrp * GService::Co_Queue (GIrp * pIrp, void *)
{
    printf ("GService::Co_Queue %08X\n", pIrp);

    return pIrp;
};

void GService::StartQueue (GIrp * pIrp)
{
    SetCurCoProc <GService, void *>
   (pIrp, & GService::Co_Queue, this, NULL);

    if (m_pUpIrp == pIrp)
    {
    if (m_pDnIrp == NULL)
    {
	
    }
    }
    else
//  if (m_pDnIrp == pIrp)
    {
    if (m_pUpIrp)
    {
	CancelIrp (m_pUpIrp);
    
	return;
    }
    }
};

void GService::QueueStart (GIrp * pIrp, IUnknown * pIUnknown)
{
    printf ("GService::Queue %08X\n", pIrp);

    if (pIUnknown)		    // Queue for create or reconfigure
    {				    //
	if (m_pUpIrp )
	{
	    if (m_pUpIrpPre)
	    {
		CancelIrp	 (m_pUpIrpPre);

		m_qCancel.append (m_pUpIrpPre);
	    }
		m_pUpIrpPre = pIrp;

		CancelIrp	 (m_pUpIrp   );

		return;
	}
		m_pUpIrp    = pIrp;

	if (ready == GetCurState ())
	{
		m_pDnIrp    = pIrp;

	}
		ChangeState ();

		return;
    }

    if (closed < GetCurState ())
    {
	m_qOpen.append   (pIrp);

	return;
    }
	pIrp ->SetResultInfo (ERROR_NOT_READY, NULL);	// SERVICE_NOT_STARTED

	CompleteIrp (pIrp);
};

void GService::QueueStop  (GIrp * pIrp)
{
    if (closed < GetCurState ())
    {
    if (m_pDnIrp)
    {
	m_qDone.append (pIrp);

	return;
    }
	StartQueue (m_pDnIrp = pIrp);

	return;
    }
	pIrp ->SetResultInfo (ERROR_SUCCESS,  NULL);

	CompleteIrp (pIrp);
};















/*
void GService::QueueStart (GIrp * pIrp, IUnknown * pIUnknown)
{
    printf ("GService::Queue %08X\n", pIrp);

    if (pIUnknown)		    // Queue for create or reconfigure
    {				    //
	if     (m_pDnIrp   )
	{
	    if (m_pUpIrp   )
	    {
		m_qCancel.append (m_pUpIrp   );
	    }
		m_pUpIrp    = pIrp;

		return;
	}
	if     (m_pUpIrp   )
	{
	    if (m_pUpIrpPre)
	    {
		m_qCancel.append (m_pUpIrpPre);
	    }
		m_pUpIrpPre = pIrp;

		CancelIrp	 (m_pUpIrp   );

		return;
	}
		m_pUpIrp    = pIrp;

	if (ready == GetCurState ())
	{
		m_pDnIrp    = pIrp;

	}
		ChangeState ();

		return;
    }

    if (closed < GetCurState ())
    {
	m_qOpen.append   (pIrp);

	return;
    }
	pIrp ->SetResultInfo (ERROR_NOT_READY, NULL);	// SERVICE_NOT_STARTED

	CompleteIrp (pIrp);
};
*/


//
//[]------------------------------------------------------------------------[]







/*

void SomeCareator ()
{
    XService *	pIService;

    if (ERROR_SUCCESS == GService::CreateInstance (IID_XService, & pIService))
    {
    }
};

void Application::On_RRPService (GIrp * pIrp, XDispatch::CallBack * hCallBack)
{
    if (...)
    {
	return;
    }
    else
    if (m_hRRPService)
    {
	m_hRRPSerivce ->Close ();

	m_hRRPSerivce = NULL;
    }
	CompleteIrp (pIrp);
};


0 :

    XServiceManager * pXServiceManager = NULL;

    if (ERROR_SUCCESS == ::QueryInterface (IID_XServiceMenager, (void **) & pXServiceManager))
    {
    XSignalRaiser   * pXSignalRaiser   = NULL;

    if (ERROR_SUCCESS == ::QueryInterface (IID_XSignalRaiser  , (void **) & pXSignalRaiser))
    {
	Application::On_XServiceCallBack
    }
    }



    IIServiceManager () ->




2 :

    m_hRRPService =

    QueryInterface (m_pIRRPDevice


    if (ERROR_SUCCESS == CreateXDispatchCallBack <Application>
		      (m_pIRRPService, IID_ISignal_RRPService, & m_hRRPCallBack, & Application::On_RRPCallBack, this, (const void *) '_RRP, m_pIRRPService)
    {
	m_pIRRPService ->

    }








1 :

    GIrp * pIrp	  = CreateIrp ();

    m_hRRPService = CreateXDispatchCallBack
   <Application>    (pIrp, & Application::On_RRPService, this, (const void *) '_RRP');


		    CreateXDispatchCallBack


    if (ERROR_SUCCESS == m_IRRPService ->QueryInterface (IID_ISignal, (void **) m_IRRPCallBack))
    {{
	m_IRRPCallBack ->Connect

	m_IRRPCallBack ->Register
    }}

    m_IRRPService ->RegisterCallBack (

    if (ERROR_TIMEOUT == m_hRRPService ->Wait








    m_hRRPService = CreateXDispatcher 
   <Application> (& Application::On_RRPService, this, (const void *) '_RRP');





    if (m_hRRPService)
    {
	m_hRRPService ->Close ();

	m_hRRPService = NULL;
    }


    QueryInterface (




    QueueGetClassObject (CreateIrp (), m_hRRPService, 







    CreateXDispatchCallBack <Application>
    (& m_hRRPService, CreateIrp (), & Application::On_RRPService, this);

    QueueGetClassObject (pIrp, m_hRRPService, 



2 :






    QueueGetClassObject (pIrp, m_hRRPService, & Application::Xxxx_On_RRPService, CLSID_RRPSerivice);




    QueueGetClassObject (pIrp, NULL, & Application::On_RRPService, CLSDID_RRPService);



*/




//
//[]------------------------------------------------------------------------[]

#if FALSE

class GSerialConnector : public GService
{
public :
		GSerialConnector ();

	GResult		QueryInterface		(ISerial *&);

protected :

virtual	GIrp *		PerformReady		(GIrp *)  = NULL;

	GIrp *		Co_Connect		(GIrp *, void *);

virtual	GIrp *		PerformConnect		(GIrp *)  = NULL;

	GIrp *		Co_ConnectLoop		(GIrp *, void *);

virtual	GResult		PerformCreate		(GIrp *)  = NULL;

	GIrp *		Co_Notify		(GIrp *, void *);

	GIrp *		Co_CreationLoop		(GIrp *, void *);

	void		PerformStart		(GIrp *);

//virtual	void		BuildConnectRequest	(GIrp *);

	void		Connect			(GIrp *);

protected :

	ISerial *		m_pISerial    ;

	GIrp *			m_pPrepIrp    ;	// (*);
	GIrp *			m_pWorkIrp    ;	// (*);
	GTimeout		m_tTimeout    ;
	DWord			m_dTimeoutCrt ;
	DWord			m_dTimeoutCon ;
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//

GSerialConnector::GSerialConnector ()
{
    m_pPrepIrp	  =
    m_pWorkIrp	  = NULL;

    m_dTimeoutCrt = 1000;
    m_dTimeoutCon =  500;
};

GResult GSerialConnector::QueryInterface (ISerial *& pISerial)
{
    if (ready == GetCurState ())
    {
       (pISerial = m_pISerial) ->AddRef ();

	return ERROR_SUCCESS;
    }

	return ERROR_INVALID_HANDLE;
};

GIrp * GSerialConnector::Co_Connect (GIrp * pIrp, void *)
{
    if (! pIrp ->Cancelled ())
    {
	SetCurCoProc <GSerialConnector, void *>
	     (pIrp, & GSerialConnector::Co_ConnectLoop, this, NULL);

	if (ERROR_SUCCESS == pIrp ->GetCurResult ())
	{
	    return PerformReady (pIrp);
	}


//
//
//	Sleep (m_tTimeout);
    }
	return pIrp;
};

GIrp * GSerialConnector::Co_ConnectLoop (GIrp * pIrp, void *)
{
    if (! pIrp ->Cancelled ())
    {
	m_tTimeout.Activate (m_dTimeoutCon);

	SetCurCoProc <GSerialConnector, void *>
	     (pIrp, & GSerialConnector::Co_Connect    , this, NULL);

	return PerformConnect (pIrp);
    }
	return pIrp;
};

GIrp * GSerialConnector::Co_Notify (GIrp * pIrp, void *)
{
    return QueueForDelay (pIrp, m_tTimeout.Relative ());
};

GIrp * GSerialConnector::Co_CreationLoop (GIrp * pIrp, void *)
{
    if (! pIrp ->Cancelled ())
    {
	    SetCurCoProc <GSerialConnector, void *>
		  (pIrp, & GSerialConnector::Co_CreationLoop, this, NULL);

	    m_tTimeout.Activate  (m_dTimeoutCrt);

	if (ERROR_SUCCESS == (PerformCreate (pIrp)))
	{
	    m_tTimeout.Disactivate ();

	    SetCurCoProc <GSerialConnector, void *>
		 (pIrp, & GSerialConnector::Co_ConnectLoop  , this, NULL);

	    return pIrp;
	}
	    SetCurCoProc <GSerialConnector, void *>
		 (pIrp, & GSerialConnector::Co_Notify	    , this, NULL);

//	    return m_NPort.SendNotify (pIrp);
    }
	CompleteIrp (pIrp);


printf ("...Exit form GSerialConnector::PerformCreate\n");

	pIrp = m_pWorkIrp = m_pPrepIrp ;

			    m_pPrepIrp = NULL;
    if (pIrp)
    {
	CompleteIrp (pIrp);
    }
	return NULL;
};

void GSerialConnector::PerformStart (GIrp * pIrp)
{
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
#pragma pack (_M_PACK_VPTR)

#pragma pack ()

class GWinSockConnector : public GSerialConnector
{
public :
		GWinSockConnector   () : GSerialConnector () {};

	void		Start		(GIrp *, GWinSock::AddressFamily    ,
						 GWinSock::Protocol	    ,
						 GWinSock::Type		    ,
						 GWinSock::Address &	    ,
						 GWinSock::SQOS	   * = NULL );

	void		Start		(GIrp * pIrp)
			{
			    PerformStart (pIrp);
			};

protected :

	GIrp *		PerformReady		(GIrp *);

	GIrp *		PerformConnect		(GIrp *);

	GResult		PerformCreate		(GIrp *);

protected :


//	GWinSock		m_oWinSock  ;
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//

GIrp * GWinSockConnector::PerformReady (GIrp * pIrp)
{
    pIrp ->SetCurResultInfo (ERROR_TIMEOUT);

    return pIrp;
};


GIrp * GWinSockConnector::PerformConnect (GIrp * pIrp)
{
    pIrp ->SetCurResultInfo (ERROR_TIMEOUT);

    return pIrp;
};

GResult GWinSockConnector::PerformCreate (GIrp * pIrp)
{
    return ERROR_FILE_NOT_FOUND;
};
//
//[]------------------------------------------------------------------------[]

/*
    GIrp *		 pIrp	    = NULL;
    IServiceControl *	 pIServCtrl = NULL;

    if (ERROR_SUCCESS == GWinSockConnector::CreateInstance 
			(IID_IServiceControl, & pIServCtrl))
    {
	pIrp = CreateIrp ();

	SetCurRequest GWinSockConnector::
	CreateRequest (pIrp);

	pIServCtrl ->Start (pIrp);
    }

    ...




/*

    IService		 pIServ	    = NULL;

    if (ERROR_SUCCESS == QueryInterface (CLSIID_WinSockConnector

*/


#endif




















#if FALSE


GOverlapped::IoStatus GAPI Connect (SOCKET	hSock	    ,
//				    GWinSock::Address &	    ,
//						  hSockAddress,
				    GOverlapped & o	    ,
				    IWakeUp *	pIIoCoWakeUp)
{
    DWord   dResult	= 0;

    return o.GetIoCoStatus (dResult, NULL);
};


//[]------------------------------------------------------------------------[]
//
class GNetworkConnector
{
public :

protected :

#ifdef _MSC_BUG_C2248
public :
#endif

#pragma pack (_M_PACK_VPTR)

	struct ConnectRequest : public GIrp_Request
	{
	    DWord		dFailTimeout ;
	    GTimeout		tFailTimeout ;

	    ConnectRequest *	Init	()
				{
				    return this;
				};
	};

#pragma pack ()

protected :
//
//----------------------------------------------------------------------------
//
virtual	void		Io_ConnectProc	(GIrp *) = NULL;

	GIrp *		Io_ConnectLoop	(GIrp *, void *);
//
//----------------------------------------------------------------------------
//
protected :

	GCloseLock	    m_fCloseLock ;
};
//
//[]------------------------------------------------------------------------[]


GIrp * GNetworkConnector::Io_ConnectLoop (GIrp * pIrp, void *)
{
    if (m_fCloseLock.LockAcquire ())
    {
	SetCurCoProc   <GNetworkConnector, void *>
		       (pIrp, & GNetworkConnector::Io_ConnectLoop, this, NULL);

	Io_ConnectProc (pIrp);

	m_fCloseLock.LockRelease ();

	return NULL;
    }
	pIrp ->SetResultInfo (ERROR_HANDLE_EOF, NULL);

	return pIrp;
};


class GWinSockAcceptor : public GNetworkAcceptor
{
public :

	void		Listen		(GIrp *	    pIrp	  ,
					 SOCKET	    hSocket	  );
};


#endif




//[]------------------------------------------------------------------------[]
//
#pragma pack (_M_PACK_VPTR)

	struct GOverlappedIoBuffer
	{
	    typedef TSLink <GOverlappedIoBuffer, 0>    Link;
	    typedef TXList <GOverlappedIoBuffer,
		    TSLink <GOverlappedIoBuffer, 0> >  List;

	    Link	    SLink    ;
	    GOverlapped	    oIo      ;
	    void *	    pBuf     ;
	    DWord	    dBufSize ;
	    DWord	    dDataLen ;
	};

#pragma pack ()

//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
/*
void GSerialIoDevice::Send (IoBuffer & IoBuf)
{
};
*/
//
//[]------------------------------------------------------------------------[]

#pragma pack (_M_PACK_LONG)

	struct GIrp_OnCancel : public GIrp_Request
	{
	    GIrp_OnCancel * Init ()
	    {
		GIrp_Request::Init (0x8000);

		return this;
	    };
	};

	struct GIrp_IoRecvRequest : public GIrp_Request
	{
	};

	struct GIrp_IoRecvReply	  : public GIrp_Request
	{
/*
	    GResult		dIoResult ;
	    Byte *		pDataHead ;
	    Byte *		pDataTail ;
*/
	    GIrp_IoRecvReply *  Init ()
	    {
		GIrp_Request::Init (0x0003);

		return this;
	    };
	};

#pragma pack ()

GIrp * GAPI IoCo_RecvLoop (GIrp * pIrp, void *, void *)
{
    return pIrp;
};

GIrp * GAPI DpCo_RecvProc (GIrp * pIrp, GIrp_DpContext dc, void *, void *)
{
    if (NULL !=
       (pIrp  = DispatchIrpHigh (pIrp, dc)))
    {
    }
    return pIrp;
};

void GAPI IoCo_RecvProc (GOverlapped & o, void *, GIrp * pIrp)
{
    GIrp_IoRecvRequest	 * rq = GetCurRequest
   <GIrp_IoRecvRequest> (pIrp);

    if (! o.GrabIoCoResult ())
    {
	GrabOverlappedResult (NULL, o, True);
    }

    GIrp_IoRecvReply	 * on;

    SetCurRequest	(pIrp, on =
    CreateRequest
   <GIrp_IoRecvReply>   (pIrp) ->Init ());

    SetCurCoProc	(pIrp, IoCo_RecvLoop, NULL, NULL);

    if (NULL != 
       (pIrp  = DispatchIrpHigh (pIrp, NULL)))
    {
		CompleteIrp	(pIrp);
    }
};



void Io_RecvProc (GIrp * pIrp)
{
    SetCurDpProc (pIrp, DpCo_RecvProc, NULL, NULL);
};


GIrp * GAPI WorkDpProc (GIrp * pIrp, void *, void *, GIrp_DpContext * dc)
{
    union
    {
	GIrp_Request *		rq;
	GIrp_IoRecvReply *	pIoRecvReply;
    };
	rq = GetCurRequest <GIrp_Request> (pIrp);

    return NULL;
};













//[]------------------------------------------------------------------------[]
//
class GIoDevice
{
public :

virtual	void		    Start	(XReply *, GIrp *);

	void		    Close	(XReply::Request, GIrp *);

protected :

virtual	void		    Create	(GIrp *);

virtual	GIrp *	       IoDp_Create	(GIrp *, GIrp_DpContext, void *) = NULL;

virtual	GIrp *	       IoDp_Close	(GIrp *, GIrp_DpContext, void *) = NULL;

virtual	GIrp *	       IoDp_IoControl	(GIrp *, GIrp_DpContext, void *) = NULL;

static	GIrp * GAPI    IoCo_CloseLoop	(GIrp *, void *, void *);








protected :

virtual	void		    IoControl	(GIrp *	 pIrp	     ,
					 DWord	 dIoCtrlCode ,
					 void *	 pInpBuf     ,
					 DWord	 dInpBufSize ,
				   const void *	 pOutBuf     ,
					 DWord	 dOutBufSize );
protected :

	XReply *		m_pXReply    ;
	GCloseLock		m_hCloseLock ;
};

void GIoDevice::Start (XReply * pXReply, GIrp * pIrp)
{
    if (  pXReply)
    {
       (m_pXReply = pXReply) ->AddRef ();

	return ;
    }
	CompleteIrp (pIrp);
};

void GIoDevice::Close (XReply::Request hReply, GIrp * pIrp)
{
    SetCurDpProc <GIoDevice, void *> 
		 (pIrp, & GIoDevice::IoDp_Close, this, NULL);

/*
    if (NULL !=  (pIrp = m_hCloseLock.Close (pIrp)))
    {
    CompleteIrp  (pIrp);
    }
*/
};











GIrp * GIoDevice::IoDp_Create (GIrp * pIrp, GIrp_DpContext, void *)
{
/*
    if (MJ_DISPATCH == m.uCtrl)
    {
	if (

    }
*/
    if (m_hCloseLock.Open ())
    {
    }
	return pIrp;
};

void GIoDevice::Create (GIrp * pIrp)
{
    SetCurDpProc <GIoDevice, void *>
		 (pIrp, & GIoDevice::IoDp_Create, this, NULL);

    if (m_hCloseLock.Open ())
    {
    if (NULL != DispatchIrpHigh (pIrp, NULL))
    {
		CompleteIrp (pIrp);
    }
    }
};


#if TRUE

GIrp * GAPI GIoDevice::IoCo_CloseLoop (GIrp * pIrp, void *, void *)
{
	        SetCurCoProc (pIrp, IoCo_CloseLoop, NULL, NULL);

    if (NULL !=
       (pIrp  = DispatchIrpHigh (pIrp, NULL)))
    {
    }
	return  pIrp;
};

#else

GIrp * ThisClass::IoDp_Process (GIrp * pIrp, void * a, GIrp_DpParam m)
{
    if ((DP_LISTEN == m.uCtrl) && (a == (void *) & m_pWinPipeAcceptor))
    {{
	CreateRequest <

	CreateRequest <

	return m_pQueueIrpForComplete (
    }}
};


class GIrp_Dispatch_Entry
{

};


GIrp * ThisClass::Process (GIrp * pIrp, void * a, GIrp_DpParam m)
{
};


void ThisClass::Start (GIrp * pIrp)
{
    GIrp * pIrp = CreateIrp ();

    new (pIrp, & m_pWinPipeAcceptor) GWinPipeAcceptor (& m_IUnknown);

    SetCurDpProc <ThisClass, void *>
		 (pIrp, & ThisClass::IoDp_Process, & m_pWinPipeAcceptor);

    m_pWinPipeAcceptor ->Listen (pIrp, ...);

	   pIrp = CreateIrp ();

    new (pIrp, & m_TcpSockAcceptor) GWinSockAcceptor;

    SetCurDpProc <ThisClass, void *>
		 (pIrp, & ThisClass::IoDp_Process, & m_pTcpSockAcceptor);

    m_pWinSockAcceptor ->Listen (pIrp, ...);
};


GIrp * GAPI GIoDevice::IoCo_CloseLoop (GIrp * pIrp, void * pIoDevice, void *)
{
    if (((GIoDevice *) pIoDevice) ->m_hCloseLock.LockAcquire ())
    {
	       SetCurCoProc (pIrp, IoCo_CloseLoop, pIoDevice, NULL);

	pIrp = DispatchIrp  (pIrp, 0/*MJ_DISPATCHIO*/, (WPARAM) 0, (LPARAM) 0);

	((GIoDevice *) pIoDevice) ->m_hCloseLock.LockRelease ();
    }
    else
    {
	pIrp = DispatchIrp  (pIrp, 0/*MJ_DISPATCHIO*/, (WPARAM) 0, (LPARAM) 0);
    }
    if (pIrp)
    {
	       CompleteIrp  (pIrp);
    }
	return NULL;
};

#endif
//
//[]------------------------------------------------------------------------[]






















#if FALSE


#include "r_rrpdevs.h"


//[]------------------------------------------------------------------------[]
//
class Application : 
		    public GApartment
//		    public GWinMsgApartment
{
public :

		    Application ()
		    {
			printf (":: GApplication ()\n");

			m_pIConsole = NULL;
		    };

		   ~Application ()
		    {
			printf ("::~GApplication ()\n");
		    };

static	GResult GAPI	MainStatic  (int, void **, HANDLE);

protected :

	GResult		Main	    (int, void **, HANDLE);

	void		Main	    (int = 0, void ** = NULL);

	void		MainEx	    (int = 0, void ** = NULL);

protected :

	GIrp *		On_RRPService	(GIrp *, XDispatchHandle *, const void *);

	GIrp *		On_Console	(GIrp *, XDispatchHandle *, const void *);

	GIrp *		On_WSDispatch	(GIrp *, XDispatchHandle *, const void *);

	GIrp *		Close		(GIrp *);

	void		XUnknown_FreeInterface		(void *);

protected :

	ISerial *	    m_pIConsole	    ;

	XDispatchHandle *   m_hConsole	    ;
	XDispatchHandle *   m_hRRPService   ;

	GConsole	    m_Console	    ;

	GWinSock	    m_WSAcceptor    ;
	GWinSock	    m_WSConnector   ;

	GService	    m_Service	    ;
};

GIrp * Application::Close (GIrp * pIrp)
{
    if (m_hConsole)
    {
	m_hConsole ->Close ();

	m_hConsole = NULL;
    }

    m_Service	 .QueueStop  (NULL);

    m_Console	 .Close (CreateIrp ());

    m_WSAcceptor .Close	(CreateIrp ());

    m_WSConnector.Close (CreateIrp ());

    return pIrp;
};

/*
void Application::On_Console (GIrp * pIrp, const void *)
{
    if (IoCo_Recv_CtrlCode == GetCurRequest <GIrp_Request> (pIrp) ->GetCtrlCode ())
    {{
	...

	if (ERROR_SUCCESS != rq ->dIoCoResult)
	{
	    pIrp = Close (pIrp);
	}
	else
	{
	    ...

	    pIrp = Close (pIrp);
	}
    }}
};

*/

GIrp * Application::On_Console (GIrp * pIrp, XDispatchHandle *, const void *)
{
    if (IoCo_Recv_CtrlCode == GetCurRequest <GIrp_Request> (pIrp) ->GetCtrlCode ())
    {{
	int	     i, n;
	INPUT_RECORD *  p;

	ISerial::IoCo_Recv * rq = GetCurRequest
       <ISerial::IoCo_Recv> (pIrp);

	    if (ERROR_SUCCESS != rq ->dIoCoResult)
	    {
		printf ("Fail console read %08X\n", rq ->dIoCoResult);

		return Close (pIrp);
	    }

	_asm { mov p, esp}

	n = rq ->dRecvBufLen / sizeof (INPUT_RECORD);

		printf ("%08X %d records readed\n", p, n);

	for (i = 0, p = (INPUT_RECORD *) rq ->pRecvBuf; i < n; i ++, p ++)
	{
	    if (KEY_EVENT == p ->EventType)
	    {
		printf ("(%d) KeyEvent : [%04X %04X %02X '%c - %s'\n", i, p ->Event.KeyEvent.wVirtualScanCode,
									  p ->Event.KeyEvent.wVirtualKeyCode ,
									  p ->Event.KeyEvent.uChar.AsciiChar ,
									  p ->Event.KeyEvent.uChar.AsciiChar ,
									 (p ->Event.KeyEvent.bKeyDown) ? "Press" : "Release");

		if ((p ->Event.KeyEvent.bKeyDown)
		&&  (p ->Event.KeyEvent.dwControlKeyState & (LEFT_CTRL_PRESSED | RIGHT_CTRL_PRESSED))
		&&  (p ->Event.KeyEvent.wVirtualScanCode == 0x2E))
		{
		    printf ("\n\t\tCtrl-C, Done application !!!\n\n");

		    return Close (pIrp);
		}
		else
		if ((p ->Event.KeyEvent.bKeyDown)
		&&  (p ->Event.KeyEvent.wVirtualKeyCode == VK_SPACE))
		{
		    CompleteIrp  (pIrp);

//		    ::MessageBox (NULL, "...Some modl dialog box", "...Report", MB_OK);

		    return NULL;
		};
	    }
	    else if (MOUSE_EVENT == p ->EventType)
	    {
		printf ("(%d) MouseEvent {%d.%d}\n"  , i, p ->Event.MouseEvent.dwMousePosition.X,
							  p ->Event.MouseEvent.dwMousePosition.Y);
	    }
	    else
	    {
		printf ("Some code ....\n");
	    }
	}
    }}
		return pIrp;
};


GIrp * Application::On_WSDispatch (GIrp * pIrp, XDispatchHandle *, const void * pSourcer)
{
    if (pSourcer == & m_WSAcceptor)
    {
    }
    else
    if (pSourcer == & m_WSConnector)
    {
    }
    else
    {
	
    }
	return pIrp;
};

GIrp * Application::On_RRPService (GIrp * pIrp, XDispatchHandle *, const void *)
{
	return pIrp;
};


/*
GIrp * GAPI OnConsole (GIrp * pIrp, void * pConsole, void *)
{
    IoDp_RecvRequest * rq = GetCurRequest
   <IoDp_RecvRequest> (pIrp);

    if (ERROR_SUCCESS != rq ->dIoResult)
    {
	printf ("Fail console read %08X\n", rq ->dIoResult);

      ((GConsole *) pConsole) ->Close (pIrp);

	return pIrp;
    }

	    return pIrp;
};
*/

void Application::XUnknown_FreeInterface (void * pi)
{
    if (pi == this/*& m_XUnknown*/)
    {
	printf ("Free Application::XUnknown\n");

	return;
    }

    GApartment::XUnknown_FreeInterface (pi);
};


GIrp * GAPI Co_Console (GIrp * pIrp, void *, void *)
{
/*last
    printf ("Co_Console %08X (%d)\n", pIrp ->pCurCoEntry, TCurrent () ->GetCurApartment () ->GetRefCount ());
*/
    return pIrp;
};

void Application::MainEx (int, void **)
{
    m_pCoThread = TCurrent ();

    for (int i = 0; i < 10; i ++)

    m_Service.QueueStart (CreateIrp ());

    printf ("Application::Main Run ()\n");

//  Run ();
};


void Application::Main (int, void **)
{
    TIrp <1024>		   sIrp ;
    GIrp *	  pIrp = & sIrp ;

//  GetCoHWND ();


    SetCurCoProc (pIrp, Co_Console, NULL, NULL);

    XDispatch *	  pXDispatch = NULL;

/*
    XDispatch *	  pXDispatch = CreateXDispatcher <Application> 
			    (& Application::XxxxOn_Dispatch, this);





    if (ERROR_SUCCESS == m_Console.Create (...))
    {
	m_Console.Connect (pIrp, m_hConsole = CreateXDispatchCallBack (pXDispatch, 
    }





*/

    m_Console.Connect	    (  pIrp	    ,
			       pXDispatch = CreateXDispatcher <Application>
			    (& m_hConsole   ,
			     & Application::On_Console, NULL, this));

			       pXDispatch = (pXDispatch ->Release (), NULL);

#if FALSE
    {{
	GWinSock::Address addr (GWinSock::INet4, "127.0.0.1:50000");

//			     m_WSAcceptor.Create  (GWinSock::INet4,

	if (ERROR_SUCCESS == m_WSAcceptor.Create  (addr, GWinSock::IP_TCP))
	{
	}
    }}

    if (ERROR_SUCCESS == m_WSAcceptor.Create  (GWinSock::INet4 ,
					       GWinSock::IP_TCP,
					       GWinSock::stream, 0))
    {{
	GWinSock::Address   addr;

	addr.init (GWinSock::INet4, "127.0.0.1:5000");

//	GWinSock::Address   addr (GWinSock::INet4, "127.0.0.1:50000");
/*
	m_WSAcceptor.QueueForListen
			    (  NULL
			     pXDispatch = CreateXDispatcher <Application>
			    (  NULL,
			     & Application::On_WSDispatch, & m_WSAcceptor, this), addr, 0);
			     pXDispatch = (pXDispatch ->Release (), NULL);
*/

    if (ERROR_SUCCESS == m_WSConnector.Create (GWinSock::INet4 ,
					       GWinSock::IP_TCP,
					       GWinSock::stream, 0))
    {{
//	addr.init (GWinSock::INet4, "127.0.0.1:50001");

//	m_WSConnector.Connect
//		 (pXDispatch ->Bind (NULL, NULL, & m_WSConnector), addr, 0);
    }}
    }}

#endif

/*
    DECLARE_INTERFACE_ (XClassBuilder, IUnknown)
    {
    }



    RegisterClassObject	    (
    

    QueryInterface	    (REFCLSID



    QueueForQueryInterface  (REFCLSID, REFIID,



    DECLARE_INTERFACE_ (XBuilder, IUnknown)
    {
    STDMETHOD_ (void,	 CreateInstance)(XDispatchCallBack *, int nArgs, void * pArgs);
    };


    void    XBuilder_CreateInstance	(XDispatchCallBack *, REFCLSID clsid, int nArgs, void * pArgs);



	-   -	-   -	-   -	-   -	-   -	-   -	-   -

    Root [CLSID_IServiceRoot, IID_XDispatch, pXDispatch]

	- XObject_Instance  [REFCLSID, IID_IUnknown, pIUnknown]
	- XObject_Instance1 [REFCLSID, IID_IUnknown, pIUnknown]
	- ...
	- XObject_InstanceM [REFCLSID, IID_IUnknonw, pIUnknown]


	- Builder_Instance  [REFCLSID, IID_XBuilder, pXBuilder]
	- Builder_Instnace1 [REFCLSID, IID_XBuilder, pXBuilder]
	-  ...
	- Builder_InstnaceN [REFCLSID, IID_XBuilder, pXBuilder]






class GObjectBuilder : public XBuilder
{
};


	RegisterRRPDeviceService (...,



*/

    {
	IUnknown   *	pIUnknown	= NULL;
	IRRPDeviceService * 
			pIRRPDevService = NULL;
	IRRPDevice *	pIRRPDev	= NULL;

	if (ERROR_SUCCESS == CreateRRPDeviceService (pIRRPDevService, NULL))
	{
	if (ERROR_SUCCESS == pIRRPDevService ->CreateRRPDevice 
						    ((void **) & pIUnknown ,
						     IID_IUnknown	   ,
						     NULL		   ,
						     IID_IUnknown	   ))
	{
	}
	    pIRRPDevService ->Release ();
	    pIRRPDevService = NULL;
	}
    }

/*
    QueryRRPService	    (  ::CreateIrp  (),
			       pXDispatch = CreateXDispatcher <Application>
			    (  NULL	      ,
			     & Application::On_RRPService, NULL, this));

			       pXDispatch = (pXDispatch ->Release (), NULL);

    for (int i = 0; i < 9; i ++)

    m_Service.QueueStart (CreateIrp ());

    printf ("Application::Main Run ()\n");

/*
1:
    RegisterClassBuilder    (CLSID_RRPService, Build_RRPService);

    m_hService = CreateXDispatchCallBack <Application>
			    (  0		, 
			    & Application::On_RRPService	, this


    GetClassObject    (	 m_hService	, 
		       & Application::Xxxx_On_RRPService, this, CLSID_RRPService, ) 

    RevokeClassBuilder	    (CLSID_RRPService);

    ...

    m_hSerivce ->Close ();

  
2:
    m_hXDispatchCallBackRRPServiceCallBackService = CreateXDispatchCallBack <Application>
			    (









    RegisterService (& CLSID_IRRPService);
    ...

    if (ERROR_SUCCESS == QueryInterface (m_pIRRPService, NUL, CLSID_CLSRRPService, IID_IRRPService, IID_IRRP& m_pIRRPService)

    RegisterService (CID_SU3000);


    if (ERROR_SUCCESS == QueryInterface (CID_SU3000, IID_ISU3000, 



    OnExit :


    UnRegisterService (NULL);

*/

//  m_Console.QueueForRecv (pIrp, NULL, NULL);

/*
    SetReleaseCoProc (pIrp = CreateIrp (), pXDispatch);

    m_Service.Start  (pIrp);
*/


//  Run ();

    printf ("Application::Main exit (%d)\n", TCurrent () ->GetCurApartment () ->GetRefCount ());

/*
1)
    XDispatch *   pXDispatch = CreateXDispatcher <Application> 
			    (& Application::XxxxOn_Dispatch, this);

    m_Console.Connect (pIrp, pXDispatch, & m_xConsole);

		  pXDispatch ->Release ();
		  pXDispatch = NULL;

    WaitForComplete   (m_xConsole, INFINITE);


2)  m_pXDispatch = CreateXDispatcher <Application>
		      (& Application::XxxxOn_Dispatch, this);

    m_Console.Connect (pIrp, pXDispatch, NULL);


3)  m_Console.Connect (m_pXDispatch ->CreateXDispatchRequest 
		      (NULL, & m_XConsole, & m_Console), ...);

4)  m_Console.Connect (m_pXDispatch ->CreateXDispatchRequest
		      (NULL, NULL, & m_Console), ...);

    m_Console.Connect (NULL, ...);


    m_pXDispatch ->CreateXDispatchRequest
   (pIrp, & m_XConsole, & m_Console);







    Some next version :


1)

    m_Console.Create (CreateXBindingHandle <Application>
		     (& m_xConsole, & Application::On_Console, this),)















    m_Console.Connect (pIrp, pXDispatch, & m_xConsole);

    pXDispatch ->Release ();
					    m_xConsole.Cancel  ();
					    m_xConsole = NULL;






    XDispatch * pXDispatch = CreateXDispatcher
			     <Application> (XxxxOn_Dispatch, this);

    m_WinSock.Connect (NULL, pXDispatch, & m_xWinSock);
    ...
    ...

    if (WaitForCompletion (m_xWinSockConnect, 200))
    {
	...
    }
    else
    {
	m_xWinSockConnection ->Cancel ();
	m_xWinSockConnection = NULL;
    }




    m_pXDispatch = CreateXDispatcher
		   <Application> (On_Dispatch, this);


    m_WinSock.Connect (NULL, m_pXDispatch = CreateXDispatcher, & m_xWinSock);



    m_Console.Connect (pIrp, pXDispatch, & m_xConnect);

    if (WaitForCompletion (m_xConnect, 200))
    {
	...
    }
    else
    {
	m_xConnect ->Cancel (& m_xConnect);
    }

    while (XDispatchRequest::Completed == m_xConnect.GetStatus ())
    {
    }



*/
};

//[]------------------------------------------------------------------------[]
//

static TInterlockedPtr <IWakeUp *> s_pIWakeUp = NULL;
static DWord			   s_Start    =	   0;

static Bool GAPI Xxxx_Co_Wait (IWakeUp * pIWakeUp, IWakeUp::Reason Reason, void *, void * pArgument)
{
    printf ("Xxxx_Co_Wait %08X (%d) {%4d ms}\n", TCurrent () ->GetId (), TCurrent () ->GetType (), ::GetTickCount () - s_Start);

    if (IWakeUp::OnCancelled == Reason)
    {
    printf ("Xxxx_Co_Wait cancelled\n");

	goto lDestroy;
    }

    if (20 > ((int &) pArgument) ++)
    {
	pIWakeUp ->QueueWaitFor (NULL, Xxxx_Co_Wait, NULL, pArgument, 500);

	return True ;
    }

lDestroy :

	s_pIWakeUp.Exchange (NULL);

	pIWakeUp ->Release  ();

	return False;
};

static GIrp * GAPI Xxxx_Co_Work (GIrp * pIrp, void *, void * pArgument)
{
    printf ("Xxxx_CoWork::Entry %08X (%d)\n", TCurrent () ->GetId (), TCurrent () ->GetType ());

    ::Sleep (1000 + (500 * (int) pArgument));

    printf ("Xxxx_CoWork::Leave %08X (%d) {%4d ms}\n", TCurrent () ->GetId (), TCurrent () ->GetType (), ::GetTickCount () - s_Start);

    if (s_pIWakeUp)
    {
	if (5000 < (::GetTickCount () - s_Start))
	{{

	printf ("Xxxx_Cancel condition !!!!!!!!!\n");

	    IWakeUp * pIWakeUp = s_pIWakeUp.Exchange (NULL);

	    if	 (pIWakeUp)
	    {
		pIWakeUp ->Cancel  ();
	    }
	}}
    }

    return pIrp ;
};

static GIrp * GAPI Xxxx_Co_Exit (GIrp * pIrp, void *, void * pArgument)
{
    printf ("\t\t\tXxxx_Co_Exit (%d) {%4d ms}\n", (int) pArgument, 1000 + (500 * (int) pArgument));

    return pIrp;
};

static void DoSome ()
{
    s_Start = ::GetTickCount ();

/*
    IWakeUp * pIWakeUp;

    if (ERROR_SUCCESS == TPool () ->CreateWakeUp (pIWakeUp))
    {

	s_pIWakeUp.Exchange	(pIWakeUp);

	pIWakeUp ->QueueWaitFor (NULL, Xxxx_Co_Wait, NULL, (void *) 0, 0);
    }
*/
    GIrp * pIrp;

    for (int i = 0; i < 20; i ++)
    {
		      pIrp = CreateIrp ();

	SetCurCoProc (pIrp, Xxxx_Co_Work, NULL, (void *) i);

	TPool () ->QueueIrpForComplete (GThread::Free, pIrp);


		      pIrp = CreateIrp ();

	SetCurCoProc (pIrp, Xxxx_Co_Exit, NULL, (void *) i);

	SetCurCoProc (pIrp, Xxxx_Co_Work, NULL, (void *) i);

	TPool () ->QueueIrpForComplete (GThread::WorkIo, pIrp);


		      pIrp = CreateIrp ();

	SetCurCoProc (pIrp, Xxxx_Co_Work, NULL, (void *) i);

	TPool () ->QueueIrpForComplete (GThread::Work, pIrp);

    }


    printf ("All createed\n");
};

static void DoSome2 ()
{
    IWakeUp   * pIWakeUp   ;
    IIoCoWait * pIIoCoWait ;

    CreateWakeUp   (pIWakeUp  );

//  CreateIoCoWait (pIIoCoWait);

    pIIoCoWait ->AddRef  ();
    pIIoCoWait ->Release ();
    pIIoCoWait ->Release ();

    pIWakeUp ->AddRef  ();
    pIWakeUp ->Release ();
    pIWakeUp ->Release ();
    pIWakeUp = NULL;


    pIIoCoWait = NULL;
};













//[]------------------------------------------------------------------------[]
//
class GIrp_DispatchLoop
{
};

class GIrp_Timer
{
public :

		GIrp_Timer ()
		{
		    m_pIApartment   = NULL;
		    m_pRunIrp	    = NULL;
		    m_dPeriod	    =	 0;
		};

	void		Run		    (GIrp *, DWord dDueTimeout ,
						     DWord dPeriod     );

	void		Break		    ();

	void		Cancel		    ();

protected :

static	GIrp * GAPI	Xxxx_Co_Timeout	    (GIrp * pIrp, GIrp_Timer *, void *);

static	GIrp * GAPI	Xxxx_Co_Run	    (GIrp * pIrp, GIrp_Timer *, void *);

protected :

	IApartment *	    m_pIApartment   ;
	GIrp *		    m_pRunIrp	    ;

	GTimeout	    m_tPeriod	    ;
	DWord		    m_dPeriod	    ;
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
GIrp * GAPI GIrp_Timer::Xxxx_Co_Timeout (GIrp * pIrp, GIrp_Timer * pThis, void *)
{
    if ((NULL == pThis ->m_pRunIrp) || pIrp ->Cancelled ())
    {
	return pIrp;
    }

    DWord dCurTick   = GetTickCount ();

    SetCurCoProc (pIrp, (GIrp_CoProc) Xxxx_Co_Timeout, pThis, NULL);

    if (! pThis ->m_tPeriod.Occured	 (dCurTick))
    {
	pThis ->m_pIApartment ->QueueIrpForComplete
       (pIrp, pThis ->m_tPeriod.Relative (dCurTick));

	return NULL;
    }

    if (INFINITE != pThis ->m_dPeriod)
    {
	pThis ->m_tPeriod.Activate 
       (pThis ->m_dPeriod, dCurTick);
    }
	return DispatchIrpHigh (pIrp, NULL);
};

GIrp * GAPI GIrp_Timer::Xxxx_Co_Run (GIrp * pIrp, GIrp_Timer * pThis, void *)
{
    if (pThis ->m_pIApartment)
    {
	pThis ->m_pIApartment ->Release ();

	pThis ->m_pIApartment = NULL;
    }
	return pIrp;
};

void GIrp_Timer::Break ()
{
	m_pRunIrp    = NULL;
};

void GIrp_Timer::Cancel ()
{
    GIrp * pIrp = m_pRunIrp;
		  m_pRunIrp = NULL;
    if (pIrp)
    {
	CancelIrp (pIrp);
    }
};

void GIrp_Timer::Run (GIrp * pIrp, DWord dDueTimeout ,
				   DWord dPeriod     )
{
    m_pRunIrp	   = pIrp    ;

#if TRUE

    m_pIApartment  = TCurrent () ->GetCurApartment () ->QueryIApartment ();

    m_pIApartment ->AddRef  ();
    m_pIApartment ->Release ();

#else

   (m_pIApartment  = & TCurrent () ->GetCurApartment () ->m_IApartment) ->AddRef ();

#endif

    SetCurCoProc (pIrp, (GIrp_CoProc) Xxxx_Co_Run, this, NULL);

    DWord dCurTick = GetTickCount ();

    m_dPeriod	   = dPeriod ;

    if (INFINITE  != dDueTimeout)
    {
	m_tPeriod.Activate (dDueTimeout, dCurTick);
    }
    else
    if (INFINITE  != dPeriod)
    {
	m_tPeriod.Activate (dPeriod    , dCurTick);
    }

    SetCurCoProc (pIrp, (GIrp_CoProc) Xxxx_Co_Timeout, this, NULL);

    CompleteIrp  (pIrp);
    
//  m_pIApartment ->QueueIrpForComplete (pIrp);
};
//
//[]------------------------------------------------------------------------[]


static	int	s_count	= 0;

GIrp * GAPI SomeDp_Proc (GIrp * pIrp, GIrp_DpContext, void *, void * pArgument)
{
    printf ("Hello from SomeDp_Proc %d\n", s_count ++);

    if (1000 < s_count)
    {
	((GIrp_Timer *) pArgument) ->Break  ();
//	((GIrp_Timer *) pArgument) ->Cancel ();
    }
	return pIrp;
};

GIrp * GAPI SomeCo_Proc (GIrp * pIrp, void *, void *)
{
    printf ("Hello from SomeCo_Proc %s\n", pIrp ->Cancelled () ? "Canceled" : "Normal");

    return pIrp;
};

static void DoSome4 ()
{
    GIrp       * pIrp  = CreateIrp ();

    SetCurCoProc (pIrp, SomeCo_Proc, NULL, NULL);

#if FALSE

    SetCurDpProc (pIrp, SomeDp_Proc, NULL, NULL);

    (new (pIrp, (GIrp_Timer **) NULL) GIrp_Timer) ->Run 
    (pIrp, NULL, 0, 500);

#else

    GIrp_Timer * pTimer = new (pIrp, (GIrp_Timer **) NULL) GIrp_Timer;

    SetCurDpProc (pIrp, SomeDp_Proc, NULL, pTimer);

		 pTimer ->Run (pIrp, 0, 580);
/*
    for (int i = 0; i < 100; i ++)
    {
		 pIrp = CreateIrp ();

		 pTimer = new (pIrp, (GIrp_Timer **) NULL) GIrp_Timer;

    SetCurDpProc (pIrp, SomeDp_Proc, NULL, pTimer);

		 pTimer ->Run (pIrp, 5000, 1000 + (10 * i));
    }
*/
//  CancelIrp    (pIrp);

/*
//..............
//
    SetCurDpProc	(pIrp, GetCurApartment (), SomeDp_Proc, NULL, NULL);

    SetCurRequest	(pIrp,
    CreateRequest
   <CreateRequest	(pIrp) ->Init (...));

    QueueIrpForDispatch (pIrp, 1000, 500);
//
//..............
    

//..............
//
    

    SetCurDpProc <GSerivce, void *>
   (m_hWinSockListener = CreateIrp (),
   <GService 
			 GetCurApartment (), SomeDp_Proc, NULL, NULL);
    QueueIrpForDispatch	(m_hWinSockListener, 0, 1000);


...

    SetCurDpProc	(pIrp, GetCurApartment (), SomeDp_Proc, NULL, NULL);


    QueueIrpForDispatch (pIrp, NULL, SomeDp_Proc , NULL, NULL);

    QueueIrpForDispatch (pIrp, NULL, SomeDp_Proc2, 

    SetCurDpProc	(pIrp, GetCurApartment (), SomeDp_Proc, NULL, NULL);

    SetCurCoProc	(pIrp, GetCurA

    CreateRecvRequest	(pIrp, m_pRecvBuf, sizeof (m_pRecvBuf),

    QueueIrpForDispatch	(pIrp,



    if (m_hWinSockListener)
    {
	CancelIrp (m_hWinSockListener);
		   m_hWinSockListener = NULL;
    }


    SetCurDpProc	(pIrp, GetCurApartment (), SomeDp_Proc, NULL, NULL);

    QueueIrpForRecv	(pIrp, ...);



    QueueForRecv	(...)

    CreateRecvRequest	(pIrp, m_pRecvBuf, sizeof (m_pRecvBuf))

    QueueIrpForDispatch (pIrp, 0, 0);



void GAPI QueueIrpForDispatch (GIrp * pIrp, DWord dDueTime = 0	,    








    QueueIrpForDispatch (pIrp, 0, 1000);




		void	GAPI	QueueIrpForDispatch (GIrp *, DWord dDueTime = 0	      ,
							     DWord dPeriod  = INFINITE);

		void	GAPI	QueueIrpForComplete (GIrp *, DWord dDueTime = 0);
*/


#endif

//  CancelIrp (pIrp);
};


static void DoSome3 ()
{
    IRRPDeviceService * pIDevService = NULL;
    IRRPUrp *		pIUrp	     = NULL;

    if (ERROR_SUCCESS == CreateRRPDeviceService (pIDevService, NULL))
    {
    if (ERROR_SUCCESS == pIDevService ->CreateRRPDevice ((void **) & pIUrp,
							 IID_NULL	  ,
							 NULL		  ,
							 IID_NULL	  ))
//							 IID_IRRPUrp	  ))
    {
	pIUrp	     ->Release ();
    }
	pIDevService ->Release ();

	pIDevService   = NULL;
    }
};
//
//[]------------------------------------------------------------------------[]





/*
//..............
//
static void GThread::QueueIrpForDelay (GApartment *, 


//
//..............






void GWinMsgApartment::CompletePacket ()
{
    if (NULL == m_hCoWnd)
    {
	GWaitIoApartment::CompletePacket ();

	return;
    };

    if (TCurrent () ->GetId () != m_pCoThread ->GetId ())
    {
    if (m_qCoEvent.CompareExchange (0, 1) == 0)
    {
	::PostMessage (m_hCoWnd, WM_NULL, (WPARAM) 0, (LPARAM) this);
    }
	return;
    }
	m_qCoEvent.Exchange	   (   1);

	::SendMessage (m_hCoWnd, WM_NULL, (WPARAM) 0, (LPARAM) this);
};

//[]------------------------------------------------------------------------[]
//
void GApartment::QueueIrpForComplete (GIrp * pIrp, DWord dTimeout)
{
    if (0 < dTimeout)
    {
       (m_pQueueIrpForDelay)(this, pIrp, dTimeout)

	return;
    }

    LockAcquire ();

	if     (m_pCoCur)
	{
			    m_qCoQueue.append	 (pIrp);

	    if (m_pCoCur == pIrp)
	    {
		m_pCoCur  = m_qCoQueue.extract_first ();
	    }
		pIrp	  = NULL;
	}
	else
	{
		m_pCoCur  = pIrp;
	}

    LockRelease ();

    if (pIrp && (m_qCoSignal.CompareExchange (1, 0) == 0	 )

	     && (m_pCoThread ->GetId () != TCurrent () ->GetId ()))
    {
       (m_pSetCoSignal)(this);
    }
};

void GThreadPool::QueueIrpForComplete (GThread::Type, GIrp * pIrp)  // Ok...
{
};

void GThread::QueueIrpForComplete (GIrp * pIrp, DWord dTimeout)
{
    if (0 < dTimeout)
    {
    }
    else
    {
	QueueIrpForComplete (pIrp);
    }
};

void GThread::QueueIrpForComplete (GIrp * pIrp, DWord dTimeout)	    // Ok...
{
    if (GetCurApartment ())
    {
	GetCurApartment () ->QueueIrpForComplete (pIrp, dTimeout);
    }
    else
    {
	::QueueIrpForComplete (GThread::WorkIo, pIrp, dTimeout);
    }
};

void GAPI QueueIrpForComplete (GIrp * pIrp, DWord dTimeout)	    // Ok...
{
    GThread * th = TCurrent ();

    if (th)
    {
        th ->QueueIrpForComplete (pIrp, dTimeout);
    }
    else
    {
	::QueueIrpForComplete (GThread::WorkIo, pIrp, dTimeout);
    }
};
//
//[]------------------------------------------------------------------------[]








void GAPI QueueIrpForDispatch (GIrp * pIrp, DWord dDueTime,
					    DWord dPeriod )
{
    GThread * th = TCurrent ();

    if (th)
    {
	th ->QueueIrpForDispatch (pIrp, dDueTime, dPeriod);
    }
    else
    {
	::QueueIrpForDispatch (GThread::WorkIo, pIrp, dDueTime, dPeriod);
    }
};












void GThread::QueueIrpForDispatch (GIrp * pIrp, DWord dTimeout)
{
};

void GAPI QueueIrpForDispatch (GIrp *, DWord dDueTime,
				       DWord dPeriod )
{
    GIrp_Timer * pTimer = new (pIrp, (GIrp_Timer **) NULL) GIrp_Timer;

    pTimer ->Run (pIrp, dDueTime, dPeriod);
};

    GIrp_Context * pContext = new (pIrp, (GIrp_Context **) NULL) GIrp_Context;

    SetCurDpProc  (GApartment *, pIrp, SomeDp_Proc, NULL, pContext);

    QueueIrpForDispatch (









class GThread
{
...

	IApartment *		    m_pContext	;
}

void GThread::QueueIrpForDispatch (GIrp * pIrp, DWord dDueTime,
						DWord dPeriod ,
{
    
};




void GAPI QueueIrpForComplete (GIrp *, DWord dDueTime)

class GApartment : public IApartment
{
protected


static	void		QueueIrpForComplete (GIrp * pIrp, DWord dTimeout);

	void		


protected :

	GThread *		    m_pCoThread ;
	TInterlocked <int>	    m_qCoSignal	;

	GIrp *			    m_pCoCur    ;   // (*)
	GIrp_List		    m_qCoQueue  ;   // (*)
};

void 


GApartment::GApartment ()
{
    if (TCurrent () ->WaitIoThread ())
}

class Application : public GApartment
*/


/*

Application::Application ()
{
    GIrp * pIrp = CreateIrp ();

    pIrp ->...

    ...



    RegisterApartment (this);
}

Application::~Application ()
{
    UnRegister
};


DWORD Application::Main (int nArgs, void ** pArgs, HANDLE hResume)
{
    Application	a;

    return a.Run (nArgs, pArgs, hResume);
};

//..................

DWORD Application::Run (int nArgs, void ** pArgs, HANDLE hResult)
{
    ....


    GApartment::Run ();
};

GResult Application::Main (int nArgs, void ** pArgs, HANDLE hResume)
{
    TCurrent () ->SetCurApartment (...);


    TCurrent () ->SetCurApartment (...);
};

DWORD Application::Main (int nArgs, void ** pArgs, HANDLE hResume)
{
    Application	* a = new Application ();

    return (a) ? a ->Main (nArgs, pArgs, hResume);
};



{
    QueueWorkItem (GThread::WorkIo, Application::Main, 0, NULL, NULL);
}



template <int W = MAXIMUM_WAIT_OBJECTS> class TWaitIoThread : public TWakeUpThread <GWaitIoThread, W>
{};

template <int W = MAXIMUM_WAIT_OBJECTS> class TWinMsgThread : public TWakeUpThread <GWinMsgThread, W>
{};


template <class T, class W>
GResult PrimaryThread (GWinMainProc pMainProc     ,
		       int	    nMainProcArgs ,
		       void **	    pMainProcArgs ,
		       const T *    pThreadType    = NULL)
{
    TPrimaryThread <T> th;
    W		       w

    return (pMainProc)(nMainProcArgs, pMainProcArgs, NULL);
};

template <class T, WaitIoThread <




GResult GWaitIoThread::Run (GWinMainProc, int, void **, HANDLE)
{
    
};



int main ()
{
    {{
	TPrimaryThread <TWaitIoThread <> > th;

	TPrimaryThread <TWinMsgThread <> > th;

	th ->Run ((Application::Main)(0, NULL, NULL));
    }}
};




DWORD Application::Main (int nArgs, void ** pArgs, HANDLE hResume)
{
    if (True)
    {{
	Application a;

	a.Main (nArgs, pArgs, hResume);
    }}


    delete (new Application ->Run ());
};




DWORD Application::Main (int nArgs, void ** pArgs, HANDLE hResume)
{
    return new Application (nArgs, pArgs) ->Run ();
};

int main ()
{
    if (GWinInit ())
    {{
	QueueWorkItem (GThread::Work, Application::Main, 0, NULL);

	GWinDone ();
    }}
}

class Application : public GApartment
{
};

DWord Application::Main (int nArgs, void ** pArgs, HANDLE hResume)
{
    return RunApartment (new Application (), nArgs, pArgs, hResume);
};

class WinMsgApp : public GWinMsgApartment
{
};

DWord WinMsgApp::Main (int nArgs, void ** pArgs, HANDLE hResume)
{
    WinMsgApp	a;

    return RunApartment (a, nArgs, pArgs, hResume);
};

int main ()
{
    if (GWinInit ())
    {
//..............................
1)	PrimaryThread <TPrivateWakeUpThread <GThread, 32> > (WinMsgApp::Main, 0, NULL);

	PrimaryThread <GThread> (WinMsgApp::Main, 0, NULL);

	CreateThread  <TPrivateWakeUpThread <GThread, 32> > (WinMsgApp::Main, 0, NULL);

	CreateThread  <GThread>	(WinMsgApp:Main, 0, NULL);
//..............................
	PrimaryThread <TSharedWakeUpThread  <GThread, 32 > > (Application::Main, 0, NULL);

	PrimaryThread <GThread> (Application::Main, 0, NULL);

	CreateThread  <TSharedWakeUpThread  <GThread, 32 > > (Application::Main, 0, NULL);

	CreateThread  <GThread> (Application::Main, 0, NULL);
//..............................
2)	QueueWorkItem (GThread::Main, Application::Main, 0, NULL);

	GWinDone ();
    }
};


*/




DWord Application::MainStatic (int, void **, HANDLE hResume)
{
    Application	a;

    return RunApartment (& a, 0, NULL, hResume);

/*
    a.Run (TCurrent (), 0, NULL, hResume);

	   RunApartment (

    Application a (NULL)

    return new (newAllocator) Application ->Run (0, NULL, hResume);
*/



#if FALSE

//    TCurrent () ->SetCurApartment ( & a);

    a.m_pCoThread = TCurrent ();

    {{
	if (hResume)
	{
	    ::SetEvent (hResume);
	}

//	a.m_WakeUpPort.EnterWaitLoop ();

	    DoSome3  ();

//	    DoSome2  ();

//	    DoSome   ();

//	    a.Main   ();

//	    a.MainEx ();

//	a.m_WakeUpPort.LeaveWaitLoop ();
    }}

//  TCurrent () ->SetCurApartment (NULL);

    

    return ERROR_SUCCESS;

/*
    T	    Apartment  ;

    GResult InitResult = Apartment.Create (cArgs, pArgs);

    

    if (ERROR_SUCCESS == 
       (InitResult     = a.InitApartment (cArgs, pArgs)))
    {
			 a.Main ();
    }

    

    TCurrent () ->
*/

#endif
};

GResult Application::Main (int, void **, HANDLE hResume)
{
    printf ("\t\tMain <<<\n");

    if (hResume)
    {
	::SetEvent (hResume);
    }

	DoSome3 ();

    printf ("\t\t>>> Main\n");

	return ERROR_SUCCESS;
};


GResult	GAPI QueueWorkItem (GThread::Type tType, GWinMainProc pWorkProc ,
						 int	      nWorkArgs ,
						 void **      pWorkArgs )
{
    return TPool () ->QueueWorkItem (tType, pWorkProc,
					    nWorkArgs,
					    pWorkArgs);
};


#if FALSE   //Main entry point !!!!

int main ()
{
    if (GWinInit ())
    {{
    if (GWinSockStartUp ())
    {{
#if TRUE
	    PrimaryThread <TPrivateWakeUpThread <GThread, 32> > (	    Application::MainStatic, 0, NULL);
#else
	    HANDLE	     hThread = NULL;

	if (ERROR_SUCCESS == 
	    CreateThread  <TPrivateWakeUpThread <GThread, 32> > (& hThread, Application::MainStatic, 0, NULL))
	{
	    printf ("Wait for Secondary thread <<\n");

	    ::WaitForAny (1, hThread, INFINITE);

	    printf (">> Done Wait for Secondary thread\n");
	}
#endif
	GWinSockCleanUp ();
    }}
	GWinDone ();
    }}

	return 0;
};



#endif
//
//[]------------------------------------------------------------------------[]





    DECLARE_INTERFACE_ (XBuilder, IUnknown)
    {
    STDMETHOD_ (void,	 CreateInstance)(XDispatchCallBack *, int nArgs, void * pArgs) PURE;
    };



    typedef void (GAPI *    XBuilderProc)(GIrp * pIrp);

    void GAPI	RegisterXBuilder    (REFCLSID, XBuilderProc);







    class GObjectBuilder : public XBuilder
    {
    public :

    static void GAPI	    XBuilderProc    (GIrp * pIrp);

    protected :

//	    XBuilderProc
    };


/*
    struct GWindowClass
    {
	HINSTANCE	Registered		   ;	// Initiated INT_WINDOW_CLASS or
							//	     EXT_WINDOW_CLASS
	LPCTSTR (GAPI * GetInfo) (WNDCLASSEX *	  );

	Bool		Register (HINSTANCE = NULL);
	void	      UnRegister ();
    };

#define DECLARE_INT_WINDOW_CLASS(theName)				\
    protected :								\
    virtual GWindowClass &  GetWindowClass    () const			\
	    {								\
		    return  s_WndClass;					\
	    };								\
    private :								\
    static  LPCTSTR GAPI    GetWindowClassInfo (WNDCLASSEX * wc = NULL)	\
	    {								\
		    LPCTSTR pName      = theName      ;			\
									\
		if (wc)							\
		{							\
		    ::GetWindowClassInfo (pName, wc);			\
		}							\
		    return  pName;					\
	    };								\
    static  GWindowClass    s_WndClass


#define DEFINE_INT_WINDOW_CLASS(theClass)				\
GWindowClass theClass::s_WndClass =					\
{ INT_WINDOW_CLASS, theClass::GetWindowClassInfo}
*/

    struct GObjectClassInfo
    {
	XBuilderProc	pBuilderProc  ;
	LPCCLSID	pClsId	      ;
    };

    struct GObjectClass
    {
	void *		Registered    ; // Interlocked access !!!
	ULONG		cRegCount     ; // Interlocked access !!!

	void (GAPI *	GetInfo) (GObjectClassInfo &);

	void		Register ();
	void	      UnRegister ();
    };


#define DECLARE_OBJECT_CLASS(RefClsId)					\
    protected :								\
    virtual GObjectClass &  GetObjectClass	() const		\
	    {								\
		    return  s_ObjClass;					\
	    };								\
    private :								\
    static  void GAPI	    GetObjectClassInfo  (GObjectClassInfo & oc)	\
	    {								\
		oc.pBuilderProc	=   CreateInstance  ;			\
		oc.pClsId	= & RefClsId	    ;			\
	    };								\
    static  GObjectClass    s_ObjClass

#define DEFINE_OBJECT_CLASS(theName)					\
									\
    GObjectClass   theName::s_ObjClass =				\
									\
    { NULL, 0, theName::GetObjectClassInfo }


class SomeClass
{
DECLARE_OBJECT_CLASS (IID_IUnknown);

static	void GAPI	CreateInstance		(GIrp *); 
};

DEFINE_OBJECT_CLASS  (SomeClass);

void GAPI SomeClass::CreateInstance (GIrp * pIrp)
{
};








void SomeMainProc ()
{
/*
    typedef GIrp *  (GAPI *   XDispatchProc)(GIrp *, XDispatch::Handle *, void *);

    typedef GIrp *  (T::*     TDispatchProc)(GIrp *, XDispatch::Handle *);

    XDispatch *	     GAPI  CreateXDispatcer (XDispatchProc, const void *);


    void GAPI CreateXDispatchCallBack (XDispatch::CallBack *, XDispatch *, XDispatchProc, void *);

    void GAPI CreateXDispatchCallBack (XDispatch::CallBack *, XDispatch *, XDispatchProc, T *   );

    void GAPI CreateXDispatchCallBack (XDispatch::CallBack *, XDispatch *,		  void *);


GIrp * Application::Xxxx_On_Console (GIrp * pIrp, XDispatch::Handle)
{
    XDispContext * pDispContext = m_pXDispatch ->GetDispApart
    XDispContext * pCurrContext = GetCurDispContext ();

    if ((NULL == pDstContext) || (pDstContext != pCurContext))
    {
	pDstContext ->LockAcquire ();

	...

    	pDstContext ->LockRelease ();
    }

    if (! NeedDstCall)
    {
	return pIrp;
    }
	m_pXDispatch ->Invoke (pIrp, & m_On_Console;

	return NULL;
};

void Application::Init (...)
{
    m_Console.Connect (CreateXDispatchCallBack <Application>
		      (& m_hConsole, NULL, & Application::Xxxx_On_Console, this),






};

void Application::ReInit (...)
{
    if (m_hConsole)
    {
	m_hConsole ->Close ();

	m_hConsole = NULL;
    }

    m_Console.Connect (CreateXDispatchCallBack <App

    m_hConsole.Close 
};





GIrp * Application::Xxxx_On_Console


    CreateXDispatchCallBack <Application>
			    (& m_On_Console, & Application::Xxxx_On_Console, this);







    void GAPI CreateXDispatchCallBack (XDispatchCallBack::Handle *, & Application::Xxxx_On_Root, 



    CreateXDispatchCallBack (& m_On_Root, & Application


    SetCurXDispatchCoProc   (pIrp, ... & Application::Xxxx_On_Console);



    CreateXDispatchCallBack (& m_On_Root, NULL, & Application::On_Root,



*/
};



/*

texte:

SomeClass::RegisterClassBuilder







SomeClass::SomeClass ()
{
    printf ("Build SomeClass\n");
};
*/

//SomeClass   s;


    struct PPP
    {
	LPCCLSID	RefClsId    ;
	void *		Registered  ;
    };


    PPP p = {& IID_IUnknown, NULL};


/*
void GObjectClass::Register ()
{
    if (0 == _Sys_InterlockedIncrement (



//    ULONG    cRegCount = m_cRegCount ++;

    if (1  < cRegCount)
    {
	return;
    }
    if (1 == cRegCount)
};
*/


void GAPI RegisterXBuilder (REFCLSID clsid, XBuilderProc pProc)
{


};






























class GWinSockHandle : public GIoDeviceHandle
{
public :

	GResult		    AssignHandle    (SOCKET hSock, IIoCoPort * pIIoCoPort)
			    {
				return GIoDeviceHandle::AssignHandle 
						     ((void *) hSock, pIIoCoPort ,
						      GWinSockHandle::CloseHandle);
			    };

	SOCKET		    GetHandle	    () const { return (SOCKET) GIoDeviceHandle::GetHandle ();};

protected :

static	void GAPI	    CloseHandle	(GIoDeviceHandle &);

};
//[]------------------------------------------------------------------------[]
//

//[]------------------------------------------------------------------------[]
// GWinSock interface definition
//
//
// Created by GSh.
//[]------------------------------------------------------------------------[]

void GAPI GWinSockHandle::CloseHandle (GIoDeviceHandle & hDevice)
{
    SOCKET hSock = (SOCKET) hDevice.GetHandle ();

    GIoDeviceHandle::CloseHandle (hDevice);

    if (hSock)
    {
	::closesocket (hSock);
    }
};


#if FALSE
//[]------------------------------------------------------------------------[]
//
class GClassBuilder
{



};



class GIUnknown_Entry
{

private :

	LPCCLSID 	m_pCLSID    ;
	IUnknown *	m_pIUnknonw ;
	void (GAPI *	m_pCreateInstance)(GIrp *, REFIID);
};

    DECLARE_INTERFACE_ (IClassBuilder, IUnknown)
    {
//	typedef (void GAPI ClassBuilder)(GIrp *, REFCLSID

//    STDMETHOD_ (GResult,    RegisterBuiler)
    };

    DECLARE_INTERFACE_ (IEnumerator, IUnknown)
    {
	
    };

//void GAPI QueryInterface (GIrp *, REFIID iid,

//
//[]------------------------------------------------------------------------[]

#endif




















#pragma pack (_M_PACK_VPTR)

    struct GBindingHandle
    {
	typedef TSLink <GBindingHandle, 0>   Link;
	typedef TXList <GBindingHandle, 
		TSLink <GBindingHandle, 0> > List;


	GCloseLock		m_CloseLock ;

/*
	GIrp * (GAPI *	Xxxx_Do_CloseProc)(GIrp *)		Close

*/

//	GCloseLock	m_xCloseLock ;

/*
	GIrp_DpProc	m_pXxxx_DpCallBack  ;
	GIrp_DpProc	m_pXxxx_DpClose	    ;
*/

    };
/*
    struct GSignal
    {
	GBindingHandle::List	    m_BList ;


	void		Raise	    (GIrp_ExRecord *);
    };
*/
#pragma pack ()


/*
void PerformCallBack (GIrp * pIrp)
{
    TXList_Iterator
   <GBindingHandle>   I;

    for (I = m_BindingList.head (); I.next ();)
    {
	if (I.next () ->m_CloseLock.LockAcquire ())
	{

	    SetCurCoProc (pIrp, I.next () ->m_pXxxx_CoCallBack	      ,
			        I.next () ->m_pXxxx_CoCallBackContext ,
				I.next () ->m_pXxxx_CoCallBackArgument);

	    I.next () ->m_CloseLock.LockRelease ();
	}

	    I <<= m_hWaitList.getnext (I.next ());
    }
}
*/

/*



    TXList_Iterator 
   <WakeUpEntry>      I;

    int		      nWait ;
    HANDLE	      hWait [MAXIMUM_WAIT_OBJECTS];

    for (nWait = 0, I = m_hWaitList.head (); I.next ();)
    {
	 hWait [nWait ++] = I.next () ->m_hWaitFor;

	 I <<= m_hWaitList.getnext (I.next ());
    };
};
*/











class GSignal
{


public :


	void		Emit	    (GIrp *);


protected :

	GIrp *		Xxxx_Co_BList	    (GIrp *,		     void *);

	GIrp *		Xxxx_Dp_BList	    (GIrp *, GIrp_DpContext, void *);

protected :

	GIrp_DispatchQueue	m_DQueue    ;	// As lock for (*)

	GBindingHandle::List	m_BList	    ;	// (*)
	TXList_Iterator
       <GBindingHandle>		m_I	    ;	// (*)
};

GIrp * GSignal::Xxxx_Dp_BList (GIrp * pIrp, GIrp_DpContext, void *)
{
    m_I = m_BList.head ();

    SetCurCoProc <GSignal, void *>
    (pIrp, & GSignal::Xxxx_Co_BList, this, NULL);

    CompleteIrp (pIrp);

    return NULL;
};

GIrp * GSignal::Xxxx_Co_BList (GIrp * pIrp, void *)
{
    return pIrp;    
};

void GSignal::Emit (GIrp * pIrp)
{
    SetCurDpProc <GSignal, void *> 
    (pIrp, & GSignal::Xxxx_Dp_BList, this, NULL);

    m_DQueue.QueueForDispatch (pIrp);
};





/*

void SomeClass::Connect (XDispatch::CallBack * pXCallBack)
{
    XBindingHandle * pXCallBack ->GetBinginHandle ();

    QueryInterface (....)

    if (ERROR_SUCCESS, QueryInterface (& pXCallBack ->m_pInterface, 
};


void XDispatch::CallBack::Close ()
{
    if (m_xCloseLock.CloseAcquire ())
    {{
	GIrp * pIrp = CreateIrp ();

	SetCurDpProc (pIrp, m_pXxx_DpClose, this, NULL);

	m_xCloseLock.ClosePerform (pIrp);
    }}
};






*/

/*
static Bool GAPI Xxxx_Co_QueueForDelay (IWakeUp * pIWakeUp, IWakeUp::Reason reason, void * pContext, void * pIrp)
{
    if (IWakeUp::OnTimeout == reason)
    {
	pIWakeUp ->Destroy ();

	((GApartment *) pContext) ->QueueIrpForComplete ((GIrp *) pIrp);

	((GApartment *) pContext) ->Release ();
    }
	return False;
};

void GAPI QueueForDelay (GIrp * pIrp, DWord dTimeout)
{
    IWakeUp *	    pIWakeUp;

    if (ERROR_SUCCESS == TCurrent () ->GetCurApartment () ->
			 CreateWakeUp (pIWakeUp))
    {
			 TCurrent () ->GetCurApartment () ->AddRef ();
	pIWakeUp ->QueueWaitFor 
			(NULL, Xxxx_Co_QueueForDelay, 
			 TCurrent () ->GetCurApartment (), pIrp, dTimeout);
    }
    else
    {
	::Sleep (0);

	CompleteIrp (pIrp);
    }
};
*/


//STDAPI_(HRESULT)    GetClassObject (REFCLSID clsid, DWord d











/*
//[]------------------------------------------------------------------------[]
//
class GIrp_DispatchQueue : public GIrp_List
{
public :

	GIrp_DispatchQueue () : m_pCurIrp (NULL) {};

	GIrp_DispatchQueue (const GAccessLock & lock) : m_qLock (lock), m_pCurIrp (NULL) {};

// Dispatching group ------------------
//
    	void		QueueRequest	    (GIrp * pIrp);
//
// End dispatching gorup --------------

protected :

	void		LockAcquire	    () { m_qLock.LockAcquire ();};

	void		LockRelease	    () { m_qLock.LockRelease ();};

	void		DispatchRequest	    (GIrp *);

static	GIrp * GAPI	Xxxx_Co_Dispatch    (GIrp * pIrp, void *, void *);

static	GIrp * GAPI	Xxxx_Dispatch	    (GIrp * pIrp, void *, void *);

private :

        GAccessLock	    m_qLock	;   // (*) Lock for (*) member
        GIrp *		    m_pCurIrp	;   // (*)
};
//
//[]------------------------------------------------------------------------[]
*/

//[]------------------------------------------------------------------------[]
//
class GIrp_EventQueue
{
public :

	GIrp_EventQueue (DWord dCoState = 0) : m_dCoState (dCoState) {};

	GIrp_EventQueue (const GAccessLock & lock, DWord dCoState = 0) : 

			       m_qLock (lock), m_dCoState (dCoState) {};

// Dispatching group ------------------
//
    	DWord		QueueForWaitEvent  (GIrp * pIrp, DWord  dUpMask ,
							  DWord  dDnMask ,
						    const void * pKey	 ,
							  DWord  dTimeout = INFINITE);

	Bool		ExtractForEvent    (GIrp_List  &, DWord  dUpMask ,
							  DWord  dDnMask ,
						    const void * pKey	 );
//
// End dispatching gorup --------------

protected :

#pragma pack (_M_PACK_VPTR)

	struct QueueEntry
	{
	    typedef TSLink <QueueEntry, 0>   Link;
	    typedef TXList <QueueEntry,
		    TSLink <QueueEntry, 0> > List;

	    Link	    QLink    ;

	    GIrp *	    pIrp     ;
	    DWord	    dUpMask  ;
	    DWord	    dDnMask  ;
	    const void *    pKey     ;
	    GTimeout	    tTimeout ;
	};

#pragma pack ()

	void		LockAcquire	    () { m_qLock.LockAcquire ();};

	void		LockRelease	    () { m_qLock.LockRelease ();};

	void		DispatchRequest	    (GIrp *);

static	GIrp * GAPI	Xxxx_Co_Dispatch    (GIrp * pIrp, void *, void *);

static	GIrp * GAPI	Xxxx_Dispatch	    (GIrp * pIrp, void *, void *);

private :

        GAccessLock	    m_qLock	;   // (*) Lock for (*) member

	QueueEntry::List    m_qCoQueue	;   // (*)
	DWord		    m_dCoState	;   // (*)
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
DWord GIrp_EventQueue::QueueForWaitEvent (GIrp * pIrp, DWord  dUpMask  ,
						       DWord  dDnMask  ,
						 const void * pKey     ,
						       DWord  dTimeout )
{
    TXList_Iterator  <QueueEntry> I;

    QueueEntry * e = (QueueEntry *) pIrp ->alloc (sizeof (QueueEntry));
    DWord	 t;

    e ->QLink	     .init ();
    e ->pIrp	   = pIrp    ;
    e ->dUpMask	   = dUpMask ;
    e ->dDnMask	   = dDnMask ;
    e ->pKey	   = pKey    ;

    LockAcquire ();

	if (INFINITE != dTimeout)
	{
	    e ->tTimeout.Activate     (dTimeout, t = GetTickCount ());

	    for (I  = m_qCoQueue.head (); I.next ();)
	    {
		if (dTimeout < I.next () ->tTimeout.Relative (t))
		{
		    break;
		}
		I <<= m_qCoQueue.getnext (I.next ());
	    }
		      m_qCoQueue.insert  (I, e);
	}
	else
	{
	    e ->tTimeout.Disactivate ();

		      m_qCoQueue.append  (e);
	}

    LockRelease ();

    return dTimeout;
};

Bool GIrp_EventQueue::ExtractForEvent (GIrp_List & List, DWord  dUpMask ,
							 DWord  dDnMask ,
						   const void * pKey    )
{
    if (INVALID_PTR == pKey)
    {
	dUpMask = 
	dDnMask = 0xFFFFFFFF;
    }


    DWord   dChMask = dUpMask ^ dDnMask;

    return False;
};
//
//[]------------------------------------------------------------------------[]

#endif