//[]------------------------------------------------------------------------[]
// GWin file io subsystem definitions
//
//
// Created by GSh.
//[]------------------------------------------------------------------------[]

#ifndef __G_WINFILE
#define __G_WINFILE

#include "G_winbase.h"

//[]------------------------------------------------------------------------[]
//
    Bool		  GAPI	GrabOverlappedResult	
					    (HANDLE, GOverlapped &, Bool bWait);

    GOverlapped::IoStatus GAPI  PerformIo   (GOverlapped &  o		    ,
					     IIoCoWait *    pIIoCoWait	    ,
					     IIoCoPort *    pIIoCoPort	    ,
					     DWord	    dIoCoResult	    ,
					     void *	    pIoCoResultInfo );

    GOverlapped::IoStatus GAPI	WriteFile   (HANDLE	    hFile       ,
					     DWord	    dOffset     ,
					     DWord	    dOffsetHigh ,
				       const void *	    pBuf        ,
					     DWord	    dBufSize    ,
					     GOverlapped &  o	        ,
					     IIoCoWait *    pIIoCoWait	,
					     IIoCoPort *    pIIoCoPort  );

    GResult		  GAPI WriteFile    (HANDLE	    hFile	,
					     DWord	    dOffset     ,
					     DWord	    dOffsetHigh ,
				       const void *	    pBuf	,
					     DWord	    dBufSize    ,
					     DWord *	    pTransfered ,
					     HANDLE	    hCoEvent    );

    GOverlapped::IoStatus GAPI	ReadFile    (HANDLE	    hFile       ,
					     DWord	    dOffset     ,
					     DWord	    dOffsetHigh ,
					     void *	    pBuf	,
					     DWord	    dBufSize    ,
					     GOverlapped &  o		,
					     IIoCoWait *    pIIoCoWait	,
					     IIoCoPort *    pIIoCoPort  );

    GResult		  GAPI	ReadFile    (HANDLE	    hFile	,
					     DWord	    dOffset     ,
					     DWord	    dOffsetHigh ,
					     void  *	    pBuf	,
					     DWord	    dBufSize    ,
					     DWord *	    pTransfered ,
					     HANDLE	    hCoEvent    );

    GOverlapped::IoStatus GAPI	DeviceIoControl 
					    (HANDLE	    hFile	,
					     DWord	    dCtrlCode   ,
					     void *	    pInpBuf     ,
					     DWord	    dInpBufSize ,
					     void *	    pOutBuf     ,
					     DWord	    dOutBufSize ,
					     GOverlapped & o		,
					     IIoCoWait *    pIIoCoWait	,
					     IIoCoPort *    pIIoCoPort  );

    GOverlapped::IoStatus GAPI	LockFile    (HANDLE	    hFile	,
					     DWord	    dOffset	,
					     DWord	    dOffsetHigh	,
					     DWord	    dLength	,
					     DWord	    dLengthHigh	,
					     Bool	    bExclusive	,
					     GOverlapped &  o		,
					     IIoCoWait *    pIIoCoWait	,
					     IIoCoPort *    pIIoCoPort	);

    GOverlapped::IoStatus GAPI	ConnectNamedPipe 
					    (HANDLE	    hFile	,
					     GOverlapped &  o		,
					     IIoCoWait *    pIIoCoWait	,
					     IIoCoPort *    pIIoCoPort  );
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
class GSerialPort
{
friend class GSerialIoDevice;

protected :
		    GSerialPort () //: m_ISerial (IID_ISerial)

		    {
			m_dRecvSeqNo =
			m_dRecvAckNo = 0;
		    };

protected :
#ifdef _MSC_BUG_C2248
public :
#endif

#pragma pack (_M_PACK_VPTR)

	struct SendRequest : public GIrp_Request
	{
	    void *		pSendBuf	;
	    DWord		dSendBufLen	;
	    DWord		dSendBufSize	;

	    DWord		dSendSeqNo	;
	    GIrp_List		XferList	;
/*
	union
	{
	    ISerial::BusAddress	Address		;
	    Byte		AddressBuf [60] ;
	};
*/
	    SendRequest *   Init (void * pBuf, DWord dBufSize)
			    {
				GIrp_Request::Init (0x0000);

				this ->pSendBuf	    = pBuf     ;
				this ->dSendBufLen  = 0	       ;
				this ->dSendBufSize = dBufSize ;

				this ->dSendSeqNo   = 0;
				this ->XferList.init ();

//				this ->Address .init (sizeof (this ->AddressBuf));

				return this;
			    };
	};

	struct RecvRequest : public GIrp_Request
	{
	    void *		pRecvBuf	;
	    DWord		dRecvBufSize	;

	    DWord		dRecvSeqNo	;
	    ISerial::DataType	tDataType	;
/*
	    Byte *		pRecvBufHead	;
	    Byte **		pRecvBufTail






	union
	{
	    ISerial::BusAddress	Address		;
	    Byte		AddressBuf [60] ;
	};
*/
	    RecvRequest *   Init (void * pBuf, DWord dBufSize)
			    {
				GIrp_Request::Init (0x0000);

				this ->pRecvBuf	    = pBuf     ;
				this ->dRecvBufSize = dBufSize ;

				this ->dRecvSeqNo   = 0;
				this ->tDataType    = ISerial::eof;

//				this ->Address.init (sizeof (this ->AddressBuf));

				return this;
			    };
	};

	struct XferRequest : public GIrp_Request
	{
	    XDispatchCallBack *	pICallBack  ;
	    const void *	pRequestId  ;
	    const ISerial::BusAddress * 
				pAddress    ;
	    const void *	pSendBuf    ;
	    DWord		dSendBufLen ;

	    XferRequest *   Init (XDispatchCallBack *
						 pICallBack  ,
				  const void *	 pRequestId  ,
				  const ISerial::BusAddress *
						 pAddress    ,
				  const void *	 pSendBuf    ,
				  DWord		 dSendBufLen )
	    {
		GIrp_Request::Init (0x0000);

	    if((this ->pICallBack  = pICallBack) != NULL)
	    {	this ->pICallBack  ->AddRef ();}
		this ->pRequestId  = pRequestId	 ;
		this ->pAddress    = pAddress	 ;
		this ->pSendBuf	   = pSendBuf	 ;
		this ->dSendBufLen = dSendBufLen ;

		return this;
	    };

	    static void GAPI	Destroy (XferRequest * pThis)
				{
				    if (pThis ->pICallBack)
				    {
					pThis ->pICallBack ->Release ();
					pThis ->pICallBack = NULL;
				    }
				};
	};

#pragma pack ()

protected :

//...........
//
	GIrp *	    Co_Send		    (GIrp *, void *);

virtual GIrp *	    Io_SendProc		    (GIrp *)  = NULL;

virtual	GIrp *	    PrepareXfer		    ();

	void	    PerformXfer		    ();

virtual	void	    BuildSendRequest	    (GIrp *	pSendIrp     ,
					     void *	pSendBuf     ,
					     DWord	dSendBufSize );

	void	    QueueForSend	    (GIrp *	pSendIrp     ,
					     void *	pSendBuf     ,
					     DWord	dSendBufSize );
//...........
//
	GIrp *	    Co_Recv		    (GIrp *, void *);

	GIrp *	    XxxxCo_Recv		    (GIrp *, void *);

	GIrp *	    Co_RecvLoop		    (GIrp *, void *);

virtual	GIrp *	    Io_RecvProc		    (GIrp *) = NULL;

virtual	void	    BuildRecvRequest	    (GIrp *	pRecvIrp     ,
					     void *	pRecvBuf     ,
					     DWord	dRecvBufSize );

	void	    QueueForRecv	    (GIrp *	pRecvIrp     ,
					     void *	pRecvBuf     ,
					     DWord	dRecvBufSize );
//...........
//
protected :
#ifdef _MSC_BUG_C2248
public :
#endif

virtual	HRESULT	XUnknown_QueryInterface	    (REFIID, void **);

virtual void	XUnknown_FreeInterface	    (void *);

virtual	void	ISerial_Listen	    (GIrp *, XDispatchCallBack *,
					     const void *	,
					     const ISerial::BusAddress *);

virtual	void	ISerial_Connect	    (GIrp *, XDispatchCallBack *,
					     const void *	,
					     const ISerial::BusAddress *,
					     const void *	, DWord );

virtual	void	ISerial_Disconnect  (GIrp *, XDispatchCallBack *,
					     const void *	);

	void	ISerial_Xfer	    (GIrp *, XDispatchCallBack *,
					     const void *	,
					     const ISerial::BusAddress *,
					     const void *	, DWord );
protected :

	friend class 
	TXUnknown_Entry <GSerialPort>;
	TXUnknown_Entry <GSerialPort>
				m_XUnknown ;

	class ISerial_Entry : public TInterface_Entry <ISerial, GSerialPort>
	{
	public :
	STDMETHOD_ (void,	 Listen)(GIrp *	      pIrp	  ,
					 XDispatchCallBack *
						      pICallBack  ,
				   const void *	      pRequestId  ,
				   const BusAddress * pSrcAddress )
	{
	    GetT () ->ISerial_Listen (pIrp, pICallBack, pRequestId, pSrcAddress);
	};
	STDMETHOD_ (void,	Connect)(GIrp *	      pIrp	  ,
					 XDispatchCallBack *
						      pICallBack  ,
				   const void *	      pRequestId  ,
				   const BusAddress * pDstAddress ,
				   const void *	      pSendBuf    ,
					 DWord	      dSendBufLen )
	{
	    GetT () ->ISerial_Connect (pIrp, pICallBack, pRequestId, pDstAddress, pSendBuf, dSendBufLen);
	};
	STDMETHOD_ (void,    Disconnect)(GIrp *	      pIrp	  ,
					 XDispatchCallBack *
						      pICallBack  ,
				   const void *	      pRequestId  )
	{
	    GetT () ->ISerial_Disconnect (pIrp, pICallBack, pRequestId);
	};
	STDMETHOD_ (void,	   Xfer)(GIrp *	      pIrp	  ,
					 XDispatchCallBack *
						      pICallBack  ,
				   const void *	      pRequestId  ,
				   const BusAddress * pDstAddress ,
				   const void *	      pSendBuf    ,
			    		 DWord	      dSendBufLen )
	{
	    GetT () ->ISerial_Xfer (pIrp, pICallBack, pRequestId, pDstAddress, pSendBuf, dSendBufLen);
	};
	}			m_ISerial    ;

protected :

	GCloseLock		m_fCloseLock ;
//...........
//
	GIrp_List		m_XferQueue  ;

//...........
//
	GIrp_List		m_SendQueue  ;

//...........
//
	GIrp_List		m_RecvQueue  ;

	DWord			m_dRecvSeqNo ;
	DWord			m_dRecvAckNo ;
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
class GSerialIoDevice //: public GSerialPort
{
protected :
		    GSerialIoDevice ()
		    {
			m_pIIoCoPort	= NULL;
		    };

private :

// For (1300 > _MSC_VER) compatible !!! ???
//
	struct ParentSendRequest : public GSerialPort::SendRequest {};
	struct ParentRecvRequest : public GSerialPort::RecvRequest {};
//
// For (1300 > _MSC_VER) compatible !!! ???

protected :
#ifdef _MSC_BUG_C2248
public :
#endif

#pragma pack (_M_PACK_VPTR)

	struct SendRequest : public ParentSendRequest
	{
	    IWakeUp *		pIIoCoWakeUp	;
	    GOverlapped		o		;

	    SendRequest *   Init (void * pBuf, DWord dBufSize)
	    {
		ParentSendRequest::Init (pBuf, dBufSize);

//		pIIoCoWait = NULL;

//		if (! bIoCoPortUsed)
		{
//		    CreateEventWakeUp 
//		   (this ->pIIoCoWakeUp, True, False);
		}
		o.Init ();

		return this;
	    };

	static void GAPI    Destroy (SendRequest * pThis)
			    {
				if (pThis ->pIIoCoWakeUp)
				{
				    pThis ->pIIoCoWakeUp ->Release ();

				    pThis ->pIIoCoWakeUp = NULL;
				}
			    };
	};

	struct RecvRequest : public ParentRecvRequest
	{
	    IWakeUp *		pIIoCoWakeUp	;
	    GOverlapped		o		;

	    RecvRequest *   Init (void * pBuf, DWord dBufSize)
	    {
		ParentRecvRequest::Init (pBuf, dBufSize);

		pIIoCoWakeUp = NULL;

//		if (! bIoCoPortUsed)
		{
//		    CreateEventWakeUp
//		   (this ->pIIoCoWakeUp, True, False);
		}
		o.Init ();

		return this;
	    };

	static void GAPI    Destroy (RecvRequest * pThis)
			    {
				if (pThis ->pIIoCoWakeUp)
				{
				    pThis ->pIIoCoWakeUp ->Release ();

				    pThis ->pIIoCoWakeUp = NULL;
				}
			    };
	};








	struct IoCoHandler : public XHandle
	{
	    typedef TSLink <IoCoHandler, sizeof (XHandle)>   Link ;
	    typedef TXList <IoCoHandler, 
		    TSLink <IoCoHandler, sizeof (XHandle)> > List ;

	    GOverlapped		o	   ;
	    GApartment *	pCoContext ;
	};

#pragma pack ()

/*
	void

*/

virtual	GResult		CreateIoCoHandler   (XIoCoHandler *&);

protected :
//...........

	void		BuildSendRequest    (GIrp *, void *, DWord);
//...........

	void		BuildRecvRequest    (GIrp *, void *, DWord);
//...........

	GIrp *		Co_Close	    (GIrp *, void *);

virtual	GIrp *		Xxxx_Dp_Close	    (GIrp *, GIrp_DpContext, void *);

	void		QueueForClose	    (GIrp *);

	void		QueueForSend	    (GIrp *, XIoCoHandler * hIoCoHandler ,
						     const ISerial::BusAddress *
								    pDstAddr     ,
						     const void *   pSendBuf     ,
							   DWord    dSendBufSize );

	void		QueueForRecv	    (GIrp *, XIoCoHandler * hIoCoHandler ,
							   void *   pRecvBufBuf	 ,
							   DWord    dRecvBufSize );

/*
	DECLARE_INTERFACE_ (IIoCoHandler, XHandle)
	{
	STDMETHOD_ (void,      QueueForRecv)(void *, DWord);
	};

virtual	GResult		CreateIoCoHandler   (& Receiver);

	void		QueueForRecv	    (GIrp *, IIoCoHandler *, void * pBuf, DWord dBufSize);


	void		QueueForSend	    (






	DECLARE_INTERFACE_ (IIoCoHandler, XHandle)
	{
	STDMETHOD_ (void,	 QueueForIo)(GIrp *);
	};


	DECLARE_INTERFACE_ (IIoCoHandle, XHandle)
	{
	};


	DECLARE_INTERFACE_ (ISerialIoBus, XHandle)
	{
	STDMETHOD_  
	};

	DECLARE_INTERFACE_ (XSerialIoDevice, XHandle)
	{
	DECLARE_INTERFACE  (XferHandle)
	{
	STDMETHOD_ (void,      QueueForSend)

	};

	STDMETHOD_ (void,	 QueueClose)(GIrp *);

	STDMETHOD_ (void,	       Send)(const void * pBuf, DWord dBufSize


	STDMETHOD_ (XferHandle,  CreateSendBuf)(GIrp *, void * pBuf, DWord dBufSize);

	STDMETHOD_ (GResult,  CreateRecvBuf)(GIrp *, void * pBuf, DWord dBufSize);
	};

	STDMETHOD_ (GResult,	CreateSendRequest)(IoSendRequest, 





	DECLARE_INTERFACE_ (SendRequest, Request)
	{
	DECLARE_INTERFACE_ (GStreamBuf &,   

	    typedef enum
	    {
		out	= 0,
		inp	= 1,

	    }		Type;
	}

	STDMETHOD_ (void,

	STDMETHOD_ (void,	Xfer	   )(void *
	};

virtual void		QueueForSend	    (GIrp *);

virtual	void		QueueForRecv	    (GIrp *);










virtual	void		QueueSendRequest    (GIrp *	pIrp	     ,
					     void *     pSendBuf     ,
					     DWord	dSendBufSize );


virtual	void		QueueSendRequest    (GIrp *	pIrp	  ,



virtual	void		QueueXferRequest    (GIrp *	pIrp		, 
					     const ISerial::BusAddress *
							pRecvAddress	,







	typedef void	(GAPI *	XxxIoCo_RecvCallBack)(GIrp * pIrp,
						      void

	void		QueueForRecv	    (GIrp * pIrp);

	void		QueueForSend	    (GIrp * pIrp);
*/



/*
virtual	void		QueueForCreate	    (GIrp *) = NULL;

virtual	void		XxxxIoCo_Recv	    (GOverlapped &, 

virtual	GIrp *		Io_RecvProc	    (GIrp *) = NULL;
*/
protected :

	GCloseLock		m_fCloseLock	;

	IIoCoPort *		m_pIIoCoPort	;
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
class GSerialFile : public GSerialIoDevice
{
protected :
//...........

static	void GAPI	XxxxIoCo_Send	    (GOverlapped &, GSerialFile *, GIrp * pIrp);

	GIrp *		Io_SendProc	    (GIrp *);
//...........

static	void GAPI	XxxxIoCo_Recv	    (GOverlapped &, GSerialFile *, GIrp * pIrp);

	GIrp *		Io_RecvProc	    (GIrp *);
//...........

	GIrp *		Xxxx_Dp_Close	    (GIrp *, GIrp_DpContext, void *);

protected :

protected :

	HANDLE			m_hFile		;
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
class GWinComm : public GSerialFile
{
};

class GWinPipe : public GSerialFile
{
public :

static	void	    CreatePipeName  (LPTSTR pPipeName, LPCSTR pName);

protected :

};
//
//[]------------------------------------------------------------------------[]

#endif//__G_WINFILE
