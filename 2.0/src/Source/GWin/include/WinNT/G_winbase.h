//[]------------------------------------------------------------------------[]
// Main GWin declarations
//
//
// Created by GSh.
//[]------------------------------------------------------------------------[]

#ifndef __G_WINBASE_H
#define __G_WINBASE_H

#include "g_windefs.h"

//[]------------------------------------------------------------------------[]
// Global structure must be define for any Dll/Exe module
//
//
#pragma pack (_M_PACK_VPTR)

    struct GWinModule
    {
	TInterlocked	<int   >    cInitCount	;
	DWORD			    dTlsIndex	;

	TInterlockedPtr <void *>    pParent	;
	HMODULE			    hExeDll	;

	IThreadPool *		    pIThreadPool;

	GAccessLock		    StationLock	;
	GAccessLock		    ProcessLock	;
    };

    typedef void * GWinModuleArea   [roundup(sizeof (GWinModule), sizeof (void *)) / sizeof (void *)];

#pragma pack ()

	extern GWinModuleArea		g_GWinModuleArea ;

#define	g_GWinModule ((GWinModule &)	g_GWinModuleArea)

						// For Dll module hModule must be != NULL
						//
	Bool GAPI		    GWinInit	(HMODULE hModule = NULL);

	void GAPI		    GWinDone	();
//
//[]------------------------------------------------------------------------[]

class GThread	 ;
class GApartment ;

/*
class GWinProcess : public GWinModule
{
};




class GProcess
{
public :

static	GAccessLock &		    GetLock
}
*/
	GFORCEINLINE GAccessLock &  GetProcessLock () { return g_GWinModule.ProcessLock;};


//	GFORCEINLINE void	    LockAcquire () { _GWinModule.CLock.LockAcquire ();};
//	GFORCEINLINE void	    LockRelease () { _GWinModule.CLock.LockRelease ();};

/*GSh ---
	GFORCEINLINE ITPool *	    TPool	()
				    {
					return (* g_GWinModule.pITPool);
				    };

*/
	GFORCEINLINE IThreadPool *  TPool	()
				    {
					return   g_GWinModule.pIThreadPool;
				    };

	GFORCEINLINE GThread *	    TCurrent	()
				    {
					return (GThread *) 
					::TlsGetValue (g_GWinModule.dTlsIndex);
				    };

	GFORCEINLINE GThread *	    GetCurThread	()
				    {
					return (GThread *)
					::TlsGetValue (g_GWinModule.dTlsIndex);
				    };

	GFORCEINLINE GWinModule *   GetCurModule	()
				    {
					return & g_GWinModule;
				    };


	GFORCEINLINE GThread *	    GWinCurrentThread	()
				    {
					return (GThread *)
					::TlsGetValue (g_GWinModule.dTlsIndex);
				    };
/*
	GFORCEINLINE GWinModule *   GWinCurrentModule	()
				    {
					return (g_GWinModule)
				    };




	GFORCEINLINE GAccessLock &  DefAccessLock ()
				    {
					return g_GWinModule.FLock;
				    };
*/
// Some Window's Kernel wrappers -------------------------------------------[]
//
	DWORD	    GAPI    WaitForAny	    (int nHandle, ... /* pHandles [nHandle], dTimeout */);

	DWORD	    GAPI    WaitForAnyAlertable
					    (int nHandle, ... /* pHandles [nHandle], dTimeout */);

	DWORD	    GAPI    WaitForAll	    (int nHandle, ... /* pHandles [nHandle], dTimeout */);

	DWORD	    GAPI    WaitForAllAlertable
					    (int nHandle, ... /* pHandles [nHandle], dTimeout */);

	HANDLE	    GAPI    DuplicateHandle (HANDLE);
/*
	GThread *   GAPI     ThCur	    ()ThisThread	    () { return TCurrent  ();};

	DWORD	    GAPI     ThCur_CurTh_Id	    () { return CurTh () ->GetId     ();};

	HANDLE	    GAPI     CurTh_Handle   () { return CurTh () ->GetHandle ();};

	HANDLE	    GAPI     CurTh_Resume   () { return CurTh () ->GetResume ();};
*/

	HANDLE	    WINAPI  OpenResume	    ();

	void	    WINAPI  CloseResume	    (HANDLE);

//	GThread *   WINAPI  TCurrent	    ();
//
	DWORD	    WINAPI  TCurrentId	    ();
//
//	HANDLE	    WINAPI   TCurrentSync   ();

typedef	DWORD	   (GAPI *  GWaitForProc)   (int cHandles, const HANDLE * pHandles, DWord dTimeout);

typedef void	   (GAPI *  GUserAPCProc)   (int nArgs, void ** pArgs, HANDLE hResume);

	Bool	    GAPI    QueueUserAPC    (HANDLE hThread, GUserAPCProc , int nArgs, void ** pArgs, HANDLE hResume = NULL);

typedef	void	   (GAPI *  GAPCCoProc  )   (GIrp *);

	Bool	    GAPI    QueueAPCCoProc  (HANDLE hThread, GAPCCoProc , GIrp *);

	GResult	    GAPI    RunApartment    (GApartment *, int nArgs, void ** pArgs, HANDLE hResume);

	GResult	    GAPI    QueryIApartment (IApartment *&);
//
// Some Window's Kernel wrappers -------------------------------------------[]

//[]------------------------------------------------------------------------[]
// Windows synchronization objects
//
//
class GCriticalSection : public GAccessLock
{
public :
		GCriticalSection () : 

		GAccessLock (Lock, & m_Section)
		{
		    ::InitializeCriticalSection (& m_Section);
		}

		GCriticalSection (DWord dSpinCount) :

		GAccessLock (Lock, & m_Section) 
		{
#if (0x0403 <= WINVER)
		    ::InitializeCriticalSectionAndSpinCount 
						(& m_Section, dSpinCount);
#else
		    ::InitializeCriticalSection (& m_Section);
#endif
		};

	       ~GCriticalSection ()
		{
		    ::DeleteCriticalSection     (& m_Section);
		};
protected :

static	Bool	Lock		(void *, LockType);

private :

        CRITICAL_SECTION    m_Section;
};

class GSpinCountCriticalSection : public GCriticalSection
{
    public :
		GSpinCountCriticalSection (DWord dSpinCount = 0x10000) : 

		GCriticalSection (dSpinCount) {};
};

class GSyncEvent : public GAccessLock
{
public :
		GSyncEvent  (LPCTSTR pName = NULL, Bool bAccured = False) :

		GAccessLock (Lock, & m_hSyncEvent)
		{
		    m_hSyncEvent = ::CreateEvent (NULL, False, (bAccured) ? False : True, pName);
		};

	       ~GSyncEvent  ()
		{
		    if (	       m_hSyncEvent)
		    {
			::CloseHandle (m_hSyncEvent);

				       m_hSyncEvent = NULL;
		    }
		};
protected :

static	Bool	Lock		(void *, LockType);

private :

        HANDLE		m_hSyncEvent;
};

class GSemaphore : public GAccessLock
{
public :
		GSemaphore  (LPCTSTR pName = NULL, Bool bAccured = False) :

		GAccessLock (Lock, & m_hSemaphore)
		{
		    m_hSemaphore = ::CreateSemaphore (NULL, (bAccured) ? 0 : 1, 1, pName);
		};

	       ~GSemaphore ()
		{
		    if (	       m_hSemaphore)
		    {
			::CloseHandle (m_hSemaphore);

			    	       m_hSemaphore = NULL;
		    }
		};
protected :

static	Bool	Lock		(void *, LockType);

private :

	HANDLE		m_hSemaphore;
};

class GMutex : public GAccessLock
{
public :
		GMutex	    (LPCTSTR pName = NULL, Bool bAccured = False) :

		GAccessLock (Lock, & m_hMutex)
		{
		    m_hMutex = ::CreateMutex (NULL, bAccured, pName);
		};

	       ~GMutex ()
		{
		    if (	       m_hMutex)
		    {
			::CloseHandle (m_hMutex);

				       m_hMutex = NULL;
		    }
		};
protected :

static	Bool	Lock		(void *, LockType);

private :

	HANDLE		m_hMutex ;
};
//
//[]------------------------------------------------------------------------[]


//[]------------------------------------------------------------------------[]
// GThread base object
//
//
class GWakeUpPort;

#pragma pack (_M_PACK_VPTR)

	struct GException_Record
	{
	    DWord	    dExCode	;
	    int		    nExParams	;
	    void **	    pExParams	;
	};

	struct GException_Handler
	{
	    GException_Handler *
	    		    pParent	;
	    void *	    pContext	;
	    Bool (GAPI *    pHandler)(void *, GException_Record &);

	    GException_Handler (Bool (GAPI *)(void *, GException_Record &), void *);

	   ~GException_Handler ();
	};

#define	DEFINE_EXCEPTION_BLOCK(p,c)	    \
	{				    \
	    GException_Handler	h (p, c);

#define ENDDEF_EXCEPTION_BLOCK()	    \
	}

#pragma pack ()

class GThread : public GThreadBase
{
	friend class    GApartment	    ;
	friend struct	GException_Handler  ;

	friend GResult	GAPI RunApartment   (GApartment *, int nArgs, void **, HANDLE);

public :

	Type		GetType		    () const { return m_tType   ;};

	DWord		GetId		    () const { return m_tId     ;};

	HANDLE		GetHandle	    () const { return m_tHandle ;};

	HANDLE		GetResume	    () const { return m_tResume ;};

	int		GetBasePriority	    () const { return ::GetThreadPriority (GetHandle ());};

	void		SetBasePriority	    (int nPriority) const {::SetThreadPriority (GetHandle (), nPriority);};

	GApartment *	GetCurApartment	    () const { return m_pCurApartment ;};

//	GApartment *	SetCurApartment	    (GApartment * pNewAp   ) { GApartment * pAp = m_pCurApartment	   ;
//											  m_pCurApartment = pNewAp ;
//								       return       pAp;};

friend	Bool  GAPI	RaiseException	    (DWord dExCode, int nExParams, void ** pExParams);

#pragma pack (_M_PACK_VPTR)

	struct Entry : public  TSLink <Entry, 0>
	{
	    GThread *	    pThread	;
	};

	typedef TXList <Entry, TSLink <Entry, 0> > List;

#pragma pack ()

	GWakeUpPort *	GetWakeUpPort   () const { return m_pWakeUpPort;};

protected :
			 GThread	(Type tType = User);

			~GThread	();

static	GResult		_create		(HANDLE *      pHandle	      ,
					 LPTHREAD_START_ROUTINE	      ,
					 GWinMainProc  pMainProc      ,
					 int	       nMainProcArgs  ,
					 void **       pMainProcArgs  ,
					 SIZE_T	       tSackSize      ,
					 LPSECURITY_ATTRIBUTES	     
						       pSecAttributes );
	GResult		_threadProc	(void *);

/*
virtual	Bool		PumpInputWork	();

virtual	void		WaitInputWork	(DWord dTimeout);

	void		WaitInputWork	(DWord (GAPI *)(int, const HANDLE *, DWord), DWord dTimeout);
*/
private :

	GResult		RunApartment	(GApartment *, int, void **, HANDLE);

private :

	void *			m_pOldTls	;
	HANDLE			m_tHandle	;
	HANDLE			m_tResume	;

	DWORD			m_tId		;
	Type			m_tType		;

	GApartment  *		m_pCurApartment ;

protected :

	GWakeUpPort *		m_pWakeUpPort	;

	GException_Handler *	m_pTopExHandler ;
};
//
//[]------------------------------------------------------------------------[]
//
template <class T> class TThread : public T
{
public :

static	GResult		_create		(HANDLE	*      pHandle		,
					 GWinMainProc  pMainProc	,
					 int	       nMainProcArgs	,
					 void **       pMainProcArgs	,
					 SIZE_T	       tStackSize	,
					 LPSECURITY_ATTRIBUTES
						       pSecAttributes	,
					 const T *   = NULL		)
			{
			    return GThread::_create   (pHandle		,
					(LPTHREAD_START_ROUTINE)
					 TThread <T>::_threadProc	,
						       pMainProc	,
						       nMainProcArgs	,
						       pMainProcArgs	,
						       tStackSize	,
						       pSecAttributes	);
			};
protected :

static	DWORD WINAPI	_threadProc	(T * pParams /* void * pParams */)
			{
			    DECLSPEC_ALIGN (_M_CACH_SIZE)

			    TThread <T>    th;

			    return
			    th.GThread::_threadProc 
					   (pParams);
			};
};
//
//[]------------------------------------------------------------------------[]
//
template <class T>
GResult PrimaryThread (GWinMainProc pMainProc     ,
		       int	    nMainProcArgs ,
		       void **	    pMainProcArgs ,
		       const T *    pThreadType    = NULL)  // Not used !!!
{
    TThread <T> th;

    return (pMainProc)(nMainProcArgs, pMainProcArgs, NULL);
};

template <class T> 
GResult CreateThread  (HANDLE *	    pHandle	  ,
		       GWinMainProc pMainProc     ,
		       int	    nMainProcArgs ,
		       void **	    pMainProcArgs ,
		       size_t	    tStackSize     = 0   ,
		       LPSECURITY_ATTRIBUTES
				    pSecAttributes = NULL,
		       const T *    pThreadType    = NULL)  // Not used !!!
{
    return TThread <T>::_create	   (pHandle	  ,
				    pMainProc     ,
				    nMainProcArgs ,
				    pMainProcArgs ,
				    tStackSize    ,
				    pSecAttributes,
				    pThreadType   );
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
class GFifoThreadQueue : public GAccessLock
{
public :
		GFifoThreadQueue (const GAccessLock & src) : m_tLock (src)
		{
		    m_tCurr = NULL;
		    m_cCurr = NULL;

		    Init (Lock, this);
		};

	       ~GFifoThreadQueue () {};
protected :

	void	    Lock	(GThread::Entry *);
static	Bool	    Lock	(void *, LockType);

private :

	GAccessLock		m_tLock  ;  // Lock for (*)

	GThread::List		m_tQueue ;  // (*)
	GThread::Entry *	m_tCurr  ;  // (*)
	int			m_cCurr  ;  // (*)
};
//
//[]------------------------------------------------------------------------[]
//
//....
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
class GWakeUpPort
{
public :

	typedef DWord (GAPI *	WaitProc)(int, const HANDLE *, DWord);

protected :
#ifdef _MSC_BUG_C2248
public :
#endif

public :

#pragma pack (_M_PACK_VPTR)

	class Entry	  : public IWakeUp
	{};

	class WakeUpEntry : public Entry
	{
	    typedef TSLink <WakeUpEntry, sizeof (Entry)>   Link;
	    typedef TXList <WakeUpEntry,
		    TSLink <WakeUpEntry, sizeof (Entry)> > List;

	    void *  operator new    (size_t, void * ptr) { return ptr;};
	    void    operator delete (void *, void *) {};
	    void    operator delete (void *	   ) {};
	    
	    Link		m_QLink	      ;
	    TInterlocked <ULONG>
				m_cRefCount   ;
	    GWakeUpPort *	m_pOwner      ;

	    List *		m_qList	      ;

	    HANDLE		m_hWaitFor    ;
	    GTimeout		m_tWaitFor    ;
	    DWord		m_dPeriod     ;

	    Reason		m_eOnReason   ;
	    CallBack		m_pOnProc     ;
	    void *		m_pOnContext  ;
	    void *		m_pOnArgument ;


	STDMETHOD  (	     QueryInterface)(REFIID, void **);

	STDMETHOD_ (ULONG,	     AddRef)();

	STDMETHOD_ (ULONG,	    Release)();

	STDMETHOD_ (void,      QueueWaitFor)(	       HANDLE	     ,
					     CallBack, void *, void *,
						       DWord , DWord );
	STDMETHOD_ (void,     CancelWaitFor)(Bool);

	friend class GWakeUpPort;
	};

	friend class WakeUpEntry;

	struct DelayRequest : public GIrp_Request
	{
	    GTimeout		m_tWaitFor   ;

	    DelayRequest *	Init (DWord dTimeout,
				      DWord dCurTick)
				{
				    GIrp_Request::Init (0);

				    this ->m_tWaitFor.Activate (dTimeout, dCurTick);

				    return this;
				};
	};

#pragma pack ()

public :
			GWakeUpPort	(int	       nWakeUpArray,
					 WakeUpEntry * eWakeUpArray);

	GResult		CreateWakeUp	(IWakeUp *&);

	void		QueueIrpForComplete 
					(GIrp *, DWord dTimeout, Bool bCancelable = True);

// Work wakeup function ------------------------------------------------------
//
	Bool		EnterWaitLoop	() { return (NULL == m_tOwner) ? (m_tOwner = TCurrent (), True) : False;};

	void		LeaveWaitLoop	() { m_tOwner = NULL;};

	void		Break		() { ::QueueUserAPC (m_tOwner ->GetHandle (), NULL, 0, NULL, NULL);};

	void		WaitWakeUp	(WaitProc pWait, DWord dTimeout);

	Bool		PeekWakeUp	() const { return (m_oList.last () || m_oCoList.last ()) ? True : False;};

	Bool		PumpWakeUp	();

	GThread *	GetOwner	() { return m_tOwner;};

	ULONG		GetRefCount	() const { return m_cRefCount.GetValue ();};
//
// Work wakeup function ------------------------------------------------------

protected :

    	Bool		LockAcquire	() { return m_PLock.LockAcquire ();};

	void		LockRelease	() {        m_PLock.LockRelease ();};

	ULONG		AddRef		() { return m_cRefCount ++;};

	ULONG		Release		() { return m_cRefCount --;};

static	void   GAPI	Cancel_Co_Delay	(GIrp *, GAccessLock *, void *);

private :

#pragma pack (_M_PACK_VPTR)

	struct ExCallEntry : public GThread::Entry
	{
	    WakeUpEntry *    pEntry	;
	    void (GAPI  *    pExProc	)(ExCallEntry *);
	    void *	     pExArgs [8];
	};

#pragma pack ()

	void		free		(WakeUpEntry *);

	void  GAPI	queue		(WakeUpEntry *		,
					 HANDLE	    hWaitFor	,
					 IWakeUp::CallBack   
						    pOnProc     ,
					 void *	    pOnContext  ,
					 void *	    pOnArgument ,
					 DWord	    dDueTime    ,
					 DWord	    dPeriod     );

static	void  GAPI   ex_queue		(ExCallEntry *);

	void  GAPI	cancel		(WakeUpEntry *);

static	void  GAPI   ex_cancel		(ExCallEntry *);

	void  GAPI   ex_call		(ExCallEntry *);

static	DWord GAPI	wait		(WaitProc, int, const HANDLE *, DWord);

protected :

	TInterlocked <ULONG>	m_cRefCount ;

	GThread *		m_tOwner    ; // Owner thread for (t)

	WakeUpEntry *		m_oCurrent  ; // (t)
	WakeUpEntry::List	m_oList	    ; // (t)
	WakeUpEntry::List	m_hWaitList ; // (t)
	WakeUpEntry::List	m_tWaitList ; // (t)

	GAccessLock		m_PLock	    ; // (*) Lock for
	WakeUpEntry::List	m_freeList  ; // (*)

	GThread::List		m_syncList  ; // (*)
	TInterlocked <Bool>	m_syncSent  ; // (*)

	GIrp_List		m_tCoList   ; // (*)
	GIrp_List		m_oCoList   ; // (t)
};

template <int W = MAXIMUM_WAIT_OBJECTS> class TPrivateWakeUpPort : public GWakeUpPort
{
public :
	    TPrivateWakeUpPort () :

	    GWakeUpPort (W, (WakeUpEntry *) m_eWakeUpArray) {};
private :
	    void *		m_eWakeUpArray 
	   [roundup(sizeof (GWakeUpPort::WakeUpEntry) * W, sizeof (void *)) /
							   sizeof (void *)];
};

template <int W = MAXIMUM_WAIT_OBJECTS> class TSharedWakeUpPort : public GWakeUpPort
{
public :
	    TSharedWakeUpPort  (const GAccessLock & src) : 

	    GWakeUpPort (W, (WakeUpEntry *) m_eWakeUpArray) {m_PLock = src;};
private :
	    void *		m_eWakeUpArray 
	   [roundup(sizeof (GWakeUpPort::WakeUpEntry) * W, sizeof (void *)) /
							   sizeof (void *)];
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
template <class T = GThread, int W = MAXIMUM_WAIT_OBJECTS> class TPrivateWakeUpThread : public T
{
public :
	    TPrivateWakeUpThread () :

		m_WakeUpPort () 
	    {
		m_pWakeUpPort	= & m_WakeUpPort;
	    };

protected :
	    TPrivateWakeUpPort <W>  m_WakeUpPort;
};

template <class T = GThread, int W = MAXIMUM_WAIT_OBJECTS> class TSharedWakeUpThread : public T
{
public :    
	    TSharedWakeUpThread () : m_WLock (),

		m_WakeUpPort (m_WLock) 
	    {
		m_pWakeUpPort	= & m_WakeUpPort;
	    };

protected :
	    GCriticalSection	    m_WLock	;
	    TSharedWakeUpPort <W>   m_WakeUpPort;
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
class GApartment // : public TXUnknown_Entry <GApartment>
{
	friend class	GThread;

public :

	GThread *	GetCoThread () const { return m_pCoThread;};

	LONG		GetRefCount () const { return m_XUnknown.GetRefCount ();};

	LONG		AddRef	    () { return m_XUnknown.AddRef  ();};

	LONG		Release	    () { return m_XUnknown.Release ();};
/*
	void		QueueIrpForComplete (GIrp * pIrp, DWord dTimeout)
			{
			    IApartment_QueueIrpForComplete (pIrp, dTimeout);
			};

	IApartment *	QueryIApartment	    ()
			{
			    return m_IApartment.AddRef (); & m_IApartment) ->AddRef
			};
*/
	IApartment *	QueryIApartment	    ()
			{
			    void * pI;

			    return (XUnknown_QueryInterface (IID_IApartment, & pI), (IApartment *) pI);
			};

virtual	GResult		WorkLoop	    (LONG volatile * pExitFlag);

virtual	void		WaitInputWork	    (DWord dTimeout = INFINITE);

virtual	Bool		PumpInputWork	    ();

	void		BreakFromWorkLoop   (DWord dExitCode = 0)
			{
			    if (m_pExitCode)
			    {
			      * m_pExitCode = dExitCode;
			    }
			    if (m_pExitFlag)
			    {
			      * m_pExitFlag = False;
			    }
			};

virtual	void		Work		    ();

protected :
public :

//	typedef Bool   (GAPI * ExitConditionRunCtrlProc)(void * pContext, GResult & dResult);

			GApartment ()
			{
			    m_pCoThread	    = NULL  ;
			    m_bInsideWait   = False ;

			    m_bCoCur	= False ;
			    m_pCoCur	= NULL  ;

			    m_bCoBusy	= False ;
			    m_pExitFlag	= NULL	;
			    m_pExitCode = NULL  ;

//			    m_XUnknown  .Init (this);
//			    m_IApartment.Init (this, IID_IApartment, & m_XUnknown);
//
//			    m_XUnknown  .Release ();
			};

virtual		       ~GApartment ();

public :

virtual	HRESULT		XUnknown_QueryInterface		(REFIID, void **);

virtual	void		XUnknown_FreeInterface		(void *);

protected :
#ifdef _MSC_BUG_C2248
public :
#endif
public :

virtual	void		IApartment_QueueIrpForComplete	(GIrp *, DWord);

protected :

	Bool		LockAcquire	    (Bool bExclusive = True) 
					    { 
						return m_qCoLock.LockAcquire (bExclusive);
					    };
	void		LockRelease	    () {       m_qCoLock.LockRelease ();};

	GIrp *		GetIrpForComplete   ();

static	GIrp * GAPI	Co_RecvCoProcIrp    (GIrp *, void * pa, void *);

static	GIrp * GAPI	Co_RecvCoProcIrp2   (GIrp *, void * pa, void *);

	void		RecvCoProcIrp	    (GIrp * pIrp)
					    {
						m_bCoCur = 
					       (m_pCoCur = pIrp, True);
					    };

virtual	void		SendCoProcIrp	    (GIrp *);

virtual	GResult		InitInstance	    (int nArgs, void ** pArgs, HANDLE hResume);

virtual	GResult		MainProc	    (int nArgs, void ** pArgs, HANDLE hResume);

virtual	void		Done		    ();

protected :

	friend
	class TXUnknown_Entry <GApartment>  ;
	      TXUnknown_Entry <GApartment>
			    m_XUnknown	    ;
	friend
	class TInterface_Entry <IApartment, GApartment>	;
	class IApartment_Entry : public TInterface_Entry <IApartment, GApartment>
	{
	public :
	STDMETHOD_ (void,   QueueIrpForComplete)(GIrp * pIrp, DWord dTimeout)
	{
	    GetT () ->IApartment_QueueIrpForComplete (pIrp, dTimeout);
	}
	}		    m_IApartment    ;

protected :

	GSpinCountCriticalSection
			    m_qCoLock	    ;  // Lock for (*)

	GThread *	    m_pCoThread	    ;

	Bool		    m_bInsideWait   ;

	Bool		    m_bCoCur	    ;
	GIrp *		    m_pCoCur	    ;

	Bool		    m_bCoBusy	    ; // (*)
	GIrp_List	    m_qCoQueue	    ; // (*)

	LONG volatile *	    m_pExitFlag	    ;
	DWord *		    m_pExitCode	    ;
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
/*
class GWaitIoApartment : public GApartment
{
protected :

	    GWaitIoApartment (int nWakeUpArray, GWakeUpPort::WakeUpEntry * eWakeUpArray) :

	    m_WakeUpPort     (nWakeUpArray, eWakeUpArray)
	    {};

	Bool		PumpInputWork	    ();

	void		WaitInputWork	    (DWord dTimeout);

protected :

//	GWakeUpPort	    m_WakeUpPort ;
};
*/
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
class GWinMsgApartment : public GApartment
{
public :

	friend class GWinMsgLoop;

protected :

			GWinMsgApartment ()
			{
			    m_hCoWnd        = NULL ;
			    m_pCoWndProcDef = NULL ;

			    m_bInsideDisp   = False;
			};

		       ~GWinMsgApartment ();

	GResult		WorkLoop	    (LONG volatile *);

	Bool		PumpInputWork	    ();

static	DWord GAPI	WaitWakeUpProc	    (int, const HANDLE *, DWord);

	void		WaitInputWork	    (DWord dTimeout);

	void		SendCoProcIrp	    (GIrp *);

	HWND		GetCoHWND	    ();

static	LRESULT	WINAPI	CoWndProc	    (HWND, UINT, WPARAM, LPARAM);

virtual	Bool		PreprocMessage	    (MSG *);

protected :

	HWND		    m_hCoWnd	    ;
	WNDPROC		    m_pCoWndProcDef ;

	Bool		    m_bInsideDisp   ;
};
//
//[]------------------------------------------------------------------------[]



#if FALSE

virtual	GResult		DoDispatch	    (DoDispatchCtrlProc, void *);


//[]------------------------------------------------------------------------[]
//
class GWaitIoApartment : public GApartment
{
public :

	GWakeUpPort *	GetWakeUpPort	    ();

	GResult		CreateWakeUp	    (IWakeUp   *&);

	GResult		CreateIoCoWait	    (IIoCoWait *&);

protected :

		GWaitIoApartment (int nWakeUpArray, GWakeUpPort::WakeUpEntry *
							    eWakeUpArray) :
		    m_WakeUpPort (nWakeUpArray, eWakeUpArray)
		{};

	GResult		Run		    ();

protected :

	GWakeUpPort		    m_WakeUpPort ;
};
//
//[]------------------------------------------------------------------------[]
//

/*
template <class T> class TPrivateThread : public GThread
{
protected :

	T		    m_Apartment	    ;
};

class GWaitIoThread : public TPrivateThread <GWaitIoApartment>
{
};

class GWinMsgThread : public TPrivateThread <GWinMsgApartment>
{
};

static GResult GAPI WorkIo_SomeProc (int, void **, HANDLE hResult)
{
    new GSharedApartment ()
};

class CRRPSerivice : public TShareGSharedApartment
{
public :

static GResult GAPI	MainProc	    (int, void **, HANDLE);

};









void SomeProc ()
{
    RegisterInterfaceClass  (CLDID_CRRPService, CRRPSerivce::Creator, 0, NULL);


    if (ERROR_SUCCESS == QueueWorkItem (IThreadPool::WorkIo, 0, NULL, NULL))
    {
    }

    if (ERROR_SUCCESS == 

    new GSharedApartment (
}






*/

//
//[]------------------------------------------------------------------------[]
//
class GSharedApartment : public GApartment
{
private :
	    GSharedApartment () : GApartment ()
	    {
		m_pITPool   = NULL;
	    };

//	GResult		CreateWakeUp	    (IWakeUp *&, HANDLE hForHandle);

protected :

	GIrp *	     Co_CompletePacket	    (GIrp *, void *);

	void		CompletePacket	    ();

protected :

	IThreadPool *	    m_pITPool	    ;
};
//
//[]------------------------------------------------------------------------[]
//
template <class T, int W = MAXIMUM_WAIT_OBJECTS> class TWakeUpApartment : public T
{
public :
	    TWakeUpApartment () : T (W, (GWakeUpPort::WakeUpEntry *) m_eWakeUpArray) {};
private :

	void *		    m_eWakeUpArray 

	   [roundup(sizeof (GWakeUpPort::WakeUpEntry) * W, sizeof (void *)) /
							   sizeof (void *)];
};

template <int W = MAXIMUM_WAIT_OBJECTS> class TWaitIoApartment : public 
					      TWakeUpApartment <GWaitIoApartment, W>
{};

asdf
template <int W = MAXIMUM_WAIT_OBJECTS> class TWinMsgApartment : public
					      TWakeUpApartment <GWinMsgApartment, W>
{};
//
//[]------------------------------------------------------------------------[]
//
/*
    template <typename T>
    struct TIrp_ReplyHandler : public GIrp_ReplyHandler
    {
	T *		tRplProcContext	;
	void	  (T::* tRplProc)(GIrp *, const void *);

	   TIrp_ReplyHandler (GIrp_ReplyHandler &
					 tTemplate  ,
			      GIrp *	 pIrp	    ,
			void (  T::*	 tRplProc   )(GIrp *, const void *),
			        T  *	 tRplContext,
			const void *	 pRequestId ) : 

	   GIrp_ReplyHandler (tTemplate, pIrp, (GIrp_ReplyProc) RplProc, tRplContext, pRequestId)
	   {
		this ->tRplProcContext = tRplContext ;
		this ->tRplProc	       = tRplProc    ;
	   };

	static void   GAPI  RplProc (GIrp * pIrp, TIrp_ReplyHandler * e, const void * pRequestId)
			    {
				((e ->tRplProcContext) ->* (e ->tRplProc))(pIrp, pRequestId);
			    };
    };

    template <typename T>
    GFORCEINLINE GIrp_ReplyHandler * CreateXReplyHandler 
				    (GIrp * pIrp, GIrp_ReplyHandler &
							       tTemplate   ,
						  void (T::*   tRplProc    )(GIrp *, const void *),
							T  *   tRplContext ,
						  const void * pRequestId  )
    {
	return new (pIrp, (TIrp_ReplyHandler <T> **) NULL) 
			   TIrp_ReplyHandler <T> (tTemplate, pIrp, tRplProc, tRplContext, pRequestId);
    };
*/
//
//[]------------------------------------------------------------------------[]

#endif










//[]------------------------------------------------------------------------[]
// GWin overlapped extension
//
//
#ifndef NT_STATUS_SUCCESS
#define	NT_STATUS_SUCCESS    ((DWORD)0x00000000L)
#endif

#pragma pack (_M_PACK_VPTR)

    GFORCEINLINE
    Bool	    IsIoSysQueued (DWord dResult, Bool bCanBeSysQueued)
		    {
			return (bCanBeSysQueued && ((ERROR_IO_PENDING == dResult) 
						||  (ERROR_SUCCESS    == dResult)))
			      ? True : False;
		    };

    GFORCEINLINE
    Bool	    IsIoPending	  (DWord dResult)
		    {
			return (ERROR_IO_PENDING == dResult) ? True : False;
		    };

    class GOverlapped : public OVERLAPPED
    {
    public :

	typedef	TSLink <GOverlapped, sizeof (OVERLAPPED)>   SLink;
	typedef TXList <GOverlapped,
		TSLink <GOverlapped, sizeof (OVERLAPPED)> > SList;

	typedef void	(GAPI *    IoCoProc)(GOverlapped &, void * pContext  ,
							    void * pArgument );
	typedef enum
	{
	    IoCompleted = 0,
	    IoPending	= 1,
	    IoSysQueued = 2,

	}		IoStatus;

			GOverlapped	()	 { Init ();}

	void		Init ()
			{
    			    Internal		  =
    			    InternalHigh	  =
    			    Offset		  =
    			    OffsetHigh		  = 0     ;
			    hEvent		  = NULL  ;

			    pIoCoResultInfo	  = NULL  ;
			    dIoCoResult		  = 
			    dIoCoResultEx	  = 0	  ;

			    pIIoCoWait		  = NULL  ;
			    pIIoCoPort		  = NULL  ;

			    pIoCoProc		  = NULL  ;
			    pIoCoContext	  =
			    pIoCoArgument	  = NULL  ;
			};
//
//[]------------------------------------------------------------------------[]
// Io starting part
//
	GOverlapped &	SetIoCoProc	(IoCoProc     pIoCoProc	    ,
					 void *	      pIoCoContext  ,
					 void *	      pIoCoArgument )
			{
			    this ->pIoCoProc	 =    pIoCoProc	    ;
			    this ->pIoCoContext  =    pIoCoContext  ;
			    this ->pIoCoArgument =    pIoCoArgument ;

			    return * this;
			};

	Bool		StartIo		(DWord	      dOffset	    ,
					 DWord	      dOffsetHigh   ,
					 HANDLE	      hIoCoEvent    )
			{
    			    Internal	    =
    			    InternalHigh    = 0			;
    			    Offset	    = dOffset		;
    			    OffsetHigh	    = dOffsetHigh	;

			    pIoCoResultInfo = NULL		;
    			    dIoCoResult	    = ERROR_IO_PENDING	;
			    dIoCoResultEx   = 0			;

			    hEvent	    = hIoCoEvent	;
	if (hEvent)
	{
			    hEvent = (HANDLE)((ULONG_PTR) hEvent + 1);
	}
			    return False;
			};
/*
	Bool		StartIo		(DWord	      dOffset	    ,
					 DWord	      dOffsetHigh   ,
					 IIoCoWait *  pIIoCoWait    ,
					 IIoCoPort *  pIIoCoPort    )
			{
    			    Internal	    =
    			    InternalHigh    = 0			;
    			    Offset	    = dOffset		;
    			    OffsetHigh	    = dOffsetHigh	;

			    pIoCoResultInfo = NULL		;
    			    dIoCoResult	    = ERROR_IO_PENDING	;
			    dIoCoResultEx   = 0			;

			    if (pIIoCoWait)
			    {
				hEvent = pIIoCoWait ->GetIoCoHandle ();

			       (this ->pIIoCoWait = pIIoCoWait) ->AddRef ();

			    if (pIIoCoPort)
			    {
				hEvent = (HANDLE)((ULONG_PTR) hEvent + 1);
			    }
			    }
			    else
			    if (pIIoCoPort)
			    {
			       (this ->pIIoCoPort = pIIoCoPort) ->AddRef ();

				return True ;
			    }
				return False;
			};
*/
//
//[]------------------------------------------------------------------------[]
// Io perform part
//
/*
static	DWord		PerformIo	(...);
*/
//
//[]------------------------------------------------------------------------[]
// IoStatus processing part
//
	IoStatus	GetIoCoStatus	(DWord	  dResult      ,
					 void *	  pResultInfo  ,
					 DWord	  dResultEx = 0)
			{
			//  Check this conditon outside befor this call
			//
			//  if (IsIoSysQueued (dResult, bSQ)
			//  {
			//	return IoSysQueued ;
			//  }
			//  else
			//  {	...
			//
			    if (IsIoPending (dResult))
			    {
				return IoPending   ;
			    }
				SetIoCoResult (dResult, pResultInfo, dResultEx);

				SetIoCoSignal	 ();

				return IoCompleted ;
			};
//
//[]------------------------------------------------------------------------[]
// IoStatus processing part
//
	Bool		QueueIoCoStatus (Bool	   bForceQueue)
			{
			//  Check this conditon outside befor this call
			//
			//  if (IoSysQueued == uStatus)
			//  {}
			//  else
			//  {	...
			//
			    if (pIIoCoPort && bForceQueue)
			    {
				pIIoCoPort ->QueueIoCoStatus (* this);

				return True  ;
			    }
				return False ;
			//
			//  }
			};
/*
	Bool		QueueIoCoWait	(IoStatus  uStatus    ,
					 Bool	   bForceWait )
			{
			    if (bForceWait || (IoPending == uStatus))
			    {
				return True  ;
			    }
				return False ;
			};
*/
/*
			    if (GOverlapped::IoSysQueued ==  oStatus)
			    {}
			    else
			    if (o.QueueIoCoStatus (False))
			    {}
			    else
			    if (o.QueueIoCoWait	  (False))
			    {
				if (pIWakeUp)
    				{
				    pIWakeUp ->QueueWaitFor (o.Event,
				}
			    }
			    else
			    {
				o.CallIoCoProc	  ();
			    }




*/
/*
	Bool		QueueIoCoStatus	(
					 IoStatus  uStatus     ,
					 Bool	   bForceQueue )
			{
			//  Check this conditon outside befor this call
			//
			//  if (IoSysQueued == uStatus)
			//  {}
			//  else
			//  {	...
			//
			    if (bForceQueue || (IoPending  == uStatus))
			    {
				if	(pIIoCoWait)
				{
				    pIIoCoWait ->QueueIoCoWait   (* this);
				}
				else if (pIIoCoPort)
				{
				    pIIoCoPort ->QueueIoCoStatus (* this);
				}
				return True ;
			    }
				return False;
			//
			//  }
			};
*/




	Bool		QueueIoCoStatus	(IoStatus  uStatus     ,
					 Bool	   bForceQueue )
			{
			//  Check for this conditon outside befor this call
			//
			//  if		       (IoSysQueued > uStatus)
			//  {
			//	return True;
			//  }
			    if (bForceQueue || (IoPending  == uStatus))
			    {
				if	(pIIoCoWait)
				{
				    pIIoCoWait ->QueueIoCoWait   (* this);
				}
				else if (pIIoCoPort)
				{
				    pIIoCoPort ->QueueIoCoStatus (* this);
				}
				return True ;
			    }
				return False;
			};
//
//[]------------------------------------------------------------------------[]
// Io completion part
//
//
	Bool		HasIoCoResult	() const
			{
			    return ((ERROR_IO_PENDING == dIoCoResult) || ((ULONG_PTR) hEvent & 1)) ? False : True;
			};

	void		SetIoCoResult	(DWord	  dResult      ,
					 void *	  pResultInfo  ,
					 DWord	  dResultEx = 0)
			{
				dIoCoResultEx	= dResultEx    ;
				pIoCoResultInfo	= pResultInfo  ;
				dIoCoResult	= dResult      ;
			};

	void		SetIoCoSignal	()
			{
			    _Sys_InterlockedDecrementPtr (& hEvent);

			    if (hEvent)
			    {
				::SetEvent (hEvent);
			    }
			};

	void		CallIoCoProc	(DWord	  dResult      ,
					 void *	  pResultInfo  ,
					 DWord	  dResultEx = 0)
			{
				SetIoCoResult	 (dResult      ,
						  pResultInfo  ,
						  dResultEx    );
				CallIoCoProc ();
			};

	void		CallIoCoProc	()
			{
				hEvent		= NULL ;

			    if (pIIoCoWait)
			    {
				pIIoCoWait ->Release ();
				pIIoCoWait	= NULL ;
			    }
			    if (pIIoCoPort)
			    {
				pIIoCoPort ->Release ();
				pIIoCoPort	= NULL ;
			    }

				IoCoProc pProc	       = pIoCoProc     ;
				void *   pContext      = pIoCoContext  ;
				void *   pArgument     = pIoCoArgument ;

					 pIoCoProc     = NULL ;
					 pIoCoContext  = NULL ;
					 pIoCoArgument = NULL ;
			    if (pProc)
			    {
			       (pProc)(* this, pContext, pArgument);
			    }
			};
//
//[]------------------------------------------------------------------------[]
// IoCoProc inside call completion part
//
//
	Bool		GrabIoCoResult	()
			{
			    if (HasIoCoResult ())
			    {
				return True;
			    }
			    if (Internal == NT_STATUS_SUCCESS)
			    {
				SetIoCoResult (ERROR_SUCCESS, (void *) InternalHigh);

				return True ;
			    }
				return False;
			};

	DWord		GetIoCoResult	    (void *& pResultInfo, DWord * pResultEx = NULL) const
			{
			    pResultInfo = pIoCoResultInfo ;

			if (pResultEx)
			{
			  * pResultEx	= dIoCoResultEx	  ;
			}
			    return	  dIoCoResult	  ;
			};

	DWord		GetIoCoResult	    (DWord & pResultInfo, DWord * pResultEx = NULL) const
			{
			    pResultInfo = (DWord) 
					  pIoCoResultInfo ;
			if (pResultEx)
			{
			  * pResultEx   = dIoCoResultEx	  ;
			}
			    return	  dIoCoResult	  ; 
			};

	DWord		GetIoCoResult	    () const 
			{   return	  dIoCoResult     ;};

	DWord		GetIoCoResultEx	    () const
			{   return	  dIoCoResultEx	  ;};

	void *		GetIoCoResultInfo   () const 
			{   return	  pIoCoResultInfo ;};

    private :

	TSLink <GOverlapped, sizeof (OVERLAPPED)>
			    QLink		;

	void *		    pIoCoResultInfo	;
	DWord		    dIoCoResult		;
	DWord		    dIoCoResultEx	;

	IIoCoWait *	    pIIoCoWait		;
	IIoCoPort *	    pIIoCoPort		;

	IoCoProc	    pIoCoProc		;
	void *		    pIoCoContext	;
	void *		    pIoCoArgument	;
    };

#pragma pack ()
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
/*
class GWakeUp
{
public :
		    GWakeUp     () {};//: m_hIWateUp (NULL), m_pIWakeUp (NULL) {};

		   ~GWakeUp     ()
		    {
			Destroy ();
		    };

	void		Destroy			()
			{
			    if (m_hIWakeUp)
			    {
				::CloseHandle (m_hIWakeUp);
				m_hIWakeUp = NULL;
			    }
			    if (m_pIWakeUp)
			    {
				m_pIWakeUp ->Release ();
				m_pIWakeUp = NULL;
			    }
			};

	HANDLE		GetHandle		() const { return m_hIWakeUp;};

	IWakeUp *	GetIWakeUp		() const { return m_pIWakeUp;};

	GResult		CreateWakeUp		(HANDLE	    hIWakeUp	,
						 IWakeUp *  pIWakeUp	);

	void		QueueWaitFor		(IWakeUp::CallBack
							    pOnProc	,
						 void *	    pOnContext  ,
						 void *	    pOnArgument ,
						 DWord	    dDueTime    = INFINITE,
						 DWord	    dPeriod	= INFINITE);
protected :

	HANDLE		    m_hIWakeUp	;
	IWakeUp *	    m_pIWakeUp	;
};

class GIoCoWakeUp : protected GWakeUp
{
public :

        operator	GOverlapped &		()  { return m_o;};

        void		QueueIoCoWait		();

protected :

	GOverlapped	    m_o		;
};
*/
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
/*
	GResult GAPI	    CreateEventWakeUp	(IWakeUp *&		    , 
						 Bool bNotification = True  ,
						 Bool bInitState    = False );

	void	GAPI	    DestroyEventWakeUp	(IWakeUp *);
*/
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
GFORCEINLINE void * WINAPI __alloc (size_t  sz)
{
    return ::HeapAlloc (::GetProcessHeap (), 0, sz);
};

GFORCEINLINE void * WINAPI __free  (void * ptr)
{
    if (ptr)
    {
	   ::HeapFree (::GetProcessHeap (), 0, ptr);
					       ptr = NULL;
    }
    return ptr;
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
// Allocators
//
    class GAllocator
    {
    public :

    virtual    ~GAllocator () {};

    virtual	void *	__alloc (size_t  sz) = NULL;
    virtual	void *	__free  (void * ptr) = NULL;

		void *  operator new    (size_t s)  { return ::__alloc (s);};
		void    operator delete (void * p)  {	     ::__free  (p);};
    };

    class GProcessAllocator
    {
    public :

		void *	operator new	(size_t s)  { return ::__alloc (s);};
		void	operator delete (void * p)  {	     ::__free  (p);};
    };

class GHeapAllocator : public GAllocator
{
	    DECLARE_STATIC_ALLOCATOR	()

    public :

    static  GHeapAllocator *	Create	(SIZE_T	  szInit ,
					 SIZE_T	  szHigh ,
					 DWord	  dFlags );

    static  void		Destroy (GHeapAllocator *);


		void *	    __alloc  (size_t  sz);
		void *	    __free   (void * ptr);

    private :

        HANDLE		    m_hHeap	 ;
        DWord		    m_dHeapFlags ;
};
//
//[]------------------------------------------------------------------------[]














//[]------------------------------------------------------------------------[]
//
#pragma pack (_M_PACK_VPTR)
/*
	struct IoDp_Request : public GIrp_Request
	{
	    GResult		dIoResult     ;
	    void *		pIoResultInfo ;

	    IoDp_Request * Init (DWord   dCtrlCode			,
				 GResult dIoResult     = ERROR_SUCCESS	,
				 void *  pIoResultInfo = NULL		)
	    {
		GIrp_Request::Init (dCtrlCode);

		this ->dIoResult     = dIoResult     ;
		this ->pIoResultInfo = pIoResultInfo ;

		return this;
	    };
	};
*/
	struct IoCo_Request : public GIrp_Request
	{
	    GResult	    dIoCoResult      ;
	    void *	    pIoCoResultInfo  ;

	    IoCo_Request *  Init (DWord   dCtrlCode			 ,
				  GResult dIoCoResult     = ERROR_SUCCESS,
				  void *  pIoCoResultInfo = NULL         )
	    {
		GIrp_Request::Init    (dCtrlCode);

		this ->dIoCoResult     = dIoCoResult     ;
		this ->pIoCoResultInfo = pIoCoResultInfo ;

		return this;
	    };
	};

#define	IoCo_Close_CtrlCode	0x0300

	struct IoCo_Close : public GIrp_Request
	{
	    IoCo_Close *    Init ()
	    {
		GIrp_Request::Init (IoCo_Close_CtrlCode);

		return this;
	    };
	};

#pragma pack ()


    DECLARE_INTERFACE_ (ISerial, IUnknown)
    {
	typedef enum
	{
	    eof		    = -1,
	    streamraw	    =  0,
	    streamoob	    =  1,
	    stream	    =  2,
	    packet	    =  3,
	    packetpartial   =  4,

	}	DataType;

	typedef enum
	{
//	    stream	    =  0,
/*
	    broadcast
	    multicast
	    singlecast

	    packet

*/
	}	BusType ;

	typedef enum
	{
//	    secunce

	}       FlowType;


#pragma pack (_M_PACK_LONG)

	struct BusAddress
	{
	    Word	wSize	;   // Self reserved structure size
	    Word	wLength	;   // Actual name size
	
//	union
//	{
//	    Byte	dReserved [wSize - sizeof (BusAddress)];
//	    sockaddr	name	;
//	    useraddr	user	;
//	    ...
//	};

			BusAddress  (DWord dSize) { init (dSize);};

	    void	init	    (DWord dSize) { wSize = LOWORD (dSize); clear ();};

	    void	clear	    () { wLength = 0; memset (this + 1, 0, wSize - sizeof (BusAddress));};

	    DWord	get_size    () const { return wSize  ;};

	    DWord	get_len	    () const { return wLength;};
	};

#pragma pack ()

#pragma pack (_M_PACK_VPTR)

				    



#define	IoCo_Connect_CtrlCode	0x0300

	struct IoCo_Connect : public IoCo_Request
	{
	    ISerial *		pSrcISerial ;
	    const BusAddress *	pSrcAddress ;

	    IoCo_Connect *  Init (GResult   dIoCoResult	    ,
				  void *    pIoCoResultInfo ,
				  ISerial * pSrcISerial	    ,
				  const BusAddress *
					    pSrcAddress	    )
	    {
		IoCo_Request::Init (IoCo_Connect_CtrlCode, dIoCoResult, pIoCoResultInfo);

	    if((this ->pSrcISerial = pSrcISerial) != NULL)
	    {	this ->pSrcISerial ->AddRef    ();}

		this ->pSrcAddress = pSrcAddress ;

		return this;
	    };

	    static void GAPI	Destroy (IoCo_Connect * pThis)
				{
				    if (pThis ->pSrcISerial)
				    {
					pThis ->pSrcISerial ->Release ();
					pThis ->pSrcISerial = NULL	;
				    }
				};
	};

#define IoCo_Recv_CtrlCode	0x0301

	struct IoCo_Recv : public IoCo_Request
	{
	    ISerial *		pSrcISerial ;
	    const BusAddress *	pSrcAddress ;

	    DataType		tDataType   ;
	    const void *	pRecvBuf    ;
	    DWord		dRecvBufLen ;

	    IoCo_Recv *	    Init (GResult   dIoCoResult     ,
				  void *    pIoCoResultInfo ,
				  ISerial * pSrcISerial	    ,
				  const BusAddress *
					    pSrcAddress	    ,
				  DataType  tDataType	    ,
			    const void *    pRecvBuf	    ,
				  DWord	    dRecvBufLen	    )
	    {
		IoCo_Request::Init (IoCo_Recv_CtrlCode, dIoCoResult, pIoCoResultInfo);

		this ->pSrcISerial = pSrcISerial ;
		this ->pSrcAddress = pSrcAddress ;
		this ->tDataType   = tDataType	 ;
		this ->pRecvBuf	   = pRecvBuf    ;
		this ->dRecvBufLen = dRecvBufLen ;

		return this;
	    };
        };


#define	IoCo_Send_CtrlCode	0x0302

	struct IoCo_Send : public IoCo_Request
	{
	};

	struct IoCo_Disconnect : public IoCo_Request
	{};


	struct On_ListenReply : public IoCo_Request
	{};

	struct ListenRequest : public GIrp_Request
	{};


	struct ConnectRequest : public GIrp_Request
	{};

	struct DisconnectRequest : public GIrp_Request
	{};

	struct XferRequest : public GIrp_Request
	{
	    const BusAddress * pAddress    ;
	    const void *       pSendBuf    ;
	    DWord	       dSendBufLen ;
	    DWord	       dFlags	   ;

	    XferRequest *   Init (const BusAddress *
					       pAddress    ,
				  const void * pSendBuf    ,
				  DWord	       dSendBufLen ,
				  DWord	       dFalgs	   )
			    {
				GIrp_Request::Init (0x0000);

				this ->pAddress    = pAddress	 ;
				this ->pSendBuf	   = pSendBuf	 ;
				this ->dSendBufLen = dSendBufLen ;
				this ->dFlags	   = dFalgs	 ;

				return this;
			    };
	};

#pragma pack ()

#if FALSE

GIrp * SomeClient::On_Dispatch (GIrp * pIrp)
{
    GIrp_DispatchRequest * rq = GetCurRequest
   <GIrp_DispatchRequest> (pIrp);











};


    DECLARE_INTERFACE  (XDispatchHandle)
    {
    };

    STDMETHOD_ (void,		      Close)(GIrp *) PURE;




    DECLARE_INTERFACE_ (XDispatch)
    {
    STDMETHOD_ (GIrp *,		     Invoke)(GIrp *) PURE;
    };


    XDispatch GAPI	      CreateXDispatcher	(XDispatchHandle **,
						 XDispatchProc
				 GIrp * (GAPI *)(GIrp *, XDispatchHandle *, const void *, void *), const void *, void *);

    template <typename T> GFORCEINLINE 
    XDispatch GAPI	      CreateXDispatcher	(XDispatchHandle **,
			GIrp * (T::*  tDispProc)(GIrp *, XDispatchHandle *, const void *), const void *, T * pDispContext)
    {
	return new TXDispatcher <T> (tProc, pContext);
    };


void GConsole::Connect (XDispatchCallBack, ...)
{
};

{
    m_Console.Connect	    (CreateXDispatcher <Application>
			    (NULL, & Application::On_Console, NULL, this), ...);

    m_TcpAcceptor.Listen    (CreateXDispatcher <Application>
			    (& m_hTcpAcceptor , & Application::On_TcpAccept , NULL, this), ...);

    m_TcpConnector.Connect  (pIUnknown = CreateXDispatcher <Application>
			    (& m_hTcpConnector, & Application::On_TcpConnect, NULL, this), ...);
};

GIrp * SomeClient::On_Transact (GIrp * pIrp, XBindingHandle * hBinding)
{
    ...

    m_hTransact = hBinding;
};


GIrp * SomeClient::On_Console (GIrp * pIrp, XBindingHandle * hBinding)
{





    if (NULL == hBinding)
    {
	Close (m_hConsole);

	m_hConsole = NULL;
    }


    if (NULL == m_hTransact)
    {
	m_pISerial ->Xfer (m_hTransact = m_pIXDispatch ->Bind (& m_hTransact),
			   NULL, pIrp, NULL, pCmdBuf, sizeof (pCmdBuf));
    }
	m_pISerial ->Xfer (m_pIXDispatch ->Bind (CreateIrp


	m_pISerial ->Xfer (m_pIXDispatch ->




    m_pISerial ->Xfer (NULL, NULL, pIrp, NULL, pCmdBuf, sizeof (pCmdBuf);
    m_pISerial ->Xfer (NULL, NULL, pIrp, NULL, pCmdBuf, sizeof (pCmdBuf);
    m_pISerial ->Xfer (NULL, NULL, pIrp, NULL, pCmdBuf, sizeof (pCmdBuf);

    m_pISerial ->Xfer (m_pIX
};

GIrp * SomeClient::On_Dispatch (GIrp * pIrp, XBindingHandle * hBinding  ,
					     const void *     pRequestId)
{
    if (pRequestId == & m_hConsole)
    {
	On_Console (pIrp, hBinding);
    }
    if (pRequestId == & m_hTransact)
    {
	On_Transact (pIrp, hBinding)
    }
};





    m_Console.Create (m_hConsole = m_pIXDispatch ->Bind (& m_hConsole));

    ...
    m_hConsole ->Close (NULL);


    m_WinSock.Connect (m_hWinSock = m_pIXDispatch ->Bind (& m_hWinSock));

    ...
    m_hWinSock ->Close (NULL);

    m_WinSock

void SomeClient::On_Ok ()
{
    XBindingHandle *	hReply; 
    GIrp *		pIrp  ;

    m_pISerial ->Xfer  (hReply = m_pIXDispatch ->Bind (NULL),
			NULL, NULL, pBuf, dBufSize);

    if (NULL != (pIrp = hReply ->ExtractIrpForComplete (200))
    {
    }
   








    STDMETHOD_ (void,	     Listen)(GIrp *	  pIrp	       ,
				     XDispatchCallBack *       ,
			       const BusAddress * pSrcAddress  ,
				     void *	  pRecvBuf     ,
				     DWord	  dRecvBufSize ) PURE;

    STDMETHOD_ (void,	       Xfer)(GIrp *
				     XDispatchCallBack *       ,
			       const BusAddress * pScrAddress  ,
				     void *	  pRecvBuf     ,
				     DWord	  dRecvBufSize ,
				     GIrp *	  pIrp	       ,
			       const BusAddress * pDstAddress  ,
			       const void *	  pSendBuf     ,
				     DWord	  dSendBufLen  ,
				     DWord	  dTimeout     ) PURE;

    DECLARE_INTERFACE_ (IRpc, IUnknown)
    {
    STDMETHOD_ (void,	     Listen)(GIrp *	  pIrp	      ,
				     XDispatchCallBack *      ,
				     void *	  p

    };

#else

    STDMETHOD_ (void,	     Listen)(GIrp *	  pIrp	      ,
				     XDispatchCallBack *      ,
			       const void *	  pRequestId  ,
			       const BusAddress * pSrcAddress ) PURE;

    STDMETHOD_ (void,	    Connect)(GIrp *	  pIrp	      ,
				     XDispatchCallBack *      ,
			       const void *	  pRequestId  ,
			       const BusAddress * pDstAddress ,
			       const void *	  pSendBuf    ,
				     DWord	  dSendBufLen ) PURE;

    STDMETHOD_ (void,	 Disconnect)(GIrp *	  pIrp	      ,
				     XDispatchCallBack *      ,
			       const void *	  pRequestId  ) PURE;

    STDMETHOD_ (void,	       Xfer)(GIrp *	  pIrp	      ,
				     XDispatchCallBack *      ,
			       const void *	  pRequestId  ,
			       const BusAddress * pDstAddress ,
			       const void *	  pSendBuf    ,
			    	     DWord	  dSendBufLen ) PURE;
#endif

#if FALSE

    STDMETHOD_ (void,	       Bind)(XDispatchCallBack *
						  pIRecvCallBack,
			       const BusAddress * pSrcAddress	) PURE;

    STDMETHOD_ (void,	       Xfer)(GIrp *	  pIrp	      ,
			       const BusAddress * pDstAddress ,
				     void *	  pIoBuf      ,
				     DWord	  dIoBufSize  ,
#endif
    };

#if FALSE


    DECLARE_INTERFACE_ (IRRPCallBack, IUnknown)
    {
    DECLARE_INTERFACE  (Handle)
    {
    STDMETHOD_ (GResult,  Duplicate)(Handle *&	  hICallBack,
				     IRRPCallBack *
						  pICallBack) PURE;
    STDMETHOD_ (void,	      Close)() PURE;

    STDMETHOD_ (void,	     Select)(_U32 *	  pParamIds ) PURE;

    STDMETHOD_ (void,	   UnSelect)(_U32 *	  pParamIds ) PURE;

    STDMETHOD_ (void,	    Refresh)(_U32 *	  pParamIds ) PURE;
    };
    STDMETHOD_ (void,	     Invoke)(const void * hICallBack ,
				     const 
				     IRRPProtocol::DataPack *) PURE;
    };

    DECLARE_INTERFACE_ (XDispatch, IUnknown)
    {
	struct Param
	{
	    DWord	dCtrlSize	;
	};

	struct IoCoPack : public Param
	{
	    DWord	dIoCoResult	;
	    DWord	dIoCoResultEx	;
	    void *	pIoCoResultInfo ;

	    GIrp *	pIoIrp		;
	};

    STDMETHOD_	(void,	Xxxx_Invoke)(GIrp *	   pIrp	      ,
				     const void  * hRequsetId ,
				     const Param * pParam     ) PURE;
    };

    DECLARE_INTERFACE_ (ISerial, IUnknown)
    {
    STDMETHOD_	(void,	       Recv)(XDispatch  * pICallBack ,
				     const void * hRequestId ,
				     GIrp *	  pIrp	     ,
				     void *	  pBuf	     ,
				     DWord	  dBufSize   ) PURE;

    STDMETHOD_	(void,	       Send)(XDispatch  * pICallBack ,
				     const void * hRequestId ,
				     GIrp *	  pIrp	     ,
				     const void * pBuf	     ,
				     DWord	  dBufLen    ) PURE;
    };

/*

GIrp * GAPI CoSomeProc (GIrp * pIrp, void *, void *)
{
};

GIrp * GAPI Xxxx_CoSomeProc (GIrp * pIrp, void *, void * pApartment)
{
	   pIrp = 
    ((GApartment *) pApartment) ->QueueIrpForComplete (pIrp, 0, False);	// Not force Queue, may be completed;

    ((GApartmetn *) pApartment) ->Release ();

    return pIrp;
};

GResult GAPI	Send (ISerial * pISerial    ,
		const void    * pSendBuf    ,
		      DWord	dSendBufLen )
{
};

void SomeSend (ISerial * pISerial)
{
    static LPCTSTR  HelloMsg = _T("Hello world from Gary Sh.");

    GIrp * pIrp = new GIrp;

    SetCurCoProc    (pIrp,	CoSomeProc, NULL, NULL);

						  GetCurApartment () ->AddRef ();
    SetCurCoProc    (pIrp, Xxxx_CoSomeProc, NULL, GetCurApartment ());

    pISerial ->Send (pIrp, HelloMsg, ::lstrlen (HelloMsg));
};
*/



void GWinSock::Connect (GIrp *	     pIrp	    ,
			XDispatchCallBack *	    ,
				     pIRecvCallBack ,
		  const BusAddress * pSrcAddress    ,
		  const void *	     pSendBuf	    ,
			DWord	     dSendBufSize   )
{
};


/*
    m_pXDispatch    =  CreateXDispatcher <Application>
		      (& Application::XxxxOn_Dispatch, this);

    Initialize_INet4
   (m_WinSockAddr, "192.168.0.104:50004");


    m_WinSockConnector.CreateConnectRequest 
		       (pIrp = CreateIrp (),
		        AF_INET, IPPROTO_TCP, SOCK_DGRAM, const char * pAddress);
    m_WinSockConnector.


    m_WinSockConnector.SetAdddressConfigure ();

    m_WinSockConnector.Connect (m_pXDispatch ->CreateXDispatchRequest 
	     (NULL, & m_XWinSockConnector, & m_WinSockConnector), "192.168.0.104:50004",
								  GWinSock::INet4      ,
								  GWinSock::IP_TCP     ,
								  GWinSock::stream     ,
								  sqos = NULL	       ));


    m_WinPipeConnector.Connect (m_pXDispatch ->CreateXDispatchRequest
		      (NULL, & m_XWinPipe, & m_WinPipeConnector), "\\\\ServerName\\pipe\\RRPService");


    m_WinSockAcceptor.Listen   (m_pXDispatch ->CreateXDispatchRequest
	       (NULL, & m_XWinSockAcceptor, & m_WinSockAcceptor), NULL		  ,
								  GWinSock::INet4 ,
								  GWinSock::IP_TCP,
								  GWinSock::stream));

    m_WinPipeAcceptor.Listen   (m_pXDispatch ->CreateXDispatchRequest
	       (NULL, & m_XWinPipeAcceptor, & m_WinPipeAcceptor), "\\\\.\\pipe\\RPPSerivce",
								  GWinPipe::fdupliex	   ,
								  GWinPipe::packet	   ,
								  0, 0,
								  NULL			   ));

    m_WinCommConnector.Connect (m_pXDispatch ->CreateXDispatchRequest
	     (NULL, & m_XWinCommConnector, & m_WinCommConnector), "Com4",
								  GWinComm::bound_115200  ,
								  GWinComm::datalen_8	  ,
								  
								  GWinComm::parity_odd	  ,
								  GWinComm::data_2	  )

    m_FtdiConnector.Connect (m_pXDispatch ->CreateXDispatchRequest
	    (NULL, & m_XFtdiConnector, & m_FtdiConnector), "Ftdi",
							   FT_BAUD_115200,
							   FT_BITS_8	 ,
							   FT_STOP_BITS_2,
							   FT_PARITY_ODD ,


    m_pISerial ->Xfer (m_pXDispatch ->CreateXDispatchRequest
	    (





    ...

    m_XWinSock ->



*/





#endif


    DECLARE_INTERFACE_ (ISerialBus, IUnknown)
    {
#pragma pack (_M_PACK_LONG)

	struct Address
	{
	    Word	wSize	;   // Self reserved structure size
	    Word	wLength	;   // Actual name size
	
//	union
//	{
//	    Byte	dReserved [wSize - sizeof (Address)];
//	    sockaddr	name	;
//	    useraddr	user	;
//	    ...
//	};

	    void	init	    (DWord dSize) { wSize = LOWORD (dSize); clear ();};

	    void	clear	    () { wLength = 0;};

	    DWord	get_size    () const { return wSize  ;};

	    DWord	get_len	    () const { return wLength;};
	};

#pragma pack ()

#pragma pack (_M_PACK_VPTR)

    DECLARE_INTERFACE  (IEndPoint)
    {
    STDMETHOD_ (void,	      Close)(GIrp *	  pIrp	      ) PURE;

    STDMETHOD_ (void,	       Xfer)(GIrp *	  pIrp	      ,
			       const Address *	  pDstAddress ,
			       const void *	  pSendBuf    ,
			    	     DWord	  dSendBufLen ) PURE;
    };

    STDMETHOD_ (void,	     Listen)(GIrp *	  pIrp	      ,
			       const Address *	  pAddress    ) PURE;

    STDMETHOD_ (void,	    Connect)(GIrp *	  pIrp	      ,
			       const Address *	  pDstAddress ,
			       const void *	  pSendBuf    ,
				     DWord	  dSendBufLen ) PURE;

    STDMETHOD_ (GResult,     Create)(IEndPoint *& pIEndPoint  ,
			       const Address *	  pSrcAddress ) PURE;
    };

    DECLARE_INTERFACE_ (ITcpBus, ISerialBus)
    {
    };





    DECLARE_INTERFACE_ (ISerialCallBack, XDispatchCallBack)
    {
#pragma pack (_M_PACK_VPTR)

	struct Dp_AcceptRequest : public IoCo_Request
	{
	    ISerial *		pDstISerial ;
	    const ISerial::BusAddress *   
				pDstAddress ;
	    const void *	pRecvBuf    ;
	    DWord		dRecvBufLen ;

	    Dp_AcceptRequest *	Init (GResult	dIoResult     ,
				      void *	pIoResultInfo ,
				      ISerial *	pDstISerial   ,
				const ISerial::BusAddress *
						pDstAddress   ,
				const void *	pRecvBuf      ,
				      DWord	dRecvBufLen   )
	    {
		IoCo_Request::Init (0x0000, dIoResult, pIoResultInfo);

		this ->pDstISerial = pDstISerial ;
		this ->pDstAddress = pDstAddress ;
		this ->pRecvBuf	   = pRecvBuf    ;
		this ->dRecvBufLen = dRecvBufLen ;

		return this;
	    };
	};

	struct Dp_RecvRequest : public IoCo_Request
	{
	    ISerial *		pSrcSerial  ;
	    const ISerial::BusAddress *
				pSrcAddress ;
	    ISerial::DataType	tDataType   ;
	    const void *	pRecvBuf    ;
	    DWord		dRecvBufLen ;

	    Dp_RecvRequest *	Init (GResult	dIoResult     ,
				      void *	pIoResultInfo ,
				const ISerial::BusAddress *
						pSrcAddress   ,
				      ISerial::DataType
						tDataType     ,
				const void *	pRecvBuf      ,
				      DWord	dRecvBufLen   )
	    {
		IoCo_Request::Init (0x55CC, dIoResult, pIoResultInfo);

		this ->pSrcAddress = pSrcAddress ;
		this ->tDataType   = tDataType	 ;
		this ->pRecvBuf	   = pRecvBuf    ;
		this ->dRecvBufLen = dRecvBufLen ;

		return this;
	    };
        };

#pragma pack ()
    };

//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
class GCloseLock
{
//
// Open/CloseRelease can not cross
//
// ...__|-Open-|________________________________________________|-Open-|_...
//
// ..._____________________________|-Close-|_____________________________...
//
// ...___________|...|-Acquire-|-Acquire-|-Acquire-|...|_________________...
//
// ..._________________|...|-Release-|-Release-|-Release-|...|___________...
//

public :

// Public Xxxx_Call methods ------------------------------------------------
//
	Bool		LockAcquire	();

	void		LockRelease	();
//
// Public Xxxx_Call methods ------------------------------------------------

// Public Onwer's methods --------------------------------------------------
//
			GCloseLock ()
			{
			    m_cOpen  .Init (0);
			    m_cClose .Init (0);
			    m_rClose .Init ((GIrp *) INVALID_PTR);
			};

	Bool		Closed		() const { return 0    == m_cOpen .GetValue ();};

	Bool		Opened		() const { return NULL == m_rClose.GetValue ();};

	Bool		CloseAcquire	();

	Bool		Open		();

/*
	void		QueueCreate	(GIrp *);

	void		QueueOpen	(GIrp *);
*/
//		    if (this ->CloseAcquire ())
//		    {
	void		QueueForClose	(GIrp *);
//		    }
// Public Onwer's methods --------------------------------------------------

protected :

static	GIrp * GAPI Xxxx_Co_Close	(GIrp * pIrp, void *, void *);

protected :

	TInterlocked	<LONG  >    m_cOpen  ;
	TInterlocked	<LONG  >    m_cClose ;
	TInterlockedPtr <GIrp *>    m_rClose ;
};
//
//[]------------------------------------------------------------------------[]














#endif __G_WINBASE_H













/*
//[]------------------------------------------------------------------------[]
//
class GWakeUpThread : public GThread
{
protected :
	    GWakeUpThread (int nWakeUpArray, GWakeUpPort::WakeUpEntry * eWakeUpArray) :

	    m_WakeUpPort  (nWakeUpArray, eWakeUpArray) {};
protected :

	    GWakeUpPort	    m_WakeUpPort ;

	void		DispatchWakeUp		();

	Bool		WaitWakeUp		(DWord dTimeout);

	void		QueueIrpForComplete	(TInterlocked <Bool> &, GIrp *, Bool bForceQueue);
};

class GWaitIoThread : public GWakeUpThread
{
protected :

	    GWaitIoThread (int nWakeUpArray, GWakeUpPort::WakeUpEntry * eWakeUpArray) :

	    GWakeUpThread (nWakeUpArray, eWakeUpArray) {};
};

class GWinMsgThread : public GWakeUpThread
{
protected :

	    GWinMsgThread (int nWakeUpArray, GWakeUpPort::WakeUpEntry * eWakeUpArray) :

	    GWakeUpThread (nWakeUpArray, eWakeUpArray) {};

	void		DispatchWakeUp		();

	Bool		WaitWakeUp		(DWord dTimeout);

	void		QueueIrpForComplete	(TInterlocked <Bool> &, GIrp *, Bool bForceQueue);
};

template <class T, int W = MAXIMUM_WAIT_OBJECTS> class TWakeUpThread : public T
{
public :
	    TWakeUpThread () : T (W, (GWakeUpPort::WakeUpEntry *) m_eWakeUpArray) {};
private :
	    void *	    m_eWakeUpArray 

	   [roundup(sizeof (GWakeUpPort::WakeUpEntry) * W, sizeof (void *)) /
							   sizeof (void *)];
};

template <int W = MAXIMUM_WAIT_OBJECTS> class TWaitIoThread : public TWakeUpThread <GWaitIoThread, W>
{};

template <int W = MAXIMUM_WAIT_OBJECTS> class TWinMsgThread : public TWakeUpThread <GWinMsgThread, W>
{};
//
//[]------------------------------------------------------------------------[]
*/

#if FALSE

//[]------------------------------------------------------------------------[]
//
class GApartment : public TXUnknown_Entry <GApartment>
{
public :
			GApartment ()
			{
			    m_pCoCur   = NULL;

//			    Create (IID_IUnknown, this, NULL);
//			    m_IApartment.Init (this, this);
			};

//	GThread::Type	GetType		    () const { return m_tCoType;};

virtual	HRESULT		XUnknown_QueryInterface		(REFIID, void **);

virtual	void		XUnknown_FreeInterface		(void *);

virtual	GWakeUpPort *	GetWakeUpPort	    ();

virtual	GResult		CreateWakeUp	    (IWakeUp   *&);

virtual	GResult		CreateIoCoWait	    (IIoCoWait *&);

	void		QueueIrpForComplete (GIrp * pIrp);

	void		QueueIrpForComplete (GIrp * pIrp, DWord dTimeout);



//	void		QueueForCompleteDelay		    (DWord dTimeout, Bool bCancelled = True);

//	ULONG		GetRefCount	    () const { return /*m_XUnknown.*/GetRefCount ();};

	Bool		LockAcquire	    (Bool bExclusive = True) 
					    { 
						return m_qCoLock.LockAcquire (bExclusive);
					    };
	void		LockRelease	    () {       m_qCoLock.LockRelease ();};

protected :

virtual	void		CompletePacket	    ();

public :

//	void		CompleteNextPacket  ();

virtual	GResult		Run		    ();

protected :

public :

	GSpinCountCriticalSection   m_qCoLock   ;   // Lock for (*)

protected :

	GThread *		    m_pCoThread ;
	GThread::Type		    m_tCoType   ;
	TInterlocked <Bool>	    m_qCoEvent  ;

	GIrp_List		    m_qCoQueue  ;   // (*)
	GIrp *			    m_pCoCur    ;   // (*)
	TInterlocked <int>	    m_cCoCur    ;   // (*)

	GIrp_List		    m_qCoDelay  ;   // (*)
};
//
//[]------------------------------------------------------------------------[]

#endif



/*
//[]------------------------------------------------------------------------[]
//
class GWaitIoThread : public GThread
{
protected :

	GWakeUpPort *	GetWakeUpPort	();

protected :

	    GWaitIoThread (int nWakeUpArray, GWakeUpPort::WakeUpEntry * eWakeUpArray) :

	    m_WakeUpPort  (nWakeUpArray, eWakeUpArray)
	    {};

	Bool		PumpInputWork	();

	void		WaitInputWork	(DWord dTimeout);

protected :

	GWakeUpPort	    m_WakeUpPort ;
};

template <class T, int W = MAXIMUM_WAIT_OBJECTS> class TWaitIoThread : public T
{
public :
	    TWaitIoThread () : T (W, (GWakeUpPort::WakeUpEntry *) m_eWakeUpArray) 
	    {};
private :
	    void *	    m_eWakeUpArray 

	   [roundup(sizeof (GWakeUpPort::WakeUpEntry) * W, sizeof (void *)) /
							   sizeof (void *)];
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
class GWinMsgThread : public GWaitIoThread
{
protected :

	    GWinMsgThread (int nWakeUpArray, GWakeUpPort::WakeUpEntry * eWakeUpArray) :

	    GWaitIoThread (nWakeUpArray, eWakeUpArray)
	    {};

	   ~GWinMsgThread ();

static	DWord GAPI	WaitWakeUpProc	    (int, const HANDLE *, DWord);

	Bool		PumpInputWork	    ();

	void		WaitInputWork	    (DWord dTimeout);
};

template <class T, int W = MAXIMUM_WAIT_OBJECTS> class TWinMsgThread : public T
{
public :
	    TWinMsgThread () : T (W, (GWakeUpPort::WakeUpEntry *) m_eWakeUpArray) 
	    {};
private :
	    void *	    m_eWakeUpArray 

	   [roundup(sizeof (GWakeUpPort::WakeUpEntry) * W, sizeof (void *)) /
							   sizeof (void *)];
};
//
//[]------------------------------------------------------------------------[]
*/
