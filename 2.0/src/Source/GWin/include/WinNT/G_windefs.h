//[]------------------------------------------------------------------------[]
// Global GWin types and macros declaration for Windows system
//
//
// Created by GSh.
//[]------------------------------------------------------------------------[]

#ifndef	__G_WINDEFS_H
#define	__G_WINDEFS_H

// Default Windows & IExplorer version's -----------------------------------[]
//
#ifndef	   _WIN32_WINNT
#   define _WIN32_WINNT	    0x0501	    // <= Windows XP
#endif

#ifndef	    WINVER
#   define  WINVER	   _WIN32_WINNT
#endif

#ifndef	   _WIN32_IE
#   define _WIN32_IE	    0x0600	    // <= IExplorer 6.0
#endif
//
// End Default Windows & IExplorer version's -------------------------------[]

// General C/C++ compiler's definitions ------------------------------------[]
//
// Microsoft & Microsoft compatible compilers <= VC 6.0 defintions ---------[]
//
#if defined (_MSC_VER) && (1200 <= _MSC_VER)

#ifndef	    GFASTCALL
#   define  GFASTCALL	    __fastcall
#endif

#ifndef	    GINLINE
#   define  GINLINE	    __inline
#endif

#ifndef	    GFORCEINLINE   
#   define  GFORCEINLINE    __forceinline
#endif

#   define _MSC_BUG_C2440
#   define _MSC_BUG_C2248
#   define _MSC_BUG_C4003

//  (1400 <= _MSC_VER) -----------------------------------------------------[]
//
#if (1310 <= _MSC_VER)	    // <= VC 7.0/2005 (DDKIFS)

#   undef  _MSC_BUG_C2440
#   undef  _MSC_BUG_C2248

#if (1500 <= _MSC_VER)	    // <= VC 9.0/2008

#if (1600 <= _MSC_VER)	    // <= VC 10.0/2010

#endif
//
//  (1600 <= _MSC_VER) -----------------------------------------------------[]

#endif
//
//  (1500 <= _MSC_VER) -----------------------------------------------------[]

#endif
//
//  (1400 <= _MSC_VER) -----------------------------------------------------[]

// Intel compiler defintions (1200 <= _MSC_VER definied) -------------------[]
//
#if defined (__INTEL_COMPILER)

#if (700 <=  __INTEL_COMPILER)

#   undef  _MSC_BUG_C2440
#   undef  _MSC_BUG_C2248
#   undef  _MSC_BUG_C4003

#   define _MSC_BUG_C2248   // Temporary !!! Must be removed !!!

#if (900 <=  __INTEL_COMPILER)

#endif

#endif
//
//  (700 <= __INTEL_COMPLILER) ---------------------------------------------[]
// ...
//
// End Intel compiler definitions ------------------------------------------[]
#endif
// ...
// End Microsoft & Microsoft compatible compilers <= VC 6.0 defintions -----[]
#else
#   error   Not supported C++ compiler
#endif
//
// End General C/C++ compiler's definitions --------------------------------[]

// Platform specific definitions -------------------------------------------[]
//
#if  defined (_M_IX86 ) && defined (_M_X64) // MS VC10 bug !!! do this definitions
#if !defined (_M_AMD64)
#   define    _M_AMD64	    _M_X64
#endif
#if !defined (_WIN64  )
#   define    _WIN64
#endif
#   undef     _M_IX86
#endif

#if   defined (_M_IX86)			    // A 32-bit 0x86 Intel platform

#   define  _M_ALIGNED	    0
#   define  _M_BIGENDIAN    0
#   define  _M_CACH_SIZE    64
#   define  _M_PAGE_SIZE    4096

#   define  _M_DATA_SIZE    4
#   define  _M_VPTR_SIZE    4

#elif defined (_M_AMD64)		    // A 64-bin AMD processor

#   define  _M_ALIGNED	    0
#   define  _M_BIGENDIAN    0
#   define  _M_CACH_SIZE    64
#   define  _M_PAGE_SIZE    4096

#   define  _M_DATA_SIZE    4
#   define  _M_VPTR_SIZE    8

#elif defined (_M_IA64)			    // A 64-bit Intel Itanium processor

#   define  _M_ALIGNED	    4
#   define  _M_BIGENDIAN    0
#   define  _M_CACH_SIZE    64
#   define  _M_PAGE_SIZE    4096

#   define  _M_DATA_SIZE    4
#   define  _M_VPTR_SIZE    8

#endif
//
// End Platform specific definitions ---------------------------------------[]

// Window's types declarations ---------------------------------------------[]
//
#define	 WIN32_LEAN_AND_MEAN		    // Include main Windows headers
#include <windows.h>
#include <limits.h>
#include <objbase.h>
#include <windowsx.h>
#include <tchar.h>
#include <stdio.h>
//
// Window's types declarations ---------------------------------------------[]

// Define _Sys_XXX macros definitions --------------------------------------[]
//
#if     (0x0400 <= WINVER)
#define _Sys_InterlockedIncrement	    InterlockedIncrement
#else
#define _Sys_InterlockedIncrement(p)	   (InterlockedExchangeAdd (p,  1) + 1)
#endif

#if     (0x0400 <= WINVER)
#define _Sys_InterlockedDecrement	    InterlockedDecrement
#else
#define _Sys_InterlockedDecrement(p)	   (InterlockedExchangeAdd (p, -1) - 1)
#endif

#define _Sys_InterlockedExchangeAdd	    InterlockedExchangeAdd

#define	_Sys_InterlockedExchange	    InterlockedExchange
#define _Sys_InterlockedCompareExchange	    InterlockedCompareExchange

#ifdef  _WIN64
#define	_Sys_InterlockedIncrementPtr(p)	    ((void *) InterlockedIncrement64	  ((LONGLONG volatile *) p))
#define	_Sys_InterlockedDecrementPtr(p)	    ((void *) InterlockedDecrement64	  ((LONGLONG volatile *) p))
#define	_Sys_InterlockedExchangeAddPtr(p,v) ((void *) InterlockedExchangeAdd64	  ((LONGLONG volatile *) p, (LONGLONG) v))
#else
#define	_Sys_InterlockedIncrementPtr(p)	    ((void *) _Sys_InterlockedIncrement   ((	LONG volatile *) p))
#define	_Sys_InterlockedDecrementPtr(p)	    ((void *) _Sys_InterlockedDecrement	  ((	LONG volatile *) p))
#define	_Sys_InterlockedExchangeAddPtr(p,v) ((void *) _Sys_InterlockedExchangeAdd ((	LONG volatile *) p, (	 LONG) v))
#endif

#define	_Sys_InterlockedExchangePtr	    InterlockedExchangePointer
#define _Sys_InterlockedCompareExchangePtr  InterlockedCompareExchangePointer

#define	_Sys_GetTickCount		    GetTickCount
//
// End define _Sys_XXX macros definitions ----------------------------------[]
//
//...
// End platform specific declaration, Include g_stddefs.h ------------------[]

#include "g_stddefs.h"
#define	  INITGUID
#include "G_winguid.h"
#undef	  INITGUID

//[]------------------------------------------------------------------------[]
// Thunk mechanics
//
// Description :
//
//  type __stdcall  CallBack    (			      p1,...,pn);
//
//  Entry to CallBack_ by jmp command.
//
//  type __stdcall  CallBack_	(const __stdcallThunk * thnk, p1,...,pn)
//  {
//	thnk = GetThisPtr (thnk);
//
//	...
//
//	return type;
//  };

#if defined  (_M_IX86)

#pragma pack (_M_PACK_BYTE)

    struct  __stdcallThunk
    {
	_U8     _cPush [3]	    ;   //  push  [esp    ]
	_U8     _cCall [1]	    ;   //  call  [eip    ] + _oCall = & _cPop;
	_U32    _oCall		    ;
	_U8     _cPop  [4]	    ;   //  pop   [esp + 4] 
	_U8     _cJump [1]	    ;   //  jmp   [eip    ] + _oJump;
	_U32    _oJump   	    ;
	_U8	_cNop  [3]	    ;   //  roundup U32 nop

	    typedef void (__stdcall *	ThunkProc)(const __stdcallThunk *);

	    __stdcallThunk ()
	    {
//	        _cPush  [0] = 0xFF  ;
//	        _cPush  [1] = 0x34  ;
//	        _cPush  [2] = 0x24  ;
//
//	        _cCall  [0] = 0xE8  ;
//		_oCall      = 0     ;
//
//		_cPop   [0] = 0x8F  ;
//		_cPop   [1] = 0x44  ;
//		_cPop   [2] = 0x24  ;
//		_cPop   [3] = 0x04  ;
//
//		_bJmp   [0] = 0xE9  ;
//		_oJmp	    = 0     ;
//
//		_cNop   [0] = 0x90  ;
//		_cNop   [1] = 0x90  ;
//		_cNop   [2] = 0x90  ;

		* ((_U32 *) this + 0) = 0xE82434FF;
		* ((_U32 *) this + 1) = 0x00000000;
		* ((_U32 *) this + 2) = 0x0424448F;
		* ((_U32 *) this + 3) = 0x000000E9;
		* ((_U32 *) this + 4) = 0x90909000;
	    };

	    void	    SetDispPtr	(ThunkProc pProc)
	    {
		_oJump = (_U32) pProc - (_U32)(& _oJump + 1);

		::FlushInstructionCache (::GetCurrentProcess   (),
					 this, sizeof (__stdcallThunk));
	    };

	    Bool	    IsThunkPtr	(UINT sz, ThunkProc pProc) const
	    {
		return (::IsBadReadPtr (this, sz) ||
 
		((_U32) pProc != (_oJump + (_U32)(& _oJump + 1)))) ? False : True;
	    };

	    const __stdcallThunk *	GetThisPtr () const
	    {
		return (const __stdcallThunk *)((const Byte *) this - offsetof (__stdcallThunk, _cPop));
	    };
    };

#pragma pack ()

#elif True
//	    ... Attention ...
#else
#   error   Not supported target platform.
#endif
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
    DECLARE_INTERFACE_ (IWakeUp, IUnknown)
    {
    // IWakeUp methods
    //
	typedef	enum Reason
	{
	    OnCancelled	= 0,	// Wait operation was cancelled !!!
	    OnOccured	= 1,
	    OnTimeout	= 2
	};

	typedef Bool	      (GAPI *	     CallBack		    )
			      (		     IWakeUp *  hWakeUp	    ,
					     Reason     eOnReason   ,
					     void *     pOnContext  ,
					     void *     pOnArgument );

    STDMETHOD_ (void,	       QueueWaitFor)(HANDLE	hWaitFor    ,
					     CallBack   pOnProc	    ,
					     void *     pOnContext  ,
					     void *     pOnArgument ,
					     DWord      dDueTime    = INFINITE,
					     DWord	dPeriod	    = INFINITE) PURE;

    STDMETHOD_ (void,	      CancelWaitFor)(Bool	bCallBack   = False   ) PURE;
    };
//
//[]------------------------------------------------------------------------[]


    DECLARE_INTERFACE_ (XDispatch, IUnknown)
    {
    STDMETHOD_ (GIrp *,		     Invoke)(GIrp *)	   PURE;
    };

    typedef XDispatch	XDispatchCallBack;



    class GOverlapped;

    DECLARE_INTERFACE_ (IIoCoWait, IUnknown)
    {
    // IIoCoWait methods
    //
/*
    STDMETHOD_ (HANDLE,	    
*/
    STDMETHOD_ (HANDLE,	      GetIoCoHandle)()		    PURE;

    STDMETHOD_ (void,	      QueueIoCoWait)(GOverlapped &) PURE;
    };


    DECLARE_INTERFACE  (XHandle)
    {
    STDMETHOD_ (XHandle *,	  Duplicate)() PURE;

    STDMETHOD_ (void,		      Close)() PURE;
    };












// Define base GWin proc types ---------------------------------------------[]
//
    typedef GResult (GAPI * GWinMainProc)(int nArgs, void ** pArgs, HANDLE hResume);
//
//[]------------------------------------------------------------------------[]



//
//[]------------------------------------------------------------------------[]
//

    DECLARE_INTERFACE_ (IIoCoPort, IUnknown)
    {
    // IIoCoPort methods
    //
    STDMETHOD_ (void,	    QueueIoCoStatus)(GOverlapped &) PURE;
    };

    DECLARE_INTERFACE_ (XIoCoHandler, XHandle)
    {
    // IIoCoHandler methods
    //
    };


    class GThreadBase
    {
    public :

	typedef enum
	{
	    User	 = 0,
	    Work	    ,
	    WorkIo	    ,
	    WaitIo	    ,

	}   Type ;
    };

    DECLARE_INTERFACE_ (IThreadPool, IUnknown)
    {
    // IThreaPool methods
    //
    STDMETHOD_	(GResult,     QueueWorkItem)(GThreadBase::Type	     ,
					     GWinMainProc pWorkProc  ,
					     int	  nWorkArgs  ,
					     void **	  pWorkArgs  ) PURE;

    STDMETHOD_	(void,	QueueIrpForComplete)(GThreadBase::Type	     ,
					     GIrp *	  pIrp	     ) PURE;
/*
    STDMETHOD_	(void,  QueueIrpForDispatch)(GThreadBase::Type	     ,
					     GIrp *	  pIrp	     ) PURE;
*/
    STDMETHOD_	(GResult,      CreateWakeUp)(IWakeUp   *& pIWakeUp   ) PURE;

    STDMETHOD_	(GResult,    CreateIoCoPort)(IIoCoPort *& pIIoCoPort ,
					     HANDLE	  hFileHandle) PURE;
    };


    GResult GAPI    CreateWakeUp	(IWakeUp   *&);

    GResult GAPI    CreateIoCoPort	(IIoCoPort *&, HANDLE hFileHandle);



#endif // __G_WINDEFS_H
