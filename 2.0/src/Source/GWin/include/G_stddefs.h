//[]------------------------------------------------------------------------[]
// Global types and macros declaration
//
//
// Created by GSh.
//[]------------------------------------------------------------------------[]

#ifndef __G_STDDEFS_H
#define __G_STDDEFS_H

// Default platform specific declarations ----------------------------------[]
//
// A Default (Common) for 32-bit platform
//
#ifndef	    _M_ALIGNED
#   define  _M_ALIGNED	    1		// Data aligned (most processors) as default
#endif

#ifndef	    _M_BIGENDIAN
#   define  _M_BIGENDIAN    0		// Data LittleEndian (Intel) as default
#endif

#ifndef	    _M_CACH_SIZE
#   define  _M_CACH_SIZE    16		// Default cach size as 128 bit
#endif

#ifndef	    _M_PAGE_SIZE
#   define  _M_PAGE_SIZE    4096	// Default page size as 4 K
#endif

#   define  _M_BYTE_SIZE    1
#   define  _M_WORD_SIZE    2
#   define  _M_LONG_SIZE    4

#ifndef	    _M_DATA_SIZE
#   define  _M_DATA_SIZE    _M_LONG_SIZE
#endif

#ifndef	    _M_VPTR_SIZE
#   define  _M_VPTR_SIZE    _M_DATA_SIZE
#endif

#   define  _M_PACK_BYTE    _M_BYTE_SIZE
#   define  _M_PACK_WORD    _M_WORD_SIZE
#   define  _M_PACK_LONG    _M_LONG_SIZE
#   define  _M_PACK_DATA    _M_DATA_SIZE
#   define  _M_PACK_VPTR    _M_VPTR_SIZE
#   define  _M_PACK_RDBL    8
//
// End Default platform specific declarations ------------------------------[]

// Define Windows's types & macoros ----------------------------------------[]
//
#ifndef	_INC_WINDOWS	    // Windows.h not included define !!! :

#ifndef	    STDAPICALLTYPE
#   define  STDAPICALLTYPE	__stdcall
#endif

#ifndef	    STDMETHODCALLTYPE
#   define  STDMETHODCALLTYPE	__stdcall
#endif

#ifndef	    DECLSPEC_ALIGN
#   define  DECLSPEC_ALIGN(x)
#endif

#ifndef	    DECLSPEC_IMPORT	
#   define  DECLSPEC_IMPORT
#endif

#ifndef	    DECLSPEC_UUID
#   define  DECLSPEC_UUID
#endif

#ifndef	    DECLSPEC_NOVTABLE
#   define  DECLSPEC_NOVTABLE
#endif

#ifndef	    EXTERN_C
#   define  EXTERN_C	    extern "C"
#endif

#ifndef	    STDAPI_
#   define  STDAPI_(type)   EXTERN_C type STDAPICALLTYPE
#endif

#ifndef	    STDAPI
#   define  STDAPI	    STDAPI_(HRESULT)
#endif

    typedef int		    BOOL	;
    typedef unsigned char   BYTE	;
    typedef unsigned short  WORD	;
    typedef unsigned long   DWORD	;
    typedef char	    CHAR	;
    typedef unsigned short  WCHAR	;
    typedef int		    INT		;
    typedef unsigned int    UINT	;
    typedef long	    LONG	;
    typedef unsigned long   ULONG	;
    typedef CHAR	    TCHAR	;

    typedef int		    INT_PTR	;
    typedef unsigned int    UINT_PTR	;
    typedef long	    LONG_PTR	;
    typedef unsigned long   ULONG_PTR	;
    typedef void *	    HANDLE	;
    typedef UINT_PTR	    WPARAM	;
    typedef ULONG_PTR	    LPARAM	;
    typedef ULONG_PTR	    LRESULT	;
    typedef LONG	    HRESULT	;

#ifndef	    NULL
#   define  NULL	    0L
#endif

#ifndef	    FALSE
#   define  FALSE	    0
#   define  TRUE	    1
#endif

#ifndef	    INFINITE
#   define  INFINITE	    0xFFFFFFFF
#endif

#ifndef	    LOBYTE
#   define  LOBYTE(w)	    ((BYTE)(w))
#endif

#ifndef	    HIBYTE
#   define  HIBYTE(w)	    ((BYTE)(((WORD)(w)) >> 8))
#endif

#ifndef	    LOWORD
#   define  LOWORD(d)	    ((WORD)(d))
#endif

#ifndef	    HIWORD
#   define  HIWORD(d)	    ((WORD)(((DWORD)(d)) >> 16))
#endif

#pragma pack (_M_PACK_LONG)

    typedef struct _POINT   {LONG x    ; LONG y	    ;	 }  POINT ;
    typedef struct _RECT    {LONG left ; LONG top   ;
			     LONG right; LONG bottom;	 }  RECT  ;
#pragma pack ()

// Define COM like Interface's macros --------------------------------------[]
//
    typedef struct _GUID    {DWORD Data1; WORD Data2;
					  WORD Data3;
					  BYTE Data4 [8];}  GUID  ;
    typedef GUID	    IID	  ;
    typedef GUID	    CLSID ;

#   define  REFGUID   const GUID  &
#   define  REFIID    const IID	  &
#   define  REFCLSID  const CLSID &

#ifndef	    interface
#   define  interface struct
#endif

#ifndef	    STDMETHOD
#   define  STDMETHOD(theMethod)				\
    virtual HRESULT STDMETHODCALLTYPE	theMethod

#   define  STDMETHOD_(theType,theMethod)			\
    virtual theType STDMETHODCALLTYPE	theMethod
#endif

#ifndef	    PURE
#   define  PURE      = 0
#endif

#ifndef	      DECLARE_INTERFACE
#   define    DECLARE_INTERFACE(  theInterface)			\
    interface DECLSPEC_NOVTABLE   theInterface

#   define    DECLARE_INTERFACE_( theInterface, theBase)	\
    interface DECLSPEC_NOVTABLE	  theInterface : public theBase
#endif

    DECLARE_INTERFACE  (IUnknown)
    {
    // IUnknown methods
    //
    STDMETHOD	(	 QueryInterface)(REFIID, void **) PURE;
    STDMETHOD_	(ULONG,		 AddRef)()		  PURE;
    STDMETHOD_	(ULONG,		Release)()		  PURE;
    };

#endif
//
// End Window's types definitions ------------------------------------------[]

// Define base GWin types --------------------------------------------------[]
//
#   define  GAPI	    STDAPICALLTYPE

#ifndef	    GFASTCALL
#   define  GFASTCALL	    GAPI
#endif

#ifndef	    GINLINE
#   define  GINLINE	    inline
#endif

#ifndef	    GFORCEINLINE
#   define  GFORCEINLINE    GINLINE
#endif

#ifndef	    GFASTCALLBACK
#   define  GFASTCALLBACK   GFASTCALL
#endif

#   define  LPCIID	    const IID	*
#   define  LPCCLSID	    const CLSID *

// Must be removed !!! Use Microsoft DECLARE_INTERFACE macro !!! .....
//
#   define  DECLARE_GINTERFACE(theInterface)		\
    interface GNOVTABLE			theInterface

#   define  DECLARE_GINTERFACE_(theInterface,theBase)	\
    interface GNOVTABLE			theInterface : public theBase
//
// Must be removed !!! Use Microsoft DECLARE_INTERFACE macro !!! .....

    typedef BOOL	Bool	;
    typedef BYTE	Byte	;
    typedef WORD	Word	;
    typedef DWORD	DWord	;
    typedef LONG	Long	;
    typedef TCHAR	Char	;

    typedef char	_I8	;
    typedef BYTE	_U8	;

    typedef short	_I16	;
    typedef WORD	_U16	;

    typedef LONG	_I32	;
    typedef ULONG	_U32	;

    typedef float	_R32	;
    typedef double	_R64	;

    typedef WPARAM	WParam	;
    typedef LPARAM	LParam  ;

    typedef LRESULT	LResult ;
    typedef HRESULT	HResult ;
    typedef DWord	GResult ;

#ifndef	    False
#   define  False	FALSE
#   define  True	TRUE
#endif

#   define  INVALID_PTR	((void *)((LONG_PTR) -1))
//
// End Define base GWin types ----------------------------------------------[]

// Default _Sys_XXX macros definitions -------------------------------------[]
//
#ifndef	_Sys_InterlockedIncrement
#define _Sys_InterlockedIncrement	    _Non_InterlockedIncrement

GFORCEINLINE LONG _Non_InterlockedIncrement (LONG volatile * Target)
{
    return ++ (* Target);
};
#endif

#ifndef _Sys_InterlockedDecrement
#define _Sys_InterlockedDecrement	    _Non_InterlockedDecrement

GFORCEINLINE LONG _Non_InterlockedDecrement (LONG volatile * Target)
{
    return -- (* Target);
};
#endif

#ifndef _Sys_InterlockedExchange
#define	_Sys_InterlockedExchange	    _Non_InterlockedExchange

GFORCEINLINE LONG _Non_InterlockedExchange (LONG volatile * Target,
					    LONG Exchange)
{
    LONG  Res = * Target;
    {
       (* Target) = Exchange;
    }
    return Res;
};
#endif

#ifndef	_Sys_InterlockedExchangeAdd
#define _Sys_InterlockedExchangeAdd	    _Non_InterlockedExchangeAdd

GFORCEINLINE LONG _Non_InterlockedExchangeAdd (LONG volatile * Target,
					       LONG Value)
{
    LONG  Res = * Target;
    {
       (* Target) += Value;
    }
    return Res;
};
#endif

#ifndef	_Sys_InterlockedCompareExchange
#define _Sys_InterlockedCompareExchange	    _Non_InterlockedCompareExchange

GFORCEINLINE LONG _Non_InterlockedCompareExchange (LONG volatile * Target,
						   LONG	Exchange,
						   LONG Comperand)
{
    LONG  Res = * Target;

    if (Comperand == Res)
    {
	* Target = Exchange;
    }
    return Res;
};
#endif

#ifndef _Sys_InterlockedExchangePtr
#define	_Sys_InterlockedExchangePtr	    _Non_InterlockedExchangePtr

GFORCEINLINE void * _Non_InterlockedExchangePtr (void * volatile * Target  ,
						 void		 * Exchange)
{
    void * Res	  = * Target;

    {
	 * Target = Exchange;
    }
    return Res;
};
#endif

#ifndef _Sys_InterlockedCompareExchangePtr
#define _Sys_InterlockedCompareExchangePtr  _Non_InterlockedCompareExchangePtr

GFORCEINLINE void * _Non_InterlockedCompareExchangePtr (void * volatile * Target   ,
							void *		  Exchange ,
							void *		  Comperand)
{
    void * Res	  = * Target;

    if (Comperand == Res)
    {
	 * Target = Exchange;
    }
    return Res;
};
#endif

#ifndef _Sys_InterlockedExchangeAddPtr
#define _Sys_InterlockedExchangeAddPtr	    _Non_InterlockedExchangeAddPtr

GFORCEINLINE void * _Non_InterlockedExchangeAddPtr (void * volatile * Target, LONG Add)
{
    void * Res	  = * Target;

    (* (unsigned char * volatile * &) Target) += Add;

    return Res;
};
#endif

#ifndef _Sys_InterlockedIncrementPtr
#define	_Sys_InterlockedIncrementPtr	    _Non_InterlockedIncrementPtr

GFORCEINLINE void * _Non_InterlockedIncrementPtr (void * volatile * Target)
{
    return ++ (* (unsigned char * volatile * &) Target);
};
#endif

#ifndef _Sys_InterlockedDecrementPtr
#define	_Sys_InterlockedDecrementPtr	    _Non_InterlockedDecrementPtr

GFORCEINLINE void * _Non_InterlockedDecrementPtr (void * volatile * Target)
{
    return -- (* (unsigned char * volatile * &) Target);
};
#endif

#ifndef _Sys_GetTickCount
#define	_Sys_GetTickCount		    _Null_GetTickCount

GFORCEINLINE DWord _Null_GetTickCount ()
{
    return 0;
};
#endif
//
// End Default _Sys_XXX macros definitions ---------------------------------[]

// Some Util macros --------------------------------------------------------[]
//
#ifndef	    roundup
#   define  roundup(v,b)	((((size_t) v + b - 1) / b) * b)
#endif

#ifndef	    offsetof
#   define  offsetof(s,m)	((size_t)&(((s *)NULL)->m))
#endif

#ifndef	    ptrbyoffset
#   define  ptrbyoffset(b,o)	((void *)((Byte *) b + (size_t) o))
#endif

#ifndef	    offsetbyptr
#   define  offsetbyptr(b,p)	((size_t)((Byte *) p - (Byte *) b))
#endif

#ifndef	    gabs
#   define  gabs(a)		((0 > a) ? (- (a)) : (a))
#endif

#   define  round_I32(f)	((_I32)(f + 0.5))
//
// End Util macros ---------------------------------------------------------[]

// Some C++ extensions -----------------------------------------------------[]
//
	class	fooClass;

	typedef void * FOO_PMF [roundup(sizeof (void(fooClass::*)(void)), sizeof (void *)) /
									  sizeof (void *)];

#define DECLARE_STATIC_ALLOCATOR()			\
							\
	void *	operator new	(size_t, void * ptr)	\
				{return ptr;};		\
	void	operator delete (void *)	 {};	\
	void	operator delete (void *, void *) {};

	struct	GStaticAllocator { DECLARE_STATIC_ALLOCATOR ()};

	class	GObject
	{
	public :

		void *	operator new	    (size_t);
		void  	operator delete	    (void *);

		void *	operator new	    (size_t, void *);
		void	operator delete	    (void *, void *) {};

			GObject	    () {};

	static	void	Delete	    (GObject * o);

	protected :

	virtual	       ~GObject	    ();

	virtual	void    ShutDown    ();
	};

void  GFORCEINLINE  MoveX (POINT * p, LONG dx)
		    {
			p ->x	+= dx;
		    };

void  GFORCEINLINE  MoveY (POINT * p, LONG dy)
		    {
			p ->y	+= dy;
		    };

void  GFORCEINLINE  Move  (POINT * p, LONG dx, LONG dy)
		    {
			MoveX (p, dx);
			MoveY (p, dy);
		    };

void  GFORCEINLINE  MoveX (RECT *  r, LONG dx)
		    {
			r ->left  += dx;
			r ->right += dx;
		    };

void  GFORCEINLINE  MoveY (RECT *  r, LONG dy)
		    {
			r ->top	   += dy;
			r ->bottom += dy;
		    };

void  GFORCEINLINE  Move  (RECT *  r, LONG dx, LONG dy)
		    {
			MoveX (r, dx);
			MoveY (r, dy);
		    };
//
//--------------------------------------------------------------------------[]
//
#pragma pack (_M_PACK_LONG)

    struct GPoint : public POINT
    {
			GPoint	    () {};

			GPoint	    (LONG lx, LONG ly) { x = lx; y = ly;};

			GPoint	    (const GPoint & p) { x = p.x; y = p.y;};

	void		Move	    (LONG dx, LONG dy) { x += dx; y += dy; };

        GPoint &	operator += (const GPoint & p) { Move (  p.x,   p.y); return * this;};

	GPoint &	operator -= (const GPoint & p) { Move (- p.x, - p.y); return * this;};

	GPoint 		operator -  (const GPoint & p) const 
						       { return GPoint (x - p.x, y - p.y);};

	GPoint 		operator +  (const GPoint & p) const 
						       { return GPoint (x + p.x, y + p.y);};

	GPoint &	operator =  (const GPoint & p) { return (x = p.x, y = p.y), * this;}

	Bool		operator == (const GPoint & p) const { return   Equal (p);};

	Bool		operator != (const GPoint & p) const { return ! Equal (p);};

	GPoint &	Assign	    (LONG px, LONG py) { x  = px; y  = py;    return * this;};

	Bool		Equal	    (const GPoint & p) const { return (x == p.x) && (y == p.y);};
    };

    struct GRect
    {
	LONG		x   ;
	LONG		y   ;
	LONG		cx  ;
	LONG		cy  ;

	LONG		left	    () const { return		  x; };
	LONG		top	    () const { return		  y; };
	LONG		right	    () const { return	     x + cx; };
	LONG		bottom	    () const { return	     y + cy; };

	RECT   &	Rect	    () const { return (RECT   &)  x; };
	GPoint &	Pos	    () const { return (GPoint &)  x; };
	GPoint &	Size	    () const { return (GPoint &) cx; };

	void		MoveX	    (LONG dx)	       {  x += dx;};
	void		MoveY	    (LONG dy)	       {  y += dy;};
	void		Move	    (LONG dx, LONG dy) {  x += dx;  y += dy;};
	void		Move	    (const GPoint & p) {  Pos () += p;};

	void		GrowX	    (LONG dx)	       { cx += dx;};
	void		GrowY	    (LONG dy)	       { cy += dy;};
	void		Grow	    (LONG dx, LONG dy) { cx += dx; cy += dy;};
	void		Grow	    (const GPoint & p) { Size () += p;};

	GRect &		operator =  (const GRect & r)  { return (x = r.x, y = r.y, cx = r.cx, cy = r.cy), * this;};

	Bool		operator == (GRect & r) const  { return   Equal (r);};

	Bool		operator != (GRect & r) const  { return ! Equal (r);};

	GRect &		Assign	    (LONG px, LONG py,
				     LONG cx, LONG cy) { return (x = px, y = py, this ->cx = cx, this ->cy = cy), * this;};

	Bool		Equal	    (GRect & r)	       const { return (x == r.x) && (y == r.y) && (cx == r.cx) && (cy == r.cy);};

	Bool		Contains    (LONG px, LONG py) const { return (0  <= (px -= x)) && (0  <= (py -= y))
								  &&  (px <	   cx ) && (py <        cy );};
	Bool		Intersect   (GRect & r)
			{
			    return False;
			};
    };

#pragma pack ()

void  GFORCEINLINE	CopyRect (RECT  * d, const GRect * s)
			{
			    d ->right  = (d ->left = s ->x) + s ->cx;
			    d ->bottom = (d ->top  = s ->y) + s ->cy;
			};

void  GFORCEINLINE	CopyRect (GRect * d, const RECT  * s)
			{
			    d ->cx = s ->right  - (d ->x = s ->left);
			    d ->cy = s ->bottom - (d ->y = s ->top );
			};
//
//--------------------------------------------------------------------------[]
//
//
// End Some C++ extensions -------------------------------------------------[]

//[]------------------------------------------------------------------------[]
// Timer's Tick counter
//
//
GFORCEINLINE DWord SubTicks (DWord d1, DWord d2)
{
    return d1 - d2;
};

GFORCEINLINE DWord AddTicks (DWord d1, DWord d2)
{
    return d1 + d2;
};

GFORCEINLINE DWord HotTicks (DWord dCur, DWord dTimeout)
{
    DWord dHot =  AddTicks (dCur, dTimeout);

    return (INFINITE != dHot) ? dHot : 0;
};

GFORCEINLINE DWord RelativeTicks (DWord dCur, DWord dHot)
{
    DWord dSub = SubTicks (dHot, dCur);

    return (DWord) ((0 < ((Long) dSub)) ? dSub : 0);
};

GFORCEINLINE DWord ElapsedTicks (DWord dStart, DWord dCur)
{
    DWord dSub = SubTicks (dCur, dStart);

    return (DWord) ((0 < ((Long) dSub)) ? dSub : 0);
};

GFORCEINLINE DWord ElapsedTicks (DWord dStart)
{
    return ElapsedTicks (dStart, _Sys_GetTickCount ());
};

GFORCEINLINE Bool OccuredTicks (DWord dCur, DWord dHot)
{
    return (0 < ((Long) SubTicks (dHot, dCur))) ? False : True;
};

class GTimeout
{
public :
		GTimeout    () {Disactivate ();};

	Bool	Infinite    () const { return m_dHotCount == INFINITE;};

	Bool	NotInfinite () const { return m_dHotCount != INFINITE;};

	DWord	HotCount    () const { return m_dHotCount;};

	void	Disactivate ()	     {	      m_dHotCount  = INFINITE;};

	Bool	Activate    (DWord dTimeout,
			     DWord dCurrent) { return (dTimeout != INFINITE) 
					     ? ((m_dHotCount = HotTicks (dCurrent	     , dTimeout)), True)
					     : ( Disactivate (), False);};

	Bool	Activate    (DWord dTimeout) { return (dTimeout != INFINITE)
					     ? ((m_dHotCount = HotTicks (_Sys_GetTickCount (), dTimeout)), True)
					     : ( Disactivate (), False);};

	Bool	Occured	    (DWord dCurrent) const { return  (NotInfinite ())
					     ? OccuredTicks  (dCurrent,		    m_dHotCount) : False;};

	Bool	Occured	    () const		   { return  (NotInfinite ()) 
					     ? OccuredTicks  (_Sys_GetTickCount (), m_dHotCount) : False;};

	DWord	Relative    (DWord dCurrent) const { return  (NotInfinite ())
					     ? RelativeTicks (dCurrent,		    m_dHotCount) : m_dHotCount;};

	DWord	Relative    () const		   { return  (NotInfinite ())
					     ? RelativeTicks (_Sys_GetTickCount (), m_dHotCount) : m_dHotCount;};
private :

	DWord		    m_dHotCount;    // INFINITE if not activated
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
// Interlocked extention
//
class GInterlocked
{
protected :

	LONG		Init		(LONG l) { return  (m_lValue = l, l);};

	LONG		GetValue	() const { return   m_lValue;};

	LONG volatile *	GetValuePtr	()	 { return & m_lValue;};

	LONG		ExchangeAdd	(LONG l)
			    { return _Sys_InterlockedExchangeAdd (& m_lValue, l) + l;};

	LONG		Exchange	(LONG l)
			    { return _Sys_InterlockedExchange    (& m_lValue, l);};

	LONG		CompareExchange (LONG l, LONG c)
			    { return (LONG) _Sys_InterlockedCompareExchange
								 (& m_lValue, l, c);};

        LONG operator ++ ()	    // ++ v
			    { return _Sys_InterlockedIncrement   (& m_lValue);};

	LONG operator ++ (int)	    // v ++
			    { return _Sys_InterlockedExchangeAdd (& m_lValue, 1);};

        LONG operator -- ()	    // -- v
			    { return _Sys_InterlockedDecrement   (& m_lValue);};

	LONG operator -- (int)	    // v ++
			    { return _Sys_InterlockedExchangeAdd (& m_lValue, -1);};
    private :

        LONG		m_lValue;
};

class GInterlockedPtr
{
protected :

	void *		    Init	(void * p) { return   (m_pValue = p, p);};

	void *		    GetValue	() const   { return    m_pValue;};

	void * volatile *   GetValuePtr ()	   { return  & m_pValue;};

	void *		ExchangeAdd	(LONG l)
			    { return _Sys_InterlockedExchangeAddPtr(& m_pValue, l);};

	void *		Exchange	(void * p)
			    { return _Sys_InterlockedExchangePtr   (& m_pValue, p);};

	void *		CompareExchange (void * p, void * c)
			    { return _Sys_InterlockedCompareExchangePtr
								   (& m_pValue, p, c);};
	void * operator ++ ()	    // ++ v
			    { return _Sys_InterlockedIncrementPtr  (& m_pValue);};

	void * operator ++ (int)    // ++ v
			    { return _Sys_InterlockedExchangeAddPtr(& m_pValue, 1);};

	void * operator -- ()	    // -- v
			    { return _Sys_InterlockedDecrementPtr  (& m_pValue);};

	void * operator -- (int)
			    { return _Sys_InterlockedExchangeAddPtr(& m_pValue, -1);};
private :

	void *		m_pValue;
};

template <class T>
class TInterlocked : private GInterlocked
{
public :
			TInterlocked	() { GInterlocked::Init (0);};

			TInterlocked	(const T t) { Init (t);};

	T		Init		(const T t) { return (T) GInterlocked::Init    (t);};

	T		GetValue	() const    { return (T) GInterlocked::GetValue ();};

	T volatile *	GetValuePtr	()	    { return (T volatile *) GInterlocked::GetValuePtr ();};

	T	operator =  (const T & t)	    { return Init (t);};

		operator T  () const	{ return GetValue ();};

        T	operator ++ ()		{ /* ++ v */ return (T)(GInterlocked::operator ++ ( ));};

	T	operator ++ (int)	{ /* v ++ */ return (T)(GInterlocked::operator ++ (1));};

        T	operator -- ()		{ /* -- v */ return (T)(GInterlocked::operator -- ( ));};

	T	operator -- (int)	{ /* v -- */ return (T)(GInterlocked::operator -- (1));};


	T	Exchange    (const T & t)
					{ return (T  )(GInterlocked::Exchange	 ((LONG) t));};

	T	ExchangeAdd (const T & t)
					{ return (T  )(GInterlocked::ExchangeAdd ((LONG) t));};

	T	CompareExchange 
			    (const T & t, const T & c)
					{ return (T  )(GInterlocked::CompareExchange 
								       ((LONG) t, (LONG) c));};
};

template <class T>
class TInterlockedPtr : public GInterlockedPtr
{
public :
			TInterlockedPtr	() { GInterlockedPtr::Init (NULL);};

			TInterlockedPtr (const T t) { Init (t);};

	T		Init (T t)		    { return (T) GInterlockedPtr::Init	  (t);};

	T		GetValue	() const    { return (T) GInterlockedPtr::GetValue ();};

	T volatile *	GetValuePtr	()	    { return (T volatile *) GInterlockedPtr::GetValuePtr ();};

	T	operator =  (const T & t)	    { return Init (t);};

		operator T  () const	{ return GetValue ();};

	T	Exchange    (const T & t)
					{ return (T  )(GInterlockedPtr::Exchange ((void *) t));};
	T	CompareExchange
			    (const T & t, const T & c)
					{ return (T  )(GInterlockedPtr::CompareExchange
								     ((void *) t, (void *) c));};
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
// Access control extensions
//
class GAccessLock
{
public :
		    GAccessLock () { Init (Lock, NULL);};

		    GAccessLock (const GAccessLock & src) { * this = src;};

      GAccessLock & operator =  (const GAccessLock & src)
			{
			    GAccessLock::Init (src.m_pfLock, src.m_pfLockContext);

			    return * this;
			};
      GAccessLock & operator =  (const void *)
			{
			    Close ();

			    return * this;
			}; 

	void *	    Close	()
			{
			    void *   pfLockContext = 
				   m_pfLockContext;
				   m_pfLock	   = Lock;
				   m_pfLockContext = NULL;
			    return   pfLockContext;
			};

	Bool	    LockAcquire	(Bool bExclusive = True)
			{   return (m_pfLock)(m_pfLockContext, (bExclusive) 
							      ? exclusive
							      : shared   );};
	void	    LockRelease ()
			{	   (m_pfLock)(m_pfLockContext,  release  );};

	Bool	    NotInited	() const
			{   return (m_pfLock == Lock) ? True : False;};

protected :

	    typedef enum
	    {
		release	  = 0,
		shared	     ,
		exclusive    ,

	    }   LockType ;

	    typedef Bool (* LockProc)(void *, LockType);

		    GAccessLock	(LockProc pfLock, void * pfLockContext)
			{
			    Init	 (pfLock,	 pfLockContext);
			};

	void	    Init	(LockProc pfLock, void * pfLockContext)
			{
			    m_pfLock	    = pfLock	    ;
			    m_pfLockContext = pfLockContext ;
			};

static	Bool	    Lock	(void *, LockType);

private :

	    LockProc		m_pfLock	;
	    void *		m_pfLockContext ;
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
// XUnknown based interface declarations
//
//
#pragma pack (_M_PACK_VPTR)

    DECLARE_INTERFACE_ (XUnknown, IUnknown)
    {
    // XUnknown methods
    //
    STDMETHOD_ (void,	      FreeInterface)(void *) PURE;
    };

    template <typename I, typename T>
    class TInterface_Entry : public I
    {
    public :

	  TInterface_Entry () : m_pXUnknown (NULL), m_pDst (NULL), m_IID (& IID_NULL), m_cRefCount (0) {};

    STDMETHOD  (     QueryInterface)(REFIID iid, void ** pi)
    {
	if (pi  == NULL)
	{
	    return ERROR_INVALID_PARAMETER;
	}
	  * pi   = NULL;

	if (iid == * m_IID)
	{
	  ((TInterface_Entry <I, T> *)(* pi = this)) ->AddRef ();

	    return ERROR_SUCCESS;
	}
	    return m_pXUnknown ->QueryInterface (iid, pi);
    };

    STDMETHOD_ (ULONG,	     AddRef)()
    {
	return  ++ m_cRefCount;
    };

    STDMETHOD_ (ULONG,	    Release)()
    {
	if (0 < -- m_cRefCount)
	{
	    return m_cRefCount.GetValue ();
	}
	     XUnknown *   pXUnknown =
			m_pXUnknown		 ;
			m_pXUnknown = NULL	 ;
			m_IID       = & IID_NULL ;
			m_pDst	    = NULL	 ;

	if (pXUnknown)
	{
	    pXUnknown ->FreeInterface (this);

	    pXUnknown ->Release ();
	}
	    return 0;
    };

	I *	Init	    (T * pDst, REFIID iid, XUnknown * pXUnknown)
		{
			m_pDst	    = pDst	 ;
			m_IID	    = & iid	 ;
			m_pXUnknown = pXUnknown  ;
		    if (m_pXUnknown)
		    {
			m_pXUnknown ->AddRef ()  ;
		    }
			m_cRefCount ++		 ;
			return this;
		};

	ULONG		 GetRefCount	() const { return m_cRefCount.GetValue	  ();};

	ULONG volatile * GetRefCountPtr ()	 { return m_cRefCount.GetValuePtr ();};

    protected :

	T *	GetT	    () const { return (T *) m_pDst;};

    protected :

        XUnknown *		m_pXUnknown ;
        void *			m_pDst	    ;
	LPCIID			m_IID	    ;
        TInterlocked <ULONG>    m_cRefCount ;
    };

    template <typename T>
    class TXUnknown_Entry : public TInterface_Entry <XUnknown, T>
    {
    public :

    STDMETHOD  (	 QueryInterface)(REFIID iid, void ** pi)
    {
	if (pi  == NULL)
	{
	    return ERROR_INVALID_PARAMETER;
	}
	  * pi   = NULL;

	if (iid == * m_IID)
	{
	  ((TXUnknown_Entry <T> *)(* pi = this)) ->AddRef ();

	    return ERROR_SUCCESS;
	}
	    return GetT () ->XUnknown_QueryInterface (iid, pi);
    };

    STDMETHOD_ (ULONG,		Release)()
    {
	if (0 < -- m_cRefCount)
	{
	    return m_cRefCount.GetValue ();
	}
	    return (GetT () ->XUnknown_FreeInterface (this), 0);
    };
    
    STDMETHOD_ (void,     FreeInterface)(void * pi)
    {
		    GetT () ->XUnknown_FreeInterface (pi);
    };

    XUnknown *	Init	 (T * pDst, REFIID iid = IID_IUnknown)
		{
		    return TInterface_Entry <XUnknown, T>::Init (pDst, iid, NULL);
		};
    };

// Example for use : -------------------------------------------------------[]
/*
    DECLARE_INTERFACE_	(IFoo, IUnknown)
    {
	STDMETHOD  (	     Method)() PURE;
    };

    template <typename T>
    class TIFoo_Entry : public TInterface_Entry <IFoo, T>
    {
    public :

	STDMETHOD  (	     Method)()
	{
	    return GetT () ->IFoo_Method ();
	};
    };

    class FooClass
    {
    public :

	    HRESULT		XUnknown_QueryInterface	(REFIID, void **);

	    void *		XUnknown_FreeInterface	(void *);

	    HRESULT		IFoo_Method ();

    protected :

	    TXUnknown_Entry  <FooClass>	    m_XUnknown ;

	    TInterface_Entry <IUnknown, FooClass> 
					    m_IUnknown ;

	    TIFoo	     <FooClass>	    m_IFoo     ;

      class TIFoo2 : public TIFoo_Entry <FooClass>
	    {
	    STDMETHOD  (	     Method)()
	    {
		return GetT () ->IFoo_Method ();
	    };
	    }				    m_IFoo2    ;

      class TIFoo3 : public TInterface_Entry 
			     <IFoo, FooClass>
	    {
	    STDMETHOD  (	     Method)()
	    {
		return GetT () ->IFoo_Method ();
	    };
	    }				    m_IFoo3   ;
    };
*/
// Example for use : -------------------------------------------------------[]
//
#pragma pack ()
//
//[]------------------------------------------------------------------------[]

#include "g_stdlink.h"
#include "g_irpdefs.h"
#include "g_strdefs.h"

//[]------------------------------------------------------------------------[]
// GWin based interface declarations
//
//
    DECLARE_INTERFACE_ (IApartment, IUnknown)
    {
    // IApartment methods
    //
    STDMETHOD_ (void,	QueueIrpForComplete)(GIrp * pIrp, DWord dTimeout = 0) PURE;
    };
//
//[]------------------------------------------------------------------------[]

//??????????????????????????????????????????
//??????????????????????????????????????????
//??????????????????????????????????????????




#if FALSE

























// Define base interfaces --------------------------------------------------[]
//
    struct GIrp;

    typedef void (GAPI * XReplyProc  )(GIrp *, void *, const void *);

    DECLARE_INTERFACE  (XReplyHandler)
    {
    STDMETHOD_ (void,		Close)()       PURE;

    STDMETHOD_ (void,	       Invoke)(GIrp *) PURE;
    };







/*






    DECLARE_INTERFACE_ (XDispatch, IUnknown)
    {
    DECLARE_INTERFACE  (CallBack)
    {
    STDMETHOD_ (void,	      Close)() PURE;

				    (
    STDMETHOD_ (XBindingHandle *, GetBindingHandle () PURE;
    };

    STDMETHOD_ (GIrp *,	     Invoke)(GIrp *, CallBack *) PURE;
    };

    DECLARE_INTERFACE  (Request)
    {
	typedef enum CoAction
	{
	    
	}		Action ;

    STDMETHOD_ (void,		 OnResponse)(GIrp *)		     PURE;

    STDMETHOD_ (void,		     Cancel)()			     PURE;
    };

    typedef	void (STDCALLTYPE *    CallBack)(GIrp *);

    STDMETHOD_ (void,		       Dispatch)(CallBack ** pCallBack  ,
					   const void *	     pRequestId ,
						 int	     iMethodId  ,
						 int	     nArgs      ,
						 void **     pArgs      ) PURE;
    };


class GSerialPort : public XDispatch
{
public :

virtual	void			Dispatch	(CallBack ** pCallBack ,

	void			Xfer		(CallBack ** pCallback ,
					   const void *	     pRequestId,

protected :



};




    DECLARE_INTERFACE  (XDispatchHandle)
    {
    STDMETHOD  (	     QueryXDispatch)(XDispatch *&) PURE;

    STDMETHOD_ (void,		      Close)(GResult)	   PURE;
    };

    DECLARE_INTERFACE_ (XDispatch, IUnknown)
    {
    STDMETHOD_ (GIrp *,		     Invoke)(GIrp *)	   PURE;
    };

    DECLARE_INTERFACE  (XDispatchSignal)
    {
    STDMETHOD  (		    Connect)(XDispatchHandle **, XDispatch *, const void * pXDispatchId) PURE;
    };


	typedef	GIrp *		 (T::*      TDispatchProc    )(GIrp *, XDispatchHandle *, const void *);

	typedef	GIrp * 		 (GAPI *    XDispatchProc     (GIrp *, XDispatchHandle *, const void *, void *)

	friend	XDispatchHandle * GAPI	    CreateXDispatcher (XDispatchProc, void *);


    DECLARE_INTERFACE_ (IRRPDevice, IUnknown)
    {
    STDMETHOD  (		    Connect)(XDispatchHandle **, XDispatch *, const void * pXDispatchId) PURE;
    };

*/


    DECLARE_INTERFACE  (XDispatchHandle)
    {
    STDMETHOD_ (void,	     QueryXDispatch)(XDispatch *&) PURE;

    STDMETHOD_ (void,		      Close)()		   PURE;
    };


    DECLARE_INTERFACE  (XNotify)
    {
    STDMETHOD_ (void,		    Destroy)() PURE;
    STDMETHOD_ (void,		    Refresh)() PURE;
    };



    DECLARE_INTERFACE  (XSignal)
    {
    DECLARE_INTERFACE  (Handle )
    {
    STDMETHOD_ (void,		     Disconnect)()	      PURE;
    };
	typedef	void (GAPI * Xxxx_On_SignalProc)(Handle * , GIrp *);

    STDMETHOD_ (void,			Connect)(Handle **, Xxxx_On_SignalProc, void *, void *) PURE;
    };



/*
    DECLARE_INTERFACE_ (XService, IUnknown)
    {
    };







    XSignal::Handle m_On_RRPStatus = NULL;
    
    ...

    if (ERROR_SUCCESS == m_pIRRPService ->QueryInterface (IID_XSignal_OnStatus, (void **) & pXSignal))
    {
	pXSignal ->Connect (& m_OnRRRStatus, pIrp))
    }
    



*/
//
//[]------------------------------------------------------------------------[]




// Define base interfaces (continue) ---------------------------------------[]
//


/*
    class Some
    {

	HANDLE			    m_hEvent	;
	GInterlockedPtr <HANDLE>    m_hWait	;
    }

...

void Some::CallBack (Some *, BOOL)
{
    ...

    HANDLE hWait = m_hWait.Exchange (NULL);

    if (hWait)
    {
	UnregisterWait (hWait);
    }
};


{
    if (RegisterWaitForSingleObject (m_hWait.GetValuePtr (),
				     m_hEvent		   ,
				     this		   ,
				     CallBack
				     ...		   ))
    {
    }
};
    




*/




//
//[]------------------------------------------------------------------------[]
//


#if FALSE

    DECLARE_INTERFACE_ (IWakeUpPort, IUnknown)
    {
    // IWakeUpPort methods
    //
/*
    STDMETHOD_ (void,		 WaitWakeUp)(DWord dTimeout = INFINITE) PURE;
*/
    STDMETHOD_ (GResult,       QueueWaitFor)(IWakeUp ** pIWakeUp       ,
					     HANDLE	hWaitFor       ,
					     IWakeUp::CallBack
							pOnProc	       ,
					     void *	pOnContext     ,
					     void *	pOnArgument    ,
					     DWord	dDueTime       ,
					     DWord	dPeriod	       ) PURE;

    STDMETHOD_ (void,	QueueIrpForComplete)(GIrp *, DWord dTimeout = 0) PURE;
    };

#endif
//
//[]------------------------------------------------------------------------[]
//







/*
    DECLARE_INTERFACE_ (IIoCoWait, IUnknown)
    {
    }


    DECLARE_INTERFACE_ (IWakeUp, IUnknonw)
    {
    DECLARE_INTERFACE  (Handle)
    {
    STDMETHOD_ (void,		Continue

    STDMETHOD_ (void,		      Close)() PURE;
    };
	typedef	enum Reason
	{
	    OnCancelled	= 0,	// Wait operation was cancelled !!!
	    OnOccured	= 1,
	    OnTimeout	= 2
	};

	typedef  Bool	      (GAPI *	     CallBack		    )
			      (		     Handle *	hWakeUp	    ,
					     Reason     eOnReason   ,
					     void *     pOnContext  ,
					     void *     pOnArgument );

    STDMETHOD_ (GResult,       QueueForWait)(Handle **	hWakeUp	    ,
					     HANDLE	hForWait    ,
					     CallBack	pOnProc	    ,
					     void *	pOnContext  ,
					     void *	pOnArgument ,
					     DWord	dDueTime    ,
					     DWord	dPeriod	    ) PURE;

    STDMETHOD_ (GResult,      QueueForWait)

    };
*/




#if FALSE

    DECLARE_INTERFACE_ (IIoCoHandler, IUnknown)
    {
    // IIoCoWait methods
    //
    STDMETHOD_ (void,	  PrepareForStartIo)(GOverlapped &) PURE;

    STDMETHOD_ (HANDLE,		GetIoCoWait)()		    PURE;

    STDMETHOD_ (IIoCoPort *,	GetIoCoPort)()		    PURE;

    STDMETHOD_ (void,	      QueueIoCoWait)(GOverlapped &) PURE;
    };

#endif


//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
//    void    GAPI    ThreadPool_QueueIrpForComplete	
//					(GThread::Type	  tType	     ,
//					 GIrp *		pIrp

//  void    GAPI    QueueIrpForComplete	(GIrp *, Bool bForceQueue = False);

//
//[]------------------------------------------------------------------------[]

#pragma pack (_M_PACK_VPTR)

//[]------------------------------------------------------------------------[]
//

    DECLARE_INTERFACE_ (XReply, IUnknown)
    {
    DECLARE_INTERFACE  (IHandler)
    {
    STDMETHOD_	(void,			  Close)()	 PURE;

    STDMETHOD_	(void,			 Invoke)(GIrp *) PURE;
    };
    typedef IHandler   * Handler;

    DECLARE_INTERFACE  (IRequest)
    {};
    typedef IRequest   * Request;

	typedef	 GIrp *   (GAPI  * DispatchProc)(GIrp *, void *, const void *);

    STDMETHOD_	(void,			OnReply)(GIrp *, Request) PURE;
    };

	void	GAPI	XReply_OnReply		(GIrp *, XReply::Request);

	void	GAPI	XReply_Close		(XReply::Handler);


    template <typename T>
    class TXDispatch_Entry : public TInterface_Entry <XDispatch, T>
    {
    public :

	STDMETHOD_ (void,   QueueIrpForComplete)(GIrp * pIrp, Bool bForceQueue)
	{
	    GetT () ->XDispatch_QueueIrpForComplete (pIrp, bForceQueue);
	};
    };

/*
    class XDispatch_XUnknown


    class XDispatch_Entry : public XInterface


    template <typename I>
    interface XDispatch_Interface : public 


    template <typename I, typename T>
    class TDispatch_Entry : public TInterface_Entry <I, T>
    {
    public :
	  TDispatch_Entry () : m_pIDispatch (NULL) {};


    private :

//	XDispatch *
    };
*/

/*
    DECLARE_INTERFACE_ (XDispatch, IUnknown)
    {
    STDMETHOD_ (Bool,		    LockAcquire)(Bool bExclusive = True)   PURE;

    STDMETHOD_ (void,		    LockRelease)()			   PURE;

    };
*/
//	HRESULT GAPI	XDispatch_QueryInterface    (GIrp *, REFIID, void **);

/*
    GIrp * GAPI XDispatch_QueueForCompleteCoProc  (GIrp * pIrp, void *, void * pArgument)
    {
	XDispatch *  pXDispatch =
       (XDispatch *) pArgument ;

	if (pXDispatch)
	{
	    pXDispatch ->QueueForComplete (pIrp);

	    pXDispatch ->Release ();

	    return NULL;
	}
	    return pIRp;
    };
*/

//	void GAPI SetXDispatch_QueueForCompleteCoProc (GIrp *, XDispatch *) PURE


//???	void GAPI IThreadPool_CleanUp		();



/*
    DECLARE_INTERFACE_ (XSerial, XHandle)
    {
    DECLARE_INTERFACE  (Receiver)
    {
    STDMETHOD_ (void,		 OnRecv)(GIrp  *     pRecvIrp	 ,
				   const void  *     pRecvId	 ,
					 DWord	     dRecvIdLen	 ,
				   const void  *     pRecvBuf	 ,
					 DWord	     dRecvBufLen );
    };
    STDMETHOD_ (void,		   Xfer)(Receiver ** pRecv	 ,
				   const void      * pSendBuf    ,
					 DWord	     dSendBufLen );
    };

    DECLARE




GResult STDAPICALLTYPE Xfer (ISerial * pISerial	    ,
			     void    * pRecvBuf	    ,
			     DWord     dRecvBufSize ,
			     DWord   * pRecvBufLen  ,
		       const void    * pSendBuf     ,
			     DWord     dSendBufLen  ,
			     DWord     dTimeout     )
{
    if (NULL == pISerial)
    {
    }
    if (NULL == pRecvBuf)
    {
    
    }
};

void SomeProc ()
{
    void *  hRequest	=  NULL;
    Byte    pSendBuf [] = {0x00, 0x00, 0x80, 0x81};

    m_pISerial ->Xfer (& hRequest, pSendBuf, sizeof (pSendBuf));

    


};








*/





/*
    DECLARE_INTERFACE_	(ISerial, IUnknown)
    {
	typedef enum
	{
	    streamraw	    = 0,
	    streamoob	    = 1,
	    stream	    = 2,
	    packet	    = 3,
	    packetpartial   = 4,

	}	DataType;

	interface IRecv ;
	interface ISend ;

    DECLARE_INTERFACE_	(IRecv	, IUnknown)
    {
    STDMETHOD_	(void,		 OnRecv)(GIrp *	     pRecvIrp     ,
				   const GDataPack * pRecvId	  ,
				   const void *	     pRecvData    ,
					 DWord	     dRecvDataLen ) PURE;
    };

    DECLARE_INTERFACE_	(ISend	, IUnknown)
    {
    STDMETHOD_	(void,	     CancelXfer)(IRecv *     pRecv	  ) PURE;

    STDMETHOD_	(void,		   Xfer)(IRecv *     pRecv	  ,
				   const GDataPack * pRecvId	  ,
				   const void *	     pSendBuf     ,
					 DWord	     dSendBufSize ,
					 DWord	     dSendFlags	  ) PURE;
    };

*/



//	void GAPI XReply_QueueForClose	(XReply *, GIrp *);

//???	Implement this call as part of the XDispatch::QueueForComplete ();
//
//	void GAPI XReply_Create		(XReply *, GIrp *, const GDataPack *);
//
//???


/*
    DECLARE_INTERFACE_ (IXDispatch, IUnknown)
    {

	interface IRequest	      ;
	typedef	  IRequest  * Request ;

	typedef	void   (STDAPICALLTYPE * DispatchProc		  )
					(GIrp *	      pIrp	  ,
					 void *	      pDspContext ,
					 int	      cDspArgs	  ,
				   const void *
				   const      *       pDspArgs	  );

	typedef GIrp * (STDAPICALLTYPE * ReplyProc		  )
					(GIrp *	      pIrp	  ,
					 void *	      pRplContext ,
				   const void *	      pRplId	  ,
					 int	      cRplArgs	  ,
				   const void *
				   const      *	      pRplArgs	  );

    DECLARE_INTERFACE  (IRequest)
    {
    STDMETHOD_	(void,		  Close)(Bool	      bCancel	  ) PURE;

    STDMETHOD_	(GIrp *,	  Reply)(GIrp *	      pIrp	  ,
					 int	      cRplArgs	  ,
				   const void *
				   const      *	      pRplArgs	  ) PURE;
    };

    STDMETHOD_	(Request, CreateRequest)(GIrp *	      pIrp	  ,
					 Request      hParent	  ,
					 ReplyProc    pRplProc	  ,
					 void *	      pRplContext ,
				   const void *	      pRplId	  ) PURE;


    STDMETHOD_	(Bool,	    LockAcquire)(Bool bExclusive = True)    PURE;

    STDMETHOD_	(void,	    LockRelease)()			    PURE;

    STDMETHOD_	(void,	    QueueIrpForComplete)(GIrp *, Bool)	    PURE;
    };
*/
//
//[]------------------------------------------------------------------------[]


#pragma pack ()


/*
#ifdef GSTDCALL_
#else
#   error   Not defined GSTDCALL_
#endif
*/



#if FALSE

//[]------------------------------------------------------------------------[]
//
    LResult    Dispatch_IUnknown (void *, int, void *);

    template <class I> class TInterface : public I
    {
    public :

	struct QueryInterface_Param
	{
	    GIrp *	pIrp	;
	    REFIID	iid	;
	    void **	pi	;

	    QueryInterface_Param    (GIrp * i , REFIID d, void ** p) :
				     pIrp  (i),
				     iid   (d),
				     pi    (p) {};
	};

    STDMETHOD_	(HRESULT,	 QueryInterface)(REFIID iid, void ** pi)
    {
	if      (NULL == pi)
	{
	return  (HRESULT) ERROR_INVALID_PARAMETER;
	}
	 	       * pi = NULL;

	QueryInterface_Param param (NULL, iid, pi);

        return	(HRESULT) Invoke (0, & param);
    };

    STDMETHOD_	(ULONG,		 AddRef)()
    {
	return (++ m_cRefCount);
    };

    STDMETHOD_	(ULONG,		Release)()
    {
	if (1 < m_cRefCount   )
	{
	return	m_cRefCount --;
	}

	return ((ULONG)  Invoke (2, NULL), 0);
    };

    public :

	    long	GetRefCount () const 
	    { 
		return m_cRefCount.GetValue ();
	    };

	    IUnknown *	GetIUnknown ()
	    {
		return (IUnknown *) Invoke (1, NULL);
	    };

    protected :
			TInterface ()
			{
			    SetDispPtr (::Dispatch_IUnknown, NULL);
			};

	    void	SetDispPtr (LResult (* pDsp)(void *, int, void *), void * oDsp)
	    {
		m_cRefCount.Init (0) ;
		m_pDsp	    = pDsp   ;
		m_oDsp	    = oDsp   ;
	    };

	    LResult	Invoke	(int i, void * p)
	    {
		return (m_pDsp) (this, i, p);
	    };

    protected :

	    TInterlocked <long>	    m_cRefCount	;
	    LResult	         (* m_pDsp)(void *, int, void *);
	    void *		    m_oDsp	;
	    FOO_PMF		    m_tDsp 	;
    };

    class GInterface : public TInterface <IUnknown>
    {};

    template <class T> class TInterfaceHandler : public GInterface
    {
    public :
	    void	Create	  (const void * pI, LResult (T::* tDsp)(int, void *), T * oDsp)
			{
			    SetDispPtr ((LResult (*)(void *,   int, void *)) 
					 TInterfaceHandler::Invoke, oDsp  );
			    m_TDsp () = tDsp ;

			    * ((const void **) this) = pI;

			    m_cRefCount ++;
			};

    protected :

	    void	Close	  ()
			{
			    m_cRefCount --;

			    m_TDsp () = NULL ;

			    SetDispPtr (NULL, NULL);
			};
    private :

	    LResult (T::*&  m_TDsp () const)(int, void *)
	    {
		return (LResult (T::*&)(int, void *)) m_tDsp;
	    };

    static  LResult	Invoke (TInterfaceHandler * pThis, int i, void * p)
	    {
		return (((T *) pThis ->m_oDsp	) ->*
			       pThis ->m_TDsp ())(i, p);
	    };
    };

    template <class T, class C>
    GFORCEINLINE C * CreateInterfaceHandler (GInterface * pInterface, LResult (T::* tDsp)(int, void *), T * oDsp)
    {
	C   c;
	return (((TInterfaceHandler <T> *) pInterface) ->Create (* ((const void **) & c), tDsp, oDsp), (C *) pInterface);
    };
//
//[]------------------------------------------------------------------------[]


#endif







//[]------------------------------------------------------------------------[]
//
#pragma pack (_M_PACK_VPTR)

    typedef struct gobject {} * GPtr  ;
/*
struct GObjectHandle
{
	    GPtr		gPtr	  ;
    enum    Type
    {
	Pointer	     = 0,
	Interface	,
    }				tPtr	  ;
    union
    {
	    void *		hKey	  ;
	    DWord		dKey	  ;
    };
	    TInterlocked <int>	cRefCount ;
	    GResult	(GAPI *	pOpenClose)(GObjectHandle &, Bool);
};
*/
/*
	    Create		(GDataPack * pInstanceId, 
				 GDataPack * pClassId	,

	    Open		(GDataPack * pInstanceId,
				 GDataPack * pClassId	,
*/				 


/*
class GObject
{
    public :
				GObject	 ()  {};

	static	    void	Destroy  (GObject * o)
				{   if (o) { o ->ShutDown (); delete o; }   };

	static	    void	Destroy  (GObject & o)
				{	     o.  ShutDown (); o.~GObject ();};
    protected :

	virtual		       ~GObject  ();

	virtual	    void	ShutDown ();

    private :

	static	long	m_Counter;
};


class GReferenceObject : protected GObject
{
public :
	    GReferenceObject () :   m_ReferenceCount (1) {};

	long		GetRef	    () { return	m_ReferenceCount.GetRef ();};
	long		AddRef	    () { return m_ReferenceCount.AddRef ();};
	long		Release	    ();

protected :

	    GReferenceCount	    m_ReferenceCount   ;
};

class GReferenceObjectHandle
{
public :
	    GReferenceObjectHandle  () : m_pReferenceObject (NULL)  {};

	    GReferenceObjectHandle  (GReferenceObject & o) 
				       : m_ReferenceCount   (  1),
				         m_pReferenceObject (& o) {o.AddRef ();};

	void		Open	    (GReferenceObject & o);

	long		GetRef	    () { return m_ReferenceCount.GetRef ();};
	long		AddRef	    () { return m_ReferenceCount.AddRef ();};
	long		Release	    ();

protected :

	    GReferenceCount	    m_ReferenceCount   ;
	    GReferenceObject *	    m_pReferenceObject ;
};
*/
//[]------------------------------------------------------------------------[]


#pragma pack ()


#endif

//??????????????????????????????????????????
//??????????????????????????????????????????
//??????????????????????????????????????????


#endif//__G_STDDEFS_H
