//[]------------------------------------------------------------------------[]
// Global types definitions implicit included from any system G_xxxdefs.cpp
//
// G_windefs.cpp
// G_nixdefs.cpp
// ...
//
// Created by GSh.
//[]------------------------------------------------------------------------[]

#ifdef __G_STDDEFS_H

//[]------------------------------------------------------------------------[]
//
void GStreamBuf::printfA (const char * fmt, ...)
{
    int	      out;
    va_list   arg;

    va_start (arg, fmt);

    if (0 < (out = ::_vsnprintf ((char *) pTail, sizeforput (), fmt, arg)))
    {
	pTail += out;
    }

    va_end   (arg);
};

void GStreamBuf::printfW (const WCHAR * fmt, ...)
{
    int	      out;
    va_list   arg;

    va_start (arg, fmt);

    if (0 < (out = ::_vsnwprintf ((WCHAR *) pTail, sizeforput (), fmt, arg)))
    {
	pTail += (out * sizeof (WCHAR));
    }

    va_end   (arg);
};

void GStreamBuf::sethead (size_t n)
{
    if ((pBase + n) != pHead)
    {
	if (sizeforget ())
	{
	    ::memmove (pBase + n, 
		       pHead	,  sizeforget ());
	}
	    pTail    = pBase + n + sizeforget ();
	    pHead    = pBase + n;
    }
};
//
//[]------------------------------------------------------------------------[]
//
static GFORCEINLINE const char * findChrA (const char * pStr, int cChr)
{
    for (; * pStr && (* pStr != cChr); pStr ++)
    {}
    return pStr;
};

static GFORCEINLINE const WCHAR * findChrW (const WCHAR * pStr, int cChr)
{
    for (; * pStr && (* pStr != cChr); pStr ++)
    {}
    return pStr;
};

static GFORCEINLINE const char * findChrA (const char * pStr, const char * pChr, const char * pTail)
{
    for (; (pStr < pTail) && ('\0' == * findChrA (pChr, * pStr)); pStr ++)
    {}
    return  pStr;
};

static GFORCEINLINE const WCHAR * findChrW (const WCHAR * pStr, const WCHAR * pChr, const WCHAR * pTail)
{
    for (; (pStr < pTail) && ('\0' == * findChrW (pChr, * pStr)); pStr ++)
    {}
    return  pStr;
};

static int skipChrA (const char *& pHead, const char * missStr, const char * pTail)
{
    for (; (pHead < pTail) && ('\0' != * findChrA (missStr, * pHead)); pHead ++)
    {}
    return (int)(pTail - pHead);
};

static int skipChrW (const WCHAR *& pHead, const WCHAR * missStr, const WCHAR * pTail)
{
    for (; (pHead < pTail) && ('\0' != * findChrW (missStr, * pHead)); pHead ++)
    {}
    return (int)(pTail - pHead);
};
//
//[]------------------------------------------------------------------------[]
//
int GStreamBuf::ScanfA (const char * missStr, const char * fmt, ...)
{
    int	      inp, n;
    va_list   arg;

    char *    fmtTail = NULL  ;
    char      fmtHead [64 + 4];

    if ((NULL == fmt) || ('\0' == * fmt))
    {
	return skipChrA ((const char *&) pHead, missStr, (const char *) pTail);
    }

    va_start (arg, fmt);

    for (inp = 0;  * fmt; fmt ++)
    {
	if	(fmtTail)
	{
	    if	(fmtTail < (fmtHead + 64))
	    {
		if ('\0' != findChrA ("cCdiouxeEfgGnsS", * fmt))
		{
		    * fmtTail ++ = * fmt;
		    * fmtTail ++ = '%'	;
		    * fmtTail ++ = 'n'	;
		    * fmtTail ++ = '\0'	;
		      fmtTail    = NULL ;
#if FALSE
		    if (1 == ::_snscanf ((char *) pHead, sizeforget (), fmtHead, va_arg (arg, void *), & n))
#else
		    if (1 ==   ::sscanf ((char *) pHead		      , fmtHead, va_arg (arg, void *), & n))
#endif
		    {
			get ((n <= (int) sizeforget ()) ? n : (int) sizeforget ());
			inp   ++;

			continue;
		    }
			break;
		}
			continue;
	    }
		break;
	}

	if ('%' == * fmt)
	{
	    fmtTail    = fmtHead;
	  * fmtTail ++ = * fmt  ;
	        continue;
	}

	if (0 < skipChrA ((const char *&) pHead, missStr, (const char *) pTail))
	{
	    if (* fmt == headChrA ())
	    {
		getChrA ();
		continue;
	    }
	}
		break;
    };

    va_end (arg);

    return  inp;
};

/*
int GStreamBuf::ScanfW (const WCHAR * missStr, const WCHAR * fmt, ...)
{
    return 0;
};
*/
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
void * GObject::operator new (size_t sz, void * ptr)
{
    if (ptr)
    {
	memset (ptr, 0, sz);
    }
	return  ptr;
};

void * GObject::operator new (size_t sz)
{
    return operator new (sz, ::operator new (sz));
};

void GObject::operator delete (void * ptr)
{
    ::operator delete (ptr);
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
// Lock stub
//
//
Bool GAccessLock::Lock (void * pfLockContext, LockType t)
{
    return True;
};
//
//[]------------------------------------------------------------------------[]

/*
LResult Dispatch_IUnknown (void *, int, void *)
{
    return (LResult) ERROR_NOT_SUPPORTED;
};
*/

//[]------------------------------------------------------------------------[]
//
static GIrp * GAPI defIrpCoProc (GIrp *, void *, void *);

static GIrp * GAPI defIrpDpProc (GIrp *, GIrp_DpContext, void *, void *);

GIrp * GIrp::Init (void * pIrpBlock, size_t tIrpBlockSize, CoProc  pFreeCoProc	      ,
							   void *  pFreeCoProcContext )
{
    GIrp *  pIrp =		 (GIrp *) pIrpBlock;

	    pIrp ->QLink	 .unlink ();

	    pIrp ->pTailESP    = (Byte *) pIrpBlock + tIrpBlockSize ;
	    pIrp ->pHeadESP    = (Byte *) pIrpBlock + sizeof  (GIrp);

	    pIrp ->pCurCoEntry	      = NULL;
	    pIrp ->pCurCoContext      = NULL;
	    pIrp ->pCurExEntry	      = NULL;
	    pIrp ->pCurDpEntry	      = NULL;

	    pIrp ->pCurStatus	      = NULL;
	    pIrp ->pCurRequest	      = NULL;

	    pIrp ->pCancelProcLock    = NULL;
	    pIrp ->pCancelProc	      = NULL;
	    pIrp ->pCancelProcContext = NULL;

	    pIrp ->tTimeout.  Disactivate ();

	    pIrp ->SetCurCoProc	 (NULL, NULL, NULL);

	    pIrp ->SetCurStatus	 (
	    pIrp ->CreateStatus	 (sizeof (Status)) ->Init ());

	if (pFreeCoProc == NULL)
	{
	    pFreeCoProc = defIrpCoProc;
	}
	    pIrp ->SetCurCoProc (pFreeCoProc , pFreeCoProcContext, NULL);
	    pIrp ->SetCurDpProc (defIrpDpProc, NULL		 , NULL);

    return  pIrp;
};

static GIrp * GAPI freeIrpCoProc (GIrp * pIrp, void *, void *)
{
    free (pIrp);

    return NULL;
};

void * GIrp::operator new (size_t, size_t tIrpBlockSize)
{
    void *  pIrpBlock = malloc (tIrpBlockSize);

    if	   (pIrpBlock)
    {
	return (GIrp::Init (pIrpBlock, tIrpBlockSize, freeIrpCoProc), pIrpBlock);
    }
	return NULL;
};

GIrp * GAPI CreateIrp ()
{
    return new GIrp;
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
GIrp * GAPI GIrp::RestoreCurStatus (GIrp * pIrp , void * pDestructor, void * pStatus)
{
    if (pDestructor)
    {
       ((void (GAPI *)(void *)) pDestructor)(pIrp ->pCurStatus);
    }
	pIrp ->pCurStatus  = (Status *)  pStatus;

	return pIrp;
};

GIrp * GAPI GIrp::RestoreCurRequest (GIrp * pIrp, void * pDestructor, void * pRequest)
{
    if (pDestructor)
    {
       ((void (GAPI *)(void *)) pDestructor)(pIrp ->pCurRequest);
    }
	pIrp ->pCurRequest = (Request *) pRequest;

	return pIrp;
};

GIrp * GAPI GIrp::RestoreCurExEntry (GIrp * pIrp, void *, void * pExEntry)
{
	pIrp ->pCurExEntry = (ExProcEntry *) pExEntry;

	return pIrp;
};

GIrp * GAPI GIrp::RestoreCurDpEntry (GIrp * pIrp, void *, void * pDpEntry)
{
	pIrp ->pCurDpEntry = (DpProcEntry *) pDpEntry;

	return pIrp;
};

GIrp * GAPI GIrp::ReleaseIUnknown (GIrp * pIrp, void *, void * pRef)
{
	((IUnknown *) pRef) ->Release ();

	return pIrp;
};
//
//[]------------------------------------------------------------------------[]
//
Bool GAPI RaiseEx (GIrp * pIrp, GIrp_ExRecord * r)
{
    for (GIrp_ExProcEntry *  e = pIrp ->pCurExEntry; e; e = e ->HighExEntry ())
    {
	if ((e ->pExProc)(r, e ->pExProcContext ,
			     e ->pExProcArgument))
	{
	    return True ;
	}
    }
	    return False;
};

GIrp * GAPI defIrpCoProc (GIrp *, void *, void *)
{
    return NULL;
};

void GAPI CompleteIrp (GIrp * pIrp)
{
    GIrp_CoProcEntry * e;

    while (pIrp)
    {
	pIrp ->pHeadESP = 
	e		= pIrp ->pCurCoEntry ;
			  pIrp ->pCurCoEntry = e ->HighCoEntry ();

	pIrp = (e ->pCoProc) 
	     ? (e ->pCoProc)(pIrp, e ->pCoProcContext  ,
				   e ->pCoProcArgument ) : pIrp  ;
    };
};

static GIrp * GAPI defIrpDpProc (GIrp * pIrp, GIrp_DpContext, void *, void *)
{
    return pIrp;
};

GIrp * GAPI DispatchIrpHigh (GIrp * pIrp, GIrp_DpContext c)
{
	GIrp_DpProcEntry * e;

    if (pIrp)
    {
	    c = (c) ? c : (GIrp_DpContext) pIrp ->pCurDpEntry;
	do
	{
	    e = (GIrp_DpProcEntry *) c;
				     c = (GIrp_DpContext) e ->HighDpEntry ();
	} while (e ->pDpProc == NULL);

	pIrp  =	(e ->pDpProc)(pIrp,  c, e ->pDpProcContext, e ->pDpProcArgument);
    }

    return pIrp;
};

void GAPI DispatchIrp (GIrp * pIrp)
{
    if (NULL !=	   (pIrp = DispatchIrpHigh (pIrp, NULL)))
    {
       CompleteIrp (pIrp);
    }
};
/*
Bool GAPI SetCancelProc (GIrp * pIrp, GAccessLock *   pCancelLock,
				      GIrp_CancelProc pProc	 ,
				      void *	      pContext   )
{
//  pCancelLock ->LockAcquire ();	    // Must be called !!!
//
//
	Bool    bResult = False;	    // Request has ben cancelled

    pCancelLock ->LockAcquire ();

	if (pProc)			    // Set new cancelation proc
	{				    //
	    if (pIrp ->pCancelProcLock.CompareExchange 
		     (pCancelLock, NULL) == NULL     )
    	    {
		pIrp ->pCancelProc	  = pProc    ;
		pIrp ->pCancelProcContext = pContext ;

		bResult = True ;
	    }
	}
	else				    // Clear   cancelation proc
	{				    //
	    if (pIrp ->pCancelProcLock.CompareExchange 
	    	     (NULL, pCancelLock) == pCancelLock)
	    {
		pIrp ->pCancelProc	  = NULL ;
		pIrp ->pCancelProcContext = NULL ;

		bResult = True ;
	    }
	}

    pCancelLock ->LockRelease ();

	return  bResult;
};
*/
/*
    m_PLock.LockAcquire ();

	pIrp = ClearCancelable ( m_Queue.extract (pIrp)))

    m_PLock.LockRelease ();

*/


Bool GAPI SetCancelProc (GIrp * pIrp, GAccessLock *   pCancelLock,
				      GIrp_CancelProc pProc	 ,
				      void *	      pContext   )
{
//  pCancelLock ->LockAcquire ();   // Must be called !!!
//
    if (pIrp ->pCancelProcLock.CompareExchange 
	      (pCancelLock, NULL) == NULL     )
    {
	pIrp ->pCancelProc	   = pProc    ;
	pIrp ->pCancelProcContext  = pContext ;

	return True ;
    }
	return False;
};

GIrp * GAPI ClrCancelProc (GIrp * pIrp, GAccessLock * pCancelLock)
{
//  pCancelLock ->LockAcquire ();   // Must be called !!!
//
    if (pIrp ->pCancelProcLock.CompareExchange
	      (NULL, pCancelLock) == pCancelLock)
    {
	pIrp ->pCancelProc	   = NULL     ;
	pIrp ->pCancelProcContext  = NULL     ;

	return pIrp;
    }
	return NULL;
};

void GAPI CancelIrp (GIrp * pIrp)
{
    GAccessLock *      pCancelLock = pIrp ->pCancelProcLock.Exchange 
				     ((GAccessLock *) INVALID_PTR);

    if (INVALID_PTR == pCancelLock)
    {	return;}

    if (NULL	    == pCancelLock)
    {	return;}

    pCancelLock ->LockAcquire ();

	GIrp_CancelProc  pProc = pIrp ->pCancelProc		 ;
				 pIrp ->pCancelProc	   = NULL;
	void *	      pContext = pIrp ->pCancelProcContext	 ;
				 pIrp ->pCancelProcContext = NULL;

       (pProc)(pIrp, pCancelLock, pContext);
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
GIrp * GAPI GIrp_DispatchQueue::Xxxx_Dispatch (GIrp * pIrp, void *, void *)
{
    return DispatchIrpHigh (pIrp, NULL);
};

GIrp * GAPI GIrp_DispatchQueue::Xxxx_Co_Dispatch (GIrp * pIrp, void * pQueue, void *)
{
      GIrp *	pNextIrp ;

    ((GIrp_DispatchQueue *) pQueue) ->LockAcquire ();

				      pNextIrp  =
    ((GIrp_DispatchQueue *) pQueue) ->m_pCurIrp =

    ((GIrp_DispatchQueue *) pQueue) ->extract_first ();

    ((GIrp_DispatchQueue *) pQueue) ->LockRelease ();

    if (pNextIrp)
    {
	SetCurCoProc	    (pNextIrp, Xxxx_Dispatch, pQueue, NULL);

	QueueIrpForComplete (pNextIrp);
    }
	return pIrp;
};

void GIrp_DispatchQueue::QueueForDispatch (GIrp * pIrp)
{
	SetCurCoProc (pIrp, Xxxx_Co_Dispatch, this, NULL);

    LockAcquire ();

	if (NULL != 
	   (pIrp  = (m_pCurIrp) ? (append (pIrp), NULL) : pIrp))
	{
    LockRelease ();

	    Xxxx_Dispatch (m_pCurIrp = pIrp , this, NULL);
	}
};
//
//[]------------------------------------------------------------------------[]


//[]------------------------------------------------------------------------[]
//

//
//[]------------------------------------------------------------------------[]
















#if FALSE
//[]------------------------------------------------------------------------[]
//
GIrp * GAPI GIrp_ReplyHandler::CoRequest (GIrp * pIrp, void *, void *)
{
    return pIrp;
};

void STDMETHODCALLTYPE GIrp_ReplyHandler::Close ()
{
    GIrp * pIrp = (GIrp *) _Sys_InterlockedExchangePtr (& m_pIrp, NULL);

    if (pIrp)
    {
	CancelIrp (pIrp);
    }
};

static GIrp * GAPI OnReplyProc (GIrp * pIrp, void * h, void *)
{
    (((GIrp_ReplyHandler *) h) ->m_pRplProc   )
     (				   pIrp	      ,
     ((GIrp_ReplyHandler *) h) ->m_pRplContext,
     ((GIrp_ReplyHandler *) h) ->m_pRequestId );

// ???    

    return NULL;
}; 

void STDMETHODCALLTYPE GIrp_ReplyHandler::Invoke (GIrp * pIrp)
{
    SetCurCoProc     (pIrp, OnReplyProc, this, NULL);

    m_pXDispatch ->QueueIrpForComplete (pIrp, False);
};
//
//[]------------------------------------------------------------------------[]
#endif

//[]------------------------------------------------------------------------[]
//
/*
void GIrp_Dispatcher::QueueIrpForComplete (GIrp * pIrp)
{
    LockAcquire ();

//	    append  (pIrp);

	if (pIrp  != last ())
	{
    LockRelease ();

	    return;
	}
	    m_qDisp.dispatch (* this, NULL);
};
*/

/*
void GIrp_Dispatcher::dispatch (GIrp_Queue & q, GIrp * pIrp)
{
//  LockAcquire (); was called !!!
//
    if (m_pBusy)
    {
	if (pIrp)
	{
	}
    }
};
*/
//
//[]------------------------------------------------------------------------[]


#if FALSE

//[]------------------------------------------------------------------------[]
//
void GAPI GIrp_Queue::Cancel_Queue (GIrp * pIrp, GAccessLock * pAccessLock, void * pQueue)
{
	pIrp = ((GIrp_Queue *) pQueue) ->extract (pIrp);

    pAccessLock ->LockRelease ();

    if (pIrp)
    {
	CompleteIrp (pIrp);
    }
};

GIrp * GIrp_Queue::Dispatch_CoProc (GIrp * pIrp, void *)
{
    LockAcquire ();

//	m_pCurIrp = pIrp;

    LockRelease ();

    return NULL;
};

void GIrp_Queue::Dispatch (GIrp * pIrp)
{
    SetCurCoProc <GIrp_Queue, void *>
		 (pIrp, & GIrp_Queue::Dispatch_CoProc, this, NULL);

    CompleteIrp (pIrp);
};

void GIrp_Queue::QueueIrpForDispatch (GIrp * pIrp, Bool bCancelable)
{
    LockAcquire ();

	if (m_pBusy && (m_pBusy != pIrp))
	{
	    append  (pIrp);

    LockRelease ();

	    return;
	}
	    m_qDisp.dispatch (* this, m_pBusy = pIrp);
};
//
//[]------------------------------------------------------------------------[]

#endif



#if FALSE


class SomeClass
{
public :

	SomeClass () : m_DLock (), 
		       m_DQueue (m_DLock, & SomeClass::DispatchDQueue, NULL),
		       m_WQueue (m_DLock, & SomeClass::DispatchWQueue, NULL)
	{};

protected :

	void		DispatchDQueue	    (GIrp_DispatchQueue &, GIrp * pCurIrp, 
								   GIrp * pIrp   );

	void		DispatchWQueue	    (GIrp_DispatchQueue &, GIrp * pCurIrp, 
								   GIrp * pIrp   );

	Bool	  DLock_BuildSendRequest    (GIrp * pIrp);

private :

	GCriticalSection        m_DLock ;

	GIrp_List		m_RQueue;

	TIrp_DispatchQueue 
	<SomeClass>		m_DQueue;

	TIrp_DispatchQueue 
	<SomeClass>		m_WQueue;
};

void SomeClass::DispatchDQueue (GIrp_DispatchQueue & q, GIrp *, GIrp * pIrp)
{
//  Test some condition :
//
};

Bool SomeClass::DLock_BuildSendRequest (GIrp * pIrp)
{
    GIrp_List	q;
/*
    while (NULL != for (GIrp * p = NULL; _Queuewhile (NULL != (p = m_DQueue.extract_first ())
    {
    }
*/
    return False;
};

void SomeClass::DispatchWQueue (GIrp_DispatchQueue & q, GIrp *, GIrp * pIrp)
{
	if (DLock_BuildSendRequest (pIrp))
	{
    m_DLock.LockRelease ();

	}

//	if (! m_DQueue.Busy ())
	{
//	      buildSendRequest (pIrp);

    m_DLock.LockRelease ();

	}
};

SomeClass * GetSomeClass ()
{
    return new SomeClass (); 
}; 



/*
static GIrp * GAPI RestoreCoQueue (GIrp * pIrp, void *,
				   GIrp::CoQueueEntry * pCoQueueEntry)
{
    if (pIrp ->pCurCoQueue)
    {
//	pIrp ->pCurCoQueue ->CompleteNextPacket ();
//	pIrp ->pCurCoQueue ->Release ();
    }

    pIrp ->pCurCoQueue = pCoQueueEntry ->pCoQueue;
    pIrp ->tCurCoType  = pCoQueueEntry ->tCoType ;

    if (0 == pIrp ->tCurCoType)
    {
	return pIrp;
    }
//	QueueIrpForComplete (pIrp);

	return NULL;
};


static GIrp * GAPI RestoreCoQueueEntry (GIrp * pIrp, void *,
					GIrp::CoQueueEntry * pCoQueueEntry)
{
    return pIrp;
};

void GIrp::SetCurCoType (CoType tCoType, GIrp_CoQueue * pCoQueue)
{
    if (NULL == pCoQueue)
    {
	if (pCurCoQueue)
	{
//	    pCurCoQueue ->CompleteNextPacket ();

	    pCurCoQueue = pOldCoEntry ->pCoQueue ;
	    tCurCoType	=		tCoType  ;
	}
    }
	    pOldCoEntry ->pCoQueue = pCurCoQueue ;
	    pOldCoEntry ->tCoType  = tCurCoType	 ;
	   ((CoProcEntry *)
	   (pOldCoEntry + 1)) ->Init ((GIrp_CoProc) RestoreCoQueue,
				      NULL, pOldCoEntry		  );
	    pOldCoEntry = 
	  ((CoQueueEntry *) alloc (sizeof (CoQueueEntry))) ->Init (pOldCoEntry);
	    SetCurCoProc   (NULL, NULL, NULL);

	    pCurCoQueue = pCoQueue;
	    tCurCoType  = tCoType ;
};
*/





/*
GIrp * GAPI GIrp::RestoreRequests (GIrp * pIrp, void * pCurRequest, void * pCurDispRequest)
{
    pIrp ->pCurRequest	   = (Request *)
	   pCurRequest	  ;
    pIrp ->pCurDispRequest = (DispRequest *)
	   pCurDispRequest;

    return pIrp;
};

GIrp * GAPI GIrp::RestoreRequest (GIrp * pIrp, void * pCurRequest ,
					       void * pCurStatus  )
{
    pIrp ->pCurRequest = (Request *)
	   pCurRequest	;
    pIrp ->pCurStatus  = (Status  *)
	   pCurStatus	;

    return pIrp;
};

GIrp * GAPI GIrp::DispatchRequest (GIrp * pIrp, void *, void * pDispProc)
{
    if (pDispProc)
    {
       (pIrp ->pCurStackEntry + 1) ->
	       pLowStackEntry = pIrp ->pCurStackEntry;
	pIrp ->pCurStackEntry ++;	

       ((GIrp_DispProc) pDispProc)(pIrp, NULL);

	return NULL;
    }
	return pIrp;
};

GIrp * GAPI GIrp::DispatchRequest (GIrp * pIrp, void * pContext, void * pArgument)
{
    DispProc  pDispProc = 
   (DispProc) pArgument;
	      pArgument = NULL;

    if (pDispProc)
    {
       (pDispProc)(pIrp, pContext);
    }
	return pIrp;
};

GIrp * GAPI GIrp::ClearCancelPtr (GIrp * pIrp, void *, void * pCancelPtr)
{
    if (pIrp == ((CancelPtr *) pCancelPtr) ->tIrp.CompareExchange (NULL, pIrp))
    {
    return pIrp;
    }
    return pIrp;
};
//
//[]------------------------------------------------------------------------[]
//

void GAPI CompleteDispatchIrp (GIrp * pIrp)
{
    if (pIrp)
    {
    if (pIrp ->pCurDispRequest)
    {
      * pIrp ->pCurDispRequest ->pDispProc = NULL;
    }
	CompleteIrp (pIrp);
    }
};

//
//[]------------------------------------------------------------------------[]
*/







































//[]------------------------------------------------------------------------[]
// Standard object
/*
GObject::~GObject ()
{};

void GObject::ShutDown ()
{};

long GReferenceObject::Release () 
{
#ifdef _DEBUG
    if (0 >= GetRef ())
    {	::RaiseException (0x80000000, 0, 0, NULL);}
#endif

    long    c = m_ReferenceCount.Release ();

    return (c) ? c : (GObject::Destroy (this), c);
};

void GReferenceObjectHandle::Open (GReferenceObject & o)
{
    if (1 == m_ReferenceCount.AddRef ())
    {
	m_pReferenceObject = & o;

	m_pReferenceObject ->AddRef ();
    }
    else
    {
	m_ReferenceCount.Release ();
    }
};

long GReferenceObjectHandle::Release ()
{
    if (0 == m_ReferenceCount.Release ())
    {
	m_pReferenceObject ->Release ();

	m_pReferenceObject = NULL;
    }
    return m_ReferenceCount.GetRef ();
};
*/
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
class GPageAllocator
{
public :
		    GPageAllocator  () { m_pBase = NULL;};

		   ~GPageAllocator  () { m_pBase = NULL;};

	void	    create	    (void * pBase, _U32 nPages,
						   _U32 iFirst)
		    {
			m_pBase	    = (Header *) pBase;

			FreeBlock * 
			fb	    = FreeBlock::From (pBase, (iFirst * _M_PAGE_SIZE));
			fb ->oNext  = 0		      ;
			fb ->nPages = nPages - iFirst ;
		    };

	void	    open	    (void * pBase)
		    {
			m_pBase = (Header *) pBase;
		    };

	void *	    alloc	    (		   _U32 nPages);

	void	    free	    (void * pPage, _U32 nPages);

protected :

#pragma pack (_M_PACK_LONG)

    struct FreeBlock
    {
	_U32	    oNext	;	// + 0 offset for next free block
	_U32	    nPages	;	// + 4 number of the contiues pages
					// + 8

	static FreeBlock *
			From	(void * pBase, _U32	   o)
				{ return (FreeBlock *) ptrbyoffset (pBase, o);};

	static _U32	From	(void * pBase, FreeBlock * p)
				{ return      (_U32  ) offsetbyptr (pBase, p);};
    };

    struct Header
    {
	_U32	    oFreeBlock  ;	// + 0 offset for first free block
					// + 4 Header's data area
    };					// + 8 ...

#pragma pack ()

protected :

	Header *	m_pBase	;	// (Creator's Locked)
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
void * GPageAllocator::alloc (_U32 n)
{
    if ((NULL == m_pBase) || (0 == m_pBase ->oFreeBlock))
    {
	return NULL;
    }

    FreeBlock * bb;
    FreeBlock * fb;
    DWord	sz = n * _M_PAGE_SIZE;

    for (bb  = NULL					      ,
	 fb  = FreeBlock::From (m_pBase, m_pBase ->oFreeBlock);
	 fb && (n > fb ->nPages)			      ;
	 bb  =  fb					      ,
	 fb  = (    fb ->oNext ) 
	     ? FreeBlock::From (m_pBase, fb ->oNext	      )
	     : NULL					      )
    {};

    if (fb)
    {
	if (0 < (fb ->nPages -= n))
	{
	    ((FreeBlock *) (((Byte *) fb) + sz)) ->oNext  = fb ->oNext ;
	    ((FreeBlock *) (((Byte *) fb) + sz)) ->nPages = fb ->nPages;

	    if (bb)
	    {
		bb ->oNext	     +=	sz;
	    }
	    else
	    {
		m_pBase ->oFreeBlock += sz;
	    }
	}
	else
	{
	    if (bb)
	    {
		bb ->oNext	      = fb ->oNext;
	    }
	    else
	    {
		m_pBase ->oFreeBlock  = fb ->oNext;
	    }
	}
    }
	return fb;
};

void GPageAllocator::free (void * p, _U32 n)
{
    if (NULL == p)
    {
    	    return;
    }
//	    p = (void *)((DWord) p & (~(((DWord) _M_PAGE_SIZE) - 1)));

    if (0 == m_pBase ->oFreeBlock)
    {
	    ((FreeBlock *) p) ->oNext  = 0;
	    ((FreeBlock *) p) ->nPages = n;

	    m_pBase ->oFreeBlock 
			 = FreeBlock::From (m_pBase, (FreeBlock *) p);
	    return;
    }

    FreeBlock * bb;
    FreeBlock * fb;
    DWord	sz = n * _M_PAGE_SIZE;

    for (bb = NULL					     ,
	 fb = FreeBlock::From (m_pBase, m_pBase ->oFreeBlock); p < fb;
	 bb = fb,
	 fb = FreeBlock::From (m_pBase, fb	->oNext	    ));

    if (bb)
    {
	if (((Byte *) p	     ) == ((Byte *) bb + (bb ->nPages * _M_PAGE_SIZE)))
	{
	    bb ->nPages += n;

	if (((Byte *) p +  sz) == ((Byte *) fb))
	{
	    bb ->oNext	 = fb ->oNext ;
	    bb ->nPages += fb ->nPages;
	}
	    return;
	}
    }

    if (fb)
    {
	if (((Byte *) p +  sz) == ((Byte *) fb))
	{
	    ((FreeBlock *)  p) ->oNext  = fb ->oNext;
	    ((FreeBlock *)  p) ->nPages = fb ->nPages + n;

	if (bb)
	{
	    bb ->oNext   = FreeBlock::From (m_pBase, (FreeBlock *) p);
	    return;
	}
	    m_pBase ->oFreeBlock 
			 = FreeBlock::From (m_pBase, (FreeBlock *) p);
	    return;
	}
	    ((FreeBlock *) p) ->oNext  = FreeBlock::From (m_pBase, fb);
	    ((FreeBlock *) p) ->nPages = n;

	    m_pBase ->oFreeBlock 
			 = FreeBlock::From (m_pBase, (FreeBlock *) p);
	    return;
    }
	    ((FreeBlock *) p) ->oNext  = 0;
	    ((FreeBlock *) p) ->nPages = n;

	    bb ->oNext	 = FreeBlock::From (m_pBase, (FreeBlock *) p);
};
//
//[]------------------------------------------------------------------------[]










/*???
void GIrp::ReleaseCurDispQueue ()
{
    GIrp_DispatchQueue *  pQueue =
   (GIrp_DispatchQueue *) pCurCoQueue ->pCoProcArgument;

    if (pQueue)
    {
	pCurCoQueue ->pCoProcArgument = NULL;
	pQueue	     ->DispatchNextPacket ();
    }
};

GIrp * GAPI GIrp::RestoreCurDispQueue (GIrp * pIrp, void * pOldCoQueue, void * pQueue)
{
	pIrp ->pCurCoQueue = (CoProcEntry *) pOldCoQueue;

    if (pQueue)
    {
	((GIrp_DispatchQueue *) pQueue) ->DispatchNextPacket ();
    }
	return pIrp;
};
???*/

/*
GIrp * GAPI GIrp::QueueIrpForDispatch (GIrp * pIrp, void *, void * pQueue)
{
//  if (pQueue)
//  {
	pIrp ->SetCurDispQueue 
		((GIrp_DispatchQueue *) pQueue);

		((GIrp_DispatchQueue *) pQueue) ->QueueIrpForDispatch (pIrp);

	return NULL;
//  }
//	Not entry to this point !!!
//
//	return pIrp;
};
*/
//


#endif

#endif//__G_STDDEFS_H
