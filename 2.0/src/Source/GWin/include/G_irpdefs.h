//[]------------------------------------------------------------------------[]
// Global types and macros declaration
//
//
// Created by GSh.
//[]------------------------------------------------------------------------[]

#ifndef __G_IRPDEFS_H
#define __G_IRPDEFS_H

//[]------------------------------------------------------------------------[]
//
#pragma pack (_M_PACK_VPTR)

    		struct  GIrp;

	typedef struct  GIrp_DpContext_tag {} * GIrp_DpContext;

	typedef GIrp * (GAPI *	GIrp_DpProc)	    (GIrp *, GIrp_DpContext, void *, void *);

		GIrp *	GAPI	DispatchIrpHigh	    (GIrp *, GIrp_DpContext);

		void	GAPI	DispatchIrp	    (GIrp *);

	struct GIrp_ExRecord
	{
	    DWord		dCtrlSize   ;

	    GResult		dResult	    ;

	union
	{
	    DWord		dResultInfo ;
	    void *		pResultInfo ;
	};

	    GIrp_ExRecord *	init	    (DWord dSize)
				{
				    this ->dCtrlSize   = dSize;

				    return this;
				};

	    GIrp_ExRecord *	Init	    (DWord   dCtrlCode, GResult dResult     =    0,
								void *  pResultInfo = NULL)
				{
				    this ->dCtrlSize  |= (dCtrlCode << 16);

				    this ->dResult     = dResult	  ;
				    this ->pResultInfo = pResultInfo	  ;

				    return this;
				};

	    DWord		GetSize	    () const { return LOWORD (dCtrlSize);};

	    DWord		GetCtrlCode () const { return HIWORD (dCtrlSize);};
	};

	typedef	Bool   (GAPI *	GIrp_ExProc)	    (GIrp_ExRecord *, void *, void *);

		Bool	GAPI	RaiseEx		    (GIrp *, GIrp_ExRecord *);

	typedef GIrp * (GAPI *	GIrp_CoProc)	    (GIrp *, void *, void *);

		void	GAPI	CompleteIrp	    (GIrp *);

	typedef void   (GAPI *	GIrp_CancelProc)    (GIrp *, GAccessLock *, void *);

						    // pCancelLock ->LockAcquire () inside :
						    //
		Bool	GAPI	SetCancelProc	    (GIrp *, GAccessLock *, GIrp_CancelProc, void *);

						    // pCancelLock ->LockAcquire () inside :
						    //
		GIrp *	GAPI	ClrCancelProc	    (GIrp *, GAccessLock *);

		void	GAPI	CancelIrp	    (GIrp *);

//		void	GAPI	QueueIrpForDispatch (GIrp *, Bool bForceQueue);

		void	GAPI	QueueIrpForComplete (GIrp *, DWord dTimeout = 0);

	struct GIrp_StackEntry
	{
	    void *		pHighEntry  ;

	    GIrp_StackEntry *   init	(GIrp_StackEntry * pHighEntry)
				{
				    this ->pHighEntry	 = pHighEntry;

				    return this;
				};

	    GIrp_StackEntry *	HighEntry	() const { return
						(GIrp_StackEntry *) pHighEntry;};
	};

	struct GIrp_Status : public GIrp_StackEntry
	{
	    GResult		dStatus	    ;

	union
	{
	    DWord		dStatusInfo ;
	    void *		pStatusInfo ;
	};

		GIrp_Status *	init	    (GIrp_Status * pHighStatus)
				{
				    this ->pHighEntry	 = pHighStatus;

				    return this;
				};

		GIrp_Status *	HighStatus	() const { return 
						(GIrp_Status *)   HighEntry ();};

		DWord		Status		() const { return dStatus     ;};
		DWord		StatusInfo	() const { return dStatusInfo ;};
		void *		StatusInfoPtr   () const { return pStatusInfo ;};

		GResult		Result		() const { return dStatus     ;};
		DWord		ResultInfo	() const { return dStatusInfo ;};
		void *		ResultInfoPtr	() const { return pStatusInfo ;};

		GIrp_Status *	Init	    (DWord  dStatus	= 0	  ,
					     void * pStatusInfo = NULL	  )
				{
				    SetStatusInfoPtr (dStatus, pStatusInfo);

				    return this;
				};

		void		SetStatus	    (DWord   dStatus)
				{
				    this ->dStatus     = dStatus     ;
				};

		void		SetStatusInfo	    (DWord   dStatus	,
						     DWord   dStatusInfo)
				{
				    SetStatusInfoPtr (dStatus, (void *) dStatusInfo);
				};

		void		SetStatusInfoPtr    (DWord   dStatus	,
						     void *  pStatusInfo)
				{
				    this ->dStatus     = dStatus     ;
				    this ->pStatusInfo = pStatusInfo ;
				};

		void		SetResult	    (GResult dResult)
				{
				    this ->dStatus     = dResult     ;
				};

		void		SetResultInfo	    (GResult dResult	,
						     DWord   dResultInfo)
				{
				    SetResultInfoPtr (dResult, (void *) dResultInfo);
				};

		void		SetResultInfoPtr    (GResult dResult	,
						     void *  pResultInfo)
				{
				    this ->dStatus     = dResult     ;
				    this ->pStatusInfo = pResultInfo ;
				};
	};

	struct GIrp_Request : public GIrp_StackEntry
	{
	    DWord		dCtrlSize   ;

	    GIrp_Request *	init	    (GIrp_Request * pHighRequest, size_t dSize)
				{
				    this ->pHighEntry  = pHighRequest ;
				    this ->dCtrlSize   = (DWord) dSize;

				    return this;
				};

	    GIrp_Request *	HighRequest () const { return
					    (GIrp_Request *) HighEntry ();};

	    GIrp_Request *	Init	    (DWord dCtrlCode)
				{
				    this ->dCtrlSize  |= (dCtrlCode << 16);

				    return this;
				};

	    DWord		GetSize	    () const { return LOWORD (dCtrlSize);};

	    DWord		GetCtrlCode () const { return HIWORD (dCtrlSize);};
	};

	struct GIrp_CoProcEntry : public GIrp_StackEntry
	{
	    GIrp_CoProc		pCoProc		;
	    void *		pCoProcContext  ;
	    void *		pCoProcArgument ;

	    GIrp_CoProcEntry *	HighCoEntry	() const { return
						(GIrp_CoProcEntry *) HighEntry ();};

	    GIrp_CoProcEntry *	Init		(GIrp_CoProc pCoProc	     ,
						 void *	     pCoProcContext  ,
						 void *	     pCoProcArgument )
				{
				    this ->pCoProc	   = pCoProc	     ;
				    this ->pCoProcContext  = pCoProcContext  ;
				    this ->pCoProcArgument = pCoProcArgument ;

				    return this;
				};
	};

	struct GIrp_ExProcEntry : public GIrp_StackEntry
	{
	    GIrp_ExProc		pExProc		;
	    void *		pExProcContext  ;
	    void *		pExProcArgument ;

	    GIrp_ExProcEntry *	HighExEntry	() const { return
						(GIrp_ExProcEntry *) HighEntry ();};

	    GIrp_ExProcEntry *	Init		(GIrp_ExProc pExProc	     ,
						 void *	     pExProcContext  ,
						 void *	     pExProcArgument )
				{
				    this ->pExProc	   = pExProc	     ;
				    this ->pExProcContext  = pExProcContext  ;
				    this ->pExProcArgument = pExProcArgument ;

				    return this;
				};
	};

	struct GIrp_DpProcEntry : public GIrp_StackEntry
	{
	    GIrp_DpProc		pDpProc		;
	    void *		pDpProcContext	;
	    void *		pDpProcArgument ;

	    GIrp_DpProcEntry *	HighDpEntry	() const { return
						(GIrp_DpProcEntry *) HighEntry ();};

	    GIrp_DpProcEntry *	Init		(GIrp_DpProc pDpProc	     ,
						 void *	     pDpProcContext  ,
						 void *	     pDpProcArgument )
				{
				    this ->pDpProc	   = pDpProc	     ;
				    this ->pDpProcContext  = pDpProcContext  ;
				    this ->pDpProcArgument = pDpProcArgument ;

				    return this;
				};
	};

	struct GIrp
	{
//
// Self types ----------------------------------------------------------------
//
	typedef	TSLink <GIrp, 0>    Link		;
	typedef TXList <GIrp,
		TSLink <GIrp, 0> >  List		;

	typedef	GIrp_CoProcEntry    CoProcEntry		;
	typedef GIrp_ExProcEntry    ExProcEntry		;
	typedef GIrp_DpProcEntry    DpProcEntry		;

	typedef GIrp_ExProc	    ExProc		;
	typedef	GIrp_CoProc	    CoProc		;
	typedef	GIrp_DpProc	    DpProc		;
	typedef GIrp_CancelProc	    CancelProc		;

	typedef GIrp_StackEntry	    StackEntry		;
	typedef GIrp_Status	    Status		;
	typedef GIrp_Request	    Request		;

//
// Self members --------------------------------------------------------------
//
	    Link		    QLink		;

	    void *		    pHeadESP		;
	    void *		    pTailESP		;

	    CoProcEntry *	    pCurCoEntry		;
	    void *		    pCurCoContext	;

    	    DpProcEntry *	    pCurDpEntry		;
	    void *		    pCurDpContext	;

	    ExProcEntry *	    pCurExEntry		;

	    Status  *		    pCurStatus		;
	    Request *		    pCurRequest		;

	    TInterlockedPtr <GAccessLock *>
				    pCancelProcLock	;
	    CancelProc		    pCancelProc		;
	    void *		    pCancelProcContext	;

	    GTimeout		    tTimeout		;
//
// Public methods ------------------------------------------------------------
//
	void *	operator    new		    (size_t sz)
			    {
				return GIrp::operator new (sz, _M_PAGE_SIZE);
			    };

	void *	operator    new		    (size_t, size_t tIrpBlockSize);

	void	operator    delete	    (void *)	     {};
	void	operator    delete	    (void *, size_t) {};

static	GIrp *		    Init	    (void * pIrpBlock	 ,
					     size_t tIrpBlockSize, CoProc pFreeCoProc	     = NULL,
								   void * pFreeCoProcContext = NULL);

	size_t		    GetStackSize    () const { return ((Byte *) pTailESP - (Byte *) pHeadESP);};

	Bool		    Cancelled	    () const
			    {
				return ((GAccessLock *) INVALID_PTR == pCancelProcLock.GetValue ()) ? True : False;
			    };

	Status *	    CurStatus	    () const { return pCurStatus ;};

	GResult		    CurResult	    () const { return pCurStatus ->Result ();};

	GResult		    Result	    () const { return pCurStatus ->Result ();};

	DWord		    ResultInfo	    () const { return pCurStatus ->ResultInfo ();};

	void *		    ResultInfoPtr   () const { return pCurStatus ->ResultInfoPtr ();};

	void		    SetResultInfo	(GResult dResult, DWord dResultInfo)
			    {
				pCurStatus ->SetStatusInfo    (dResult, dResultInfo);
			    };
/*
	void		    SetResultInfoPtr	(GResult dResult, void * pResultInfo)
			    {
				pCurStatus ->SetResultInfoPtr (dResult, pResultInfo);
			    };
*/

	GResult		    GetCurResult	() const { return pCurStatus ->Result ();};

	void *		    GetCurResultInfo	() const { return pCurStatus ->ResultInfoPtr ();};

	void		    SetCurResultInfo	(GResult dResult, void * pResultInfo = NULL)
			    {
				pCurStatus ->SetResultInfoPtr (dResult, pResultInfo);
			    };

	template <typename T>
	T *		    CurStatus	    () const { return (T *) CurStatus  ();};

	Request *	    GetCurRequest   () const { return pCurRequest;};

	template <typename T>
	T *		    GetCurRequest   () const { return (T *) CurRequest ();};

	void *		    GetCurCoContext () const { return pCurCoContext;};

//???	GIrp_DispatchQueue *
//???			    CurDispQueue    () const { return (GIrp_DispatchQueue *) pCurCoQueue ->pCoProcArgument;};

//???	void *		    HeadParam	    () const { return (void *) pHeadESP ;};
//???
//???	template <typename T>
//???	T *		    HeadParam	    () const { return (T *) HeadParam ();};

	void *		    alloc	    (size_t sz)
			    {
				void * ptr = pHeadESP;
			      ((Byte *&)     pHeadESP) += roundup (sz, _M_PACK_VPTR);

				return ptr;
			    };

	StackEntry *	    allocStackEntry (size_t sz, StackEntry * pHighEntry)
			    {
				return
				((StackEntry *) alloc (sz)) ->init (pHighEntry);
			    };
//
//----------------------------------------------------------------------------
//
	CoProcEntry *	    CreateCoProcEntry	(size_t sz)
			    {
				return 
			       (CoProcEntry *) allocStackEntry (sz, pCurCoEntry);
			    }

	void		    SetCurCoProcEntry	(CoProcEntry * pCoEntry)
			    {
				pCurCoEntry = pCoEntry;
			    };

	void		    SetCurCoProc	(CoProc	pCoProc		,
						 void *	pCoProcContext  ,
						 void *	pCoProcArgument )
			    {
				SetCurCoProcEntry (
				CreateCoProcEntry (sizeof   (CoProcEntry   )) ->
					     Init (pCoProc, pCoProcContext,
							    pCoProcArgument));
			    };
//
//----------------------------------------------------------------------------
//
	ExProcEntry *	    CreateExProcEntry	(size_t sz)
			    {
				return
			       (ExProcEntry *) allocStackEntry (sz, pCurExEntry);
			    };

	void		    SetCurExProcEntry	(ExProcEntry * pExEntry)
			    {
				SetCurCoProc (RestoreCurExEntry, NULL, pCurExEntry);
				pCurExEntry = pExEntry;
			    };

	void		    SetCurExProc	(ExProc pExProc	        ,
						 void *	pExProcContext  ,
						 void * pExProcArgument )
			    {
				SetCurExProcEntry (
				CreateExProcEntry (sizeof   (ExProcEntry   )) ->
					     Init (pExProc, pExProcContext,
							    pExProcArgument));
			    };
//
//----------------------------------------------------------------------------
//
	DpProcEntry *	    CreateDpProcEntry	(size_t sz)
			    {
				return
			       (DpProcEntry *) allocStackEntry (sz, pCurDpEntry);
			    };

	void		    SetCurDpProcEntry	(DpProcEntry * pDpEntry)
			    {
				SetCurCoProc (RestoreCurDpEntry, NULL, pCurDpEntry);
				pCurDpEntry = pDpEntry;
			    };

	void		    SetCurDpProc	(DpProc	pDpProc		,
						 void *	pDpProcContext  ,
						 void *	pDpProcArgument )
			    {
				SetCurDpProcEntry (
				CreateDpProcEntry (sizeof   (DpProcEntry   )) ->
					     Init (pDpProc, pDpProcContext,
							    pDpProcArgument));
			    };
//
//----------------------------------------------------------------------------
//
	Status *	    CreateStatus    (size_t sz)
			    {
				return 
			       (Status *)   allocStackEntry  (sz, pCurStatus);
			    };

	void		    SetCurStatus    (Status * pStatus , void (GAPI * pDestructor)(void *) = NULL)
			    {
				SetCurCoProc (RestoreCurStatus , pDestructor, pCurStatus);
				pCurStatus  = pStatus;
			    };
//
//----------------------------------------------------------------------------
//
	Request *	    CreateRequest   (size_t sz)
			    {
				return
			      ((Request *)  alloc (sz)) ->init (pCurRequest, sz);
			    };

	void		    SetCurRequest   (Request * pRequest, void (GAPI * pDestructor)(void *))
			    {
				SetCurCoProc (RestoreCurRequest, pDestructor, pCurRequest);
				pCurRequest = pRequest;
			    };
//
//----------------------------------------------------------------------------
//
/*???
	void		    SetCurDispQueue (GIrp_DispatchQueue * pQueue)
			    {
				SetCurCoProc (RestoreCurDispQueue, pCurCoQueue, pQueue);
				pCurCoQueue = pCurCoEntry;
			    };

	void		    ReleaseCurDispQueue	();
???*/

static	GIrp * GAPI	    RestoreCurExEntry	(GIrp *, void *, void *);
static	GIrp * GAPI	    RestoreCurDpEntry	(GIrp *, void *, void *);

static	GIrp * GAPI	    RestoreCurStatus	(GIrp *, void *, void *);
static	GIrp * GAPI	    RestoreCurRequest	(GIrp *, void *, void *);

static	GIrp * GAPI	    ReleaseIUnknown	(GIrp *, void *, void *);

static	GIrp * GAPI	    QueueForComplete	(GIrp *, void *, void *);


/*???
static	GIrp * GAPI	    RestoreCurDispQueue	(GIrp *, void *, void *);
???*/


/*
	void		    SkipCurRequest  ()
			    {
				pCurRequest = CurRequest () ->HighRequest ();
			    };
*/
/*+++
	void		    SetDispProc	    (DispProc	pDispProc	 ,
					     void *	pDispProcContext )
			    {
			      (((BackProcEntry *) pCurStatus) - 1) ->Init 
						       (pDispProc	 ,
							pDispProcContext );
			    };


	Request *	    CreateRequest      (size_t sz, Status * pStatus)
			    {
				CreateBackProc (GIrp::RestoreRequests,
						pCurRequest	     ,
						pCurDispRequest	     );

				return		pCurRequest =  
			      ((Request *) alloc (sz)) ->init (sz, pStatus);
			    };

	Request *	    CreateDispRequest  (size_t sz, Status * pStatus)
			    {
				CreateBackProc (GIrp::RestoreRequests,
						pCurRequest	     ,
						pCurDispRequest	     );

						pCurRequest =  
			      ((Request *) alloc (sz)) ->init (sz, pStatus);

				return		pCurDispRequest =
			       (DispRequest *)	pCurRequest;
			    };
+++*/
/*
*/
    };

#pragma pack ()
//
//[]------------------------------------------------------------------------[]


//[]------------------------------------------------------------------------[]
//
    template <size_t S>	struct TIrp : public GIrp
    {
			    TIrp () { Init (this, sizeof (TIrp <S>));};

        Byte		    pReserved [S -  sizeof (GIrp)];
    };
//
//[]------------------------------------------------------------------------[]
//
    typedef GIrp::Link	GIrp_Link;
    typedef GIrp::List	GIrp_List;
//
//[]------------------------------------------------------------------------[]
//
    template <typename T> class TIrp_Allocator
    {
	public :
	void * operator new     (size_t sz, GIrp * pIrp, T ** t)
			{
			    void *  p = pIrp ->alloc (sz);

			    pIrp ->SetCurCoProc ((GIrp_CoProc) destroyCoProc, t, p);

			    return (t) ? (* t = (T *) p) : p;
			};

	void   operator delete  (void *)	       {};
	void   operator delete  (void *, GIrp *, T **) {};

	static GIrp * GAPI  destroyCoProc (GIrp * pIrp, T ** t, T * p)
			{
			    if (t) 
			    {
			      * t = NULL;
			    }

			    return p ->~T (), pIrp;
			};
    };
//
//[]------------------------------------------------------------------------[]
//
    template <typename T>
    GFORCEINLINE T * CurStatus  (GIrp * pIrp)
    {
	return (T *) pIrp ->CurStatus  ();
    };

    template <typename T>
    GFORCEINLINE T * GetCurRequest (GIrp * pIrp)
    {
	return (T *) pIrp ->GetCurRequest ();
    };

//  GFORCEINLINE GIrp_DispatchQueue * CurDispQueue (GIrp * pIrp)
//  {
//	return pIrp ->CurDispQueue ();
//  };

//  template <typename T>
//  GFORCEINLINE T * HeadParam (GIrp * pIrp)
//  {
//	return (T *) pIrp ->TopParam   ();
//  };
//
//[]------------------------------------------------------------------------[]
//
    GFORCEINLINE void * alloc (GIrp * pIrp, size_t sz)
    {
	return (void *) pIrp ->alloc (sz);
    };

    template <typename T>
    GFORCEINLINE T *	alloc (GIrp * pIrp, const T * = NULL)
    {
	return (T *) pIrp ->alloc (sizeof (T));
    };

    template <typename T>
    GFORCEINLINE void * operator new (size_t sz, GIrp * pIrp, T ** t)
    {
	return TIrp_Allocator <T>::operator new (sz, pIrp, t);
    };
//
//[]------------------------------------------------------------------------[]
/*
    GFORCEINLINE void SetCurCoType (GIrp * pIrp, CoType tCoType, GIrp_CoQueue * pCoQueue = NULL)
    {
	pIrp ->SetCurCoType (tCoType, pCoQueue);
    };
*/
//[]------------------------------------------------------------------------[]
//
    template <typename T>
    GFORCEINLINE T * CreateStatus (GIrp * pIrp, const T * = NULL)
    {
	return (T *) pIrp ->CreateStatus (sizeof (T));
    };

    GFORCEINLINE void SetCurStatus (GIrp * pIrp, GIrp_Status * pStatus, void (GAPI * pDestructor)(void *) = NULL)
    {
	pIrp ->SetCurStatus (pStatus, pDestructor);
    };
//
//[]------------------------------------------------------------------------[]
//
    template <typename T>
    GFORCEINLINE T * CreateRequest (GIrp * pIrp, const T * = NULL)
    {
	return (T *) pIrp ->CreateRequest (sizeof (T));
    };

    GFORCEINLINE void SetCurRequest (GIrp * pIrp, GIrp_Request * pRequest, void (GAPI * pDestructor)(void *) = NULL)
    {
	pIrp ->SetCurRequest (pRequest, pDestructor);
    };

    template <typename T>
    GFORCEINLINE void SetCurRequest (GIrp * pIrp, T * pRequest, void (GAPI * pDestructor)(T *) = NULL)
    {
	pIrp ->SetCurRequest (pRequest, (void (GAPI *)(void *)) pDestructor);
    };
//
//[]------------------------------------------------------------------------[]
//
    GFORCEINLINE void SetCurCoProc (GIrp *	pIrp		,
				    GIrp_CoProc	pCoProc		,
				    void *	pCoProcContext  ,
				    void *	pCoProcArgument )
    {
	pIrp ->SetCurCoProc (pCoProc, pCoProcContext  , 
				      pCoProcArgument );
    };

    template <typename T>
    struct TIrp_CoProcEntry : public GIrp_CoProcEntry
    {
	T *		tCoProcContext ;
	GIrp *	  (T::* tCoProc)(GIrp *, void *);

	GIrp_CoProcEntry *  Init	(GIrp * (T::*	  tCoProc     )(GIrp *, void *),
						 T    *	  tCoContext  ,
						 void *	  pCoArgument )
			    {
				GIrp_CoProcEntry::Init
			      ((GIrp_CoProc) TIrp_CoProcEntry::CoProc ,
							  this	      ,
							  pCoArgument );

				this ->tCoProcContext	= tCoContext  ;
				this ->tCoProc		= tCoProc     ;

				return this;
			    };

	static GIrp * GAPI  CoProc	(GIrp * pIrp, TIrp_CoProcEntry * e, void * a)
			    {
				return  ((e ->tCoProcContext) ->* (e ->tCoProc))(pIrp, a);
			    };
    };

    template <typename T, typename Ptr>
    GFORCEINLINE void SetCurCoProc	(GIrp * pIrp, GIrp * (T::* tCoProc    )(GIrp *, Ptr),
							      T  * tCoContext ,
							      Ptr  pCoArgument)
    {
	pIrp ->SetCurCoProcEntry (	((TIrp_CoProcEntry <T>	 *) 
	pIrp ->CreateCoProcEntry (sizeof (TIrp_CoProcEntry <T>	  ))) ->Init (
					      (GIrp * (T::*)(GIrp *, void *)) tCoProc	  ,
									      tCoContext  ,
								    (void *)  pCoArgument ));
    };
//
//[]------------------------------------------------------------------------[]
//
    GFORCEINLINE void SetCurExproc (GIrp *	pIrp	       ,
				    GIrp_ExProc pExProc	       ,
				    void *	pExProcContext ,
				    void *	pExProcArgument)
    {
	pIrp ->SetCurExProc (pExProc, pExProcContext  ,
				      pExProcArgument );
    };

    template <typename T>
    struct TIrp_ExProcEntry : public GIrp_ExProcEntry
    {
	T *		tExProcContext ;
	Bool	  (T::* tExProc)(GIrp_ExRecord *, void *);

	TIrp_ExProcEntry *  Init	(Bool  (T::*	  tExProc     )(GIrp_ExRecord *, void *),
						T  *	  tExContext  ,
						void *	  tExArgument )
			    {
				GIrp_ExProcEntry::Init
			      ((GIrp_ExProc) TIrp_ExProcEntry::ExProc ,
							  this	      ,
							  pExArgument );

				this ->tExProcContext   = tExContext ;
				this ->tExProc		= tExProc    ;

				return this;
			    };

	static Bool GAPI    ExProc	(GIrp_ExRecord r, TIrp_ExProcEntry * e, void * a)
			    {
				return  ((e ->tExProcContext) ->* (e ->tExProc))(r, a);
			    };
    };

    template <typename T, typename Ptr>
    GFORCEINLINE void SetCurExProc (GIrp * pIrp, Bool    (T::* tExProc    )(GIrp_ExRecord *, Ptr),
							  T  * tExContext ,
							  Ptr  tExArgument)
    {
	pIrp ->SetCurExProcEntry (	((TIrp_ExProcEntry <T>     *)
	pIrp ->CreateExProcEntry (sizeof (TIrp_ExProcEntry <T>      ))) ->Init (
				         (Bool (T::*)(GIrp_ExRecord *, void *)) tExProc	    ,
									        tExContext  ,
								      (void *)	tExArgument ));
    };
//
//[]------------------------------------------------------------------------[]
//
    GFORCEINLINE void SetCurDpProc (GIrp *	pIrp	       ,
				    GIrp_DpProc	pDpProc	       ,
				    void *	pDpProcContext ,
				    void *	pDpProcArgument)
    {
	pIrp ->SetCurDpProc (pDpProc, pDpProcContext  ,
				      pDpProcArgument );
    };

    template <typename T>
    struct TIrp_DpProcEntry : public GIrp_DpProcEntry
    {
	T *		tDpProcContext ;
	GIrp *	  (T::* tDpProc)(GIrp *, GIrp_DpContext, void *);

	TIrp_DpProcEntry *  Init	(GIrp * (T::*	  tDpProc     )(GIrp *, GIrp_DpContext, void *),
					         T  *	  tDpContext  ,
					         void *	  pDpArgument )
			    {
				GIrp_DpProcEntry::Init
			      ((GIrp_DpProc) TIrp_DpProcEntry::DpProc ,
							  this	      ,
							  pDpArgument );

				this ->tDpProcContext   = tDpContext ;
				this ->tDpProc	        = tDpProc    ;

				return this;
			    };

	static GIrp * GAPI  DpProc	(GIrp * pIrp, GIrp_DpContext d, TIrp_DpProcEntry * e, void * a)
			    {
				return	((e ->tDpProcContext) ->* (e ->tDpProc))(pIrp, d, a);
			    };
    };

    template <typename T, typename Ptr>
    GFORCEINLINE void SetCurDpProc (GIrp * pIrp, GIrp *  (T::* tDpProc    )(GIrp *, GIrp_DpContext, Ptr),
							  T  * tDpContext ,
							  Ptr  pDpArgument)
    {
	pIrp ->SetCurDpProcEntry (	((TIrp_DpProcEntry <T>	 *) 
	pIrp ->CreateDpProcEntry (sizeof (TIrp_DpProcEntry <T>	  ))) ->Init (
					 (GIrp * (T::*)(GIrp *, GIrp_DpContext, void *)) tDpProc     ,
											 tDpContext  ,
									       (void *)  pDpArgument ));
    };
//
//[]------------------------------------------------------------------------[]
//
    GFORCEINLINE void SetReleaseCoProc	(GIrp * pIrp, IUnknown * pI)
    {
	pIrp ->SetCurCoProc (GIrp::ReleaseIUnknown, NULL, pI);
    };
//
//[]------------------------------------------------------------------------[]







//[]------------------------------------------------------------------------[]
//
class GIrp_DispatchQueue : public GIrp_List
{
public :

	GIrp_DispatchQueue () : m_pCurIrp (NULL) {};

	GIrp_DispatchQueue (const GAccessLock & lock) : m_qLock (lock), m_pCurIrp (NULL) {};

// Dispatching group ------------------
//
    	void		QueueForDispatch    (GIrp * pIrp);
//
// End dispatching gorup --------------

protected :

	void		LockAcquire	    () { m_qLock.LockAcquire ();};

	void		LockRelease	    () { m_qLock.LockRelease ();};

	void		DispatchRequest	    (GIrp *);

static	GIrp * GAPI	Xxxx_Co_Dispatch    (GIrp * pIrp, void *, void *);

static	GIrp * GAPI	Xxxx_Dispatch	    (GIrp * pIrp, void *, void *);

private :

        GAccessLock	    m_qLock	;   // (*) Lock for (*) member
        GIrp *		    m_pCurIrp	;   // (*)
};
//
//[]------------------------------------------------------------------------[]



//[]------------------------------------------------------------------------[]
//
/*
class GIrp_Dispatcher
{
	friend class GIrp_Queue;

// Completion group -------------------
//
	void		QueueIrpForComplete (GIrp * pIrp);
//
// End completion group ---------------

protected :

	void		LockAcquire	    () { m_qLock.LockAcquire ();};

	void		LockRelease	    () { m_qLock.LockRelease ();};

virtual	void		complete	    ();

virtual	void		dispatch	    (GIrp_Queue &, GIrp * pIrp);    // (*) on entry, Must be released !!!

private :

	GAccessLock	    m_qLock ;	// (*) Lock for (*) member
	GIrp *		    m_pBusy ;	// (*)
};


class GIrp_Queue : public GIrp_List
{
public :

	GIrp_Queue (GIrp_Dispatcher & disp) : m_qDisp (disp), m_pBusy (NULL) {};

	void	    LockAcquire	()	{ m_qDisp.LockAcquire ();};

	void	    LockRelease	()	{ m_qDisp.LockRelease ();};

	void	    QueueIrp	(GIrp * pIrp)
		    {
			LockAcquire ();

			    append (pIrp);

			LockRelease ();
		    };

// Dispatch group ---------------------
//
	void	    QueueIrpForComplete	(GIrp * pIrp)
		    {
			m_qDisp.QueueIrpForComplete (pIrp);
		    };

	void	    QueueIrpForDispatch (GIrp *, Bool bCancelable = False);
//
// End dispatch group -----------------

protected :

static	void GAPI   Cancel_Queue	(GIrp *, GAccessLock *, void *);

	GIrp *	    Dispatch_CoProc	(GIrp *, void *);

virtual	void	    Dispatch		(GIrp *);

private :

//	GIrp_Dispatcher &   m_qDisp ;
	GIrp *		    m_pBusy ;	// (*) Locked by m_qDisp::m_qLock
};

*/




template <typename T>
class TIrp_DispatchQueue : public GIrp_DispatchQueue
{
public :

	TIrp_DispatchQueue (const GAccessLock & lock, void (T::* tDispProc)(GIrp_DispatchQueue &, GIrp *, GIrp *), T * tDispContext) :
	
	GIrp_DispatchQueue (lock) {};

private :

static	void GAPI    DispatchProc     (GIrp_DispatchQueue & DQueue,
				       GIrp * pCurIrp, GIrp * pIrp,
				       void * tDispContext	  )
	{
	    (((T *) tDispContext) ->* ((TIrp_DispatchQueue <T> &) DQueue).m_tDispProc)(DQueue, pCurIrp, pIrp);
	};

private :

	void (T::*	    m_tDispProc)(GIrp_DispatchQueue &, GIrp *, GIrp *);
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
/*???
    GFORCEINLINE void SetCurDispQueue	  (GIrp * pIrp, GIrp_DispatchQueue * q)
    {
	pIrp ->SetCurDispQueue (q);
    };

    GFORCEINLINE void ReleaseCurDispQueue (GIrp * pIrp)
    {
	pIrp ->ReleaseCurDispQueue ();
    };

    GFORCEINLINE void SetQueueForCompleteCoProc	(GIrp * pIrp)
    {
	pIrp ->SetCurCoProc (GIrp::QueueForComplete, NULL, NULL);
    };
???*/
//
//[]------------------------------------------------------------------------[]

    GIrp * GAPI	    CreateIrp	();











#if FALSE
//[]------------------------------------------------------------------------[]
//
//	typedef void   (GAPI *  GIrp_ReplyProc)	    (GIrp *, void *, const void * pRequestId);

	struct GIrp_ReplyHandler : public XReplyHandler
	{
	typedef TSLink <GIrp_ReplyHandler, sizeof (XReplyHandler)>   Link ;
	typedef TXList <GIrp_ReplyHandler,
		TSLink <GIrp_ReplyHandler, sizeof (XReplyHandler)> > List ;

	    Link		    m_QLink       ;
	    GIrp *		    m_pIrp        ;
	    XDispatch  *	    m_pXDispatch  ;

	    XReplyProc		    m_pRplProc	  ;
		  void *	    m_pRplContext ;
	    const void *	    m_pRequestId  ;

	       GIrp_ReplyHandler ()
	       {
		    m_pIrp	  = NULL;
		    m_pXDispatch  = NULL;
		    m_pRplProc    = NULL;
		    m_pRplContext = NULL;
		    m_pRequestId  = NULL;
	       };

	       GIrp_ReplyHandler (GIrp *      pIrp	  ,
				  XDispatch * pXDispatch  ,
				  XReplyProc  pRplProc	  ,
				  void *      pRplContext ,
			    const void *      pRequestId  )
	       {
		    m_pIrp	  = pIrp;
		   (m_pXDispatch  = pXDispatch	) ->AddRef ();
		    m_pRplProc    = pRplProc    ;
		    m_pRplContext = pRplContext ;
		    m_pRequestId  = pRequestId  ;
	       };

	      ~GIrp_ReplyHandler ()
	       {
		    m_pXDispatch ->Release ();
	       };

	static GIrp * GAPI	CoRequest   (GIrp *, void *, void *);

	void STDMETHODCALLTYPE	Close	    ();

	void STDMETHODCALLTYPE	Invoke	    (GIrp *);
	};

    GFORCEINLINE GIrp_ReplyHandler * CreateXReplyHandler 
				    (GIrp * pIrp, XDispatch * pXDispatch  ,
						  XReplyProc  pRplProc    ,
						  void *      pRplContext ,
					    const void *      pRequestId  )
    {
	GIrp_ReplyHandler * h = new (pIrp, (GIrp_ReplyHandler **) NULL)
	GIrp_ReplyHandler	    (pIrp, pXDispatch, pRplProc, pRplContext, pRequestId);

	SetCurCoProc (pIrp, GIrp_ReplyHandler::CoRequest, h, NULL);

	return h;
    };

//
//[]------------------------------------------------------------------------[]
//
    template <typename T>
    struct TIrp_ReplyHandler : public GIrp_ReplyHandler
    {
	T *		tRplProcContext	;
	void	  (T::* tRplProc)(GIrp *, const void *);

	   TIrp_ReplyHandler (GIrp_ReplyHandler &
					 tTemplate  ,
			      GIrp *	 pIrp	    ,
			void (  T::*	 tRplProc   )(GIrp *, const void *),
			        T  *	 tRplContext,
			const void *	 pRequestId ) : 

	   GIrp_ReplyHandler (tTemplate, pIrp, (GIrp_ReplyProc) RplProc, tRplContext, pRequestId)
	   {
		this ->tRplProcContext = tRplContext ;
		this ->tRplProc	       = tRplProc    ;
	   };

	static void   GAPI  RplProc (GIrp * pIrp, TIrp_ReplyHandler * e, const void * pRequestId)
			    {
				((e ->tRplProcContext) ->* (e ->tRplProc))(pIrp, pRequestId);
			    };
    };

    template <typename T>
    GFORCEINLINE GIrp_ReplyHandler * CreateXReplyHandler 
				    (GIrp * pIrp, GIrp_ReplyHandler &
							       tTemplate   ,
						  void (T::*   tRplProc    )(GIrp *, const void *),
							T  *   tRplContext ,
						  const void * pRequestId  )
    {
	return new (pIrp, (TIrp_ReplyHandler <T> **) NULL) 
			   TIrp_ReplyHandler <T> (tTemplate, pIrp, tRplProc, tRplContext, pRequestId);
    };
//
//[]------------------------------------------------------------------------[]
#else

    struct GIrp_ReplyHandler {};


#endif

#endif//__G_IRPDEFS_H
