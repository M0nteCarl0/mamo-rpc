//[]------------------------------------------------------------------------[]
// Main Stream based declarations
//
//
// Created by GSh.
//[]------------------------------------------------------------------------[]

#ifndef __G_STRDEFS_H
#define __G_STRDEFS_H

//[]------------------------------------------------------------------------[]
//
#pragma pack (_M_PACK_BYTE)

    struct WPack
    {
	union
	{
	    _U16    w    ;
	    Byte    b [2];
	};

		void	put		(Word w)
			{
			    this ->w = w;
			};

		void	putLitEndian    (Word w)
			{
#if _M_BIGENDIAN
			    b [0] = LOBYTE (w);
			    b [1] = HIBYTE (w);
#else
			    this ->w = w;
#endif
			};

		void	putBigEndian    (Word w)
			{
#if _M_BIGENDIAN
			    this ->w = w;
#else
			    b [0] = HIBYTE (w);
			    b [1] = LOBYTE (w);
#endif
			};
    };

    struct DPack
    {
	union
	{	    
	    _U32    d	 ;
	    WPack   w [2];
	};

		void	put		(DWord d)
			{
			    this ->d = d;
			};

		void	putLitEndian    (DWord d)
			{
#if _M_BIGENDIAN
			    w [0].putLitEndian (LOWORD (d));
			    w [1].putLitEndian (HIWORD (d));
#else
			    this ->d = d;
#endif
			};

		void	putBigEndian    (DWord d)
			{
#if _M_BIGENDIAN
			    this ->d = d;
#else
			    w [0].putBigEndian (HIWORD (d));
			    w [1].putBigEndian (LOWORD (d));
#endif
			};
    };

#pragma pack ()

    GFORCEINLINE void	putB	       (void * p, size_t o, Byte b)
			{
			    * ((Byte *) p  + o) = b;
			};

    GFORCEINLINE void	putW	       (void * p, size_t o, Word w)
			{
			    ((WPack *)((Byte *) p + o)) ->w = w;
			};

    GFORCEINLINE void	putLitEndianW (void * p, size_t o, Word w)
			{
#if _M_BIGENDIAN
			    ((WPack *)((Byte *) p + o)) ->b [0] = LOBYTE(w);
			    ((WPack *)((Byte *) p + o)) ->b [1] = HIBYTE(w);
#else
			    ((WPack *)((Byte *) p + o)) ->w = w;
#endif
			};

    GFORCEINLINE void	putBigEndianW (void * p, size_t o, Word w)
			{
#if _M_BIGENDIAN
			    ((WPack *)((Byte *) p + o)) ->w = w;
#else
			    ((WPack *)((Byte *) p + o)) ->b [0] = HIBYTE(w);
			    ((WPack *)((Byte *) p + o)) ->b [1] = LOBYTE(w);
#endif
			};

    GFORCEINLINE void	putD	       (void * p, size_t o, DWord d)
			{
			    ((DPack *)((Byte *) p + o)) ->d = d;
			};

    GFORCEINLINE void	putLitEndianD (void * p, size_t o, DWord d)
			{
#if _M_BIGENDIAN
			    putLitEndianW (p, o    , LOWORD(d));
			    putLitEndianW (p, o + 2, HIWORD(d));
#else
			    ((DPack *)((Byte *) p + o)) ->d = d;
#endif
			};

    GFORCEINLINE void	putBigEndianD (void * p, size_t o, DWord d)
			{
#if _M_BIGENDIAN
			    ((DPack *)((Byte *) p + o)) ->d = d;
#else
			    putBigEndianW (p, o    , HIWORD(d));
			    putBigEndianW (p, o + 2, LOWORD(d));
#endif
			};

    GFORCEINLINE Byte	getB	       (void * p, size_t o)
			{
			    return * ((Byte *) p  + o);
			};

    GFORCEINLINE Word	getW	       (const void * p, size_t o)
			{
			    return ((WPack *)((Byte *) p + o)) ->w;
			};

    GFORCEINLINE Word	getLitEndianW (const void * p, size_t o)
			{
#if _M_BIGENDIAN
			    return ((Word)((WPack *)((Byte *) p + o)) ->b [1] << 8) |
				   ((Word)((WPack *)((Byte *) p + o)) ->b [0] << 0);
#else
			    return ((WPack *)((Byte *) p + o)) ->w;
#endif
			};

    GFORCEINLINE Word	getBigEndianW (const void * p, size_t o)
			{
#if _M_BIGENDIAN
			    return ((WPack *)((Byte *) p + o)) ->w;
#else
			    return ((Word)((WPack *)((Byte *) p + o)) ->b [0] << 8) |
				   ((Word)((WPack *)((Byte *) p + o)) ->b [1] << 0);
#endif
			};

    GFORCEINLINE DWord	getD	       (const void * p, size_t o)
			{
			    return ((DPack *)((Byte *) p + o)) ->d;
			};

    GFORCEINLINE DWord	getLitEndianD (const void * p, size_t o)
			{
#if _M_BIGENDIAN
			    return ((DWord) getLitEndianW (p, o + 2) << 16) |
				   ((DWord) getLitEndianW (p, o    ) <<  0);
#else
			    return ((DPack *)((Byte *) p + o)) ->d;
#endif
			};

    GFORCEINLINE DWord	getBigEndianD (const void * p, size_t o)
			{
#if _M_BIGENDIAN
			    return ((DPack *)((unsigned char *) p + o)) ->d;
#else
			    return ((DWord) getBigEndianW (p, o    ) << 16) |
				   ((DWord) getBigEndianW (p, o + 2) <<  0);
#endif
			};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
#define	TXT_DELIMITORS	(const char *)(1)

    struct GStreamBuf
    {
	Byte *	    pTail ;
	Byte *	    pHead ;
	Byte *	    pBase ;
	Byte *	    pHigh ;

// Public methods ------------------------------------------------------------
//
	DWord	    Write		(const void *& pSrcBuf, DWord & dLength)
		    {
			DWord forput = (DWord)((sizeforput () < dLength)
					     ?  sizeforput () : dLength);

			put  (pSrcBuf, forput);

		       (const Byte *&) pSrcBuf += forput;
				       dLength -= forput;

			return forput;
		    };


	DWord	    Read		(void * pDstBuf, DWord dLength)
		    {
			dLength = (DWord)((sizeforget () < dLength)
					?  sizeforget () : dLength);

		    if (pDstBuf)
		    {
			memmove (pDstBuf, pHead,   dLength);
		    }
			pHead		        += dLength;

			return dLength;
		    };
//
// ---------------------------------------------------------------------------
//
		    GStreamBuf		() { SetBuf (NULL, 0);};

		    GStreamBuf		(void *	pBuf, size_t sBufLen) { SetBuf (pBuf, sBufLen);};

	void *	    SetBuf		(void * pBuf, size_t sBufLen)
		    {
		        pHigh   = 
		       (pBase   =	   pTail
				=	   pHead 
		    		=	   pBase 
				= (Byte *) pBuf ) + ((pBuf) ? sBufLen : 0); 

			return    pBase;
		    };

	void	    sethead		(size_t headoffest = 0);

	void	    clear		(size_t headoffset = 0)
		    {
		        pTail   =
		        pHead   = pBase + headoffset;
		    };
//
// ---------------------------------------------------------------------------
//
	Byte *	    head		() const { return pHead;};

	Byte *	    tail		() const { return pTail;};

	size_t	    size		() const { return pHigh - pBase;};

	size_t	    sizeforputback	() const { return pHead - pBase;};

	size_t	    sizeforget		() const { return pTail - pHead;};

	size_t	    sizeforput		() const { return pHigh - pTail;};
//
// ---------------------------------------------------------------------------
//
	void *	    put			(DWord n) { pTail += n; return pTail - n;};

	void	    putB		(Byte b) { ::putB (pTail, 0, b); pTail ++;};

	void	    putW		(_U16 w) { ::putW (pTail, 0, w); pTail += sizeof (_U16);};

	void	    putD		(_U32 d) { ::putD (pTail, 0, d); pTail += sizeof (_U32);};

	void	    putBigEndianW	(_U16 w) { ::putBigEndianW (pTail, 0, w); pTail += sizeof (_U16);};

	void	    putBigEndianD	(_U32 d) { ::putBigEndianD (pTail, 0, d); pTail += sizeof (_U32);};

	void	    put			(const void * p, DWord n) { ::memcpy (put (n), p, n); };

//New	void	    put			(const void * p, DWord n) { memcpy (pTail, p, n); pTail += n;};
//
// ---------------------------------------------------------------------------
//
	void	    putbackB		(Byte b) { pHead --; ::putB (pHead, 0, b);};

	void	    putbackW		(_U16 w) { pHead -= sizeof (_U16); ::putW (pHead, 0, w);};

	void	    putbackD		(_U32 d) { pHead -= sizeof (_U32); ::putD (pHead, 0, d);};

	void	    putbackBigEndianW	(_U16 w) { pHead -= sizeof (_U16); ::putBigEndianW (pHead, 0, w);};

	void	    putbackBigEndianD	(_U32 d) { pHead -= sizeof (_U32); ::putBigEndianD (pHead, 0, d);};

	void	    pubback		(const void * p, DWord n) { pHead -= n; memcpy (pHead, p, n);};
//
// ---------------------------------------------------------------------------
//
	void *	    putLengthB		() { putB (0); return (Byte  *) pTail - 1;};

	void *	    putLengthW		() { putW (0); return (WPack *) pTail - 1;};

	void *	    putLengthD		() { putD (0); return (DPack *) pTail - 1;};
//
// ---------------------------------------------------------------------------
//
	void	    printfA		(const char  *, ...);

//	int	    PrintfA		(const char  *, ...);	// return sizeforput (-1)

	void	    printfW		(const WCHAR *, ...);

//	int	    PrintfW		(const WCHAR *, ...);	// return sizeforput (-1)
//
// ---------------------------------------------------------------------------
//
	Byte	    headB		(size_t o = 0) const { return ::getB (pHead, o);};

	int	    headChrA		(size_t o = 0) const { return	(int) headB (o);};

	_U16	    headW		(size_t o = 0) const { return ::getW (pHead, o);};

	int	    headChrW		(size_t o = 0) const { return   (int) headW (o);};

	_U16	    headLitEndianW	(size_t o = 0) const { return ::getLitEndianW (pHead, o);};

	_U16	    headBigEndianW	(size_t o = 0) const { return ::getBigEndianW (pHead, o);};

	_U32	    headD		(size_t o = 0) const { return ::getD (pHead, o);};

	_U32	    headLitEndianD	(size_t o = 0) const { return ::getLitEndianD (pHead, o);};

	_U32	    headBigEndianD	(size_t o = 0) const { return ::getBigEndianD (pHead, o);};
//
// ---------------------------------------------------------------------------
//
	void *	    get			(DWord n) { pHead += n; return pHead - n;};

	Byte	    getB		() { Byte b = headB (); pHead ++; return b;};

	int	    getChrA		() { return (int) getB ();};

	_U16	    getW		() { _U16 w = headW (); pHead += sizeof (_U16); return w;};

	int	    getChrW		() { return (int) getW ();};

	_U16	    getLitEndianW	() { _U16 w = headLitEndianW (); pHead += sizeof (_U16); return w;};

	_U16	    getBigEndianW	() { _U16 w = headBigEndianW (); pHead += sizeof (_U16); return w;};

	_U32	    getD		() { _U32 d = headD (); pHead += sizeof (_U32); return d;}; 

	_U32	    getLitEndianD	() { _U32 d = headLitEndianD (); pHead += sizeof (_U32); return d;}; 

	_U32	    getBigEndianD	() { _U32 d = headBigEndianD (); pHead += sizeof (_U32); return d;};

	void	    get			(void * p, DWord n) { memcpy (p, pHead, n); get (n);};

	int	    GetChrA		() { return ((sizeof (Byte) - 1) < (int) sizeforget ()) ? getChrA () : -1;};

	int	    GetChrW		() { return ((sizeof (Word) - 1) < (int) sizeforget ()) ? getChrW () : -1;}

	int	    ScanfA		(const char  * missStr, const char  * fmt, ...);

	int	    ScanfW		(const WCHAR * missStr, const WCHAR * fmt, ...);
//
// ---------------------------------------------------------------------------
//
    };

    template <size_t S> struct TStreamBuf : public GStreamBuf
    {
		    TStreamBuf () : GStreamBuf (pReserved, sizeof (pReserved)) {};

	Byte 	    pReserved [S - sizeof (GStreamBuf)];
    };
//
//[]------------------------------------------------------------------------[]

#endif//__G_STRDEFS_H











#if FALSE

//[]------------------------------------------------------------------------[]
//
#pragma pack (_M_PACK_LONG)

typedef enum SerialDataPackType_tag
{
    SDT_NULL			= 0x00,			// Unknown data type
    SDT_ARR			= 0x10,

    STD_U1			= 0x01,
    STD_U2			= 0x02,
    STD_U4			= 0x04,
};

struct GSPHeader
{
	Byte		vihl    ;	    // Version & header length'G0'
	Byte		aDstLength:4;	    // Roundup to DWord 0 - 0 1 - 4...
	Byte		aSrcLength:4;	    // Roundup to DWord 0 - 0 1 - 4...
	Word		wSize	    ;	    // Roundup to DWord
};

#pragma pack ()

#pragma pack (_M_PACK_LONG)

	typedef enum GDataType_tag
	{    
	    GDT_NULL			= 0x00,			// Unknown data type
	    GDT_ARR			= 0x20,
	    GDT_PTR			= 0x40,
	//
	//.....................................
	//
	    GDT_U1			= 0x01,
	    GDT_U2			= 0x02,
	    GDT_U4			= 0x04,
	    GDT_U8			= 0x08,
	    GDT_I1			= 0x11,
	    GDT_I2			= 0x12,
	    GDT_I4			= 0x14,
	    GDT_I8			= 0x18,

	    GDT_U16			= GDT_U2,
	    GDT_I16			= GDT_I2,

	    GDT_U32			= GDT_U4,
	    GDT_I32			= GDT_I4,
	//
	//.....................................
	//
	    GDT_GUID			= 0x0A,
	    GDT_STRA			= 0x0B,
	    GDT_STRW			= 0x0C,
	//				= 0x0D,
	//				= 0x0E,
	//				= 0x0F,

	    GDT_MSZSTRA			= (GDT_ARR | GDT_STRA),
	    GDT_MSZSTRW			= (GDT_ARR | GDT_STRW),
	//
	//.....................................
	//
	    GDT_DT_PACK			= 0x80,
	    GDT_ID_DWRD			= 0x84,
	    GDT_ID_PRPL			= 0x88,
	    GDT_ID_GUID			= 0x8A,
	    GDT_ID_STRA			= 0x8B,
	    GDT_ID_STRW			= 0x8C,
	//
	//.....................................
	//
	    GDT_VOID_PTR		= (GDT_PTR | GDT_NULL),
	    GDT_GUID_PTR		= (GDT_PTR | GDT_GUID),
	    GDT_STRA_PTR		= (GDT_PTR | GDT_STRA),
	    GDT_STRW_PTR		= (GDT_PTR | GDT_STRW),
	    GDT_MSZSTRA_PTR		= (GDT_PTR | GDT_MSZSTRA),
	    GDT_MSZSTRW_PTR		= (GDT_PTR | GDT_MSZSTRW),

	    GDT_DT_PACK_PTR		= (GDT_PTR | GDT_DT_PACK),
	    GDT_ID_DWRD_PTR		= (GDT_PTR | GDT_ID_DWRD),
	    GDT_ID_GUID_PTR		= (GDT_PTR | GDT_ID_GUID),
	    GDT_ID_STRA_PTR		= (GDT_PTR | GDT_ID_STRA),
	    GDT_ID_STRW_PTR		= (GDT_PTR | GDT_ID_STRW),

	} GDataType;

/*
#pragma pack (4)

template <int D = 0> struct TDataPack : public GDataPack
{
	DWord		    dPack [D + 1];
}

template <class T> struct TDataPack1 : public TDataPack <1>
{
};

template <class T, class T2> struct TDataPack1 : public TDataPack <2>
{
};

struct Io::Reply : public GDataPack
{
    DWord

}

#pragma pack (sizeof (void *))

class GObjectOwner

class GObjectHandle
{
	GTSLink <GObjectHandle, sizeof (void *)>    SLink;
	void *


    GErrorCode GAPI		Create	    (GObjectHnande &, LPTCHAR pName...);

    STD_METHOD_	    (GErrorCode,     Create)(GObjectHandle *, GDataPack &


};

    "{7882C4E4-1CDB-4745-9A05-3F495047AC78}\\Channel"
    "BlNetpass\\{7882C4E4-1CDB-4745-9A05-3F495047AC78}\0"
    "udp\\127.0.0.0.1:5001\0"
    "tcp\\127.0.0.0.1:5001\0/tstack="
    "udp\\ServerName:5001\0"
    "tcp\\ServerName:5001\0"
    "pipe\\ServerName"
    "mailslot\\ServiceName"
    

void SomeProc
{
    if (ERROR_SUCCESS == GClient::Create (& 

    GClient::Create (NULL, pName, True);


    m_hWinPipe = GWinPipePort::Create (

    GWinPipePort *  pWinPipePort = new GWinPipPort;

    if (NULL != (pWinPipePort = new GWinPipePort ()))
    {
    }
};



class GIo
{
public :

    GErrorCode GAPI	    Write   (void * pBuf, DWord dBufSize, DWord *);

    GErrorCode GAPI	    Read    (void * pBuf, DWord dBufSize, DWord *);

public :

    STD_METHOD_	    (void,    Write)(GIo::CompletionStatus &, void * pBuf     ,
							      DWord  dBufSize );

    STD_METHOD_	    (void,     Read)(GIo::CompletionStatus &, void * pBuf     ,
							      DWord  dBufSize );
};




class GIoHandle : public GObjectHandle
{
class GIo

};

class GIoStream : public GIoHandle
{

public :

    STD_METHOD_	    (void,
};



struct Io::Reply :

struct Io::ReadParam



#pragma pack ()


struct GStrAPtrGConstCharTDataPack <const char *>

struct GString : public TDataPack2 <char *>

struct GBuffer : public TDataPack <2, void *, DWord>


template <class T> TDataPack : public GDataPack

#pragma pack (push, 4
struct TDataPack <T> : public GDataPack
{
};

#define DECLARE_GDT_PACK(Class, GGUID)		/
template <class 


    Class 

DECLARE_GDT_PACK

    GData <const GUID *>    ClassId ;
    GData <int>		    Channel ;

struct 


    TDataPack <const char *> 
    {
	TData <int>	
    }


*/


    struct GDataPack
    {
    union				    // Intel	  order : s0,s1,s2,bt
    {					    // Big endian order : bt,s2,s1,s0
	struct
    {
	Byte		bPackSize [3];	    // Intel order pack size
	Byte		bPackType [1];	    // High byte packet type
    };
	DWord		dPackHeader  ;
    };
//	Byte		bPackData [bPackSize - sizeof (dPackHeader)];
//					    // Packet's data
// Public Methods --------------------
//
//
	GDataType	GetType		() const { return (GDataType) bPackType [0];};

	DWord		GetSize		() const { return (0x00FFFFFF & dPackHeader);};

	DWord		GetDataSize	() const { return GetSize () - sizeof (dPackHeader);};

	void *		GetDataPtr	() const { return (void *) (this + 1);};

	GDataPack *	Next		() const { return (GDataPack *) (((Byte *) this ) + GetSize ());};

	GDataPack *	GetFirst	() const { return (0x80 & GetType ()) ? (GDataPack *) GetDataPtr () : NULL;};

	GDataPack *	GetNext		(const GDataPack * pPack) const 
					     { return (pPack ->Next () < this ->Next ()) ? pPack ->Next () : NULL;};

	GDataPack *	GetIdPack	(GDataType     tId,
					 const void *  pId) const;

	void *	        GetIdDataPtr    (GDataType     tId, 
					 const void *  pId, DWord * pSize = NULL) const;

	void *		GetIdDataPtr	(const GUID  * pId, DWord * pSize) const
					     { return GetIdDataPtr (GDT_ID_GUID, (const void *) pId, pSize);};

	void *		GetIdDataPtr	(      DWORD   dId, DWord * pSize) const
					     { return GetIdDataPtr (GDT_ID_DWRD, (const void *) dId, pSize);};

	void *		GetIdDataPtr	(const char  * pId, DWord * pSize) const
					     { return GetIdDataPtr (GDT_ID_STRA, (const void *) pId, pSize);};

	void *		GetIdDataPtr	(const WCHAR * pId, DWord * pSize) const
					     { return GetIdDataPtr (GDT_ID_STRW, (const void *) pId, pSize);};
//
// Public Methods --------------------
};

    typedef GDataPack	GDataId;

//
//[]------------------------------------------------------------------------[]
//
//  0xttLn(dd)	: 0xtt,0xL0,0xL1,0xL2{,b[0],b[1],...b[0x00L2L1L0 - 4])
//							: 4 pack type &  Intel order pack size 
//							:   0xtt,L0,L1,L2,b0,b1,...b[0x00L2L1L0 - 4]
//  **		: Any data
//
//  &&		: Any {GDT_ID_DWRD | GDT_ID_GUID | GDT_ID_STRA | GDT_ID_STRW} data
//
//  Dt(**)	: 0x**Ln(**)
//
//  Id(&&)	: 0x&&Ln(&&,**)
//
//[]------------------------------------------------------------------------[]

#pragma pack ()

#endif