//[]------------------------------------------------------------------------[]
// Single & Double link base declaration
//
//
// Created by GSh.
//[]------------------------------------------------------------------------[]

#ifndef __G_STDLINK_H
#define __G_STDLINK_H

//[]------------------------------------------------------------------------[]
// GSLink, GDLink declaration
//
//
class GSLink
{
friend 
class GDLink;

public :

			GSLink () : m_link (NULL) {};
protected :

	void *		_get	     () const	{ return m_link;    };
	void		_set	     (void * n)	{	 m_link = n;};

	void *		_get_and_set (void * n)	{ void * t = _get ( );
							     _set (n);
						  return t;	    };
private :

	void *		m_link;
};

class GDLink
{
public :
			GDLink () : m_link_next (), m_link_prev () {};
protected :

	void *		_get_next   () const { return m_link_next._get ();};
	void *		_get_prev   () const { return m_link_prev._get ();};

	void		_set_next   (void * n) { m_link_next._set (n);};
	void		_set_prev   (void * n) { m_link_prev._set (n);};

	void *		_get_and_set_next (void * n)
				    { return m_link_next._get_and_set (n);};
	void *		_get_and_set_prev (void * n)
				    { return m_link_prev._get_and_set (n);};
private :

	GSLink		m_link_next;
	GSLink		m_link_prev;
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
// TSLink declaration
//
//
template <class T, class L>
class TXList;

template <class T, int I>
class TSLink : private GSLink
{
friend 
class TXList <T, TSLink <T, I> >;

public :

	void		init	    () { unlink ();};

	void		unlink	    () { _set (NULL);};

	Bool		linked	    () const { return NULL != next ();};
	T *		next	    () const { return _get (); };
	T *		prev	    () const;

private :
//
// {this ->...}; this ->_link (); {this ->this};
//
	void		_link	    ()	    { _set (_t(this));};
//
// {this ->this + 1}; this ->_link (t); {this -> t -> this + 1};
//
	void		_link	    (T * t) {
					      _l (t) ->_set (_get ( ));
							     _set (t) ;
					    };
//
// {this ->this + 1 ->this + 2}; this ->_linkex (); { this ->this + 2};
// return  this + 1;
//
	T *		_linkex	    ()	    {
					      T *    t  = _get (		    );
							  _set (_l (t) ->_get (    ));
							        _l (t) ->_set (NULL) ;
					      return t;
					    };

//	void		_linkup (T * t)	    { _set (	   t);};

static	T *		_t	    (const TSLink <T, I> * _l)
					    { return (T		    *) ((Byte *) _l - I);};

static	TSLink <T, I> *	_l	    (const T * _t)
					    { return (TSLink <T, I> *) ((Byte *) _t + I);};

	T *		_get () const { return (T *) GSLink::_get (); };

	void		_set (T * n) { GSLink::_set (n); };
};
//
//[]------------------------------------------------------------------------[]
//
template <class T, int I>
GFORCEINLINE T * TSLink <T, I>::prev () const
{
    T * i = _t(this);

    if (next () != NULL)
    {
	while (_l(i) ->next () != _t(this))
	{
	   i = _l(i) ->next ();
	}
    }

    return i;
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
// GTDLink declaration
//
//
template <class T, int I>
class TDLink : private GDLink
{
friend 
class TXList <T, TDLink <T, I> >;

public :

	void		init	    () { unlink ();};

	void		unlink	    () { _set_next (NULL);
				         _set_prev (NULL);};

	T *		next	    () const { return _get_next (); };
	T *		prev	    () const { return _get_prev (); };

private :
//
// {this ->...}; this ->_link (); {this ->this};
//
	void		_link	    ()	     { _set_next (_t (this));
					       _set_prev (_t (this));};
//
// {this ->this + 1}; this ->_link (t); {this -> t -> this + 1};
//
	void		_link	    (T * t)  {
					       _l(t) ->_set_next (_get_and_set_next (t));
					       _l(t) ->_set_prev (_l
								 (_l(t) ->_get_next ( )) ->
								  _get_and_set_prev (t));
					     };
//
// {this ->this + 1 ->this + 2}; this ->_linkex (); { this ->this + 2};
// return  this + 1;
//
	T *		_linkex	    ()	     {
					       T * t = _get_next ();
						       _set_next (_l (t) ->_get_next ());
								  _l (	   _get_next ()) ->_set_prev (_t (this));

					       _l (t) ->_set_next (NULL);
					       _l (t) ->_set_prev (NULL);

					       return t;
					     };

static	T *		_t	    (const  TDLink <T, I> * _l)
					     { return (T *) ((Byte *) _l - I); };
static	TDLink <T, I> *	_l	    (const T * _t)
					     { return (TDLink <T, I> *) ((Byte *) _t + I); };

	T *		_get_next   () const { return (T *) GDLink::_get_next (); };

	T *	        _get_prev   () const { return (T *) GDLink::_get_prev (); };

	void	        _set_next   (T * t)	    { GDLink::_set_next (t);};

	void	        _set_prev   (T * t)	    { GDLink::_set_prev (t);};

	T *	        _get_and_set_next   (T * t) { return (T *) GDLink::_get_and_set_next (t);};

	T *	        _get_and_set_prev   (T * t) { return (T *) GDLink::_get_and_set_prev (t);};
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
// TXList declaration
//
#ifndef GLISTCALLBACK
#define	GLISTCALLBACK	GFASTCALL
#endif

template <class T>
class TXList_Base;

template <class T>
class TXList_Iterator
{
friend
class TXList_Base <T>;

public :
			TXList_Iterator () {};

	T *		next	() const { return (T *) m_next;};
	T *		prev	() const { return (T *) m_prev;};

	TXList_Iterator &   operator   = (const TXList_Iterator & I) { m_prev = I.m_prev; 
								       m_next = I.m_next; return * this;};

	TXList_Iterator &   operator <<= (T * next) { m_prev = m_next; m_next = next; return * this;};
	TXList_Iterator &   operator >>= (T * prev) { m_next = m_prev; m_prev = prev; return * this;};

private :

			TXList_Iterator (T * prev, T * next) { m_prev = prev;
							       m_next = next;};
	void *		m_prev;
	void *		m_next;
};

template <class T>
class TXList_Base
{
protected :

static	TXList_Iterator <T> get_Iterator    (T * prev, T * next) { return TXList_Iterator <T> (prev, next);};

static	T *		set_next	    (TXList_Iterator <T> & I, T * next) { return (T *)(I.m_next = next);};
static	T *		set_prev	    (TXList_Iterator <T> & I, T * prev) { return (T *)(I.m_prev = prev);};
};

template <class T, class L>
class TXList : public TXList_Base <T>
{
public :

    typedef Bool (GLISTCALLBACK	   * GTLCALLBACK	 )(T *, void *);
    typedef Bool (GLISTCALLBACK T::* GTLCALLBACKPMF	 )(	void *);
    typedef Bool (GLISTCALLBACK T::* GTLCALLBACKPMF_CONST)(	void *) const;

public :
			TXList (T * l = NULL) { init (l);};

	void		init		(T * l = NULL) { m_last = l;};

	Bool		empty		() const { return NULL == last ();};

	T *		last		() const;
	T *		first		() const;

	T *		getnext		(const T *) const;
	T *		getprev		(const T *) const;

	TXList_Iterator <T> head	() const { return get_Iterator (NULL, first ());};

	TXList_Iterator <T> tail	() const { return get_Iterator (last (),  NULL);};

	T *		contains	(const T *) const;

	T *		append		(T *);
	T *		insert		(T *);
	T *		insert		(TXList_Iterator <T> &, T *);

	T *		extract		(T *);
	T *		extract_first	();
	T *		extract_last	();
	T *		extract		(TXList_Iterator <T> &);

// For remove <<<
//
	T *		append		(T * p, T *);
	Bool		append		(TXList <T, L> &);
	Bool		append		(TXList <T, L> &, GTLCALLBACK,		void *);
	Bool		append		(TXList <T, L> &, GTLCALLBACKPMF,	void *);
	Bool		append		(TXList <T, L> &, GTLCALLBACKPMF_CONST, void *);

	T *		insert		(T *, T * n);
	T *		insert		(T *, GTLCALLBACK	  );
	T *		insert		(T *, GTLCALLBACKPMF_CONST);

	T *	        extract		(T * p, T *);
	T *		extract		(GTLCALLBACK,	       void *);
	T *		extract		(GTLCALLBACKPMF_CONST, void *);

	Bool		anyfor		(GTLCALLBACK,	       void *) const;
	Bool		anyfor		(GTLCALLBACKPMF_CONST, void *) const;

	Bool		foreach		(GTLCALLBACK,	       void *) const;
	Bool		foreach		(GTLCALLBACKPMF,       void *) const;

	T *		findfor		(GTLCALLBACK,	       void *) const;
	T *		findfor		(GTLCALLBACKPMF,       void *) const;
	T *		findfor		(GTLCALLBACKPMF_CONST, void *) const;
//
// For remove >>>
private :

	T *		_first		() const;

private :

	T *		m_last;
};
//
//[]------------------------------------------------------------------------[]
//
template <class T, class L> GFORCEINLINE
T * TXList <T, L>::last () const
{
    return m_last;
};

template <class T, class L> GFORCEINLINE
T * TXList <T, L>::_first () const
{
    return L::_l (last ()) ->next ();
};

template <class T, class L> GFORCEINLINE
T * TXList <T, L>::first () const
{
    return (last ()) ? _first () : NULL;
};

template <class T, class L> GFORCEINLINE
T * TXList <T, L>::getnext (const T * t) const
{
    return (t != last ()) ? L::_l(t) ->next () : NULL;
};

template <class T, class L> GFORCEINLINE
T * TXList <T, L>::getprev (const T * t) const
{
    T * p = L::_l(t) ->prev ();

    return (p != last ()) ? p : NULL;
};

template <class T, class L> GFORCEINLINE
T * TXList <T, L>::contains (const T * t) const
{
    T * n, * i;

    if ((i = last ()) != NULL)
    {
	do
	{
	    if  ((n = L::_l(i) ->next ()) == t)
	    {
		return i;
	    }

	} while ((i = n) != last ());

	i = NULL;
    }
    return  i;
};
//
//[]------------------------------------------------------------------------[]
//
template <class T, class L> GFORCEINLINE
T * TXList <T, L>::append (T * t)
{
    if (last ())
    {
	L::_l(   last ()) ->_link (t);
    }
    else
    {
	L::_l(	       t) ->_link ( );
    }
    return  m_last = t;
};

template <class T, class L> GFORCEINLINE
T * TXList <T, L>::insert (T * t)
{
    if (last ())
    {
	L::_l(   last ()) ->_link (t);
    }
    else
    {
	L::_l(m_last = t) ->_link ( );
    }
    return  t;
};

template <class T, class L> GFORCEINLINE
T * TXList <T, L>::insert (TXList_Iterator <T> & I, T * t)
{
    T * p = (I.prev ()) ? I.prev () : last ();

    if (p)
    {
	L::_l(	       p) ->_link (t);
    }
    else
    {
	L::_l(	       t) ->_link ( );
    }
    if (NULL == I.next ())
    {
	    m_last   = t;
    }
    return  set_next (I, t);
};
//
//[]------------------------------------------------------------------------[]
//
/*
template <class T, class L> GFORCEINLINE
T * TXList <T, L>::extract (T * p, T * t)
{
    {
	L::_l(p) ->_linkex ();

	if (t == last ())
	{
	    m_last = (p != t) ? p : NULL;
	}
    }

    return  t;
};
*/
template <class T, class L> GFORCEINLINE
T * TXList <T, L>::extract (T * t)
{
    T * p;

    if ((p = contains (t)) != NULL)
    {
	L::_l(p) ->_linkex ();

	if (t == last ())
	{
	    m_last = (p != t) ? p : NULL;
	}

	return t;
    }

    return  NULL;
};

template <class T, class L> GFORCEINLINE
T * TXList <T, L>::extract_first ()
{
    T * t;

    if ((t = first ()) != NULL)
    {
	L::_l(last ()) ->_linkex ();

	if (t == last ())
	{
	    m_last = NULL;
	}
    }

    return  t;
};

template <class T, class L> GFORCEINLINE
T * TXList <T, L>::extract_last ()
{
    T * p, * t;

    if ((t = last ()) != NULL)
    {
	L::_l(p = L::_l(t) ->prev ()) ->_linkex ();

//	if (t == last ())
	{
	    m_last = (p != t) ? p : NULL;
	}
    }

    return  t;
};

template <class T, class L> GFORCEINLINE
T * TXList <T, L>::extract (TXList_Iterator <T> & I)
{
    T * p, * t;

    p = (I.prev ()) ? I.prev () : last ();
    t =  I.next ();

    if (p && t)
    {
	set_next (I, getnext (t));

	L::_l(p) ->_linkex ();

	if (t == last ())
	{
	    m_last = (p != t) ? p : (p = NULL);
	}
    }

    return  t;
};
//
//[]------------------------------------------------------------------------[]
//
template <class T, class L> GFORCEINLINE
T * TXList <T, L>::append (T * p, T * t)
{
    if (p)
    {
	L::_l(	    p) ->_link (t);

	if   (	    p !=  last ( ))
	{
	    return t;
	} 
    }
    else
    {
	if (last ())
	{
	    L::_l(last ()) ->_link (t);
	}
	else
	{
	    L::_l(	t) ->_link ( );
	}
    }
    return  m_last = t;
};

template <class T, class L> GFORCEINLINE
Bool TXList <T, L>::append (TXList <T, L> & src)
{
    T * n, * i;

    if ((i =  src.first  ()) != NULL)
    {
	L::_l(src  .last ()) ->unlink ();
	      src.m_last     = NULL	;

	do
	{
	    n = L::_l(i) ->next ();

	    if (m_last)
	    {
		L::_l(m_last) ->_link (i);
	    }
	    else
	    {
		L::_l(	   i) ->_link ( );
	    }
		m_last = i;

	} while ((i = n) != NULL);

	return True ;
    }
	return False;
};

template <class T, class L> GFORCEINLINE
Bool TXList <T, L>::append (TXList <T, L> & src, GTLCALLBACK pf, void * context)
{
    T *	n, * i, ** r; Bool bRes = False;

    if ((i =  src.first  ()) != NULL)
    {
	L::_l(src  .last ()) ->unlink ();
	      src.m_last     = NULL	;
	do
	{
	    n = L::_l(i) ->next ();

	    r = ((pf) (i, context)) ?		     & src.m_last 
				    : (bRes |= True, &     m_last);
	    if (* r)
	    {
		L::_l(* r) ->_link (i);
	    }
	    else
	    {
		L::_l(  i) ->_link ( );
	    }
		* r = i;
    
	} while ((i = n) != NULL);
    }

    return  bRes;
};

template <class T, class L> GFORCEINLINE
Bool TXList <T, L>::append (TXList <T, L> & src, GTLCALLBACKPMF pmf, void * context)
{
    T *	n, * i, ** r; Bool bRes = False;

    if ((i =  src.first  ()) != NULL)
    {
	L::_l(src  .last ()) ->unlink ();
	      src.m_last     = NULL     ;
	do
	{
	    n = L::_l(i) ->next ();

	    r = ((i ->* pmf) (context)) ?		 & src.m_last 
					: (bRes |= True, &     m_last);
	    if (* r)
	    {
		L::_l(* r) ->_link (i);
	    }
	    else
	    {
		L::_l(  i) ->_link ( );
	    }
		* r = i;
    
	} while ((i = n) != NULL);
    }

    return  bRes;
};

template <class T, class L> GFORCEINLINE
Bool TXList <T, L>::append (TXList <T, L> & src, GTLCALLBACKPMF_CONST pmf, void * context)
{
    T *	n, * i, ** r; Bool bRes = False;

    if ((i =  src.first  ()) != NULL)
    {
	L::_l(src  .last ()) ->unlink ();
	      src.m_last     = NULL     ;
	do
	{
	    n = L::_l(i) ->next ();

	    r = ((i ->* pmf) (context)) ?		 & src.m_last 
					: (bRes |= True, &     m_last);
	    if (* r)
	    {
		L::_l(* r) ->_link (i);
	    }
	    else
	    {
		L::_l(  i) ->_link ( );
	    }
		* r = i;
    
	} while ((i = n) != NULL);
    }

    return  bRes;
};
//
//[]------------------------------------------------------------------------[]
//
template <class T, class L> GFORCEINLINE
T * TXList <T, L>::insert (T * t, T * n)
{
    T * i;

    if ((i = last ()) != NULL)
    {
	do
	{
	    if (n == L::_l(i) ->next ())
	    {
		L::_l(i) ->_link (t);

		return t;
	    }

	} while ((i = L::_l(i) ->next ()) != last ());

	L::_l(i) ->_link (t);
    }
    else
    {
	L::_l(t) ->_link ( );
    }

    return  m_last = t;
};

template <class T, class L> GFORCEINLINE
T * TXList <T, L>::insert (T * t, GTLCALLBACK pf)
{    
    T * i;

    if ((i = last ()) != NULL)
    {
	do
	{
	    if (! pf (L::_l(i) ->next (), t))
	    {
		L::_l(i) ->_link (t);

		return t;
	    }

	} while ((i = L::_l(i) ->next ()) != last ());

	L::_l(i) ->_link (t);
    }
    else
    {
	L::_l(t) ->_link ( );
    }

    return  m_last = t;
};

template <class T, class L> GFORCEINLINE
T * TXList <T, L>::insert (T * t, GTLCALLBACKPMF_CONST pmf)
{    
    T * i;

    if ((i = last ()) != NULL)
    {
	do
	{
	    if (!(L::_l(i) ->next () ->* pmf)(t))
	    {
		L::_l(i) ->_link (t);

		return t;
	    }

	} while ((i = L::_l(i) ->next ()) != last ());

	L::_l(i) ->_link (t);
    }
    else
    {
	L::_l(t) ->_link ( );
    }

    return  m_last = t;
};
//
//[]------------------------------------------------------------------------[]
//
template <class T, class L> GFORCEINLINE
T * TXList <T, L>::extract (GTLCALLBACK pf, void * context)
{
    T * p, * t;

    if ((p = last ()) != NULL)
    {
	do
	{
	    if (! pf ((t = L::_l(p) ->next ()), context))
	    {
		return extract  (p, t);
	    }

	} while ((p = t) != last ());
    }

    return NULL;
};

template <class T, class L> GFORCEINLINE
T * TXList <T, L>::extract (GTLCALLBACKPMF_CONST pmf, void * context)
{
    T * p, * t;

    if ((p = last ()) != NULL)
    {
	do
	{
	    if (!((t = L::_l(p) ->next ()) ->* pmf)(context))
	    {
		return extract (p, t);
	    }

	} while ((p = t) != last ());
    }

    return NULL;
};
//
//[]------------------------------------------------------------------------[]
//
template <class T, class L> GFORCEINLINE
Bool TXList <T, L>::anyfor (GTLCALLBACK pf, void * context) const
{
    T * p, * t;

    if ((p = last ()) != NULL)
    {
	do
	{
	    if (! pf ((t = L::_l(p) ->next ()), context))
	    {
		return True;
	    }

	} while ((p = t) != last ());
    }

    return False;
};

template <class T, class L> GFORCEINLINE
Bool TXList <T, L>::anyfor (GTLCALLBACKPMF_CONST pmf, void * context) const
{
    T * p, * t;

    if ((p = last ()) != NULL)
    {
	do
	{
	    if (!((t = L::_l(p) ->next ()) ->* pmf)(context))
	    {
		return True;
	    }

	} while ((p = t) != last ());
    }

    return False;
};
//
//[]------------------------------------------------------------------------[]
//
template <class T, class L> GFORCEINLINE
Bool TXList <T, L>::foreach (GTLCALLBACK pf, void * context) const
{
    T *	p, * i; Bool bRes = True;

    if ((p = last ()) != NULL)
    {
	do
	{
	    bRes &= (pf (i = L::_l(p) ->next (), context));

	} while ((p = i) != last ());
    }

    return bRes;
};

template <class T, class L> GFORCEINLINE
Bool TXList <T, L>::foreach (GTLCALLBACKPMF pmf, void * context) const
{
    T *	p, * i; Bool bRes = True;

    if ((p = last ()) != NULL)
    {
	do
	{
	    bRes &= ((i = L::_l(p) ->next ()) ->* pmf)(context);

	} while ((p = i) != last ());
    }

    return bRes;
};
//
//[]------------------------------------------------------------------------[]
//
template <class T, class L> GFORCEINLINE
T * TXList <T, L>::findfor (GTLCALLBACK pf, void * context) const
{
    T * p, * t;

    if ((p = last ()) != NULL)
    {
	do
	{
	    if (! pf ((t = L::_l(p) ->next ()), context))
	    {
		return t;
	    }

	} while ((p = t) != last ());
    }

    return NULL;
};

template <class T, class L> GFORCEINLINE
T * TXList <T, L>::findfor (GTLCALLBACKPMF pmf, void * context) const
{
    T * p, * t;

    if ((p = last ()) != NULL)
    {
	do
	{
	    if (!((t = L::_l(p) ->next ()) ->* pmf)(context))
	    {
		return t;
	    }

	} while ((p = t) != last ());
    }

    return NULL;
};

template <class T, class L> GFORCEINLINE
T * TXList <T, L>::findfor (GTLCALLBACKPMF_CONST pmf, void * context) const
{
    T * p, * t;

    if ((p = last ()) != NULL)
    {
	do
	{
	    if (!((t = L::_l(p) ->next ()) ->* pmf)(context))
	    {
		return t;
	    }

	} while ((p = t) != last ());
    }

    return NULL;
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
/*
template <class T, class L> GFORCEINLINE
void TXList_Iterator <T, L>::headof (TXList <T, L> & l)
{
};

template <class T, class L> GFORCEINLINE
void TXList_Iterator <T, L>::tailof (TXList <T, L> & l)
{
};

template <class T, class L> GFORCEINLINE
T * TXList_Iterator <T, L>::operator ++ ()
{
    return NULL;
};

template <class T, class L> GFORCEINLINE
T * TXList_Iterator <T, L>::operator ++ (int)
{
    return NULL;
};

template <class T, class L> GFORCEINLINE
T * TXList_Iterator <T, L>::operator -- ()
{
    return NULL;
};

template <class T, class L> GFORCEINLINE
T * TXList_Iterator <T, L>::operator -- (int)
{
    return NULL;
};

template <class T, class L> GFORCEINLINE
T * TXList_Iterator <T, L>::insert (T * t)
{
    if (prev ())
    {
	I.nextptr = L::_l(prev ()) ->_link (t);
    }
    else
    {
    }

    return NULL;
};

*/
//[]------------------------------------------------------------------------[]


//[]------------------------------------------------------------------------[]
// GTXAllocator declaration
//
//
template <class T, class L>
class TXListAllocator {

public :
			TXListAllocator	()		    { m_top  =  NULL;};
			TXListAllocator	(int N, void * ptr) { init (N, ptr) ;};

	void		init		(int N, int S, void *);
	void		init		(int N,	       void *);

static	DWord		getextensionsize	
					(int N)	     { return N * sizeof (T);};

	T *		alloc		();
	void		free		(T		* t);
	void		free		(TXList <T, L>	& l);

private :

	T *		    m_top;
};

template <class T, class L> GFORCEINLINE
void TXListAllocator <T, L>::init (int N, int S, void * TBuf)
{
    T * i;

    if (N)
    {
	i = (T *) & (((Byte *) TBuf) [S * (-- N)]);

	    L::_l(i		         ) ->_linkup (NULL);

	for (; i != TBuf; i = (T *)(((Byte *) i) - S))
	{
	    L::_l((T *)(((Byte *) i) - S)) ->_linkup (i	);
	}
    }
    else
    {
	i  = NULL;
    }
	m_top = i;
};

template <class T, class L> GFORCEINLINE
void TXListAllocator <T, L>::init (int N, void * TBuf)
{
    T *	i;

    if (N)
    {
	i = & (((T *) TBuf) [-- N]);

	    L::_l(i    ) ->_linkup (NULL);

	for (; i != TBuf; i --)
	{
	    L::_l(i - 1) ->_linkup (i	);
	}
    }
    else
    {
	i  = NULL;
    }
	m_top = i;
};

template <class T, class L> GFORCEINLINE
T * TXListAllocator <T, L>::alloc ()
{
    T *	t;

    if (NULL != (t = m_top))
    {
	m_top = L::_l(t) -> next   ();
		L::_l(t) ->_unlink ();
    }
    return t;
};

template <class T, class L> GFORCEINLINE
void TXListAllocator <T, L>::free (T * t)
{
    if (t)
    {
	L::_l(t) ->_linkup (m_top);
			    m_top = t;
    }
};

template <class T, class L> GFORCEINLINE
void TXListAllocator <T, L>::free (TXList <T, L> & l)
{
    if (l.last ())
    {
	T * t = l.first ();

	L::_l(l.last ()) ->_linkup (m_top);
				    m_top = t;
    }
};
//
//[]------------------------------------------------------------------------[]

#endif//__G_STDLINK_H





/*
	T *		first	    (		GXList::Iterator &) const;
	T *		next	    (const T *, GXList::Iterator &) const;

	T *		extract	    (		GXList::Iterator &);
	T *		insert	    (	   T *, GXList::Iterator &);
	T *		append	    (	   T *, GXList::Iterator &);
*/

/*
template <class T, class L> GFORCEINLINE
T * TXList <T, L>::first (GXList::Iterator & I) const
{
    return (NULL != (I.m_prev = last ())) ? _first () : NULL;
};

template <class T, class L> GFORCEINLINE
T * TXList <T, L>::next  (const T * i, GXList::Iterator & I) const
{
// GASSERT (I.m_prev != NULL) must be external condition !!!
//
    if (i)
    {
	if (I.m_prev != i)
	{
	    I.m_prev  = i;

	    i = L::_l ((T *) I.m_prev) ->next ();
	}
	else
	{
	    I.m_prev  = i = NULL;
	}
    }
    else
    {
	    i = L::_l ((T *) I.m_prev) ->next ();
    }

    return (T *) i;
};



*/

/*
template <class T, class L> GFORCEINLINE
T * TXList <T, L>::contains (int c) const
{
    T * i;

    if ((i = last ()) != NULL)
    {
	do
	{
	    if (0 == c -- )
	    {
		return i;
	    }

	} while ((i = L::_l(i) ->next ()) != last ());

	i = NULL;
    }
    return  i;
};
*/

/*
template <class T, class L> GFORCEINLINE
T * TXList <T, L>::extract (int c)
{
    T * p, * t;

    if ((p = contains (c)) != NULL)
    {
	if ((t = L::_l(p) ->_linkex ()) == last ())
	{
	    m_last = (p != t) ? p : NULL;
	}
    return  t;
    }

    return  NULL;
};
*/
