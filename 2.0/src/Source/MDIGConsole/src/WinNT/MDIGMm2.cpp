//[]------------------------------------------------------------------------[]
// MDIGConsole CMM2Panel dialog defintion
//
//
// Created by GSh.
//[]------------------------------------------------------------------------[]

#include "MDIGUtils.h"

//[]------------------------------------------------------------------------[]
//
class CMM2Panel : public GDialog
{
public :
		CMM2Panel () : GDialog (NULL)
		{
		    m_uRegStatus    = 0x0000;
		    m_uRegMove	    = 0x0000;
		    m_uRegPress	    = 0x0000;

		    m_pIRRPMM2	    = NULL  ;
		    m_hIRRPCallBack = NULL  ;
		};

protected :

	Bool		PreprocMessage		(MSG *);

	Bool		On_RadMode		(GWinMsg &, int);
	Bool		On_RunMove		(GWinMsg &, int);

	Bool		On_Edit			(GWinMsg &);

	Bool		On_DlgMsg		(GWinMsg &);

	Bool		On_InitDialog		(GWinMsg &);

	void		On_Destroy		();
	Bool		On_Create		(void *);

protected :

	void		On_IRRPParam		(_U32, _U32);
	void		On_IRRPCallBack		(IRRPCallBack::Handle *);

protected :

	CLedIndicator		m_oLedOnline	;

	CLedIndicator		m_oLedRotL	;
	CLedIndicator		m_oLedRotR	;
	CLedIndicator		m_oLedRotLPin	;
	CLedIndicator		m_oLedRotL45Pin	;
	CLedIndicator		m_oLedRotMPin	;
	CLedIndicator		m_oLedRotR45Pin ;
	CLedIndicator		m_oLedRotRPin	;

	CLedIndicator		m_oLedUpPin	;
	CLedIndicator		m_oLedDnPin	;
	CLedIndicator		m_oLedMoveUp	;
	CLedIndicator		m_oLedMoveDn	;

	CLedIndicator		m_oLedPressUpPin;
	CLedIndicator		m_oLedPressDnPin;

	_U32			m_uRegStatus	;
	_U32			m_uRegMove	;
	_U32			m_uRegPress	;

	IRRPMM2 *		m_pIRRPMM2	;

	CWndCallBack		m_sIRRPCallBack ;
	IRRPCallBack::Handle *	m_hIRRPCallBack	;

	CEditHandler		m_hEdit		;
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
void CMM2Panel::On_Destroy ()
{
    if (m_pIRRPMM2)
    {
    if (m_hIRRPCallBack)
    {
	m_hIRRPCallBack ->Close ();
    }

	m_pIRRPMM2 ->Release ();
	m_pIRRPMM2 = NULL;
    }
};

Bool CMM2Panel::On_InitDialog (GWinMsg & m)
{
    return m.DispatchCompleteDlgEx (False);
};

Bool CMM2Panel::PreprocMessage (MSG * msg)
{
    m_hEdit.PreprocMessage (msg);

    return GDialog::PreprocMessage (msg);
};

Bool CMM2Panel::On_Create (void * pParam)
{
    if (! GDialog::On_Create (pParam))
    {	return False;}

    m_oLedOnline.SetColor (NULL, CLedIndicator::Red);
    m_oLedOnline.TurnOn   (NULL, True);

    if (s_pIRRPService)
    {
    if (ERROR_SUCCESS == s_pIRRPService ->CreateRRPDevice 
				((void **) & m_pIRRPMM2	    ,
				  IID_IUnknown		    ,
				 (void  *) 0x91000000	    ,
				  IID_IUnknown		    ))
    {
	m_sIRRPCallBack.SetRecvHWND (GetHWND ());

	m_pIRRPMM2 ->Connect  (m_hIRRPCallBack, 
			     & m_sIRRPCallBack);
    }
    }
	return True ;
};

Bool CMM2Panel::On_Edit (GWinMsg & m)
{
    int		nValue	 ;
    int		nCtrlId	 ;
    TCHAR	sBuf [64];

    GetCtrlText (nCtrlId = LOWORD (m.wParam), sBuf, sizeof (sBuf) / sizeof (TCHAR));

    switch (nCtrlId)
    {
    case IDC_MM2_PRESSLIM_EDT	    :
	sscanf (sBuf, "%d", & nValue);
	RRPSetParam (m_pIRRPMM2, IRRPMM2::ID_RegSetPressHigh, nValue);
	return True;
    }
	return False;
};

Bool CMM2Panel::On_RadMode (GWinMsg & m, int iIndex)
{
    RRPSetParam (m_pIRRPMM2, IRRPMM2::ID_SetPressMode, (0 == iIndex) ? 0x00 : 0x1);

    return True;
};

Bool CMM2Panel::On_RunMove (GWinMsg & m, int nDirPos)
{
    RRPSetParam (m_pIRRPMM2, IRRPMM2::ID_RunMove, nDirPos);

    return True;
};

Bool CMM2Panel::On_DlgMsg (GWinMsg & m)
{
    if (m_hIRRPCallBack && (CWndCallBack::RRPMSG == m.uMsg ))
    {
	On_IRRPCallBack	  ((IRRPCallBack::Handle *) m.lParam);

	return m.DispatchComplete ();
    }

    int		nCtrlId;

    if (m_hEdit.On_ReflectWinMsg (m))
    {
	return	On_Edit (m);
    }

    if (0 !=   (nCtrlId = IsControlNotify (m)))
    {
	if (WM_DRAWITEM == m.uMsg)
	{
	switch (nCtrlId)
	{
	case IDC_MM2_ONLINE_LED  :
	    return (m_oLedOnline.On_Draw (m.pDRAWITEMSTRUCT), True);

	case IDC_MM2_ROTL_LED :
	    return (m_oLedRotL	.On_Draw (m.pDRAWITEMSTRUCT), True);
	case IDC_MM2_ROTR_LED :
	    return (m_oLedRotR	.On_Draw (m.pDRAWITEMSTRUCT), True);

	case IDC_MM2_ROTLPIN_LED :
	    return (m_oLedRotLPin  .On_Draw (m.pDRAWITEMSTRUCT), True);
	case IDC_MM2_ROTL45PIN_LED :
	    return (m_oLedRotL45Pin.On_Draw (m.pDRAWITEMSTRUCT), True);
	case IDC_MM2_ROTMPIN_LED :
	    return (m_oLedRotMPin  .On_Draw (m.pDRAWITEMSTRUCT), True);
	case IDC_MM2_ROTR45PIN_LED :
	    return (m_oLedRotR45Pin.On_Draw (m.pDRAWITEMSTRUCT), True);
	case IDC_MM2_ROTRPIN_LED :
	    return (m_oLedRotRPin  .On_Draw (m.pDRAWITEMSTRUCT), True);

	case IDC_MM2_MOVEUP_LED :
	    return (m_oLedMoveUp.On_Draw (m.pDRAWITEMSTRUCT), True);
	case IDC_MM2_MOVEDN_LED :
	    return (m_oLedMoveDn.On_Draw (m.pDRAWITEMSTRUCT), True);
	case IDC_MM2_UPPIN_LED :
	    return (m_oLedUpPin .On_Draw (m.pDRAWITEMSTRUCT), True);
	case IDC_MM2_DNPIN_LED :
	    return (m_oLedDnPin .On_Draw (m.pDRAWITEMSTRUCT), True);

	case IDC_MM2_PRESSUPPIN_LED    :
	    return (m_oLedPressUpPin.On_Draw (m.pDRAWITEMSTRUCT), True);
	case IDC_MM2_PRESSDNPIN_LED    :
	    return (m_oLedPressDnPin.On_Draw (m.pDRAWITEMSTRUCT), True);
	};
	}

	if (WM_COMMAND == m.uMsg)
	{
	switch (nCtrlId)
	{
	case IDC_MM2_ROTLPIN_RAD    :
	    return (RRPSetParam (m_pIRRPMM2, IRRPMM2::ID_RunRotateFix, IRRPMM2::RotLt90), True);
	case IDC_MM2_ROTL45PIN_RAD  :
	    return (RRPSetParam (m_pIRRPMM2, IRRPMM2::ID_RunRotateFix, IRRPMM2::RotLt45), True);
	case IDC_MM2_ROTMPIN_RAD    :
	    return (RRPSetParam (m_pIRRPMM2, IRRPMM2::ID_RunRotateFix, IRRPMM2::RotMd  ), True);
	case IDC_MM2_ROTR45PIN_RAD  :
	    return (RRPSetParam (m_pIRRPMM2, IRRPMM2::ID_RunRotateFix, IRRPMM2::RotRt45), True);
	case IDC_MM2_ROTRPIN_RAD    :
	    return (RRPSetParam (m_pIRRPMM2, IRRPMM2::ID_RunRotateFix, IRRPMM2::RotRt90), True);

	case IDC_MM2_ROTL_BTN	    : 
	    return (RRPSetParam (m_pIRRPMM2, IRRPMM2::ID_RunRotate, INT_MIN), True);
	case IDC_MM2_ROTSTOP_BTN    :
	    return (RRPSetParam (m_pIRRPMM2, IRRPMM2::ID_RunRotate,	  0), True);
	case IDC_MM2_ROTR_BTN	    :
	    return (RRPSetParam (m_pIRRPMM2, IRRPMM2::ID_RunRotate, INT_MAX), True);

	case IDC_MM2_PRESSMODE0_RAD : return On_RadMode	  (m, 0);
	case IDC_MM2_PRESSMODE1_RAD : return On_RadMode	  (m, 1);
	case IDC_MM2_MOVEUP_BTN	    : return On_RunMove	  (m, INT_MAX);
	case IDC_MM2_MOVEDN_BTN	    : return On_RunMove	  (m, INT_MIN);
	case IDC_MM2_MOVESTOP_BTN   : return On_RunMove	  (m, 0);

	case IDC_MM2_PRESSUP_BTN    :
	    return (RRPSetParam (m_pIRRPMM2, IRRPMM2::ID_RunPress, INT_MAX), True);
	case IDC_MM2_PRESSDN_BNT    :
	    return (RRPSetParam (m_pIRRPMM2, IRRPMM2::ID_RunPress, INT_MIN), True);
	case IDC_MM2_PRESSSTOP_BTN  :
	    return (RRPSetParam (m_pIRRPMM2, IRRPMM2::ID_RunPress,	 0), True);
	};
	}
    }

	return GDialog::On_DlgMsg (m);
};
//
//[]------------------------------------------------------------------------[]
//
void CMM2Panel::On_IRRPParam (_U32 uParamId, _U32 uValue)
{
//	int		i;
	TCHAR		pBuf [64];

    if (0x91000000 == uParamId)
    {
	m_uRegStatus = uValue;

	s_pMainPanel ->Send_WM (CWndCallBack::EQUMSG, (WPARAM) IRRPMDIG::EquMask_MM2, uValue & 0x0003);

	m_oLedOnline.SetColor (GetCtrlHWND (IDC_MM2_ONLINE_LED), (0x0002 & uValue) ? CLedIndicator::Green : CLedIndicator::Red);
    }
    if (IRRPMM2::ID_RegGetMoveStatus  == uParamId)
    {
	m_uRegMove   = uValue;

	m_oLedRotLPin  .TurnOn (GetCtrlHWND (IDC_MM2_ROTLPIN_LED  ), (uValue & IRRPMM2::RotLtPin  ) ? True : False);
	m_oLedRotL45Pin.TurnOn (GetCtrlHWND (IDC_MM2_ROTL45PIN_LED), (uValue & IRRPMM2::RotLt45Pin) ? True : False);
	m_oLedRotMPin  .TurnOn (GetCtrlHWND (IDC_MM2_ROTMPIN_LED  ), (uValue & IRRPMM2::RotMdPin  ) ? True : False);
	m_oLedRotR45Pin.TurnOn (GetCtrlHWND (IDC_MM2_ROTR45PIN_LED), (uValue & IRRPMM2::RotRt45Pin) ? True : False);
	m_oLedRotRPin  .TurnOn (GetCtrlHWND (IDC_MM2_ROTRPIN_LED  ), (uValue & IRRPMM2::RotRtPin  ) ? True : False);

	m_oLedUpPin    .TurnOn (GetCtrlHWND (IDC_MM2_UPPIN_LED	  ), (uValue & IRRPMM2::MoveUpPin ) ? True : False);
	m_oLedDnPin    .TurnOn (GetCtrlHWND (IDC_MM2_DNPIN_LED	  ), (uValue & IRRPMM2::MoveDnPin ) ? True : False);
    }

    if (IRRPMM2::ID_RegGetMotorsStatus == uParamId)
    {
	m_oLedMoveUp.TurnOn (GetCtrlHWND (IDC_MM2_MOVEUP_LED), (uValue & 0x01) ? True : False);
	m_oLedMoveDn.TurnOn (GetCtrlHWND (IDC_MM2_MOVEDN_LED), (uValue & 0x02) ? True : False);

	m_oLedRotL.TurnOn   (GetCtrlHWND (IDC_MM2_ROTL_LED  ), (uValue & 0x08) ? True : False);
	m_oLedRotR.TurnOn   (GetCtrlHWND (IDC_MM2_ROTR_LED  ), (uValue & 0x04) ? True : False);
    }

    if (IRRPMM2::ID_RegGetPressStatus == uParamId)
    {
	m_uRegPress = uValue;

	m_oLedPressUpPin.TurnOn (GetCtrlHWND (IDC_MM2_PRESSUPPIN_LED), ((uValue & 0x10)) ? True : False);
	m_oLedPressDnPin.TurnOn (GetCtrlHWND (IDC_MM2_PRESSDNPIN_LED), ((uValue & 0x20)) ? True : False);

	::CheckRadioButton (GetHWND (), IDC_MM2_PRESSMODE0_RAD,
					IDC_MM2_PRESSMODE1_RAD, IDC_MM2_PRESSMODE0_RAD + ((uValue & 0x04) ? 1 : 0));
    }
    if (IRRPMM2::ID_RegGetRotPos      == uParamId)
    {
	wsprintf (pBuf, "%d", (short) uValue);
	SetCtrlText (IDC_MM2_DISP_ROT	, pBuf);
    }
    if (IRRPMM2::ID_RegGetThick	      == uParamId)
    {
	wsprintf (pBuf, "%d", (short) uValue);
	SetCtrlText (IDC_MM2_DISP_THICK	, pBuf);
    }
    if (IRRPMM2::ID_RegGetPress	      == uParamId)
    {
	wsprintf (pBuf, "%d.%d", (short) uValue / 10, (short) uValue % 10);
	SetCtrlText (IDC_MM2_DISP_PRESS	, pBuf);
    }
};

void CMM2Panel::On_IRRPCallBack (IRRPCallBack::Handle * hIRRPCallBack)
{
    Word	    uLength	;
    Word	    uType	;
    IRRPProtocol::DataPack *
		    pDataPack	;
    IRRPProtocol::ParamPack::Entry * 
		    pEntry	;

    if (hIRRPCallBack == m_hIRRPCallBack)
    {
	pDataPack = (IRRPProtocol::DataPack *) m_sIRRPCallBack.PeekHeadPacket ();

    while (pDataPack)
    {
	uLength = getBigEndianW (pDataPack, offsetof (IRRPProtocol::DataPack, uLength));
	uType	= getBigEndianW (pDataPack, offsetof (IRRPProtocol::DataPack, uType  ));

	if (0  == uType)
	{
		pEntry = (IRRPProtocol::ParamPack::Entry *)(pDataPack + 1);

	    while ((Byte *) pEntry < ((Byte *) pDataPack + uLength))
	    {
		On_IRRPParam (getBigEndianD (pEntry, offsetof (IRRPProtocol::ParamPack::Entry, uParamId)),
			      getBigEndianD (pEntry, offsetof (IRRPProtocol::ParamPack::Entry, uValue  )));
		pEntry ++;
	    }
	}

	pDataPack = (IRRPProtocol::DataPack *) m_sIRRPCallBack.PeekNextPacket ();
    }
    }
};
//
//[]------------------------------------------------------------------------[]
