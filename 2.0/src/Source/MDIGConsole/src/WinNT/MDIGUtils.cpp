//[]------------------------------------------------------------------------[]
// MDIGConsole util classes definitions
//
//
// Created by GSh.
//[]------------------------------------------------------------------------[]

#include "MDIGUtils.h"

	HIMAGELIST  g_hLedImages = NULL;
//
//[]------------------------------------------------------------------------[]
//
HFONT GAPI GetBoaldGUIFont ()
{
//  GetObject (...)
//
    static HFONT     s_hFont = NULL;

    if	  (s_hFont)
    {
    return s_hFont;
    }

    TEXTMETRIC	tm;
    LOGFONT	lf;

    HDC		hDC	= ::GetDC (NULL);
    HFONT	hDCFont = (HFONT) ::SelectObject (hDC, ::GetStockObject (DEFAULT_GUI_FONT));

	      memset (& lf, 0, sizeof (lf));

    ::GetTextFace    (hDC, sizeof (lf.lfFaceName) / sizeof (TCHAR), lf.lfFaceName);
    ::GetTextMetrics (hDC, & tm);

		      lf.lfHeight  = tm.tmHeight  ;
		      lf.lfWeight  = FW_BOLD	  ;
		      lf.lfCharSet = tm.tmCharSet ;

    ::SelectObject (hDC , hDCFont);
    ::ReleaseDC	   (NULL, hDC);

    return s_hFont = ::CreateFontIndirect (& lf);
};
//
//[]------------------------------------------------------------------------[]
//
void GAPI RRPSetParam (IRRPDevice * pInterface, DWord dParamId, DWord dParamValue)
{
    IRRPDevice::Param	Param;

    if (pInterface)
    {{
	Param.Init (dParamId, dParamValue);

	pInterface ->SetParam (1, & Param);
    }}
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
Bool GAPI PromptFileName (HWND hParent, LPTSTR pFileName, DWord nFileName, const TCHAR * pFilter)
{
    OPENFILENAME	      ofn   ;
    memset (& ofn, 0, sizeof (ofn)) ;

    ofn.lStructSize     = sizeof(OPENFILENAME);
    ofn.hwndOwner	= hParent;

    ofn.lpstrFile	= pFileName ;
    ofn.nMaxFile	= nFileName ;

    ofn.lpstrFileTitle	= NULL	    ;
    ofn.nMaxFileTitle	=	   0;

    ofn.lpstrFilter     = pFilter;
    ofn.nFilterIndex    = 1;
    ofn.Flags		= OFN_PATHMUSTEXIST | OFN_CREATEPROMPT | OFN_HIDEREADONLY;
    ofn.lpstrDefExt     = pFilter;

    return ::GetSaveFileName (& ofn);
};

void GAPI ExecContrlEng (HANDLE hFile)
{
    TCHAR   pLine [MAX_PATH] = {""} ;
    SECURITY_ATTRIBUTES	  sa = {sizeof (SECURITY_ATTRIBUTES), NULL, True};
    HANDLE  hEvent	     = ::CreateEvent (& sa, False, False, NULL);

    STARTUPINFO		      si;
    PROCESS_INFORMATION	      pi;

    memset (& si, 0,  sizeof (si));
	      si.cb = sizeof (si);

    wsprintf (pLine, "ContrlEng.exe /h %d %d", hFile, hEvent);

    if (::CreateProcess (NULL	,
			 pLine	,
			 NULL	,
			 NULL	,
			 TRUE	,
			 NORMAL_PRIORITY_CLASS ,
			 NULL	,
			 NULL	,
		       & si	,
		       & pi	))
    {
	::WaitForAny	(2, pi.hThread, hEvent);

	::CloseHandle   (pi.hThread );
	::CloseHandle	(pi.hProcess);
    }
	::CloseHandle	(hEvent);
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
    	UINT	CWndCallBack::RRPMSG = 0;
	UINT	CWndCallBack::EQUMSG = 0;

HRESULT CWndCallBack::QueryInterface (REFIID, void ** pi)
{
	if (pi)
	{
	  * pi = NULL;
	}
		return ERROR_NOT_SUPPORTED;
};

ULONG CWndCallBack::AddRef ()
{
	return 1;
};

ULONG CWndCallBack::Release ()
{
	return 1;
};

void * CWndCallBack::PeekHeadPacket ()
{
    m_Lock.LockAcquire ();

	if (m_pHead < m_pTail)
	{
    m_Lock.LockRelease ();
    
	    return m_pHead;
	}
    m_Lock.LockRelease ();

	    return NULL;
};

void * CWndCallBack::PeekNextPacket ()
{
    m_Lock.LockAcquire ();

	if (m_pHead < m_pTail)
	{
	    m_pHead += getBigEndianW (m_pHead, offsetof (IRRPProtocol::DataPack, uLength));

	if (m_pHead < m_pTail)
	{
    m_Lock.LockRelease ();

	    return m_pHead;
	}
	    m_pHead = m_pTail = m_pBase;
	}

    m_Lock.LockRelease ();

	    return NULL;
};

void CWndCallBack::Invoke (const void * hICallBack	 ,
			   const IRRPProtocol::DataPack *
					pDataPack	 )
{
    Bool    bPost = False;

    Word    uLength = getBigEndianW (pDataPack, offsetof (IRRPProtocol::DataPack, uLength));

    m_Lock.LockAcquire ();

	if (m_pTail + uLength < m_pHigh)
	{
//	    memmove (m_pBase, m_pHead,   m_pTail - m_pHead);
//		    m_pTail = m_pBase + (m_pTail - m_pHead);
//		    m_pHead = m_pBase;
	}
	if (m_pTail + uLength < m_pHigh)
	{
	if (m_pTail == m_pHead)
	{
	    bPost = True ;
	}
	    memcpy (m_pTail, pDataPack, uLength);

		    m_pTail +=		uLength ;
	}

    m_Lock.LockRelease ();

    if (bPost)
    {
        ::PostMessage (m_hWnd, RRPMSG, 0, (LPARAM) hICallBack);
    }
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
void CLedIndicator::TurnOn   (HWND  hWnd, Bool bOnState)
{
    if (((  m_bCurState) && (! bOnState))
    ||  ((! m_bCurState) && (  bOnState)))
    {
	m_bCurState  = (bOnState) ? True : False;

    if (m_iCurIndex != GetColorIndex
       (m_uCurColor))
    {
	m_iCurIndex  = GetColorIndex
       (m_uCurColor);

	if (hWnd)
	{
	    ::InvalidateRect (hWnd, NULL, False);
	}
    }
    }
};

void CLedIndicator::SetColor (HWND hWnd, Color uColor)
{
    if (m_uCurColor != uColor)
    {
    if (m_iCurIndex != GetColorIndex 
       (m_uCurColor  = uColor))
    {
	m_iCurIndex  = GetColorIndex
       (m_uCurColor);

	if (hWnd)
	{
	    ::InvalidateRect (hWnd, NULL, False);
	}
    }
    }
};

void CLedIndicator::On_Draw (DRAWITEMSTRUCT * pdi)
{
    ::FillRect (pdi ->hDC, & pdi ->rcItem, ::GetSysColorBrush (COLOR_3DFACE));

    ::ImageList_Draw 
	       (g_hLedImages, GetColorIndex (m_uCurColor),
		pdi ->hDC   , 
		pdi ->rcItem.left + (((pdi ->rcItem.right  - pdi ->rcItem.left) / 2) - 8),
		pdi ->rcItem.top  + (((pdi ->rcItem.bottom - pdi ->rcItem.top ) / 2) - 8), ILD_TRANSPARENT);
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
Bool CIndicatorBar::On_Create (void * pParam)
{
    const Descriptor *	  pDescriptor =
			m_pDescriptor =
   (const Descriptor *)	  pParam;

    for (; pDescriptor ->uMask; pDescriptor ++)
    {
	m_dAndBitMask |= pDescriptor ->uMask;
    }

	return True;
};

const CIndicatorBar::Descriptor * CIndicatorBar::GetFirstItem (RECT & rc)
{
    GetClientRect (& rc);

    if (m_pDescriptor ->uMask)
    {
	rc.left   =  rc.right - (rc.bottom - rc.top);
	rc.top   += (rc.bottom - rc.top  - m_iSize.y) / 2;
	rc.bottom =  rc.top  + m_iSize.y;
	rc.left  += (rc.right  - rc.left - m_iSize.x) / 2;
	rc.right  =  rc.left + m_iSize.x;

	return m_pDescriptor;	
    }
	return NULL;
};

const CIndicatorBar::Descriptor * CIndicatorBar::GetNextItem (const Descriptor * pDescriptor, RECT & rc)
{
	pDescriptor ++;

    if (pDescriptor ->uMask)
    {
	rc.left  -= (m_iSize.x + 4);
	rc.right -= (m_iSize.y + 4);

	return pDescriptor;
    }
	return NULL;
};

const CIndicatorBar::Descriptor * CIndicatorBar::GetOverDescriptor (int x, int y)
{
    RECT rc;

    for (const Descriptor * i = GetFirstItem (rc); i; i = GetNextItem (i, rc))
    {
	if ((rc.left - 3 < x) && (x < rc.right  + 3)
	&&  (rc.top  - 2 < y) && (y < rc.bottom + 2))
	{
	    return i;
	}
    }
	    return NULL;
};

const CIndicatorBar::Descriptor * CIndicatorBar::GetHighDescriptor (DWord dAndMask) const
{
    for (const Descriptor * i = m_pDescriptor; i ->uMask; i ++)
    {
	if (m_dValBitMask & dAndMask & i ->uMask)
	{
	    return i;
	}
    }
	    return NULL;
};

void CIndicatorBar::On_Draw (DRAWITEMSTRUCT * pdi)
{
#if TRUE

    RECT rc;

    ::FillRect (pdi ->hDC, & pdi ->rcItem, (HBRUSH) ::GetSysColorBrush (COLOR_3DFACE));

    for (const Descriptor * i = GetFirstItem (rc); i; i = GetNextItem (i, rc))
    {
	::ImageList_Draw
	       (g_hLedImages, (i ->uMask & m_dValBitMask) ? Red : Gray,
		pdi ->hDC   ,
		rc.left	    ,
		rc.top	    , ILD_TRANSPARENT);
    }

#else

    if (ODA_DRAWENTIRE != pdi ->itemAction)
    {
	return;
    }

    RECT rc;

    ::FillRect (pdi ->hDC, & pdi ->rcItem, (HBRUSH) ::GetSysColorBrush (COLOR_3DFACE));

    for (const Descriptor * i = GetFirstItem (rc); i; i = GetNextItem (i, rc))
    {
	if (i ->uMask & m_dValBitMask)
	{
	    ::DrawFrameControl (pdi ->hDC, & rc, DFC_BUTTON, DFCS_BUTTONRADIO);
	}
	else
	{
	    ::DrawFrameControl (pdi ->hDC, & rc, DFC_BUTTON, DFCS_BUTTONRADIO | DFCS_INACTIVE);
	}
    }

#endif
};

Bool CIndicatorBar::On_ReflectWinMsg (GWinMsg & m)
{
    if (WM_DRAWITEM == m.uMsg)
    {{
	On_Draw ((LPDRAWITEMSTRUCT) m.lParam);

	return m.DispatchComplete ();
    }}
	return m.DispatchComplete ();
};

void CIndicatorBar::SetValue (DWord dValue)
{
    m_dValBitMask = (dValue & m_dAndBitMask);

    if (GetHWND ())
    {
	::InvalidateRect (GetHWND (), NULL, True);
    }
};

void CIndicatorBar::SetBits (DWord dClrMask, DWord dSetMask)
{
    m_dValBitMask = (m_dValBitMask & (~(dClrMask & m_dAndBitMask)))
		  |		       (dSetMask & m_dAndBitMask);

    if (GetHWND ())
    {
	::InvalidateRect (GetHWND (), NULL, True);
    }
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
void CDisplay::InitSells (int nSellX, int nSellY)
{
    GRect   rc;

    GetClientRect (& rc);

    m_oSell.cx = rc.cx / nSellX;
    m_oSell.cy = rc.cy / nSellY;

    m_oSell.x  = (rc.cx - (m_oSell.cx * nSellX)) / 2;
    m_oSell.y  = (rc.cy - (m_oSell.cy * nSellY)) / 2;
};


static HBRUSH WINAPI CreateSellBrush ()
{
/*
    static const Word Pattern [8] = {0x5555, 0xAAAA, 0x5555, 0xAAAA,
				     0x5555, 0xAAAA, 0x5555, 0xAAAA};
*/
    static const Word Pattern [8] = {0x7F7F, 0xFFFF, 0xFFFF, 0xFFFF,
				     0x7F7F, 0xFFFF, 0xFFFF, 0xFFFF};

    HBRUSH  hBrush  = NULL;
    HBITMAP hBitmap = NULL;

    if (NULL != 
       (hBitmap = ::CreateBitmap (8, 8, 1, 1, & Pattern)))
    {
	hBrush  = ::CreatePatternBrush (hBitmap);

		  ::DeleteObject       (hBitmap);
    }
	return hBrush;
};

void CDisplay::DrawBkGnd (HDC hDC, RECT & rd)
{
    ::DrawEdge (hDC, & rd, EDGE_SUNKEN, BF_RECT | BF_ADJUST);
    ::FillRect (hDC, & rd, (m_dStateFlags & 0x01) ? (HBRUSH) ::GetStockObject  (BLACK_BRUSH )
						  : (HBRUSH) ::GetSysColorBrush(COLOR_3DFACE));
#if DISPLAY_SHOW_SELS

    HBRUSH  hBrush = ::CreateSellBrush ();

    COLORREF cb    = ::SetBkColor   (hDC, (m_dStateFlags & 0x01) 
				    ?	  RGB (0x00, 0x00, 0x00)
				    : ::GetSysColor (COLOR_3DFACE));
    COLORREF ct	   = ::SetTextColor (hDC, RGB (0x00, 0x40, 0x00));

    RECT    rs;

    rs.left   = rd.left	 ;
    rs.right  = rd.right ;

    for (rs.top = m_oSell.y; rs.top + 1 < rd.bottom; rs.top += m_oSell.cy)
    {
    rs.bottom = rs.top + 1;

    ::FillRect (hDC, & rs, hBrush);
    }

    rs.top    = rd.top	 ;
    rs.bottom = rd.bottom;

    for (rs.left = m_oSell.x; rs.left + 1 < rd.right; rs.left += m_oSell.cx)
    {
    rs.right  = rs.left + 1;

    ::FillRect (hDC, & rs, hBrush);
    }
		     ::SetBkColor   (hDC, cb);
		     ::SetTextColor (hDC, ct);

		     ::DeleteObject (hBrush);
#endif
};

void CDisplay::On_ReflectDrawItem (DRAWITEMSTRUCT * pdi)
{
    RECT    rc;

    if (ODA_DRAWENTIRE & pdi ->itemAction)
    {
	rc = pdi ->rcItem;

	DrawBkGnd   (pdi ->hDC, rc);
/*
	rc.left	  += 2;
	rc.top    += 2;
	rc.right  -= 2;
	rc.bottom -= 2;
*/

//	::DrawEdge  (pdi ->hDC, & rc, EDGE_SUNKEN, BF_RECT);

//	::FrameRect (pdi ->hDC, & rc, (HBRUSH) ::GetStockObject (BLACK_BRUSH));
    }
};

Bool CDisplay::On_ReflectWinMsg (GWinMsg & m)
{
    if (WM_DRAWITEM == m.uMsg)
    {
	return (On_ReflectDrawItem ((DRAWITEMSTRUCT *) m.lParam), True);
    }
	return False;
};


Bool CDisplay::On_Create (void * pParam)
{
    if (! GButton::On_Create (pParam))
    {	return False;}

	return True ;
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
Bool CEditHandler::PreprocMessage (MSG * msg)
{
    if ((NULL == m_hCtrl) || (WM_KEYDOWN != msg ->message))
    {	return False;}

    if (VK_RETURN == msg ->wParam)
    {
//	::SendMessage (m_hCtrl,	EM_SETMODIFY, (WPARAM) 1, (LPARAM)  0);
	::SendMessage (m_hCtrl, EM_SETSEL   , (WPARAM) 0, (LPARAM) -1);

	return True ;
    }
	return False;
};

Bool CEditHandler::On_ReflectWinMsg (GWinMsg & m)
{
    if (WM_COMMAND != m.uMsg)
    {	return False;}

    if (EN_SETFOCUS  == m.HiwParam)
    {
	m_nCtrlId  = m.LowParam ;
	m_hCtrl	   = m.lWnd	;

	return False;
    }
    if ((m_hCtrl == NULL) || (m_hCtrl != m.lWnd))
    {
	return False;
    }
    if (EN_KILLFOCUS == HIWORD (m.wParam))
    {
	m_nCtrlId  = 0    ;
	m_hCtrl	   = NULL ;

	return ::SendMessage ((HWND) m.lParam, EM_GETMODIFY, (WPARAM) 0, (LPARAM) 0) ;
    }
	return False;
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
void GRange::SetLoValue (int nValue)
{
    if (nValue < m_nMin)
    {
	nValue = m_nMin;
    }
    if (nValue > m_nMax - m_nMinRange)
    {
	nValue = m_nMax - m_nMinRange;
    }
    if (nValue >= m_nHiValue - m_nMinRange)
    {
	nValue  = m_nHiValue - m_nMinRange;
    }
	m_nLoValue = nValue;

    	SetStep (m_nStep);
};

void GRange::SetHiValue (int nValue)
{
    if (nValue > m_nMax)
    {
	nValue = m_nMax;
    }
    if (nValue < m_nMin + m_nMinRange)
    {
	nValue = m_nMin + m_nMinRange;
    }
    if (nValue <= m_nLoValue + m_nMinRange)
    {
	nValue  = m_nLoValue + m_nMinRange;
    }
	m_nHiValue = nValue;

    	SetStep (m_nStep);
};

void GRange::SetStep (int nStep)
{
    if (nStep ==  0	     )
    {
	nStep  =  1;
    }
    if (nStep <   m_nMinRange)
    {
	nStep  =  m_nMinRange;
    }
    if (nStep >= (m_nHiValue - m_nLoValue))
    {
	nStep  = (m_nHiValue - m_nLoValue);
    }

    int nRange = (((m_nHiValue - m_nLoValue) / nStep)	     )
	       + (((m_nHiValue - m_nLoValue) % nStep) ? 1 : 0);

    if (nRange)
    {
	nStep  =  (m_nHiValue - m_nLoValue) / nRange;
    }
	m_nStep = nStep;
};

void GRange::SetNum (int nNum)
{
    if (nNum	< 2)
    {
	nNum	= 2;

	m_nStep = (m_nHiValue - m_nLoValue);
    }
    else
    {
	SetStep ((2 <= nNum) ? (m_nHiValue - m_nLoValue) / (nNum - 1)
			     : (m_nHiValue - m_nLoValue));
    }
};

int GRange::GetNum  () const
{
    return ((m_nHiValue - m_nLoValue) / m_nStep) + 1;

//  return ((m_nHiValue - m_nLoValue) / m_nStep) + 1 + (((m_nHiValue - m_nLoValue) % m_nStep) ? 1 : 0);
};

int GRange::GetStep () const
{
    return m_nStep;
};

int GRange::GetValue (int nPoint)
{
    int	n = m_nLoValue + (nPoint * GetStep ());

    if (n < m_nHiValue)
    {
	return n;
    }
	return m_nHiValue;
};
//
//[]------------------------------------------------------------------------[]



#if FALSE


//[]------------------------------------------------------------------------[]
//
    DECLARE_INTERFACE_ (IIniFile, IUnknown)
    {
	typedef enum
	{
	    Notify_Closed   = 0x00,
	    Notify_Deleted  = 0x01,
	    Notify_Append   = 0x02,
	    Notify_Remove   = 0x02,
	    Notify_Change   = 0x03,

	}   Notification_Mask ;

    DECLARE_INTERFACE_ (CallBack, IUnknown)
    {
    DECLARE_INTERFACE  (Handle)
    {
    STDMETHOD_ (void,	      Close)() PURE;
    };
    STDMETHOD_ (void,	     Invoke)(Handle *	  hCallBack  ,
				     Notification_Mask	     ,
				     void *	  pContext   ) PURE;
    };

    DECLARE_INTERFACE  (Handle)
    {
    STDMETHOD_ (void,	      Close)() PURE;

    STDMETHOD_ (GResult,	Get)(const char * pSecPath  ,
					   char * pBuf	    ,
					  DWord	  dBufSize  ) PURE;

    STDMETHOD_ (GResult,	Put)(const char * pSecPath  ,
				           char * pBuf	    ) PURE;

    STDMETHOD_ (void, SetNotificator)(Notification_Mask	    ,
				      CallBack ** pCallBack ,
					  void *  pContext  );
    };

    STDMETHOD_ (GResult,     Create)(Handle    *& hSection  ,
				     Handle     * hParent   ,
				     const char * pSecPath  ,
				     Bool	  bCreate = False) PURE;

    STDMETHOD_ (GResult,     Delete)(Handle	* hParent   ,
				     const char * pSecPath  )	   PURE;
    };
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
//	printf ("{\r\n}");
//
static const char * findChr (const char * pStr, char cChr)
{
    for (; * pStr && (* pStr != cChr); pStr ++)
    {}
    return pStr;
};

static char sscanChr (const char *& pStr, char cChr, const char * pDelPtr = NULL)
{
    for (; * pStr && (pDelPtr) ? pStr < pDelPtr : True; pStr ++)
    {
	if (cChr == * pStr)
	{
	    break;
	}
    }
    return (pDelPtr && (pDelPtr <= pStr)) ? '\0' : * pStr;
};

static char sscanChr (const char *& pStr, const char * pDelStr, const char * pDelPtr = NULL)
{
    for (; * pStr && (pDelPtr) ? pStr < pDelPtr : True; pStr ++)
    {
	if ('\0' != * findChr (pDelStr, * pStr))
	{
	    break;
	}
    }
    return (pDelPtr && (pDelPtr <= pStr)) ? '\0' : * pStr;
};

static char sskipChr (const char *& pStr, const char * pDelStr, const char * pDelPtr = NULL)
{
    for (; * pStr && (pDelPtr) ? pStr < pDelPtr : True; pStr ++)
    {
	if ('\0' == * findChr (pDelStr, * pStr))
	{
	    break;
	}
    }
    return (pDelPtr && (pDelPtr <= pStr)) ? '\0' : * pStr;
};

static Bool sscan_I32 (const char *& pStr, const char * pDelStr, _I32 * pValue)
{
    Bool    bSigFlag = False;
    Bool    bHexFlag = False;

    Bool    bValue   = False;
    _U32    dValue   =	   0;

    if (pDelStr) sskipChr (pStr, pDelStr);

    if ('-' == * pStr)
    {
	bSigFlag = True;

	pStr ++;
	sskipChr (pStr, " \t");
    }
    else
    if ('+' == * pStr)
    {
	pStr ++;
	sskipChr (pStr, " \t");
    }

    if (('0' == * pStr) && (('x' == * (pStr + 1)) || ('X' == * (pStr + 1))))
    {
	bValue	 = True;
	bHexFlag = True;

	pStr += 2;
    }

    for (; * pStr; pStr ++)
    {
	if (! bHexFlag && ((('0' <= * pStr) && (* pStr <= '9'))))
	{
	    bValue = True;
	    dValue = (dValue * 8) + (dValue * 2) + (* pStr - '0');
	    continue;
	}
	else
	if (  bHexFlag && ((('0' <= * pStr) && (* pStr <= '9'))
		       ||  (('A' <= * pStr) && (* pStr <= 'F')) 
		       ||  (('a' <= * pStr) && (* pStr <= 'f'))))
	{
	    bValue = True;
	    dValue = (dValue * 16) + ( (* pStr <= '9') ? (* pStr - '0')
				   : (((* pStr <= 'F') ? (* pStr - 'A') : (* pStr - 'a')) + 10));
	    continue;
	}
	else
	if ('.' == * pStr)
	{
	    bValue = True;
	    pStr ++;
	}
	    break;
    }

    if (bValue && pValue)
    {
      * pValue = (! bSigFlag) ? dValue : - (_I32) dValue;
    }

    return bValue;
};

static Bool sscan_R64 (const char *& pStr, const char * pDelStr, _R64 * pValue)
{
    Bool    bSigFlag = False;
    Bool    bPntFlag = False;

    Bool    bValue   = False;
    _R64    rValue   = .0;
    _R64    rDivisor = .1;

    if (pDelStr) sskipChr (pStr, pDelStr);

    if ('-' == * pStr)
    {
	bSigFlag = True;

	pStr ++;
	sskipChr (pStr, " \t");
    }
    else
    if ('+' == * pStr)
    {
	pStr ++;
	sskipChr (pStr, " \t");
    }

    for (; * pStr; pStr ++)
    {
	if ((('0' <= * pStr) && (* pStr <= '9')))
	{
	    bValue = True;

	    if (bPntFlag)
	    {
		rValue	 += (rDivisor	  * (* pStr - '0'));
		rDivisor *= 0.1;
	    }
	    else
	    {
		rValue    = (rValue * 10) + (* pStr - '0');
	    }
	    continue;
	}
	else
	if (('.' == * pStr) && (! bPntFlag))
	{
	    bPntFlag = True;
	    bValue   = True;

	    continue;
	}
	    break;
    }

    if (bValue && pValue)
    {
      * pValue = (! bSigFlag) ? rValue : - rValue;
    }
	return bValue;
};

static Bool sscan_R32 (const char *& pStr, const char * pDelStr, _R32 * pValue)
{
    _R64    rValue;

    return (sscan_R64 (pStr, pDelStr, & rValue)) ? ((pValue) ? * pValue = (float) rValue : NULL, True) : False;
};
//
//[]------------------------------------------------------------------------[]


//[]------------------------------------------------------------------------[]
//
class GIniFile // : public IIniFile
{
public :

	typedef	struct SectionHandle {} * HSection;

		    GIniFile	()
		    {
			m_pBase	=
			m_pHigh	=
			m_pTail	= NULL;

			m_hFile	= NULL;
		    };

		   ~GIniFile	()
		    {
			Close ();
		    };

	GResult		Create	    (LPCTSTR, DWord dMaxSize);

	void		Close	    ();

	GResult		Create	    (HSection   & hSection ,
				     HSection     hParent  ,
				     const char * pSecPath ,
				     Bool	  bCreate =  False );

	GResult		Get	    (HSection     hSection ,
				     const char * pSecPath ,
				           char * pBuf     ,
					   DWord  dBufSize );

	GResult		Put	    (HSection     hSection ,
				     const char * pSecPath ,
				           char * pBuf     );
protected :

#pragma pack (_M_PACK_VPTR)

	struct Section
	{
/*
	    struct Handle
	    {
		TSLink <Handle , 0> m_SLink ;

		Section *	m_pSection  ;
		DWord		m_dCallMask ;
		void *		m_pCallBack ;
		void *		m_pContext  ;
	    };
*/
	    TSLink <Section, 0>	    m_SLink ;
	    TXList <Section,
	    TSLink <Section, 0> >   m_SList ;
//	    TXList <Handle,
//	    TSLink <Handle, 0 > >   m_HList ;

	    DWord		m_dCallMask ;
	    Section *		m_pParent   ;
	    DWord		m_dFlags    ;

	    DWord		m_dOffset   ;	// Section Offset from the parent's Base.
	    DWord		m_dLength   ;	// Section Length


	    size_t		GetBaseOffset	() const;
	    DWord		GetLength	() const { return m_dLength;};
	};
//
//....................................
//
#pragma pack ()

protected :

	GResult		Save		();

private :

	Section *	findSection	(Section *, const char * pPath) const;
//
//....................................
//
	void		freeSection	(Section *);
	Section *	callocSection	();

	void		deleteSection	(Section *);

	char * 		parseSection	(Section *, char *, char *);

	void		parse		();

protected :

        TInterlocked <ULONG>    m_cRefCount ;

	TXList <Section,
	TSLink <Section, 0> >	m_SRoot	    ;

	char *			m_pBase	    ;
	char *			m_pHigh	    ;
	char *			m_pTail	    ;

	HANDLE			m_hFile	    ;
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
GResult	GIniFile::Create (HSection   & hSection ,
			  HSection     hParent  ,
			  const char * pSecPath ,
			  Bool	       bCreate  )
{
    return (NULL != (hSection = (HSection) findSection ((Section *) hParent, pSecPath)))
	    ? ERROR_SUCCESS : ERROR_NOT_FOUND;
};
//
//[]------------------------------------------------------------------------[]
//

//
//[]------------------------------------------------------------------------[]
//
GResult GIniFile::Get (HSection     hSection ,
		       const char * pSecPath ,
			     char * pBuf     ,
			     DWord  dBufSize )
{
    Section *	 p;
    char    *	 pBufHead = pBuf;

    if (NULL == (p = findSection ((Section *) hSection, pSecPath)))
	return ERROR_NOT_FOUND;

    const char * pHead;
    const char * pTail;
    const char * pHigh;

	pHead  = findChr 
       (pHigh  = m_pBase + p ->GetBaseOffset (), '\n') + 1;
	pHigh += p ->GetLength ();

    if (p ->m_SList.last ())
	pTail  = m_pBase + p ->m_SList.first () ->GetBaseOffset ();
    else
	pTail  = pHigh;

    if (dBufSize < (DWord)(pTail - pHead))
	return ERROR_INSUFFICIENT_BUFFER;

    memcpy  (pBuf, pHead, (pTail - pHead));

	pBuf	 += (pTail - pHead);
	dBufSize -= (pTail - pHead);
	pHead	  =  pTail;

    for (Section * i = p ->m_SList.first (); i; i = p ->m_SList.getnext (i))
    {
        pTail = findChr
       (pHead = m_pBase + i ->GetBaseOffset (), '\n') + 1;

    if (dBufSize < (DWord)(pTail - pHead))
	return ERROR_INSUFFICIENT_BUFFER;

    memcpy  (pBuf, pHead, (pTail - pHead));

	pBuf	 += (pTail - pHead);
	dBufSize -= (pTail - pHead);
	pHead	  =  m_pBase + i ->GetBaseOffset () + i ->GetLength ();
    }

	pTail	  =  pHigh;

    if (dBufSize < (DWord)(pTail - pHead))
	return ERROR_INSUFFICIENT_BUFFER;

    memcpy  (pBuf, pHead, (pTail - pHead));

	pBuf	 += (pTail - pHead);
	dBufSize -= (pTail - pHead);

    for (;( '}' != * pBuf) && (pBufHead <= pBuf); pBuf --);
    for (;('\n' != * pBuf) && (pBufHead <= pBuf); pBuf --);

      * ++ pBuf = '\0';

	return ERROR_SUCCESS;
};

GResult GIniFile::Put (HSection     hSection ,
		       const char * pSecPath ,
			     char * pBuf     )
{
    Section *	 p;

    if (NULL == (p = findSection ((Section *) hSection, pSecPath)))
	return ERROR_NOT_FOUND;

    char * pHead    ;
    char * pTail    ;
    char * pBufTail ;

	pHead = 
	pTail = (char *) findChr (m_pBase + p ->GetBaseOffset (), '\n') + 1;

    pBufTail  = (char *) findChr (pBuf, '{');

    if (p ->m_SList.last ())
    {
	pTail  = m_pBase + p ->m_SList.first () ->GetBaseOffset ();

	for (;('\n' != * pBufTail) && (pBuf <= pBufTail); pBufTail --);

	pBufTail ++;
    }
    else
    {
	sscanChr (pTail, "}");

	for (;('\n' != * pTail) && (pHead <= pTail); pTail --);

	pTail ++;
    }

    size_t dBufSize = pBufTail - pBuf;

    if (m_pTail - (pTail - pHead) + dBufSize >= m_pHigh)
    {
	return ERROR_INSUFFICIENT_BUFFER;
    }

    memmove (pHead + dBufSize, pTail, m_pTail - pTail);
    m_pTail -= (pTail - pHead);

    memcpy  (pHead, pBuf, dBufSize);
    m_pTail += dBufSize;

    TXList <Section,
    TSLink <Section, 0> > & SList = (p ->m_pParent) ? p ->m_pParent ->m_SList : m_SRoot;

    for (; p; p = SList.getnext (p))
    {
	p ->m_dOffset -= (pTail - pHead);
	p ->m_dOffset += dBufSize;	
    }

    Save ();

	return ERROR_SUCCESS;
};
//
//[]------------------------------------------------------------------------[]
//
GIniFile::Section *
GIniFile::findSection (Section * pParent, const char * pPath) const
{
    if (NULL == pPath)
	return pParent;

    while (* pPath == '/')
    {
	   pPath ++;

    if ((* pPath ==  '.') && (* (pPath + 1) == '.'))
    {
	return (pParent) ? findSection (pParent ->m_pParent, pPath + 2) : NULL;
    }
    }

    if ('\0' == * pPath)
       return pParent;

    Section *	  i;
    TXList <Section,
    TSLink <Section, 0> > & SList = (pParent) ? pParent ->m_SList : m_SRoot;

    const char * pHead;
    const char *     p;

    for (i = SList.first (); i; i = SList.getnext (i))
    {
	pHead  = findChr (m_pBase + i ->GetBaseOffset (), '"');
	pHead ++;

	for (p = pPath; * p; p ++, pHead ++)
	{
	    if (('"'  == * pHead)
	    ||  (* p  != * pHead))
		break;
	}

	if ('\0' == * p)
	    return i;
//	if ('"'  == * p)
//	    return i;
	else
	if ('/'  == * p)
	{
	    return findSection (i, p);
	}
    }

	    return NULL;
};

void GIniFile::freeSection (GIniFile::Section * p)
{
    (p) ? free (p) : NULL;
};

GIniFile::Section *
GIniFile::callocSection ()
{
    return (Section *) calloc (1, sizeof (Section));
};

size_t
GIniFile::Section::GetBaseOffset () const
{
    return (size_t)(m_dOffset + ((m_pParent) ? m_pParent ->GetBaseOffset () : 0));
};

void GIniFile::deleteSection (Section * pSection)
{
    if (pSection ->m_pParent)
    {
	pSection ->m_pParent ->
	m_SList.extract (pSection);
    }
    else
    {
	m_SRoot.extract (pSection);
    }

    while (pSection ->m_SList.first ())
    {
	deleteSection   (pSection ->m_SList.first ());
    }
	freeSection	(pSection);
};

static Bool sscanIdName (const char *& pHead, const char * pTail)
{
};

static Bool hasBegSection (const char * pHead, const char * pTail)
{
    const char * p = pHead;

    if ('{'  != sskipChr (   p, " \t\r\n", pTail))
	return False;

    if ('"'  != sscanChr (++ p,	      '"', pTail))
	return False;

    if ('"'  != sscanChr (++ p,	      '"', pTail))
	return False;

    if ('\0' != sskipChr (++ p, " \t\r\n", pTail))
	return False;

	return True;
};

static Bool hasEndSection (const char * pHead, const char * pTail)
{
    const char * p = pHead;

    if ('}'  != sskipChr (   p, " \t\r\n", pTail))
	return False;

    if ('\0' != sskipChr (++ p, " \t\r\n", pTail))
	return False;

	return True;
};

char * GIniFile::parseSection (Section * pParent, char * pHead, char * pTail)
{
    Section * pSection = callocSection ();

	    pSection ->m_dOffset = pHead - m_pBase;

    for (pHead = pTail; pSection && (pHead != (pTail = (char *) findChr (pHead, '\n'))); pHead = pTail)
    {
	 pTail ++;

    if (hasBegSection (pHead, pTail))
    {
	 pTail = parseSection  (pSection, pHead, pTail);
    }
    else
    if (hasEndSection (pHead, pTail))
    {
	    pSection ->m_dLength  = (pTail - m_pBase) - pSection ->m_dOffset;

	if (pParent)
	{
	   (pSection ->m_pParent  = pParent)
		     ->m_SList.append (pSection);
	    pSection ->m_dOffset -= pParent ->GetBaseOffset ();
	}
	else
	{
		       m_SRoot.append (pSection);
	}
	    pSection = NULL ;
    }
    }

    if (pSection)
    {
	freeSection (pSection);
    }

    return pTail;
};

void GIniFile::parse ()
{
    char * pHead;
    char * pTail;

    for (pHead = pTail = m_pBase; pHead != (pTail = (char *) findChr (pHead, '\n')); pHead = pTail)
    {
	++ pTail;

    if (hasBegSection (pHead, pTail))
    {
	pTail = parseSection  (NULL,  pHead, pTail);
    }
    }
};

GResult GIniFile::Save ()
{
    DWord   dSize = 0;

    if (NULL == m_hFile)
    {
	return ERROR_INVALID_HANDLE;
    }
	  ::SetFilePointer (m_hFile, 0, NULL, FILE_BEGIN);

    if (! ::WriteFile (m_hFile, m_pBase, m_pTail - m_pBase, & dSize, NULL))
    {
	return ::GetLastError ();
    }
	  ::SetEndOfFile   (m_hFile);

	return ERROR_SUCCESS;
};

GResult GIniFile::Create (LPCTSTR pFileName, DWord dMaxSize)
{
    DWord   dSize = 0;

	Close ();

    if (INVALID_HANDLE_VALUE == 
       (m_hFile = ::CreateFile (pFileName	   ,
				GENERIC_READ	 |
				GENERIC_WRITE	   ,
				FILE_SHARE_READ  |
				FILE_SHARE_WRITE   ,
				NULL		   ,
				OPEN_EXISTING	   ,
				FILE_ATTRIBUTE_NORMAL,
				NULL		     )))
    {
	return ::GetLastError ();
    }

	dSize = ::GetFileSize (m_hFile, NULL);

    if (dMaxSize < dSize)
    {
	Close ();

	return ERROR_INSUFFICIENT_BUFFER;
    }

    if (NULL    ==
       (m_pBase  = (char *) malloc (dMaxSize)))
    {
	Close ();

	return ERROR_NOT_ENOUGH_MEMORY;
    }
	m_pHigh  = m_pBase + dMaxSize ;
    
    if (! ::ReadFile (m_hFile, m_pBase, dSize, & dSize, NULL))
    {
	Close ();

	return ::GetLastError ();
    }

	m_pTail  = m_pBase + dSize;

	parse ();

	return ERROR_SUCCESS;
};

void GIniFile::Close ()
{
    while (m_SRoot.first ())
    {
	deleteSection (m_SRoot.first ());
    }

    if (m_pBase)
    {
	free (m_pBase);

	m_pBase =
	m_pHigh = 
	m_pTail = NULL;
    }
    if (m_hFile)
    {
	::CloseHandle (m_hFile);
	m_hFile = NULL;
    }
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
class GIniSection //: public  GStreamBuf
{
public :
		    GIniSection (void * pBuf, size_t sBufLen) { SetBuf (pBuf, sBufLen);};

	void *	    SetBuf	(void * pBuf, size_t sBufLen)
		    {
		        m_pHigh = 
		       (m_pTail =
		        m_pBase = (char *) pBuf ) + ((pBuf) ? sBufLen : 0); 

			Clear ();

			return m_pBase;
		    };

	void	    Clear	    ()
		    {
			    m_pTail = m_pBase;

			if (m_pBase)
			  * m_pBase = '\0';
		    };

	GResult	    Load	    (GIniFile *,  GIniFile::HSection	 ,
						  const  char * pSecPath );

	GResult	    Save	    (GIniFile *,  GIniFile::HSection	 ,
						  const  char * pSecPath );

	Bool	    Get		    (const char * pName, char * pBuf	 ,
							 DWord	dBufSize );

	Bool	    Put		    (const char * pName, char * pBuf	 );

	int	    scanf	    (const char * pName, const char * fmt, ...);

	Bool	    printf	    (const char * pName, const char * fmt, ...);

protected :

	char *	    find	    (char *& pTail, const char * pName);

protected :

	char *		m_pBase;
	char *		m_pTail;
	char *		m_pHigh;
};

    template <size_t S> struct TIniSection : public GIniSection
    {
		    TIniSection () : GIniSection (pReserved, sizeof (pReserved)) {};

	Byte 	    pReserved [S - sizeof (GIniSection)];
    };
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
char * GIniSection::find (char *& pTail, const char * pName)
{
    char * pHead = m_pBase;

    if (pHead)
    {
    while (NULL != (pHead = ::StrStrA (pHead, pName)))
    {
	if ((m_pBase == pHead) || ('"' != * (pHead - 1)) || ('"' != * (pHead + strlen (pName))))
	{
	    pHead += (strlen (pName)	);

	    continue;
	}
	    pHead += (strlen (pName) + 1);

	if ('=' != sskipChr  (pHead, " \t"))
	{
	    continue;
	}

	if (';' != * (pTail = (char *) findChr (++ pHead, ';')))
	{
	    continue;
	}
	    return pHead;
    };
    }
	return (pTail = NULL, NULL);
};
//
//[]------------------------------------------------------------------------[]
//
GResult GIniSection::Load (GIniFile   *	pIniFile ,
			   GIniFile::HSection
					hSection ,
			   const char *	pSecPath )
{
	Clear ();

    if (NULL == pIniFile)
	return ERROR_INVALID_PARAMETER;

    if (NULL == m_pBase )
	return ERROR_INSUFFICIENT_BUFFER;

    GResult	dResult;

    if (ERROR_SUCCESS == 
       (dResult	       = pIniFile ->Get (hSection, pSecPath, m_pBase, m_pHigh - m_pBase)))
    {
	m_pTail = m_pBase + strlen (m_pBase) + 1;
    }
	return  dResult;
};

GResult GIniSection::Save (GIniFile   * pIniFile ,
			   GIniFile::HSection
					hSection ,
			   const char * pSecPath )
{
    if (NULL == pIniFile)
	return ERROR_INVALID_PARAMETER;

    if (NULL == m_pBase )
	return ERROR_INSUFFICIENT_BUFFER;

	return pIniFile ->Put (hSection, pSecPath, m_pBase);
};

Bool GIniSection::Get (const char * pName, char * pBuf	   ,
					   DWord  dBufSize )
{
    char * pTail;
    char * pHead = find (pTail, pName);

    if (pHead && ((DWord) (pTail - pHead) < dBufSize))
    {
	memcpy (pBuf, pHead, pTail - pHead);

	* (pBuf + (pTail - pHead)) = '\0';

	return True ;
    }
	return False;
};
			
Bool GIniSection::Put (const char * pName, char * pBuf)
{
    char * pTail;
    char * pHead    = find (pTail, pName);

    size_t dBufSize = strlen (pBuf);

    if (pHead && (m_pTail - (pTail - pHead) + dBufSize < m_pHigh))
    {
	memmove  (pHead + dBufSize, pTail, m_pTail - pTail);
	memcpy   (pHead, pBuf, dBufSize);

	m_pTail = m_pBase + strlen (m_pBase) + 1;

	return True ;
    }
	return False;
};
//
//[]------------------------------------------------------------------------[]
//
Bool GIniSection::printf (const char * pName, const char * fmt, ...)
{
    int	      out ;
    va_list   arg ;    
    char      pBuf [1 * 4096];

    va_start (arg, fmt);

    if (0 < (out = ::_vsnprintf (pBuf, sizeof (pBuf), fmt, arg)))
    {
	return Put (pName, pBuf);
    }
	return False;
};

int GIniSection::scanf  (const char * pName, const char * fmt, ...)
{
//  int	      inp ;
    va_list   arg ;
    char      pBuf [1 * 4096];

    va_start (arg, fmt);

    if (Get (pName, pBuf, sizeof (pBuf)))
    {
    }
	return 0;//inp;
};
//
//[]------------------------------------------------------------------------[]












//[]------------------------------------------------------------------------[]
//


/*
    STDMETHOD  (     QueryInterface)(REFIID, void **);

    STDMETHOD_ (ULONG,	     AddRef)();

    STDMETHOD_ (ULONG,	    Release)();

    STDMETHOD_ (GResult,     Create)(Handle    *& hSection  ,
				     Handle     * hParent   ,
				     const char * pSecPath  ,
				     Bool	  bCreate   );

    STDMETHOD_ (GResult,     Delete)(Handle	* hParent   ,
				     const char * pSecPath  );
*/



/*
    struct SectionHandle : public IIniFile::Handle
    {
    STDMETHOD_ (void,	      Close)();

    STDMETHOD_ (GResult,	Get)(const char * pSecPath  ,
					   char * pBuf	    ,
					  DWord	  dBufSize  );

    STDMETHOD_ (GResult,	Put)(const char * pSecPath  ,
				           char * pBuf	    );

    STDMETHOD_ (void, SetNotificator)(Notification_Mask	    ,
				      CallBack ** pCallBack ,
					  void *  pContext  );
    };
*/

//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//



//
//[]------------------------------------------------------------------------[]













// 1.35 1.005

void Translate (char * v)
{
    GStreamBuf	s (v,  lstrlen (v));
	        s.put (lstrlen (v));

    int		UA, IF, IA, c;


		printf ("\n\n\n\t\t\t........Translate........\n\n\n");

lNextUA :
	     s.ScanfA (" \t\r\n", NULL);
    if (1 != s.ScanfA (" \t", "<%d, ", & UA))
	return;

    printf ("UA %d : \n", UA);

lNextIF :
	     s.ScanfA (" \t\r\n", NULL);
    if (2 != s.ScanfA (" \t", "<%d,%d>", & IF, & IA))
	return;

    printf ("\t\t%3d <-> %3d\n", IF, IA);

	     s.ScanfA (" \t", NULL);
    if (',' == (c = s.GetChrA ()))
	goto lNextIF;
    else
    if ('>' !=  c)
	return;

	     s.ScanfA (" \t", NULL);

    if (',' == s.GetChrA ())
	goto lNextUA;

	     s.ScanfA (" \t\r\n", NULL);

    if (0 != s.sizeforget ())
    {
	return;
    }
		printf ("\n\n\n\t\t\t.....End Translate.......\n\n\n");
};

void TestProc ()
{
    GIniFile		    f;
    GIniFile::HSection	    h;

    TIniSection <4 * 1024>  s;

    char      v [4 * 1024];

    if (ERROR_SUCCESS == f.Create ("MDIGTest.ini", 16 * 1024))
    {
	if (ERROR_SUCCESS == f.Create (h, NULL, "URP_CC5/HVSettings/SFocus", 16 * 1024))
	{
	if (ERROR_SUCCESS == s.Load   (& f, h, NULL))
	{
	    if (s.Get ("Some new creation Value", v, sizeof (v)))
		printf ("---[%s]\n", v);

	    s.printf  ("Some new creation Value", " <\"This is new Value\", <%d,%d>>", 1024, 32);

	    if (s.Get ("Some new creation Value", v, sizeof (v)))
		printf ("---[%s]\n", v);

	    if (s.Get ("UAnodeMinMax", v, sizeof (v)))
		printf ("---[%s]\n", v);

	    if (s.Get ("IFilamentPre", v, sizeof (v)))
		printf ("---[%s]\n", v);

	    if (s.Get ("IFilamentMinMax", v, sizeof (v)))
		printf ("---[%s]\n", v);

	    if (s.Get ("UAnode (IFilament, IAnode)", v, sizeof (v)))
	    {
		printf ("---[%s]\n", v);

		Translate (v);

	    }

	    if (s.Get ("IAnodeMax", v, sizeof (v)))
		printf ("---[%s]\n", v);

	}
		s.Save   (& f, h, NULL);
	}
		printf ("\n\n");

/*
	f.Create (h, NULL, "URP_CC5");
	printf ("%08X\n", h);

	f.Create (h,    h, "HVSettings");
	printf ("%08X\n", h);

	f.Create (h,    h, "SFocus");
	printf ("%08X\n", h);

	f.Create (h,	h, "/../../HVSettings/SFocus");
	printf ("%08X\n", h);

	f.Create (h, NULL, "URP_CC5/HVSettings/BFocus/../SFocus");
	printf ("%08X\n", h);

	f.Create (h, NULL, "URP_CC5/HVSettings/BFocus");
	printf ("%08X\n", h);

	if (ERROR_SUCCESS == s.Load  (& f, h, NULL))
	{
	    if (s.Get ("UAnodeMinMax", v, sizeof (v)))
		printf ("---[%s]\n", v);

	    if (s.Get ("IFilamentPre", v, sizeof (v)))
		printf ("---[%s]\n", v);

	    if (s.Get ("IFilamentMinMax", v, sizeof (v)))
		printf ("---[%s]\n", v);

	    if (s.Get ("UAnode (IFilament, IAnode)", v, sizeof (v)))
		printf ("---[%s]\n", v);

	    if (s.Get ("IAnodeMax", v, sizeof (v)))
		printf ("---[%s]\n", v);
	}
*/

/*
	if (h)
	{
	    f.Get (h, NULL, pSec, sizeof (pSec));
	    printf ("<|%s|>", pSec);

	    f.Get (h, "/../BFocus", pSec, sizeof (pSec));
	    printf ("<|%s|>", pSec);

	    f.Get (h, "/../../", pSec, sizeof (pSec));
	    printf ("<|%s|>", pSec);
	}

	    printf ("\n\n");


	f.Create (h, NULL, "KKM_Default");
	printf ("%08X\n", h);

	if (h)
	{
	    f.Get (h, NULL, pSec, sizeof (pSec));
	    printf ("<|%s|>", pSec);
	}
*/
	    printf ("\n\n");

	f.Close ();
    }
};

//
//[]------------------------------------------------------------------------[]








//[]------------------------------------------------------------------------[]
//
class SetupData
{
public :
	void		SetParam	(void *, DWord dValue);

	void		SetParamBits	(void *, DWord dClear, DWord dSet);

protected :

virtual	Bool		PerformSetParam	(void *, DWord dValue);

protected :
};

Bool SetupData::PerformSetParam (void *, DWord)
{
    return False;
};
//
//[]------------------------------------------------------------------------[]


#endif