
//[]------------------------------------------------------------------------[]
// MDIGConsole CUrpPanel dialog defintion
//
//
// Created by GSh.
//[]------------------------------------------------------------------------[]

#include "MDIGUtils.h"

#define	CALMSC_RELEASE	    1

#define	MSCAN_ONLY_MODE	    1

static	int		s_iDetCalibSet = 0;

static void GetBasePath (TCHAR * pPath)
{
    TCHAR   pBase [MAX_PATH] = {0};

    ::GetEnvironmentVariable (_T("XPROM_ROOT"), pBase, sizeof (pBase) / sizeof (TCHAR));
    strcat (pBase, _T("\\bin"));

    strcpy (pPath, pBase);
};

static void GetNVCalibratePath (TCHAR * pPath, int iDetCalibSet)
{
    GetBasePath (pPath);

    strcat (pPath, "\\NVCalibrate");

    switch (iDetCalibSet)
    {
    case -2: strcat (pPath, "\\_L90"); break;
    case -1: strcat (pPath, "\\_L45"); break;
    case  1: strcat (pPath, "\\_R45"); break;
    case  2: strcat (pPath, "\\_R90"); break;
//  case  0:
    default : break;
    };
};

static void GetNVCalibrateIFPath (TCHAR * pPath)
{
    GetNVCalibratePath (pPath, 0);

    strcat (pPath, "\\Temp");
};

static void GetNVCalibrateUAPath (TCHAR * pPath, int iDetCalibSet, Bool bFocus, int nUA)
{
    TCHAR   pName [MAX_PATH];

    GetNVCalibratePath (pPath, iDetCalibSet);

    sprintf (pName, "\\%s\\%d", bFocus ? "BF" : "SF", nUA);
    strcat  (pPath, pName);
};

static void EraseDir (TCHAR * pPath, Bool bSubDirs = False)
{
    TCHAR   pName [MAX_PATH];

    HANDLE	    hf;
    WIN32_FIND_DATA fi;

    strcpy (pName, pPath);
    strcat (pName, _T("\\*.*"));

    for (hf = ::FindFirstFile (pName, & fi); hf && (hf != INVALID_HANDLE_VALUE);)
    {
    if  (fi.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
    {
    if  (bSubDirs && strcmp (fi.cFileName, ".") && strcmp (fi.cFileName, ".."))
    {
	strcpy (pName, pPath	   );
	strcat (pName, _T("\\")	   );
	strcat (pName, fi.cFileName);

	EraseDir (pName, bSubDirs  );

	::RemoveDirectory (pName);
    }
    }
    else
    {
	strcpy (pName, pPath	   );
	strcat (pName, _T("\\")	   );
	strcat (pName, fi.cFileName);

	::DeleteFile (pName);
    }
	if (::FindNextFile (hf, & fi))
	{
	    continue;
	}
	    ::FindClose	   (hf);

	    break   ;
    }
};

static void RemoveDir (TCHAR * pPath)
{
    EraseDir (pPath);

    ::RemoveDirectory (pPath);
};

static void PrepareNextIF ()
{
    TCHAR   pPath [MAX_PATH];

    GetNVCalibrateIFPath (pPath);

    EraseDir		 (pPath);
    ::CreateDirectory	 (pPath, NULL);
};

static void PrepareNextUA (Bool bFocus, int nUAnode)
{
    TCHAR   pPath [MAX_PATH];

    GetNVCalibrateUAPath (pPath, s_iDetCalibSet, bFocus, nUAnode);

    EraseDir		 (pPath);
    ::CreateDirectory	 (pPath, NULL);
};

static void PrepareFirst (Bool bFocus)
{
    TCHAR   pPath [MAX_PATH];

    GetNVCalibratePath (pPath, s_iDetCalibSet);

    strcat (pPath, bFocus ? "\\BF" : "\\SF");

//  EraseDir (pPath, True);
};
//
//[]--------------------------------------------------------------------------

//[]--------------------------------------------------------------------------
//
#define FLR_HEADER_MARKER      ((WORD) ('R' << 8) | 'F')

      typedef struct tagFLRFILEHEADER { 
			WORD	ffType;
			WORD	ffWidth;
			WORD	ffHeight;
			WORD	ffBpp;
			WORD	ffBitsOffset;
      } FLRFILEHEADER;

#define	IMAGE_RULE_WIDTH (1537		   * 1)
#define IMAGE_LINE_WIDTH (IMAGE_RULE_WIDTH * 3)
#define	RAW_IMAGE_SIZEX	 (IMAGE_LINE_WIDTH * 2)

GResult GAPI SaveImageAsyncProc (int nArgs, void ** pArgs, HANDLE hResume)
{
    HANDLE  hFile	;
    Word  * pImageBuf	;
    DWord   dImageSize	;
    HANDLE  hCancel	;

    hFile	    = (HANDLE ) pArgs [0];
    pImageBuf	    = (Word  *)	pArgs [1];
    dImageSize	    = (DWord  ) pArgs [2];
    hCancel	    = (HANDLE ) pArgs [3];

    if (hResume)
    {
	::SetEvent    (hResume);
    }
    
    GResult	      r	 = ERROR_SUCCESS;
    DWORD	      t	      ;
    int		      nSizeY  ;
    FLRFILEHEADER     flrHead ;

    flrHead.ffType	 =    FLR_HEADER_MARKER;

    flrHead.ffWidth	 = ((RAW_IMAGE_SIZEX / 4) * 4) / 2;
    flrHead.ffHeight	 =
	    nSizeY	 = ( dImageSize / RAW_IMAGE_SIZEX) 
			 + ((dImageSize % RAW_IMAGE_SIZEX) ? 1 : 0);

    flrHead.ffBpp	 =   16;
    flrHead.ffBitsOffset =    0;

    DWord   RowLen	 = flrHead.ffWidth * 2;
    Word    pBuf	  [roundup (RAW_IMAGE_SIZEX, 4)];


    {
	    int i, j;

	if (! ::WriteFile (hFile, & flrHead, sizeof (flrHead), & t, NULL) || (t != sizeof (flrHead)))
	{   r = ::GetLastError (); goto lExit;}

	for	(i = 0; i < nSizeY	    ; i ++)
	{
	    for (j = 0; j < IMAGE_RULE_WIDTH; j ++)
	    {
		pBuf [j + (IMAGE_RULE_WIDTH * 1)    ] = * pImageBuf ++;
		pBuf [j + (IMAGE_RULE_WIDTH * 0)    ] = * pImageBuf ++;
		pBuf [    (IMAGE_RULE_WIDTH * 3) - j] = * pImageBuf ++;
	    }

	    if (! ::WriteFile (hFile, pBuf, RowLen, & t, NULL) || (t != RowLen))
	    {   r = ::GetLastError (); goto lExit;}

	    if (hCancel && (WAIT_TIMEOUT == ::WaitForSingleObject (hCancel, 0)))
	    {   r = ERROR_CANCELLED  ; goto lExit;}
	}
    }

lExit :

    return r;
};

HANDLE SaveImageAsync (void * hFile, const void * pImageBuf ,
					   DWord  dImageSize,
					   HANDLE hCancel   )
{
    HANDLE  t     = NULL;

    void *  p [4] = {(void *) hFile, (void *) pImageBuf  ,
				     (void *) dImageSize , 
				     (void *) hCancel    };
    CreateThread <GThread>
   (& t, SaveImageAsyncProc, 4, p, (256 * 1024)); 

    return  t;
};
//
//[]--------------------------------------------------------------------------
//
void SaveImage (HWND hParent, const void * pImageBuf, DWord dImageSize)
{
    DWORD   dExitCode = ERROR_SUCCESS	;
    HANDLE  hFile			;
    HANDLE  hSave			;
    TCHAR   pFileName [MAX_PATH] = {""} ;

    if (PromptFileName (hParent, pFileName, MAX_PATH, "flr\0*.flr\0"))
    {
    if (INVALID_HANDLE_VALUE != (hFile = ::CreateFile 
				(pFileName	       ,
				 GENERIC_WRITE	       ,
				 0		       ,
				 NULL		       ,
				 CREATE_ALWAYS	       ,
				 FILE_ATTRIBUTE_NORMAL 
			       | FILE_FLAG_SEQUENTIAL_SCAN,
				 NULL		       )))
    {
	if (NULL != (hSave = SaveImageAsync (hFile, pImageBuf, dImageSize, NULL)))
	{
	    HCURSOR	  hOld = 
	    ::SetCursor	 (::LoadCursor (NULL, IDC_WAIT));

	    while (WAIT_TIMEOUT == ::WaitForSingleObject (hSave, 0))
	    {
 		if (TCurrent () ->GetCurApartment () ->PumpInputWork ())
		{
		    TCurrent () ->GetCurApartment () ->Work ();

		    continue;
		}
		    TCurrent () ->GetCurApartment () ->WaitInputWork (20);
	    }

	    ::SetCursor	  (hOld);

	    ::GetExitCodeThread
			  (hSave, & dExitCode);

	    ::CloseHandle (hSave);
	}
	    ::CloseHandle (hFile);

	if (ERROR_SUCCESS != dExitCode)
	{
	    ::DeleteFile  (pFileName);
	}
    }
    }
};
//
//[]--------------------------------------------------------------------------

//[]------------------------------------------------------------------------[]
//
void SendImage (HWND hParent, IRRPMScan * pIRRPMScan)
{
    DWORD   dExitCode = 2		;
    HANDLE  hFile			;
    HANDLE  hSave			;

    void *  pImageBuf			;
    DWord   dImageSize			;
    
    if (0 == (dImageSize = pIRRPMScan ->GetDataPtr (pImageBuf, 0, 64 * (1024 * 1024))))
    {
	return;
    }

    SECURITY_ATTRIBUTES sa = {sizeof (SECURITY_ATTRIBUTES), NULL, True};

    TCHAR   pPathName [MAX_PATH] = {""} ;
    TCHAR   pFileName [MAX_PATH] = {""} ;

    ::GetTempPath (sizeof (pPathName) / sizeof (TCHAR), pPathName);
    ::GetTempFileName	  (pPathName, _T("raw"),     0,	pFileName);

    if (True)
    {
    if (INVALID_HANDLE_VALUE != (hFile = ::CreateFile 
				(pFileName	       ,
			         GENERIC_WRITE
			       | GENERIC_READ	       ,
				 FILE_SHARE_READ       ,
			       & sa		       ,
				 CREATE_ALWAYS	       ,
				 FILE_ATTRIBUTE_NORMAL 
			       | FILE_FLAG_SEQUENTIAL_SCAN
			       | FILE_FLAG_DELETE_ON_CLOSE,
				 NULL		       )))
    {
	if (NULL != (hSave = SaveImageAsync (hFile, pImageBuf, dImageSize, NULL)))
	{
	    HCURSOR	  hOld = 
	    ::SetCursor	 (::LoadCursor (NULL, IDC_WAIT));

	    while (WAIT_TIMEOUT == ::WaitForSingleObject (hSave, 0))
	    {
 		if (TCurrent () ->GetCurApartment () ->PumpInputWork ())
		{
		    TCurrent () ->GetCurApartment () ->Work ();

		    continue;
		}
		    TCurrent () ->GetCurApartment () ->WaitInputWork (20);
	    }

	    ::SetCursor	  (hOld);

	    ::GetExitCodeThread 
			  (hSave, & dExitCode);

	    ::CloseHandle (hSave);
	}
	if (ERROR_SUCCESS == dExitCode)
	{
	    ExecContrlEng (hFile);
	}
	    ::CloseHandle (hFile);
    }
    }
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
// Local classes
//
//[]------------------------------------------------------------------------[]
//
class CMMotorParam : public GDialog
{
public :
		CMMotorParam () : GDialog (NULL)
		{};
protected :

	Bool		On_DlgMsg	    (GWinMsg &);

protected :

	_U32		    m_uRegStatus    ;

	CEditHandler	    m_hEdit	    ;
};

Bool CMMotorParam::On_DlgMsg (GWinMsg & m)
{
    int		nCtrlId;
    TCHAR	sBuf [64];

//  if (m_hEdit.On_ReflectWinMsg (m))
//  {
//	return  On_Edt (m);
//  }

    if (0 !=   (nCtrlId = IsCommand (m)))
    {
    if (m_hEdit.On_ReflectWinMsg (m))
    {
	GetCtrlText (nCtrlId, sBuf, sizeof (sBuf) / sizeof (TCHAR));

	switch (nCtrlId)
	{
	case IDC_MMSC_PARAM_DELAY_EDT	   :
	case IDC_MMSC_PARAM_V0_EDT	   :
	case IDC_MMSC_PARAM_ACCELTIME_EDT  :
	case IDC_MMSC_PARAM_V_EDT	   :
	case IDC_MMSC_PARAM_SHOTROWS_EDT   :
	case IDC_MMSC_PARAM_STEPPERDEC_EDT :
	
	    ;
	};

	return True;
    }
	switch (nCtrlId)
	{
	case IDOK     :
	case IDCANCEL :
	    ::SetWindowLong (GetHWND (), GWL_USERDATA, (nCtrlId));
	    return (TCurrent () ->GetCurApartment () ->BreakFromWorkLoop (), True);
	};
    }

	return GDialog::On_DlgMsg (m);
};
//
//[]------------------------------------------------------------------------[]
//
class CMMotorPanel : public GDialog
{
public :
		CMMotorPanel (_U32 uDeviceId, int nStep, int nStepPerDec) : GDialog (NULL)
		{
		    m_uDeviceId	    = uDeviceId	    ;
		    m_hIRRPCallBack = NULL	    ;
		    m_pIRRPMMotor   = NULL	    ;

		    m_nStep	    = nStep	    ;
		    m_nStepPerDec   = nStepPerDec   ;

		    m_iSetVelocity  =
		    m_iSetVelocity0 =
		    m_iSetDelay	    =
		    m_iSetAccelTime = 0		;

		    m_dMoveStart    = INFINITE	;


		    m_uRegStatus    = 0		;
		    m_uRegD7D6	    = 0		;
		};

	friend class CMScanPanel;
	friend class CCalMScan	;

	void		SetEquMask		(Bool);

protected :

	Bool		On_DlgMsg		(GWinMsg &);

	void		On_Destroy		();
	Bool		On_Create		(void *);

protected :

	Bool		PreprocMessage		(MSG *);

	Bool		On_RadMode		(GWinMsg &, int iIndex);
	Bool		On_ChkExtSync		(GWinMsg &, Bool bFalg);
	Bool		On_ChkMovSync		(GWinMsg &, Bool bFlag);
	Bool		On_ChkBackDC		(GWinMsg &, Bool bFlag);

	Bool		On_BtnMove		(GWinMsg &, int nSteps );
	Bool		On_Edt			(GWinMsg &);

	void		On_IRRPParam		(_U32, _U32);
	void		On_IRRPCallBack		(IRRPCallBack::Handle *);

protected :

	_U32			m_uDeviceId	;

	CLedIndicator		m_oLedOnline	;
	CLedIndicator		m_oLedBegPin	;
	CLedIndicator		m_oLedEndPin	;

	CWndCallBack		m_sIRRPCallBack ;
	IRRPCallBack::Handle *	m_hIRRPCallBack	;

	IRRPMMotor *		m_pIRRPMMotor	;

	CEditHandler		m_hEdit		;

	int			m_nStep		;
	int			m_nStepPerDec	;

	int			m_iSetVelocity	;
	int			m_iSetVelocity0	;
	int			m_iSetDelay	;
	int			m_iSetAccelTime ;
	int			m_iSetShotRows	;

	DWord			m_dMoveStart	;

	_U32			m_uRegStatus	;

	_U32			m_uRegD7D6	;
};
//
//[]------------------------------------------------------------------------[]
//
static const int s_pMSCCalibHV [8] = {20, 22, 24, 26, 28, 30, 32, 34};

struct FitHeader
{
    int      Key;
    int      W;
    int      H;
    int      Yimg0;
    int      Yimg1;
    int      Ydc0;
    int      Ydc1;
};

struct FitInfo
{
    int		uHV    ;
    Bool	bExist ;
    FILETIME	ftCrt  ;

		FitInfo (int HV) : uHV (HV), bExist (False) {};
};

class CMScanPanel : public GDialog
{
public :
		CMScanPanel () : GDialog (NULL),

		m_DiafrPanel (0xD7000000, 200,  5),
		m_ScannPanel (0xD6000000, 400, 10)

		{
		    m_uReg0057	    = 0	;
		    m_uReg0058	    = 0	;
		};

	friend class CCalMScan;
	friend class CMMotorPanel;

	void		SetEquMask	    (DWord dEquMask);

	void		UpdateScannTitle    (const char * pText);

protected :

	Bool		On_DlgMsg	    (GWinMsg &);

	Bool		On_InitDialog	    (GWinMsg &);

	void		On_Destroy	    ();
	Bool		On_Create	    (void *);

	void		On_IRRPParam	    (_U32, _U32);
	void		On_GetVersion	    ();

protected :

	CMMotorPanel		m_DiafrPanel	;
	CMMotorPanel		m_ScannPanel	;

	CLedIndicator		m_oLedFGOnline	;
	CLedIndicator		m_oLedFGImage	;
	CLedIndicator		m_oLedFGData	;
	CLedIndicator		m_oLedFGDataOver;

	_U32			m_uReg0057	;
	_U32			m_uReg0058	;
};
//
//[]------------------------------------------------------------------------[]

//
//[]------------------------------------------------------------------------[]

void CMMotorPanel::On_Destroy ()
{
    if (m_pIRRPMMotor)
    {
	m_hIRRPCallBack ->Close ();
	m_hIRRPCallBack = NULL;

	m_pIRRPMMotor   ->Release ();
	m_pIRRPMMotor	= NULL;
    }
};

void CMMotorPanel::SetEquMask (Bool bIncFlag)
{
    if (bIncFlag && (NULL == m_pIRRPMMotor))
    {
    if (s_pIRRPService)
    {
	if (ERROR_SUCCESS == s_pIRRPService ->CreateRRPDevice 
				    ((void **) & m_pIRRPMMotor  ,
				      IID_IUnknown		,
				     (void * )   m_uDeviceId	,
				      IID_IUnknown		))
	{
	m_sIRRPCallBack.SetRecvHWND (GetHWND ());

	m_pIRRPMMotor ->Connect  (m_hIRRPCallBack, 
				& m_sIRRPCallBack);
	m_nStepPerDec = 
	m_pIRRPMMotor ->GetParam (0x0000DEDF);

	m_oLedOnline.SetColor (NULL, CLedIndicator::Red);
	m_oLedOnline.TurnOn   (GetCtrlHWND (IDC_MMSC_ONLINE_LED), True );

	m_oLedBegPin.SetColor (NULL, CLedIndicator::Gray);
	m_oLedBegPin.TurnOn   (GetCtrlHWND (IDC_MMSC_BEGPIN_LED), True );

	m_oLedEndPin.SetColor (NULL, CLedIndicator::Gray);
	m_oLedEndPin.TurnOn   (GetCtrlHWND (IDC_MMSC_ENDPIN_LED), True );

	PrintfCtrlText (IDC_MMSC_STEP_EDT   , "%d",  m_nStep);
	PrintfCtrlText (IDC_MMSC_STEPEX_EDT , "%d", ((m_nStep * 10000) / m_nStepPerDec) / 100);
	}
    }}
    else
    if (! bIncFlag && (NULL != m_pIRRPMMotor))
    {
	SetCtrlText    (IDC_MMSC_STEP_EDT   , "");
	SetCtrlText    (IDC_MMSC_STEPEX_EDT , "");

	m_oLedBegPin.TurnOn   (GetCtrlHWND (IDC_MMSC_BEGPIN_LED), False);
	m_oLedEndPin.TurnOn   (GetCtrlHWND (IDC_MMSC_ENDPIN_LED), False);

	m_oLedOnline.TurnOn   (GetCtrlHWND (IDC_MMSC_ONLINE_LED), False);

    if (m_hIRRPCallBack)
    {
	m_hIRRPCallBack ->Close ();
	m_hIRRPCallBack = NULL;
    }
	m_pIRRPMMotor   ->Release ();
	m_pIRRPMMotor   = NULL;
    }
};

Bool CMMotorPanel::On_Create (void * pParam)
{
    if (! GDialog::On_Create (pParam))
    {	return False;}


/*
    if (0xD6000000 == m_uDeviceId)
    {
	SetEquMask (False);
//	SetEquMask (FalseTrue );
    }
*/
/*
    if (s_pIRRPService)
    {
    if (ERROR_SUCCESS == s_pIRRPService ->CreateRRPDevice 
				((void **) & m_pIRRPMMotor  ,
				  IID_IUnknown		    ,
				 (void * )   m_uDeviceId    ,
				  IID_IUnknown		    ))
    {
	m_sIRRPCallBack.SetRecvHWND (GetHWND ());

	m_pIRRPMMotor ->Connect  (m_hIRRPCallBack, 
			        & m_sIRRPCallBack);
	m_nStepPerDec = 
	m_pIRRPMMotor ->GetParam (0x0000DEDF);
    }
    }
*/

// Look for this, for restore !!!
//
/*
    PrintfCtrlText (IDC_MMSC_STEP_EDT	, "%d",  m_nStep);
    PrintfCtrlText (IDC_MMSC_STEPEX_EDT , "%d", ((m_nStep * 10000) / m_nStepPerDec) / 100);
*/
/*
	::sprintf (sBuf, "%d",  m_nStep);
	SetCtrlText (IDC_MMSC_STEP_EDT  , sBuf);
	::sprintf (sBuf, "%d", ((m_nStep * 10000) / m_nStepPerDec) / 100);
	SetCtrlText (IDC_MMSC_STEPEX_EDT, sBuf);
*/
	return True ;
};

Bool CMMotorPanel::PreprocMessage (MSG * msg)
{
    m_hEdit.PreprocMessage (msg);

    return GDialog::PreprocMessage (msg);
};

/*
Bool CMMotorPanel::On_SetMotorParam (GWinMsg &, int iResId)
{
    RECT	    rw	     ;
    DWord	    uParam   ;
    CMMotorParam    Dlg	     ;
    TCHAR	    sBuf [64];

    if (m_pIRRPMMotor && Dlg.Create (this,
				     NULL, MAKEINTRESOURCE (iResId)))
    {
	AddPreprocHandler (& Dlg);

	::GetWindowRect  (GetCtrlHWND (IDC_MMSC_MOTOR_PARAMS_BTN), & rw, NULL);
	Dlg.SetWindowPos (NULL, rw.left + (rw.right - rw.left) / 2, rw.bottom, 0, 0, SWP_NOSIZE);

	    wsprintf (sBuf, "%d", m_iSetDelay	  = m_pIRRPMMotor ->GetParam (IRRPMMotor::ID_RegSetSyncDelay));
	    Dlg.SetCtrlText (IDC_MMSC_PARAM_DELAY_EDT	  , sBuf);

	    wsprintf (sBuf, "%d", m_iSetVelocity0 = m_pIRRPMMotor ->GetParam (IRRPMMotor::ID_RegSetDefCoeff ));
	    Dlg.SetCtrlText (IDC_MMSC_PARAM_V0_EDT	  , sBuf);

	    wsprintf (sBuf, "%d", m_iSetAccelTime = m_pIRRPMMotor ->GetParam (IRRPMMotor::ID_RegSetAccelTime));
	    Dlg.SetCtrlText (IDC_MMSC_PARAM_ACCELTIME_EDT , sBuf);

	    wsprintf (sBuf, "%d", m_iSetVelocity  = m_pIRRPMMotor ->GetParam (IRRPMMotor::ID_RegSetVelocity ));
	    Dlg.SetCtrlText (IDC_MMSC_PARAM_V_EDT	  , sBuf);

	    wsprintf (sBuf, "%d", m_iSetShotRows  = m_pIRRPMMotor ->GetParam (IRRPMMotor::ID_RegSetShotRows));
	    Dlg.SetCtrlText (IDC_MMSC_PARAM_SHOTROWS_EDT, sBuf);

	    wsprintf (sBuf, "%d", m_nStepPerDec	  = m_pIRRPMMotor ->GetParam (0x0000DEDF));
	    Dlg.SetCtrlText (IDC_MMSC_PARAM_STEPPERDEC_EDT, sBuf);

	    Dlg.ExecWindow  ();

	if (IDOK == ::GetWindowLong (Dlg.GetHWND (), GWL_USERDATA))
	{
	    Dlg.GetCtrlText (IDC_MMSC_PARAM_DELAY_EDT	  , sBuf, sizeof (sBuf) / sizeof (TCHAR));
	    if (1 == sscanf (sBuf, "%d", & uParam))
	    RRPSetParam (m_pIRRPMMotor, IRRPMMotor::ID_RegSetSyncDelay, uParam);

	    Dlg.GetCtrlText (IDC_MMSC_PARAM_V0_EDT	  , sBuf, sizeof (sBuf) / sizeof (TCHAR));
	    if (1 == sscanf (sBuf, "%d", & uParam))
	    RRPSetParam (m_pIRRPMMotor, IRRPMMotor::ID_RegSetDefCoeff,	uParam);
	    
	    Dlg.GetCtrlText (IDC_MMSC_PARAM_ACCELTIME_EDT , sBuf, sizeof (sBuf) / sizeof (TCHAR));
	    if (1 == sscanf (sBuf, "%d", & uParam))
	    RRPSetParam (m_pIRRPMMotor, IRRPMMotor::ID_RegSetAccelTime, uParam);

	    Dlg.GetCtrlText (IDC_MMSC_PARAM_V_EDT	  , sBuf, sizeof (sBuf) / sizeof (TCHAR));
	    if (1 == sscanf (sBuf, "%d", & uParam))
	    RRPSetParam (m_pIRRPMMotor, IRRPMMotor::ID_RegSetVelocity,	uParam);

	    Dlg.GetCtrlText (IDC_MMSC_PARAM_SHOTROWS_EDT  , sBuf, sizeof (sBuf) / sizeof (TCHAR));
	    if (1 == sscanf (sBuf, "%d", & uParam))
	    RRPSetParam (m_pIRRPMMotor, IRRPMMotor::ID_RegSetShotRows,  uParam);

	    Dlg.GetCtrlText (IDC_MMSC_PARAM_STEPPERDEC_EDT, sBuf, sizeof (sBuf) / sizeof (TCHAR));
	    if (1 == sscanf (sBuf, "%d", & uParam))
	    RRPSetParam (m_pIRRPMMotor, 0x0000DEDF, m_nStepPerDec = uParam);
	}

	RemPreprocHandler (& Dlg);
    }

    return True;
};
*/

Bool CMMotorPanel::On_RadMode (GWinMsg &, int iIndex)
{
    if (m_pIRRPMMotor)
    {
	m_pIRRPMMotor ->SetParamBits (0x000000D6, 0x06, (0 == iIndex) ? 0x04 : 0x02);
    }
    return True;
};

Bool CMMotorPanel::On_ChkExtSync (GWinMsg &, Bool bFlag)
{
    if (m_pIRRPMMotor)
    {
	m_pIRRPMMotor ->SetParamBits (0x000000D6, 0x08, (bFlag) ? 0x08 : 0x00);
    }
    return True;
};

Bool CMMotorPanel::On_ChkMovSync (GWinMsg &, Bool bFlag)
{
    if (m_pIRRPMMotor)
    {
	m_pIRRPMMotor ->SetParamBits (0x000000D6, 0x10, (bFlag) ? 0x10 : 0x00);
    }
    return True;
};

Bool CMMotorPanel::On_ChkBackDC (GWinMsg &, Bool bFlag)
{
    if (m_pIRRPMMotor)
    {
	m_pIRRPMMotor ->SetParamBits (0x00000096, 0x01, (bFlag) ? 0x01 : 0x00);
    }
    return True;
};

Bool CMMotorPanel::On_BtnMove (GWinMsg & m, int nSteps)
{
    if (m_pIRRPMMotor)
    {{
	IRRPMMotor::RunCommand cmd = (0 == nSteps) ? IRRPMMotor::Stop 
						   :
				    ((0  < nSteps) ? IRRPMMotor::RunForward
						   : IRRPMMotor::RunBackward);

	m_pIRRPMMotor ->Run (cmd, (0 <= nSteps) ? nSteps : - nSteps);
    }}
    return True;
};

Bool CMMotorPanel::On_Edt (GWinMsg & m)
{
    int		nStepEx	 ;
    int		nCtrlId  ;
    TCHAR	sBuf [64];

    GetCtrlText (nCtrlId = LOWORD (m.wParam), sBuf, sizeof (sBuf) / sizeof (TCHAR));

    switch (nCtrlId)
    {
    case IDC_MMSC_STEP_EDT   :
	m_nStep = 200;
	::sscanf  (sBuf, "%d", &  m_nStep);
	::sprintf (sBuf, "%d",  ((m_nStep * 10000) / m_nStepPerDec) / 100);
	SetCtrlText (IDC_MMSC_STEPEX_EDT, sBuf);
	break;

    case IDC_MMSC_STEPEX_EDT :
	nStepEx = (200 * 100 * m_nStepPerDec);
	::sscanf  (sBuf, "%d", &  nStepEx);
	::sprintf (sBuf, "%d",    m_nStep = (nStepEx * 100 * m_nStepPerDec) / 10000);
	SetCtrlText (IDC_MMSC_STEP_EDT  , sBuf);
	break;
    };

    return False;
};

Bool CMMotorPanel::On_DlgMsg (GWinMsg & m)
{
    if (m_hIRRPCallBack && (CWndCallBack::RRPMSG == m.uMsg ))
    {
	On_IRRPCallBack	  ((IRRPCallBack::Handle *) m.lParam);

	return m.DispatchComplete ();
    }

    int		nCtrlId  ;

    if (m_hEdit.On_ReflectWinMsg (m))
    {
	return  On_Edt (m);
    }

    if (0 !=   (nCtrlId = IsCommand (m)))
    {
	switch (nCtrlId)
	{
	case IDC_MMSC_MODE0_RAD		: return On_RadMode	  (m, 0);
	case IDC_MMSC_MODE1_RAD		: return On_RadMode	  (m, 1);
	case IDC_MMSC_EXTSYNC_CHK	: return On_ChkExtSync	  (m, IsCtrlChecked (IDC_MMSC_EXTSYNC_CHK) ? False : True);
	case IDC_MMSC_MOVSYNC_CHK	: return On_ChkMovSync	  (m, IsCtrlChecked (IDC_MMSC_MOVSYNC_CHK) ? False : True);
//	case IDC_MMSC_MOTOR_PARAMS_BTN  : return On_SetMotorParam (m, 
//					(0xD7000000 == m_uDeviceId) ? IDD_MSC_DIAFR : IDD_MSC_SCAN_3x1537);
	case IDC_MMSC_BACKDC_CHK	: return On_ChkBackDC	  (m, IsCtrlChecked (IDC_MMSC_BACKDC_CHK ) ? False : True);


	case IDC_MMSC_BACKSTEP_BTN	: return On_BtnMove (m, - m_nStep);
	case IDC_MMSC_BACKEND_BTN	: return On_BtnMove (m,	   -15000);
	case IDC_MMSC_STOP_BTN		: return On_BtnMove (m,		0);
	case IDC_MMSC_FRWDSTEP_BTN	: return On_BtnMove (m,   m_nStep);
	case IDC_MMSC_FRWDEND_BTN	: return On_BtnMove (m,	    15000);
	};
    }

    if (0 !=   (nCtrlId = IsControlNotify (m)))
    {
	if (WM_DRAWITEM == m.uMsg)
	{
	switch (nCtrlId)
	{
	case IDC_MMSC_ONLINE_LED  :
	    return (m_oLedOnline.On_Draw (m.pDRAWITEMSTRUCT), True);
	case IDC_MMSC_BEGPIN_LED  :
	    return (m_oLedBegPin.On_Draw (m.pDRAWITEMSTRUCT), True);
	case IDC_MMSC_ENDPIN_LED  :
	    return (m_oLedEndPin.On_Draw (m.pDRAWITEMSTRUCT), True);
	};
	}
    }

	return GDialog::On_DlgMsg (m);
};
//
//[]------------------------------------------------------------------------[]
//
void CMMotorPanel::On_IRRPParam (_U32 uParamId, _U32 uValue)
{
	DWord	      t;
	int	      i;
	TCHAR	      pBuf [64];

    if ((m_uDeviceId | 0x00000000) == uParamId)
    {
	s_pMainPanel ->Send_WM (CWndCallBack::EQUMSG, (WPARAM) (0xD7000000 == m_uDeviceId) 
						? IRRPMDIG::EquMask_MDiafr
						: IRRPMDIG::EquMask_MScan , (LPARAM) (uValue & 0x03));

	m_oLedOnline.SetColor (GetCtrlHWND (IDC_MMSC_ONLINE_LED),
			      (uValue & 0x0002) ? CLedIndicator::Green
						: CLedIndicator::Red	);

	m_uRegStatus	= uValue;
//	::InvalidateRect (GetCtrlHWND (IDC_MMSC_ONLINE_LED), NULL, False);

	return;
    }

    if ((m_uDeviceId | 0x0000D7D6) == uParamId)
    {
	m_uRegD7D6   = uValue;

	switch (uValue & 0x0006)
	{
	case 0x0004 : i =  0; break;
	case 0x0002 : i =  1; break;
	default     : i = -1; break;
	};

	::CheckRadioButton (GetHWND (), IDC_MMSC_MODE0_RAD,
					IDC_MMSC_MODE1_RAD, IDC_MMSC_MODE0_RAD + i);

	CheckCtrl  (IDC_MMSC_EXTSYNC_CHK , uValue & 0x0008);
	CheckCtrl  (IDC_MMSC_MOVSYNC_CHK , uValue & 0x0010);

	m_oLedBegPin.SetColor (GetCtrlHWND (IDC_MMSC_BEGPIN_LED),
			      (uValue & 0x0200) ? CLedIndicator::Yellow : CLedIndicator::Gray);
	m_oLedEndPin.SetColor (GetCtrlHWND (IDC_MMSC_ENDPIN_LED),
			      (uValue & 0x0400) ? CLedIndicator::Yellow : CLedIndicator::Gray);

//	::InvalidateRect (GetCtrlHWND (IDC_MMSC_BEGPIN_LED), NULL, False);
//	::InvalidateRect (GetCtrlHWND (IDC_MMSC_ENDPIN_LED), NULL, False);

//	EnableCtrl (IDC_MMSC_BEGPIN_LED	 , uValue & 0x0200);
//	EnableCtrl (IDC_MMSC_ENDPIN_LED	 , uValue & 0x0400);

	EnableCtrl (IDC_MMSC_MOVEBACK_LED, uValue & 0x0100);
	EnableCtrl (IDC_MMSC_MOVEFRWD_LED, uValue & 0x0100);

	if (( (uValue & 0x0100)) && (m_dMoveStart == INFINITE))
	{
	    m_dMoveStart = ::GetTickCount ();

	    wsprintf (pBuf, "0.00");
	    SetCtrlText (IDC_MMSC_DISP_MOVETIME, pBuf);
	}
	if ((!(uValue & 0x0100)) && (m_dMoveStart != INFINITE))
	{
	    t = ElapsedTicks (m_dMoveStart, ::GetTickCount ());
	    m_dMoveStart = INFINITE;

	    wsprintf (pBuf, "%d.%03d ���.", t / 1000, t % 1000);
	    SetCtrlText (IDC_MMSC_DISP_MOVETIME, pBuf);
	}
    }

    if ((m_uDeviceId | 0x0000DEDF) == uParamId)
    {
	wsprintf (pBuf, "%d"   ,   - (short) (0x8000 - uValue));
	SetCtrlText (IDC_MMSC_DISP_POS,   pBuf);

	sprintf  (pBuf, "%.3f" , (((double)(- (short) (0x8000 - uValue))) / m_nStepPerDec));

//	wsprintf (pBuf, "%d.%d", (((- (short) (0x8000 - uValue)) * 100) / m_nStepPerDec) / 100,
//				 (((- (short) (0x8000 - uValue)) * 100) % m_nStepPerDec) / 100);

	SetCtrlText (IDC_MMSC_DISP_POSEX, pBuf);

	if (m_dMoveStart != INFINITE)
	{
	    t = ElapsedTicks (m_dMoveStart, ::GetTickCount ());

	    wsprintf (pBuf, "%d.%03d ���.", t / 1000, t % 1000);
	    SetCtrlText (IDC_MMSC_DISP_MOVETIME, pBuf);
	}
    }

	i = 0;

    if ((m_uDeviceId | 0x000000BE) == uParamId)
    {
	m_iSetDelay	= (int) uValue;
	i = 1;
    }
    if ((m_uDeviceId | 0x000000BD) == uParamId)
    {
	m_iSetVelocity0 = (int) uValue;
	i = 1;
    }
    if ((m_uDeviceId | 0x0000DCDD) == uParamId)
    {
	m_iSetAccelTime	= (int) uValue;
	i = 2;
    }
    if ((m_uDeviceId | 0x0000DADB) == uParamId)
    {
	m_iSetVelocity  = (int) uValue;
	i = 2;
    }
    if (i == 1)
    {
	wsprintf (pBuf, "D = %d / Vo = %d", m_iSetDelay, m_iSetVelocity0);
	SetCtrlText (IDC_MMSC_DISP_PARAM, pBuf);
    }
    if (i == 2)
    {
	wsprintf (pBuf, "Atime = %d / V = %d", m_iSetAccelTime, m_iSetVelocity);
	SetCtrlText (IDC_MMSC_DISP_PARAM2, pBuf);
    }

    if (0xD6000000 == m_uDeviceId)
    {
	((CMScanPanel *) GetParent ()) ->On_IRRPParam (uParamId, uValue);
    }
};

void CMMotorPanel::On_IRRPCallBack (IRRPCallBack::Handle * hIRRPCallBack)
{
    Word	    uLength	;
    Word	    uType	;
    IRRPProtocol::DataPack *
		    pDataPack	;
    IRRPProtocol::ParamPack::Entry * 
		    pEntry	;

    if (hIRRPCallBack == m_hIRRPCallBack)
    {
	pDataPack = (IRRPProtocol::DataPack *) m_sIRRPCallBack.PeekHeadPacket ();

    while (pDataPack)
    {
	uLength = getBigEndianW (pDataPack, offsetof (IRRPProtocol::DataPack, uLength));
	uType	= getBigEndianW (pDataPack, offsetof (IRRPProtocol::DataPack, uType  ));

	if (0  == uType)
	{
		pEntry = (IRRPProtocol::ParamPack::Entry *)(pDataPack + 1);

	    while ((Byte *) pEntry < ((Byte *) pDataPack + uLength))
	    {
		On_IRRPParam (getBigEndianD (pEntry, offsetof (IRRPProtocol::ParamPack::Entry, uParamId)),
			      getBigEndianD (pEntry, offsetof (IRRPProtocol::ParamPack::Entry, uValue  )));
		pEntry ++;
	    }
	}

	pDataPack = (IRRPProtocol::DataPack *) m_sIRRPCallBack.PeekNextPacket ();
    }
    }
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
void CMScanPanel::On_GetVersion (/*HWND hPage*/)
{
    IRRPMScan * pIRRPMScan = NULL;

    if (True)
    {
	if (ERROR_SUCCESS == s_pIRRPService ->CreateRRPDevice 
				      ((void **) & pIRRPMScan ,
					IID_IUnknown	      ,
				       (void * ) 0xD6000000   ,
					IID_IUnknown	      ))
	{
//    	    GTimeout  tTimeout;
//
//	    for (tTimeout.Activate (1000);;)
//	    {
//		if (0xFFFFFFFF != pIRRPMScan ->GetParam (IRRPMScan::ID_RegGetVersion))
//		{
//		    break;
//		}
//		if (tTimeout.Occured ())
//		{
//		    break;
//		}
//	    }

	    _U32 dPar, dPar2;
	    char pBuf [128];
//.....................................................................
//
	    EnableCtrl (IDTEST, False);

	    if (0xFFFFFFFF != (dPar = pIRRPMScan ->GetParam (IRRPMScan::ID_RegGetVersion )))
	    {
	    wsprintf (pBuf,		    "%c%c - ",		LOBYTE(LOWORD(dPar)), HIBYTE(LOWORD(dPar)),
								LOBYTE(HIWORD(dPar)));
			       dPar  = pIRRPMScan ->GetParam (IRRPMScan::ID_RegGetVersion2 );

	    wsprintf (pBuf + strlen (pBuf), "%1d.%1d, %1d.%1d", LOBYTE(LOWORD(dPar)), HIBYTE(LOWORD(dPar)),
								LOBYTE(HIWORD(dPar)), HIBYTE(HIWORD(dPar)));

	    if (0xFFFFFFFF != (dPar2 = pIRRPMScan ->GetParam (IRRPMScan::ID_RegGetVersion2a)))
	    {
		wsprintf (pBuf + strlen (pBuf), ", %1d.%1d", dPar2 / 10, dPar2 % 10);

		EnableCtrl (IDTEST, True);
	    }
	    }
	    else {strcpy (pBuf, "n/a");}

	    SetCtrlText (IDC_MMSC_VERTEXT, pBuf);
//.....................................................................
//
	    if (0xFFFFFFFF != (dPar = pIRRPMScan ->GetParam (IRRPMScan::ID_RegGetVersion3)))
	    {
	    wsprintf (pBuf,		    "%05d",		LOWORD(dPar));
	    }
	    else {strcpy (pBuf, "n/a");}
	    SetCtrlText (IDC_MMSC_VERTEXT2, pBuf);
//.....................................................................
//
	    if (0xFFFFFFFF != (dPar = pIRRPMScan ->GetParam (IRRPMScan::ID_RegGetVersion4)))
	    {
	    wsprintf (pBuf,		    "%4d",		LOWORD(dPar));
	    }
	    else {strcpy (pBuf,"n/a");}
	    SetCtrlText (IDC_MMSC_VERTEXT3, pBuf);
//.....................................................................
//
	    GetCtrlText (IDC_MMSC_VERTEXT3, pBuf, sizeof (pBuf));

	    if (0xFFFFFFFF != (dPar  = pIRRPMScan ->GetParam (IRRPMScan::ID_RegGetVersion5)))
	    {
	    wsprintf (pBuf + strlen (pBuf), " + %d",		LOWORD(dPar));
	    }
	    else {strcat (pBuf, " + n/a");}

	    if (0xFFFF != HIWORD ((dPar  = pIRRPMScan ->GetParam (IRRPMScan::ID_RegGetVersion4))))
	    {
	    wsprintf (pBuf + strlen (pBuf), "  (%d)",		LOBYTE(HIWORD(dPar)));
	    }
	    else {strcat (pBuf, " (n/a)");}

	    SetCtrlText (IDC_MMSC_VERTEXT3, pBuf);
//.....................................................................
//
	    if (0xFFFFFFFF != (dPar  = pIRRPMScan ->GetParam (IRRPMScan::ID_RegGetVersion5a)))
	    {
	    wsprintf (pBuf,		    "%3d",		LOWORD(dPar));
	    }
	    else {strcpy (pBuf,"n/a");}
	    SetCtrlText (IDC_MMSC_VERTEXT4, pBuf);

	    if (0xFFFF != HIWORD ((dPar  = pIRRPMScan ->GetParam (IRRPMScan::ID_RegGetVersion4))))
	    {
	    wsprintf (pBuf + strlen (pBuf), "  (%d)",		HIBYTE(HIWORD(dPar)));
	    }
	    else {strcat (pBuf, " (n/a)");}

	    SetCtrlText (IDC_MMSC_VERTEXT4, pBuf);
//.....................................................................
//
	    if (0xFFFFFFFF != (dPar  = pIRRPMScan ->GetParam (IRRPMScan::ID_RegGetVersion6)))
	    {{
		* pBuf = 0; int i = 0x80;

		for (; 0x08 < i; i >>= 1)
		{
		   wsprintf (pBuf + strlen (pBuf), "%c", (dPar & i) ? '1' : '0');
		}
		   wsprintf (pBuf + strlen (pBuf), ".");

		for (; 0x00 < i; i >>= 1)
		{
		   wsprintf (pBuf + strlen (pBuf), "%c", (dPar & i) ? '1' : '0');
		}
	    }}
	    else {strcpy (pBuf, "n/a");}
	    SetCtrlText (IDC_MMSC_VERTEXT5, pBuf);
//.....................................................................
//
	    pIRRPMScan ->Release ();
	}
    }
};

Bool CMScanPanel::On_DlgMsg (GWinMsg & m)
{
    if ((WM_COMMAND == m.uMsg) && (IDSAVE == LOWORD (m.wParam)))
    {{
	SendImage (GetHWND (), (IRRPMScan *) m_ScannPanel.m_pIRRPMMotor);

	return True;
    }}
    else
    if ((WM_COMMAND == m.uMsg) && (IDTEST == LOWORD (m.wParam)))
    {{
	((IRRPMScan *) m_ScannPanel.m_pIRRPMMotor) ->GetTestImage ();

	return True;
    }}

    int		nCtrlId;

    if (0 !=   (nCtrlId = IsControlNotify (m)))
    {
	if (WM_DRAWITEM == m.uMsg)
	{
	switch (nCtrlId)
	{
	case IDC_MMSC_FGONLINE_LED :
	    return (m_oLedFGOnline  .On_Draw (m.pDRAWITEMSTRUCT), True);
	case IDC_MMSC_FGIMAGE_LED  :
	    return (m_oLedFGImage   .On_Draw (m.pDRAWITEMSTRUCT), True);
	case IDC_MMSC_FGDATA_LED   :
	    return (m_oLedFGData    .On_Draw (m.pDRAWITEMSTRUCT), True);
	case IDC_MMSC_FGDATAOVERFLOW_LED :
	    return (m_oLedFGDataOver.On_Draw (m.pDRAWITEMSTRUCT), True);
	case IDC_MMSC_IMAGE :
	    {{
		DRAWITEMSTRUCT *  ds =
	       (DRAWITEMSTRUCT *) m.lParam;

		RECT rw = ds ->rcItem;

		::DrawEdge (ds ->hDC, & rw, EDGE_SUNKEN, BF_RECT | BF_ADJUST);
		::FillRect (ds ->hDC, & rw, (HBRUSH) ::GetStockObject (BLACK_BRUSH));

		return True;
	    }}
	};
	}
    }
    return GDialog::On_DlgMsg (m);
};

Bool CMScanPanel::On_InitDialog (GWinMsg & m)
{
/*
    SendCtrlMessage (IDC_MMSC_DISP_IMAGE  , WM_SETFONT, (WPARAM) ::GetBoaldGUIFont (), (LPARAM) 0);
    SendCtrlMessage (IDC_MMSC_DISP_IMAGEEX, WM_SETFONT, (WPARAM) ::GetBoaldGUIFont (), (LPARAM) 0);
*/
    return m.DispatchCompleteDlgEx (False);
};

void CMScanPanel::On_Destroy ()
{
    m_DiafrPanel.Destroy ();
    m_ScannPanel.Destroy ();
};

Bool CMScanPanel::On_Create (void * pParam)
{
    RECT    rw;
    TCHAR   pString [128];

    if (! GDialog::On_Create (pParam))
    {	return False;}
//
//............................    
//
    if (! m_DiafrPanel.Create (this,
			       NULL, MAKEINTRESOURCE (IDD_MSC_MOTOR)))
    {	return False;}

    ::GetWindowText (GetCtrlHWND (IDC_DIAFR_PANEL), pString, sizeof (pString) / sizeof (TCHAR));
    ::SetWindowText (m_DiafrPanel.
		     GetCtrlHWND (IDC_MMSC_TITLE ), pString);

    ::GetWindowRect (GetCtrlHWND (IDC_DIAFR_PANEL), & rw, GetHWND ());
    m_DiafrPanel.SetWindowPos 
		    (NULL, rw.left, rw.top, 0, 0, SWP_NOSIZE);
//
//............................    
//
    if (! m_ScannPanel.Create (this,
			       NULL, MAKEINTRESOURCE (IDD_MSC_MOTOR)))
    {	return False;}

    ::GetWindowText (GetCtrlHWND (IDC_SCANN_PANEL), pString, sizeof (pString) / sizeof (TCHAR));
    ::SetWindowText (m_ScannPanel.
		     GetCtrlHWND (IDC_MMSC_TITLE ), pString);

    ::GetWindowRect (GetCtrlHWND (IDC_SCANN_PANEL), & rw, GetHWND ());
    m_ScannPanel.SetWindowPos 
		    (NULL, rw.left, rw.top, 0, 0, SWP_NOSIZE);

    ::SetWindowLong (m_ScannPanel.
    GetCtrlHWND (IDC_MMSC_BACKDC_CHK), GWL_STYLE, 
    
		 WS_VISIBLE | 

    ::GetWindowLong (m_ScannPanel.
    GetCtrlHWND (IDC_MMSC_BACKDC_CHK), GWL_STYLE));

	return True ;
};

void CMScanPanel::UpdateScannTitle (const char * pTextEx)
{
    TCHAR pText [128];

    ::GetWindowText (GetCtrlHWND (IDC_SCANN_PANEL), pText, sizeof (pText) / sizeof (TCHAR));

    ::lstrcat (pText, (pTextEx) ? pTextEx : "");

    ::SetWindowText (m_ScannPanel.GetCtrlHWND (IDC_MMSC_TITLE ), pText);
};

void CMScanPanel::On_IRRPParam (_U32 uParamId, _U32 uValue)
{
    if (0xD6000057 == uParamId)
    {
	m_uReg0057  = uValue;

	s_pMainPanel ->Send_WM	  (CWndCallBack::EQUMSG, (WPARAM) IRRPMDIG::EquMask_MFG,
		  (LPARAM) 0x01 | ((uValue & IRRPMScan::FGStatus_Online) ? 0x02 : 0x00));

	m_oLedFGOnline.SetColor	  (GetCtrlHWND (IDC_MMSC_FGONLINE_LED),
				   (uValue & IRRPMScan::FGStatus_Online  ) ? CLedIndicator::Green
									   : CLedIndicator::Red  );

	m_oLedFGDataOver.TurnOn (GetCtrlHWND (IDC_MMSC_FGDATAOVERFLOW_LED),
				(uValue & IRRPMScan::FGStatus_Overflow) ? True : False);

	m_oLedFGData	.TurnOn (GetCtrlHWND (IDC_MMSC_FGDATA_LED),
				(uValue & IRRPMScan::FGStatus_DataSof ) ? True : False);

	m_oLedFGImage	.TurnOn (GetCtrlHWND (IDC_MMSC_FGIMAGE_LED),
				(uValue & IRRPMScan::FGStatus_DataReady ) ? True : False);
//				(uValue & IRRPMScan::FGStatus_DataEof ) ? True : False);

	EnableCtrl (IDSAVE,	(uValue & IRRPMScan::FGStatus_DataReady ) ? True : False);
//	EnableCtrl (IDSAVE,	(uValue & IRRPMScan::FGStatus_DataEof ) ? True : False);
    }
    if (0xD6000058 == uParamId)
    {
	m_uReg0058  = uValue;

	PrintfCtrlText (IDC_MMSC_DISP_IMAGE,   "%d.%d", uValue / ((1537 * 3) * 2),
							uValue % ((1537 * 3) * 2));
    }
    if (0xD6000059 == uParamId)
    {
	PrintfCtrlText (IDC_MMSC_DISP_IMAGEEX, "%d"   , uValue / 2);
    }
    if (0xD6000066 == uParamId)
    {{
	TCHAR pString [128] = "";

	::wsprintf (pString + ::strlen (pString), " %d.%d", HIBYTE(HIWORD(uValue)), LOBYTE(HIWORD(uValue)));

	::EnableWindow (m_ScannPanel.GetCtrlHWND (IDC_MMSC_BACKDC_CHK), (1 < HIBYTE(HIWORD(uValue))) ? True : False);

	On_GetVersion  ();

//	if (0xFFFF != LOWORD (uValue))
//	{
//	    ::wsprintf (pString + ::strlen (pString), ".%d", LOWORD (uValue));
//	}
//	else
//	{
//	    ::wsprintf (pString + ::strlen (pString), ".n/a");
//	}
//	if (0xFFFFFFFF != (dParam = ((IRRPMScan *) m_ScannPanel.m_pIRRPMMotor) ->GetParam (IRRPMScan::ID_RegGetVersion5)))
//	{
//	    ::wsprintf (pString + ::strlen (pString), " + %d", LOWORD (dParam));
//	}
//	    UpdateScannTitle (pString);
    }}
    if (0xD6000067 == uParamId)
    {
	m_ScannPanel.CheckCtrl (IDC_MMSC_BACKDC_CHK, (uValue & 0x01) ? False : True);
    }
};

void CMScanPanel::SetEquMask (DWord dEquMask)
{
    if (dEquMask & IRRPMDIG::EquMask_MScan)
    {
	m_oLedFGOnline.SetColor (NULL, CLedIndicator::Red);
	m_oLedFGOnline.TurnOn	(GetCtrlHWND (IDC_MMSC_FGONLINE_LED), True );
    }
    else
    {
	::EnableWindow (m_ScannPanel.GetCtrlHWND (IDC_MMSC_BACKDC_CHK), False);

	m_oLedFGOnline.TurnOn	(GetCtrlHWND (IDC_MMSC_FGONLINE_LED), False);
    }
    m_ScannPanel.SetEquMask (dEquMask & IRRPMDIG::EquMask_MScan );

    m_DiafrPanel.SetEquMask (dEquMask & IRRPMDIG::EquMask_MDiafr);
};
//
//[]------------------------------------------------------------------------[]


































//[]------------------------------------------------------------------------[]
//
#define	UA_MIN_VALUE	20
#define UA_MAX_VALUE	35

#define IF_MIN_VALUE	280
#define IF_MAX_VALUE	480

static	int		s_iDCBeg	;
static	int		s_iDCEnd	;
static	int		s_iActiveBeg	;
static	int		s_iActiveEnd	;
static	int		s_iKX		;
static	int		s_iKY		;

class CCalMScan : public GDialog
{
public :
		CCalMScan (int iDetCalibSet, Bool bFocus, int nFitInfo, FitInfo ** pFitInfo);

	void		PrintfStatus		(int nRow, LPCTSTR fmt, ...);

protected :

	void		On_SelectFocus		(int);
	void		On_Rotate		();

	void		On_Press		(int);

	Bool		On_Edt			(GWinMsg &);

	Bool		On_DlgMsg		(GWinMsg &);

	void		On_Destroy		();
	Bool		On_Create		(void *);
	
	void		On_IRRPParam		(_U32, _U32);
	void		On_IRRPCallBack		(IRRPCallBack::Handle *);

//.....................................
//
	GIrp *		Co_ShotCycle2		(GIrp *, void *);
	GIrp *		Co_ShotCycle		(GIrp *, void *);

	GIrp *		Co_CalWaitAsyncCall	(GIrp *, void *);

	GIrp *		Co_CalStatLoop		(GIrp *, void *);
	GIrp *		Co_CalIFLoop		(GIrp *, void *);

	GIrp *		Co_CalWaitImageEof	(GIrp *, void *);
	GIrp *		Co_CalWaitImageSof	(GIrp *, void *);
	GIrp *		Co_CalWaitReady		(GIrp *, void *);

	GIrp *		Co_CalStep		(GIrp *, void *);

	GIrp *		Co_CalLoop		(GIrp *, void *);
	GIrp *		Co_CalProc		(GIrp *, void *);

	Bool		On_Start		();

	void		Do_ShotCycleCancel	();

	void		Do_CalCycleCancel	();
//
//.....................................

private :

//	void		SetUAValue		(Bool bHi  , int);
//	void		SetUAStep		(Bool bStep, int);

	void		SetIFValue		(Bool bHi  , int);
	void		SetIFStep		(Bool bStep, int);

protected :

	CEditHandler		m_hEdit		;

	int			m_nStatistic	;

	Bool			m_bCurFocus	;

	struct
	{
	    int			m_nFitInfo	;
	    FitInfo **		m_pFitInfo	;

	    void		Init	(int nFitInfo, FitInfo ** pFitInfo)
	    {
		m_nFitInfo = nFitInfo;
		m_pFitInfo = pFitInfo;
	    };

	    int			GetNum	 (     ) const { return m_nFitInfo; };

	    int			GetValue (int i) const { return m_pFitInfo [i] ->uHV;};

	}			m_UARange	;

//	GRange			m_UARange	;
	GRange			m_IFRange	;

	enum CalState
	{
	    Ready    = 0,
	    Work	,
	    Paused	,
	    Completed	,

	}   			m_uCalState	;

	void		SetCalState		(CalState);

public :


	IRRPUrp *		m_pIRRPUrp	;
	IRRPMScan  *		m_pIRRPMScan	;
	IRRPMMotor *		m_pIRRPMDiafr	;
	IRRPMM2 *		m_pIRRPMM2	;

	CWndCallBack		m_sIRRPxCallBack;
	IRRPCallBack::Handle *  m_hIRRPUCallBack;
	IRRPCallBack::Handle *	m_hIRRPDCallBack;
	IRRPCallBack::Handle *	m_hIRRPSCallBack;
	IRRPCallBack::Handle *	m_hIRRPMCallBack;

	GIrp *			m_pShotIrp	;

	GIrp *			m_pStpIrp	;
	GIrp *			m_pCalIrp	;

//	int			m_iUA_SF_Min	;
//	int			m_iUA_SF_Max	;
	int			m_iIF_SF_Min	;
	int			m_iIF_SF_Max	;

//	int			m_iUA_BF_Min	;
//	int			m_iUA_BF_Max	;
	int			m_iIF_BF_Min	;
	int			m_iIF_BF_Max	;

	int			m_iCurUAPoint	;
	int			m_iCurIFPoint	;
	int			m_iCurDC	;
	int			m_iCurStat	;

	_U32			m_uHVReady	;
	_U32			m_uMSStatus	;   // RegD7D6
	_U32			m_uMDStatus	;   // RegD7D6
	Bool			m_bMM2Disable	;   // ! (MM2 Connected & valid angle !!!)
	_U32			m_uReg0057	;
	_U32			m_uReg0058	;
};
//
//[]------------------------------------------------------------------------[]
//
void CCalMScan::PrintfStatus (int nRow, LPCTSTR fmt, ...)
{
    va_list arg	       ;
    TCHAR * pPtr       ;
    TCHAR   pBuf [4096];

    GetCtrlText (IDC_CALMSC_DISP_EDT, pBuf, sizeof (pBuf) / sizeof (TCHAR));

    for (pPtr = pBuf; (* pPtr) && (0 < nRow);)
    {
	if (* pPtr ++ == '\n')
	{
	    nRow --;
	}
    }

    va_start (arg, fmt);

    _vsnprintf (pPtr, (pBuf + sizeof (pBuf) - 2 - pPtr) / sizeof (TCHAR), fmt, arg);

    va_end   (arg);

    strcat   (pPtr, "\r\n");

    SetCtrlText (IDC_CALMSC_DISP_EDT, pBuf);
};

CCalMScan::CCalMScan (int iDetCalibSet, Bool bFocus, int nFitInfo, FitInfo ** pFitInfo) : GDialog (NULL)
{
    IRRPSettings * pIRRPSettings = NULL;
    s_pIRRPService ->CreateRRPSettings 
		  (pIRRPSettings);

    pIRRPSettings ->GetValue (IRRPSettings::ID_MSC_ImageDCBeg, & s_iDCBeg, sizeof (int), NULL);
    pIRRPSettings ->GetValue (IRRPSettings::ID_MSC_ImageDCEnd, & s_iDCEnd, sizeof (int), NULL);
    pIRRPSettings ->GetValue (IRRPSettings::ID_MSC_ImageActiveBeg, & s_iActiveBeg, sizeof (int), NULL);
    pIRRPSettings ->GetValue (IRRPSettings::ID_MSC_ImageActiveEnd, & s_iActiveEnd, sizeof (int), NULL);
    pIRRPSettings ->GetValue (IRRPSettings::ID_MSC_ImageKX, & s_iKX, sizeof (int), NULL);
    pIRRPSettings ->GetValue (IRRPSettings::ID_MSC_ImageKY, & s_iKY, sizeof (int), NULL);

//  pIRRPSettings ->GetValue (IRRPSettings::ID_URP_SHOT_UAnodeMin    , & m_iUA_SF_Min, sizeof (int), NULL);
//  pIRRPSettings ->GetValue (IRRPSettings::ID_URP_SF_UAnodeMax	     , & m_iUA_SF_Max, sizeof (int), NULL);
    pIRRPSettings ->GetValue (IRRPSettings::ID_URP_SF_IFilamentPreMax, & m_iIF_SF_Min, sizeof (int), NULL);
    pIRRPSettings ->GetValue (IRRPSettings::ID_URP_SF_IFilamentMax   , & m_iIF_SF_Max, sizeof (int), NULL);

//  pIRRPSettings ->GetValue (IRRPSettings::ID_URP_SHOT_UAnodeMin    , & m_iUA_BF_Min, sizeof (int), NULL);
//  pIRRPSettings ->GetValue (IRRPSettings::ID_URP_BF_UAnodeMax	     , & m_iUA_BF_Max, sizeof (int), NULL);
    pIRRPSettings ->GetValue (IRRPSettings::ID_URP_BF_IFilamentPreMax, & m_iIF_BF_Min, sizeof (int), NULL);
    pIRRPSettings ->GetValue (IRRPSettings::ID_URP_BF_IFilamentMax   , & m_iIF_BF_Max, sizeof (int), NULL);

//  if (m_iUA_SF_Min >= m_iUA_SF_Max)
//  {
//	m_iUA_SF_Min = UA_MIN_VALUE;
//	m_iUA_SF_Max = UA_MAX_VALUE;
//  }
    if (m_iIF_SF_Min >= m_iIF_SF_Max)
    {
	m_iIF_SF_Min = IF_MIN_VALUE;
	m_iIF_SF_Max = IF_MAX_VALUE;
    }

//  if (m_iUA_BF_Min >= m_iUA_BF_Max)
//  {
//	m_iUA_BF_Min = UA_MIN_VALUE;
//	m_iUA_BF_Max = UA_MAX_VALUE;
//  }
    if (m_iIF_BF_Min >= m_iIF_BF_Max)
    {
	m_iIF_BF_Min = IF_MIN_VALUE;
	m_iIF_BF_Max = IF_MAX_VALUE;
    }

    m_nStatistic     = 1	;

    s_iDetCalibSet   = iDetCalibSet;
    m_bCurFocus	     = bFocus	;

    m_UARange.Init   (nFitInfo, pFitInfo);
//  m_UARange.Init   (uHV - 1, uHV, 1);
//  m_UARange.SetNum (2);
   
    m_IFRange.Init   (m_iIF_BF_Min, m_iIF_BF_Max, 1);
    m_IFRange.SetNum (3);

    m_uCalState	  = Ready;

    m_pShotIrp	  =
    m_pStpIrp	  =
    m_pCalIrp	  = NULL ;

    m_pIRRPUrp	  = NULL ;
    m_pIRRPMScan  = NULL ;
    m_pIRRPMDiafr = NULL ;
    m_pIRRPMM2	  = NULL ;

    m_hIRRPUCallBack =
    m_hIRRPDCallBack =
    m_hIRRPSCallBack = 
    m_hIRRPMCallBack = NULL;

    m_uHVReady	  = IRRPUrp::HVReady_NotConnect ;
    m_uMSStatus	  =
    m_uMDStatus	  = 
    m_uReg0057	  = 
    m_uReg0058	  = 0	;

    m_bMM2Disable = 0x05;
};

void CCalMScan::On_SelectFocus (int /*bBigFocus*/)
{
//  int	iUACurNum = m_UARange.GetNum ();
    int	iIFCurNum = m_IFRange.GetNum ();

    if (m_bCurFocus)
    {
//	m_UARange.Init	 (m_iUA_BF_Min, m_iUA_BF_Max, 1);
//	m_UARange.SetNum (iUACurNum);

	m_IFRange.Init	 (m_iIF_BF_Min, m_iIF_BF_Max, 1);
	m_IFRange.SetNum (iIFCurNum);
    }
    else
    {
//	m_UARange.Init	 (m_iUA_SF_Min, m_iUA_SF_Max, 1);
//	m_UARange.SetNum (iUACurNum);

	m_IFRange.Init	 (m_iIF_SF_Min, m_iIF_SF_Max, 1);
	m_IFRange.SetNum (iIFCurNum);
    }

	SetCtrlText (IDC_CALMSC_UAN_EDT	    , m_UARange.GetNum  ());
//	SetCtrlText (IDC_CALMSC_UASTEP_EDT  , m_UARange.GetStep ());

	SetCtrlText (IDC_CALMSC_UALO_EDT    , m_UARange.GetValue (0));
	SetCtrlText (IDC_CALMSC_UAHI_EDT    , m_UARange.GetValue (m_UARange.GetNum () - 1));

	SetCtrlText (IDC_CALMSC_IFN_EDT	    , m_IFRange.GetNum  ());
	SetCtrlText (IDC_CALMSC_IFSTEP_EDT  , m_IFRange.GetStep ());

	SetCtrlText (IDC_CALMSC_IFLO_EDT    , m_IFRange.m_nLoValue);
	SetCtrlText (IDC_CALMSC_IFHI_EDT    , m_IFRange.m_nHiValue);
};

Bool CCalMScan::On_Create (void * pParam)
{
    if (! GDialog::On_Create (pParam))
    {	return False;}

    char pTitle [64]; char pAngle [32];
    
	wsprintf (pAngle, "%s%d", (0 < s_iDetCalibSet) ? "+" : "", s_iDetCalibSet * 45);

	wsprintf (pTitle, LoadString (IDS_ANGLE_FMT), pAngle);

	wsprintf (pTitle + strlen (pTitle),
	
	LoadString (IDS_FOCUS_FMT), LoadString ((m_bCurFocus) ? IDS_FOCUS_LARGE : IDS_FOCUS_SMALL));

    for (int i = 0; i < m_UARange.m_nFitInfo; i ++)
    {
	wsprintf (pTitle + strlen (pTitle), "%d", m_UARange.m_pFitInfo [i] ->uHV);

	if (	1 + i < m_UARange.m_nFitInfo)
	{
	strcat	 (pTitle, ", ");
	}
    }
	wsprintf (pTitle + strlen (pTitle), LoadString (IDS_TEXT_KV));

    SetCtrlText (IDC_CALMSC_HEADER, pTitle);

    SetCtrlText (IDC_CALMSC_NSTATISTIC_EDT, m_nStatistic );

    SendCtrlMessage (IDC_CALMSC_RIDPOS_BTN, WM_SETFONT, (WPARAM) ::GetBoaldGUIFont (), (LPARAM) 0);

//  CheckRadioButton (IDC_CALMSC_SETFOCUS0_RAD,
//		      IDC_CALMSC_SETFOCUS1_RAD, IDC_CALMSC_SETFOCUS1_RAD);
    On_SelectFocus (1);

    SetCalState (m_uCalState);

    return True ;
};

void CCalMScan::On_Destroy ()
{};

/*
void CCalMScan::SetUAStep  (Bool bStep, int uValue)
{
    if (1 == bStep)
    {
	m_UARange.SetStep (uValue);
    }
    if (2 == bStep)
    {
	m_UARange.SetNum  (uValue);
    }
    if (3 == bStep)
    {
	m_UARange.SetStep (m_UARange.m_nStep);
    }

    SetCtrlText (IDC_CALMSC_UAN_EDT   , m_UARange.GetNum  ());
    SetCtrlText (IDC_CALMSC_UASTEP_EDT, m_UARange.GetStep ());
};

void CCalMScan::SetUAValue (Bool bHi, int nValue)
{
    if (! bHi)
    {
	m_UARange.SetLoValue (nValue);
	SetCtrlText (IDC_CALMSC_UALO_EDT, m_UARange.m_nLoValue);
    }
    else
    {
	m_UARange.SetHiValue (nValue);
	SetCtrlText (IDC_CALMSC_UAHI_EDT, m_UARange.m_nHiValue);
    }
	SetUAStep (3, 0);
};
*/
void CCalMScan::SetIFStep  (Bool bStep, int uValue)
{
    if (1 == bStep)
    {
	m_IFRange.SetStep (uValue);
    }
    if (2 == bStep)
    {
	m_IFRange.SetNum  (uValue);
    }
    if (3 == bStep)
    {
	m_IFRange.SetStep (m_IFRange.m_nStep);
    }

    SetCtrlText (IDC_CALMSC_IFN_EDT   , m_IFRange.GetNum  ());
    SetCtrlText (IDC_CALMSC_IFSTEP_EDT, m_IFRange.GetStep ());
};

void CCalMScan::SetIFValue (Bool bHi, int nValue)
{
    if (! bHi)
    {
	m_IFRange.SetLoValue (nValue);
	SetCtrlText (IDC_CALMSC_IFLO_EDT, m_IFRange.m_nLoValue);
    }
    else
    {
	m_IFRange.SetHiValue (nValue);
	SetCtrlText (IDC_CALMSC_IFHI_EDT, m_IFRange.m_nHiValue);
    }
	SetIFStep (3, 0);
};

Bool CCalMScan::On_Edt (GWinMsg & m)
{
    int		nValue	 ;
    int		nCtrlId  ;
    TCHAR	sBuf [64];

    GetCtrlText (nCtrlId = LOWORD (m.wParam), sBuf, sizeof (sBuf) / sizeof (TCHAR));

    switch (nCtrlId)
    {
    case IDC_CALMSC_NSTATISTIC_EDT :
	sscanf (sBuf, "%d", & m_nStatistic);
	return True;

//  case IDC_CALMSC_UASTEP_EDT :
//	sscanf (sBuf, "%d", & nValue);
//	SetUAStep (1, nValue);
//	return True;
//  case IDC_CALMSC_UAN_EDT :
//	sscanf (sBuf, "%d", & nValue);
//	SetUAStep (2, nValue);
//	return True;
// case IDC_CALMSC_UALO_EDT :
//	sscanf (sBuf, "%d", & nValue);
//	SetUAValue (0, nValue);
//	return True;
// case IDC_CALMSC_UAHI_EDT :
//	sscanf (sBuf, "%d", & nValue);
//	SetUAValue (1, nValue);
//	return True;

    case IDC_CALMSC_IFSTEP_EDT :
	sscanf (sBuf, "%d", & nValue);
	SetIFStep (1, nValue);
	return True;
    case IDC_CALMSC_IFN_EDT :
	sscanf (sBuf, "%d", & nValue);
	SetIFStep (2, nValue);
	return True;
    case IDC_CALMSC_IFLO_EDT :
	sscanf (sBuf, "%d", & nValue);
	SetIFValue (0, nValue);
	return True;
    case IDC_CALMSC_IFHI_EDT :
	sscanf (sBuf, "%d", & nValue);
	SetIFValue (1, nValue);
	return True;
    };
	return False;
};

void CCalMScan::SetCalState (CCalMScan::CalState State)
{
    if ((Work == m_uCalState)
    &&  (Work !=       State))
    {
	SetCtrlText (IDC_CALMSC_START_BTN, LoadString (IDS_CALIB_START));
	SetCtrlText (IDC_CALMSC_SAVE_BTN , LoadString (IDS_CALIB_SAVE));
//	EnableCtrl  (IDC_CALMSC_CLOSE_BTN, True);
    }

    switch (m_uCalState = State)
    {
    case Ready :
	SendCtrlMessage (IDC_CALMSC_UASTEP_BAR, PBM_SETPOS, (WPARAM) (m_iCurUAPoint = 0), (LPARAM) 0);
	SendCtrlMessage (IDC_CALMSC_IFSTEP_BAR, PBM_SETPOS, (WPARAM) (m_iCurIFPoint = 0), (LPARAM) 0);

	SetCtrlText (IDC_CALMSC_START_BTN, LoadString (IDS_CALIB_START));
	SetCtrlText (IDC_CALMSC_SAVE_BTN , LoadString (IDS_CALIB_RESET));
	EnableCtrl  (IDC_CALMSC_SAVE_BTN , False);

    PrintfStatus (0, "");

	break;

    case Work :
	SetCtrlText (IDC_CALMSC_START_BTN, LoadString (IDS_CALIB_SUSPEND));
	EnableCtrl  (IDC_CALMSC_SAVE_BTN , False);
//	EnableCtrl  (IDC_CALMSC_CLOSE_BTN, False);
	On_Start    (/*True */);
	break;

    case Paused :
	SetCtrlText (IDC_CALMSC_START_BTN, LoadString (IDS_CALIB_RESUME));
	SetCtrlText (IDC_CALMSC_SAVE_BTN , LoadString (IDS_CALIB_RESET));
	EnableCtrl  (IDC_CALMSC_SAVE_BTN , True );
	On_Start    (/*False */);
	break;

    case Completed :
	SetCtrlText (IDC_CALMSC_START_BTN, LoadString (IDS_CALIB_START));
	SetCtrlText (IDC_CALMSC_SAVE_BTN , LoadString (IDS_CALIB_SAVE));
	EnableCtrl  (IDC_CALMSC_SAVE_BTN , True );

    PrintfStatus (0, "");

	break;
    };

    if ((Ready == m_uCalState) || (Completed == m_uCalState))
    {
	EnableCtrl (IDC_CALMSC_CLOSE_BTN     , True );

//	EnableCtrl (IDC_CALMSC_SETFOCUS0_RAD , True );
//	EnableCtrl (IDC_CALMSC_SETFOCUS1_RAD , True );
	EnableCtrl (IDC_CALMSC_NSTATISTIC_EDT, True );

//	EnableCtrl (IDC_CALMSC_UALO_EDT      , True );
//	EnableCtrl (IDC_CALMSC_UASTEP_EDT    , True );
//	EnableCtrl (IDC_CALMSC_UAN_EDT       , True );
//	EnableCtrl (IDC_CALMSC_UAHI_EDT      , True );

	EnableCtrl (IDC_CALMSC_IFLO_EDT      , True );
	EnableCtrl (IDC_CALMSC_IFSTEP_EDT    , True );
	EnableCtrl (IDC_CALMSC_IFN_EDT       , True );
	EnableCtrl (IDC_CALMSC_IFHI_EDT      , True );
    }
    else
    {
//	EnableCtrl (IDC_CALMSC_SETFOCUS0_RAD , False);
//	EnableCtrl (IDC_CALMSC_SETFOCUS1_RAD , False);
	EnableCtrl (IDC_CALMSC_NSTATISTIC_EDT, False);

//	EnableCtrl (IDC_CALMSC_UALO_EDT      , False);
//	EnableCtrl (IDC_CALMSC_UASTEP_EDT    , False);
//	EnableCtrl (IDC_CALMSC_UAN_EDT       , False);
//	EnableCtrl (IDC_CALMSC_UAHI_EDT      , False);

	EnableCtrl (IDC_CALMSC_IFLO_EDT      , False);
	EnableCtrl (IDC_CALMSC_IFSTEP_EDT    , False);
	EnableCtrl (IDC_CALMSC_IFN_EDT       , False);
	EnableCtrl (IDC_CALMSC_IFHI_EDT      , False);

	EnableCtrl (IDC_CALMSC_CLOSE_BTN     , False);
    }
};


void CCalMScan::On_Rotate ()
{
    switch (s_iDetCalibSet)
    {
    case -2 :
	RRPSetParam (m_pIRRPMM2, IRRPMM2::ID_RunRotateFix, IRRPMM2::RotLt90);
	break;
    case -1 :
	RRPSetParam (m_pIRRPMM2, IRRPMM2::ID_RunRotateFix, IRRPMM2::RotLt45);
	break;
    case  1 :
	RRPSetParam (m_pIRRPMM2, IRRPMM2::ID_RunRotateFix, IRRPMM2::RotRt45);
	break;
    case  2 :
	RRPSetParam (m_pIRRPMM2, IRRPMM2::ID_RunRotateFix, IRRPMM2::RotRt90);
	break;
    default :
//  case  0 :
	RRPSetParam (m_pIRRPMM2, IRRPMM2::ID_RunRotateFix, IRRPMM2::RotMd  );
    };
};

void CCalMScan::On_Press (int nCtrlId)
{
    if (IDC_CALMSC_START_BTN == nCtrlId)
    {
	switch (m_uCalState)
	{
	case Ready  :
	case Paused :
	    SetCalState (Work  );
	    break;

	case Work  :
	    SetCalState (Paused);
	    break;

	case Completed :
	    SetCalState (Ready );
	    SetCalState (Work  );
	    break;
	};
    }
    else
    if (IDC_CALMSC_SAVE_BTN  == nCtrlId)
    {
	switch (m_uCalState)
	{
	case Paused :
	if (m_pStpIrp != NULL)
	{
	    m_pStpIrp  ->SetResultInfo (ERROR_CANCELLED, NULL);
	    m_pStpIrp  = NULL;

	    CompleteIrp (m_pCalIrp);
	    break;
	}
					// Not entry to this point;
//	    SetCalState (Ready);
	    break;

	case Completed :
printf ("\n\r\r\r...Save calibrate result...\n");
	    SetCalState (Ready);
	    break;
	};
    }
};

Bool CCalMScan::On_DlgMsg (GWinMsg & m)
{
    if (CWndCallBack::RRPMSG == m.uMsg)
    {
	On_IRRPCallBack ((IRRPCallBack::Handle *) m.lParam);

	return m.DispatchComplete ();
    }

    if (m_hEdit.On_ReflectWinMsg (m))
    {
	return On_Edt (m);
    }

    int		nCtrlId;

    if (0 !=   (nCtrlId = IsCommand (m)))
    {
	switch (nCtrlId)
	{
//	case IDC_CALMSC_SETFOCUS0_RAD :
//	case IDC_CALMSC_SETFOCUS1_RAD :
//	    return (On_SelectFocus (IsCtrlChecked (IDC_CALMSC_SETFOCUS1_RAD) ? True : False), True);

	case IDC_CALMSC_RIDPOS_BTN :
	    return (On_Rotate (), True);

	case IDC_CALMSC_START_BTN  :
	case IDC_CALMSC_SAVE_BTN   :
	    return (On_Press (nCtrlId), True);

	case IDC_CALMSC_CLOSE_BTN :
	    ::SetWindowLong (GetHWND (), GWL_USERDATA, TRUE);
	    return (TCurrent () ->GetCurApartment () ->BreakFromWorkLoop (), True);
	};
    }

    if ((WM_CTLCOLORBTN == m.uMsg) && (IDC_CALMSC_RIDPOS_BTN == GetCtrlId ((HWND) m.lParam)))
    {
	::SetTextColor (m.wHDC, RGB (128 + 16,0,0));
	::SetBkColor   (m.wHDC, ::GetSysColor      (COLOR_3DFACE));

    	return m.DispatchCompleteDlgEx ((LRESULT)
				::GetSysColorBrush (COLOR_3DFACE));
    }
    return GDialog::On_DlgMsg (m);
};
//
//[]------------------------------------------------------------------------[]
//
/*
static GResult GAPI AccumulateAsyncProc (int nArgs, void ** pArgs, HANDLE hResume)
{
    TCHAR   pFileName [MAX_PATH];
    TCHAR   pPath     [MAX_PATH];

    HANDLE  hCancel	;

    ::strncpy 
   (pFileName,	      (LPCTSTR) pArgs [0], MAX_PATH);
    ::strncpy
   (pPath    ,	      (LPCTSTR) pArgs [1], MAX_PATH);
    hCancel	    = (HANDLE ) pArgs [2];

    ::SetEvent	      (hResume);

    return Accumulate (pFileName, pPath);
};

static HANDLE AccumulateAsync (TCHAR * pFileName, TCHAR * pPath, HANDLE hCancel)
{
    HANDLE  t     = NULL;

    void *  p [3] = {(void *) pFileName, (void *) pPath	 ,
					 (void *) hCancel};
    CreateThread <GThread>
   (& t, AccumulateAsyncProc, 3, p, (512 * 1024)); 

    return  t;
};
*/
//
//[]------------------------------------------------------------------------[]
//
static GResult GAPI CalibrateAsyncProc (int nArgs, void ** pArgs, HANDLE hResume)
{
    void * pImageBuf  =		  pArgs [0];
    DWord  dImageSize = (DWord  ) pArgs [1];
    Bool   bCurFocus  = (Bool	) pArgs [2];
    int    uCurUA     = (int	) pArgs [3];
    int    uCurIF     = (int	) pArgs [4];
    int    iCurStat   = (int	) pArgs [5];
    Bool   bNextUA    = (Bool	) pArgs [6];
    Bool   bNextIF    = (Bool	) pArgs [7];
    Bool   bDC	      = (Bool	) pArgs [8];
    HANDLE hCancel    = (HANDLE ) pArgs [9];

    if (hResume)
    {
	SetEvent (hResume);
    }

    void *	  p [16];
    
    GResult	  dResult = ERROR_SUCCESS  ;

    TCHAR	  pName	    [MAX_PATH];
    TCHAR	  pFileName [MAX_PATH];
    TCHAR	  pPath	    [MAX_PATH];

//..........
//
    if (bDC)
    {
	wsprintf (pName, _T("\\DC_%d_%d.flr"),	       uCurIF, iCurStat);
    }
    else
    {
	wsprintf (pName, _T("\\%d_%d_%d.flr"), uCurUA, uCurIF, iCurStat);
    }

    GetNVCalibrateIFPath 
		 (pFileName);
    strcat	 (pFileName, pName);

    printf ("...SaveImage %c, %c, %c, %d, {%s}\n", (bNextUA) ? 'T' : 'F',
						   (bNextIF) ? 'T' : 'F',
						   (bDC)     ? 'D' : ' ', dImageSize / RAW_IMAGE_SIZEX, pFileName);
    p [1] = (void *) pImageBuf	;
    p [2] = (void *) dImageSize ;
    p [3] = (void *) NULL	;

    if (INVALID_HANDLE_VALUE ==
   (p [0] = (void *)
    ::CreateFile    (pFileName		,
		     GENERIC_WRITE	,
		     0			,
		     NULL	    	,
		     CREATE_ALWAYS	,
		     FILE_ATTRIBUTE_NORMAL 
		   | FILE_FLAG_SEQUENTIAL_SCAN,
		     NULL		)))
    {	return dResult = ::GetLastError ();}
    
	       dResult = SaveImageAsyncProc (4, p, NULL);

    ::CloseHandle ((HANDLE) p [0]);

    if ((ERROR_SUCCESS != dResult) || (! bNextIF))
    {
	return dResult;
    }
//..........
//
    if (bDC)
    {
	wsprintf (pPath, "\\DC_%d.flr",		uCurIF);
    }
    else
    {
	wsprintf (pPath, "\\%d_%d.flr", uCurUA, uCurIF);
    }

    GetNVCalibrateUAPath (pFileName, s_iDetCalibSet, bCurFocus, uCurUA);
    strcat		 (pFileName, pPath);
    GetNVCalibrateIFPath (pPath);

    printf ("\n...Accumulate {%s}\n", pFileName);

    if (ERROR_SUCCESS != (dResult = Accumulate (pFileName, pPath))  || (! bNextUA))
    {	return dResult;}

//..........
//
    GetNVCalibrateUAPath (pPath	   , s_iDetCalibSet, bCurFocus, uCurUA);

    printf ("\n...Calibrate  {%s}\n", pPath);

    if (ERROR_SUCCESS != (dResult = Calibrate  (uCurUA	    ,
						s_iActiveBeg, s_iActiveEnd,
						s_iDCBeg    , s_iDCEnd	  ,
						s_iKX	    , s_iKY	  ,
						pPath	  )))
    {	return dResult;}

	return dResult;
};

static HANDLE CalibrateAsync (void * pImageBuf ,
			      DWord  dImageSize,
			      Bool   bCurFocus ,
			      int    iCurUA    ,
			      int    iCurIF    ,
			      int    iCurStat  ,
			      Bool   bNextUA   ,
			      Bool   bNextIF   ,
			      Bool   bDC       ,
			      HANDLE hCancel   )
{
    HANDLE t      = NULL;

    void * p [10] = {	      pImageBuf ,
		     (void *) dImageSize,
		     (void *) bCurFocus	,
		     (void *) iCurUA	,
		     (void *) iCurIF	,
		     (void *) iCurStat	,
		     (void *) bNextUA	,
		     (void *) bNextIF	,
		     (void *) bDC	,
		     (void *) hCancel	};

    CreateThread <GThread>
   (& t, CalibrateAsyncProc, 10, p, (512 * 1024)); 

    return  t;
};
//
//[]------------------------------------------------------------------------[]
//
GIrp * CCalMScan::Co_ShotCycle2 (GIrp * pIrp, void *)
{
    if (pIrp ->Cancelled () || (ERROR_SUCCESS == pIrp ->GetCurResult ()))
    {
	return pIrp;
    }
    PrintfStatus (2, LoadString (IDS_CALMSC_ERR_CYCLE2), pIrp ->GetCurResult ());

	QueueIrpForComplete (pIrp, 10000);	// Delay 10 c (Look at Error);
	return NULL;
};

GIrp * CCalMScan::Co_ShotCycle (GIrp * pIrp, void *)
{
    printf ("...Co_ShotCycle...\n");

    m_pShotIrp = NULL;

    return pIrp;
};

GIrp * CCalMScan::Co_CalWaitAsyncCall (GIrp * pIrp, void * hSave)
{
    if (WAIT_TIMEOUT == ::WaitForSingleObject ((HANDLE) hSave, 0))
    {
	SetCurCoProc <CCalMScan, void *>
       (pIrp, & CCalMScan::Co_CalWaitAsyncCall, this, hSave);

	QueueIrpForComplete (pIrp, 20);
	return NULL;
    }
	DWORD	  dResult = -1;

	::GetExitCodeThread
	(hSave, & dResult);

    PrintfStatus (2, "");

        printf ("...End Async call with with %08X\n", dResult);

	::CloseHandle (hSave);

	pIrp ->SetCurResultInfo (dResult, NULL);
	return pIrp;
};

GIrp * CCalMScan::Co_CalStep (GIrp * pIrp, void * pArgument)
{
    if (pIrp ->Cancelled ())
    {
	pIrp ->pCancelProcLock.Exchange (NULL);

	return pIrp;
    }
//  if (FALSE)
    if (ERROR_SUCCESS != pIrp ->GetCurResult ())
    {
	printf ("\t\t\t...Co CalStep Fail %08X...\n", pIrp ->GetCurResult ());
    }
    else
    {{
	if (m_iCurDC)
	{
	printf ("\t\t\t...Co CalStep (%2d.%2d.%1d) - %3d:%3d\n", m_iCurUAPoint ,
								 m_iCurIFPoint ,
								 m_iCurStat    ,
					     m_UARange.GetValue (m_iCurUAPoint),
					     m_IFRange.GetValue (m_iCurIFPoint));
	}
	else
	{
	printf ("\t\t\t...Co CalStep (%2d.%2d.%1d) - DC :%3d\n", m_iCurUAPoint ,
								 m_iCurIFPoint ,
								 m_iCurStat    ,
					     m_IFRange.GetValue (m_iCurIFPoint));
	}

//  Process Step result in this place !!!
//
//
//  .....................................

    if (++ m_iCurStat	 >= m_nStatistic       )
    {
	   m_iCurStat	 = 0;

	   PrepareNextIF  ();

    if (   m_iCurDC			       )
    {
    if (++ m_iCurIFPoint >= m_IFRange.GetNum ())
    {
    if (++ m_iCurUAPoint  < m_UARange.GetNum ())
    {
	   m_iCurDC	 = 0;
	   m_iCurIFPoint = 0;

	   PrepareNextUA  (m_bCurFocus, m_UARange.GetValue (m_iCurUAPoint));
    }
    SendCtrlMessage (IDC_CALMSC_UASTEP_BAR, PBM_SETPOS, (WPARAM) m_iCurUAPoint, (LPARAM) 0);
    }
    }
    else
    {
	++ m_iCurDC;
    }

    SendCtrlMessage (IDC_CALMSC_IFSTEP_BAR, PBM_SETPOS, (WPARAM) m_iCurIFPoint + m_iCurDC, (LPARAM) 0);
    }
    }}
//
//
//
//  Process Step result in this place !!!

	QueueIrpForComplete (pIrp, 0/*5000*/);

	return NULL;
};

GIrp * CCalMScan::Co_CalWaitImageEof (GIrp * pIrp, void * pArgument)
{
    GTimeout * pTimeout = (GTimeout *)(& pArgument);

    if (pIrp ->Cancelled () || (ERROR_SUCCESS != pIrp ->GetCurResult ()))
    {	return pIrp;}

    if (pTimeout ->Occured ())
    {
printf ("...Wait Eof Timeout...\n");

	pIrp ->SetCurResultInfo (ERROR_TIMEOUT, NULL);
	return pIrp;
    }

    if (m_uReg0057 & IRRPMScan::FGStatus_DataReady)
    {{
	void * pImageBuf  = NULL;
	DWord  dImageSize = m_pIRRPMScan ->GetDataPtr (pImageBuf, 0, 63 * (1024 * 1024));

	if ((((DWord) s_iDCEnd	   * RAW_IMAGE_SIZEX) > dImageSize) 
	||  (((DWord) s_iActiveEnd * RAW_IMAGE_SIZEX) > dImageSize))
	{
printf ("...Problem fail...\n");

	    pIrp ->SetCurResultInfo (-1, NULL);
	    return pIrp;
	}

printf ("...Problem solute...\n");

	HANDLE	     hSave     = CalibrateAsync (pImageBuf, dImageSize,		m_bCurFocus	,
							    m_UARange.GetValue (m_iCurUAPoint)	,
							    m_IFRange.GetValue (m_iCurIFPoint)	,
										m_iCurStat	,
				 ((m_iCurIFPoint + 1) >= m_IFRange.GetNum ()) ? True : False	,
				 ((m_iCurStat	 + 1) >= m_nStatistic	    ) ? True : False	,
				 ( m_iCurDC	      == 0		    ) ? True : False	,
										NULL		);
	if (hSave)
	{

    PrintfStatus (2, LoadString (IDS_CALMSC_MSG_WIMAGE), ((m_iCurIFPoint + 1) >= m_IFRange.GetNum ()) ? "Accumulate + Calibrate" : "Accumulate");

	    SetCurCoProc <CCalMScan, void *>
	   (pIrp, & CCalMScan::Co_CalWaitAsyncCall, this, hSave);

	    QueueIrpForComplete (pIrp, 20);
	    return NULL;
	}
	    pIrp ->SetCurResultInfo (-1, NULL);
	    return pIrp;
    }}

    if (m_uReg0057 & IRRPMScan::FGStatus_DataEof)
    {{
	void * pImageBuf  = NULL;
	DWord  dImageSize = m_pIRRPMScan ->GetDataPtr (pImageBuf, 0, 64 * (1024 * 1024));

	m_uReg0057	 &= (~IRRPMScan::FGStatus_DataEof);

	if ((((DWord) s_iDCEnd	   * RAW_IMAGE_SIZEX) > dImageSize) 
	||  (((DWord) s_iActiveEnd * RAW_IMAGE_SIZEX) > dImageSize))
	{
	    m_pIRRPMDiafr ->Run (IRRPMMotor::RunBackward, 15000);
	    m_pIRRPMScan  ->Run (IRRPMMotor::RunBackward, 15000);

printf ("...Problem generation...\n");
	}
    }}

	SetCurCoProc <CCalMScan, void *>
       (pIrp, & CCalMScan::Co_CalWaitImageEof, this, pArgument);

	QueueIrpForComplete (pIrp, 20);
	return NULL;
};

GIrp * CCalMScan::Co_CalWaitImageSof (GIrp * pIrp, void * pArgument)
{
    if (pIrp ->Cancelled () || (ERROR_SUCCESS != pIrp ->GetCurResult ()))
    {	return pIrp;}

    GTimeout * pTimeout = (GTimeout *)(& pArgument);

    if (m_uReg0057 & (IRRPMScan::FGStatus_DataSof 
		   |  IRRPMScan::FGStatus_DataEof 
		   |  IRRPMScan::FGStatus_DataReady))
    {
	pTimeout ->Activate (8000);

	SetCurCoProc <CCalMScan, void *>
       (pIrp, & CCalMScan::Co_CalWaitImageEof, this, pArgument);

	QueueIrpForComplete (pIrp);
	return NULL;
    }
    if (pTimeout ->Occured ())
    {
printf ("...Wait Sof Timeout...\n");

	pIrp ->SetCurResultInfo (ERROR_TIMEOUT, NULL);
	return pIrp;
    }
	SetCurCoProc <CCalMScan, void *>
       (pIrp, & CCalMScan::Co_CalWaitImageSof, this, pArgument);
    
	QueueIrpForComplete (pIrp, 20);
	return NULL;
};

GIrp * CCalMScan::Co_CalWaitReady (GIrp * pIrp, void *)
{
    if (pIrp ->Cancelled () || (ERROR_SUCCESS != pIrp ->GetCurResult ()))
    {
	return pIrp;
    }

#if (CALMSC_RELEASE )

#if (MSCAN_ONLY_MODE)

    if ((0 == (m_uMSStatus & 0x0200))
    &&	(0 == (m_uMSStatus & 0x0100)))
    {
	       m_pIRRPMScan  ->Run (IRRPMMotor::RunBackward, 15000);
    }

    if (      (m_uMSStatus & 0x0200)
//  &&	      (m_bMM2Disable ==   0)
#else

    if ((0 == (m_uMDStatus & 0x0200))
    &&	(0 == (m_uMDStatus & 0x0100)))
    {
	       m_pIRRPMDiafr ->Run (IRRPMMotor::RunBackward, 15000);
    }
    if ((0 == (m_uMSStatus & 0x0200))
    &&	(0 == (m_uMSStatus & 0x0100)))
    {
	       m_pIRRPMScan  ->Run (IRRPMMotor::RunBackward, 15000);
    }

    if (      (m_uMDStatus & 0x0200)
    &&	      (m_uMSStatus & 0x0200)
    &&	      (m_bMM2Disable ==   0)

#endif

    &&  (0 ==  m_uHVReady				)
    &&	    ( (m_uReg0057 & IRRPMScan::FGStatus_Online ))
    &&	    (!(m_uReg0057 & IRRPMScan::FGStatus_DataSof)))

    {{
//<---- Clear Image Buf !!!
//
	RRPSetParam (m_pIRRPMScan, IRRPMScan::ID_RegGetFGStatus, 0x00);
//
//<----
	       m_uReg0058 = 0;

	GTimeout tTimeout;

	tTimeout.Activate (10000);

	SetCurCoProc <CCalMScan, void *>
       (pIrp, & CCalMScan::Co_CalWaitImageSof, this, (void *)(* (DWord *) & tTimeout));

//	SetCurCoProc <CCalMScan, void *>
//     (pIrp, & CCalMScan::Co_CalWaitImageEof, this, (void *)(* (DWord *) & tTimeout));

	SetCurCoProc
       (m_pShotIrp, & Co_RestoreCurApartment, NULL, TCurrent () ->GetCurApartment ());

printf ("Run HV...!!!\n");

	m_pIRRPUrp ->RunHV  (pIrp);

    PrintfStatus (2, LoadString (IDS_CALMSC_MSG_HVGRBI));

	return NULL;
    }}
#else
	QueueIrpForComplete (pIrp, 200);
	return NULL;
#endif

printf ("Wait Ready ...!!!\n");

    PrintfStatus (2, LoadString (IDS_CALMSC_MSG_WAITRDY), m_uHVReady);

    SetCurCoProc <CCalMScan, void *>
   (pIrp, & CCalMScan::Co_CalWaitReady, this, NULL);

    QueueIrpForComplete (pIrp, 500);
    return NULL;
};

GIrp * CCalMScan::Co_CalLoop (GIrp * pIrp, void *)
{
    if (   m_iCurUAPoint >= m_UARange.GetNum ())
    {
	return pIrp;
    }
    if (   m_uCalState   != Work)
    {
	return (m_pStpIrp = pIrp, NULL);
    }

    pIrp ->SetCurResultInfo (ERROR_SUCCESS, NULL);

    SetCurCoProc <CCalMScan, void *>
   (pIrp, & CCalMScan::Co_CalLoop, this, NULL);

    SetCurCoProc <CCalMScan, void *>
   (pIrp, & CCalMScan::Co_CalStep, this, NULL);

//  Build Shot request in this place !!!
//
    m_pShotIrp = pIrp;

    SetCurCoProc <CCalMScan, void *>
   (m_pShotIrp, & CCalMScan::Co_ShotCycle , this, NULL);

    SetCurCoProc <CCalMScan, void *>
   (m_pShotIrp, & CCalMScan::Co_ShotCycle2, this, NULL);

    if (m_iCurDC)
    {
    printf ("\t\t\t...Do CalStep (%2d.%2d.%1d) - %3d:%3d\n", m_iCurUAPoint ,
							     m_iCurIFPoint ,
							     m_iCurStat    ,
					 m_UARange.GetValue (m_iCurUAPoint),
					 m_IFRange.GetValue (m_iCurIFPoint));

    PrintfStatus (0, LoadString (IDS_CALMSC_MSG_POINT),      m_iCurUAPoint ,
							     m_iCurIFPoint ,
							     m_iCurStat    ,
					 m_UARange.GetValue (m_iCurUAPoint),
					 m_IFRange.GetValue (m_iCurIFPoint)); 
    }
    else
    {
    printf ("\t\t\t...Do CalStep (%2d.%2d.%1d) - DC :%3d\n", m_iCurUAPoint ,
							     m_iCurIFPoint ,
							     m_iCurStat    ,
					 m_IFRange.GetValue (m_iCurIFPoint));

    PrintfStatus (0, LoadString (IDS_CALMSC_MSG_POINTDC),    m_iCurUAPoint ,
							     m_iCurIFPoint ,
							     m_iCurStat    ,
					 m_IFRange.GetValue (m_iCurIFPoint)); 
    }

    RRPSetParam (m_pIRRPUrp, IRRPUrp::ID_ParSetFieldSize, 0);
    RRPSetParam (m_pIRRPUrp, IRRPUrp::ID_Focus	      ,			    m_bCurFocus   );
    RRPSetParam (m_pIRRPUrp, IRRPUrp::ID_IFilamentPre ,  -1);

    if (m_iCurDC)
    {
    RRPSetParam (m_pIRRPUrp, IRRPUrp::ID_RegSetUAnode , m_UARange.GetValue (m_iCurUAPoint));
    RRPSetParam (m_pIRRPUrp, IRRPUrp::ID_IFilament    , m_IFRange.GetValue (m_iCurIFPoint));
    }
    else
    {
    RRPSetParam	(m_pIRRPUrp, IRRPUrp::ID_RegSetUAnode ,					 0);
    RRPSetParam (m_pIRRPUrp, IRRPUrp::ID_IFilament    , m_IFRange.GetValue (m_iCurIFPoint));
    }

    RRPSetParam (m_pIRRPUrp, IRRPUrp::ID_MAS	      , 1500);
    RRPSetParam (m_pIRRPUrp, IRRPUrp::ID_ExposeTime   , 8000);

    SetCurCoProc <CCalMScan, void *>
   (pIrp, & CCalMScan::Co_CalWaitReady, this, NULL);

    QueueIrpForComplete (pIrp);
    return NULL;
};

GIrp * CCalMScan::Co_CalProc (GIrp * pIrp, void *)
{
    RRPSetParam (m_pIRRPUrp, IRRPUrp::ID_ParSetMode, 0x00020000);

    printf ("[%08X]...CCalMScan::Co_CalProc %08X...\n", TCurrent () ->GetId (), pIrp ->GetCurResult ());

    m_pCalIrp = NULL;

    if (ERROR_SUCCESS == pIrp ->GetCurResult ())
    {
	SetCalState (Completed);
    }
    else
    {
	SetCalState (Ready    );
    }

    return pIrp;
};

void CCalMScan::Do_ShotCycleCancel ()
{
    if (m_pShotIrp)
    {
			      CancelIrp (m_pShotIrp);

	while (NULL != (volatile void *) m_pShotIrp)
	{
 	    if (TCurrent () ->GetCurApartment () ->PumpInputWork ())
	    {
		TCurrent () ->GetCurApartment () ->Work ();

		continue;
	    }
		TCurrent () ->GetCurApartment () ->WaitInputWork (20);
	}
    }
};

Bool CCalMScan::On_Start ()
{
    if (m_pCalIrp == NULL)		// Start process
    {					//
	m_pCalIrp     = new GIrp;

//	m_bCurFocus   = IsCtrlChecked (IDC_CALMSC_SETFOCUS1_RAD) ? True : False;
	m_iCurUAPoint = 0;
	m_iCurIFPoint =
	m_iCurDC      =
	m_iCurStat    = 0;

	PrepareFirst   (m_bCurFocus);
	PrepareNextIF  ();
	PrepareNextUA  (m_bCurFocus, m_UARange.GetValue (m_iCurUAPoint));

	SendCtrlMessage (IDC_CALMSC_UASTEP_BAR, PBM_SETRANGE, 0, MAKELPARAM (0, m_UARange.GetNum ()    ));
	SendCtrlMessage (IDC_CALMSC_IFSTEP_BAR, PBM_SETRANGE, 0, MAKELPARAM (0, m_IFRange.GetNum () + 1));

	SetCurCoProc <CCalMScan, void *>
       (m_pCalIrp, & CCalMScan::Co_CalProc, this, NULL);

	RRPSetParam (m_pIRRPUrp, IRRPUrp::ID_ParSetMode, 0x00000002);

	SetCurCoProc <CCalMScan, void *>
       (m_pCalIrp, & CCalMScan::Co_CalLoop, this, NULL);

	CompleteIrp (m_pCalIrp);

	return True;
    }
    else
    if (m_pStpIrp != NULL)		// Resume process
    {
	SetCurCoProc <CCalMScan, void *>
       (m_pCalIrp, & CCalMScan::Co_CalLoop, this, NULL);

	m_pStpIrp  = NULL;

	CompleteIrp (m_pCalIrp);

	return True;
    }
    else				// Pause  process
    {					//
    PrintfStatus (0, LoadString (IDS_CALMSC_MSG_PAUSE));

	    Do_ShotCycleCancel ();

	while ((NULL != (volatile void *) m_pCalIrp)
	&&     (NULL == (volatile void *) m_pStpIrp))
	{
 	    if (TCurrent () ->GetCurApartment () ->PumpInputWork ())
	    {
		TCurrent () ->GetCurApartment () ->Work ();

		continue;
	    }
		TCurrent () ->GetCurApartment () ->WaitInputWork (20);
	}
    }
	return (m_pCalIrp) ? True : False;
};
//
//[]------------------------------------------------------------------------[]
//
void CCalMScan::On_IRRPParam (_U32 uParamId, _U32 uValue)
{
				    // Urp notifications !!!
    				    //
    if (0x80000000 == uParamId)
    {
	s_pMainPanel ->Send_WM (CWndCallBack::EQUMSG, IRRPMDIG::EquMask_Urp, (LPARAM)(uValue & 0x03));
    }
    if (IRRPUrp::ID_RegGetHVReady == uParamId)
    {
	m_uHVReady  = uValue;
    }
				    // Diafr notifications !!!
    				    //
    if (0xD7000000 == uParamId)
    {
	s_pMainPanel ->Send_WM (CWndCallBack::EQUMSG, IRRPMDIG::EquMask_MDiafr, (LPARAM) (uValue & 0x03));
    }
    if (0xD700D7D6 == uParamId)
    {
	m_uMDStatus = uValue;
    }
				    // Scanner notifications !!!
				    //
    if (0xD6000000 == uParamId)
    {
	s_pMainPanel ->Send_WM (CWndCallBack::EQUMSG, IRRPMDIG::EquMask_MScan, (LPARAM) (uValue & 0x03));
    }

    if (0xD600D7D6 == uParamId)
    {
	m_uMSStatus = uValue;
    }
    if (0xD6000057 == uParamId)
    {
	m_uReg0057 = uValue;

	s_pMainPanel ->Send_WM	  (CWndCallBack::EQUMSG, (WPARAM) IRRPMDIG::EquMask_MFG,
		  (LPARAM) 0x01 | ((uValue & IRRPMScan::FGStatus_Online) ? 0x02 : 0x00));
    }
    if (0xD6000058 == uParamId)
    {
	m_uReg0058 = uValue;
    }
				    // MM2 boad notification !!!
				    //
    if (0x91000000 == uParamId)
    {
	m_bMM2Disable |=   0x01;
	m_bMM2Disable &= ((0x03 == (uValue & 0x0003)) ? (~0x01) : (~0x00));
	s_pMainPanel ->Send_WM (CWndCallBack::EQUMSG, (WPARAM) IRRPMDIG::EquMask_MM2, uValue & 0x03);

//printf ("\t\t\t\t%02X <- (v (%02X))\n", m_bMM2Disable, uValue);

	ShowCtrl (IDC_CALMSC_RIDPOS_BTN, (m_bMM2Disable) ? True : False);
    }
    if (IRRPMM2::ID_RegGetMoveStatus  == uParamId)
    {{
	m_bMM2Disable |=   0x04;

	switch (s_iDetCalibSet)
	{
	case -2 :
	    m_bMM2Disable &= ((uValue & IRRPMM2::RotLtPin  ) ? (~0x04) : (~0x00)); break;
	case -1 :
	    m_bMM2Disable &= ((uValue & IRRPMM2::RotLt45Pin) ? (~0x04) : (~0x00)); break;
	case  0 :
	    m_bMM2Disable &= ((uValue & IRRPMM2::RotMdPin  ) ? (~0x04) : (~0x00)); break;
	case  1 :
	    m_bMM2Disable &= ((uValue & IRRPMM2::RotRt45Pin) ? (~0x04) : (~0x00)); break;
	case  2 :
	    m_bMM2Disable &= ((uValue & IRRPMM2::RotRtPin  ) ? (~0x04) : (~0x00)); break;
	};

//printf ("\t\t\t\t%02X <- (s (%02X))\n", m_bMM2Disable, uValue);

	ShowCtrl (IDC_CALMSC_RIDPOS_BTN, (m_bMM2Disable) ? True : False);
    }}
};

void CCalMScan::On_IRRPCallBack (IRRPCallBack::Handle * hIRRPCallBack)
{
    Word	    uLength	;
    Word	    uType	;
    IRRPProtocol::DataPack *
		    pDataPack	;
    IRRPProtocol::ParamPack::Entry * 
		    pEntry	;

    if ((hIRRPCallBack == m_hIRRPUCallBack)
    ||  (hIRRPCallBack == m_hIRRPDCallBack)
    ||  (hIRRPCallBack == m_hIRRPSCallBack)
    ||  (hIRRPCallBack == m_hIRRPMCallBack))
    {
	pDataPack = (IRRPProtocol::DataPack *) m_sIRRPxCallBack.PeekHeadPacket ();

    while (pDataPack)
    {
	uLength = getBigEndianW (pDataPack, offsetof (IRRPProtocol::DataPack, uLength));
	uType	= getBigEndianW (pDataPack, offsetof (IRRPProtocol::DataPack, uType  ));

	if (0  == uType)
	{
		pEntry = (IRRPProtocol::ParamPack::Entry *)(pDataPack + 1);

	    while ((Byte *) pEntry < ((Byte *) pDataPack + uLength))
	    {
		On_IRRPParam (getBigEndianD (pEntry, offsetof (IRRPProtocol::ParamPack::Entry, uParamId)),
			      getBigEndianD (pEntry, offsetof (IRRPProtocol::ParamPack::Entry, uValue  )));
		pEntry ++;
	    }
	}

	pDataPack = (IRRPProtocol::DataPack *) m_sIRRPxCallBack.PeekNextPacket ();
    }
    }
};
//
//[]------------------------------------------------------------------------[]
//
Bool On_MSC_Calibrate (int x, int y, GWindow * pParent, int iDetCalibSet, Bool bFocus, int nFitInfo, FitInfo ** pFitInfo)
{
    CCalMScan	Dlg (iDetCalibSet, bFocus, nFitInfo, pFitInfo);

    if (! Dlg.Create (pParent, NULL, MAKEINTRESOURCE (IDD_MSC_CALIBRATE)))
    {	return False;}


	Dlg.SetWindowPos (NULL, x, y, 0, 0, SWP_NOSIZE);

	AddPreprocHandler (& Dlg);

	if (ERROR_SUCCESS == s_pIRRPService ->CreateRRPDevice 
				    ((void **) & Dlg.m_pIRRPUrp	  ,
				      IID_IUnknown		  ,
				     (void  *) 0x80000000	  ,
				      IID_IUnknown		  ))
	{
			     s_pIRRPService ->CreateRRPDevice 
				    ((void **) & Dlg.m_pIRRPMDiafr,
				      IID_IUnknown		  ,
				     (void * ) 0xD7000000	  ,
				      IID_IUnknown		  );

			     s_pIRRPService ->CreateRRPDevice 
				    ((void **) & Dlg.m_pIRRPMScan ,
				      IID_IUnknown		  ,
				     (void * ) 0xD6000000	  ,
				      IID_IUnknown		  );

			     s_pIRRPService ->CreateRRPDevice
				    ((void **) & Dlg.m_pIRRPMM2	  ,
				      IID_IUnknown		  ,
				     (void * ) 0x91000000	  ,
				      IID_IUnknown		  );

	    Dlg.m_sIRRPxCallBack.SetRecvHWND (Dlg.GetHWND ());

	    Dlg.m_pIRRPUrp    ->Connect  (Dlg.m_hIRRPUCallBack,
				        & Dlg.m_sIRRPxCallBack);

	    Dlg.m_pIRRPMDiafr ->Connect  (Dlg.m_hIRRPDCallBack,
				        & Dlg.m_sIRRPxCallBack);

	    Dlg.m_pIRRPMScan  ->Connect  (Dlg.m_hIRRPSCallBack, 
				        & Dlg.m_sIRRPxCallBack);

	    Dlg.m_pIRRPMM2    ->Connect  (Dlg.m_hIRRPMCallBack,
					& Dlg.m_sIRRPxCallBack);

	    Dlg.ExecWindow ();

	    if (Dlg.m_hIRRPSCallBack)
	    {
		Dlg.m_hIRRPSCallBack ->Close ();
		Dlg.m_hIRRPSCallBack = NULL;
	    }
	    if (Dlg.m_hIRRPDCallBack)
	    {
		Dlg.m_hIRRPDCallBack ->Close ();
		Dlg.m_hIRRPDCallBack = NULL;
	    }
	    if (Dlg.m_hIRRPUCallBack)
	    {
		Dlg.m_hIRRPUCallBack ->Close ();
		Dlg.m_hIRRPUCallBack = NULL;
	    }
	    if (Dlg.m_hIRRPMCallBack)
	    {
		Dlg.m_hIRRPMCallBack ->Close ();
		Dlg.m_hIRRPMCallBack = NULL;
	    }

	    Dlg.m_pIRRPMM2	->Release ();
	    Dlg.m_pIRRPMM2	= NULL;

	    Dlg.m_pIRRPMScan	->Release ();
	    Dlg.m_pIRRPMScan	= NULL;

	    Dlg.m_pIRRPMDiafr	->Release ();
	    Dlg.m_pIRRPMDiafr	= NULL;

	    Dlg.m_pIRRPUrp	->Release ();
	    Dlg.m_pIRRPUrp	= NULL;
	}

	RemPreprocHandler (& Dlg);

	Dlg.Destroy	  ();

	return True;
};
//
//[]------------------------------------------------------------------------[]
