//[]------------------------------------------------------------------------[]
// Main GWin::GUI definition
//
//
// Created by GSh.
//[]------------------------------------------------------------------------[]

/* Remember :

    WS_EX_CONTROLPARENT		Must be set for keyboard processing.
    
    WS_EX_NOINHERITLAYOUT	???
    WS_EX_COMPOSITED		???

    DS_SYSMODAL			???
*/

#if FALSE
#define	 ISOLATION_AWARE_ENABLED    1
#define	 OEMRESOURCE
#endif

#include <stdio.h>
#include <shlwapi.h>
#include <commctrl.h>
#include <uxtheme.h>

    static HWND	g_hWnd = NULL;


//[]------------------------------------------------------------------------[]
//
void CalcLayoutRect (DWord dLayout, const GRect  & rt,
				          LONG	   cx,
				          LONG	   cy,
				          GRect  & wn)
{
    switch (GetHorLayout (dLayout))
    {
	case (GGF_LOLO_CONST_SIZE) :

	    wn. x = rt. x;
	    wn.cx = rt.cx;
	    break;

	case (GGF_HIHI_CONST_SIZE) :

	    wn. x = cx - rt.x - rt.cx;
	    wn.cx = rt.cx;
	    break;

	case (GGF_LOHI) :

	    wn. x = rt. x;
	    wn.cx = cx - rt.x - rt. x;
	    break;
    };
    switch (GetVerLayout (dLayout))
    {
	case (GGF_LOLO_CONST_SIZE) :

	    wn. y = rt. y;
	    wn.cy = rt.cy;
	    break;

	case (GGF_HIHI_CONST_SIZE) :

	    wn. y = cy - rt.y - rt.cy;
	    wn.cy = rt.cy;
	    break;

	case (GGF_LOHI) :

	    wn. y = rt. y;
	    wn.cy = cy - rt.y - rt.y;
	    break;
    };
};


//[]------------------------------------------------------------------------[]
//
#pragma pack (_M_PACK_VPTR)

#define DPF_VISIBLE	    0x00000001
#define DPF_UPDATED	    0x00000002
#define	DPF_VER		    0x00000080
#define	DPF_CTRL_BAR	    0x00000040	    // This fixed size control bar

//#define	DPF_DOCK_BAR	    0x00000004
//#define	DPF_DRAG_BAR	    0x00000008

#define	DPS_MAX		    16


//	struct GViewPane
	struct GDockPane
	{
	typedef TDLink <GDockPane, 0>	SLink	  ;
	typedef TXList <GDockPane, 
		TDLink <GDockPane, 0> >	SList	  ;
//	typedef enum
//	{	Horizontal  = 0,
//		Vertical	  }	SplitType ;

	    SLink			m_SLink	  ;
	    SList			m_SList	  ;

	    GDockPane *			m_pParent ;
	    DWord			m_dFlags  ;

	    void *			m_pOwner  ;
	    GRect			m_PRect	  ;

	    LONG	      m_lxWOff, m_hxWOff  ;
	    LONG	      m_lyWOff, m_hyWOff  ;
	    LONG	      m_cxWMin, m_cyWMin  ;
	    LONG	      m_cxWUsr, m_cyWUsr  ;

		    GDockPane ()
		    {
			m_pParent   = NULL;
			m_dFlags    =	 0;

			m_pOwner    = NULL;
			m_PRect.Assign (0, 0, 0, 0);

			m_lxWOff    =
			m_hxWOff    =
			m_lyWOff    =
			m_hyWOff    =
			m_cxWMin    =
			m_cyWMin    =
			m_cxWUsr    =
			m_cyWUsr    =	0;
		    };

	    GDockPane *	    GetParent	() const { return m_pParent;};

	    Bool	    IsRoot	() const { return NULL == GetParent ();};

	    Bool	    ChildOfRoot () const { return GetParent () ->IsRoot ();};

	    Bool	    ChildOfVer	() const { return GetParent () ->m_dFlags & DPF_VER;};

	    Bool	    ChildOfHor	() const { return ! ChildOfVer ();};

	    Bool	    IsVisible	() const { return m_dFlags & DPF_VISIBLE;};

	    Bool	    IsGrowable	() const { return ! (m_dFlags & DPF_CTRL_BAR);}

	    int		    GetHorGrowPriority	() const { return (HIBYTE(LOWORD(m_dFlags)) & 0xF0) >> 4;};

	    int		    GetVerGrowPriority	() const { return (HIBYTE(LOWORD(m_dFlags)) & 0x0F) >> 0;};

	    LONG	    HasDragBar		() const { return  LOBYTE(HIWORD(m_dFlags));};

	    int		    GetZorder		() const { return  HIBYTE(HIWORD(m_dFlags));};

	    void	    GetMinSize	(LONG * cxPMin, LONG * cyPMin) const;

	    void	    PRect	(GRect & pr) const 
			    {
				pr.Assign (0, 0, m_PRect.cx, m_PRect.cy);
			    };

	    const GPoint &  PRectPos	() const { return m_PRect.Pos  ();};

	    const GPoint &  PRectSize	() const { return m_PRect.Size ();};

	    void	    DragBarRect	(GRect & pr) const
			    {
				// GASSERT (this has DragBar !!!)
				//
				    PRect (pr);

				if (ChildOfVer ())
				{   
				    pr.x = pr.cx - HasDragBar ();
					   pr.cx = HasDragBar ();
				}
				else
				{
				    pr.y = pr.cy - HasDragBar ();
					   pr.cy = HasDragBar ();
				}
			    }

	    LONG	    WRectPosX	() const { return m_lxWOff;};

	    LONG	    WRectPosY	() const { return m_lyWOff;};

	    LONG	    WRectSizeX	() const { return m_PRect.cx - m_lxWOff - m_hxWOff;};

	    LONG	    WRectSizeY	() const { return m_PRect.cy - m_lyWOff - m_hyWOff;};

	    void	    WRect	(GRect & wr) const 
			    {
				wr.Assign (WRectPosX (), WRectPosY (), WRectSizeX (), WRectSizeY ());
			    };

	    void *	    GetOwner	() const
			    {
				for (GDockPane * i = (GDockPane *) GetParent (); i ; i = i ->GetParent ())
				{
				    if (i ->m_pOwner)
				    {
					return i ->m_pOwner;
				    }
				}
					return	   m_pOwner;
			    };

	    void	    PToOwner	(GPoint & p) const
			    {
				for (GDockPane * i = (GDockPane *) this; i ; i = i ->GetParent ())
				{
					p.Move (i ->m_PRect.x, i ->m_PRect.y);

				    if (i ->m_pOwner)
				    {
					return;
				    }
				}
			    };

	    void	    OwnerToP	(GPoint & p) const
			    {
				for (GDockPane * i = (GDockPane *) this; i ; i = i ->GetParent ())
				{
					p.Move (- i ->m_PRect.x, - i ->m_PRect.y);

				    if (i ->m_pOwner)
				    {
					return;
				    };
				}
			    };

	    void	    GetGrowableRect (GRect &) const;


	    void	    initWRect		(LONG  lxWOff, LONG hxWOff,
						 LONG  lyWOff, LONG hyWOff,
						 LONG  cxWMin, LONG cyWMin,
						 LONG  cxWUsr, LONG cyWUsr)
			    {
				m_lxWOff =  lxWOff;
				m_hxWOff =  hxWOff;
				m_lyWOff =  lyWOff;
				m_hyWOff =  hyWOff;

				m_cxWMin =  cxWMin;
				m_cyWMin =  cyWMin;
				m_cxWUsr =  cxWUsr;
				m_cyWUsr =  cyWUsr;
			    };

	    void	    InitRoot		(void *	      pOwner     ,
						 LONG cxP   , LONG cyP   ,
						 LONG lxWOff, LONG hxWOff,
						 LONG lyWOff, LONG hyWOff,
						 LONG cxWMin, LONG cyWMin)
			    {
				m_pOwner = pOwner;

				if ((lxWOff + cxWMin + hxWOff) > cxP) {cxP = cxWMin;}
				if ((lyWOff + cyWMin + hyWOff) > cyP) {cyP = cyWMin;}

				initWRect (lxWOff, hxWOff, lyWOff, hyWOff,
							   cxWMin, cyWMin,
						    cxP - lxWOff - hxWOff,
						    cyP - lyWOff - hyWOff);

				m_PRect.Assign (0, 0, cxP, cyP);

				m_dFlags = DPF_VISIBLE;
			    };

	    void	    InsertCtrlBar	(GDockPane *  pPane	 ,
						 GDockPane *  pBefore	 ,
						 void *	      pOwner	 ,
						 LONG lxWOff, LONG hxWOff,
						 LONG lyWOff, LONG hyWOff,
						 LONG czW   , LONG czWMin)
			    {
				pPane ->m_pOwner  = pOwner;
			       (pPane ->m_pParent = this) ->m_SList.insert
							   (pPane, pBefore);

				pPane ->m_dFlags  = DPF_CTRL_BAR;

				if (pPane ->ChildOfVer ())
				{
				    pPane ->initWRect (lxWOff, hxWOff, lyWOff, hyWOff,
								       czW   , czWMin,
								       czW   , czWMin);

				    pPane ->m_PRect.Assign (0, 0, 0, 0);
				}
				else
				{
				    pPane ->initWRect (lxWOff, hxWOff, lyWOff, hyWOff,
								       czWMin, czW   ,
								       czWMin, czW   );

				    pPane ->m_PRect.Assign (0, 0, 0, 0);
				}

				if (True/*pOwner ->HasStyle (WS_VISIBLE)*/)
				{
				    pPane ->Show (True);
				}
				else
				{
				    RecalcLayout ();
				}
			    };

	    void	    Insert		(GDockPane *  pPane	 ,
						 GDockPane *  pBefore	 ,
						 void *	      pOwner	 ,
						 LONG cxW   , LONG cyW	 ,
						 LONG lxWOff, LONG hxWOff,
						 LONG lyWOff, LONG hyWOff,
						 LONG cxWMin, LONG cyWMin)
			    {
				pPane ->m_pOwner  = pOwner;
			       (pPane ->m_pParent = this) ->m_SList.insert 
							   (pPane, pBefore);

				if (cxW < cxWMin) { cxW = cxWMin;}
				if (cyW < cyWMin) { cyW = cyWMin;}

				pPane ->initWRect (lxWOff, hxWOff, lyWOff, hyWOff,
								   cxWMin, cyWMin,
								   cxW	 , cyW   );

				pPane ->m_PRect.Assign (0, 0, 0, 0);

				if (True/*pOwner ->HasStyle (WS_VISIBLE)*/)
				{
				    pPane ->Show (True);
				}
				else
				{
				    RecalcLayout ();
				}
			    };

	    LONG	    GetDragBarWidth	(Bool bVertical)
			    {
				return 4;
			    };

	    LONG	    SetVerDragBar	(Bool bSet)
			    {
				m_hxWOff   -= HasDragBar ();
				m_PRect.cx -= HasDragBar ();
				m_dFlags   &= 0xFF00FFFF   ;

			    if (bSet)
			    {
				m_dFlags   |= (((DWord)((Byte) GetDragBarWidth (True))) << 16);

				m_hxWOff   += HasDragBar ();
				m_PRect.cx += HasDragBar ();
			    }
				return HasDragBar ();
			    };

	    LONG	    SetHorDragBar	(Bool bSet)
			    {
				m_hyWOff   -= HasDragBar ();
				m_PRect.cy -= HasDragBar ();
				m_dFlags   &= 0xFF00FFFF   ;

			    if (bSet)
			    {
				m_dFlags   |= (((DWord)((Byte) GetDragBarWidth (False))) << 16);

				m_hyWOff   += HasDragBar ();
				m_PRect.cy += HasDragBar ();
			    }
				return HasDragBar ();
			    };

	    void	    SetZorder		(int iZorder)
			    {
				m_dFlags  = (m_dFlags & 0x00FFFFFF) | ((DWord)((Byte) iZorder << 24));
			    };

	    void	    Show		(Bool bShow)
			    {
				m_dFlags &=	    (~DPF_VISIBLE);
				m_dFlags |= (bShow) ? DPF_VISIBLE : 0;

				//GASSERT (NULL != GetParent ()) Root pane always visible !!!
				//
				GetParent () ->RecalcLayout ();
			    };

	    void	    RecalcLayout	();

//	    void	    Validate		();

	    void	    UpdateLayout	();

	    void	    Grow		(LONG dx, LONG dy);

	    void	    UpdateContext	();

	    GDockPane *	    GetPaneByCursor	(LONG px, LONG py) const;

	    Bool	    IsCursorOverDragBar	(LONG px, LONG py) const;
	};

	struct GLayoutCtrlEntry
	{
	    int		    m_nCtrlId;
	    DWord	    m_dLayout;
	};

	struct GLayoutRect : public GDockPane
	{
	    const GLayoutCtrlEntry *
			    m_pEnties;
	};
/*
#define DECLARE_LAYOUT_RECT

#define	DEFINE_LAYOUT_RECT(theClass,theName)		    \
    const GLayoutCtrlEntry theClass::theNameGLayoutCtrl
*/
/*
	template <int N> struct TLayoutRect : public GLayoutRect
	{
			    TLayoutRect (DWord ) {};

	    CtrlEntry	    m_pForEntries [N - 1];
	};
*/
#pragma pack ()
//
//[]------------------------------------------------------------------------[]



























    class GWindow;

//[]------------------------------------------------------------------------[]
//
#pragma pack (_M_PACK_VPTR)

// Next flags declared for HIWORD of the GM_MESSAGE::wParam 
//
//
#define	GM_MESSAGE_BASE	     0

//  uMsg   : ---------------------------------------------------------------[]
//
#define	GM_MESSAGE	    (WM_APP + GM_MESSAGE_BASE)

//  wParam : ---------------------------------------------------------------[]
//
//#define	GM_Create		    1	// Send to Child same as WM_CREATE
//  lParam  : m_hWnd
//
//  lResult : 0 : success, ! = 0 Abort creation
//
//[]------------------------------------------------------------------------[]

//  wParam : ---------------------------------------------------------------[]
//
//#define	GM_Destroy		    2	// Send to Child
//  lParam  : m_hWnd
//
//[]------------------------------------------------------------------------[]

//  wParam : ---------------------------------------------------------------[]
//
#define	GM_RegisterCtrlNotify		    1
//
//  lParam  : GWinMsgHandler <GWindow, GWinMsg> *
//
//  lResult : True if Registered False : None

//  wParam : ---------------------------------------------------------------[]
//
#define GM_LookAtChild			    2
//
//  lParam  :  * GM_LookAtChild_Param

    typedef struct 
    {
	GWindow *	pChild	;	// Some action was happened
	DWord		dFlags  ;

    }	GM_LookAtChild_Param	;

/*
//  wParam : ---------------------------------------------------------------[]
//
#define	GM_Old_Child		    4	// Send to Parent
//  lParam  : pChild
//
//[]------------------------------------------------------------------------[]

//  wParam : ---------------------------------------------------------------[]
//
#define GM_New_Parent		    4	// Send to Child
//  lParam  : pOldParent
//
//[]------------------------------------------------------------------------[]

//  wParam : ---------------------------------------------------------------[]
//
#define GM_New_RClient		    5	// Send to Child
//  lParam  : GRect
//
//[]------------------------------------------------------------------------[]
*/
//[]------------------------------------------------------------------------[]
//
#define	GM_Get_RootViewPane
//  wParma  : none
//  lParam  : none
//
//  lResult : Pointer to the root ViewPane
//
//[]------------------------------------------------------------------------[]
#pragma pack ()

//
//[]------------------------------------------------------------------------[]






#pragma pack (_M_PACK_VPTR)

    struct HookDispatchProcContext
    {
	GWinMsgProc	pOrgDspProc ;
	void *		oOrgDspProc ;
	GWinMsgProc	pUsrDspProc ;
	void *		oUsrDspProc ;
    };

#pragma pack ()

/*
static Bool GAPI hookDispatchProc (HookDispatchProcContext * pContext, GWinMsg & m)
{

    m.pDefProc = pContext ->pOrgDspProc;
    m.oDefProc = pContext ->oOrgDspProc;

    return (pContext ->pUsrDspProc)(pContext ->oUsrDspProc, m);
};

DWord GWinProcThunk::ExecDispatchProc (GWinMsgProc pUsrProc, void * oUsrProc)
{
    DWord  dExitCode;

    HookDispatchProcContext Context = {pDspProc, oDspProc,
				       pUsrProc, oUsrProc};

    SetDispatchProc ((GWinMsgProc) hookDispatchProc, & Context);

	   dExitCode = GWinMsgLoop::Exec ();

    SetDispatchProc (Context.pOrgDspProc, Context.oOrgDspProc);

    return dExitCode;
};
*/






































//[]------------------------------------------------------------------------[]
//
static void PrintWCRect (GWindow * w)
{
    GRect		 r ;

    w ->GetWindowRect (& r);
    printf ("\t-Current WinPos {%4d.%4d}{%4d.%4d}\n", r.x, r.y, r.cx, r.cy);

    w ->GetClientRect (& r);
    printf ("\t-Current Client {%4d.%4d}{%4d.%4d}\n", r.x, r.y, r.cx, r.cy);
};

static void PrintWINDOWPOS (DWORD dStyle, WINDOWPOS * wp)
{
    printf ("%c", (wp ->flags & SWP_FRAMECHANGED) ? 'f' : '.');
    printf ("%c", (wp ->flags & SWP_HIDEWINDOW	) ? 'h' : '.');
    printf ("%c", (wp ->flags & SWP_SHOWWINDOW	) ? 's' : '.');
    printf ("%c", (wp ->flags & SWP_NOACTIVATE	) ? '.' : 'a');
    printf ("%c", (wp ->flags & SWP_NOMOVE	) ? '.' : 'm');
    printf ("%c", (wp ->flags & SWP_NOSIZE	) ? '.' : 's');

    if (!(wp ->flags & SWP_NOMOVE))
    printf ("{%4d.%4d}", wp ->x,  wp ->y);
    if (!(wp ->flags & SWP_NOSIZE))
    printf ("{%4d.%4d}", wp ->cx, wp ->cy);

    if (wp ->flags & SWP_FRAMECHANGED)
    {
	switch (dStyle & (WS_MINIMIZE | WS_MAXIMIZE))
	{
	    case 0:
		printf ("\n\t->Normal");
		break;
	    case WS_MINIMIZE :
		printf ("\n\t->Minimize");
		break;
	    case WS_MAXIMIZE :
		printf ("\n\t->Maximize");
		break;
	};
    }

    printf ("%s\n", (wp ->flags & SWP_NOCOPYBITS) ? ", No copy" : ", Copy");
};
//
//[]------------------------------------------------------------------------[]








/*
//[]------------------------------------------------------------------------[]
//
DWord GWinMsgThread::ExecWindow (GWindow * pWindow, int iCmdShow)
{
    DWord	    dExitCode	    = (DWord) -1;
    Bool	    bOwnerEnabled   = False;
    Bool	    bHiden	    = False;
    GWindow *	    pExecWindow	    = NULL ;

    if ((NULL == pWindow	       )
    ||  (NULL == pWindow ->GetHWND   ())
    ||	(NULL != pWindow ->GetParent ()))
    {
	    return dExitCode;
    }
					// Save current visible state and
	    bHiden	  = 
	  ! pWindow ->IsWindowVisible    ();
					// Show window with iShowCmd
					//					
	    pWindow ->ShowWindow (iCmdShow);

    if	   (pWindow ->GetOwner ())
    {					// Disable owner if it exist & not disabled
	if (bOwnerEnabled = 
	    pWindow ->GetOwner () ->IsWindowEnabled (	  ))
	{
	    pWindow ->GetOwner () ->EnableWindow    (False);
	}
    }
    else
    {
	if (NULL == m_pMainWindow)	// Set this window as Main window
			  m_pMainWindow = pWindow;
    }
					// Save & set new Exec window
	    pExecWindow	= m_pExecWindow ;
			  m_pExecWindow = pWindow;

					// Run Dispatch processing
	    dExitCode	= RunDispatchLoop ();

					// Restore old m_ExecWindow
			  m_pExecWindow = pExecWindow;

					// Check if this is Main window
	if (m_pMainWindow = pWindow)
	{
			  m_pMainWindow = NULL;
	}

	if (bOwnerEnabled)		// If was disabled at entry, enable owner !!!
	{
	    pWindow ->GetOwner () ->EnableWindow    (True );
	}

	if (bHiden)			// If was hiden in entry, hide it !!!
	{
	    pWindow ->ShowWindow (SW_HIDE);
	}
	    return dExitCode;
};
//
//[]------------------------------------------------------------------------[]
*/















//[]------------------------------------------------------------------------[]
//
class GStatusBar : public GControl
{
DECLARE_SYS_WINDOW_CLASS (STATUSCLASSNAME);

public :
	    GStatusBar (const void * pWndId);
protected :
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
DEFINE_SYS_WINDOW_CLASS (GStatusBar);

GStatusBar::GStatusBar (const void * pWndId) : GControl (pWndId)
{};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
class GReBar : public GControl
{
DECLARE_SYS_WINDOW_CLASS (REBARCLASSNAME);

public :
	    GReBar	(const void * pWndId);

	LONG		GetBoundRect	    ();

protected :

protected :

	Bool		DispatchWinMsg	    (GWinMsg &);

	Bool		RBN_HeightChange    (GWinMsg &, NMHDR *);
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
DEFINE_SYS_WINDOW_CLASS (GReBar);

GReBar::GReBar (const void * pWndId) : GControl (pWndId)
{};

LONG GReBar::GetBoundRect ()
{
    int	    i;
    GRect   rl = {0, 0, 0, 0};

    for (i = 0; i < Send_WM (RB_GETROWCOUNT , (WPARAM) 0, (LPARAM) 0); i ++)
    {
	rl.cy +=    Send_WM (RB_GETROWHEIGHT, (WPARAM) i, (LPARAM) 0);
    }
	return rl.cy;
};

Bool GReBar::RBN_HeightChange (GWinMsg & m, NMHDR *)
{
    GRect	     wr;

    GetWindowRect (& wr);

    return m.DispatchComplete ();
};

Bool GReBar::DispatchWinMsg (GWinMsg & m)
{
/*
    switch (m.uType)
    {
    case GWinMsg::CtrlNotify :

	printf ("Rebar notify %d Capture = %08X\n", ((NMHDR *) m.lParam) ->code, ::GetCapture ());

	DISPATCH_WM_NOTIFY_CODE (RBN_HEIGHTCHANGE   , RBN_HeightChange	, NMHDR)

	return GControl::DispatchWinMsg (m);
    };
*/
	return GControl::DispatchWinMsg (m);
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
class GToolBar : public GControl
{
DECLARE_SYS_WINDOW_CLASS (TOOLBARCLASSNAME);

public :
	    GToolBar	(const void * pWndId);
/*
	Bool		Create	    (GWindow *	pParent	      ,
				     LPCTSTR    pTitle	      ,
				     DWord      dStyle	      ,
				     DWord      dExStyle      ,
				     int	x       = 0   ,
				     int	y       = 0   ,
				     int	cx      = 0   ,
				     int	cy      = 0   ,
				     void *	pParam  = NULL)
			{
			    if (GControl::Create (pParent	    , pTitle   ,
						 (dStyle | WS_CHILD), dExStyle ,
						  x, y, cx, cy, pParam))
			    {
				Send_WM (TB_BUTTONSTRUCTSIZE, (WPARAM) sizeof (TBBUTTON), 
							      (LPARAM) 0);
				return True ;
			    };
				return False;
			};
*/

protected :

	void		On_Destroy	    ();
	Bool		On_Create	    (void * pParam);

	Bool		DispatchWinMsg	    (GWinMsg &);

protected :
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
DEFINE_SYS_WINDOW_CLASS (GToolBar);

GToolBar::GToolBar (const void * pWndId) : GControl (pWndId)
{};

Bool GToolBar::On_Create (void * pParam)
{
    if (! GControl::On_Create (pParam))
    {
	return False;
    }
	Send_WM (TB_BUTTONSTRUCTSIZE, (WPARAM) sizeof (TBBUTTON), 
				      (LPARAM) 0);
	return True ;
};

void GToolBar::On_Destroy ()
{
//  UnRegisterChildNotifyHandler ();

    GControl::On_Destroy ();
};

Bool GToolBar::DispatchWinMsg (GWinMsg & m)
{
/*
    switch (m.uType)
    {
    case GWinMsg::CtrlNotify :

	printf ("Toolb notify %d Capture = %08X\n", ((NMHDR *) m.lParam) ->code, ::GetCapture ());

	return GControl::DispatchWinMsg (m);
    };
*/
	return GControl::DispatchWinMsg (m);
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
typedef const MENUITEMTEMPLATEHEADER * LPCMENUTEMPLATEHEADER;

class GToolBarMenu : public GToolBar
{
public :
	    GToolBarMenu    (const void * pWndId);

	Bool		Create		(GWindow *  pParent	 ,
					 HMODULE    hResModule	 ,
					 LPCTSTR    pResName	 ,
					 void *	    pParam = NULL);

	LONG		GetBoundRect	();

protected :

#ifdef _MSC_BUG_C2248
public :
#endif

	struct TrackPopupContext
	{
	    int		iCurItem ;
	    Bool	bTrack	 ;
	    Bool	bPopup	 ;

//	    MENUITEMINFO    mii	;
//	    TBBUTTON	    bt	;
	};

#ifdef _MSC_BUG_C2248
protected :
#endif

protected :

	Bool		DispatchTrackPopup  (	     GWinMsg &);
static	Bool GAPI	DispatchTrackPopup  (void *, GWinMsg &);

static	LRESULT GAPI	GetMessageHook	    (TrackPopupContext *, int	 cCode ,
								  WPARAM wParam,
								  LPARAM lParam);
	void		DoTrackPopupMenu    (int, Bool);
static 	void GAPI	DoTrackPopupMenu    (void * oDsp, void * pContext)
			{
			    ((GToolBarMenu *) oDsp) ->DoTrackPopupMenu ((int )(LOWORD((DWORD)pContext)),
									(Bool)(HIWORD((DWORD)pContext)));
			};

	Bool		DispatchWinMsg	    (GWinMsg &);

	Bool		TBN_DropDown	    (GWinMsg &, NMTOOLBAR   *);
	Bool		TBN_HotItemChange   (GWinMsg &, NMTBHOTITEM *);

protected :

	    GWindow *		m_pThunked  ;

	    HMENU		m_hMenu	    ;
	    int			m_nMenuItem ;

private :
/*
	    int			m_iCurItem  ;

	    Bool		m_bTracking ;
	    Bool		m_bBreak    ;
	    DWord		m_vKey	    ;
*/
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
GToolBarMenu::GToolBarMenu (const void * pWndId) : GToolBar (pWndId)
{
    m_pThunked	= NULL	;
    m_hMenu	= NULL	;
    m_nMenuItem = 0	;
/*
    m_iCurItem	= -1	;
    m_bTracking	= 
    m_bBreak	= False ;
    m_vKey	= 0	;
*/
};

LONG GToolBarMenu::GetBoundRect ()
{
    int	    i = 0;
    GRect   rl, rr;

	Send_WM (TB_GETITEMRECT,(WPARAM) i ++, (LPARAM) & rl);

    for (; i < m_nMenuItem; i ++)
    {
	Send_WM (TB_GETITEMRECT,(WPARAM) i,    (LPARAM) & rr);
    
	if (rl.cx < rr.cx) { rl.cx = rr.cx; }
	if (rl.cy < rr.cy) { rl.cy = rr.cy; }
    }
	rl.cx += rl.x;
	rl.cy += rl.y;

	Send_WM (TB_GETMAXSIZE, (WPARAM) 0, (LPARAM) & rr.Size ());

    printf ("Bound rect %d.%d : %d.%d (%d) {Max size %d.%d}\n", rl.x, rl.y, rl.cx, rl.cy,
								Send_WM (TB_GETROWS, (WPARAM) 0, (LPARAM) 0),
								rr.cx, rr.cy);

    return rl.cy;
};

Bool GToolBarMenu::Create (GWindow * pParent   ,
			   HMODULE   hResModule,
			   LPCTSTR   pResName  ,
			   void *    pParam    )
{
    if (! GToolBar::Create (pParent, NULL, WS_VISIBLE 
					 | CCS_NOPARENTALIGN | CCS_NORESIZE
					 | CCS_TOP | TBSTYLE_FLAT | TBSTYLE_LIST
					 | CCS_NODIVIDER | TBSTYLE_WRAPABLE,
					   0,
					   0, 0, 0, 0, pParam))
    {	return False;}
/*
    Send_WM (TB_SETEXTENDEDSTYLE, (WPARAM) 0, (LPARAM) (TBSTYLE_EX_HIDECLIPPEDBUTTONS |
    Send_WM (TB_GETEXTENDEDSTYLE, (WPARAM) 0, (LPARAM) 0)));
*/
/*
    if (GetHWND () || (NULL == pMenuOwner) || (NULL == pMenuOwner ->GetHWND    ()) 
					   || (NULL == pMenuOwner ->GetWndProc ()))
    {
	return False;
    }
*/

    LPCMENUTEMPLATEHEADER pTemplate = 
   (LPCMENUTEMPLATEHEADER) ::LoadResource (hResModule, pResName, RT_MENU);

    if ((NULL == pTemplate) || (NULL == (m_hMenu = ::LoadMenuIndirect (pTemplate))))
    {	return False;}


    int		    i  ;
    MENUITEMINFO    mii;
    TBBUTTON	    bt ;
    TCHAR	    buf [128];

    m_nMenuItem = ::GetMenuItemCount (m_hMenu);
/*
    MENUINFO	    mi ;
    mi.cbSize = sizeof (mi);
    mi.fMask  = MIM_MAXHEIGHT;
    ::GetMenuInfo (m_hMenu, & mi);
    mi.cyMax  = 16;
    mi.fMask |= MIM_APPLYTOSUBMENUS;
    ::SetMenuInfo (m_hMenu, & mi);
*/
    for (i = 0; i < m_nMenuItem; i ++)
    {
	mii.cbSize	= sizeof (mii);
	mii.fMask	= MIIM_TYPE | MIIM_STATE | MIIM_ID;
	mii.cch		= sizeof (buf)/sizeof (TCHAR);
	mii.dwTypeData  = buf;

	::GetMenuItemInfo (m_hMenu, i, True, & mii);

	bt.iBitmap	= I_IMAGENONE ;
	bt.idCommand	= mii.wID     ;	// == iItem
	bt.fsState	= (MFS_DISABLED & mii.fState) ? 0 : TBSTATE_ENABLED;
	bt.fsStyle	= BTNS_BUTTON | BTNS_DROPDOWN | BTNS_AUTOSIZE;
#ifdef _WIN32
	bt.bReserved[0] =
	bt.bReserved[1] = 0;
#else
#error	Not supported...
#endif
	bt.dwData	= i;
	bt.iString	= (INT_PTR) buf;

    if (! Send_WM (TB_INSERTBUTTON, (WPARAM) i, (LPARAM) & bt))
    {   
	return False;
    }
    }

    GRect wr;

    GetWindowRect (& wr);


	Send_WM	(TB_AUTOSIZE    , (WPARAM) 0, (LPARAM) 0);

    GetWindowRect (& wr);


    GetBoundRect ();


    return True;



//	SetStyle (CCS_NORESIZE | GetStyle ());
#if FALSE

    GRect w;

    GetWindowRect (& w);

    printf ("Menu bar size : (%08X) = %d.%d\n", GetHWND (), w.cx, w.cy);

	::SetWindowPos (GetHWND ()      ,
		        NULL	        ,
			0, 0, w.cx, w.cy,
		        SWP_NOZORDER 	);

    GetWindowRect (& w);

    printf ("Menu bar size : (%08X) = %d.%d\n", GetHWND (), w.cx, w.cy);

    return True;


//	SetStyle (0x984D/*CCS_NORESIZE*/ | GetStyle ());

    GetWindowRect (& w);

    printf ("Menu bar size : %d.%d\n", w.cx, w.cy);

	Send_WM  (TB_GETMAXSIZE  , (WPARAM) 0, (LPARAM) & w.Size ());

	::SetWindowPos (GetHWND ()      ,
		        NULL	        ,
			0, 0, w.cx, w.cy,
		        SWP_NOZORDER 
		      | SWP_NOACTIVATE 
		      | SWP_NOMOVE    
		      | SWP_NOCOPYBITS
		      | SWP_NOREDRAW	);

    w.cx -= 2;

    GetWindowRect (& w);

    printf ("Menu bar size : %d.%d\n", w.cx, w.cy);

	return True ;
#endif
};

Bool GToolBarMenu::DispatchTrackPopup (GWinMsg & m)
{
    GPoint	p  ;

//	printf ("SelfMessage %08X: %08X\n", m.hWnd ,
//					    m.uMsg );
/*
    return m.DispatchDefault ();

lBreak:

    if (m_bBreak)
    {
	m_bTracking = True ;
	m_bBreak    = False;

    printf ("X:Break with %d\n", m_iCurItem);

	::SendMessage (m_pThunked ->GetHWND (), WM_CANCELMODE, (WPARAM) 0, (LPARAM) 0);
    }
    if (m_bTracking)
    {
        return m.DispatchDefault ();
    }
*/
/*
    if ((WM_MENUSELECT	    == m.uMsg))
    {
	    m_vKey &= (~0x03);

	if (HIWORD(m.wParam) & MF_POPUP)
	{
    	    printf ("Pupup selected \n");
	}
	else
	{
	    m_vKey |= ( 0x02);
	    printf ("Menu item \n");
	}
    }
*/
/*
    if ((WM_UNINITMENUPOPUP == m.uMsg)
    ||  (WM_INITMENUPOPUP   == m.uMsg))
    {
    if  (WM_INITMENUPOPUP   == m.uMsg)
    {
    printf ("Pupup %08X\n", m.wParam);
    }

    printf ("Clear VK....\n");

	m_vKey	&= (~0x05);

	return m.DispatchDefault ();
    }
    
    if (m_vKey & 0x05)
    {
	if (m_vKey & 0x04)
	{
	    if (0 > -- m_iCurItem)
	    {
		m_iCurItem  = m_nMenuItem - 1;
	    }
	}
	if (m_vKey & 0x01)
	{
	    if (m_nMenuItem <= ++ m_iCurItem)
	    {
		m_iCurItem  = 0;
	    }
	}

	m_bBreak  = True;
	m_vKey   &= (~0x05);

	Post_WM (WM_KEYDOWN, VK_DOWN, 0);

	goto lBreak;
    }
*/
    if (WM_ENTERIDLE == m.uMsg)
    {
/*
    while (True)
    {
    printf ("Wait <\n");

	GWinMsgLoop::Wait ();

    printf ("> Wait\n");

	if (::PeekMessage (& msg, m_pThunked ->GetHWND (), 0, 0, PM_NOREMOVE))
	{
	    if (WM_MOUSEMOVE == msg.message)
	    {
		p.Assign (msg.pt.x, msg.pt.y); ScreenToClient (& p);

		i = Send_WM (TB_HITTEST, (WPARAM) 0, (LPARAM) & p);

		if ((0 <= i) && (i != m_iCurItem) && (i < m_nMenuItem))
		{
		    m_iCurItem = i   ;
		    m_bBreak   = True;

	printf ("Set MK.... %d\n", m_iCurItem);
		}
	    }
	    else 
	    if (WM_KEYDOWN == msg.message)
	    {
	    if ((VK_LEFT   == msg.wParam ) && (m_vKey & 0x08)) { m_vKey |= 0x04;}
	    if ((VK_RIGHT  == msg.wParam ) && (m_vKey & 0x02)) { m_vKey |= 0x01;}
	    }
	    return m.DispatchComplete ();
	}
    };
*/
    }
	    return m.DispatchDefault ();
};

Bool GAPI GToolBarMenu::DispatchTrackPopup (void * oDsp, GWinMsg & m)
{
    return ((GToolBarMenu *) oDsp) ->DispatchTrackPopup (m);
};

LRESULT GAPI GToolBarMenu::GetMessageHook (TrackPopupContext * tp, int	  nCode ,
								   WPARAM wParam,
								   LPARAM lParam)
{
    if ((HC_ACTION == nCode) && (PM_REMOVE == wParam) && (NULL != lParam))
    {
    printf ("GetMsgHook:: %08X, %08X %08X %08X\n", ((MSG *) lParam) ->hwnd   ,
						   ((MSG *) lParam) ->message,
						   ((MSG *) lParam) ->wParam ,
						   ((MSG *) lParam) ->lParam );
//	tp ->bTrack = False;

    }
    return ::CallNextHookEx (NULL, nCode, wParam, lParam);
};

void GToolBarMenu::DoTrackPopupMenu (int iFromItem, Bool bPopup)
{
    GWndProcThunk * pth;
    RECT	    r  ;

    MENUITEMINFO    mii;
    TBBUTTON	    bt ;
    TPMPARAMS	    tpm;
    MSG		    msg;

    TrackPopupContext  tc = { iFromItem, True, bPopup };
    HHOOK	       hh = NULL;
    GWndHookThunk    * th = new GWndHookThunk
			    ((GWinHookProc) GetMessageHook, & tc);

	    printf ("TrackPupup <<<\n");

    if (NULL != 
       (hh    = ::SetWindowsHookEx (WH_GETMESSAGE, 
				   (HOOKPROC) th, NULL, TCurrent () ->GetId ())))
    {
	do
	{
	    if (tc.bPopup)
	    {
	    }
	    else
	    {
	do
	{
	    if (::GetMessage (& msg, NULL, 0, 0))
	    {
		::TranslateMessage (& msg);
		::DispatchMessage  (& msg);
	    }

	} while (tc.bTrack && (! tc.bPopup));

	    }

	} while (tc.bTrack);

		::UnhookWindowsHookEx (hh);
    }

    delete (th);

	    printf ("TrackPopup >>>\n");















//	m_pTrackPopupContext = & tc ;

/*
    if (NULL == (pth = m_pThunked ->GetWndProc ()))
    {	return; }

GWinMsgLoop::BegModalLoop ();
*/



//  m_pThunked ->SubclassWndProc (DispatchTrackPopup, this);

#if FALSE

    do
    {
//	    m_vKey	= 0x0A ;
//	    m_bBreak	=
//	    m_bTracking = False;

    if (Send_WM (TB_GETBUTTON  , (WPARAM)   iCurItem  , (LPARAM) & bt))
    {
	Send_WM (TB_GETRECT    , (WPARAM) bt.idCommand, (LPARAM) & r );

	ClientToScreen	      (& r);

r.top	 += 10;
r.bottom += 10;


	tpm.cbSize   = sizeof (tpm);

	GetClientRect	    (& tpm.rcExclude);
	ClientToScreen	    (& tpm.rcExclude);

			       tpm.rcExclude.bottom = r.bottom;

	    printf ("[%d.%d]:[%d.%d]\n", tpm.rcExclude.left ,
					 tpm.rcExclude.right,
					 tpm.rcExclude.top  ,
					 tpm.rcExclude.bottom);


	mii.cbSize   = sizeof (mii);
	mii.fMask    = MIIM_SUBMENU;
	mii.hSubMenu = NULL;

	::GetMenuItemInfo (m_hMenu, bt.idCommand, False, & mii);

	if (mii.hSubMenu)
	{
	    printf ("ToolBarMenu : DropDown %d %d.%d\n", iCurItem, r.left, r.bottom);

//	    Send_WM (TB_PRESSBUTTON, (WPARAM) bt.idCommand, (LPARAM) True );

	    ::TrackPopupMenuEx (mii.hSubMenu, 
				TPM_LEFTBUTTON | TPM_VERTICAL | TPM_LEFTALIGN | TPM_TOPALIGN,
				r.left, r.bottom, m_pThunked ->GetHWND (), & tpm);

//	    Send_WM (TB_PRESSBUTTON, (WPARAM) bt.idCommand, (LPARAM) False);
	}
/*
	if (m_bTracking)
	{   
	    continue;
	}
*/
	    break;
    }

    } while (True);

#endif

//  m_pThunked ->UnSubclassWndProc ();



/*
	pth ->SetDispatchProc (m_pOrgProc , 
			       m_oOrgProc );
	m_pOrgProc = NULL;
	m_oOrgProc = NULL;
*/

/*
GWinMsgLoop::EndModalLoop ();
*/
};

Bool GToolBarMenu::TBN_DropDown (GWinMsg & m, NMTOOLBAR * tb)
{
    int		   i   ;
    TBBUTTONINFO   bti ;

    bti.cbSize	 = sizeof (bti);
    bti.dwMask	 = 0	       ;

    printf ("Drop Down (%08X) %d item\n", tb ->iItem, tb ->iItem);

    i = Send_WM (TB_GETBUTTONINFO, (WPARAM) tb ->iItem, (LPARAM) & bti);

    if ((0 <= i) && (i < m_nMenuItem))
    {{
//	DoTrackPopupMenu (i);

//	GWinMsgLoop::SetPostProc  (DoTrackPopupMenu, this, (void *) ((False << 16) | i));

	return m.DispatchComplete (TBDDRET_DEFAULT);
    }}
	return m.DispatchComplete (TBDDRET_NODEFAULT);

//  printf ("Drop Down %d item\n", i);

#if FALSE

    TBBUTTONINFO	   bti ;

    bti.cbSize	 = sizeof (bti);
    bti.dwMask	 = 0	       ;


//  ::SetFocus (GetHWND ());


    m_iCurItem	 = Send_WM (TB_GETBUTTONINFO, (WPARAM) tb ->iItem, (LPARAM) & bti);

    if ((0 <= m_iCurItem) && (m_iCurItem < m_nMenuItem))
    {
//		   Send_WM (TB_SETHOTITEM   ,  (WPARAM) -1	 , (LPARAM) 0);

//	GWinMsgLoop::SetPostProc (TrackPopupProc, this);

	TrackPopupProc ();
    }

    return m.DispatchComplete (TBDDRET_DEFAULT);

#endif
};

Bool GToolBarMenu::TBN_HotItemChange (GWinMsg & m, NMTBHOTITEM * hi)
{
    printf ("HotItem change..%d -> %d\n", hi ->idOld, hi ->idNew);

//  return m.DispatchComplete (-1);

    return m.DispatchComplete (0);

    return m.DispatchComplete (IsActive () ? 0 : -1);
};

Bool GToolBarMenu::DispatchWinMsg (GWinMsg & m)
{
/*
    switch (m.uType)
    {
    case GWinMsg::CtrlNotify :

	DISPATCH_WM_NOTIFY_CODE (TBN_DROPDOWN	   , TBN_DropDown	, NMTOOLBAR  )
	DISPATCH_WM_NOTIFY_CODE (TBN_HOTITEMCHANGE , TBN_HotItemChange	, NMTBHOTITEM)

	return GToolBar::DispatchWinMsg (m);
    };
*/
	return GToolBar::DispatchWinMsg (m);
};
//
//[]------------------------------------------------------------------------[]





















//[]------------------------------------------------------------------------[]
//
class GDockParent : public GParentWindow
{
public :
	    GDockParent	  (const void * pWndId) :

	    GParentWindow (pWndId)
	    {
		m_hNSGrowCursor = ::LoadCursor (NULL, IDC_SIZENS);
		m_hWEGrowCursor = ::LoadCursor (NULL, IDC_SIZEWE);
	    };

	GDockPane &	RootPane ()	    { return m_RootPane;};

protected :

	GDockPane *	GetPaneByCursor	    (LONG px, LONG py, Bool * pOverDragBar = NULL);

static	void GAPI	DragPaneProc	    (DragProcReason    dpr,
					     DragProcContext * dpc);

virtual	void		On_PaintPaneBkGnd   (HDC, GDockPane *, const GRect & pr,
							       const GRect & wr,
							       const GRect * db);

virtual	void		On_PaintPane	    (HDC, GDockPane *, const GRect & wr);

protected :

	Bool		On_Create	    (void *);

	Bool		DispatchWinMsg	    (GWinMsg &);

	Bool		On_WM_SetCursor	    (GWinMsg &);
	Bool		On_WM_MouseLBDown   (GWinMsg &);

	Bool		On_WM_PosChanging   (GWinMsg &);
	Bool		On_WM_NcCalcSize    (GWinMsg &);
	Bool		On_WM_PosChanged    (GWinMsg &);

	Bool		On_WM_PaintBkGnd    (GWinMsg &);
	Bool		On_WM_Paint	    (GWinMsg &);

//	Bool		On_WM_Notify	    (GWinMsg &);

protected :

	    GDockPane		m_RootPane	;

	    HCURSOR		m_hNSGrowCursor ;
	    HCURSOR		m_hWEGrowCursor	;
};
//
//[]------------------------------------------------------------------------[]


//[]------------------------------------------------------------------------[]
//
Bool GDockParent::On_Create (void * pParam)
{
    if (! GParentWindow::On_Create (pParam))
    {
	return False;
    }

    GRect	     rc;

    GetClientRect (& rc);

    m_RootPane.InitRoot (this, SizeParam   (rc.cx, rc.cy),
//			       LayoutParam (5, 5, 5, 5),
//			       LayoutParam (5, 10, 10, 5),
			       LayoutParam (0, 0, 0, 0),
			       SizeParam   (0, 0));

	return True ;
};

Bool GDockParent::DispatchWinMsg (GWinMsg & m)
{
    switch (m.uType)
    {
    case GWinMsg::WndProc :

	DISPATCH_WM	(WM_SETCURSOR,		On_WM_SetCursor	    )
	DISPATCH_WM	(WM_LBUTTONDOWN,	On_WM_MouseLBDown   )

	DISPATCH_WM	(WM_WINDOWPOSCHANGING,	On_WM_PosChanging   )
	DISPATCH_WM	(WM_NCCALCSIZE,		On_WM_NcCalcSize    )
	DISPATCH_WM	(WM_WINDOWPOSCHANGED,	On_WM_PosChanged    )

	DISPATCH_WM	(WM_ERASEBKGND,		On_WM_PaintBkGnd    )
	DISPATCH_WM	(WM_PAINT,		On_WM_Paint	    )

	return GParentWindow::DispatchWinMsg (m);
    };
	return GParentWindow::DispatchWinMsg (m);
};

GDockPane * GDockParent::GetPaneByCursor (LONG px, LONG py, Bool * pOverDragBar)
{
    GPoint  cp (px, py);

    GDockPane * p = m_RootPane.GetPaneByCursor (cp.x, cp.y);

    if (pOverDragBar)
    {
	p ->OwnerToP (cp);			// p != NULL in any case

      * pOverDragBar = p ->IsCursorOverDragBar (cp.x, cp.y) ? True :False;
    }
	return  p;
};

Bool GDockParent::On_WM_SetCursor (GWinMsg & m)
{
//  Do this for controls
//
//  if (GetParent () && (0 != GetParent () ->Send_WM (m.uMsg, wParam, lParam)))
//  {
//	return m.DispatchComplete (True);
//  }
//
    if (GetHWND () == (HWND)  m.wParam)
    {
    if (HTCLIENT   == LOWORD (m.lParam))
    {{
	Bool	bOverDragBar;
	GPoint		  cp;
	GDockPane *	   p;

	    ::GetCursorPos   (		  & cp);
	    ::ScreenToClient (GetHWND (), & cp);

	p = GetPaneByCursor  (cp.x, cp.y, & bOverDragBar);

	if (bOverDragBar)
	{
	    ::SetCursor (p ->ChildOfVer () ? m_hWEGrowCursor 
					   : m_hNSGrowCursor);
	    return m.DispatchComplete (True);
	}
    }}
    }
	    return m.DispatchDefault  ();
};

void GAPI GDockParent::DragPaneProc (DragProcReason    dpr,
				     DragProcContext * dpc)
{
    HDC	    dc;

    switch (dpr)
    {
	case Drag_Begin :

	    dpc ->SetBrush (::CreateHalftoneBrush ());
	    break;

	case Drag_EndCancel :
	case Drag_End :

	    ::DeleteObject (dpc ->GetBrush ());
	    dpc ->SetBrush (NULL);
	    break;

	case Drag_ShowGhost :
	case Drag_HideGhost :

	    dc = ::GetDCEx   (dpc ->Window () ->GetHWND (), NULL,   DCX_PARENTCLIP 
								  | DCX_CLIPSIBLINGS 
								  | DCX_LOCKWINDOWUPDATE);
		 ::PatBlt    (dc, & dpc ->Ghost	   (), 
				    dpc ->GetBrush (), PATINVERT);

		 ::ReleaseDC (dpc ->Window () ->GetHWND (), dc);
	    break;
    };
};

Bool GDockParent::On_WM_MouseLBDown (GWinMsg & m)
{
    Bool    bOverDragBar;
    GDockPane *	       p;

	p = GetPaneByCursor  (GET_X_LPARAM (m.lParam),
			      GET_Y_LPARAM (m.lParam),
			    & bOverDragBar	    );
    if (bOverDragBar)
    {{
	DragProcContext	 dpc (DragPaneProc, this);
	GRect		 pr;

	p ->DragBarRect (pr);
	p ->PToOwner	(pr.Pos ());

	dpc.InitGhost	(pr,  GET_X_LPARAM (m.lParam),
			      GET_Y_LPARAM (m.lParam));

	if (ExecCaptureMouseDragging 
			(DragDispatchProc,  dpc))
	{
	    pr.Pos    () -= dpc.Ghost ().Pos ();

	    p ->Grow  (- pr.Pos ().x, - pr.Pos ().y);
	}
	if (HasFlag  (GWF_NEEDUPDATE))
	{
	    m_RootPane.UpdateContext ();

	    SetFlags (GWF_NEEDUPDATE, 0);
	}


        return m.DispatchComplete ();
    }}
        return m.DispatchDefault  ();
};

Bool GDockParent::On_WM_PosChanging (GWinMsg & m)
{
    printf ("\n\nWM_PosChanging <------------------------------\n");

    PrintWINDOWPOS (GetStyles (), (WINDOWPOS *) m.lParam);

    m.DispatchDefault ();

    if (((WINDOWPOS *) m.lParam) ->flags & SWP_NOSIZE)
    {}
    else
    {{
	GRect		 wr;

	GetWindowRect (& wr);

	if ((wr.cx != ((WINDOWPOS *) m.lParam) ->cx)
	||  (wr.cy != ((WINDOWPOS *) m.lParam) ->cy))
	{
	((WINDOWPOS *) m.lParam) ->flags |= SWP_NOCOPYBITS;
	}
    }}

    PrintWINDOWPOS (GetStyles (), (WINDOWPOS *) m.lParam);
    
    printf ("\n\nWM_PosChanging (%08X) >------------------------------\n", GetHWND ());

    return m.DispatchComplete ();
};

Bool GDockParent::On_WM_NcCalcSize (GWinMsg & m)
{
    printf ("\n\nWM_NcCalcSize <-------------------------------\n");

    if (m.wParam)
    {
	m.DispatchDefault ();
/*
    // Bypath any Client's copy
    //
       ((NCCALCSIZE_PARAMS *) m.lParam) ->rgrc [1] =
       ((NCCALCSIZE_PARAMS *) m.lParam) ->rgrc [0];

       ((NCCALCSIZE_PARAMS *) m.lParam) ->rgrc [2] =
       ((NCCALCSIZE_PARAMS *) m.lParam) ->rgrc [0];

	m.lResult = WVR_VALIDRECTS;
*/
    }
    else
    {
	m.DispatchDefault ();
    }

    if (! HasStyle (WS_MINIMIZE))
    {
	m_RootPane.Grow (((NCCALCSIZE_PARAMS *) m.lParam) ->rgrc [0].right  -
			 ((NCCALCSIZE_PARAMS *) m.lParam) ->rgrc [0].left   -
			 m_RootPane.PRectSize ().x,
			 ((NCCALCSIZE_PARAMS *) m.lParam) ->rgrc [0].bottom -
			 ((NCCALCSIZE_PARAMS *) m.lParam) ->rgrc [0].top    -
			 m_RootPane.PRectSize ().y);
/*
::Sleep (60);
*/

// ???
//	m_RootPane.m_dFlags |= DPF_UPDATED;
//	SetFlags (GWF_NEEDUPDATE | GetFlags ());
    }

    printf ("WM_NcCalcSize (%08X) >-------------------------------\n", GetHWND ());

    return m.DispatchComplete ();
};

Bool GDockParent::On_WM_PosChanged (GWinMsg & m)
{
//-------------------------------
//
{
    printf ("WM_PosChanged <-------------------------------\n");

    PrintWCRect (this);

    PrintWINDOWPOS  (GetStyles (), (WINDOWPOS *) m.lParam);

    printf ("----------------------------------------------\n\n");
}
//
//-------------------------------

    if ((((WINDOWPOS *) m.lParam) ->flags & SWP_NOSIZE) || (HasStyle (WS_MINIMIZE)))
    {}
    else
    {
	if (HasFlag  (GWF_NEEDUPDATE))
	{
    printf ("Update layouts <<\n\n");

	    m_RootPane.UpdateContext  ();

	    SetFlags (GWF_NEEDUPDATE, 0);

    printf ("Update layouts >>\n\n");
	}
    }

    printf ("WM_PosChanged (%08X) >-------------------------------\n", GetHWND ());

//	return m.DispatchComplete ();

	return m.DispatchDefault  ();
};


Bool GDockParent::On_WM_PaintBkGnd (GWinMsg & m)
{
    printf ("WM_PaintBkGnd <-------------------------------\n");

    PrintWCRect (this);

    GRect   pr;
    GRect   wr;
    GRect   db;
    GRect * pd;

    if (True/*::GetUpdateRect (GetHWND (), & pr.Rect (), False)*/)
    {

//  printf ("Update : {%d.%d},{%d.%d}\n", pr.x, pr.y, pr.cx, pr.cy);

//	    ::SetWindowOrgEx ((HDC) m.wParam, 40, 40, & org);

	    m_RootPane.PRect	(pr);
	    m_RootPane.PToOwner	(pr.Pos ());

	    m_RootPane.WRect	(wr);
	    wr.Move	(pr.Pos ());

	    On_PaintPaneBkGnd ((HDC) m.wParam, & m_RootPane, pr, wr, NULL);

    for (GDockPane * p = m_RootPane.m_SList.first (); p; p = m_RootPane.m_SList.getnext (p))
    {
	if (p ->IsVisible ())
	{
	    p ->PRect    (pr);
	    p ->PToOwner (pr.Pos ());

	    p ->WRect    (wr);
	    wr.Move	 (pr.Pos ());

	    if (p ->HasDragBar ())
	    {
		db = pr;

		if (p ->ChildOfVer ())
		{
		    pr.cx -= (db.cx = p ->HasDragBar ());
		    db.x  +=  pr.cx;
		}
		else
		{
		    pr.cy -= (db.cy = p ->HasDragBar ());
		    db.y  +=  pr.cy;
		}
		pd = & db;
	    }
	    else
	    {
		pd = NULL;
	    }

	    On_PaintPaneBkGnd ((HDC) m.wParam, p, pr, wr, pd);
	}
    }

    }
else
{
    printf ("Update : Empty\n");
}
{
    void * p;	_asm { mov p,esp}

    printf ("WM_PaintBkGnd (%08X:%08X)>-------------------------------\n", this, GetHWND ());

}
    return m.DispatchComplete (True);
};

Bool GDockParent::On_WM_Paint (GWinMsg & m)
{
    PAINTSTRUCT	    ps;
    GRect	     r;
    RECT	    rc;
    LONG	    mix;
    LONG	    miy;


{
    printf ("WM_Paint <------------------------------------\n");

    ::GetUpdateRect (GetHWND (), & r.Rect (), False);

    printf ("Update : {%d.%d},{%d.%d}\n", r.x, r.y, r.cx, r.cy);

}
    ::BeginPaint (GetHWND (), & ps);

    if (ps.fErase)
    {
	printf ("Need erase BkGnd\n");

//	Send_WM (WM_ERASEBKGND, (WPARAM) ps.hdc, (LPARAM) NULL);
    }

	GetClientRect (& rc);

//	FillRect  (ps.hdc, & rc, (HBRUSH) ::GetStockObject (BLACK_BRUSH));

	m_RootPane.WRect  (r);
    
	m_RootPane.PToOwner (r.Pos ());

	CopyRect  (& rc, & r);

//	FrameRectEx (ps.hdc, & rc, (HBRUSH) ::GetStockObject (LTGRAY_BRUSH), 1, PATCOPY);

	m_RootPane.GetMinSize (& mix, & miy);

    printf ("Root :  {%4d.%4d}{%4d.%4d}:[%4d.%4d]\n", r.x, r.y, r.cx, r.cy, mix, miy);

	for (GDockPane * p = m_RootPane.m_SList.first (); p; p = m_RootPane.m_SList.getnext (p))
	{
	    p ->WRect (r);

	    p ->PToOwner (r.Pos ());

	    CopyRect (& rc, & r);

//	    FrameRectEx (ps.hdc, & rc, (HBRUSH) ::GetStockObject (GRAY_BRUSH), 1, PATCOPY);


    printf ("\t{%4d.%4d}{%4d.%4d} User:{%4d.%4d}\n", r.x, r.y, r.cx, r.cy, p ->m_cxWUsr, p ->m_cyWUsr);
/*
	if (p ->HasDragBar ())
	{
	    p ->PRect (r);

	    r.y  = r.cy - p ->HasDragBar ();
	    r.cy =	  p ->HasDragBar ();

	    p ->PToOwner (r.Pos ());

	    CopyRect (& rc, & r);

	    FrameRectEx (ps.hdc, & rc, (HBRUSH) ::GetStockObject (WHITE_BRUSH), 1, PATCOPY);
	}
*/
	    if (p ->m_pOwner)
	    {
//		::UpdateWindow (((GWindow *) p ->m_pOwner) ->GetHWND ());
	    }
	}


    ::EndPaint	 (GetHWND (), & ps);

{
    printf ("WM_Paint >------------------------------------\n");
}
    return m.DispatchComplete ();
};


void GDockParent::On_PaintPaneBkGnd (HDC dc, GDockPane * p, const GRect & pr,
							    const GRect & wr,
							    const GRect * db)
{
//  FillRect	(dc, & pr, (HBRUSH) ::GetStockObject (WHITE_BRUSH));

    FillRect	(dc, & pr, (HBRUSH) ::GetStockObject (BLACK_BRUSH));

    FrameRectEx (dc, & pr, (HBRUSH) ::GetStockObject (LTGRAY_BRUSH), 1, PATCOPY);

    FrameRectEx (dc, & wr, (HBRUSH) ::GetStockObject (GRAY_BRUSH  ), 1, PATCOPY);

    if (db)
    {
    FrameRectEx (dc,   db, (HBRUSH) ::GetStockObject (WHITE_BRUSH ), 1, PATCOPY);
    }

    printf ("\t\t...PaintBkGnd [(%d,%d).(%d,%d)], %08X\n", pr.x, pr.y, pr.cx, pr.cy, db);

};

void GDockParent::On_PaintPane (HDC dc, GDockPane * p, const GRect & wr)
{
};
//
//[]------------------------------------------------------------------------[]













//[]------------------------------------------------------------------------[]
//
template <class T = GWindow> class TDockingWindow : public GDockPane
{
public :
		TDockingWindow	(const void * pWndId) : m_Window (pWndId) {};

	T &		Window	    ()	    { return m_Window;};
/*
	Bool		Create	    (GDockParent *  pParent  ,
				     LPCTSTR	    pTitle   ,
				     DWord	    dStyle   ,
				     DWord	    dExStyle ,
				     int	    x       = 0   ,
				     int	    y       = 0   ,
				     int	    cx      = 0   ,
				     int	    cy      = 0   ,
				     void *	    pParam  = NULL)
			{
			    if (m_Window.Create (pParent, pTitle     ,
						 dStyle , dExStyle   ,
						 x, y, cx, cy, pParam))
			    {
				return True ;
			    }
				return False;
			};
*/
/*
	Bool		Create	    (GWindow *	pParent	      ,
				     LPCTSTR    pTitle	      ,
				     DWord      dStyle	      ,
				     DWord      dExStyle      ,
				     int	x       = 0   ,
				     int	y       = 0   ,
				     int	cx      = 0   ,
				     int	cy      = 0   ,
				     void *	pParam  = NULL)
			{
			};
*/
protected :
	    T			m_Window	;
};
//
//[]------------------------------------------------------------------------[]



















//[]------------------------------------------------------------------------[]
//
void GDockPane::GetMinSize (LONG * cxPMin, LONG * cyPMin) const
{
    if (!(m_dFlags & DPF_VISIBLE))
    {
	if (cxPMin)
	{ * cxPMin = 0;}
	if (cyPMin)
	{ * cyPMin = 0;}

	return;
    }

    LONG    cxCMin;
    LONG    cyCMin;

    LONG    cxWMin = 0;
    LONG    cyWMin = 0;

    for (GDockPane * p = m_SList.first (); p; p = m_SList.getnext (p))
    {
	    p ->GetMinSize (& cxCMin, & cyCMin);

	if (m_dFlags & DPF_VER)
	{
		cxWMin += cxCMin;

	    if (cyWMin  < cyCMin)
	    {	cyWMin  = cyCMin;}
	}
	else
	{
	    if (cxWMin  < cxCMin)
	    {	cxWMin	= cxCMin;}

		cyWMin += cyCMin;
	}
    }
	if (cxWMin < m_cxWMin)
	{   cxWMin = m_cxWMin;}

	if (cyWMin < m_cyWMin)
	{   cyWMin = m_cyWMin;}

	if (cxPMin)
	{ * cxPMin = m_lxWOff + cxWMin + m_hxWOff;}
	if (cyPMin)
	{ * cyPMin = m_lyWOff + cyWMin + m_hyWOff;}
};
//
//[]------------------------------------------------------------------------[]
//
#pragma pack (_M_PACK_LONG)

    struct GDockPaneEntry
    {
	GDockPane * pPane     ;
	Bool	    bGrowable ;
	int	    gPriority ;
	LONG 	    czT	      ;
	LONG 	    czWUsr    ;
	LONG 	    czWMin    ;
	LONG	    czW	      ;
    };

#pragma pack ()

static int selectEntriesWithHiPriority (GDockPaneEntry ** pp, int cPriority, int n, GDockPaneEntry * pe)
{
    GDockPaneEntry *  e ;
    int		   i, k ;

    int	lPriority  = GROW_PRIORITY_LO - 1;

    for (e = pe, i = 0, k = 0; i < n; i ++, e ++)
    {
	if (e ->bGrowable && (e ->gPriority < cPriority)
			  && (e ->gPriority > lPriority))
	{
	    lPriority = e ->gPriority; k ++;
	}
    }

    if (0 == k)
    {	return 0;}

    for (e = pe, i = 0, k = 0; i < n; i ++, e ++)
    {
	if (e ->bGrowable && (e ->gPriority == lPriority))
	{
	    pp [k ++] = e;
	}
    }

    for (n = 0    ; n < k; n ++)
    {
    for (i = n + 1; i < k; i ++)
    {
	if (pp [n] ->czWUsr > pp [i] ->czWUsr)
    	{
	    e = pp [i]; pp [i] = pp [n]; pp [n] = e;
	}
    }
    }
	return k;
};

static void calcLayout (int n, GDockPaneEntry * pe, LONG W)
{
    GDockPaneEntry *  e ;
    int		   i, k ;
    int	      cPriority ;
    LONG	      W2;
    LONG     czW, czWUsr;

    GDockPaneEntry *  ps [DPS_MAX];

lRepeat:

    for (e = pe, i = 0, czW = czWUsr = 0; i < n; i ++, e ++)
    {
	if (e ->bGrowable)
	{
	    czW	+= e ->czW;
        }
    }
	    W2 = W; i = -1;

    cPriority  = GROW_PRIORITY_HI + 1;

    while (0 < (k = selectEntriesWithHiPriority (ps, cPriority, n, pe)))
    {
	    czW -= (W - W2);

	for (i = 0, czWUsr  = 0; i < k; i ++)
	{
		    czWUsr += ((ps [i] ->czWUsr) * czW / W);
	}

	for (i = 0; i < k; i ++)
	{
		 ps [i] ->czW = (czWUsr) ? (ps [i] ->czWUsr * czW / czWUsr) : (czW / k);

	    if ( ps [i] ->czW > W2)
	    {
		 ps [i] ->czW = W2;
	    }

	    if ( ps [i] ->czW <  ps [i] ->czWMin)
	    {
		 ps [i] ->czW =  ps [i] ->czWMin;

	    W -= ps [i] ->czW;

		 ps [i] ->bGrowable = False;

		 goto lRepeat;
	    }

	    if ((ps [i] ->gPriority	       )

	    &&  (ps [i] ->czW > ps [i] ->czWUsr))
	    {
		 ps [i] ->czW = ps [i] ->czWUsr;
	    }

	    if (0 > (W2 -= ps [i] ->czW))
	    {
		 ps [i] += W2;
		     W2 += W2;
	    }
	}
	    cPriority = (* ps) ->gPriority;
    };

    if ((0 < W2) && (0 <= i))
    {
	ps [i - 1] ->czW += W2;
    }
};

static int buildPaneEntry (GDockPaneEntry * pe, GDockPane * p)
{
    if (p ->IsVisible ())
    {
	    pe ->pPane	    = p		       ;
	    pe ->bGrowable  = p ->IsGrowable ();

	if (p ->ChildOfVer ())
	{
	    pe ->gPriority  = p ->GetVerGrowPriority ()  ;
	    pe ->czT	    = p ->m_lxWOff + p ->m_hxWOff;
	    pe ->czWUsr	    = p ->m_cxWUsr		 ;
			      p ->GetMinSize (& pe ->czWMin, NULL);
	    pe ->czWMin    -= pe ->czT			 ;
	    pe ->czW	    = p ->WRectSizeX ()		 ;
	}
	else
	{
	    pe ->gPriority  = p ->GetHorGrowPriority ()  ;
	    pe ->czT	    = p ->m_lyWOff + p ->m_hyWOff;
	    pe ->czWUsr	    = p ->m_cyWUsr		 ;
			      p ->GetMinSize (NULL, & pe ->czWMin);
	    pe ->czWMin    -= pe ->czT			 ;
	    pe ->czW	    = p ->WRectSizeY ()		 ;
	}
	return 1;
    }
	return 0;
};

void GDockPane::RecalcLayout ()
{
    GDockPane *	     p ;
    GDockPane *	  prev ;
    LONG	  d, z ;

    z = (m_dFlags & DPF_VER) ? WRectPosX () : WRectPosY ();

    for (p = m_SList.first (), prev = NULL; p; p = m_SList.getnext (p))
    {
	if (p ->IsVisible ())
	{
	    if (m_dFlags & DPF_VER)
	    {
		if (p ->IsGrowable () && prev && prev ->IsGrowable ())
		{
		    d  = prev ->SetVerDragBar (True);

		    for  (; prev != p; prev = prev ->m_SLink.next ())
		    {
			if (prev ->IsVisible ())
			{
			    prev ->m_PRect.x += d;
			}
		    }
		    z += d;
		}
		    p ->m_PRect.Assign (z, WRectPosY (), p ->m_lxWOff + p ->m_cxWUsr + p ->m_hxWOff, WRectSizeY ());

		    z += p ->m_PRect.cx ;
	    }
	    else
	    {
		if (p ->IsGrowable () && prev && prev ->IsGrowable ())
		{
		    d  = prev ->SetHorDragBar (True);

		    for  (; prev != p; prev = prev ->m_SLink.next ())
		    {
			if (prev ->IsVisible ())
			{
			    prev ->m_PRect.y += d;
			}
		    }
		    z += d;
		}
		    p ->m_PRect.Assign (WRectPosX (), z, WRectSizeX (), p ->m_lyWOff + p ->m_cyWUsr + p ->m_hyWOff);

		    z += p ->m_PRect.cy ;
	    }
	    if (p ->IsGrowable ())
	    {
		prev = p;
	    }
	}
	else
	{
		    p ->m_dFlags &= (~DPF_UPDATED);

	    if (m_dFlags & DPF_VER)
	    {
		    p ->SetVerDragBar  (False);

		    p ->m_PRect.Assign (0, 0, p ->m_lxWOff + p ->m_cxWUsr + p ->m_hxWOff, WRectSizeY ());
	    }
	    else
	    {
		    p ->SetHorDragBar  (False);

		    p ->m_PRect.Assign (0, 0, WRectSizeX (), p ->m_lyWOff + p ->m_cyWUsr + p ->m_hyWOff);
	    }
	}
    }
	UpdateLayout ();

	Grow (0, 0);
};

void GDockPane::UpdateLayout ()
{
    LONG	     z ;
    LONG	  C, W ;

    GDockPane *	     p ;
    int		  i, n ;
    union
    {
    GDockPaneEntry * e ;
    void	   * o ;
    };
    GDockPaneEntry   pe [DPS_MAX];

		     m_dFlags |= DPF_UPDATED;

    if (NULL != (o = GetOwner ()))
    {
    ((GWindow *) o) ->SetFlags  (0, GWF_NEEDUPDATE);
    }

    for (p = m_SList.first (), e = pe; p; p = m_SList.getnext (p))
    {
	if (buildPaneEntry (e, p))
	{
	    e ++;
	}
    }

    if (0 == (n = e - pe))
    {
	return;
    }

    for (e = pe, i = 0, C = 0; i < n; i ++, e ++)
    {
	    C += e ->czT ;

	if (! e ->bGrowable)
	{
	    C += e ->czW ;
	}
    }

    W = ((m_dFlags & DPF_VER) ? WRectSizeX () : WRectSizeY ()) - C;

    calcLayout (n, pe, W);

    z = (m_dFlags & DPF_VER)  ? WRectPosX  () : WRectPosY  ();

    for (e = pe, i = 0; i < n; i ++, e ++)
    {
	 p = e ->pPane;

	if (m_dFlags & DPF_VER)
	{
	    p ->m_PRect.x  = z;
	    p ->m_PRect.y  = WRectPosY  ();
	    p ->m_PRect.cy = WRectSizeY ();
	    z		  +=
	   (p ->m_PRect.cx = p ->m_lxWOff + e ->czW + p ->m_hxWOff);
	}
	else
	{
	    p ->m_PRect.x  = WRectPosX  ();
	    p ->m_PRect.y  = z;
	    p ->m_PRect.cx = WRectSizeX ();
	    z		  +=
	   (p ->m_PRect.cy = p ->m_lyWOff + e ->czW + p ->m_hyWOff);
	}

//	if (NULL == p ->m_pOwner)
	{
	    p ->UpdateLayout ();
	}
    }
};

void GDockPane::Grow (LONG dx, LONG dy)
{
    LONG *	     pz;
    LONG 	   micz;
    LONG	   micx;
    LONG	   micy;
    LONG	   P, W;
    LONG	      z;

    GDockPane *	     p ;
    int	       g, i, n ;
    GDockPaneEntry * e ;

    GDockPaneEntry   pe [DPS_MAX];

    if (IsRoot ())
    {
	GetMinSize (& micx, & micy);

	if ((m_PRect.cx + dx) < micx)
	{
	    dx = micx - m_PRect.cx;
	}
	if ((m_PRect.cy + dy) < micy)
	{
	    dy = micy - m_PRect.cy;
	}
	if ((0 == dx) && (0 == dy))
	{
	    return;					// !!!!!!!!
	}

	m_PRect.Grow (dx, dy);

	UpdateLayout ();

	m_cxWUsr = WRectSizeX ();
	m_cyWUsr = WRectSizeY ();
    }
    else
    {
	if (! IsVisible  ())
	{
	    return;
	}
	if (! IsGrowable ())
	{
	    if (ChildOfVer ())
	    {
		m_cxWMin  =
		m_cxWUsr += dx;
	    }
	    else
	    {
		m_cyWMin  =
		m_cyWUsr += dy;
	    }

	    GetParent () ->RecalcLayout ();

	    return;
	}

	if (ChildOfVer ())
	{
	    dy = 0; pz = & dx;
	}
	else
	{
	    dx = 0; pz = & dy;
	}

	if (0 < * pz)
	{
		    buildPaneEntry (e = pe, this);

		    e ++;

	    for (p = GetParent () ->m_SList.getnext (this), n = 0; p; p = GetParent () ->m_SList.getnext (p))
	    {
		if (buildPaneEntry (e, p))
		{
		    if (e ->bGrowable)
		    {
			if (0 == n)
			{
			    e ->gPriority = 1;
			}
			else 
			if (e ->gPriority > 0)
			{
			    e ->bGrowable = False;
			}
			n ++;
		    }
		    e ++;
		}
	    }
	    if  (1 > n)
	    {
		return;
	    }
		n = e - pe;
		pe [g =	     0].bGrowable = False;
	}
	else
	if  (0 > * pz)
	{
	    for (p = GetParent () ->m_SList.first (), e = pe; p != this; p = p ->m_SLink.next ())
	    {
		if (buildPaneEntry (e, p))
		{
		    if (e ->bGrowable)
		    {
			if (e ->gPriority > 0)
			{
			    e ->bGrowable = False;
			}
		    }
		    e ++;
		}
	    }
		    buildPaneEntry (e, p);

		    e ++;

	    for (p = GetParent () ->m_SList.getnext (this), n = 0; (n == 0) && p; p = GetParent () ->m_SList.getnext (p))
	    {
		if (buildPaneEntry (e, p))
		{
		    if (e ->bGrowable)
		    {	{
			    n ++;

			    e ->gPriority = 1;
		    }	}
		    e ++;
		}
	    }
	    if (1 > n)
	    {
		return;
	    }
		n = e - pe;
		pe [g = n -  1].bGrowable = False;
	}
	else
	{
		return;
	}

	for (e = pe, i = 0, micz = 0, P = 0, W = 0; i < n; i ++, e ++)
	{
	    if (e ->bGrowable)
	    {
		micz += (e ->czWMin + e ->czT);
		W    += (e ->czW	     );
		P    += (e ->czW    + e ->czT);
	    }
	}

	    * pz = gabs (* pz);

	if ((P - * pz) < micz)
	{
	    * pz = P   - micz;
	}

	if (0 == * pz)
	{
	    return;
	}

	calcLayout (n, pe, W  - * pz);

	if (ChildOfVer ())
	{
	    pe [g].czW += * pz;

	    z = pe ->pPane ->m_PRect.x;
	}
	else
	{
	    pe [g].czW += * pz;

	    z = pe ->pPane ->m_PRect.y;
	}

	for (e = pe, i = 0; i < n; i ++, e ++)
	{
	     p = e ->pPane;

	    if (ChildOfVer ())
	    {
		p ->m_PRect.x  = z;
		z	      +=
	       (p ->m_PRect.cx = p ->m_lxWOff + e ->czW + p ->m_hxWOff);

		p ->m_cxWUsr   = p ->WRectSizeX ();
	    }
	    else
	    {
		p ->m_PRect.y  = z;
		z	      +=
	       (p ->m_PRect.cy = p ->m_lyWOff + e ->czW + p ->m_hyWOff);

		p ->m_cyWUsr   = p ->WRectSizeY ();
	    }

//	    if (NULL == p ->m_pOwner)
	    {
		p ->UpdateLayout ();
	    }
	}
    }
};








#define USE_DEFERWINDOPOS

#ifdef  USE_DEFERWINDOPOS

void GDockPane::UpdateContext ()
{
    GWindow   *	     pOwner   ;
    HDWP	     pContext ;

    GDockPane *	     p ;
    GRect	     pr;
    GRect	     wr;
    GRect	     wo;

		    pContext = NULL;

    if  (m_dFlags & DPF_UPDATED)
    {
    for (p = m_SList.last (); p; p = m_SList.getprev (p))
    {
	if  (p ->m_dFlags & DPF_UPDATED)
	{
	    if (NULL != (pOwner = (GWindow *) p ->m_pOwner))
	    {
		p ->PRect    (pr);
		p ->PToOwner (pr.Pos ());
		p ->WRect    (wr);
		    wr.Move  (pr.Pos ());

		if (pContext == NULL)
		{
		    printf  ("\t********BegDeferWindowPos -----------------------\n");

		    pContext = ::BeginDeferWindowPos (DPS_MAX);
		}
		if (pContext)
		{

		    pOwner ->GetWindowRect (& wo);

		    printf ("\t********");
		    printf ("Defer  %08X {%4d.%4d}{%4d.%4d}->{%4d.%4d}{%4d.%4d}\n", pOwner ->GetHWND (),
										    wo.x, wo.y, wo.cx, wo.cy,
										    wr.x, wr.y, wr.cx, wr.cy);

		    pContext = ::DeferWindowPos ((HDWP) pContext, pOwner ->GetHWND (), NULL,
						 wr.x, wr.y, wr.cx, wr.cy, 
						 SWP_NOZORDER
					       | SWP_NOACTIVATE
//					       | SWP_NOCOPYBITS
//					       | SWP_NOREDRAW
					       | 0	       );
		}
	    }
	}
    }
	m_dFlags &= (~DPF_UPDATED);
    }

    if (pContext)
    {
	printf  ("\n\t********EndDeferWindowPos <<---------------------\n\n");

	::EndDeferWindowPos ((HDWP) pContext);

	printf  ("\n\t********EndDeferWindowPos >>---------------------\n\n");

	pContext = NULL;
    }
};





















#else

void * GDockPane::UpdateContext (void * pContext)
{
    GDockPane *	     p ;
    GRect	     pr;
    GRect	     wr;
    GRect	     wo;

    if (True/*m_dFlags & DPF_UPDATED*/)
    {
	if (/*! IsRoot () &&*/ m_pOwner)
	{
	    PRect    (pr);
	    PToOwner (pr.Pos ());
	    WRect    (wr);
	    wr.Move  (pr.Pos ());

	    if (pContext == NULL)
	    {
		pContext = & p/*::BeginDeferWindowPos (DPS_MAX)*/;
	    }
	    if (pContext)
	    {
		((GWindow *) m_pOwner) ->GetWindowRect (& wo);

		if  (wo != wr)
		{
//    		((GWindow *) GetOwner ()) ->InvalidateRect (& pr, False);

		    if  (wo.Size () == wr.Size ())
		    {
			printf ("\t********");
			printf ("Defer %08X ->{%4d.%4d}{ No . No }\n", ((GWindow *) m_pOwner) ->GetHWND (), wr.x, wr.y);

				   ::SetWindowPos   (((GWindow *) m_pOwner) ->GetHWND (), NULL,
						     wr.x, wr.y, 0, 0, SWP_NOZORDER | SWP_NOSIZE);
/*
			pContext = ::DeferWindowPos ((HDWP) pContext, ((GWindow *) m_pOwner) ->GetHWND (), NULL,
						     wr.x, wr.y, 0, 0, SWP_NOZORDER | SWP_NOSIZE);
*/
		    }
		    else
		    {
			printf ("\t********");
			printf ("Defer  %08X {%4d.%4d}{%4d.%4d}->{%4d.%4d}{%4d.%4d}\n", ((GWindow *) m_pOwner) ->GetHWND (),
											 wo.x, wo.y, wo.cx, wo.cy,
											 wr.x, wr.y, wr.cx, wr.cy);
/*
			pContext = ::DeferWindowPos ((HDWP) pContext, ((GWindow *) m_pOwner) ->GetHWND (), NULL,
						     wr.x, wr.y, wr.cx, wr.cy, SWP_NOZORDER);//| SWP_NOCOPYBITS | SWP_NOREDRAW);
*/


				   ::SetWindowPos   (((GWindow *) m_pOwner) ->GetHWND (), NULL,
						     wr.x, wr.y, wr.cx, wr.cy, SWP_NOZORDER);

		      ((GWindow *) m_pOwner) ->GetWindowRect (& wr);

			printf ("\t========");
			printf ("Defer  %08X {%4d.%4d}{%4d.%4d}\n",			((GWindow *) m_pOwner) ->GetHWND (),
											 wr.x, wr.y, wr.cx, wr.cy);
		    }

    //		wr.x = wr.y = 0;
    //		wr.cx /= 2;
    //		wr.cy /= 2;

    //		    ((GWindow *) GetOwner ()) ->InvalidateRect (& pr, False);

    //	        ((GWindow *) GetOwner ()) ->ValidateRect   (& wr);
		}
		else
		{
		    printf ("\t********");
		    printf ("No Def %08X {%4d.%4d}{%4d.%4d}=={%4d.%4d}{%4d.%4d}\n", ((GWindow *) m_pOwner) ->GetHWND (),
										     wo.x, wo.y, wo.cx, wo.cy,
										     wr.x, wr.y, wr.cx, wr.cy);
		}
	    }
	}
	else if (m_pOwner)
	{
//	       ((GWindow *) m_pOwner   ) ->InvalidateRect (NULL, True);
	}

	m_dFlags &= (~DPF_UPDATED);
    }

	return  pContext;
};

void GDockPane::UpdateContext ()
{
    GDockPane *	    p ;
    void *    pContext;

	printf  ("\t********BegDeferWindowPos -----------------------\n");

    for (p = m_SList.last (); p; p = m_SList.getprev (p))
    {
    if  (True/*p ->m_dFlags & DPF_UPDATED*/)
    {
	pContext = p ->UpdateContext (pContext);
    }
    }

    if  (pContext)
    {
	if (m_pOwner)
	{
//+++	    ((GWindow *) m_pOwner   ) ->InvalidateRect (NULL, True);
	}

	printf  ("\n\t********EndDeferWindowPos <<---------------------\n\n");

//	::EndDeferWindowPos ((HDWP) pContext);
    
	printf  ("\n\t********EndDeferWindowPos >>---------------------\n\n");

	pContext = NULL;
    }
    else
    {
	printf  ("\t********NulDeferWindowPos -----------------------\n");
    }
};

#endif














#if FALSE

void * GDockPane::UpdateContext (void * pContext)
{
    GDockPane *	     p ;
    GRect	     pr;
    GRect	     wr;
    GRect	     wo;

    for (p = m_SList.last (); p; p = m_SList.getprev (p))
    {
	if (p ->m_dFlags & DPF_UPDATED)
	{
	    pContext = p ->UpdateContext (pContext);
	}
    }
    if (m_dFlags & DPF_UPDATED)
    {
	if (! IsRoot () && m_pOwner)
	{
	    PRect    (pr);
	    PToOwner (pr.Pos ());
	    WRect    (wr);
	    wr.Move  (pr.Pos ());

	    if (pContext == NULL)
	    {
		pContext = ::BeginDeferWindowPos (DPS_MAX);
	    }
	    if (pContext)
	    {
		((GWindow *) m_pOwner) ->GetWindowRect (& wo);

		if  (wo != wr)
		{
//    		((GWindow *) GetOwner ()) ->InvalidateRect (& pr, False);

		    if  (wo.Size () == wr.Size ())
		    {
			pContext = ::DeferWindowPos ((HDWP) pContext, ((GWindow *) m_pOwner) ->GetHWND (), NULL,
						     wr.x, wr.y, 0, 0, SWP_NOZORDER | SWP_NOSIZE);

			printf ("\t********");
			printf ("Defer %08X ->{%4d.%4d}{ No . No }\n", ((GWindow *) m_pOwner) ->GetHWND (), wr.x, wr.y);
		    }
		    else
		    {
			pContext = ::DeferWindowPos ((HDWP) pContext, ((GWindow *) m_pOwner) ->GetHWND (), NULL,
						     wr.x, wr.y, wr.cx, wr.cy, SWP_NOZORDER);//| SWP_NOCOPYBITS | SWP_NOREDRAW);

			printf ("\t********");
			printf ("Defer  %08X {%4d.%4d}{%4d.%4d}->{%4d.%4d}{%4d.%4d}\n", ((GWindow *) m_pOwner) ->GetHWND (),
											 wo.x, wo.y, wo.cx, wo.cy,
											 wr.x, wr.y, wr.cx, wr.cy);
		    }

    //		wr.x = wr.y = 0;
    //		wr.cx /= 2;
    //		wr.cy /= 2;

    //		    ((GWindow *) GetOwner ()) ->InvalidateRect (& pr, False);

    //	        ((GWindow *) GetOwner ()) ->ValidateRect   (& wr);
		}
		else
		{
		    printf ("\t********");
		    printf ("No Def %08X {%4d.%4d}{%4d.%4d}=={%4d.%4d}{%4d.%4d}\n", ((GWindow *) m_pOwner) ->GetHWND (),
										     wo.x, wo.y, wo.cx, wo.cy,
										     wr.x, wr.y, wr.cx, wr.cy);
		}
	    }
	}
	else if (m_pOwner)
	{
//	       ((GWindow *) m_pOwner   ) ->InvalidateRect (NULL, True);
	}

	m_dFlags &= (~DPF_UPDATED);
    }

	return  pContext;
};

void GDockPane::UpdateContext ()
{
	   void *   pContext;

	printf  ("\t********BegDeferWindowPos -----------------------\n");

    if	  (NULL != (pContext = UpdateContext (NULL)))
    {
	if (m_pOwner)
	{
//+++	    ((GWindow *) m_pOwner   ) ->InvalidateRect (NULL, True);
	}

	printf  ("\n\t********EndDeferWindowPos <<---------------------\n\n");

	::EndDeferWindowPos ((HDWP) pContext);
    
	printf  ("\n\t********EndDeferWindowPos >>---------------------\n\n");

		    pContext = NULL;
    }
    else
    {
	printf  ("\t********NulDeferWindowPos -----------------------\n");
    }
};
#endif











//
//[]------------------------------------------------------------------------[]
//
GDockPane * GDockPane::GetPaneByCursor (LONG px, LONG py) const
{
    GRect   pr ;

    WRect  (pr);

    if (pr.Contains (px, py))
    {
	for (GDockPane * p = m_SList.first (); p; p = m_SList.getnext (p))
	{
	    if (p ->m_PRect.Contains (px, py))
	    {
		return p ->GetPaneByCursor (px, py);
	    }
	}
    }
	return (GDockPane *) this;
};

Bool GDockPane::IsCursorOverDragBar (LONG px, LONG py) const
{
    LONG    d;

    if ((0 == (d = HasDragBar ())) || IsRoot ())
    {
	return False;
    }
    
    if (ChildOfVer ())
    {
	return (((m_PRect.cx - d) <= px) && (px < m_PRect.cx)) ? True : False;
    }
	return (((m_PRect.cy - d) <= py) && (py < m_PRect.cy)) ? True : False;
};

void GDockPane::GetGrowableRect (GRect & gr) const
{
    gr.Assign (0, 0, 0, 0);

    if (IsRoot () || ! IsVisible () || ! IsGrowable ())
    {
	return;
    }
};
//
//[]------------------------------------------------------------------------[]


//[]------------------------------------------------------------------------[]
//
class GParamBox : public GControl
{
DECLARE_SYS_WINDOW_CLASS (_T("LISTBOX"));

public :
		GParamBox   ();

	Bool		InsertItem	    (int nItem, LPCTSTR pItemName    , 
							void *  pItemContext );

	Bool		AppendItem	    (		LPCTSTR pItemName    ,
							void *  pItemContext )
			{
			    return InsertItem (-1, pItemName, pItemContext);
			};

	Bool		SetItemHeight	    (int nItem, int nHeight);

protected :

	Bool		On_Create		    (void *);

	void		On_Destroy		    ();

	Bool		DispatchWinMsg		    (GWinMsg &);

	Bool		On_CtrlNotify_MeasureItem   (GWinMsg &);

	Bool		On_CtrlNotify_DrawItem	    (GWinMsg &);

protected :
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
DEFINE_SYS_WINDOW_CLASS (GParamBox);

GParamBox::GParamBox () : GControl (NULL)
{
};

Bool GParamBox::InsertItem (int nItem, LPCTSTR pItemName, void * pItemContext)
{
    LRESULT lResult = 

    ::SendMessage (GetHWND (), LB_INSERTSTRING, (WPARAM) nItem  , (LPARAM) pItemName);

    if ((LB_ERR == lResult) || (LB_ERRSPACE == lResult))
    {	return False; }

    ::SendMessage (GetHWND (), LB_SETITEMDATA , (WPARAM) lResult, (LPARAM) pItemName);

	return True ;
};

Bool GParamBox::SetItemHeight (int nItem, int nHeight)
{
    LRESULT lResult =

    ::SendMessage (GetHWND (), LB_SETITEMHEIGHT, (WPARAM) nItem, (LPARAM) nHeight);

    if ((LB_ERR == lResult) || (LB_ERRSPACE == lResult))
    {	return False; }

	return True ;
};

Bool GParamBox::On_Create (void * pParam)
{
    if (! GControl::On_Create (pParam))
    {	return False; }


    for (int i = 0; i < 2; i ++)
    {
	AppendItem    (_T(""), NULL);

	SetItemHeight (i, 64); 
    }


	return True;
};

void GParamBox::On_Destroy ()
{
    GControl::On_Destroy ();
};

Bool GParamBox::On_CtrlNotify_MeasureItem (GWinMsg & m)
{
    return m.DispatchDefault ();
};

Bool GParamBox::On_CtrlNotify_DrawItem (GWinMsg & m)
{
    printf ("\t\t\tGParentBox::On_CtrlNotify_DrawItem\n");

//  return m.DispatchComplete (TRUE);

    return m.DispatchDefault ();
};

Bool GParamBox::DispatchWinMsg (GWinMsg & m)
{
/*
    switch (m.uType)
    {
    case GWinMsg::CtrlNotify :

//	DISPATCH_WM	(WM_MEASUREITEM		    , On_CtrlNotify_MeasureItem);
	DISPATCH_WM	(WM_DRAWITEM		    , On_CtrlNotify_DrawItem);

	return GControl::DispatchWinMsg (m);
    };
*/
	return GControl::DispatchWinMsg (m);
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
class TestClient : public GParentWindow
{
DECLARE_EXT_WINDOW_CLASS (_T("GWin.TestClient"), CS_DBLCLKS);

public :
		TestClient    (const void * pWndId) : 

		GParentWindow (pWndId) 
		{};

	       ~TestClient () {printf ("::~TestClient:%08X\n", this);};

	Bool		Create		    (GWindow *	pOwner	      ,
					     LPCTSTR    pTitle	      ,
					     DWord      dStyle	      ,
					     DWord      dExStyle      ,
					     int	x       = 0   ,
					     int	y       = 0   ,
					     int	cx      = 0   ,
					     int	cy      = 0   ,
					     void *	pParam  = NULL)
			{
			    return GParentWindow::Create (pOwner, pTitle   ,
							  dStyle | WS_CHILD, dExStyle,
							  x, y, cx, cy,	     pParam  );
			};
protected :

	Bool		On_Create	    (void *)
			{
			    printf ("TestClient::On_Create\n");

			    return True;
			};

	void		On_Destroy	    ()
			{
			    printf ("TestClient::On_Destroy\n");
			};

protected :
	Bool		DispatchWinMsg	    (GWinMsg &);

	Bool		On_WM_PaintBkGnd    (GWinMsg &);

//	Bool		On_WM_EraseBkGnd    (GWinMsg &);
	Bool		On_WM_Paint	    (GWinMsg &);
};
DEFINE_EXT_WINDOW_CLASS (TestClient);

Bool TestClient::DispatchWinMsg (GWinMsg & m)
{
    switch (m.uType)
    {
    case GWinMsg::WndProc :

	DISPATCH_WM	(WM_ERASEBKGND,		On_WM_PaintBkGnd	)
	DISPATCH_WM	(WM_PAINT,		On_WM_Paint		)

	return GParentWindow::DispatchWinMsg (m);
    };
	return GParentWindow::DispatchWinMsg (m);
};

Bool TestClient::On_WM_PaintBkGnd (GWinMsg & m)
{
    GRect	     rc;

    GetClientRect (& rc);

    FillRect ((HDC) m.wParam, & rc, (HBRUSH) ::GetStockObject (LTGRAY_BRUSH));

    return m.DispatchComplete (True);
};

Bool TestClient::On_WM_Paint (GWinMsg & m)
{
	PAINTSTRUCT	 ps;
	GRect		 rc;

    ::BeginPaint (GetHWND (), & ps);

	GetClientRect (& rc);

	for (; (0 < rc.cx) && (0 < rc.cy);)
	{
	    FrameRectEx (ps.hdc, & rc, (HBRUSH) ::GetStockObject (BLACK_BRUSH), 1, PATCOPY);

	    rc.Move ( 4,  4);
	    rc.Grow (-8, -8);
	}

    ::EndPaint   (GetHWND (), & ps);

    return m.DispatchComplete ();
};
//
//[]------------------------------------------------------------------------[]










//[]------------------------------------------------------------------------[]
//
class GDataItem
{
public :
	    GDataItem (DWord dValue = 0) : m_dValue (dValue) {};

	    GDataItem (LONG  lValue    ) : m_lValue (lValue) {};

	void		SetValue	(DWord dValue, Bool bForceUpdate = False);

protected :

	union
	{
	    DWORD		m_dValue ;
	    LONG		m_lValue ;
	    int			m_iValue ;
	    unsigned short	m_hValue ;
	    short		m_sValue ;
	};
};

void GDataItem::SetValue (DWord dValue, Bool bForceUpdate)
{
    DWord   dOldVal  =  m_dValue;

    if (   (dOldVal != (m_dValue = dValue)) || bForceUpdate)
    {
//	OnDataUpdate   (bForceUpdate);
    }
};

class GBitsField : public GDataItem
{
public :
	    GBitsField (DWord dValue = 0) : GDataItem (dValue) {};

	void		SetBits		(DWord dClrMask, 
					 DWord dSetMask, Bool bForceUpdate = False);

	void		XorBits		(DWord dXorMask, Bool bForceUpdate = False)
			{
			    SetBits	(m_dValue & dXorMask, m_dValue ^ dXorMask);
			};
protected :

	DWord			m_dValue ;
};

void GBitsField::SetBits (DWord dClrMask, DWord dSetMask, Bool bForceUpdate)
{
    DWord   dValue  =  m_dValue;

    if (   (dValue != (m_dValue = (m_dValue & (~ dClrMask)) | dSetMask)) || bForceUpdate)
    {
//	OnDataUpdate  (bForceUpdate)
    }
};


















class CControlTitle : public GDialog
{
public :

	    CControlTitle (const void * pWndId);

	   ~CControlTitle ();

	Bool		Create		(GWindow *  pParent	,
					 HMODULE    hResModule	,
					 LPCTSTR    pText	);

protected :

	Bool		On_Create		(void *);

	void		On_Destroy		();

	Bool		On_DrawTitle		(GWinMsg &, DRAWITEMSTRUCT *);
/*
	void		On_UpdateState		(DWord dSyncMask,
						 DWord dSateMask,
						 DWord dSate	);
*/
/*
	Bool		OnCreate		();

	void		OnDestroy		();
*/


	void		GUIUpdate		(DWord dMask = 0xFFFFFFFF);

	void		OnSelect		(Bool);

	void		OnDropDown		(Bool);



	Bool		DispatchWinMsg		(GWinMsg &);

protected :

	DWord			m_dState    ;
	TCHAR			m_Text [128];
};

CControlTitle::CControlTitle (const void * pWndId) : GDialog (pWndId)
{
    m_dState = 0x00 ;
  * m_Text   = 0    ;
};

CControlTitle::~CControlTitle ()
{};

Bool CControlTitle::Create (GWindow * pParent	 ,
			    HMODULE   hResModule ,
			    LPCTSTR   pText	 )
{
    if (! GDialog::Create (pParent, hResModule, MAKEINTRESOURCE (IDD_CONTROL_TITLE)))
    {	return False;}

//  On_UpdateState (0x00, 0x03, 0x01);

    strcpy (m_Text, pText);

      	return True ;
};

Bool CControlTitle::On_Create (void * pParam)
{
    if (! GDialog::On_Create (pParam))
    {	return False;}

	GUIUpdate ();

	return True ;
};

void CControlTitle::On_Destroy ()
{
    GDialog::On_Destroy ();
};

Bool CControlTitle::On_DrawTitle (GWinMsg & m, DRAWITEMSTRUCT * pdi)
{
//  FillRect (pdi ->hDC, & pdi ->rcItem, (HBRUSH) ::GetStockObject (BLACK_BRUSH));    

    return m.DispatchComplete (True);
};

/*
void CControlTitle::On_UpdateState (DWord dSyncMask, DWord dStateMask, DWord dState)
{
    if (dStateMask)
    {
	m_dState &= (~dStateMask);
	m_dState |= ( dState	);

    if (m_dState    ==   0x01)
    {
	m_dState    &= (~0x01);
	  dSyncMask &= (~0x01);
    }
	EnableCtrl     (IDC_DROP_CHK	, (m_dState & 0x02) ? True : False);
    }

    if (! (dSyncMask & 0x02))
    {
	CheckCtrl      (IDC_SELECT_CHK	, (m_dState & 0x02) ? True : False);
    }
    if (! (dSyncMask & 0x01))
    {
	CheckCtrl      (IDC_DROP_CHK	, (m_dState & 0x01) ? True : False);
    }
};
*/

void CControlTitle::GUIUpdate (DWord dMask)
{
    if (0x01 & dMask)
    {
    EnableCtrl  (IDC_DROP_CHK  , (m_dState & 0x02) ? True : False);
    CheckCtrl   (IDC_SELECT_CHK, (m_dState & 0x02) ? True : False);
    CheckCtrl   (IDC_DROP_CHK  , (m_dState & 0x01) ? True : False);
    SetCtrlText (IDC_DROP_CHK  , (m_dState & 0x01) ? "Hide" : "Show");
    }
};

void CControlTitle::OnSelect (Bool bSelect)
{
	m_dState = ((m_dState & (~0x02)) | ((bSelect) ? 0x02 : 0x00));

    if (0x01 == (m_dState & 0x03))
    {
	OnDropDown (False);
    }
	GUIUpdate  (0x01);
};

void CControlTitle::OnDropDown (Bool bShow)
{
	  bShow	       = (m_dState & 0x02) ? bShow : False;

    if (  bShow && (0 == (m_dState & 0x01)))
    {
printf ("\t\t\t\tShow Window...\n");

	m_dState |=   0x01;

	GUIUpdate (0x01);
    }
    else
    if (! bShow && (0 != (m_dState & 0x01)))
    {
printf ("\t\t\t\tHide window...\n");

	m_dState &= (~0x01);
    }
	GUIUpdate (0x01);
};

Bool CControlTitle::DispatchWinMsg (GWinMsg & m)
{
    switch (m.uType)
    {
    case GWinMsg::WndProc :

	if ((WM_NOTIFY	      == m.uMsg)
	&&  (NM_CUSTOMDRAW    == ((NMHDR *) m.lParam) ->code))
	{}

	if ((WM_COMMAND	      == m.uMsg))
	{
	    if (IS_WM_COMMAND_ID_CODE (IDC_SELECT_CHK, BN_CLICKED))
	    {
		OnSelect   (((m_dState & 0x02) ^ 0x02) ? True : False);
	    }
	    else
	    if (IS_WM_COMMAND_ID_CODE (IDC_DROP_CHK  , BN_CLICKED))
	    {
		OnDropDown (((m_dState & 0x01) ^ 0x01) ? True : False);
	    }
	}

	if ((WM_CTLCOLORDLG    ==  m.uMsg)
	||  (WM_CTLCOLORSTATIC ==  m.uMsg))
	{
//	    ::SelectObject ((HDC) m.wParam, (HBRUSH) ::GetStockObject (BLACK_BRUSH));

//	return m.DispatchComplete ((LRESULT) (HBRUSH) ::GetStockObject (WHITE_BRUSH));
	}
/*
	if ((WM_DRAWITEM      ==       m.uMsg  )
	&&  (IDC_BUTTON_TITLE == (int) m.wParam))
	{
	return On_DrawTitle (m, (DRAWITEMSTRUCT *) m.lParam);
	}
	return GDialog::DispatchWinMsg (m);
*/
	if ((NM_CUSTOMDRAW    ==       m.uMsg  ))
	{
	return GDialog::DispatchWinMsg (m);
	}
	return GDialog::DispatchWinMsg (m);



    case GWinMsg::DlgProc :
/*
	if ((WM_CTLCOLORDLG   ==       m.uMsg)
	||  (WM_CTLCOLORBTN   ==       m.uMsg))
	{
//	return m.DispatchCompleteDlgEx ((LRESULT) (HBRUSH) ::GetStockObject (BLACK_BRUSH));
	}

	if ((NM_CUSTOMDRAW    ==       m.uMsg))
	{
	return GDialog::DispatchWinMsg (m);
	}
*/
	return GDialog::DispatchWinMsg (m);
    }
	return GDialog::DispatchWinMsg (m);
};

/*
template <class M, class F> class TControlPage : public TDockingWindow <CControlPage>
{
public :

	    TControlPage (const void * pWndId) :

		m_MiniBar (NULL),
		m_FullBar (NULL)

	    {}

	Bool		Create		(GWindow *  pParent	,
					 HMODULE    hResModule	,
					 LPCTSTR    pResMiniBar ,
					 LPCTSTR    pResFullBar )
	{
	    if (! Window ().::Create (pParent, hResModule, CControl
	};
protected :

	TDockingWindow <M>		m_MiniBar ;
	TDockingWindow <F>		m_FullBar ;
};
*/
//
//[]------------------------------------------------------------------------[]
//















class CMainControl : public GParent
{
public :
		CMainControl ();

	       ~CMainControl ();

	Bool	    Create		    (GWindow * pParent)
		    {
			return GDockParent::Create (pParent, NULL,
						    WS_VISIBLE | WS_CHILD, 0,
						    0, 0, 0, 0, NULL);
		    };

protected :

	Bool	    On_Create		    (void *);

	void	    On_Destroy		    ();

private :

	TDockingWindow <CControlTitle>  m_KkmTitle  ;
	TDockingWindow <CControlTitle>	m_UrpTitle  ;

	TDockingWindow <TestClient>	m_Test	    ;
};

CMainControl::CMainControl () : GDockParent (NULL),

    m_KkmTitle	(NULL),
    m_UrpTitle  (NULL),

    m_Test	(NULL)
{};

CMainControl::~CMainControl ()
{
};

Bool CMainControl::On_Create (void * pParam)
{
    if (! GParentWindow::On_Create (pParam))
    {	return False;}

    GRect   wr;

//.......
//
    if (! m_KkmTitle.Window ().Create (this, NULL, "Kkm control"))
    {	return False;}

    m_KkmTitle.Window ().GetWindowRect (& wr);

    RootPane ().InsertCtrlBar (& m_KkmTitle, NULL,
			       & m_KkmTitle.Window (),
				 LayoutParam (10, 10, 10, 2),
				 SizeParam   (wr.cy, wr.cx));

	return True;
//......
//
    if (! m_UrpTitle.Window ().Create (this, NULL, "Urp control"))
    {	return False;}

    m_UrpTitle.Window ().GetWindowRect (& wr);

    RootPane ().InsertCtrlBar (& m_UrpTitle, NULL,
			       & m_UrpTitle.Window (),
				 LayoutParam (10, 10, 2, 10),
				 SizeParam   (wr.cy, wr.cx));

//.......
//

    m_Test.Window ().Create (this, NULL,
		   WS_VISIBLE | WS_CHILD, 0,
		   0, 0, 0, 0, NULL);

    RootPane ().Insert	    (& m_Test, NULL,
			     & m_Test.Window (),
			       SizeParam (0, 0),
			       LayoutParam (10, 10, 10, 10),
//			       LayoutParam (0, 0, 0, 0),
			       SizeParam (100, 100));
//.......
//

    if (HasFlag  (GWF_NEEDUPDATE))
    {
	m_RootPane.UpdateContext  ();

	SetFlags (GWF_NEEDUPDATE, 0);
    }

	return True;
};

void CMainControl::On_Destroy ()
{
    GDockParent::On_Destroy ();
};
//
//[]------------------------------------------------------------------------[]
//


//[]------------------------------------------------------------------------[]
//
class MainDialog : public GDialog
{
public :
	    MainDialog	() : GDialog (NULL), m_TestClient (NULL) 
	    {};

	    MainDialog	(const void * pWndId) : GDialog (pWndId), m_TestClient (NULL) 
	    {};

	   ~MainDialog	() { printf ("::~MainDialog:%08X\n", this);};

protected :

	GWindow *	GetCreationWindow   (int nCtrlId)
			{
			    if (IDC_TESTCLIENT == nCtrlId)
			    {
				return & m_TestClient;
			    }
			    else
			    if (False)
			    {}
				return NULL;
			};
protected :

	Bool		On_Create	    (void *);

	void		On_Destroy	    ();

	Bool		DispatchWinMsg	    (GWinMsg &);

	Bool		On_DM_CtlColorDlg   (GWinMsg &);
	Bool		On_DM_CtlColorBtn   (GWinMsg &);

	Bool		On_DCommand_Ok	    (GWinMsg &);
	Bool		On_DCommand_Cancel  (GWinMsg &);


	Bool		On_WM_MeasureItem   (GWinMsg &);
	Bool		On_WM_DrawItem	    (GWinMsg &);

protected :

	TestClient		m_TestClient	;
	GParamBox		m_ParamBox	;
};

Bool MainDialog::On_Create (void * pParam)
{
    if (! GDialog::On_Create (pParam))
    {	return False;}

    if (! m_ParamBox.Create (this, GetCtrlHWND (IDC_PARAMBOX), pParam))
    {	return False;}


	return True;
};

void MainDialog::On_Destroy ()
{
    GDialog::On_Destroy ();
};

Bool MainDialog::On_WM_MeasureItem (GWinMsg & m)
{
    return m.DispatchDefault ();
};

Bool MainDialog::On_WM_DrawItem (GWinMsg & m)
{
    printf ("DlgProc::WM_DRAWITEM\n");

    return m.DispatchDefault ();
};

Bool MainDialog::DispatchWinMsg (GWinMsg & m)
{
    switch (m.uType)
    {
    case GWinMsg::WndProc :

	if (WM_COMMAND == m.uMsg)
	{
//	    printf ("WndProc::WM_COMMAND\n");

//	    DISPATCH_WM_COMMAND_ID  (IDOK	    , On_ID_Ok		    )
//	    DISPATCH_WM_COMMAND_ID  (IDCANCEL	    , On_ID_Cancel	    )

	    return GDialog::DispatchWinMsg (m);
	}
	else
	if (WM_NOTIFY  == m.uMsg)
	{
//	    printf ("WndProc::WM_NOTIFY\n");
	}
//	    DISPATCH_WM		    (WM_MEASUREITEM , On_WM_MeasureItem	    )
//	    DISPATCH_WM		    (WM_DRAWITEM    , On_WM_DrawItem	    )

	    return GDialog::DispatchWinMsg (m);

    case GWinMsg::DlgProc :

	if (WM_COMMAND == m.uMsg)
	{
	    printf ("DlgProc::WM_COMMAND %04Xh [%04Xh = %5d]\n", HIWORD (m.wParam),
								 LOWORD (m.wParam),
								 LOWORD (m.wParam));

	    DISPATCH_WM_COMMAND_ID  (IDOK	    , On_DCommand_Ok	    )
	    DISPATCH_WM_COMMAND_ID  (IDCANCEL	    , On_DCommand_Ok	    )
	    DISPATCH_WM_COMMAND_ID  (IDNO	    , On_DCommand_Cancel    )

	    return GDialog::DispatchWinMsg (m);
	}
	else
	if (WM_NOTIFY  == m.uMsg)
	{
//	    DISPATCH_WM_NOTIFY_CODE (...	    , On_WM_Command_Ok	    )
//	    DISPATCH_WM_NOTIFY_CODE (...	    , On_WM_Command_Cancel  )

	    printf ("DlgProc::WM_NOTIFY  ..... [%04Xh = %5d] %04Xh = %05d\n", ((NMHDR *) m.lParam) ->idFrom,
									      ((NMHDR *) m.lParam) ->idFrom,
									      ((NMHDR *) m.lParam) ->code  ,
									      ((NMHDR *) m.lParam) ->code  );
	}
	else
	if (WM_HSCROLL == m.uMsg)
	{
	    printf ("DlgProc::WM_HSCROLL .....\n");
	}
	else
	if (WM_VSCROLL == m.uMsg)
	{
	    printf ("DlgProc::WM_HSCROLL .....\n");
	}


	    DISPATCH_WM		    (WM_CTLCOLORDLG , On_DM_CtlColorDlg	    )
	    DISPATCH_WM		    (WM_CTLCOLORBTN , On_DM_CtlColorBtn	    )

	    return GDialog::DispatchWinMsg (m);
    };
	    return GDialog::DispatchWinMsg (m);
};

Bool MainDialog::On_DM_CtlColorDlg (GWinMsg & m)
{
/*
    if (GetHWND () == (HWND) m.lParam)
    {
	return m.DispatchCompleteDlgEx ((LRESULT) ::GetStockObject (BLACK_BRUSH));
    }
    else
*/
    {
	return m.DispatchDefault  ();
    }
};

Bool MainDialog::On_DM_CtlColorBtn (GWinMsg & m)
{
/*
    if (IDOK == GetCtrlId ((HWND) m.lParam))
    {
	return m.DispatchCompleteDlgEx ((LRESULT) ::GetStockObject (WHITE_BRUSH));
    }
    else
*/
    {
	return m.DispatchDefault  ();
    }
};


static void GAPI _break (void *, void * Command)
{
    ::PostQuitMessage ((int) Command);
};


Bool MainDialog::On_DCommand_Ok (GWinMsg & m)
{
#if FALSE


#else

    if (! HasStyle (WS_CHILD))
    {
#if TRUE
	::PostQuitMessage (LOWORD (m.wParam));
//	GWinMsgLoop::Exit (0);
#else

	GWinMsgLoop::SetPostProc (_break, NULL, (void *) IDOK);

#endif
    }
    ::MessageBeep (MB_ICONASTERISK);

    return m.DispatchComplete ();

#endif
};

Bool MainDialog::On_DCommand_Cancel (GWinMsg & m)
{
//  ::MessageBeep (MB_ICONHAND);

    return m.DispatchComplete ();
};
//
//[]------------------------------------------------------------------------[]
//
class MainFrame : public GFrameControl
{
public :
		MainFrame () : GFrameControl (NULL),

		m_ControlBar	(NULL),
		m_MenuBar	(NULL),
		m_ToolBarMenu	(NULL),
		m_ToolBar	(NULL),

		m_TreeView	(NULL),
		m_StatusBar	(NULL),
		m_TestClient	(NULL),
		m_TestDialog	(NULL)
		{
		    m_1STimer = 0;
		};

	       ~MainFrame () { printf ("MainFrame::~MainFrame %08X\n", this);};
protected :

	Bool	    DispatchWinMsg	    (GWinMsg &);


	void	    On_Destroy		    ();
	Bool	    On_Create		    (void *);

	Bool	    On_WM_Notify	    (GWinMsg &);

	Bool	    On_WM_SysKeyDown	    (GWinMsg &);
	Bool	    On_WM_KeyDown	    (GWinMsg &);

protected :

	    TDockingWindow <GReBar>
				m_ControlBar	;

	    TDockingWindow <GToolBarMenu>
				m_MenuBar	;

	    GToolBarMenu	m_ToolBarMenu	;
	    GToolBar		m_ToolBar	;

	    GDockPane		m_TreeViewPane	;
	    GDockPane		m_TreeViewPane2 ;
	    GTreeView		m_TreeView	;

	    TestClient		m_TestClient	;

	    TDockingWindow <MainDialog>
				m_TestDialog	;

//	    GDockPane		m_StatusBarPane	;
//	    GStatusBar		m_StatusBar	;

	    UINT_PTR		m_1STimer	;

	    TDockingWindow <GStatusBar>	    
				m_StatusBar	;
};

Bool MainFrame::On_WM_Notify (GWinMsg & m)
{
    if (WM_NOTIFY == m.uMsg)
    {
    if (((NMHDR *) m.lParam) ->hwndFrom == m_ControlBar.Window ().GetHWND ())
    {
    if (((NMHDR *) m.lParam) ->code	== RBN_HEIGHTCHANGE		 )
    {{

	GRect	rw;

	m_ControlBar.Window ().GetWindowRect (& rw);
/*
	RootPane ().m_SList.extract (& m_ToolBar);

	RootPane ().InsertCtrlBar (& m_ToolBar, & m_TreeViewPane,
				   & m_ToolBar.Window (),
				     LayoutParam (5, 5, 5, 5),
				     SizeParam   (rw.cy,   0));
*/
	rw.cy -= m_ControlBar.WRectSizeY ();

	m_ControlBar.Grow (0, rw.cy);

	if (HasFlag  (GWF_NEEDUPDATE))
	{
    printf ("Update layouts <<\n\n");

	    m_RootPane.UpdateContext  ();

	    SetFlags (GWF_NEEDUPDATE, 0);

    printf ("Update layouts >>\n\n");

	}
    }}
    }
    }
	return GFrameControl::On_WM_Notify (m);
};

Bool MainFrame::On_WM_SysKeyDown (GWinMsg & m)
{
    printf ("SysKeyDown %08X %08X\n", m.wParam, m.lParam);

    return m.DispatchComplete ();
};

Bool MainFrame::On_WM_KeyDown (GWinMsg & m)
{
    printf ("KeyDown %08X %08X\n", m.wParam, m.lParam);

    if (VK_SPACE == m.wParam)
    {
//	::SetFocus (m_ToolBarMenu.GetHWND ());
    }


    return m.DispatchComplete ();
};

Bool MainFrame::DispatchWinMsg (GWinMsg & m)
{
    switch (m.uType)
    {
    case GWinMsg::WndProc :

//	printf ("MainFrame   %08X: %08X\n", m.hWnd ,
//					    m.uMsg );

	DISPATCH_WM	(WM_NOTIFY,		On_WM_Notify	    )
	DISPATCH_WM	(WM_SYSKEYDOWN,		On_WM_SysKeyDown    )
	DISPATCH_WM	(WM_KEYDOWN,		On_WM_KeyDown	    )
	DISPATCH_WM	(WM_ACTIVATE,		On_WM_Activate	    )

    if (WM_WINDOWPOSCHANGED == m.uMsg)
    {
	m_MenuBar.Window ().Send_WM (TB_AUTOSIZE, 0, 0);

	GDockParent::On_WM_PosChanged (m);

    {
	GRect	w;

	m_MenuBar.Window ().GetWindowRect (& w);

	printf ("Menu bar size %d.%d\n", w.cx, w.cy);
    }

	return m.DispatchComplete ();
    }
    if (WM_COMMAND == m.uMsg)
    {
	printf ("MainFrame WM_COMMAND %08X %08X\n", m.wParam, m.lParam);
    }
    if (WM_ENTERIDLE == m.uMsg)
    {
	printf ("MenuEnterIdle %08X ??? %08X\n", m.hWnd, ::GetCapture ());
    }

    if ((WM_COMMAND == m.uMsg) && (MMID_FILE_TEST == m.wParam))
    {{
	MainDialog  DMain;

	if (DMain.Create (this, NULL, MAKEINTRESOURCE (IDD_DIALOG1)))
	{
/*
	    DMain.ExecWindow (...
*/
	    DMain.ExecWindow ();
	};
    }}

	return GFrameControl::DispatchWinMsg (m);
    }
	return GFrameControl::DispatchWinMsg (m);
};

static void Enum (GWindow * pWindow)
{
HWND	hWnd = NULL;

printf ("-> : ");
for (hWnd = pWindow ->GetFirstChild (); hWnd; hWnd = GWindow::GetNextChild (hWnd))
{
printf ("%08X ", hWnd);
};
printf ("\n");

printf ("<- : ");
for (hWnd = pWindow ->GetLastChild (); hWnd; hWnd = GWindow::GetPrevChild (hWnd))
{
printf ("%08X ", hWnd);
};
printf ("\n");

};

void MainFrame::On_Destroy ()
{
    if (m_1STimer)
    {
	::KillTimer (NULL, m_1STimer);
			   m_1STimer = 0;
    }
};


static void CALLBACK _1STimerProc (HWND, UINT, UINT_PTR, DWORD)
{
    static int cPost = 0;

printf ("Post timer message %d\n", ++ cPost);

    ::PostMessage (NULL, 0xC000, cPost, 0x00007777);
};

Bool MainFrame::On_Create (void *)
{
    if (! GFrameControl::On_Create (NULL))
    {	return False;}


//  m_1STimer = ::SetTimer (NULL, 0, 1000, _1STimerProc);

    ::SetMenu (GetHWND (), ::LoadMenu (::GetModuleHandle (NULL), MAKEINTRESOURCE (IDR_MAIN_MENU)));


    m_RootPane.m_dFlags = DPF_VISIBLE;

    if (! m_StatusBar.Window ().Create (this, NULL,
					WS_VISIBLE | CCS_NOPARENTALIGN | CCS_BOTTOM | SBARS_SIZEGRIP, 0))
    {	return False;}

    GRect   wr = {0, 0, 0, 20};

    m_StatusBar.Window ().GetWindowRect (& wr);

    RootPane ().InsertCtrlBar (& m_StatusBar, NULL,
			       & m_StatusBar.Window (),
				 LayoutParam (5, 5, 5, 5),
				 SizeParam   (wr.cy, 0	));

/*
    GRect   rw = {0, 0, 0, 20};

    if (! m_StatusBar.Create (this, NULL,
			      WS_VISIBLE | CCS_NOPARENTALIGN | CCS_BOTTOM | SBARS_SIZEGRIP, 0))
    {	return False;}

    m_StatusBar.GetWindowRect (& rw);

    m_RootPane.InsertCtrlBar (& m_StatusBarPane, NULL	,
			      & m_StatusBar		,
				1			,
				LayoutParam (5, 5, 5, 5),
				SizeParam   (rw.cy, 0));
*/
    if (! m_TreeView.Create (this, NULL,
			     WS_VISIBLE /*| WS_CAPTION*/, 0,
			     0, 0, 0, 0))
    {	return False;}

#if TRUE

    {{
	
    }}    


#endif


    m_RootPane.Insert	     (& m_TreeViewPane, & m_StatusBar,
			      & m_TreeView		,
				SizeParam   (0, 20)	,
				LayoutParam (5, 5, 5, 5),
//				LayoutParam (10, 10, 10, 10),
				SizeParam   (80, 80));

#if TRUE


    m_TreeViewPane.m_dFlags |= 0x00001100;

    if (! m_TestClient.Create (this, _T("Test Client"),
			       WS_VISIBLE /*| WS_CAPTION*/, 0,// WS_EX_TOOLWINDOW,
			       0, 0, 0, 0))
    {	return False;}

    m_RootPane.Insert	     (& m_TreeViewPane2, & m_StatusBar,
			      & m_TestClient		,
				SizeParam   (0, 20)	,
				LayoutParam (5, 5, 5, 5),
//				LayoutParam (10, 10, 10, 10),
				SizeParam   (80, 80));

    if (! m_TestDialog.Window ().Create (this, NULL, MAKEINTRESOURCE (IDD_DIALOG2)))
    {	return False;}

    m_TestDialog.Window ().GetWindowRect (& wr);
/*
    RootPane ().InsertCtrlBar (& m_TestDialog, & m_StatusBar,
			       & m_TestDialog.Window (),
			         LayoutParam (5, 5, 5, 5),
				 SizeParam   (wr.cy, 0  ));
*/
    RootPane ().Insert	      (& m_TestDialog, & m_StatusBar,
			       & m_TestDialog.Window (),
				 SizeParam   (200, wr.cy),
				 LayoutParam (5, 5, 5, 5),
				 SizeParam   (200, wr.cy));


    m_TreeViewPane2.m_dFlags |= 0x00002200;

    m_TestDialog.m_dFlags    |= 0x00002200;

#endif


    if (! m_MenuBar.Window ().Create (this, NULL, MAKEINTRESOURCE (IDR_MAIN_MENU)))
    {	return False;}

    m_MenuBar.Window ().GetWindowRect (& wr);

    RootPane ().InsertCtrlBar (& m_MenuBar, & m_TreeViewPane,
			       & m_MenuBar.Window (),
				 LayoutParam (5, 5, 5, 5),
				 SizeParam   (wr.cy,   0));

    
	if (HasFlag  (GWF_NEEDUPDATE))
	{
	    m_RootPane.UpdateContext  ();

	    SetFlags (GWF_NEEDUPDATE, 0);
	}



    m_MenuBar.Window ().GetWindowRect (& wr);

    printf ("Menu bar size : (%08X) = %d.%d\n", m_MenuBar.Window ().GetHWND (), wr.cx, wr.cy);




	return True ;



#if TRUE
    {

    if (! m_ControlBar.Window ().Create (this, NULL,
					 WS_VISIBLE | CCS_NOPARENTALIGN | CCS_TOP | CCS_NODIVIDER | RBS_BANDBORDERS | RBS_VARHEIGHT | RBS_AUTOSIZE, 0,
					 0, 0, 0, 0))
    {	return False;}


    if (! m_ToolBarMenu.Create (& m_ControlBar.Window (), NULL, MAKEINTRESOURCE (IDR_MAIN_MENU)))
    {	return False;}

#if TRUE

    if (! m_ToolBar.Create (& m_ControlBar.Window (), NULL,
			    WS_VISIBLE | CCS_NOPARENTALIGN | CCS_TOP | CCS_NODIVIDER | TBSTYLE_FLAT | TBSTYLE_LIST, 0,
			    0, 0, 0, 0))
    {	return False;}


	TBBUTTON bt [6] = {{STD_FILENEW,  5000, TBSTATE_ENABLED, BTNS_BUTTON | BTNS_DROPDOWN | BTNS_AUTOSIZE, {0,0}, 0, (INT_PTR) "Button(&New)"},
			   {STD_FILEOPEN, 5001, TBSTATE_ENABLED, BTNS_BUTTON | BTNS_DROPDOWN | BTNS_AUTOSIZE, {0,0}, 0, (INT_PTR) "Button(&Open)"},
			   {STD_FILESAVE, 5002, TBSTATE_ENABLED, BTNS_BUTTON | BTNS_DROPDOWN | BTNS_AUTOSIZE, {0,0}, 0, (INT_PTR) "Button(&Save)"},
			   {I_IMAGENONE,  0   , TBSTATE_ENABLED, BTNS_SEP, {0,0}, 0, (INT_PTR) 0},
			   {STD_HELP,	  5003, TBSTATE_ENABLED, BTNS_BUTTON | BTNS_DROPDOWN | BTNS_AUTOSIZE, {0,0}, 0, (INT_PTR) "Button(&Help)"},
			   {I_IMAGENONE,  0   , TBSTATE_ENABLED, BTNS_SEP, {0,0}, 0, (INT_PTR) 0}};


#if FALSE

	HIMAGELIST  il = ImageList_Create (16, 16, ILC_COLORDDB | ILC_MASK, 0, 16);

	m_ToolBar.Send_WM (TB_SETIMAGELIST, (WPARAM) 0, (LPARAM) il);
	m_ToolBar.Send_WM (TB_LOADIMAGES, (WPARAM) IDB_STD_SMALL_COLOR, (LPARAM) HINST_COMMCTRL);
#else

	HIMAGELIST  il = ImageList_Create (24, 24, ILC_COLORDDB | ILC_MASK, 0, 16);

	m_ToolBar.Send_WM (TB_SETIMAGELIST, (WPARAM) 0, (LPARAM) il);
	m_ToolBar.Send_WM (TB_LOADIMAGES, (WPARAM) IDB_STD_LARGE_COLOR, (LPARAM) HINST_COMMCTRL);
#endif

	m_ToolBar.Send_WM (TB_ADDBUTTONS, (WPARAM) 6, (LPARAM) & bt);
	m_ToolBar.Send_WM (TB_AUTOSIZE  , (WPARAM) 0, (LPARAM) 0);

//	m_ToolBar.SetStyle (CCS_NORESIZE | m_ToolBar.GetStyle ());

#endif

    REBARBANDINFO   rbi;

    memset (& rbi, 0, sizeof (rbi));
    rbi.cbSize	    = sizeof (rbi) ;

    rbi.fMask	    = RBBIM_STYLE | RBBIM_CHILD | RBBIM_COLORS;
    rbi.fMask	   |= (/*RBBIM_HEADERSIZE |*/ RBBIM_SIZE | RBBIM_CHILDSIZE | RBBIM_IDEALSIZE/*| RBBIM_TEXT*/);

    rbi.fStyle	    = RBBS_BREAK | RBBS_GRIPPERALWAYS | RBBS_USECHEVRON | RBBS_CHILDEDGE | RBBS_VARIABLEHEIGHT /*| RBBS_FIXEDSIZE*/;

    rbi.clrFore	    = GetSysColor (COLOR_MENUTEXT);
    rbi.clrBack	    = GetSysColor (COLOR_WINDOW);
    rbi.hwndChild   = m_ToolBarMenu.GetHWND ();
    rbi.lpText	    = _T("Menu Bar:");

    m_ToolBarMenu.GetWindowRect (& wr);

    rbi.cxHeader    = 80;

    rbi.cxMinChild  = wr.cx;
    rbi.cx	    =
    rbi.cxIdeal	    = wr.cx;

    rbi.cyChild	    =
    rbi.cyMinChild  = wr.cy - 4;
    rbi.cyMaxChild  = 128;//wr.cy - 4;

    m_ControlBar.Window ().Send_WM (RB_INSERTBAND, (WPARAM) -1, (LPARAM) & rbi);
//
//  -------------------------------
//
    rbi.fMask	   |= RBBIM_HEADERSIZE;
    rbi.fStyle	    = RBBS_BREAK | RBBS_GRIPPERALWAYS | RBBS_USECHEVRON | RBBS_CHILDEDGE | RBBS_VARIABLEHEIGHT /*| RBBS_FIXEDSIZE*/;

    rbi.hwndChild   = m_ToolBar.GetHWND ();

    m_ToolBar.GetWindowRect (& wr);

    rbi.cxMinChild  = wr.cx;
    rbi.cx	    =
    rbi.cxIdeal	    = wr.cx;

    rbi.cyChild	    =
    rbi.cyMinChild  =
    rbi.cyMaxChild  = wr.cy - 4;

    m_ControlBar.Window ().Send_WM (RB_INSERTBAND, (WPARAM) -1, (LPARAM) & rbi);
//
//  -------------------------------
//
    rbi.fMask	   &= (~ RBBIM_CHILD);

    rbi.hwndChild   = NULL;

    rbi.fStyle	    = RBBS_BREAK | RBBS_GRIPPERALWAYS /*| RBBS_USECHEVRON*/ | RBBS_CHILDEDGE | RBBS_VARIABLEHEIGHT | RBBS_FIXEDSIZE;

//  m_ControlBar.Window ().Send_WM (RB_INSERTBAND, (WPARAM) -1, (LPARAM) & rbi);
//
//  -------------------------------
//

    m_ControlBar.Window ().GetWindowRect (& wr);

    RootPane ().InsertCtrlBar (& m_ControlBar, & m_TreeViewPane,
			       & m_ControlBar.Window (),
				 LayoutParam (5, 5, 5, 5),
				 SizeParam   (wr.cy,   0));

    }

#endif

/*
	::SetWindowPos (m_TreeView.GetHWND  (), NULL, m_TreeViewPane.WRectPosX	(),
						      m_TreeViewPane.WRectPosY	(),
						      m_TreeViewPane.WRectSizeX (),
						      m_TreeViewPane.WRectSizeY (), SWP_NOZORDER | SWP_NOREDRAW);

	::SetWindowPos (m_StatusBar.GetHWND (), NULL, m_StatusBarPane.WRectPosX	 (),
						      m_StatusBarPane.WRectPosY	 (),
						      m_StatusBarPane.WRectSizeX (),
						      m_StatusBarPane.WRectSizeY (), SWP_NOZORDER | SWP_NOREDRAW);
*/
/*
    if (m_StatusBar.HasStyle (WS_VISIBLE))
    {
	m_StatusBarPane.Show (True);


	::SetWindowPos (m_StatusBar.GetHWND (), NULL, m_StatusBarPane.m_WRect.x ,
						      m_StatusBarPane.m_WRect.y ,
						      m_StatusBarPane.m_WRect.cx / 2,
						      m_StatusBarPane.m_WRect.cy, SWP_NOZORDER | SWP_NOREDRAW);

	::Sleep (2000);
	::MessageBeep (-1);

	::SetWindowPos (m_StatusBar.GetHWND (), NULL, m_StatusBarPane.m_WRect.x ,
						      m_StatusBarPane.m_WRect.y ,
						      m_StatusBarPane.m_WRect.cx,
						      m_StatusBarPane.m_WRect.cy, SWP_NOZORDER | SWP_NOREDRAW);
	::Sleep (2000);
	::MessageBeep (-1);

    }
*/
	if (HasFlag  (GWF_NEEDUPDATE))
	{
	    m_RootPane.UpdateContext  ();

	    SetFlags (GWF_NEEDUPDATE, 0);
	}

	return True ;
};
//
//[]------------------------------------------------------------------------[]

GResult GAPI GWinMain (int, void **, HANDLE)
{
#if TRUE
    {
//	TestClient::RegisterWindowClass ();
    {{
//	MainDialog  WMain;
	CDialogSettings	  WMain;

//	if (WMain.Create (NULL, NULL, MAKEINTRESOURCE (IDD_DIALOG1)))
	if (WMain.Create (NULL, NULL, MAKEINTRESOURCE (IDD_DIALOG4)))
	{
	    printf ("Full Created....\n");

	    WMain.ExecWindow ();
	}
	else
	{
	    printf ("Can not create MainFrame\n");
	}
    }}
	    printf ("\n\n------------------\nDestroyed WMain\n");

//	    GWinMsgLoop::Run  ();
    }
#else
    {
	    MainFrame   WMain;

	    if (WMain.Create (NULL, "MainFrame... ������� ����...",
//			      WS_VISIBLE | WS_MINIMIZE | WS_OVERLAPPEDWINDOW, 0,
//					   WS_MINIMIZE | WS_OVERLAPPEDWINDOW, 0,
//			      WS_VISIBLE | WS_MAXIMIZE | WS_OVERLAPPEDWINDOW, 0,
//					   WS_MAXIMIZE | WS_OVERLAPPEDWINDOW, 0,
			      WS_VISIBLE	       | WS_OVERLAPPEDWINDOW, 0,
//	  WS_CLIPCHILDREN |   WS_VISIBLE	       | WS_OVERLAPPEDWINDOW, 0,
//							 WS_OVERLAPPEDWINDOW, 0,
//			      WS_OVERLAPPEDWINDOW & (~(WS_MAXIMIZEBOX | WS_MINIMIZEBOX)), 0,
//  WS_THICKFRAME | WS_CAPTION | WS_VISIBLE	       | WS_POPUP | WS_SYSMENU  , 
//  WS_EX_TOOLWINDOW /*| WS_EX_WINDOWEDGE*/,

//			      800, 600,  60,  60))
//			      800, 600, 300, 300))
			      100, 600, 600, 400))
	    {
		if (! WMain.IsVisible ())
		{
		      Sleep (2000);

		      WMain.ShowWindow (True, False );

		      Sleep (1000);

		      WMain.ShowWindow (False);

		      Sleep (1000);

		      WMain.ShowWindow (True);
		}
	    }
	    else
	    {
		printf ("Can not create MainFrame\n");
	    }
/*
	    MainDialog	DMain;

	    if (DMain.Create  (& WMain, NULL, MAKEINTRESOURCE (IDD_DIALOG1)))
	    {
		DMain.ShowWindow (True, False);
	    };

	    MainDialog	DMain2;

	    if (DMain2.Create (   NULL, NULL, MAKEINTRESOURCE (IDD_DIALOG1)))
	    {
		DMain2.ShowWindow (True, False);
	    };
*/
	    GWinMsgLoop::Run ();
    }
#endif

    return 0;
};

//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
class TestFrame : public GFrameControl
{
public :
		TestFrame () : GFrameControl (NULL),

		    m_ControlBar (NULL),
		    m_MenuBar	 (NULL)
		{
		    m_pControl =  NULL;
		};

	       ~TestFrame () { printf ("TestFrame::~TestFrame\n");};

protected :

	Bool	    On_Create		    (void *);

	Bool	    DispatchWinMsg	    (GWinMsg &);

	Bool	    On_WM_PosChanged	    (GWinMsg &);

protected :

	GWindow *		m_pControl  ;
	GReBar			m_ControlBar;
	GToolBarMenu		m_MenuBar   ;
};

Bool TestFrame::On_Create (void *)
{
    if (! ::SetMenu (GetHWND (), ::LoadMenu (::GetModuleHandle (NULL), MAKEINTRESOURCE (IDR_MAIN_MENU))))
    {	return False;}

    if (! GFrameControl::On_Create (NULL))
    {	return False;}

	return True ;

    if (! m_ControlBar.Create (this, NULL,
			       WS_VISIBLE | CCS_NOPARENTALIGN | CCS_TOP | CCS_NODIVIDER /*| CCS_NORESIZE*/ | RBS_BANDBORDERS | RBS_DBLCLKTOGGLE | RBS_REGISTERDROP | RBS_VARHEIGHT | RBS_AUTOSIZE, 0/*WS_EX_TOOLWINDOW*/,
			       0, 0, 0, 0))
    {	return False;}

    if (! m_MenuBar.Create (& m_ControlBar, NULL, MAKEINTRESOURCE (IDR_MAIN_MENU)))
    {	return False;}

	m_pControl = & m_ControlBar;

    GRect	    wr ;
    REBARBANDINFO   rbi;

    memset (& rbi, 0, sizeof (rbi));
    rbi.cbSize	    = sizeof (rbi) ;

    rbi.fMask	    = RBBIM_STYLE | RBBIM_CHILD /*| RBBIM_COLORS*/;
    rbi.fMask	   |= (0 /*| RBBIM_HEADERSIZE*/ | RBBIM_IDEALSIZE | RBBIM_SIZE | RBBIM_CHILDSIZE /*| RBBIM_TEXT*/);

    rbi.fStyle	    = RBBS_BREAK | RBBS_GRIPPERALWAYS | RBBS_USECHEVRON | RBBS_CHILDEDGE | RBBS_VARIABLEHEIGHT /*| RBBS_FIXEDSIZE*/;

    rbi.clrFore	    = GetSysColor (COLOR_MENUTEXT);
    rbi.clrBack	    = GetSysColor (COLOR_WINDOW);
    rbi.hwndChild   = m_MenuBar.GetHWND ();
    rbi.lpText	    = _T("Menu Bar:");

    m_MenuBar.GetWindowRect (& wr);

    rbi.cxHeader    = 80;

    rbi.cxMinChild  = 100;//wr.cx;
    rbi.cx	    = 600;
    rbi.cxIdeal	    = 400;

    rbi.cyChild	    =
    rbi.cyMinChild  =
    rbi.cyMaxChild  = m_MenuBar.GetBoundRect ();
    rbi.cyIntegral  =   0;

    m_ControlBar.Send_WM (RB_INSERTBAND, (WPARAM) -1, (LPARAM) & rbi);

//.........................
#if FALSE
    memset (& rbi, 0, sizeof (rbi));
    rbi.cbSize	    = sizeof (rbi) ;

    rbi.fMask	    = RBBIM_STYLE | RBBIM_CHILD | RBBIM_HEADERSIZE | RBBIM_IDEALSIZE | RBBIM_SIZE | RBBIM_CHILDSIZE;
    rbi.fStyle	    = RBBS_BREAK | RBBS_GRIPPERALWAYS | RBBS_USECHEVRON | RBBS_CHILDEDGE /*| RBBS_VARIABLEHEIGHT*/ /*| RBBS_FIXEDSIZE*/;

    rbi.cxHeader    = 80;

    rbi.hwndChild   = ::CreateWindowEx(0, WC_COMBOBOXEX, NULL,
					WS_BORDER | WS_VISIBLE |
					WS_CHILD | CBS_DROPDOWN,
					// No size yet--resize after setting image list.
					0,      // Vertical position of Combobox
					0,      // Horizontal position of Combobox
					0,      // Sets the width of Combobox
					100,    // Sets the height of Combobox
					m_ControlBar.GetHWND (),
					NULL,
					NULL,
					NULL);

    ::GetWindowRect (rbi.hwndChild, & wr);

    rbi.cxMinChild  = 100;//wr.cx;
    rbi.cx	    = 600;
    rbi.cxIdeal	    = 300 - 80;//wr.cx;

    rbi.cyChild	    =
    rbi.cyMinChild  =
    rbi.cyMaxChild  = wr.cy;
    rbi.cyIntegral  = 0	   ;

    m_ControlBar.Send_WM (RB_INSERTBAND, (WPARAM) -1, (LPARAM) & rbi);

#endif






    GRect   cr;

    GetClientRect (& cr);

//  m_pControl ->SetWindowPos (NULL, 11, 11, cr.cx - 22, cr.cy - 22, SWP_NOREDRAW);

    m_ControlBar.GetWindowRect (& wr);

//  m_ControlBar.Send_WM (RB_GETRECT, (WPARAM) 0, (LPARAM) & br);

    m_ControlBar.SetWindowPos  (NULL, 11, 11, cr.cx - 22, wr.cy, 0);
/*

	{{
	    printf ("<<<<<<<<<<<<<<<<Height changed !!!>>>>>>>>>>>>>>\n");

	    REBARBANDINFO   rbi;

	    memset (& rbi, 0, sizeof (rbi));
	    rbi.cbSize	    = sizeof (rbi) ;

	    rbi.fMask	    = RBBIM_CHILDSIZE;

	    m_pControl ->Send_WM (RB_GETBANDINFO, (WPARAM) 0, (LPARAM) & rbi);

//	    rbi.cyChild	    =
//	    rbi.cyMinChild  =
//	    rbi.cyMaxChild  = m_MenuBar.GetBoundRect ();

	    m_pControl ->Send_WM (RB_SETBANDINFO, (WPARAM) 0, (LPARAM) & rbi);


//	    m_pControl ->InvalidateRect (NULL, False);
	}}
*/
	return True ;
};

Bool TestFrame::On_WM_PosChanged (GWinMsg & m)
{
    if (m_pControl && ((((WINDOWPOS *) m.lParam) ->flags & SWP_NOSIZE) == 0))
    {
	int	oh, ch;
	GRect	wr, br, cr;

	printf ("\n\n...........................................\n");

	GetClientRect (& cr);

	printf ("Bound client size : %d.%d\n", cr.cx - 22, cr.cy - 22);

	m_pControl ->GetWindowRect (& br);
	printf ("Ctrl bar     size : %d.%d\n", br.cx, br.cy);
	m_pControl ->Send_WM (RB_GETRECT, (WPARAM) 0, (LPARAM) & br);
	printf ("Band bar     size : %d.%d (%d)\n", br.cx, br.cy, m_pControl ->Send_WM (RB_GETROWHEIGHT, (WPARAM) 0, (LPARAM) 0));
	m_MenuBar.GetWindowRect	   (& wr);
	printf ("Menu bar     size : %d.%d\n", wr.cx, wr.cy);
	oh = m_MenuBar.GetBoundRect  ();
	printf ("\t\t\t\tSet to %d Height\n", br.cy);
/*
	br.Pos  () = GPoint (	0,    0);
	br.Size () = GPoint (1024, 1024);

	m_pControl ->Send_WM (RB_SIZETORECT, (WPARAM) 0, (LPARAM) & br);
	printf ("BestRect     size : %d.%d (%d.%d)\n", br.x, br.y, br.cx, br.cy);
*/

//	m_MenuBar.Send_WM (TB_AUTOSIZE, (WPARAM) 0, (LPARAM) 0);

	m_pControl ->SetWindowPos (NULL, 11, 11, cr.cx - 22, cr.cy - 22, SWP_NOREDRAW);

//	m_pControl ->SetWindowPos (NULL, 11, 11, cr.cx - 22, br.cy     , SWP_NOREDRAW);

	{{
	    m_pControl ->GetWindowRect (& wr);

	    m_pControl ->SetWindowPos  (NULL, 11, 11, cr.cx - 22,
						      m_ControlBar.GetBoundRect (), SWP_NOREDRAW);

//	    m_pControl ->SetWindowPos  (NULL, wr.x, wr.y, wr.cx + ((WINDOWPOS *) m.lParam) ->cx,
//							  wr.cy + ((WINDOWPOS *) m.lParam) ->cy, SWP_NOREDRAW);
	}}









	m_pControl ->GetWindowRect (& br);
	printf ("Ctrl bar (1) size : %d.%d\n", br.cx, br.cy);
	m_pControl ->Send_WM (RB_GETRECT, (WPARAM) 0, (LPARAM) & br);
	printf ("Band bar (1) size : %d.%d\n", br.cx, br.cy);
	m_MenuBar.GetWindowRect	   (& wr);
	printf ("Menu bar (1) size : %d.%d\n", wr.cx, wr.cy);
	ch = m_MenuBar.GetBoundRect  ();
/*
	if (ch != oh)
	if (True)
	{{
	    printf ("<<<<<<<<<<<<<<<<Height changed !!!>>>>>>>>>>>>>>\n");

	    REBARBANDINFO   rbi;

	    memset (& rbi, 0, sizeof (rbi));
	    rbi.cbSize	    = sizeof (rbi) ;

	    rbi.fMask	    = RBBIM_CHILDSIZE;

	    m_pControl ->Send_WM (RB_GETBANDINFO, (WPARAM) 0, (LPARAM) & rbi);

	    rbi.cyChild	    = ch;
	    rbi.cyMinChild  = ch;
	    rbi.cyMaxChild  = ch;

	    m_pControl ->Send_WM (RB_SETBANDINFO, (WPARAM) 0, (LPARAM) & rbi);


//	    m_pControl ->InvalidateRect (NULL, False);
	}}
	else
	{
//	    m_pControl ->ValidateRect   (NULL);
	}

	    m_pControl ->InvalidateRect (NULL, False);
*/

//	m_MenuBar.SetWindowPos (NULL, 11, 11, cr.cx - 22, m_MenuBar.GetBoundRect (), 0);


	m_pControl ->GetWindowRect (& br);
	printf ("Ctrl bar (2) size : %d.%d\n", br.cx, br.cy);
	m_pControl ->Send_WM (RB_GETRECT, (WPARAM) 0, (LPARAM) & br);
	printf ("Band bar (2) size : %d.%d\n", br.cx, br.cy);
	m_MenuBar.GetWindowRect	   (& wr);
	printf ("Menu bar (2) size : %d.%d\n", wr.cx, wr.cy);
	m_MenuBar.GetBoundRect  ();

	printf ("...........................................\n\n\n");
	
    }

    return m.DispatchDefault ();
};

Bool TestFrame::DispatchWinMsg (GWinMsg & m)
{
    switch (m.uType)
    {
    case GWinMsg::WndProc :

	DISPATCH_WM	(WM_WINDOWPOSCHANGED,	On_WM_PosChanged    )

    if ((WM_COMMAND == m.uMsg) && (MMID_FILE_TEST == m.wParam))
    {{
	MainDialog  DMain;

	if (DMain.Create (this, NULL, MAKEINTRESOURCE (IDD_DIALOG1)))
	{
	    DMain.ExecWindow ();
	};
    }}

	return GFrameControl::DispatchWinMsg (m);
    };
	return GFrameControl::DispatchWinMsg (m);
};
//
//[]------------------------------------------------------------------------[]
//
GResult GAPI GWinTest (int, void **, HANDLE)
{
#if TRUE

	    CMain	WMain;

	    if (WMain.Create (NULL, _T("UrpHF Next"),
			      WS_VISIBLE | WS_OVERLAPPED
					 | WS_CAPTION
					 | WS_THICKFRAME	      ,
			      0					      ,
			      300, 200, 800, 600))
	    {}
	    WMain.ExecWindow ();

//	    GWinMsgLoop::Run ();

#endif

#if FALSE
    {
	    TestFrame * pMain = NULL;
	    TestFrame	WMain;

	    if ((pMain = new TestFrame ()) ->Create (NULL, "TestFrame... ������� ����...",
//			      WS_VISIBLE | WS_MINIMIZE | WS_OVERLAPPEDWINDOW, 0,
//					   WS_MINIMIZE | WS_OVERLAPPEDWINDOW, 0,
//			      WS_VISIBLE | WS_MAXIMIZE | WS_OVERLAPPEDWINDOW, 0,
//					   WS_MAXIMIZE | WS_OVERLAPPEDWINDOW, 0,
			      WS_VISIBLE	       | WS_OVERLAPPEDWINDOW, 0,
//							 WS_OVERLAPPEDWINDOW, 0,

//			      800, 600,  60,  60))
//			      800, 600, 300, 300))
			      300, 600, 100, 400))
	    {}

	    if (WMain.Create (pMain, _T("TestFrame... ������� ����..."),
//			      WS_VISIBLE | WS_MINIMIZE | WS_OVERLAPPEDWINDOW, 0,
//					   WS_MINIMIZE | WS_OVERLAPPEDWINDOW, 0,
//			      WS_VISIBLE | WS_MAXIMIZE | WS_OVERLAPPEDWINDOW, 0,
//					   WS_MAXIMIZE | WS_OVERLAPPEDWINDOW, 0,
//			      WS_VISIBLE	       | WS_OVERLAPPEDWINDOW, 0,
							 WS_OVERLAPPEDWINDOW, 0,

//			      800, 600,  60,  60))
//			      800, 600, 300, 300))
			      100, 600, 100, 400))
	    {}
	    WMain.ExecWindow ();

	    GWinMsgLoop::Run ();

	    if (pMain)
	    {
		delete pMain;

		       pMain = NULL;
	    }
    }
#endif

#if FALSE
    {
	    if ((new TestFrame ()) ->Create (NULL, _T("TestFrame... ������� ����..."),
//			      WS_VISIBLE | WS_MINIMIZE | WS_OVERLAPPEDWINDOW, 0,
//					   WS_MINIMIZE | WS_OVERLAPPEDWINDOW, 0,
//			      WS_VISIBLE | WS_MAXIMIZE | WS_OVERLAPPEDWINDOW, 0,
//					   WS_MAXIMIZE | WS_OVERLAPPEDWINDOW, 0,
			      WS_VISIBLE	       | WS_OVERLAPPEDWINDOW, 0,
//							 WS_OVERLAPPEDWINDOW, 0,

//			      800, 600,  60,  60))
//			      800, 600, 300, 300))
			      100, 600, 100, 400))
	    {}
    }
	    GWinMsgLoop::Run ();
#endif


    return 0;
};

/*
Bool TestFrame::On_WM_Size (GWinMsg & m)
{
    return m.DispatchComplete ();
};

Bool TestFrame::DispatchWinMsg (GWinMsg & m)
{
    return GFrameControl::DispatchWinMsg (m);
};
*/







/*

GResult GThread::RunApartment (GApartment * pa, int nArgs, void ** pArgs, HANDLE hResume)
{
    GResult	 dResult;
    GApartment * pOldApp;

    pOldApp =  m_pCurApartment	   ;
	       m_pCurApartment = pa;

    pa ->m_pCoThread   = this;

    pa ->m_XUnknown  .Init    (pa);
    pa ->m_IApartment.Init    (pa, IID_IApartment, & pa ->m_XUnknown);

    pa ->m_XUnknown  .Release ();

    if (m_pWakeUpPort)
    {
	m_pWakeUpPort ->EnterWaitLoop ();
    }

    dResult =  pa ->Main (nArgs, pArgs, hResume);

    pa ->m_IApartment.Release ();

    while (0 < pa ->GetRefCount   ())
    {
	if (!  pa ->PumpInputWork ())
	{
	       pa ->WaitInputWork (INFINITE);
	}
    }

    if (m_pWakeUpPort)
    {
	m_pWakeUpPort ->LeaveWaitLoop ();
    }
	       pa ->Done ();

    pa ->m_pCoThread   = NULL;

	       m_pCurApartment = pOldApp;

    return dResult;
};

GResult GAPI RunApartment (GApartment * pa, int nArgs, void ** pArgs, HANDLE hResume)
{
    GResult	   dResult;
    GThread * th = TCurrent ();

    if (th && pa)
    {
	if (GThread::User == th ->GetType ())
	{
	    dResult = th ->RunApartment (pa, nArgs, pArgs, hResume);
	}
	else
	{
//	    return Run
	}
    }
    else
    {
	    dResult = ERROR_INVALID_PARAMETER;
    }

    return  dResult;
};
*/

/*
GResult GAPI CRRPDevClient::Main (int void **, HANDLE hResume)
{
};

    a.Run (TCurrent (), 0, NULL, hResume);

	   RunApartment (

    Application a (NULL)

    return new (newAllocator) Application ->Run (0, NULL, hResume);
*/

int __main (int nArgs, void ** pArgs)
{
    int	iResult = -1;

#if FALSE

    if (GWinInit ())
    {
	if (InitCtrl32 (ICC_BAR_CLASSES | ICC_COOL_CLASSES))
	{
	    TestClient::RegisterWindowClass ();

	    printf ("%d - %d\n", sizeof (GWinProcThunk), sizeof (GWinMsgHandler));

//	    PrimaryThread <TWinMsgThread <GWinMsgLoop> > (GWinMain, NULL, NULL);

	    PrimaryThread <TWinMsgThread <GWinMsgLoop> > (GWinTest, NULL, NULL);

#if FALSE

#if TRUE
	    CreateThread  <TWinMsgThread <GWinMsgLoop> > (NULL	  ,
							  GWinMain, NULL, NULL);
#else
	    HANDLE	   hGUI;

	    CreateThread  <TWinMsgThread <GWinMsgLoop> > (& hGUI  ,
							  GWinMain, NULL, NULL);

	    PrimaryThread <TWinMsgThread <GWinMsgLoop> > (GWinTest, NULL, NULL);

	    ::WaitForAny  (1, hGUI, INFINITE);

	    ::CloseHandle (hGUI);
#endif

#endif
	    iResult = 0;
	}
	else
        {
	    printf ("Can not init Shell controls\n");
	}

	GWinDone ();

	    printf ("Done...\n");
    }
    else
    {
	    printf ("Can not init GWin\n");
    }

#endif

    return iResult;
};
//
//[]------------------------------------------------------------------------[]















class GIdleLoop
{
public :

virtual	void *		Run		();

virtual	Bool		WaitInputWork	(DWord dTimeout = INFINITE);

virtual	Bool		PumpInputWork	();

protected :

	Bool		m_bExitFlag	;
	void *		m_pExitCode	;
};


Bool GIdleLoop::WaitInputWork (DWord dTimeout)
{
    if (! m_bExitFlag)
    {
	return (TCurrent () ->GetCurApartment () ->WaitInputWork (dTimeout), True);
    }
	return False;
};

/*
void * GIdleLoop::Run ()
{
    do
    {
	do {} while (! PumpInputWork ());

    }	      while (

};
*/

#if FALSE

Bool GInputLoop::WaitInputWork (DWord dTimeout)
{
    if (m_bExitFlag)
    {
	return (m_pParentLoop)
    }

};



Bool GApartment::RunInputLoop ()
{
    do 
    {
	do {} while (! PumpInputWork ());

    } 	      while (m_pTopInputLoop ->WaitInputWork (INFINITE);
};





    while (! 


#endif


//[]------------------------------------------------------------------------[]
//
class GWinMsgLoop
{
public :

	void		AddPreprocHandler	(GWindow *);

	void		RemovePreprocHandler	(GWindow *);

	INT_PTR		Exec			();

virtual	void		Exit			(INT_PTR  );

protected :

	int		    m_cPreprocArr	;
	GWindow *	    m_pPreprocArr [32]	;
};

/*

void 


void CMain::On_DialogSettings (GWinMsgHandler * h, GWindow * pSoruce, GWinMsg & m)
{

    while (True)
    {
	if (! PumpInputWork ())
	{
	      WaitInputWork (INFINITE);
	}
    };
};


    while (;;)
    {
    }

    if (WM_COMMAND == m.uMsg)
    {
	if (IS_WM_COMMAND_ID (IDCLOSE))
	{
	    ExecExit ();
	};
    }
}

    typedef Bool    (GAPI * GWinPreproc )(void *, MSG *);

    struct GWinPreprocHandler
    {
	    GWinPreproc		m_pProc ;
	    void *		m_oProc ;
    }
}

Bool CMain::On_DialogSettings (GWinMsg & m)


Bool CMain::On_FileTest (GWinMsg &)
{
    Exec (this, 

    INT_PTR dResult = -1;

    CDialogSettings Dlg	;

    

    if (Dlg.Create (this, NULL, MAKEINTRESOURCE (IDD_DIALOG4)))
    {
	dResult = ExecWindow (Dlg, GWinMsgHandler *);
    }
};

    ...


    INT_PTR uResult = (GWinMsgApartment *) TCurrent () ->GetCurApartment () ->Run (MsgLoop);
}

*/


void GWinMsgLoop::AddPreprocHandler (GWindow * pWindow)
{
    if (m_cPreprocArr < (sizeof (m_pPreprocArr) / sizeof (GWindow *)))
    {
	m_pPreprocArr [m_cPreprocArr] = pWindow;

        m_cPreprocArr ++;
    }
};

void GWinMsgLoop::RemovePreprocHandler (GWindow * pWindow)
{
    for (int i = 0; i < m_cPreprocArr; i ++)
    {
    if (m_pPreprocArr [i] == pWindow)
    {
	if (i + 1  < m_cPreprocArr)
	{
	    memmove (m_pPreprocArr + i, m_pPreprocArr + i + 1, (Byte *)(m_pPreprocArr + m_cPreprocArr) -
							       (Byte *)(m_pPreprocArr + i + 1	     ));
	}

	m_cPreprocArr --;

        return;
    }
    }
};




























//[]------------------------------------------------------------------------[]
//
#ifdef GSH_REMOVE

class GWinMsgLoop;

class GWinMsgThread : public GThread
{
friend class GWinMsgLoop;

protected :
			GWinMsgThread	(GWinMsgLoop * pWinMsgLoop)
			{
			    m_pWinMsgLoop = pWinMsgLoop;
			};

friend	GWinMsgLoop *	GetWinMsgLoop	();

private :

	GWinMsgLoop *	    m_pWinMsgLoop ;
};

GINLINE GWinMsgLoop *	GetWinMsgLoop	() 
			{
			    return ((GWinMsgThread *) TCurrent ()) ->m_pWinMsgLoop;
			};
//
//[]------------------------------------------------------------------------[]
//
class GWinMsgLoop
{
public :

typedef void (GAPI *	PostProc)(void *, void *);

static	void  GAPI	RegisterPreprocHandler	     (GWinMsgProc pProc, void * oProc);

static	void  GAPI	UnRegisterPreprocHandler     (GWinMsgProc pProc, void * oProc);

static	int   GAPI	IncLockCount () { return  ++ (GetWinMsgLoop () ->m_cLockCount);};

static	int   GAPI	DecLockCount () { return  -- (GetWinMsgLoop () ->m_cLockCount);};

static	void  GAPI	Run	     () { GetWinMsgLoop () ->Run 
						   (& GetWinMsgLoop () ->m_cLockCount);};
static	DWord GAPI	Exec	     ();
/*
static	void  GAPI	Exit	     (DWord dExitCode);
*/
static	void  GAPI	SetPostProc  (PostProc, void *, void * = NULL);

static	void  GAPI	BegModalLoop ();

static	void  GAPI	EndModalLoop ();

			GWinMsgLoop  ()
			{
			    m_pExitCode   = NULL;
			    m_pExitFlag   = NULL;

			    m_cLockCount  =    0;

			    m_pPostProc	  = NULL;
			    m_oPostProc	  =
			    m_cPostProc	  = NULL;

//			    m_cGetMsgHook =    0;
//			    m_hGetMsgHook = NULL;
			};

//static	void  GAPI	Post_GM	     ()

protected :

virtual	void		WaitMessage	    (	   DWord    dTimeout);

	Bool		PreprocMessage	    (MSG &);

static	DWord		WaitPerform	    (      int	    nWait   ,
					     const HANDLE * pWait   ,
						   DWord    dTimeout);
private :

	DWord 		    Run		    (int *);

static	LRESULT CALLBACK    GetMessageHook  (int, WPARAM, LPARAM);

private :

	    int   *		    m_pExitFlag	 ;
	    DWord *		    m_pExitCode	 ;

	    int			    m_cLockCount ;

	    GWinMsgHandler::SList   m_SPreproc	 ;

	    PostProc		    m_pPostProc	 ;
	    void *		    m_oPostProc	 ;
	    void *		    m_cPostProc	 ;

//	    int			    m_cGetMsgHook  ;
//	    HHOOK		    m_hGetMsgHook   ;
};

template <class T = GWinMsgLoop> class TWinMsgThread : public GWinMsgThread
{
public  :
			TWinMsgThread () : GWinMsgThread (& m_WinMsgLoop)
			{};
private :

	    T			    m_WinMsgLoop;
};

#endif
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
#ifdef GSH_REMOVE

DWord GWinMsgLoop::WaitPerform (int nWait, const HANDLE * pWait, DWord dTimeout)
{
    return ::MsgWaitForMultipleObjectsEx (nWait, pWait, dTimeout ,
					  QS_ALLINPUT
					| QS_ALLPOSTMESSAGE
					| 0			 ,  // Wakeup by any event in the queue
					  MWMO_ALERTABLE
    					| MWMO_INPUTAVAILABLE	    // Alertable & No wait if has input !!!
					| 0);
};

void GWinMsgLoop::WaitMessage (DWord dTimeout)
{
    WaitPerform (0, NULL, dTimeout);
};

/*
Bool GWinMsgLoop::DispatchMessage (MSG & msg)
{
    if (NULL == msg.hwnd)
    {
	printf ("Null HWND Message !!! %08X %08X %08X\n", msg.message, msg.wParam, msg.lParam);

	::MessageBeep (MB_ICONHAND);

	return (::DispatchMessage (& msg), True);
    }

	return False;
};
*/

void GAPI GWinMsgLoop::RegisterPreprocHandler	(GWinMsgProc pProc, void * oProc)
{
    GWinMsgLoop	   * pLoop = GetWinMsgLoop ();

    pLoop ->m_SPreproc.insert (new GWinMsgHandler (pProc, oProc)) ->m_PList = 
  & pLoop ->m_SPreproc;
};

void GAPI GWinMsgLoop::UnRegisterPreprocHandler (GWinMsgProc pProc, void * oProc)
{
    GWinMsgLoop * pLoop = GetWinMsgLoop ();

    for (TXList_Iterator <GWinMsgHandler> 
	I   = pLoop ->m_SPreproc.head (); I.next ();)
    {
	if ((I.next () ->m_pProc == pProc)
	&&  (I.next () ->m_oProc == oProc))
	{
	    delete pLoop ->m_SPreproc.extract (I);

	    break;
	}
	I <<= pLoop ->m_SPreproc.getnext (I.next ());
    };
};

void GAPI GWinMsgLoop::SetPostProc (PostProc pPostProc, void * oPostProc, void * pContext)
{
    GWinMsgLoop * pLoop = GetWinMsgLoop ();

    pLoop ->m_pPostProc = pPostProc;
    pLoop ->m_oPostProc = oPostProc;
    pLoop ->m_cPostProc = pContext ;
};




/*
static LRESULT CALLBACK GetMsgHookProc (int code, WPARAM wParam, LPARAM lParam)
{
    if (HC_ACTION == code)
    {
    if (PM_REMOVE == wParam)
    {
    printf ("GetMsgHook:: %08X, %08X %08X %08X\n", ((MSG *) lParam) ->hwnd   ,
						   ((MSG *) lParam) ->message,
						   ((MSG *) lParam) ->wParam ,
						   ((MSG *) lParam) ->lParam );

    }
    }
    return ::CallNextHookEx (NULL, code, wParam, lParam);
};

    HHOOK	h = ::SetWindowsHookEx (WH_GETMESSAGE, GetMsgHookProc, NULL, ::GetCurrentThreadId ());

    ::UnhookWindowsHookEx (h);
*/

LRESULT CALLBACK GWinMsgLoop::GetMessageHook (int code, WPARAM wParam, LPARAM lParam)
{
    if ((HC_ACTION == code) && (PM_REMOVE == wParam) && (NULL != lParam))
    {
    printf ("GetMsgHook:: %08X, %08X %08X %08X\n", ((MSG *) lParam) ->hwnd   ,
						   ((MSG *) lParam) ->message,
						   ((MSG *) lParam) ->wParam ,
						   ((MSG *) lParam) ->lParam );

//	if (GetWinMsgLoop () ->PreprocMessage (* (MSG *) lParam))
//	{
//	}
    }
    return ::CallNextHookEx (NULL, code, wParam, lParam);
};

/*
void GWinMsgLoop::BegModalLoop ()
{
    GWinMsgLoop * pLoop = GetWinMsgLoop ();

    if (0 == pLoop ->m_cGetMsgHook)
    {
	pLoop ->m_cGetMsgHook += ((NULL != 
       (pLoop ->m_hGetMsgHook  = ::SetWindowsHookEx (WH_GETMESSAGE , 
						     GetMessageHook, 
						     NULL, TCurrent () ->GetId ()))) ? 1 : 0);
    }
};

void GWinMsgLoop::EndModalLoop ()
{
    GWinMsgLoop * pLoop = GetWinMsgLoop ();

    if (0 == -- pLoop ->m_cGetMsgHook)
    {
	::UnhookWindowsHookEx (pLoop ->m_hGetMsgHook);
			       pLoop ->m_hGetMsgHook = NULL;
    }
};
*/

DWord GAPI GWinMsgLoop::Exec ()
{
    int	   bExitFlag = 1;

    return GetWinMsgLoop () ->Run  (& bExitFlag);
};

/*
void GAPI GWinMsgLoop::Exit (DWord dExitCode)
{
    GWinMsgLoop * pLoop = GetWinMsgLoop ();

    if (pLoop ->m_pExitCode)
    {
      * pLoop ->m_pExitCode = dExitCode;
    }
    if (pLoop ->m_pExitFlag
    && (pLoop ->m_pExitFlag != & pLoop ->m_cLockCount))
    {
      * pLoop ->m_pExitFlag = 0;
    }
};
*/


#if FALSE

DWord GWinMsgLoop::Exec (GWindow *)
{
    GWinMsgLoop	Loop;

    while (pWindow.HasFalg
};



#endif






DWord GWinMsgLoop::Run (int * pExitFlag)
{
    MSG		msg ;

    DWord	dExitCode = 0;

    int   *	pOldExitFlag = m_pExitFlag ;
    DWord *	pOldExitCode = m_pExitCode ;

    PostProc	pPostProc ;
    void  *	oPostProc ;
    void  *	cPostProc ;

    m_pExitFlag =   pExitFlag  ;
    m_pExitCode = & dExitCode  ;

    while (True)
    {
	while ((pPostProc = m_pPostProc) != NULL)
	{
		oPostProc = m_oPostProc;
		cPostProc = m_cPostProc;
			    m_pPostProc = NULL;
			    m_oPostProc = NULL;
			    m_cPostProc = NULL;
	    
	       (pPostProc)(oPostProc, cPostProc);
	};

//	if (* pExitFlag)
	{
	    if (::PeekMessage (& msg, NULL, 0, 0, PM_REMOVE))
	    {
#if TRUE
		if (WM_QUIT == msg.message)
		{
		    dExitCode = msg.wParam;

//printf ("\tQuit exit %08X...................................\n", dExitCode);
		    break;
		}
#endif

//printf ("<<Process for msg : %08X %08X [%08X,%08X]..................\n", msg.hwnd, msg.message, msg.wParam, msg.lParam);

//if (::GetCapture ())
//printf ("\t\tWith Capture : %08X\n", ::GetCapture ());


		if (PreprocMessage  (  msg))
		{	
//printf (">>Process by IsDialogMessage\n");

			continue;
		}
		::TranslateMessage  (& msg);
		::DispatchMessage   (& msg);
//printf (">>Process by DispatchMessage\n");
		{	continue;}
	    }

//printf ("\t\t\t\t<<Wait");

	    WaitMessage  (INFINITE);

//printf (">>\n");

			continue;
	}
			break;
    };

    m_pExitFlag = pOldExitFlag;
    m_pExitCode = pOldExitCode;

	return	dExitCode;
};


#endif
//
//[]------------------------------------------------------------------------[]

/*
BOOL [!output WTL_FRAME_CLASS]::PreTranslateMessage(MSG* pMsg)
{
[!if WTL_APPTYPE_MDI]
	if([!output WTL_FRAME_BASE_CLASS]<[!output WTL_FRAME_CLASS]>::PreTranslateMessage(pMsg))
		return TRUE;

	HWND hWnd = MDIGetActive();
	if(hWnd != NULL)
		return (BOOL)::SendMessage(hWnd, WM_FORWARDMSG, 0, (LPARAM)pMsg);

	return FALSE;
[!else]
[!if WTL_USE_VIEW]
	if([!output WTL_FRAME_BASE_CLASS]<[!output WTL_FRAME_CLASS]>::PreTranslateMessage(pMsg))
		return TRUE;

	return m_view.PreTranslateMessage(pMsg);
[!else]
	return [!output WTL_FRAME_BASE_CLASS]<[!output WTL_FRAME_CLASS]>::PreTranslateMessage(pMsg);
[!endif]
[!endif]
}





BOOL [!output WTL_MAINDLG_CLASS]::PreTranslateMessage(MSG* pMsg)
{
[!if WTL_HOST_AX]
	if((pMsg->message < WM_KEYFIRST || pMsg->message > WM_KEYLAST) &&
	   (pMsg->message < WM_MOUSEFIRST || pMsg->message > WM_MOUSELAST))
		return FALSE;

	HWND hWndCtl = ::GetFocus();
	if(IsChild(hWndCtl))
	{
		// find a direct child of the dialog from the window that has focus
		while(::GetParent(hWndCtl) != m_hWnd)
			hWndCtl = ::GetParent(hWndCtl);

		// give control a chance to translate this message
		if(::SendMessage(hWndCtl, WM_FORWARDMSG, 0, (LPARAM)pMsg) != 0)
			return TRUE;
	}

[!endif]
	return IsDialogMessage(pMsg);
}



BOOL [!output WTL_VIEW_CLASS]::PreTranslateMessage(MSG* pMsg)
{
[!if WTL_HOST_AX]
	if((pMsg->message < WM_KEYFIRST || pMsg->message > WM_KEYLAST) &&
	   (pMsg->message < WM_MOUSEFIRST || pMsg->message > WM_MOUSELAST))
		return FALSE;

	HWND hWndCtl = ::GetFocus();
	if(IsChild(hWndCtl))
	{
		// find a direct child of the dialog from the window that has focus
		while(::GetParent(hWndCtl) != m_hWnd)
			hWndCtl = ::GetParent(hWndCtl);

		// give control a chance to translate this message
		if(::SendMessage(hWndCtl, WM_FORWARDMSG, 0, (LPARAM)pMsg) != 0)
			return TRUE;
	}

[!endif]
[!if WTL_VIEWTYPE_HTML]
	if((pMsg->message < WM_KEYFIRST || pMsg->message > WM_KEYLAST) &&
	   (pMsg->message < WM_MOUSEFIRST || pMsg->message > WM_MOUSELAST))
		return FALSE;

	// give HTML page a chance to translate this message
	return (BOOL)SendMessage(WM_FORWARDMSG, 0, (LPARAM)pMsg);
[!else]
[!if WTL_VIEWTYPE_FORM]
	return IsDialogMessage(pMsg);
[!else]
	pMsg;
	return FALSE;
[!endif]
[!endif]
}
*/