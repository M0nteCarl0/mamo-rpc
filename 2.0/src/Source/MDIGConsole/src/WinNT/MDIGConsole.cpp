
#include "G_guibase_new.cpp"

#include "MDIGUtils.h"

//[]------------------------------------------------------------------------[]
#if False

#if True
	Word	g_LangID = 0;
#elif False
	Word	g_LangID = MAKELANGID (LANG_NEUTRAL, SUBLANG_UI_CUSTOM_DEFAULT);
#elif False
	Word    g_LangID = MAKELANGID (LANG_RUSSIAN, SUBLANG_NEUTRAL);
#elif False
	Word	g_LangID = MAKELANGID (LANG_ENGLISH, SUBLANG_DEFAULT);
#endif

#else
	Word	g_LangID = MAKELANGID (LANG_RUSSIAN, SUBLANG_NEUTRAL);
#endif
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
class GUIClientConfig
{
public :
		    GUIClientConfig ()
		    {
			m_hFile = NULL;
		    };

		   ~GUIClientConfig ()
		    {
			Close ();
		    };

public :

	GResult	    Create		    ();

	GResult	    Save		    ();

	void	    Close		    ();

protected :

	HANDLE		    m_hFile	;

#pragma pack (_M_PACK_LONG)

public :

	struct
	{
	    DWord	    dEquMask	;

	    int		    iScrSizeX	;
	    int		    iScrSizeY	;

	struct Panel_Sav
	{
	    int		    iScrPosX	;
	    int		    iScrPosY	;
	};
	    Panel_Sav	    MainPanel	;
	    Panel_Sav	    UrpPanel	;
	    Panel_Sav	    MSCPanel	;
	    Panel_Sav	    MM2Panel	;
	    Panel_Sav	    MDIGPanel	;

	}		    m_Body	;

#pragma pack ()
};

static void GetBasePath (TCHAR * pPath);

GResult GUIClientConfig::Create ()
{
    GResult dResult		;
    TCHAR   pFileName [MAX_PATH];

    GetBasePath (pFileName);

    strcat	(pFileName, "\\" );
    strcat	(pFileName, "MDIGConsole.cfg");

    HANDLE  hFile = ::CreateFile (pFileName,
				  GENERIC_READ | GENERIC_WRITE,
				  0,
				  NULL,
				  OPEN_ALWAYS,
				  FILE_ATTRIBUTE_NORMAL,
				  NULL);

    if (INVALID_HANDLE_VALUE == hFile)
    {
	return ::GetLastError ();
    }
	dResult = ERROR_SUCCESS;

	m_hFile = hFile;

    if (ERROR_ALREADY_EXISTS != ::GetLastError ())
    {
	::memset (& m_Body, 0, sizeof (m_Body));

    if (ERROR_SUCCESS != 
       (dResult	       = Save ()))
    {
	Close  ();
	return dResult;
    }
    }
	       ::SetFilePointer (m_hFile, 0, NULL, FILE_BEGIN);	
	return ::ReadFile	(m_hFile, & m_Body, sizeof (m_Body), & dResult, NULL) ? ERROR_SUCCESS
										      : ::GetLastError ();
};

GResult GUIClientConfig::Save ()
{
    GResult dResult;

	   ::SetFilePointer (m_hFile, 0, NULL, FILE_BEGIN);
    return ::WriteFile	    (m_hFile, & m_Body, sizeof (m_Body), & dResult, NULL) ? ERROR_SUCCESS
										  : ::GetLastError ();
};

void GUIClientConfig::Close ()
{
    if (m_hFile)
    {
	::CloseHandle (m_hFile);
		       m_hFile = NULL;
    }
};
//
//[]------------------------------------------------------------------------[]


//[]------------------------------------------------------------------------[]
//
static	IRRPDeviceService *	s_pIRRPService = NULL;
//
//[]------------------------------------------------------------------------[]

void AddPreprocHandler (GWindow *);

void RemPreprocHandler (GWindow *);

//[]------------------------------------------------------------------------[]
//
	GDialog *		s_pMainPanel   = NULL;

#include "MDIGUrp.cpp"
#include "MDIGScanner.cpp"
#include "MDIGMM2.cpp"
#include "MDIG.cpp"

class CMainPanel : public GDialog
{
public :
		CMainPanel () : GDialog (NULL)
		{
		    m_pIRRPSettings	= NULL ;
		    m_SetupData.
		    m_bChanged		= False;

			      m_dEquMask  = 0;
		    memset (& m_eUrpPanel , 0, sizeof (m_eUrpPanel ));
		    memset (& m_eMSCPanel , 0, sizeof (m_eMSCPanel ));
		    memset (& m_eMM2Panel , 0, sizeof (m_eMM2Panel ));
		    memset (& m_eMDIGPanel, 0, sizeof (m_eMDIGPanel));

		    m_nSetupPager	=   -1;
		    m_hSetupPager	= NULL;

		    m_pPageDlgProc	= new GDlgProcThunk 
		   (DispatchSetupDlgMsg, this, _nullDlgProc);

		    m_nCurPage		=   -1;
		    m_hCurPage		= NULL;

		    memset (m_nCurCash, 0, sizeof (m_nCurCash));

		    m_pCurPageHandler	= NULL;

		    m_cLedInterface	=
		    m_cLedUrp		=
		    m_cLedKKM		=
		    m_cLedMSCDiafr	=
		    m_cLedMSCScan	=
		    m_cLedMSCFG		=
		    m_cLedMM2		=    0;

		    m_iDetCalibSet	=    0;
		};

	void		DoSetupExchange		(int, Bool);

	Bool		GetSettingsValue	(IRRPSettings::ParamId, _U32 * pValue);
	Bool		GetSettingsValue	(IRRPSettings::ParamId, char * pBuf, DWord dBufSize);

	Bool		SetSettingsValue	(IRRPSettings::ParamId, _U32   dValue);
	Bool		SetSettingsValue	(IRRPSettings::ParamId, char * pBuf);

protected :

	void		SetEquMask		(DWord dEquMask);

	Bool		DispatchSetupDlgMsg	(GWinMsg &);

	void		On_SetupChanged		();

	Bool		On_Setup_MDIG_Page0	(GWinMsg &);

	Bool		On_Setup_URP_Page0	(GWinMsg &);
	Bool		On_Setup_URP_Page1	(GWinMsg &);
	Bool		On_Setup_URP_Page2	(GWinMsg &);

	Bool		On_Setup_MSC_Page0	(GWinMsg &);
	Bool		On_Setup_MSC_Page1	(GWinMsg &);



static	Bool GAPI	DispatchSetupDlgMsg	(void * oDsp, GWinMsg & m);

protected :

	Bool		On_Close		(GWinMsg &);

	void		On_Destroy		();


	Bool		On_SetupPage		(GWinMsg &);


	HWND		On_SelectSetupPager	(int);


//	Bool		On_CreateSetupPage	(HWND, int, HWND, LPCTSTR);
	Bool		On_Create		(void *);

	void		On_EquMsg		(DWord dEquMask, DWord dEquParam);

	Bool		On_DlgMsg		(GWinMsg &);

protected :

	struct PanelEntry
	{
	    GDialog *		m_pDialog   ;
	    GPoint		m_sWinPos   ;

			PanelEntry ()
			{
			    m_pDialog = NULL;
			    m_sWinPos.Assign (0, 0);
			};
	};

	void		On_DeletePanel		(     PanelEntry *);
	void		On_CreatePanel		(int, PanelEntry *);

	void		Do_Setup		();

protected :

	IRRPSettings *		m_pIRRPSettings ;

	int			m_cLedInterface ;
	CLedIndicator		m_oLedInterface	;

	int			m_cLedUrp	;
	CLedIndicator		m_oLedUrp	;

	int			m_cLedKKM	;
	CLedIndicator		m_oLedKKM	;

	int			m_cLedMSCDiafr	;
	CLedIndicator		m_oLedMSCDiafr	;

	int			m_cLedMSCScan	;
	CLedIndicator		m_oLedMSCScan	;

	int			m_cLedMSCFG	;
	CLedIndicator		m_oLedMSCFG	;

	int			m_cLedMM2	;
	CLedIndicator		m_oLedMM2	;

	CEditHandler		m_hEdit		;

	DWord			m_dEquMask	;

	PanelEntry		m_eUrpPanel	;
	PanelEntry		m_eMSCPanel	;
	PanelEntry		m_eMM2Panel	;
	PanelEntry		m_eMDIGPanel	;

	int			m_nSetupPager	;
	HWND			m_hSetupPager	;

	GDlgProcThunk *		m_pPageDlgProc	;

	int			m_nCurPage	;
	HWND			m_hCurPage	;

	Bool	 (CMainPanel::*	m_pCurPageHandler)(GWinMsg &);

	int			m_nCurCash [8]	;

	struct
	{
	    Bool		m_bChanged	    ;

	union
	{
	struct
	{
	    _U32		m_uEquMask	    ;
	    char		m_pSerialNum [128]  ;

	}			m_SetupMDIG ;
	struct
	{
	    _U32		m_SF_UAnodeMin	     ;
	    _U32		m_SF_UAnodeMax	     ;
	    _U32		m_SF_IFilamentPreMin ;
	    _U32		m_SF_IFilamentPreMax ;
	    _U32		m_SF_IFilamentPreDef ;
	    _U32		m_SF_IFilamentMax    ;

	    _U32		m_BF_UAnodeMin	     ;
	    _U32		m_BF_UAnodeMax	     ;
	    _U32		m_BF_IFilamentPreMin ;
	    _U32		m_BF_IFilamentPreMax ;
	    _U32		m_BF_IFilamentPreDef ;
	    _U32		m_BF_IFilamentMax    ;

	    _U32		m_UForceMin	    ;
	    _U32		m_TForceMax	    ;
	    _U32		m_TTubeMax	    ;
	    _U32		m_HeatForceLo	    ;
	    _U32		m_HeatForceHi	    ;
	    _U32		m_HeatForceK	    ;
	    _U32		m_HeatTubeLo	    ;
	    _U32		m_HeatTubeHi	    ;
	    _U32		m_HeatTubeK	    ;

	    _U32		m_HeatDelay	    ;

	    _U32		m_DelayParam_Full   ;
	    _U32		m_DelayParam_21x30  ;
	    _U32		m_DelayParam_18x24  ;
	    _U32		m_DelayParam_12x12  ;

	    _U32		m_UAnodeCorrection  ;
	    _U32		m_IAnodeRetention   ;

	}			m_SetupUrp  ;

	struct
	{
	    _U32		m_Diafr_SyncDelay   ;
	    _U32		m_Diafr_DefCoeff    ;
	    _U32		m_Diafr_AccelTime   ;
	    _U32		m_Diafr_Velocity    ;
	    _U32		m_Diafr_StepPerDec  ;

	    _U32		m_Scan_SyncDelay    ;
	    _U32		m_Scan_DefCoeff	    ;
	    _U32		m_Scan_AccelTime    ;
	    _U32		m_Scan_Velocity	    ;
	    _U32		m_Scan_StepPerDec   ;

	    _U32		m_Scan_CenterLine   ;
	    _U32		m_Scan_12_Param	    ;
	    _U32		m_Scan_23_Param	    ;
	    _U32		m_Scan_ImageProcMask;

	}			m_SetupMSC	    ;
	};

	}			m_SetupData	    ;
    
	int			m_iDetCalibSet	    ;	// -2, -1, 0, +1, +2
};
//
//[]------------------------------------------------------------------------[]
//
Bool CMainPanel::GetSettingsValue (IRRPSettings::ParamId uParamId, _U32 * pValue)
{
    if (		 m_pIRRPSettings 
    && (ERROR_SUCCESS == m_pIRRPSettings ->GetValue (uParamId, pValue, sizeof (_U32), NULL)))
    {
	return True ;
    }
	return False;
};

Bool CMainPanel::GetSettingsValue (IRRPSettings::ParamId uParamId, char * pBuf,
								   DWord  dBufSize)
{
    DWord    dReaded;

    if (		 m_pIRRPSettings
    && (ERROR_SUCCESS == m_pIRRPSettings ->GetValue (uParamId, pBuf, dBufSize, & dReaded)))
    {
	return True ;
    }
	return False;
};
//
//[]------------------------------------------------------------------------[]
//
Bool CMainPanel::SetSettingsValue (IRRPSettings::ParamId uParamId, _U32   dValue)
{
    if (		 m_pIRRPSettings
    && (ERROR_SUCCESS == m_pIRRPSettings ->SetValue (uParamId, & dValue, sizeof (_U32))))
    {
	return True ;
    }
	return False;
};

Bool CMainPanel::SetSettingsValue (IRRPSettings::ParamId uParamId, char * pBuf)
{
    if (		 m_pIRRPSettings
    && (ERROR_SUCCESS == m_pIRRPSettings ->SetValue (uParamId, pBuf, strlen (pBuf))))
    {
	return True ;
    }
	return False;
};
//
//[]------------------------------------------------------------------------[]
//
void CMainPanel::On_SetupChanged ()
{
    if (! m_SetupData.m_bChanged)
    {
	EnableCtrl (IDC_SPCANCEL, True);
	EnableCtrl (IDC_SPOK	, True);

	m_SetupData.m_bChanged  = True ;

    if (m_nSetupPager == 2)
    {{
	TCITEM ti = {TCIF_PARAM};

	::SendMessage	 (m_hSetupPager, TCM_GETITEM, (WPARAM) 0, (LPARAM) & ti);

	::EnableWindow	 (GetDlgItem ((HWND) ti.lParam, IDC_MMSC_VERSION), False);
    }}
    }
};

void CMainPanel::DoSetupExchange (int nSetupPager, Bool bSave)
{
    HWND    hPage;
    TCITEM  ti = {TCIF_PARAM};
    char    pBuf  [128];

    if (! bSave)
    {
	memset	   (& m_SetupData, 0, sizeof (m_SetupData));
    }
	m_SetupData.m_bChanged	= False;

	EnableCtrl (IDC_SPCANCEL, False);
	EnableCtrl (IDC_SPOK	, False);

    if (0 == nSetupPager)
    {{
	int	n;
	char *	pHead;

	// Page 0 :
	//
	::SendMessage (m_hSetupPager, TCM_GETITEM, (WPARAM) 0, (LPARAM) & ti); hPage = (HWND) ti.lParam;
	if (! bSave)
	{{
		* pBuf	       = '\0';
	    GetSettingsValue (IRRPSettings::ID_MDIG_SerialNum,  pBuf, sizeof (pBuf));

	    pHead = ('"' == * pBuf) ? pBuf + 1 : pBuf;

	    if ((1 < (n = strlen (pBuf))) && ('"' == pBuf [n - 1]))
	    {
		  pBuf [n - 1] = '\0';
	    }

	    ::SetDlgItemText (hPage, IDC_SPMDIG_SERIALNUM_EDT	, pHead);

	    GetSettingsValue (IRRPSettings::ID_MDIG_EquMask,  & m_SetupData.m_SetupMDIG.m_uEquMask);

	    ::CheckDlgButton (hPage, IDC_SPMDIG_EQU_URP_INC	, (IRRPMDIG::EquMask_Urp    & m_SetupData.m_SetupMDIG.m_uEquMask) ? BST_CHECKED : BST_UNCHECKED);
	    ::CheckDlgButton (hPage, IDC_SPMDIG_EQU_URPKKM_INC	, (IRRPMDIG::EquMask_KKM    & m_SetupData.m_SetupMDIG.m_uEquMask) ? BST_CHECKED : BST_UNCHECKED);
	    ::CheckDlgButton (hPage, IDC_SPMDIG_EQU_MSCDIAFR_INC, (IRRPMDIG::EquMask_MDiafr & m_SetupData.m_SetupMDIG.m_uEquMask) ? BST_CHECKED : BST_UNCHECKED);
	    ::CheckDlgButton (hPage, IDC_SPMDIG_EQU_MSCSCAN_INC , (IRRPMDIG::EquMask_MScan  & m_SetupData.m_SetupMDIG.m_uEquMask) ? BST_CHECKED : BST_UNCHECKED);
	    ::CheckDlgButton (hPage, IDC_SPMDIG_EQU_MSCFG_INC	, (IRRPMDIG::EquMask_MFG    & m_SetupData.m_SetupMDIG.m_uEquMask) ? BST_CHECKED : BST_UNCHECKED);
	    ::CheckDlgButton (hPage, IDC_SPMDIG_EQU_MM2_INC	, (IRRPMDIG::EquMask_MM2    & m_SetupData.m_SetupMDIG.m_uEquMask) ? BST_CHECKED : BST_UNCHECKED);
	}}
	else
	{
	    * (pBuf + 0) = '\"';
	    * (pBuf + 1) = '\0';
	    ::GetDlgItemText (hPage, IDC_SPMDIG_SERIALNUM_EDT	, pBuf + 1, sizeof (pBuf) - 1);

	    * (pBuf + (n = lstrlen (pBuf))) = '\"';
	    * (pBuf +  n + 1		  ) = '\0';

	    SetSettingsValue (IRRPSettings::ID_MDIG_SerialNum, pBuf);

	    SetSettingsValue (IRRPSettings::ID_MDIG_EquMask,   m_SetupData.m_SetupMDIG.m_uEquMask);

	}
	//
	// ---------------------------------------------------------------------------------------------

	if (bSave && m_pIRRPSettings)
	    m_pIRRPSettings ->Checkin (IRRPSettings::CLSID_MDIG);
    }}
    else
    if (1 == nSetupPager)
    {
	// Page 0 :
	//
	::SendMessage (m_hSetupPager, TCM_GETITEM, (WPARAM) 0, (LPARAM) & ti); hPage = (HWND) ti.lParam;
	if (! bSave)
	{
	    GetSettingsValue (IRRPSettings::ID_URP_SF_UAnodeMin, & m_SetupData.m_SetupUrp.m_SF_UAnodeMin);
	    GetSettingsValue (IRRPSettings::ID_URP_SF_UAnodeMax, & m_SetupData.m_SetupUrp.m_SF_UAnodeMax);
	    GetSettingsValue (IRRPSettings::ID_URP_SF_IFilamentPreMin, 
								 & m_SetupData.m_SetupUrp.m_SF_IFilamentPreMin);
	    GetSettingsValue (IRRPSettings::ID_URP_SF_IFilamentPreMax, 
								 & m_SetupData.m_SetupUrp.m_SF_IFilamentPreMax);
	    GetSettingsValue (IRRPSettings::ID_URP_SF_IFilamentPreDef, 
								 & m_SetupData.m_SetupUrp.m_SF_IFilamentPreDef);
	    GetSettingsValue (IRRPSettings::ID_URP_SF_IFilamentMax, 
								 & m_SetupData.m_SetupUrp.m_SF_IFilamentMax);

	    GetSettingsValue (IRRPSettings::ID_URP_BF_UAnodeMin, & m_SetupData.m_SetupUrp.m_BF_UAnodeMin);
	    GetSettingsValue (IRRPSettings::ID_URP_BF_UAnodeMax, & m_SetupData.m_SetupUrp.m_BF_UAnodeMax);
	    GetSettingsValue (IRRPSettings::ID_URP_BF_IFilamentPreMin, 
								 & m_SetupData.m_SetupUrp.m_BF_IFilamentPreMin);
	    GetSettingsValue (IRRPSettings::ID_URP_BF_IFilamentPreMax, 
								 & m_SetupData.m_SetupUrp.m_BF_IFilamentPreMax);
	    GetSettingsValue (IRRPSettings::ID_URP_BF_IFilamentPreDef, 
								 & m_SetupData.m_SetupUrp.m_BF_IFilamentPreDef);
	    GetSettingsValue (IRRPSettings::ID_URP_BF_IFilamentMax, 
								 & m_SetupData.m_SetupUrp.m_BF_IFilamentMax);

	    ::SetDlgItemInt  (hPage, IDC_SPURP_SF_UANODE_MIN,	 m_SetupData.m_SetupUrp.m_SF_UAnodeMin, False);
	    ::SetDlgItemInt  (hPage, IDC_SPURP_SF_UANODE_MAX,	 m_SetupData.m_SetupUrp.m_SF_UAnodeMax, False);
	    ::SetDlgItemInt  (hPage, IDC_SPURP_SF_IFILAMENTPRE_MIN, 
								 m_SetupData.m_SetupUrp.m_SF_IFilamentPreMin, False);
	    ::SetDlgItemInt  (hPage, IDC_SPURP_SF_IFILAMENTPRE_MAX,
								 m_SetupData.m_SetupUrp.m_SF_IFilamentPreMax, False);
	    ::SetDlgItemInt  (hPage, IDC_SPURP_SF_IFILAMENTPRE , m_SetupData.m_SetupUrp.m_SF_IFilamentPreDef, False);
	    ::SetDlgItemInt  (hPage, IDC_SPURP_SF_IFILAMENT_MIN, m_SetupData.m_SetupUrp.m_SF_IFilamentPreMax, False);
	    ::SetDlgItemInt  (hPage, IDC_SPURP_SF_IFILAMENT_MAX, m_SetupData.m_SetupUrp.m_SF_IFilamentMax   , False);
	    
	    ::SetDlgItemInt  (hPage, IDC_SPURP_BF_UANODE_MIN, m_SetupData.m_SetupUrp.m_BF_UAnodeMin, False);
	    ::SetDlgItemInt  (hPage, IDC_SPURP_BF_UANODE_MAX, m_SetupData.m_SetupUrp.m_BF_UAnodeMax, False);

	    ::SetDlgItemInt  (hPage, IDC_SPURP_BF_IFILAMENTPRE_MIN, 
								 m_SetupData.m_SetupUrp.m_BF_IFilamentPreMin, False);
	    ::SetDlgItemInt  (hPage, IDC_SPURP_BF_IFILAMENTPRE_MAX,
								 m_SetupData.m_SetupUrp.m_BF_IFilamentPreMax, False);
	    ::SetDlgItemInt  (hPage, IDC_SPURP_BF_IFILAMENTPRE , m_SetupData.m_SetupUrp.m_BF_IFilamentPreDef, False);
	    ::SetDlgItemInt  (hPage, IDC_SPURP_BF_IFILAMENT_MIN, m_SetupData.m_SetupUrp.m_BF_IFilamentPreMax, False);
	    ::SetDlgItemInt  (hPage, IDC_SPURP_BF_IFILAMENT_MAX, m_SetupData.m_SetupUrp.m_BF_IFilamentMax   , False);

	    GetSettingsValue (IRRPSettings::ID_MDIG_RadOutput,	 pBuf, sizeof (pBuf));
	    for (int i = 1; i ++; ) { if (* (pBuf + i) == '"') { * (pBuf + i) = 0; break; } }
	    ::SetDlgItemText (hPage, IDC_SPURP_RDOUTPUT	       ,  pBuf + 1);
	}
	else
	{
	    SetSettingsValue (IRRPSettings::ID_URP_SF_UAnodeMin,   m_SetupData.m_SetupUrp.m_SF_UAnodeMin);
	    SetSettingsValue (IRRPSettings::ID_URP_SF_UAnodeMax,   m_SetupData.m_SetupUrp.m_SF_UAnodeMax);
	    SetSettingsValue (IRRPSettings::ID_URP_SF_IFilamentPreMin, 
								   m_SetupData.m_SetupUrp.m_SF_IFilamentPreMin);
	    SetSettingsValue (IRRPSettings::ID_URP_SF_IFilamentPreMax, 
								   m_SetupData.m_SetupUrp.m_SF_IFilamentPreMax);
	    SetSettingsValue (IRRPSettings::ID_URP_SF_IFilamentPreDef, 
								   m_SetupData.m_SetupUrp.m_SF_IFilamentPreDef);
	    SetSettingsValue (IRRPSettings::ID_URP_SF_IFilamentMax, 
								   m_SetupData.m_SetupUrp.m_SF_IFilamentMax);

	    SetSettingsValue (IRRPSettings::ID_URP_BF_UAnodeMin,   m_SetupData.m_SetupUrp.m_BF_UAnodeMin);
	    SetSettingsValue (IRRPSettings::ID_URP_BF_UAnodeMax,   m_SetupData.m_SetupUrp.m_BF_UAnodeMax);
	    SetSettingsValue (IRRPSettings::ID_URP_BF_IFilamentPreMin, 
								   m_SetupData.m_SetupUrp.m_BF_IFilamentPreMin);
	    SetSettingsValue (IRRPSettings::ID_URP_BF_IFilamentPreMax, 
								   m_SetupData.m_SetupUrp.m_BF_IFilamentPreMax);
	    SetSettingsValue (IRRPSettings::ID_URP_BF_IFilamentPreDef, 
								   m_SetupData.m_SetupUrp.m_BF_IFilamentPreDef);
	    SetSettingsValue (IRRPSettings::ID_URP_BF_IFilamentMax, 
								   m_SetupData.m_SetupUrp.m_BF_IFilamentMax);
	}
	//
	// ---------------------------------------------------------------------------------------------

	// Page 1 :
	//
	::SendMessage (m_hSetupPager, TCM_GETITEM, (WPARAM) 1, (LPARAM) & ti); hPage = (HWND) ti.lParam;
	if (! bSave)
	{
	    GetSettingsValue (IRRPSettings::ID_URP_UForceMin,	 & m_SetupData.m_SetupUrp.m_UForceMin);
	    GetSettingsValue (IRRPSettings::ID_URP_TForceMax,	 & m_SetupData.m_SetupUrp.m_TForceMax);
	    GetSettingsValue (IRRPSettings::ID_URP_TTubeMax,	 & m_SetupData.m_SetupUrp.m_TTubeMax);

	    GetSettingsValue (IRRPSettings::ID_URP_HeatForceLo,	 & m_SetupData.m_SetupUrp.m_HeatForceLo);
	    GetSettingsValue (IRRPSettings::ID_URP_HeatForceHi,	 & m_SetupData.m_SetupUrp.m_HeatForceHi);
	    GetSettingsValue (IRRPSettings::ID_URP_HeatForceK,	 & m_SetupData.m_SetupUrp.m_HeatForceK);

	    GetSettingsValue (IRRPSettings::ID_URP_HeatTubeLo,	 & m_SetupData.m_SetupUrp.m_HeatTubeLo);
	    GetSettingsValue (IRRPSettings::ID_URP_HeatTubeHi,	 & m_SetupData.m_SetupUrp.m_HeatTubeHi);
	    GetSettingsValue (IRRPSettings::ID_URP_HeatTubeK,	 & m_SetupData.m_SetupUrp.m_HeatTubeK);

	    GetSettingsValue (IRRPSettings::ID_URP_HeatDelay,	 & m_SetupData.m_SetupUrp.m_HeatDelay);

	    ::SetDlgItemInt  (hPage, IDC_SPURP_LIM_UFORCE,	 m_SetupData.m_SetupUrp.m_UForceMin, False);
	    ::SetDlgItemInt  (hPage, IDC_SPURP_LIM_TFORCE,	 m_SetupData.m_SetupUrp.m_TForceMax, False);
	    ::SetDlgItemInt  (hPage, IDC_SPURP_LIM_TTUBE,	 m_SetupData.m_SetupUrp.m_TTubeMax , False);

	    ::SetDlgItemInt  (hPage, IDC_SPURP_LIM_PFORCE_LO,	 m_SetupData.m_SetupUrp.m_HeatForceLo, False);
	    ::SetDlgItemInt  (hPage, IDC_SPURP_LIM_PFORCE_HI,	 m_SetupData.m_SetupUrp.m_HeatForceHi, False);
	    ::SetDlgItemInt  (hPage, IDC_SPURP_LIM_PFORCE_K,	 m_SetupData.m_SetupUrp.m_HeatForceK , False);

	    ::SetDlgItemInt  (hPage, IDC_SPURP_LIM_PTUBE_LO,	 m_SetupData.m_SetupUrp.m_HeatTubeLo , False);
	    ::SetDlgItemInt  (hPage, IDC_SPURP_LIM_PTUBE_HI,	 m_SetupData.m_SetupUrp.m_HeatTubeHi , False);
	    ::SetDlgItemInt  (hPage, IDC_SPURP_LIM_PTUBE_K,	 m_SetupData.m_SetupUrp.m_HeatTubeK  , False);

	    ::SetDlgItemInt  (hPage, IDC_SPURP_HEAT_DELAY,	 m_SetupData.m_SetupUrp.m_HeatDelay, False);
	}
	else
	{
	    SetSettingsValue (IRRPSettings::ID_URP_UForceMin,	   m_SetupData.m_SetupUrp.m_UForceMin);
	    SetSettingsValue (IRRPSettings::ID_URP_TForceMax,	   m_SetupData.m_SetupUrp.m_TForceMax);
	    SetSettingsValue (IRRPSettings::ID_URP_TTubeMax,	   m_SetupData.m_SetupUrp.m_TTubeMax);

	    SetSettingsValue (IRRPSettings::ID_URP_HeatForceLo,	   m_SetupData.m_SetupUrp.m_HeatForceLo);
	    SetSettingsValue (IRRPSettings::ID_URP_HeatForceHi,	   m_SetupData.m_SetupUrp.m_HeatForceHi);
	    SetSettingsValue (IRRPSettings::ID_URP_HeatForceK,	   m_SetupData.m_SetupUrp.m_HeatForceK);

	    SetSettingsValue (IRRPSettings::ID_URP_HeatTubeLo,	   m_SetupData.m_SetupUrp.m_HeatTubeLo);
	    SetSettingsValue (IRRPSettings::ID_URP_HeatTubeHi,	   m_SetupData.m_SetupUrp.m_HeatTubeHi);
	    SetSettingsValue (IRRPSettings::ID_URP_HeatTubeK,	   m_SetupData.m_SetupUrp.m_HeatTubeK);

	    SetSettingsValue (IRRPSettings::ID_URP_HeatDelay,	   m_SetupData.m_SetupUrp.m_HeatDelay);
	}
	//
	// ---------------------------------------------------------------------------------------------

	// Page 2 :
	//
	::SendMessage (m_hSetupPager, TCM_GETITEM, (WPARAM) 2, (LPARAM) & ti); hPage = (HWND) ti.lParam;
	if (! bSave)
	{
	    GetSettingsValue (IRRPSettings::ID_URP_UACorrection ,& m_SetupData.m_SetupUrp.m_UAnodeCorrection);
	    ::CheckDlgButton (hPage, IDC_URP_UACORRECTION_CHK	, (m_SetupData.m_SetupUrp.m_UAnodeCorrection) ? BST_CHECKED : BST_UNCHECKED);
	    GetSettingsValue (IRRPSettings::ID_URP_IARetention	,& m_SetupData.m_SetupUrp.m_IAnodeRetention );
	    ::CheckDlgButton (hPage, IDC_URP_IARETENTION_CHK	, (m_SetupData.m_SetupUrp.m_IAnodeRetention ) ? BST_CHECKED : BST_UNCHECKED);
	}
	else
	{
	    SetSettingsValue (IRRPSettings::ID_URP_UACorrection ,  m_SetupData.m_SetupUrp.m_UAnodeCorrection);
	    SetSettingsValue (IRRPSettings::ID_URP_IARetention  ,  m_SetupData.m_SetupUrp.m_IAnodeRetention );
	}
	//
	// ---------------------------------------------------------------------------------------------

	if (bSave && m_pIRRPSettings)
	    m_pIRRPSettings ->Checkin (IRRPSettings::CLSID_URP);
    }
    else
    if (2 == nSetupPager)
    {

	// Page 0 :
	//
	::SendMessage (m_hSetupPager, TCM_GETITEM, (WPARAM) 0, (LPARAM) & ti); hPage = (HWND) ti.lParam;
	if (! bSave)
	{{
	    GetSettingsValue (IRRPSettings::ID_MSC_Diafr_SyncDelay,  & m_SetupData.m_SetupMSC.m_Diafr_SyncDelay);
	    GetSettingsValue (IRRPSettings::ID_MSC_Diafr_DefCoeff,   & m_SetupData.m_SetupMSC.m_Diafr_DefCoeff);
	    GetSettingsValue (IRRPSettings::ID_MSC_Diafr_AccelTime,  & m_SetupData.m_SetupMSC.m_Diafr_AccelTime);
	    GetSettingsValue (IRRPSettings::ID_MSC_Diafr_Velocity,   & m_SetupData.m_SetupMSC.m_Diafr_Velocity);
	    GetSettingsValue (IRRPSettings::ID_MSC_Diafr_StepPerDec, & m_SetupData.m_SetupMSC.m_Diafr_StepPerDec);

	    GetSettingsValue (IRRPSettings::ID_MSC_Scan_SyncDelay,   & m_SetupData.m_SetupMSC.m_Scan_SyncDelay);
	    GetSettingsValue (IRRPSettings::ID_MSC_Scan_DefCoeff,    & m_SetupData.m_SetupMSC.m_Scan_DefCoeff);
	    GetSettingsValue (IRRPSettings::ID_MSC_Scan_AccelTime,   & m_SetupData.m_SetupMSC.m_Scan_AccelTime);
	    GetSettingsValue (IRRPSettings::ID_MSC_Scan_Velocity,    & m_SetupData.m_SetupMSC.m_Scan_Velocity);
	    GetSettingsValue (IRRPSettings::ID_MSC_Scan_StepPerDec,  & m_SetupData.m_SetupMSC.m_Scan_StepPerDec);

	    ::SetDlgItemInt  (hPage, IDC_MMSC_PARAM_DDELAY_EDT,	    m_SetupData.m_SetupMSC.m_Diafr_SyncDelay, False);
	    ::SetDlgItemInt  (hPage, IDC_MMSC_PARAM_DDEFCOEFF_EDT,  m_SetupData.m_SetupMSC.m_Diafr_DefCoeff, False);
	    ::SetDlgItemInt  (hPage, IDC_MMSC_PARAM_DACCELTIME_EDT, m_SetupData.m_SetupMSC.m_Diafr_AccelTime, False);
	    ::SetDlgItemInt  (hPage, IDC_MMSC_PARAM_DV_EDT,	    m_SetupData.m_SetupMSC.m_Diafr_Velocity, False);
	    ::SetDlgItemInt  (hPage, IDC_MMSC_PARAM_DSTEPPERDEC_EDT,m_SetupData.m_SetupMSC.m_Diafr_StepPerDec, False);

	    ::SetDlgItemInt  (hPage, IDC_MMSC_PARAM_SDELAY_EDT,	    m_SetupData.m_SetupMSC.m_Scan_SyncDelay, False);
	    ::SetDlgItemInt  (hPage, IDC_MMSC_PARAM_SDEFCOEFF_EDT,  m_SetupData.m_SetupMSC.m_Scan_DefCoeff, False);
	    ::SetDlgItemInt  (hPage, IDC_MMSC_PARAM_SACCELTIME_EDT, m_SetupData.m_SetupMSC.m_Scan_AccelTime, False);
	    ::SetDlgItemInt  (hPage, IDC_MMSC_PARAM_SV_EDT,	    m_SetupData.m_SetupMSC.m_Scan_Velocity, False);
	    ::SetDlgItemInt  (hPage, IDC_MMSC_PARAM_SSTEPPERDEC_EDT,m_SetupData.m_SetupMSC.m_Scan_StepPerDec, False);

	    DWord   dVersion;

	    GetSettingsValue (IRRPSettings::ID_MSC_Version	  ,  & dVersion);

	    ::SendDlgItemMessage 
			     (hPage, IDC_MMSC_VERSION, CB_SETCURSEL, 
			     (WPARAM) (HIBYTE((LOWORD(dVersion))) - 1), (LPARAM) 0);

	    GetSettingsValue (IRRPSettings::ID_MSC_Scan_CenterLine,  & m_SetupData.m_SetupMSC.m_Scan_CenterLine);
	    GetSettingsValue (IRRPSettings::ID_MSC_Scan_12_Param  ,  & m_SetupData.m_SetupMSC.m_Scan_12_Param);
	    GetSettingsValue (IRRPSettings::ID_MSC_Scan_23_Param  ,  & m_SetupData.m_SetupMSC.m_Scan_23_Param);

	    ::SetDlgItemInt  (hPage, IDC_MMSC_IMG_CENTERLINE_EDT,      m_SetupData.m_SetupMSC.m_Scan_CenterLine, False);
	    ::SetDlgItemInt  (hPage, IDC_MMSC_IMG12_BADNUM_EDT	,
							       HIWORD (m_SetupData.m_SetupMSC.m_Scan_12_Param), False);
	    ::SetDlgItemInt  (hPage, IDC_MMSC_IMG12_INSERT_EDT	,
							       LOWORD (m_SetupData.m_SetupMSC.m_Scan_12_Param), False);
	    ::SetDlgItemInt  (hPage, IDC_MMSC_IMG23_BADNUM_EDT	,
							       HIWORD (m_SetupData.m_SetupMSC.m_Scan_23_Param), False);
	    ::SetDlgItemInt  (hPage, IDC_MMSC_IMG23_INSERT_EDT	,
							       LOWORD (m_SetupData.m_SetupMSC.m_Scan_23_Param), False);

	    GetSettingsValue (IRRPSettings::ID_URP_DelayParam_Full,
								 & m_SetupData.m_SetupUrp.m_DelayParam_Full);
	    GetSettingsValue (IRRPSettings::ID_URP_DelayParam_21x30,
								 & m_SetupData.m_SetupUrp.m_DelayParam_21x30);
	    GetSettingsValue (IRRPSettings::ID_URP_DelayParam_18x24,
								 & m_SetupData.m_SetupUrp.m_DelayParam_18x24);
	    GetSettingsValue (IRRPSettings::ID_URP_DelayParam_12x12,
								 & m_SetupData.m_SetupUrp.m_DelayParam_12x12);

	    ::SetDlgItemInt  (hPage, IDC_SPURP_HVDELAY_FULL,	   HIWORD(m_SetupData.m_SetupUrp.m_DelayParam_Full), False);
	    ::SetDlgItemInt  (hPage, IDC_SPURP_HVLENGTH_FULL,	   LOWORD(m_SetupData.m_SetupUrp.m_DelayParam_Full), False);
	    
	    ::SetDlgItemInt  (hPage, IDC_SPURP_HVDELAY_21X30,	   HIWORD(m_SetupData.m_SetupUrp.m_DelayParam_21x30), False);
	    ::SetDlgItemInt  (hPage, IDC_SPURP_HVLENGTH_21X30,	   LOWORD(m_SetupData.m_SetupUrp.m_DelayParam_21x30), False);

	    ::SetDlgItemInt  (hPage, IDC_SPURP_HVDELAY_18X24,	   HIWORD(m_SetupData.m_SetupUrp.m_DelayParam_18x24), False);
	    ::SetDlgItemInt  (hPage, IDC_SPURP_HVLENGTH_18X24,	   LOWORD(m_SetupData.m_SetupUrp.m_DelayParam_18x24), False);

	    ::SetDlgItemInt  (hPage, IDC_SPURP_HVDELAY_12X12,	   HIWORD(m_SetupData.m_SetupUrp.m_DelayParam_12x12), False);
	    ::SetDlgItemInt  (hPage, IDC_SPURP_HVLENGTH_12X12,	   LOWORD(m_SetupData.m_SetupUrp.m_DelayParam_12x12), False);
	}}
	else
	{
	    SetSettingsValue (IRRPSettings::ID_MSC_Diafr_SyncDelay,    m_SetupData.m_SetupMSC.m_Diafr_SyncDelay);
	    SetSettingsValue (IRRPSettings::ID_MSC_Diafr_DefCoeff,     m_SetupData.m_SetupMSC.m_Diafr_DefCoeff);
	    SetSettingsValue (IRRPSettings::ID_MSC_Diafr_AccelTime,    m_SetupData.m_SetupMSC.m_Diafr_AccelTime);
	    SetSettingsValue (IRRPSettings::ID_MSC_Diafr_Velocity,     m_SetupData.m_SetupMSC.m_Diafr_Velocity);
	    SetSettingsValue (IRRPSettings::ID_MSC_Diafr_StepPerDec,   m_SetupData.m_SetupMSC.m_Diafr_StepPerDec);

	    SetSettingsValue (IRRPSettings::ID_MSC_Scan_SyncDelay,     m_SetupData.m_SetupMSC.m_Scan_SyncDelay);
	    SetSettingsValue (IRRPSettings::ID_MSC_Scan_DefCoeff,      m_SetupData.m_SetupMSC.m_Scan_DefCoeff);
	    SetSettingsValue (IRRPSettings::ID_MSC_Scan_AccelTime,     m_SetupData.m_SetupMSC.m_Scan_AccelTime);
	    SetSettingsValue (IRRPSettings::ID_MSC_Scan_Velocity,      m_SetupData.m_SetupMSC.m_Scan_Velocity);
	    SetSettingsValue (IRRPSettings::ID_MSC_Scan_StepPerDec,    m_SetupData.m_SetupMSC.m_Scan_StepPerDec);

	    SetSettingsValue (IRRPSettings::ID_MSC_Scan_CenterLine,    m_SetupData.m_SetupMSC.m_Scan_CenterLine);
	    SetSettingsValue (IRRPSettings::ID_MSC_Scan_12_Param  ,    m_SetupData.m_SetupMSC.m_Scan_12_Param);
	    SetSettingsValue (IRRPSettings::ID_MSC_Scan_23_Param  ,    m_SetupData.m_SetupMSC.m_Scan_23_Param);

	    SetSettingsValue (IRRPSettings::ID_URP_DelayParam_Full,
								   m_SetupData.m_SetupUrp.m_DelayParam_Full);
	    SetSettingsValue (IRRPSettings::ID_URP_DelayParam_21x30,
								   m_SetupData.m_SetupUrp.m_DelayParam_21x30);
	    SetSettingsValue (IRRPSettings::ID_URP_DelayParam_18x24,
								   m_SetupData.m_SetupUrp.m_DelayParam_18x24);
	    SetSettingsValue (IRRPSettings::ID_URP_DelayParam_12x12,
								   m_SetupData.m_SetupUrp.m_DelayParam_12x12);
	}

        ::EnableWindow (GetDlgItem (hPage, IDC_MMSC_VERSION), True);
	//
	// ---------------------------------------------------------------------------------------------

	// Page 1 :
	//
	::SendMessage (m_hSetupPager, TCM_GETITEM, (WPARAM) 1, (LPARAM) & ti); hPage = (HWND) ti.lParam;

	if (! bSave)
	{{
	    GetSettingsValue (IRRPSettings::ID_MSC_ImageProcMask  ,  & m_SetupData.m_SetupMSC.m_Scan_ImageProcMask);

	    ::CheckDlgButton (hPage, IDC_MMSC_IMG_REPAIR_CHK	, (0x80 & m_SetupData.m_SetupMSC.m_Scan_ImageProcMask) ? BST_CHECKED : BST_UNCHECKED);
	    ::CheckDlgButton (hPage, IDC_MMSC_IMG_DUPLICATE_CHK , (0x04 & m_SetupData.m_SetupMSC.m_Scan_ImageProcMask) ? BST_CHECKED : BST_UNCHECKED);
	    ::CheckDlgButton (hPage, IDC_MMSC_IMG_DOWHITE_CHK	, (0x02 & m_SetupData.m_SetupMSC.m_Scan_ImageProcMask) ? BST_CHECKED : BST_UNCHECKED);
	    ::CheckDlgButton (hPage, IDC_MMSC_IMG_DOADAPT_CHK	, (0x01 & m_SetupData.m_SetupMSC.m_Scan_ImageProcMask) ? BST_CHECKED : BST_UNCHECKED);

	    ::CheckDlgButton (hPage, IDC_MM2_ROTLPIN_RAD  , (-2 == m_iDetCalibSet) ? BST_CHECKED : BST_UNCHECKED);
	    ::CheckDlgButton (hPage, IDC_MM2_ROTL45PIN_RAD, (-1 == m_iDetCalibSet) ? BST_CHECKED : BST_UNCHECKED);
	    ::CheckDlgButton (hPage, IDC_MM2_ROTMPIN_RAD  , ( 0 == m_iDetCalibSet) ? BST_CHECKED : BST_UNCHECKED);
	    ::CheckDlgButton (hPage, IDC_MM2_ROTR45PIN_RAD, ( 1 == m_iDetCalibSet) ? BST_CHECKED : BST_UNCHECKED);
	    ::CheckDlgButton (hPage, IDC_MM2_ROTRPIN_RAD  , ( 2 == m_iDetCalibSet) ? BST_CHECKED : BST_UNCHECKED);
	}}
	else
	{
	    SetSettingsValue (IRRPSettings::ID_MSC_ImageProcMask  ,    m_SetupData.m_SetupMSC.m_Scan_ImageProcMask);
	}
	//
	// ---------------------------------------------------------------------------------------------

	if (bSave && m_pIRRPSettings)
	{
	    m_pIRRPSettings ->Checkin (IRRPSettings::CLSID_URP);
	    m_pIRRPSettings ->Checkin (IRRPSettings::CLSID_MSC);
	}
    }
};

Bool CMainPanel::On_Setup_MDIG_Page0 (GWinMsg & m)
{
    if (m_hEdit.On_ReflectWinMsg (m))
    {
	switch (m.LowParam)
	{
	case IDC_SPMDIG_SERIALNUM_EDT :
	    return (On_SetupChanged (), True);
	};
    }

    int	        nCtrlId;

    if ((0 !=  (nCtrlId = m.IsCommand ())) && (BN_CLICKED == m.HiwParam))
    {
	switch (nCtrlId)
	{
	case IDC_SPMDIG_EQU_URP_INC	 :
	case IDC_SPMDIG_EQU_URPKKM_INC	 :
	case IDC_SPMDIG_EQU_MSCDIAFR_INC :
	case IDC_SPMDIG_EQU_MSCSCAN_INC  :
	case IDC_SPMDIG_EQU_MSCFG_INC	 :
	case IDC_SPMDIG_EQU_MM2_INC	 :

		     ::CheckDlgButton     (m.hWnd, IDC_SPMDIG_EQU_MSCFG_INC, 
((BST_CHECKED     == ::IsDlgButtonChecked (m.hWnd, IDC_SPMDIG_EQU_MSCSCAN_INC))) ? BST_CHECKED : BST_UNCHECKED);

if (BST_UNCHECKED == ::IsDlgButtonChecked (m.hWnd, IDC_SPMDIG_EQU_URP_INC))
{
		     ::CheckDlgButton	  (m.hWnd, IDC_SPMDIG_EQU_URPKKM_INC, BST_UNCHECKED);
}
	    m_SetupData.m_SetupMDIG.m_uEquMask  = 0;
	    m_SetupData.m_SetupMDIG.m_uEquMask |= (BST_CHECKED == ::IsDlgButtonChecked (m.hWnd, IDC_SPMDIG_EQU_URP_INC	    )) ? IRRPMDIG::EquMask_Urp	  : 0x00;
	    m_SetupData.m_SetupMDIG.m_uEquMask |= (BST_CHECKED == ::IsDlgButtonChecked (m.hWnd, IDC_SPMDIG_EQU_URPKKM_INC   )) ? IRRPMDIG::EquMask_KKM	  : 0x00;
	    m_SetupData.m_SetupMDIG.m_uEquMask |= (BST_CHECKED == ::IsDlgButtonChecked (m.hWnd, IDC_SPMDIG_EQU_MSCDIAFR_INC )) ? IRRPMDIG::EquMask_MDiafr : 0x00;
	    m_SetupData.m_SetupMDIG.m_uEquMask |= (BST_CHECKED == ::IsDlgButtonChecked (m.hWnd, IDC_SPMDIG_EQU_MSCSCAN_INC  )) ? IRRPMDIG::EquMask_MScan  : 0x00;
	    m_SetupData.m_SetupMDIG.m_uEquMask |= (BST_CHECKED == ::IsDlgButtonChecked (m.hWnd, IDC_SPMDIG_EQU_MSCFG_INC    )) ? IRRPMDIG::EquMask_MFG    : 0x00;
	    m_SetupData.m_SetupMDIG.m_uEquMask |= (BST_CHECKED == ::IsDlgButtonChecked (m.hWnd, IDC_SPMDIG_EQU_MM2_INC	    )) ? IRRPMDIG::EquMask_MM2    : 0x00;

	    On_SetupChanged ();

	    break;
	};
    }
	return False;
};

static void ValidateEdtCtrl (_U32 & nValue, HWND hPage, int nCtrlId, _U32 nMinValue, _U32 nMaxValue)
{
    Bool    bCorrect	= False;
    TCHAR   sBuf [64];

    ::GetWindowText (::GetDlgItem (hPage, nCtrlId), sBuf, sizeof (sBuf) / sizeof (TCHAR));

    if (0 == sscanf (sBuf, "%d", & nValue))
    {	    nValue = nMinValue; bCorrect = True;}
    else
    {
	if (nValue < nMinValue)
	{   nValue = nMinValue; bCorrect = True;}
	if (nValue > nMaxValue)
	{   nValue = nMaxValue; bCorrect = True;}
    }

    if (bCorrect)
	::SetDlgItemInt (hPage, nCtrlId, nValue, False);
};

Bool CMainPanel::On_Setup_URP_Page0 (GWinMsg & m)
{
    if (m_hEdit.On_ReflectWinMsg (m))
    {
	switch (m.LowParam)
	{
	case IDC_SPURP_SF_UANODE_MIN :
	    ValidateEdtCtrl (m_SetupData.m_SetupUrp.m_SF_UAnodeMin, m.hWnd, m.LowParam, 0, 120);
	    return (On_SetupChanged (), True);
	case IDC_SPURP_SF_UANODE_MAX :
	    ValidateEdtCtrl (m_SetupData.m_SetupUrp.m_SF_UAnodeMax, m.hWnd, m.LowParam, 0, 120);
	    return (On_SetupChanged (), True);
	case IDC_SPURP_SF_IFILAMENTPRE_MIN :
	    ValidateEdtCtrl (m_SetupData.m_SetupUrp.m_SF_IFilamentPreMin, m.hWnd, m.LowParam, 0, 1023);
	    return (On_SetupChanged (), True);	
	case IDC_SPURP_SF_IFILAMENTPRE_MAX :
	    ValidateEdtCtrl (m_SetupData.m_SetupUrp.m_SF_IFilamentPreMax, m.hWnd, m.LowParam, 0, 1023);
	    ::SetDlgItemInt (m.hWnd, IDC_SPURP_SF_IFILAMENT_MIN,
			     m_SetupData.m_SetupUrp.m_SF_IFilamentPreMax, False);
	    return (On_SetupChanged (), True);	
	case IDC_SPURP_SF_IFILAMENTPRE :
	    ValidateEdtCtrl (m_SetupData.m_SetupUrp.m_SF_IFilamentPreDef, m.hWnd, m.LowParam, 0, 1023);
	    return (On_SetupChanged (), True);
	case IDC_SPURP_SF_IFILAMENT_MAX :
	    ValidateEdtCtrl (m_SetupData.m_SetupUrp.m_SF_IFilamentMax,	  m.hWnd, m.LowParam, 0, 1023);
	    return (On_SetupChanged (), True);	

	case IDC_SPURP_BF_UANODE_MIN :
	    ValidateEdtCtrl (m_SetupData.m_SetupUrp.m_BF_UAnodeMin, m.hWnd, m.LowParam, 0, 120);
	    return (On_SetupChanged (), True);
	case IDC_SPURP_BF_UANODE_MAX :
	    ValidateEdtCtrl (m_SetupData.m_SetupUrp.m_BF_UAnodeMax, m.hWnd, m.LowParam, 0, 120);
	    return (On_SetupChanged (), True);
	case IDC_SPURP_BF_IFILAMENTPRE_MIN :
	    ValidateEdtCtrl (m_SetupData.m_SetupUrp.m_BF_IFilamentPreMin, m.hWnd, m.LowParam, 0, 1023);
	    return (On_SetupChanged (), True);	
	case IDC_SPURP_BF_IFILAMENTPRE_MAX :
	    ValidateEdtCtrl (m_SetupData.m_SetupUrp.m_BF_IFilamentPreMax, m.hWnd, m.LowParam, 0, 1023);
	    ::SetDlgItemInt (m.hWnd, IDC_SPURP_BF_IFILAMENT_MIN,
			     m_SetupData.m_SetupUrp.m_BF_IFilamentPreMax, False);
	    return (On_SetupChanged (), True);	
	case IDC_SPURP_BF_IFILAMENTPRE :
	    ValidateEdtCtrl (m_SetupData.m_SetupUrp.m_BF_IFilamentPreDef, m.hWnd, m.LowParam, 0, 1023);
	    return (On_SetupChanged (), True);
	case IDC_SPURP_BF_IFILAMENT_MAX :
	    ValidateEdtCtrl (m_SetupData.m_SetupUrp.m_BF_IFilamentMax,	  m.hWnd, m.LowParam, 0, 1023);
	    return (On_SetupChanged (), True);	
	    break;
	};
    }
	return False;
};

Bool CMainPanel::On_Setup_URP_Page1 (GWinMsg & m)
{
    if (m_hEdit.On_ReflectWinMsg (m))
    {
	switch (m.LowParam)
	{
	case IDC_SPURP_LIM_UFORCE   :
	    ValidateEdtCtrl (m_SetupData.m_SetupUrp.m_UForceMin, m.hWnd, m.LowParam, 0, 600);
	    return (On_SetupChanged (), True);
	case IDC_SPURP_LIM_TFORCE   :
	    ValidateEdtCtrl (m_SetupData.m_SetupUrp.m_TForceMax, m.hWnd, m.LowParam, 0, 200);
	    return (On_SetupChanged (), True);
	case IDC_SPURP_LIM_TTUBE    :
	    ValidateEdtCtrl (m_SetupData.m_SetupUrp.m_TTubeMax , m.hWnd, m.LowParam, 0, 200);
	    return (On_SetupChanged (), True);

	case IDC_SPURP_LIM_PFORCE_LO :
	    ValidateEdtCtrl (m_SetupData.m_SetupUrp.m_HeatForceLo, m.hWnd, m.LowParam, 0, 1000);
	    return (On_SetupChanged (), True);
	case IDC_SPURP_LIM_PFORCE_HI :
	    ValidateEdtCtrl (m_SetupData.m_SetupUrp.m_HeatForceHi, m.hWnd, m.LowParam, 0, 1000);
	    return (On_SetupChanged (), True);
	case IDC_SPURP_LIM_PFORCE_K  :
	    ValidateEdtCtrl (m_SetupData.m_SetupUrp.m_HeatForceK,  m.hWnd, m.LowParam, 0, 1000);
	    return (On_SetupChanged (), True);

	case IDC_SPURP_LIM_PTUBE_LO :
	    ValidateEdtCtrl (m_SetupData.m_SetupUrp.m_HeatTubeLo, m.hWnd, m.LowParam, 0, 1000);
	    return (On_SetupChanged (), True);
	case IDC_SPURP_LIM_PTUBE_HI :
	    ValidateEdtCtrl (m_SetupData.m_SetupUrp.m_HeatTubeHi, m.hWnd, m.LowParam, 0, 1000);
	    return (On_SetupChanged (), True);
	case IDC_SPURP_LIM_PTUBE_K  :    
	    ValidateEdtCtrl (m_SetupData.m_SetupUrp.m_HeatTubeK,  m.hWnd, m.LowParam, 0, 1000);
	    return (On_SetupChanged (), True);

	case IDC_SPURP_HEAT_DELAY :
	    ValidateEdtCtrl (m_SetupData.m_SetupUrp.m_HeatDelay,  m.hWnd, m.LowParam, 0, 1000);
	    return (On_SetupChanged (), True);
	};
    }
	return False;
};

Bool CMainPanel::On_Setup_URP_Page2 (GWinMsg & m)
{
    if ((WM_COMMAND == m.uMsg) && (BN_CLICKED == m.HiwParam) && (IDC_URP_CALIBRATE_BTN == m.LowParam))
    {{
	GRect	rw;

	::GetWindowRect (m_hSetupPager, & rw, NULL);

	On_URP_Calibrate (rw.x, rw.y, this);
	
	return True;
    }}

    int		nCtrlId;

    if ((0 !=  (nCtrlId = m.IsCommand ())) && (BN_CLICKED == m.HiwParam))
    {
	switch (nCtrlId)
	{
	case IDC_URP_UACORRECTION_CHK :
		m_SetupData.m_SetupUrp.m_UAnodeCorrection = ::IsDlgButtonChecked (m.hWnd, nCtrlId) ? 1 : 0;
		On_SetupChanged ();
	return True;
	case IDC_URP_IARETENTION_CHK :
		m_SetupData.m_SetupUrp.m_IAnodeRetention  = ::IsDlgButtonChecked (m.hWnd, nCtrlId) ? 1 : 0;
		On_SetupChanged ();
	return True;
	};
    }

	return False;
};

Bool CMainPanel::On_Setup_MSC_Page0 (GWinMsg & m)
{
    _U32    nValue   ;
    int	    nCtrlId  ;

    if (m_hEdit.On_ReflectWinMsg (m))
    {
	switch (m.LowParam)
	{
	case IDC_MMSC_PARAM_DDELAY_EDT :
	    ValidateEdtCtrl (m_SetupData.m_SetupMSC.m_Diafr_SyncDelay, m.hWnd, m.LowParam, 0, 32000);
	    return (On_SetupChanged (), True);

	case IDC_MMSC_PARAM_DDEFCOEFF_EDT :
	    ValidateEdtCtrl (m_SetupData.m_SetupMSC.m_Diafr_DefCoeff , m.hWnd, m.LowParam, 0, 32000);
	    return (On_SetupChanged (), True);

	case IDC_MMSC_PARAM_DACCELTIME_EDT :
	    ValidateEdtCtrl (m_SetupData.m_SetupMSC.m_Diafr_AccelTime, m.hWnd, m.LowParam, 0, 32000);
	    return (On_SetupChanged (), True);

	case IDC_MMSC_PARAM_DV_EDT :
	    ValidateEdtCtrl (m_SetupData.m_SetupMSC.m_Diafr_Velocity , m.hWnd, m.LowParam, 0, 32000);
	    return (On_SetupChanged (), True);

	case IDC_MMSC_PARAM_DSTEPPERDEC_EDT :
	    ValidateEdtCtrl (m_SetupData.m_SetupMSC.m_Diafr_StepPerDec, m.hWnd, m.LowParam, 0, 32000);
	    return (On_SetupChanged (), True);

	case IDC_MMSC_PARAM_SDELAY_EDT :
	    ValidateEdtCtrl (m_SetupData.m_SetupMSC.m_Scan_SyncDelay, m.hWnd, m.LowParam, 0, 32000);
	    return (On_SetupChanged (), True);

	case IDC_MMSC_PARAM_SDEFCOEFF_EDT :
	    ValidateEdtCtrl (m_SetupData.m_SetupMSC.m_Scan_DefCoeff , m.hWnd, m.LowParam, 0, 32000);
	    return (On_SetupChanged (), True);

	case IDC_MMSC_PARAM_SACCELTIME_EDT :
	    ValidateEdtCtrl (m_SetupData.m_SetupMSC.m_Scan_AccelTime, m.hWnd, m.LowParam, 0, 32000);
	    return (On_SetupChanged (), True);

	case IDC_MMSC_PARAM_SV_EDT :
	    ValidateEdtCtrl (m_SetupData.m_SetupMSC.m_Scan_Velocity , m.hWnd, m.LowParam, 0, 32000);
	    return (On_SetupChanged (), True);

	case IDC_MMSC_PARAM_SSTEPPERDEC_EDT :
	    ValidateEdtCtrl (m_SetupData.m_SetupMSC.m_Scan_StepPerDec, m.hWnd, m.LowParam, 0, 32000);
	    return (On_SetupChanged (), True);

	case IDC_MMSC_IMG12_BADNUM_EDT :
	    ValidateEdtCtrl (nValue, m.hWnd, m.LowParam, 0, 20);
	    m_SetupData.m_SetupMSC.m_Scan_12_Param = (m_SetupData.m_SetupMSC.m_Scan_12_Param & 0x0000FFFF) | (nValue << 16);
	    return (On_SetupChanged (), True);

	case IDC_MMSC_IMG12_INSERT_EDT :
	    ValidateEdtCtrl (nValue, m.hWnd, m.LowParam, 0, 20);
	    m_SetupData.m_SetupMSC.m_Scan_12_Param = (m_SetupData.m_SetupMSC.m_Scan_12_Param & 0xFFFF0000) | (nValue <<  0);
	    return (On_SetupChanged (), True);

	case IDC_MMSC_IMG23_BADNUM_EDT :
	    ValidateEdtCtrl (nValue, m.hWnd, m.LowParam, 0, 20);
	    m_SetupData.m_SetupMSC.m_Scan_23_Param = (m_SetupData.m_SetupMSC.m_Scan_23_Param & 0x0000FFFF) | (nValue << 16);
	    return (On_SetupChanged (), True);

	case IDC_MMSC_IMG23_INSERT_EDT :
	    ValidateEdtCtrl (nValue, m.hWnd, m.LowParam, 0, 20);
	    m_SetupData.m_SetupMSC.m_Scan_23_Param = (m_SetupData.m_SetupMSC.m_Scan_23_Param & 0xFFFF0000) | (nValue <<  0);
	    return (On_SetupChanged (), True);

	case IDC_SPURP_HVDELAY_FULL   :
	    ValidateEdtCtrl (nValue, m.hWnd, m.LowParam, 0, 127);
	    m_SetupData.m_SetupUrp.m_DelayParam_Full = (m_SetupData.m_SetupUrp.m_DelayParam_Full & 0x0000FFFF) | (nValue << 16);
	    return (On_SetupChanged (), True);
	case IDC_SPURP_HVLENGTH_FULL  :
	    ValidateEdtCtrl (nValue, m.hWnd, m.LowParam, 0, 127);
	    m_SetupData.m_SetupUrp.m_DelayParam_Full = (m_SetupData.m_SetupUrp.m_DelayParam_Full & 0xFFFF0000) | (nValue <<  0);
	    return (On_SetupChanged (), True);

	case IDC_SPURP_HVDELAY_21X30  :
	    ValidateEdtCtrl (nValue, m.hWnd, m.LowParam, 0, 127);
	    m_SetupData.m_SetupUrp.m_DelayParam_21x30 = (m_SetupData.m_SetupUrp.m_DelayParam_21x30 & 0x0000FFFF) | (nValue << 16);
	    return (On_SetupChanged (), True);
	case IDC_SPURP_HVLENGTH_21X30 :
	    ValidateEdtCtrl (nValue, m.hWnd, m.LowParam, 0, 127);
	    m_SetupData.m_SetupUrp.m_DelayParam_21x30 = (m_SetupData.m_SetupUrp.m_DelayParam_21x30 & 0xFFFF0000) | (nValue <<  0);
	    return (On_SetupChanged (), True);
	    
	case IDC_SPURP_HVDELAY_18X24  :
	    ValidateEdtCtrl (nValue, m.hWnd, m.LowParam, 0, 127);
	    m_SetupData.m_SetupUrp.m_DelayParam_18x24 = (m_SetupData.m_SetupUrp.m_DelayParam_18x24 & 0x0000FFFF) | (nValue << 16);
	    return (On_SetupChanged (), True);
	case IDC_SPURP_HVLENGTH_18X24 :
	    ValidateEdtCtrl (nValue, m.hWnd, m.LowParam, 0, 127);
	    m_SetupData.m_SetupUrp.m_DelayParam_18x24 = (m_SetupData.m_SetupUrp.m_DelayParam_18x24 & 0xFFFF0000) | (nValue <<  0);
	    return (On_SetupChanged (), True);

	case IDC_SPURP_HVDELAY_12X12  :
	    ValidateEdtCtrl (nValue, m.hWnd, m.LowParam, 0, 127);
	    m_SetupData.m_SetupUrp.m_DelayParam_12x12 = (m_SetupData.m_SetupUrp.m_DelayParam_12x12 & 0x0000FFFF) | (nValue << 16);
	    return (On_SetupChanged (), True);
	case IDC_SPURP_HVLENGTH_12X12 :
	    ValidateEdtCtrl (nValue, m.hWnd, m.LowParam, 0, 127);
	    m_SetupData.m_SetupUrp.m_DelayParam_12x12 = (m_SetupData.m_SetupUrp.m_DelayParam_12x12 & 0xFFFF0000) | (nValue <<  0);
	    return (On_SetupChanged (), True);
	};
    }
    else
    if ((0 !=  (nCtrlId = m.IsCommand ())) && (CBN_SELCHANGE == m.HiwParam))
    {
	switch (nCtrlId)
	{
	case IDC_MMSC_VERSION :
	{{
	    if (IDYES == ::MessageBox (GetHWND (),
	    		    "������� ��������� ?", "������ ������� ���� ��������", MB_YESNO | MB_ICONQUESTION))
	    {
		SetSettingsValue (IRRPSettings::ID_MSC_Version, (_U32)

	    ((::SendDlgItemMessage (m.hWnd, IDC_MMSC_VERSION, CB_GETCURSEL, (WPARAM) 0, (LPARAM) 0) + 1) << 8));

	    }
		DoSetupExchange	   (m_nSetupPager, False);

//		On_MSC_GetVersion  (m.hWnd);
	}}
	    return True;
	};
    }
	return False;
};

static void createCalibList (HWND hPage)
{
    HWND hList =   ::GetDlgItem	 (hPage, IDC_MMSC_CALIB_LIST);

    if (0 == (int) ::SendMessage (hList, LB_GETCOUNT, (WPARAM) 0, (LPARAM) 0))
    {
	for (int i = 0; i < sizeof (s_pMSCCalibHV) / sizeof (int); i ++)
	{
	    ::SendMessage (hList, LB_INSERTSTRING, -1, (LPARAM) "");
	    ::SendMessage (hList, LB_SETITEMDATA ,  i, (LPARAM) new FitInfo (s_pMSCCalibHV [i]));
	}
    }
};

void getFitHeader (int iDetCalibSet, Bool bFocus, FitInfo * pInfo)
{
    HANDLE hFitFile; BY_HANDLE_FILE_INFORMATION pFitFile;

    char   pFitName [MAX_PATH];

    GetNVCalibratePath (pFitName, iDetCalibSet);

    wsprintf (pFitName + strlen (pFitName), "\\%s\\%d.cff", bFocus ? "\\BF" : "\\SF", pInfo ->uHV);

    if (INVALID_HANDLE_VALUE == 

       (hFitFile = ::CreateFile (pFitName, GENERIC_READ, FILE_SHARE_READ,
				 NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL)))
    {
	pInfo ->bExist = False;

	return;
    }
	::GetFileInformationByHandle (hFitFile, & pFitFile);

	memcpy (& pInfo ->ftCrt, & pFitFile.ftLastWriteTime, sizeof (FILETIME));

//	::ReadFile (hFitFile, & pInfo ->Header, sizeof (FitHeader), & dFitRead, NULL); 

	hFitFile = (::CloseHandle (hFitFile), NULL);

	pInfo ->bExist = True ;
};

static void loadCalibList (HWND hPage, int iDetCalibSet)
{
    Bool bFocus = (0 == (int) ::SendDlgItemMessage 

   (hPage, IDC_MMSC_CALIB_PAGER, TCM_GETCURSEL, (WPARAM) 0, (LPARAM) 0)) ? True : False;

    for (int i = 0; i < sizeof (s_pMSCCalibHV) / sizeof (int); i ++)
    {
	getFitHeader (iDetCalibSet, bFocus, (FitInfo *) ::SendDlgItemMessage 

       (hPage, IDC_MMSC_CALIB_LIST, LB_GETITEMDATA, (WPARAM) i, (LPARAM) NULL));
    };
};

static void eraseCalibFile (int iDetCalibSet, Bool bFocus, int nFitInfo, FitInfo ** pFitInfo)
{
    char   pFitName [MAX_PATH];

    for (int i = 0; i < nFitInfo; i ++)
    {
	GetNVCalibratePath (pFitName, iDetCalibSet);

	wsprintf  (pFitName + strlen (pFitName), "\\%s\\%d", bFocus ? "BF" : "SF", pFitInfo [i] ->uHV);

	RemoveDir (pFitName);

	printf	  ("Remove dir %s, ", pFitName);

	strcat    (pFitName, ".cff");

	::DeleteFile (pFitName);

	printf ("Remove fit %s\n", pFitName);
    }
};

static void drawCalibList (HWND hPage, DRAWITEMSTRUCT * ds)
{
    char    pName [64];

    FILETIME   ft;
    SYSTEMTIME st;
    RECT       rb;
    RECT       ri = ds ->rcItem;

/*
    printf ("...Draw item %d, %d kV, ", (int) ds ->itemID, (int) ds ->itemData);

    printf ("%c%c%c - %c%c\n", (ds ->itemAction & ODA_DRAWENTIRE) ? 'D' : '.',
			       (ds ->itemAction & ODA_SELECT	) ? 'S' : '.',
			       (ds ->itemAction & ODA_FOCUS     ) ? 'F' : '.',

    
			       (ds ->itemState	& ODS_SELECTED  ) ? 's' : '.',
			       (ds ->itemState  & ODS_FOCUS	) ? 'f' : '.');
*/
    ::GetWindowRect (ds ->hwndItem, & rb, hPage);

    ::FillRect	    (ds ->hDC, & ri, GetSysColorBrush 
    
    ((ds ->itemState & /*ODS_FOCUS*/ ODS_SELECTED) ? COLOR_GRADIENTACTIVECAPTION : COLOR_WINDOW));

    ::wsprintf  (pName, LoadString (IDS_TEXT_KV_VALUE), ((FitInfo *) ds ->itemData) ->uHV);

    if (((FitInfo *) ds ->itemData) ->bExist)
    {
    ::FileTimeToLocalFileTime (& ((FitInfo *) ds ->itemData) ->ftCrt, & ft);
    ::FileTimeToSystemTime    (& ft, & st);

    ::wsprintf  (pName + strlen (pName), "     %02d.%02d.%02d"
					 "     %02d:%02d"    , st.wDay , st.wMonth , st.wYear % 100,
							       st.wHour, st.wMinute);

//  ::wsprintf	(pName + strlen (pName), "[%d-%d][%d-%d]", ((FitInfo *) ds ->itemData) ->Header.Yimg0,
//							   ((FitInfo *) ds ->itemData) ->Header.Yimg1,
//							   ((FitInfo *) ds ->itemData) ->Header.Ydc0 ,
//							   ((FitInfo *) ds ->itemData) ->Header.Ydc1 );
    }

    ri.left   += 4;
    ri.right  -= 4;

    ::SetBkMode (ds ->hDC, TRANSPARENT);
    ::DrawText	(ds ->hDC, pName, -1, & ri, DT_SINGLELINE | DT_VCENTER | DT_LEFT);
};

#define DEFINE_MENUITEMINFO(fMask,fType,wID,dTypeData)	    \
				{ sizeof (MENUITEMINFO) ,   \
				  fMask			,   \
				  fType			,   \
				  0			,   \
				  wID			,   \
				  NULL			,   \
				  NULL			,   \
				  NULL			,   \
				  NULL			,   \
			 (LPTSTR) dTypeData		,   \
				  0			,   \
				  NULL			}
				  
#define DEFINE_MENUITEMINFO_STRING(wID,pName)		    \
	DEFINE_MENUITEMINFO	  (MIIM_TYPE | MIIM_ID, MFT_STRING, wID, pName)

#define DEFINE_MENUITEMINFO_SEPARATOR()			    \
	DEFINE_MENUITEMINFO	  (MIIM_TYPE | MIIM_ID, MFT_SEPARATOR, 0, NULL)

int getCalibListContext (HWND hList, int x, int y)
{
    static const MENUITEMINFO Menu [] = {
		 DEFINE_MENUITEMINFO_STRING    (1, LoadString (IDS_CALIB_CLEAR)),
		 DEFINE_MENUITEMINFO_STRING    (2, LoadString (IDS_CALIB_CREATE))
					};

    HMENU  hMenu = ::CreatePopupMenu ();

    int  i;

    for (int i = 0; i < 2; i ++)
    {
	::InsertMenuItem (hMenu, i, True, & Menu [i]);
    }

    i = ::TrackPopupMenu (hMenu, TPM_NONOTIFY	 |
				 TPM_RETURNCMD	 |
				 TPM_LEFTALIGN   |
				 TPM_LEFTBUTTON	 |
				 TPM_RIGHTBUTTON , x, y, 0,
				 hList, NULL);
	::DestroyMenu	 (hMenu);

	::PostMessage	 (hList, WM_NULL, 0, 0);

    return i;
};

Bool CMainPanel::On_Setup_MSC_Page1 (GWinMsg & m)
{
    if ((WM_CONTEXTMENU == m.uMsg) && ((HWND) m.wParam == ::GetDlgItem 
				      (m.hWnd, IDC_MMSC_CALIB_LIST    )))
    {{
	POINT	 pCur = {GET_X_LPARAM (m.lParam), GET_Y_LPARAM (m.lParam)};

	int	  nFitInfo, iSel; RECT rSel;
	int	  iFitInfo [sizeof (s_pMSCCalibHV) / sizeof (int)];
	FitInfo * pFitInfo [sizeof (s_pMSCCalibHV) / sizeof (int)];

	if (0 >= (nFitInfo = (int) ::SendMessage ((HWND) m.wParam, LB_GETSELITEMS, 

		 (WPARAM) sizeof (s_pMSCCalibHV) / sizeof (int), (LPARAM) iFitInfo)))
	{
	    return True;
	}

	for (iSel = 0; iSel < nFitInfo; iSel ++)
	{
	    pFitInfo [iSel] = (FitInfo *) ::SendMessage ((HWND) m.wParam, LB_GETITEMDATA, 
	    
	    (WPARAM) iFitInfo [iSel], (LPARAM) NULL);
	};
	
	if ((0 > pCur.x) && (0 > pCur.y))
	{{
	    ::SendMessage ((HWND) m.wParam, LB_GETITEMRECT, 
	    
	    (WPARAM) iFitInfo [nFitInfo - 1], (LPARAM) & rSel);

	    pCur.x = rSel.left + (((rSel.right - rSel.left) * 2) / 10);
	    pCur.y = rSel.top  + (((rSel.bottom - rSel.top) * 8) / 10);

	    ::ClientToScreen ((HWND) m.wParam, & pCur);

	    ::SetCursorPos   (pCur.x, pCur.y);
	}}

	switch (getCalibListContext ((HWND) m.wParam, pCur.x, pCur.y))
	{
	case 0 :
	    break;
	case 1 :

	    eraseCalibFile   (m_iDetCalibSet, (0 == (int) ::SendDlgItemMessage 
	   (m.hWnd, IDC_MMSC_CALIB_PAGER, TCM_GETCURSEL, (WPARAM) 0, (LPARAM) 0)) ? True : False, nFitInfo, pFitInfo);

	    loadCalibList    (m.hWnd, m_iDetCalibSet); 
	    ::InvalidateRect (::GetDlgItem (m.hWnd, IDC_MMSC_CALIB_LIST), NULL, True);
	    break;

	case 2 :
	
	    ::GetWindowRect  (m_hSetupPager, & rSel);

	    On_MSC_Calibrate (rSel.left, rSel.top, this, m_iDetCalibSet, (0 == (int) ::SendDlgItemMessage 
	   (m.hWnd, IDC_MMSC_CALIB_PAGER, TCM_GETCURSEL, (WPARAM) 0, (LPARAM) 0)) ? True : False, nFitInfo, pFitInfo);

	    loadCalibList    (m.hWnd, m_iDetCalibSet);
	    ::InvalidateRect (::GetDlgItem (m.hWnd, IDC_MMSC_CALIB_LIST), NULL, True);
	    break;
	};

	return True;
    }}

    if ((WM_DELETEITEM == m.uMsg) && (m.wParam == IDC_MMSC_CALIB_LIST))
    {
	delete (FitInfo *) ((DELETEITEMSTRUCT *) m.lParam) ->itemData;

	return True;
    }

    if ((WM_DRAWITEM == m.uMsg) && (m.pDRAWITEMSTRUCT ->CtlID == IDC_MMSC_CALIB_LIST))
    {
	drawCalibList (m.hWnd, m.pDRAWITEMSTRUCT);

	return True;
    }

    if ((WM_NOTIFY == m.uMsg) && (m.pNMHDR) && (m.pNMHDR ->idFrom == IDC_MMSC_CALIB_PAGER))
    {
    if (m.pNMHDR ->code == TCN_SELCHANGE)
    {
	loadCalibList (m.hWnd, m_iDetCalibSet);

	::InvalidateRect (::GetDlgItem (m.hWnd, IDC_MMSC_CALIB_LIST), NULL, True);
    }
	return True;
    }

    int		nCtrlId;

    if ((0 !=  (nCtrlId = m.IsCommand ())) && (BN_CLICKED == m.HiwParam))
    {
	switch (nCtrlId)
	{
	case IDC_MMSC_IMG_REPAIR_CHK	:
	case IDC_MMSC_IMG_DUPLICATE_CHK :
	case IDC_MMSC_IMG_DOWHITE_CHK	:
	case IDC_MMSC_IMG_DOADAPT_CHK	:

	    m_SetupData.m_SetupMSC.m_Scan_ImageProcMask  = 0;
    	    m_SetupData.m_SetupMSC.m_Scan_ImageProcMask |= (BST_CHECKED == ::IsDlgButtonChecked (m.hWnd, IDC_MMSC_IMG_REPAIR_CHK   )) ? 0x80 : 0x00;
	    m_SetupData.m_SetupMSC.m_Scan_ImageProcMask |= (BST_CHECKED == ::IsDlgButtonChecked (m.hWnd, IDC_MMSC_IMG_DUPLICATE_CHK)) ? 0x04 : 0x00;
    	    m_SetupData.m_SetupMSC.m_Scan_ImageProcMask |= (BST_CHECKED == ::IsDlgButtonChecked (m.hWnd, IDC_MMSC_IMG_DOWHITE_CHK  )) ? 0x02 : 0x00;
    	    m_SetupData.m_SetupMSC.m_Scan_ImageProcMask |= (BST_CHECKED == ::IsDlgButtonChecked (m.hWnd, IDC_MMSC_IMG_DOADAPT_CHK  )) ? 0x01 : 0x00;

	    return (On_SetupChanged (), True);
	};

	switch (nCtrlId)
	{
	case IDC_MM2_ROTLPIN_RAD	:
	case IDC_MM2_ROTL45PIN_RAD	:
	case IDC_MM2_ROTMPIN_RAD	:
	case IDC_MM2_ROTR45PIN_RAD	:
	case IDC_MM2_ROTRPIN_RAD	:

	    if (BST_CHECKED == ::IsDlgButtonChecked (m.hWnd, IDC_MM2_ROTLPIN_RAD  )) m_iDetCalibSet = -2;
	    if (BST_CHECKED == ::IsDlgButtonChecked (m.hWnd, IDC_MM2_ROTL45PIN_RAD)) m_iDetCalibSet = -1;
	    if (BST_CHECKED == ::IsDlgButtonChecked (m.hWnd, IDC_MM2_ROTMPIN_RAD  )) m_iDetCalibSet =  0;
	    if (BST_CHECKED == ::IsDlgButtonChecked (m.hWnd, IDC_MM2_ROTR45PIN_RAD)) m_iDetCalibSet =  1;
	    if (BST_CHECKED == ::IsDlgButtonChecked (m.hWnd, IDC_MM2_ROTRPIN_RAD  )) m_iDetCalibSet =  2;

	    loadCalibList (m.hWnd, m_iDetCalibSet);
	    ::InvalidateRect (::GetDlgItem (m.hWnd, IDC_MMSC_CALIB_LIST), NULL, True);

	    return True;
	};
    }
	return False;
};

Bool GAPI CMainPanel::DispatchSetupDlgMsg (void * oDsp, GWinMsg & m)
{
    if ((WM_COMMAND == m.uMsg) && (0 == m.HiwParam) && (((CMainPanel *) oDsp) ->GetHWND () == m.lWnd))
    {
	switch (m.LowParam)
	{
	case ((0 << 8) + 0) :
	   ((CMainPanel *) oDsp) ->m_pCurPageHandler = & CMainPanel::On_Setup_MDIG_Page0;
	    break;

	case ((1 << 8) + 0) :
	   ((CMainPanel *) oDsp) ->m_pCurPageHandler = & CMainPanel::On_Setup_URP_Page0;
	    break;

	case ((1 << 8) + 1) :
	   ((CMainPanel *) oDsp) ->m_pCurPageHandler = & CMainPanel::On_Setup_URP_Page1;
	    break;

	case ((1 << 8) + 2) :
	   ((CMainPanel *) oDsp) ->m_pCurPageHandler = & CMainPanel::On_Setup_URP_Page2;
	    break;

//	case ((1 << 8) + 3) :
//	   ((CMainPanel *) oDsp) ->m_pCurPageHandler = & CMainPanel::On_Setup_URP_Page3;
//	    break;

	case ((2 << 8) + 0) :
	   ((CMainPanel *) oDsp) ->m_pCurPageHandler = & CMainPanel::On_Setup_MSC_Page0;
	    break;

	case ((2 << 8) + 1) :
	   ((CMainPanel *) oDsp) ->m_pCurPageHandler = & CMainPanel::On_Setup_MSC_Page1;
	    break;

//	case ((2 << 8) + 2) :
//	   ((CMainPanel *) oDsp) ->m_pCurPageHandler = & CMainPanel::On_Setup_MSC_Page2;
//	    break;

	default :
	   ((CMainPanel *) oDsp) ->m_pCurPageHandler = NULL;
	    break;
	}
    }
	return (((CMainPanel *) oDsp) ->m_pCurPageHandler) ?
	       (((CMainPanel *) oDsp) ->*
		((CMainPanel *) oDsp) ->m_pCurPageHandler)(m) : False;
};
//
//[]------------------------------------------------------------------------[]
//
void CMainPanel::On_DeletePanel (PanelEntry * e)
{
    GRect   wr;

    if (e ->m_pDialog)	
    {
	RemPreprocHandler (e ->m_pDialog);

	e ->m_pDialog ->GetWindowRect (& wr);
	e ->m_sWinPos = wr.Pos ();

	e ->m_pDialog ->Destroy ();

	delete 
       (e ->m_pDialog);
	e ->m_pDialog = NULL;
    }
};

void CMainPanel::On_CreatePanel (int nCtrlId, PanelEntry * e)
{
    Bool   bCreate = IsCtrlChecked  (nCtrlId);

    if ((  bCreate && (NULL != e ->m_pDialog))
    &&  (! bCreate && (NULL == e ->m_pDialog)))
    {	return;	}

    if (e ->m_pDialog)	
    {
	On_DeletePanel (e);

    if ((IDC_EQU_MDIG_CHK == nCtrlId) && m_eUrpPanel.m_pDialog && (IRRPMDIG::EquMask_KKM & m_dEquMask))
    {
        ((CUrpPanel *) m_eUrpPanel.m_pDialog) ->SetEquMask
        ((IRRPMDIG::EquMask_KKM	     & m_dEquMask) ? IRRPUrp::EquMask_KKM : 0);
    }
	return;	
    }

    switch (nCtrlId)
    {
    case IDC_EQU_MDIG_CHK :
       (e ->m_pDialog = new CMDIGPanel ())  ->Create (this,
						      NULL, MAKEINTRESOURCE (IDD_MDIG));
	((CMDIGPanel *) e ->m_pDialog) ->SetEquMask (m_dEquMask);
	break;

    case IDC_EQU_URP_CHK :
       (e ->m_pDialog = new CUrpPanel ())   ->Create (this,
						      NULL, MAKEINTRESOURCE (IDD_URP));

//      e ->m_pDialog ->SetExStyles (0, WS_EX_LAYERED);
//	::SetLayeredWindowAttributes  (e ->m_pDialog ->GetHWND (),
//	::GetSysColor (COLOR_3DFACE), (128 + 64 + 16), /*LWA_COLORKEY |*/ LWA_ALPHA);

        ((CUrpPanel *) e ->m_pDialog) ->SetEquMask
        ((IRRPMDIG::EquMask_KKM	     & m_dEquMask) ? IRRPUrp::EquMask_KKM : 0);

	break;

    case IDC_EQU_MSC_CHK :
       (e ->m_pDialog = new CMScanPanel ()) ->Create (this,
						      NULL, MAKEINTRESOURCE (IDD_MSC));

	((CMScanPanel *) e ->m_pDialog) ->SetEquMask 
	((IRRPMDIG::EquMask_MDiafr
	| IRRPMDIG::EquMask_MScan
	| IRRPMDIG::EquMask_MFG   ) & m_dEquMask);
	break;

    case IDC_EQU_MM2_CHK :
       (e ->m_pDialog = new CMM2Panel ())   ->Create (this,
						      NULL, MAKEINTRESOURCE (IDD_MM2));
	break;
    }

    if (e ->m_pDialog && e ->m_pDialog ->GetHWND ())
    {
	AddPreprocHandler (e ->m_pDialog);

	e ->m_pDialog ->SetWindowPos (NULL, e ->m_sWinPos.x, e ->m_sWinPos.y, 0, 0, SWP_NOSIZE);

	e ->m_pDialog ->ShowWindow   (True, False);

	CheckCtrl   (nCtrlId, True);
    }
    else
    {
	delete 
       (e ->m_pDialog);
	e ->m_pDialog = NULL;
    }
};
//
//[]------------------------------------------------------------------------[]
//
Bool CMainPanel::DispatchSetupDlgMsg (GWinMsg & m)
{
//  ::MessageBeep (-1);

    printf ("PageDlgProc %d : [%d,%d] : %08X, %08X, [%08X, %08X]\n",
	    ::GetDlgCtrlID (m.hWnd), m_nSetupPager, m_nCurPage,
	    m.hWnd, m.uMsg, m.wParam, m.lParam);

    return False;
};
//
//[]------------------------------------------------------------------------[]
//
void CMainPanel::On_Destroy ()
{
    s_pIRRPService ->SetEquCallBack (NULL);

    On_DeletePanel (& m_eMDIGPanel);
    On_DeletePanel (& m_eUrpPanel );
    On_DeletePanel (& m_eMSCPanel );
    On_DeletePanel (& m_eMM2Panel );

    {{
	GRect		    rw ;
	GUIClientConfig	    cfg;

	if (ERROR_SUCCESS == cfg.Create ())
	{
	    cfg.m_Body.dEquMask = m_dEquMask;

	    ::GetWindowRect (::GetDesktopWindow (), & rw, NULL);
	    cfg.m_Body.iScrSizeX = rw.cx;
	    cfg.m_Body.iScrSizeY = rw.cy;

	    GetWindowRect (& rw);
	    cfg.m_Body.MainPanel.iScrPosX = rw.x;
	    cfg.m_Body.MainPanel.iScrPosY = rw.y;

	    cfg.m_Body.UrpPanel.iScrPosX  = m_eUrpPanel.m_sWinPos.x;
	    cfg.m_Body.UrpPanel.iScrPosY  = m_eUrpPanel.m_sWinPos.y;

	    cfg.m_Body.MSCPanel.iScrPosX  = m_eMSCPanel.m_sWinPos.x;
	    cfg.m_Body.MSCPanel.iScrPosY  = m_eMSCPanel.m_sWinPos.y;

	    cfg.m_Body.MM2Panel.iScrPosX  = m_eMM2Panel.m_sWinPos.x;
	    cfg.m_Body.MM2Panel.iScrPosY  = m_eMM2Panel.m_sWinPos.y;

	    cfg.m_Body.MDIGPanel.iScrPosX = m_eMDIGPanel.m_sWinPos.x;
	    cfg.m_Body.MDIGPanel.iScrPosY = m_eMDIGPanel.m_sWinPos.y;

	    cfg.Save ();
	}
    }}
};

static void deleteSetupPager (HWND hPager)
{
    TCITEM  ti = {TCIF_PARAM};
    int	    n ;

    n = ::SendMessage (hPager, TCM_GETITEMCOUNT, (WPARAM) 0, (LPARAM) 0);

    for (; 0 < n; n --)
    {
	::SendMessage (hPager, TCM_GETITEM   , (WPARAM) n - 1, (LPARAM) & ti);

	::DestroyWindow ((HWND) ti.lParam);

	::SendMessage (hPager, TCM_DELETEITEM, (WPARAM) n - 1, (LPARAM) 0);
    }
};

static Bool createSetupPager (HWND hPager, GDlgProcThunk * thnk, /* int ResIds */ ...)
{
    va_list	_args		;

    int		 i, iRes	;
    HWND	 hDlgPage	;
    RECT	 wr		;
    TCITEM	 ti		;
    TCHAR	 pDlgName [128]	;

    va_start   (_args, thnk);

    for (i = 0, iRes = va_arg (_args, int); 0 < iRes; i ++, iRes = va_arg (_args, int))
    {
	hDlgPage = ::CreateDialogEx (NULL, MAKEINTRESOURCE (iRes), ::GetParent (hPager), (DLGPROC) thnk);

		   ::GetWindowText  (hDlgPage, pDlgName, sizeof (pDlgName) / sizeof (TCHAR));

	memset (& ti, 0, sizeof (ti));

	ti.mask	    =  TCIF_TEXT | TCIF_PARAM ;

	ti.pszText  = (LPTSTR) pDlgName ;
	ti.lParam   = (LPARAM) hDlgPage ;
    
	::SendMessage	(hPager, TCM_INSERTITEM, (WPARAM) i, (LPARAM) & ti);

	::GetWindowRect (hPager, & wr, ::GetParent (hPager));
	::SendMessage   (hPager, TCM_ADJUSTRECT, (WPARAM) 0, (LPARAM) & wr);

	::SetWindowPos  (hDlgPage ,
			 hPager   ,
			 wr.left  ,
			 wr.top   ,
			 wr.right  - wr.left ,
			 wr.bottom - wr.top  ,
			 0
		       | SWP_NOZORDER 
//		       | SWP_NOSIZE
		       | 0	  );
    }

    va_end     (_args);

    return True;
};

struct MSCCalibItem 
{
    int	    uHV		;
    char    pName [64]	;
};

static void initSetupPagerMSC (HWND hPager, int iDetCalibSet)
{
    TCITEM    ti = {TCIF_PARAM};

    ::SendMessage (hPager, TCM_GETITEM, (WPARAM) 0, (LPARAM) & ti);

    ::SendDlgItemMessage ((HWND) ti.lParam, IDC_MMSC_VERSION, CB_ADDSTRING, (WPARAM) 0, (LPARAM) _T("  1.0"));
    ::SendDlgItemMessage ((HWND) ti.lParam, IDC_MMSC_VERSION, CB_ADDSTRING, (WPARAM) 0, (LPARAM) _T("  2.0"));

    ::SendMessage (hPager, TCM_GETITEM, (WPARAM) 1, (LPARAM) & ti);

    TCITEM    si;
    memset (& si, 0, sizeof (si)); si.mask = TCIF_TEXT ; si.pszText  = (LPTSTR) LoadString (IDS_FOCUS_LARGE_FMT);
    ::SendDlgItemMessage ((HWND) ti.lParam, IDC_MMSC_CALIB_PAGER, TCM_INSERTITEM, (WPARAM) 0, (LPARAM) & si);

    memset (& si, 0, sizeof (si)); si.mask = TCIF_TEXT ; si.pszText  = (LPTSTR) LoadString (IDS_FOCUS_SMALL_FMT);
    ::SendDlgItemMessage ((HWND) ti.lParam, IDC_MMSC_CALIB_PAGER, TCM_INSERTITEM, (WPARAM) 1, (LPARAM) & si);

    ::SendDlgItemMessage ((HWND) ti.lParam, IDC_MMSC_CALIB_PAGER, TCM_SETCURSEL,  (WPARAM) 0, (LPARAM) 0);

    ::SendDlgItemMessage ((HWND) ti.lParam, IDC_MMSC_CALIB_LIST, LB_SETITEMHEIGHT, (WPARAM) 0, (LPARAM)
   (::SendDlgItemMessage ((HWND) ti.lParam, IDC_MMSC_CALIB_LIST, LB_GETITEMHEIGHT, (WPARAM) 0, (LPARAM) 0) * 19)/10);

    createCalibList	 ((HWND) ti.lParam);
    loadCalibList	 ((HWND) ti.lParam, iDetCalibSet);
};

HWND CMainPanel::On_SelectSetupPager (int nSetupPager)
{
    TCITEM   ti = {TCIF_PARAM};

    if (m_nSetupPager == nSetupPager)
    {
	goto lExit;
    }

    CheckRadioButton (IDC_SETUP_MDIG, IDC_SETUP_MM2, IDC_SETUP_MDIG + nSetupPager);

    if (m_SetupData.m_bChanged)
    {
	if (IDYES == ::MessageBox (GetHWND (),
	    	     "��������� ��������� ?", "������������ ������ ���� ��������", MB_YESNO | MB_ICONQUESTION))
	{
	    DoSetupExchange (m_nSetupPager, True);
	}
	else
	{
	    DoSetupExchange (-1, False);
	}
    }

    if (0 <= m_nSetupPager)
    {
    if (m_hCurPage)
    {
	m_nCurCash [m_nSetupPager] = m_nCurPage;

	::ShowWindow (m_hCurPage, SW_HIDE);

	m_hCurPage = NULL;
    }
	deleteSetupPager  (m_hSetupPager);
    }

    switch (m_nSetupPager = nSetupPager)
    {
    case 0:
	createSetupPager (m_hSetupPager, m_pPageDlgProc,
			  IDD_SETUP_MDIG_PAGE0, 0);
	break;
    case 1:
	createSetupPager (m_hSetupPager, m_pPageDlgProc,
			  IDD_SETUP_URP_PAGE0,
			  IDD_SETUP_URP_PAGE1,
			  IDD_SETUP_URP_PAGE2,  0);
	break;
    case 2:
	createSetupPager (m_hSetupPager, m_pPageDlgProc,
			  IDD_SETUP_MSC_PAGE0,
			  IDD_SETUP_MSC_PAGE1,  0);

	initSetupPagerMSC (m_hSetupPager, m_iDetCalibSet);

	break;
    case 3:
	createSetupPager (m_hSetupPager, m_pPageDlgProc,
			  IDD_SETUP_MM2_PAGE0,  0);
	break;
    };

	m_nCurPage = m_nCurCash [m_nSetupPager];

lExit :

	::SendMessage (m_hSetupPager, TCM_SETCURSEL, (WPARAM) m_nCurPage, (LPARAM) 0);

	::SendMessage (m_hSetupPager, TCM_GETITEM  ,
		      (WPARAM)
	::SendMessage (m_hSetupPager, TCM_GETCURSEL, (WPARAM) 0, (LPARAM) 0),
		      (LPARAM) & ti );

	::SendMessage ((HWND) ti.lParam, WM_COMMAND, 
		      (WPARAM)((m_nSetupPager << 8) + m_nCurPage), (LPARAM) GetHWND ());

	DoSetupExchange	 (m_nSetupPager, False);

	::ShowWindow  ((HWND) ti.lParam, SW_SHOWNA);

	return (HWND) ti.lParam;
};

Bool CMainPanel::On_Create (void * pParam)
{
    if (! GDialog::On_Create (pParam))
    {	return False;};

    DWord   dVersion	 ;
    TCHAR   pVersion [64];
    TCHAR   pTitle  [256];
    
    s_pIRRPService ->CreateRRPSettings (m_pIRRPSettings);

    GetText (pTitle, sizeof (pTitle) / sizeof (TCHAR));

    m_pIRRPSettings ->GetValue (IRRPSettings::ID_MDIG_EngVersion, pVersion, sizeof (pVersion), & dVersion);

    strcat  (pTitle, " - ");
    strcat  (pTitle, pVersion);
    SetText (pTitle);

    m_hSetupPager  = GetCtrlHWND 
			 (IDC_EQU_SETUP_PGR);
    m_hCurPage     = On_SelectSetupPager (0);

    {{
	GRect		    rw ;
	GUIClientConfig	    cfg;

	if (ERROR_SUCCESS == cfg.Create ())
	{
	    m_dEquMask = cfg.m_Body.dEquMask;

	    ::GetWindowRect (::GetDesktopWindow (), & rw, NULL);

	if ((rw.cx == cfg.m_Body.iScrSizeX)
	&&  (rw.cy == cfg.m_Body.iScrSizeY))
	{
	    m_eUrpPanel.m_sWinPos.x  = cfg.m_Body.UrpPanel.iScrPosX; 
	    m_eUrpPanel.m_sWinPos.y  = cfg.m_Body.UrpPanel.iScrPosY;

	    m_eMSCPanel.m_sWinPos.x  = cfg.m_Body.MSCPanel.iScrPosX;
	    m_eMSCPanel.m_sWinPos.y  = cfg.m_Body.MSCPanel.iScrPosY;

	    m_eMM2Panel.m_sWinPos.x  = cfg.m_Body.MM2Panel.iScrPosX;
	    m_eMM2Panel.m_sWinPos.y  = cfg.m_Body.MM2Panel.iScrPosY;

	    m_eMDIGPanel.m_sWinPos.x = cfg.m_Body.MDIGPanel.iScrPosX;
	    m_eMDIGPanel.m_sWinPos.y = cfg.m_Body.MDIGPanel.iScrPosY;

	    SetWindowPos (NULL, cfg.m_Body.MainPanel.iScrPosX,
				cfg.m_Body.MainPanel.iScrPosY, 0, 0, SWP_NOSIZE);
        }
	}
    }}

    SendCtrlMessage (IDC_EQU_INTERFACE_CMB, CB_ADDSTRING, (WPARAM) 0, (LPARAM) _T(" 485 (FTDI based)"));
    SendCtrlMessage (IDC_EQU_INTERFACE_CMB, CB_SETCURSEL, (WPARAM) 0, (LPARAM) 0);

    CheckCtrl (IDC_EQU_URP_INC	    , (m_dEquMask & IRRPMDIG::EquMask_Urp   ) ? True : False);
    CheckCtrl (IDC_EQU_URPKKM_INC   , (m_dEquMask & IRRPMDIG::EquMask_KKM   ) ? True : False);
    CheckCtrl (IDC_EQU_MSCDIAFR_INC , (m_dEquMask & IRRPMDIG::EquMask_MDiafr) ? True : False);
    CheckCtrl (IDC_EQU_MSCSCAN_INC  , (m_dEquMask & IRRPMDIG::EquMask_MScan ) ? True : False);
    CheckCtrl (IDC_EQU_MSCFG_INC    , (m_dEquMask & IRRPMDIG::EquMask_MScan ) ? True : False);
    CheckCtrl (IDC_EQU_MM2_INC	    , (m_dEquMask & IRRPMDIG::EquMask_MM2   ) ? True : False);

    return True ;
};

Bool CMainPanel::On_Close (GWinMsg & m)
{
    printf ("\n\t..............Exit Begin\n\n");

    if (m_pIRRPSettings)
    {
	m_pIRRPSettings ->Close ();
	m_pIRRPSettings = NULL;
    }

    Destroy  ();

    TCurrent () ->GetCurApartment () ->Release ();

    return True;
};

void CMainPanel::SetEquMask (DWord dEquMask)
{
    DWord dChanged = m_dEquMask ^ dEquMask;

	m_dEquMask = dEquMask;

    if ((dChanged & (IRRPMDIG::EquMask_KKM   )) && m_eUrpPanel.m_pDialog)
    {
	((CUrpPanel *)	 m_eUrpPanel.m_pDialog ) ->SetEquMask 
	((IRRPMDIG::EquMask_KKM  & m_dEquMask) ? IRRPUrp::EquMask_KKM : 0);
    }
	CheckCtrl (IDC_EQU_MSCFG_INC, dEquMask & IRRPMDIG::EquMask_MScan);

    if ((dChanged & (IRRPMDIG::EquMask_MDiafr
		  |  IRRPMDIG::EquMask_MScan
		  |  IRRPMDIG::EquMask_MFG    )) && m_eMSCPanel.m_pDialog)
    {
	((CMScanPanel *) m_eMSCPanel.m_pDialog ) ->SetEquMask
		   ((IRRPMDIG::EquMask_MDiafr
		  |  IRRPMDIG::EquMask_MScan
		  |  IRRPMDIG::EquMask_MFG     ) & m_dEquMask);
    }
    if ((dChanged) && m_eMDIGPanel.m_pDialog)
    {
       ((CMDIGPanel *) m_eMDIGPanel.m_pDialog) ->SetEquMask (m_dEquMask);
    }
};

Bool CMainPanel::On_SetupPage (GWinMsg & m)
{
    if ((WM_NOTIFY == m.uMsg) && m.pNMHDR)
    {{
    switch (m.pNMHDR ->code)
    {
    case TCN_SELCHANGE :
    {{
	TCITEM    ti = {TCIF_PARAM};

	::SendMessage (m.pNMHDR ->hwndFrom, TCM_GETITEM  , (WPARAM)
       (m_nCurPage =  (int)
	::SendMessage (m.pNMHDR ->hwndFrom, TCM_GETCURSEL, (WPARAM) 0, (LPARAM)   0)),
								       (LPARAM) & ti);
    if (m_hCurPage)
    {
	::ShowWindow (m_hCurPage, SW_HIDE  );
    }
	m_hCurPage =  (HWND) ti.lParam;

    if (m_hCurPage)
    {
	::SendMessage (m_hCurPage, WM_COMMAND,
		      (WPARAM)((m_nSetupPager << 8) + m_nCurPage), (LPARAM) GetHWND ());

	::ShowWindow  (m_hCurPage, SW_SHOWNA);
    }
    }}
	return True ;
    }
    }}
	return m.DispatchDefault ();
};

static void EquMsg (int * cLed, CLedIndicator * pLed, HWND hLed, DWord dEquParam)
{
    if (0x8000 == dEquParam)
    {
	if (0 == (* cLed) ++)
	{
	    pLed ->SetColor (NULL, CLedIndicator::Red);
	    pLed ->TurnOn   (hLed, True);
	}
    }
    else
    if (0x4000 == dEquParam)
    {
	if (0 == -- (* cLed))
	{
	    pLed ->TurnOn   (hLed, False);
	}
    }
    else
    {
	    pLed ->SetColor (hLed, 
	   (0x02 & dEquParam) ? CLedIndicator::Green : CLedIndicator::Red);
    }
};

void CMainPanel::On_EquMsg (DWord dEquMask, DWord dEquParam)
{
    if (0x8000 == dEquParam)
    {
	if (0 == m_cLedInterface ++)
	{
	    m_oLedInterface.SetColor (NULL, CLedIndicator::Red);
	    m_oLedInterface.TurnOn   (GetCtrlHWND (IDC_EQU_INTERFACE_LED), True);
	}
    }
    else
    if (0x4000 == dEquParam)
    {
	if (0 == -- m_cLedInterface)
	{
	    m_oLedInterface.TurnOn   (GetCtrlHWND (IDC_EQU_INTERFACE_LED  ), False);
	}
    }
    else
    if (0x0004 >  dEquParam)
    {
	if (IRRPMDIG::EquMask_MFG != dEquMask)
	{
	    m_oLedInterface.SetColor (GetCtrlHWND (IDC_EQU_INTERFACE_LED), 
	   (0x01 & LOWORD (dEquParam)) ? CLedIndicator::Green : CLedIndicator::Red);
	}
    }
    else
    {	return;}

    switch (dEquMask)
    {
    case IRRPMDIG::EquMask_Urp :
	    EquMsg (& m_cLedUrp, & m_oLedUrp, GetCtrlHWND (IDC_EQU_URP_LED), dEquParam);
	    break;
    case IRRPMDIG::EquMask_KKM :
	    EquMsg (& m_cLedKKM, & m_oLedKKM, GetCtrlHWND (IDC_EQU_URPKKM_LED), dEquParam);
	    break;
    case IRRPMDIG::EquMask_MDiafr :
	    EquMsg (& m_cLedMSCDiafr, & m_oLedMSCDiafr, GetCtrlHWND (IDC_EQU_MSCDIAFR_LED), dEquParam);
	    break;
    case IRRPMDIG::EquMask_MScan :
	    EquMsg (& m_cLedMSCScan, & m_oLedMSCScan , GetCtrlHWND (IDC_EQU_MSCSCAN_LED), dEquParam);
	    break;
    case IRRPMDIG::EquMask_MFG :
	    EquMsg (& m_cLedMSCFG, & m_oLedMSCFG, GetCtrlHWND (IDC_EQU_MSCFG_LED), dEquParam);
	    break;
    case IRRPMDIG::EquMask_MM2 :
	    EquMsg (& m_cLedMM2, & m_oLedMM2, GetCtrlHWND (IDC_EQU_MM2_LED), dEquParam);
	    break;
    };
};

Bool CMainPanel::On_DlgMsg (GWinMsg & m)
{
    if (m_hEdit.On_ReflectWinMsg (m))
    {
	printf ("CMainPanel::Edit................\n");
//	return	On_Edit (m);
    }

    if (CWndCallBack::EQUMSG == m.uMsg)
    {
	On_EquMsg ((DWord) m.wParam, (DWord) m.lParam);

	return m.DispatchComplete ();
    }

    int		nCtrlId = IsCommand (m);

    if ((0   != nCtrlId) && (BN_CLICKED == m.HiwParam))
    {{
	switch (nCtrlId)
	{
	case IDC_EQU_URP_INC :
	    return (SetEquMask	((m_dEquMask & (~IRRPMDIG::EquMask_Urp    ))
		    | (IsCtrlChecked (nCtrlId) ? IRRPMDIG::EquMask_Urp : 0)), True);

	case IDC_EQU_URPKKM_INC	: 
	    return (SetEquMask  ((m_dEquMask & (~IRRPMDIG::EquMask_KKM    ))
		    | (IsCtrlChecked (nCtrlId) ? IRRPMDIG::EquMask_KKM : 0)), True);

	case IDC_EQU_MSCDIAFR_INC :
	    return (SetEquMask	  ((m_dEquMask & (~IRRPMDIG::EquMask_MDiafr    ))
		    | (IsCtrlChecked (nCtrlId) ?   IRRPMDIG::EquMask_MDiafr : 0)), True);

	case IDC_EQU_MSCSCAN_INC :
	    return (SetEquMask	  ((m_dEquMask & (~IRRPMDIG::EquMask_MScan     ))
		    | (IsCtrlChecked (nCtrlId) ?   IRRPMDIG::EquMask_MScan  : 0)), True);

	case IDC_EQU_MSCFG_INC :
	    return True;
    
	case IDC_EQU_MM2_INC :
	    return (SetEquMask	((m_dEquMask & (~IRRPMDIG::EquMask_MM2    ))
		    | (IsCtrlChecked (nCtrlId) ? IRRPMDIG::EquMask_MM2 : 0)), True);
//
//....................................
//
	case IDC_EQU_MDIG_CHK :
	    return (On_CreatePanel (IDC_EQU_MDIG_CHK, & m_eMDIGPanel), True);

	case IDC_EQU_URP_CHK :
	    return (On_CreatePanel (IDC_EQU_URP_CHK , & m_eUrpPanel ), True);

	case IDC_EQU_MSC_CHK :
	    return (On_CreatePanel (IDC_EQU_MSC_CHK , & m_eMSCPanel ), True);

	case IDC_EQU_MM2_CHK :
	    return (On_CreatePanel (IDC_EQU_MM2_CHK , & m_eMM2Panel ), True);
//
//....................................
//
	case IDC_SETUP_MDIG :
	case IDC_SETUP_URP  :
	case IDC_SETUP_MSC  :
	case IDC_SETUP_MM2  :

	    return (m_hCurPage = On_SelectSetupPager (nCtrlId - IDC_SETUP_MDIG), True);
//
//....................................
//
	case IDC_SPCANCEL   :
	    DoSetupExchange (m_nSetupPager, False);
	    return True;

	case IDC_SPOK	    :
	    DoSetupExchange (m_nSetupPager, True );
	    return True;
	};
    }}

    if (0 !=   (nCtrlId = IsControlNotify (m)))
    {
	if (WM_DRAWITEM == m.uMsg)
	{
	switch (nCtrlId)
	{
	case IDC_EQU_INTERFACE_LED  :
	    return (m_oLedInterface.On_Draw (m.pDRAWITEMSTRUCT), True);
	case IDC_EQU_URP_LED	    :
	    return (m_oLedUrp	   .On_Draw (m.pDRAWITEMSTRUCT), True);
	case IDC_EQU_URPKKM_LED	     :
	    return (m_oLedKKM	   .On_Draw (m.pDRAWITEMSTRUCT), True);
	case IDC_EQU_MSCDIAFR_LED    :
	    return (m_oLedMSCDiafr .On_Draw (m.pDRAWITEMSTRUCT), True);
	case IDC_EQU_MSCSCAN_LED     :
	    return (m_oLedMSCScan  .On_Draw (m.pDRAWITEMSTRUCT), True);
	case IDC_EQU_MSCFG_LED	     :
	    return (m_oLedMSCFG	   .On_Draw (m.pDRAWITEMSTRUCT), True);
	case IDC_EQU_MM2_LED	     :
	    return (m_oLedMM2	   .On_Draw (m.pDRAWITEMSTRUCT), True);
	};
	}

	switch (nCtrlId)
	{
	case IDC_EQU_SETUP_PGR	     : return On_SetupPage (m);
	default :
	    break;
	};
    }

    if (WM_CLOSE == m.uMsg)
    {
	return On_Close (m);
    }
	return GDialog::On_DlgMsg (m);
};
//
//[]------------------------------------------------------------------------[]













//[]------------------------------------------------------------------------[]
//
class GUIClient : public GWinMsgApartment
{
public :
		GUIClient ();

	       ~GUIClient ()
		{
		    printf ("::~GUIClient\n");
		};

static	GResult GAPI	Main		    (int, void **, HANDLE hResume);




//	void		RegisterPreprocHandler   (GWindow *);
//
//	void		ExtractPreprocHandler

	void		AddPreprocHandler   (GWindow *);

	void		RemovePreprocHandler(GWindow *);

protected :

	void		ExecWindow	    (GWindow *);

	GResult		InitInstance	    (int, void **, HANDLE hResume);

	Bool		PreprocMessage	    (MSG *);


private :

	int		    m_cPreprocArr	;
	GWindow *	    m_pPreprocArr [32]	;
};

GUIClient::GUIClient ()
{
    m_cPreprocArr = 0;
};


void GUIClient::ExecWindow (GWindow * pWindow)
{
/*


*/
};

void GUIClient::AddPreprocHandler (GWindow * pWindow)
{
    if (m_cPreprocArr < (sizeof (m_pPreprocArr) / sizeof (GWindow *)))
    {
	m_pPreprocArr [m_cPreprocArr] = pWindow;

        m_cPreprocArr ++;
    }
};

void GUIClient::RemovePreprocHandler (GWindow * pWindow)
{
    for (int i = 0; i < m_cPreprocArr; i ++)
    {
    if (m_pPreprocArr [i] == pWindow)
    {
	if (i + 1  < m_cPreprocArr)
	{
	    memmove (m_pPreprocArr + i, m_pPreprocArr + i + 1, (Byte *)(m_pPreprocArr + m_cPreprocArr) -
							       (Byte *)(m_pPreprocArr + i + 1	     ));
	}

	m_cPreprocArr --;

        return;
    }
    }
};


void AddPreprocHandler (GWindow * w)
{
    ((GUIClient *) (TCurrent () ->GetCurApartment ())) ->AddPreprocHandler (w);
};

void RemPreprocHandler (GWindow * w)
{
    ((GUIClient *) (TCurrent () ->GetCurApartment ())) ->RemovePreprocHandler (w);
};























//[]------------------------------------------------------------------------[]
//
Bool GUIClient::PreprocMessage (MSG * msg)
{
	for (int i = m_cPreprocArr - 1; 0 <= i; i --)
	{
	    if (m_pPreprocArr [i] ->PreprocMessage (msg))
	    {
		return True ;
	    }
	}

    if (NULL != msg ->hwnd)
    {
	    return False;
    }
	    return False;
};


    struct GWorkHandler
    {
	typedef	TSLink <GWorkHandler, 0>    SLink;
	typedef	TXList <GWorkHandler,
		TSLink <GWorkHandler, 0> >  SList;

	    SLink		m_SLink	;

	    void *		m_hKey	;
    };

    static GWorkHandler::SList	s_WorkRequestStack;


void InsertWorkHandler (GWorkHandler & h)
{
    s_WorkRequestStack.insert  (& h);
};

void RemoveWorkHandler (GWorkHandler & h)
{
    s_WorkRequestStack.extract (& h);
};


#define	MAIN_RUN    1

	static IUnknown *   s_pIUnknown	    = NULL;
	static GWindow  *   s_pMainWindow   = NULL;

void On_CloseRequest (void * hKey, WPARAM wParam, LPARAM lParam)
{
printf ("\n\n............................Begin Exit from Application\n\n");

#if (MAIN_RUN)

printf ("\n\n............................Exec  Exit from Application\n\n");

    s_pMainWindow ->Destroy ();

    s_pIUnknown   ->Release ();

#else

    TCurrent () ->GetCurApartment () ->BreakFromWorkLoop  ();

#endif
};




static void Check ()
{
    RECT	    rw = {1000, 20, 0, 0};
    CMMotorParam    Dlg;

    if (  Dlg.Create (NULL,
		      NULL, MAKEINTRESOURCE (IDD_MSC_MOTOR_PARAMS)))
    {
	AddPreprocHandler (& Dlg);

	Dlg.SetWindowPos (NULL, rw.left , rw.bottom, 0, 0, SWP_NOSIZE);

	if (IDOK == Dlg.ExecWindow ())
	{
	}

	RemPreprocHandler (& Dlg);
    }
};

static void GAPI EquCallBack (_U32 uMask, _U32 uChanged)
{
    if (uChanged & IRRPMDIG::EquMask_Urp)
    {
	s_pMainPanel ->Post_WM (CWndCallBack::EQUMSG, (WPARAM) IRRPMDIG::EquMask_Urp, (LPARAM) (uMask & IRRPMDIG::EquMask_Urp) ? 0x8000 : 0x4000);
    }
    if (uChanged & IRRPMDIG::EquMask_KKM)
    {
	s_pMainPanel ->Post_WM (CWndCallBack::EQUMSG, (WPARAM) IRRPMDIG::EquMask_KKM, (LPARAM) (uMask & IRRPMDIG::EquMask_KKM) ? 0x8000 : 0x4000);
    }
    if (uChanged & IRRPMDIG::EquMask_MDiafr)
    {
	s_pMainPanel ->Post_WM (CWndCallBack::EQUMSG, (WPARAM) IRRPMDIG::EquMask_MDiafr, (LPARAM) (uMask & IRRPMDIG::EquMask_MDiafr) ? 0x8000 : 0x4000);
    }
    if (uChanged & IRRPMDIG::EquMask_MScan)
    {
	s_pMainPanel ->Post_WM (CWndCallBack::EQUMSG, (WPARAM) IRRPMDIG::EquMask_MScan, (LPARAM) (uMask & IRRPMDIG::EquMask_MScan) ? 0x8000 : 0x4000);
    }
    if (uChanged & IRRPMDIG::EquMask_MFG)
    {
	s_pMainPanel ->Post_WM (CWndCallBack::EQUMSG, (WPARAM) IRRPMDIG::EquMask_MFG, (LPARAM) (uMask & IRRPMDIG::EquMask_MFG) ? 0x8000 : 0x4000);
    }
    if (uChanged & IRRPMDIG::EquMask_MM2)
    {
	s_pMainPanel ->Post_WM (CWndCallBack::EQUMSG, (WPARAM) IRRPMDIG::EquMask_MM2, (LPARAM) (uMask & IRRPMDIG::EquMask_MM2) ? 0x8000 : 0x4000);
    }
};

GResult GUIClient::InitInstance (int nArgs, void ** pArgs, HANDLE hResume)
{
    CMainPanel *   pMainPanel;

    s_pMainPanel = pMainPanel = new CMainPanel ();

    if (pMainPanel ->Create (NULL,
			     NULL, MAKEINTRESOURCE (IDD_MAINEX)))
    {
	s_pIRRPService ->SetEquCallBack (EquCallBack);

//	pMainWindow ->SetExStyles (0, WS_EX_LAYERED);
//	::SetLayeredWindowAttributes  (pMainWindow ->GetHWND (),
//	::GetSysColor (COLOR_3DFACE), (128 + 64 + 32), /*LWA_COLORKEY |*/ LWA_ALPHA);

	AddPreprocHandler (pMainPanel);

	pMainPanel ->ShowWindow (True);

	AddRef ();
    }
	return ERROR_SUCCESS;
};

GResult GAPI GUIClient::Main (int, void **, HANDLE hResume)
{
    GUIClient	a;

    CreateRRPDeviceService (s_pIRRPService, NULL);

    ::RunApartment (& a, 0, NULL, hResume);

			    s_pIRRPService ->Release ();

			    s_pIRRPService = NULL;

    printf ("\t..............Exit from RunApartment\n\n");

    return 0;
};

LRESULT CALLBACK CallWndProcHook (int code, WPARAM wParam, LPARAM lParam)
{
    if (0 <= code)
    {
void * p; _asm { mov p,esp}
printf ("  SND :\t  %08X\t\t[%08X,%08X,%08X]...%08X\n", ((CWPSTRUCT *) lParam) ->hwnd   ,
							((CWPSTRUCT *) lParam) ->message,
							((CWPSTRUCT *) lParam) ->wParam ,
							((CWPSTRUCT *) lParam) ->lParam , p);
    }
    return ::CallNextHookEx (NULL, code, wParam, lParam);
};


BOOL WINAPI EnumResLang(HMODULE hModule,
    LPCTSTR lpszType,
    LPCTSTR lpszName,
    WORD wIDLanguage,
    LONG_PTR lParam)
{

    ::FindResourceEx (hModule, lpszName, lpszType, wIDLanguage);

    ::FindResourceEx (hModule, lpszName, lpszType, MAKELANGID(LANG_NEUTRAL, SUBLANG_NEUTRAL));

    return True;
}


BOOL WINAPI EnumResTypeProc (HMODULE hModule,
			     LPTSTR lpszType,
			     LONG_PTR lParam)
{
    return True;
};

void LangProc ()
{
    char Buf [128];

    ::LoadString (NULL, IDS_FIELD_FULL, Buf, sizeof (Buf));


    ::FindResourceEx (NULL, (LPCTSTR) (RT_STRING     ),
			    (LPCTSTR) (((IDS_FIELD_FULL / 16) + 1)), g_LangID);


//  EnumResourceLanguages (NULL, MAKEINTRESOURCE (RT_STRING), MAKEINTRESOURCE (IDS_FIELD_FULL), EnumResLang, 0);


    EnumResourceTypes (NULL, EnumResTypeProc, NULL);
};


#ifdef _CONSOLE
int main (int nArgs, void ** pArgs)
#else
int WINAPI WinMain (HINSTANCE, HINSTANCE, LPSTR, int nCmdShow)
#endif
{
    LoadString (0);
/*
    LangProc ();

    SetThreadLocale (MAKELCID (LANG_RUSSIAN, SUBLANG_NEUTRAL));

    TestProc ();

	return 0;
*/
    if (GWinInit ())
    {

// Do some system initialization
//
#if FALSE
printf ("\n\n");

HHOOK hh = ::SetWindowsHookEx	 (WH_CALLWNDPROC, CallWndProcHook, NULL, ::GetCurrentThreadId ());
#endif

	if (! InitCtrl32 (ICC_BAR_CLASSES | ICC_COOL_CLASSES))
	{   goto lDone;}

#if TRUE
#if TRUE
	    g_hLedImages    = ::ImageList_LoadImage
	   (::GetModuleHandle (NULL)  , MAKEINTRESOURCE (IDB_RGB_BOXLEDS),
	    16, 0, CLR_DEFAULT,	IMAGE_BITMAP, LR_CREATEDIBSECTION);
#else
	    g_hLedImages    = ::ImageList_LoadImage
	   (::GetModuleHandle (NULL)  , MAKEINTRESOURCE (IDB_RGB_CIRCLELEDS),
	    16, 0, CLR_DEFAULT,	IMAGE_BITMAP, LR_CREATEDIBSECTION);
#endif
#else
	    g_hLedImages    = ::ImageList_LoadImage
	   (::GetModuleHandle (NULL)  , MAKEINTRESOURCE (IDB_256_BOXLEDS),
	    16, 0, CLR_DEFAULT,	IMAGE_BITMAP, LR_CREATEDIBSECTION);
#endif

//
// Do some system initialization

// Create and run main thread (Primary)
//

	    PrimaryThread <TPrivateWakeUpThread <GThread, 16> > (GUIClient::Main, 0, NULL);
lDone :

#if FALSE
if   (hh)
{
	   ::UnhookWindowsHookEx (hh);
}
#endif

	GWinDone ();
    }

    return 0;
};
//
//[]------------------------------------------------------------------------[]
