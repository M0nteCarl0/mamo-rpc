//[]------------------------------------------------------------------------[]
// Main GWin::GUI definition
//
//
// Created by GSh.
//[]------------------------------------------------------------------------[]

#include "g_guibase_new.h"

//[]------------------------------------------------------------------------[]
// User32 API extention
//
Bool GAPI InitCtrl32 (DWord dICC, DWord dCtrl32VerReq)
{
    DLLVERSIONINFO  dvi		  ;
    void *	    pDllGetVersion;
    HMODULE	    hModule	  ;
    DWORD	    dCtrl32Ver = 0;

    if (NULL != (hModule	= ::LoadLibrary	   (	   _T("comctl32.dll"))))
    {
    if (NULL != (pDllGetVersion = ::GetProcAddress (hModule, "DllGetVersion")))
    {
	memset (& dvi, 0, sizeof (dvi));
	dvi.cbSize	= sizeof (dvi) ;

	if (ERROR_SUCCESS == ((DLLGETVERSIONPROC) pDllGetVersion)(& dvi))
	{
	    dCtrl32Ver	   = MAKELONG (dvi.dwMinorVersion, 
				       dvi.dwMajorVersion);
	}
    }
	::FreeLibrary (hModule);
    }

printf ("InitCtrl32 : version %d.%d\n", HIWORD(dCtrl32Ver   ), LOWORD(dCtrl32Ver   ));
printf ("Required   :	     %d.%d\n" , HIWORD(dCtrl32VerReq), LOWORD(dCtrl32VerReq));

    if (dCtrl32Ver < dCtrl32VerReq)
    {	
	return False;
    }

    INITCOMMONCONTROLSEX  i ;

    i.dwSize	= sizeof (i);
    i.dwICC	= dICC	    ;

	return ::InitCommonControlsEx (& i);
};
//
//[]------------------------------------------------------------------------[]
//
Bool WINAPI GetClientRectEx (HWND hWnd, RECT * pr)
{

//  if (!(::GetWindowLong (hWnd, GWL_STYLE) & WS_MINIMIZE))
    {
	::GetClientRect	  (hWnd, pr);

	return True;
    }
/*
	WINDOWPLACEMENT  wp; wp.length = sizeof (wp);

	::GetWindowPlacement (hWnd, & wp);

	pr ->left   = wp.rcNormalPosition.left	 ;
	pr ->top    = wp.rcNormalPosition.top	 ;
	pr ->right  = wp.rcNormalPosition.right	 ;
	pr ->bottom = wp.rcNormalPosition.bottom ;

	::SendMessage (hWnd, WM_NCCALCSIZE, (WPARAM) FALSE, (LPARAM) pr);

	pr ->right  -= pr ->left ; pr ->left = 0;
	pr ->bottom -= pr ->top  ; pr ->top  = 0;
*/
	return False;
};

Bool WINAPI ClientToScreen  (HWND hWnd, RECT * pr)
{
    pr ->right  -= pr ->left;
    pr ->bottom -= pr ->top ;

    ::ClientToScreen (hWnd, (PPOINT) & pr ->left);

    pr ->right  += pr ->left;
    pr ->bottom += pr ->top ;

    return True;
};

Bool WINAPI ScreenToClient (HWND hWnd, RECT * pr)
{
    pr ->right  -= pr ->left;
    pr ->bottom -= pr ->top ;

    ::ScreenToClient (hWnd, (PPOINT) & pr ->left);

    pr ->right  += pr ->left;
    pr ->bottom += pr ->top ;

    return True ;
};

Bool WINAPI WindowToScreen (HWND hWnd, POINT * pp)
{
    RECT    rw;

    ::GetWindowRect (hWnd, & rw);

    Move (pp, rw.left, rw.top);

    return True;
};

Bool WINAPI WindowToScreen (HWND hWnd, RECT * pr)
{
    RECT    rw;

    ::GetWindowRect (hWnd, & rw);

    Move (pr, rw.left, rw.top);

    return True;
};

Bool WINAPI GetWindowRect (HWND hWnd, GRect * pr, HWND hFrom)
{
	::GetWindowRect   (hWnd, & pr ->Rect ());

    pr ->cx -= pr ->x;
    pr ->cy -= pr ->y;

    if (hFrom)
    {
	::MapWindowPoints (NULL, hFrom, & pr ->Pos (), 1);
    }

    return True;
};

Bool WINAPI GetWindowRect (HWND hWnd, RECT * pr, HWND hFrom)
{
    GetWindowRect (hWnd, (GRect *) pr, hFrom);

    pr ->right  += pr ->left ;
    pr ->bottom += pr ->top  ;

    return True;
};
//
//[]---------------------------------------------------------------------------
//
    const  Word *   g_pStringTable = NULL;
    extern Word	    g_LangID;

const void * WINAPI LoadResource (HMODULE hResModule, LPCTSTR pResName,
						      LPCTSTR pResType,
						      DWORD * pResSize)
{
    hResModule	 = (hResModule) ? hResModule : ::GetModuleHandle (NULL);

    HRSRC   hRes =
    
    (0 < g_LangID) ? ::FindResourceEx (hResModule, pResType, pResName, g_LangID)

		   : ::FindResource   (hResModule, pResName, pResType);

    return (hRes)  ? ((((pResSize) ? (* pResSize = ::SizeofResource (hResModule, hRes)) : NULL),
						   ::LockResource   (
						   ::LoadResource   (hResModule, hRes)))) 
					
		   : (((pResSize) ? (* pResSize = 0) : NULL)			        , NULL);
};

HWND WINAPI CreateDialogEx (HINSTANCE hInstance   ,
			    LPCTSTR   pResName	  ,
			    HWND      hWndParent  ,
			    DLGPROC   pDialogProc )
{
    return (0 < g_LangID) ? (::CreateDialogIndirectParam 

   (hInstance,  (LPCDLGTEMPLATE) LoadResource 
   
   (NULL     ,  pResName, MAKEINTRESOURCE (RT_DIALOG)),
   
    hWndParent, pDialogProc, (LPARAM) NULL))

			  : (::CreateDialog 
    
   (hInstance,  pResName, hWndParent, pDialogProc));
};

BOOL WINAPI OnResString (HMODULE, LPCTSTR pResType, LPTSTR pResName, LONG_PTR pParam)
{
    GStreamBuf * pStrBuf = (GStreamBuf *) pParam;

    DWord dSize; Word * pLength;

    const Word * pHead = (const Word *) LoadResource (NULL, pResName, pResType, & dSize);
    const Word * pTail = (pHead) ? (pHead + (dSize / 2)) : pHead;

    for (UINT uResID = ((UINT) pResName - 1) * 16; pHead < pTail; uResID ++)
    {
	if (* pHead)
	{
//printf ("Target %d\n", uResID);

	    pLength = (Word *)   pStrBuf ->putLengthW ();
				 pStrBuf ->putW ((Word) uResID);

	    pStrBuf ->pTail   += WideCharToMultiByte (

	    CP_THREAD_ACP,
//	    CP_OEMCP, 
	    
	    0, (wchar_t *)(pHead + 1), * pHead, (LPSTR) pStrBuf ->pTail, 256, NULL, NULL);

	  * pStrBuf ->pTail ++ = 0;

	if (((ULONG_PTR) pStrBuf ->pTail) & 1)
	{
	  * pStrBuf ->pTail ++ = 0;
	}
	  * pLength = pStrBuf ->pTail - (Byte *) pLength;

	    pHead += (* pHead + 1);
	}
	else
	{
	    pHead ++;    
	}
    };

    return True;
};

void LoadStringTable ()
{
    TStreamBuf <16 * 1024> StrBuf;

    if (0 < g_LangID)
	EnumResourceNamesEx (NULL, MAKEINTRESOURCE (RT_STRING), OnResString, (LONG_PTR) & StrBuf, 0, g_LangID);
    else
	EnumResourceNames   (NULL, MAKEINTRESOURCE (RT_STRING), OnResString, (LONG_PTR) & StrBuf);

    StrBuf.putLengthD ();

    g_pStringTable = (Word *) malloc (StrBuf.sizeforget ());

    memcpy ((void *) g_pStringTable, StrBuf.pBase, StrBuf.sizeforget ());
};

LPCTSTR WINAPI LoadString (UINT ResName)
{
    LPCTSTR pResult = _T("");

    if (g_pStringTable == NULL)
    {
	LoadStringTable ();
    }
    if (g_pStringTable)
    {
	for (const Word * p = g_pStringTable; * p; ((Byte *&) p) += * p)
	{
	    if (* (p + 1) == (Word) ResName)
	    {
		pResult = (LPCTSTR)(p + 2);

		break;
	    }
	}
    }

    return pResult;
};
//
//[]------------------------------------------------------------------------[]
//
int WINAPI FrameRectEx (HDC dc, const RECT * pr, HBRUSH	hBrush, int iWidth, DWORD dRop)
{
	int	cx = pr ->right  - pr ->left;
	int	cy = pr ->bottom - pr ->top ;

    if ((0 < cx) && (0 < cy))
    {
	HBRUSH  hOld = (HBRUSH) ::SelectObject (dc, hBrush);

	if ((cx < (iWidth * 2))
	||  (cy < (iWidth * 2)))
	{
	    ::PatBlt (dc, pr ->left, pr ->top, cx, cy, dRop);
	}
	else 
	{
	    cy -= (iWidth * 2);

	    ::PatBlt (dc, pr ->left,		pr ->top	    , cx    , iWidth, dRop);
	    ::PatBlt (dc, pr ->left,		pr ->top    + iWidth, iWidth, cy    , dRop);
	    ::PatBlt (dc, pr ->right  - iWidth, pr ->top    + iWidth, iWidth, cy    , dRop);
	    ::PatBlt (dc, pr ->left, 		pr ->bottom - iWidth, cx    , iWidth, dRop);
	}
				::SelectObject (dc, hOld);
	return True ;
    }
	return False;
};

int WINAPI FrameRectEx (HDC dc, const GRect * pr, HBRUSH hBrush, int iWidth, DWORD dRop)
{
    RECT	r;
    CopyRect (& r, pr);

    return FrameRectEx (dc, & r, hBrush, iWidth, dRop);
};

int WINAPI PatBlt (HDC dc, const GRect * pr, HBRUSH hBrush, DWORD dRop)
{
    HBRUSH  hOld = (HBRUSH) ::SelectObject (dc, hBrush);

    ::PatBlt (dc, pr ->x, pr ->y, pr ->cx, pr ->cy, dRop);

			    ::SelectObject (dc, hOld);
    return True;
};

int WINAPI FillRect  (HDC dc, const GRect * pr, HBRUSH hBrush)
{
    RECT	  r;
    ::CopyRect (& r, pr);

    return ::FillRect (dc, & r, hBrush);
};

Bool WINAPI DrawEdge (HDC dc, const GRect * pr, UINT edge, UINT eFlags)
{
    RECT	  r;
    ::CopyRect (& r, pr);

    return ::DrawEdge (dc, & r, edge, eFlags);
};

HBRUSH WINAPI CreateHalftoneBrush ()
{
    static const Word Pattern [8] = {0x5555, 0xAAAA, 0x5555, 0xAAAA,
				     0x5555, 0xAAAA, 0x5555, 0xAAAA};
    HBRUSH  hBrush  = NULL;
    HBITMAP hBitmap = NULL;

    if (NULL != 
       (hBitmap = ::CreateBitmap (8, 8, 1, 1, & Pattern)))
    {
	hBrush  = ::CreatePatternBrush (hBitmap);

		  ::DeleteObject       (hBitmap);
    }
	return hBrush;
};
//
//[]------------------------------------------------------------------------[]







//[]------------------------------------------------------------------------[]
//
void * GWinProcThunk::operator new (size_t sz)
{
    return ::operator new (sz);
};

void GWinProcThunk::operator delete (void * p)
{
   ::operator delete (p);
};
//
//[]------------------------------------------------------------------------[]
//
LRESULT CALLBACK GWndHookThunk::Dispatch (const GWndHookThunk * thnk, int    nCode  ,
								      WPARAM wParam ,
								      LPARAM lParam )
{
	 thnk = (const GWndHookThunk *) thnk ->GetThisPtr ();

    return  ((GWinHookProc) thnk ->pDspProc)(thnk ->oDspProc, nCode, wParam, lParam);
};
//
//[]------------------------------------------------------------------------[]
//
LRESULT CALLBACK GWndProcThunk::Dispatch (const GWndProcThunk * thnk, HWND   hWnd   ,
								      UINT   uMsg   ,
								      WPARAM wParam ,
								      LPARAM lParam )
{
	 thnk = (const GWndProcThunk *) thnk ->GetThisPtr ();

    GWinMsg m = { 0, 0, uMsg, wParam, lParam, hWnd, GWndProcThunk::DispatchDef, thnk ->oDefProc};

    // GASSERT (thnk ->pDspProc != NULL)
    //
    if (! ((GWinMsgProc) thnk ->pDspProc)(thnk ->oDspProc, m))
    {
	    m.DispatchDefault ();
    }

    return  m.lResult;
};

Bool GAPI GWndProcThunk::DispatchDef (void * pWndProcDef, GWinMsg & m)
{
//	Do this call only once !!!
//
    {
	m.pDefProc = NULL;
	m.oDefProc = NULL;
    }
//	Not work with stdctrl32::windows !!!
//
//	return m.DispatchComplete (((WNDPROC) pWndProcDef)(m.hWnd, m.uMsg, m.wParam, m.lParam));
//
	return m.DispatchComplete (::CallWindowProc 
				   ((WNDPROC) pWndProcDef, m.hWnd, m.uMsg, m.wParam, m.lParam));
};
//
//[]------------------------------------------------------------------------[]
//
INT_PTR CALLBACK GDlgProcThunk::Dispatch (const GDlgProcThunk * thnk, HWND   hWnd   ,
								      UINT   uMsg   ,
								      WPARAM wParam ,
								      LPARAM lParam )
{
	 thnk = (const GDlgProcThunk *) thnk ->GetThisPtr ();

    GWinMsg m = { 0, 0, uMsg, wParam, lParam, hWnd, GDlgProcThunk::DispatchDef, thnk ->oDefProc};

    // GASSERT (thnk ->pDspProc != NULL)
    //
    INT_PTR r = ((GWinMsgProc) thnk ->pDspProc)(thnk ->oDspProc, m);

    if (!   r)
    {
	r = m.DispatchDefault ();
    }

    if (DISPATCH_BRESULT & m.fResult)   // Microsoft exception for DialogProc...
    {					//
	    r = (INT_PTR)  m.lResult;
    }
    else 
    if (DISPATCH_LRESULT & m.fResult)
    {
	::SetWindowLong (hWnd, DWL_MSGRESULT, m.lResult);
    }

    return  r;
};

Bool GAPI GDlgProcThunk::DispatchDef (void * pDlgProcDef, GWinMsg & m)
{
//	Do this call only once !!!
//
    {
	m.pDefProc = NULL;
	m.oDefProc = NULL;
    }
	return ((DLGPROC) pDlgProcDef)(m.hWnd, m.uMsg, m.wParam, m.lParam);
};

//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
Bool GAPI GetWindowClassInfo (LPCTSTR pClassName, WNDCLASSEX * wc)
{
    wc ->cbSize		= sizeof (WNDCLASSEX);

    if (pClassName)
    {					    // Predefined Windows or global classes
					    //
    return ::GetClassInfoEx (			NULL , pClassName, wc)
    ||	   ::GetClassInfoEx (::GetModuleHandle (NULL), pClassName, wc);
    }
					    // EXT, User's local classes
					    //
    wc ->style		= 0    ;	    // May  be overwrited !!!

    wc ->lpfnWndProc	= ::DefWindowProc;  // Must be overwrited !!!

    wc ->cbClsExtra     = 0    ;
    wc ->cbWndExtra     = 0    ;
    wc ->hInstance      = NULL ;
    wc ->hIcon		= NULL ;
    wc ->hCursor	= ::LoadCursor (NULL, IDC_ARROW);

    wc ->hbrBackground  = NULL ;
    wc ->lpszMenuName   = NULL ;
    wc ->lpszClassName  = NULL ;	    // Must be overwrited !!!
    wc ->hIconSm	= NULL ;

    return True;
};

Bool GWindowClass::Register (HINSTANCE hInstance)
{
	WNDCLASSEX  wc;

//  GWinLock.LockAcquire ();
//
	if ((NULL == Registered) && GetInfo)
	{
	   (GetInfo)(& wc);

	    if ((   wc.lpszClassName != NULL )
	    &&  (   wc.lpfnWndProc   != NULL ))
	    {
		    wc.hInstance = (hInstance)
				 ?  hInstance : ::GetModuleHandle (NULL);

		if (RegisterClassEx (& wc))
		{
		    Registered = wc.hInstance;
		}
	    }
	}
//
//  GWinLock.LockRelease ();

    return NULL != Registered;
};

void GWindowClass::UnRegister ()
{
//  GWinLock.LockAcquire ();
//
	if (SYS_WINDOW_CLASS < Registered)
	{
	    ::UnregisterClass ((GetInfo)(NULL),
			       Registered);

			       Registered = NULL;
	}
//
//  GWinLock.LockRelease ();
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
int GWindow::GetCtrlText (int nCtrlId)
{
    int	    nValue   ;
    TCHAR   pBuf [64];

    GetCtrlText (nCtrlId, pBuf, sizeof (pBuf) / sizeof (TCHAR));

			  nValue = 0;
    sscanf (pBuf, "%d", & nValue);

    return  nValue   ;
};

Bool GWindow::PrintfCtrlText (int nCtrlId, LPCTSTR fmt, ...)
{
    va_list arg	      ;
    TCHAR   pBuf [256];

    va_start (arg, fmt);

    _vsnprintf (pBuf, sizeof (pBuf) / sizeof (TCHAR), fmt, arg);

    va_end   (arg);

    SetCtrlText (nCtrlId, pBuf);

    return True;
};

Bool GWindow::SetCtrlText (int nCtrlId, int nValue)
{
    TCHAR   pBuf [64];

    sprintf (pBuf, "%d", nValue);

    return SetCtrlText (nCtrlId, pBuf );
};

LRESULT GWindow::Send_WM (UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    if (GetHWND ())
    {
    return (m_pWndProc) ? ((WNDPROC)  m_pWndProc )(GetHWND (), uMsg, wParam, lParam)
			:	    ::SendMessage (GetHWND (), uMsg, wParam, lParam);
    }
    return 0L;
};

Bool GWindow::PreprocMessage (MSG *)
{
    return False;
};
//
//[]------------------------------------------------------------------------[]
//
int GWindow::IsCommand (GWinMsg & m)
{
    if ((m.hWnd == GetHWND ()) && (WM_COMMAND == m.uMsg) && (1 >= (int) m.HiwParam))
    {
	return (int) m.LowParam;
    } 
	return 0;
};

int GWinMsg::IsCtrlNotify (HWND * pHandle)
{
    HWND    hCtrl   = NULL;
    int	    nCtrlId = 0	  ;

    switch (uMsg)
    {
    case WM_COMMAND :

	if (NULL != (hCtrl   = lWnd))
	{
		     nCtrlId = LowParam;
	}
	    break;

    case WM_NOTIFY :

	if (NULL != (hCtrl   = pNMHDR ->hwndFrom))
	{
		     nCtrlId = pNMHDR ->idFrom  ;
	}
	    break;

    case WM_PARENTNOTIFY :
	    break;

    case WM_DRAWITEM :

	if (wParam 
	&& (NULL != (hCtrl   = pDRAWITEMSTRUCT ->hwndItem)))
	{
		     nCtrlId = (int) wParam;
	}
	    break;

    case WM_MEASUREITEM :

	if (wParam && (ODT_MENU != pMEASUREITEMSTRUCT ->CtlType))
	{
		     nCtrlId = (int) wParam;
	}
	    break;

    case WM_COMPAREITEM :

	if (NULL != (hCtrl   = pCOMPAREITEMSTRUCT ->hwndItem))
	{
		     nCtrlId = (int) wParam;
//		     nCtrlId = pCOMPAREITEMSTRUCT ->CtlID;
	}
	    break;

    case WM_DELETEITEM :

	if (NULL != (hCtrl   = pDELETEITEMSTRUCT ->hwndItem))
	{
		     nCtrlId = (int) wParam;
//		     nCtrlId = pDELETEITEMSTRUCT  ->CtlID;
	}
	    break;

    case WM_VKEYTOITEM		:
    case WM_CHARTOITEM		:
    case WM_HSCROLL		:
    case WM_VSCROLL		:
    case WM_CTLCOLORBTN		:
    case WM_CTLCOLORDLG		:
    case WM_CTLCOLOREDIT	:
    case WM_CTLCOLORLISTBOX	:
    case WM_CTLCOLORMSGBOX	:
    case WM_CTLCOLORSCROLLBAR	:
    case WM_CTLCOLORSTATIC	:

	if (NULL != (hCtrl   = lWnd))
	{
		     nCtrlId = ::GetDlgCtrlID (hCtrl);
	}
	    break;
    };

    if (nCtrlId && pHandle)
    {
	return (NULL != (* pHandle = (hCtrl) ? hCtrl : NULL/*::GetDlgItem (hWnd, nCtrlId)*/)) ? nCtrlId : 0;
    }
	return nCtrlId;
};

int GWindow::IsControlNotify (GWinMsg & m, HWND * pHandle)
{
    HWND    hCtrl   = NULL;
    int	    nCtrlId = 0	  ;

    if (m.hWnd == GetHWND ())
    {
    switch (m.uMsg)
    {
    case WM_COMMAND :

	if (NULL != (hCtrl   = m.lWnd))
	{
		     nCtrlId = m.LowParam;
	}
	    break;

    case WM_NOTIFY :

	if (NULL != (hCtrl   = m.pNMHDR ->hwndFrom))
	{
		     nCtrlId = m.pNMHDR ->idFrom  ;
	}
	    break;

    case WM_PARENTNOTIFY :
	    break;

    case WM_DRAWITEM :

	if (m.wParam 
	&& (NULL != (hCtrl   = m.pDRAWITEMSTRUCT ->hwndItem)))
	{
		     nCtrlId = (int) m.wParam;
	}
	    break;

    case WM_MEASUREITEM :

	if (m.wParam && (ODT_MENU != m.pMEASUREITEMSTRUCT ->CtlType))
	{
		     nCtrlId = (int) m.wParam;
	}
	    break;

    case WM_COMPAREITEM :

	if (NULL != (hCtrl   = m.pCOMPAREITEMSTRUCT ->hwndItem))
	{
		     nCtrlId = (int) m.wParam;
//		     nCtrlId = m.pCOMPAREITEMSTRUCT ->CtlID;
	}
	    break;

    case WM_DELETEITEM :

	if (NULL != (hCtrl   = m.pDELETEITEMSTRUCT ->hwndItem))
	{
		     nCtrlId = (int) m.wParam;
//		     nCtrlId = m.pDELETEITEMSTRUCT  ->CtlID;
	}
	    break;

    case WM_VKEYTOITEM		:
    case WM_CHARTOITEM		:
    case WM_HSCROLL		:
    case WM_VSCROLL		:
    case WM_CTLCOLORBTN		:
    case WM_CTLCOLORDLG		:
    case WM_CTLCOLOREDIT	:
    case WM_CTLCOLORLISTBOX	:
    case WM_CTLCOLORMSGBOX	:
    case WM_CTLCOLORSCROLLBAR	:
    case WM_CTLCOLORSTATIC	:

	if (NULL != (hCtrl   = m.lWnd))
	{
		     nCtrlId = GetCtrlId (hCtrl);
	}
	    break;
    };
    }

    if (nCtrlId && pHandle)
    {
	return (NULL != (* pHandle = (hCtrl) ? hCtrl : GetCtrlHWND (nCtrlId))) ? nCtrlId : 0;
    }
	return nCtrlId;
};

Bool GWindow::On_ReflectWinMsg (GWinMsg & m)
{
    return False;
};

Bool GWindow::On_WinMsg (GWinMsg & m)
{
    if (WM_MOUSEACTIVATE == m.uMsg)
    {
//  printf ("\t\t\t\t...Mouse activate %08X (%08X)\n", m.hWnd, m.wParam);
    }
    if (WM_SETFOCUS  == m.uMsg)
    {
//  printf ("\t\t\t\t...Set  focus (%08X <- %08X)\n", m.hWnd, m.wParam);
    }
    if (WM_KILLFOCUS == m.uMsg)
    {
//  printf ("\t\t\t\t...Lost focus (%08X -> %08X)\n", m.hWnd, m.wParam);
    }


    if (WM_USER + 3000 == m.uMsg)
    {
	printf ("<<Enter User\n");
	
//	    TCurrent () ->GetCurApartment () ->WaitInputWork ();

	return m.DispatchComplete ();
    }
    if (WM_ENTERIDLE == m.uMsg)
    {
//	printf ("<<Enter Idle\n");

//	    while (TCurrent () ->GetCurApartment () ->PumpInputWork ())
//	    {}
//		   TCurrent () ->GetCurApartment () ->WaitInputWork ();

	return m.DispatchComplete ();
    }
    if (WM_ENTERMENULOOP == m.uMsg)
    {
//	printf ("<<Enter MenuLoop\n");

//	    TCurrent () ->GetCurApartment () ->WaitInputWork ();

	return m.DispatchComplete ();
    }
    if (WM_ENTERSIZEMOVE == m.uMsg)
    {
//	printf ("<<Enter SizeMove\n");

//	    TCurrent () ->GetCurApartment () ->WaitInputWork ();

	return m.DispatchComplete ();
    }
    if (WM_EXITMENULOOP == m.uMsg)
    {
//	printf ("  Leave MenuLoop>>\n");

	return m.DispatchComplete ();
    }
	return False;
};

#define TRACE_DispatchWinMsg	    0

Bool GAPI GWindow::DispatchWinMsg (void * oDsp, GWinMsg & m)
{
    GWindow * w = (GWindow *) oDsp;

    if (w ->HasFlag (GWF_CREATED))
    {
#if TRACE_DispatchWinMsg

printf ("<<WinMsg (%08X) -> %08X {%08X %08X %08X}\n", m.hWnd, w ->GetHWND (), m.uMsg, m.wParam, m.lParam);

    Bool ret = (w ->On_WinMsg (m)) ? True : m.DispatchDefault ();

printf ("WinMsg>>\n");

	return ret;
#else
	return (w ->On_WinMsg (m)) ? True : m.DispatchDefault ();
#endif
    }

#if TRACE_DispatchWinMsg
printf ("<<WinMsg (%08X) -> %08X {%08X %08X %08X}... Crt/Dst>>\n", m.hWnd, w ->GetHWND (), m.uMsg, m.wParam, m.lParam);
#endif
	       (w ->On_WinMsg (m)) ? True : m.DispatchDefault ();

    if (WM_CREATE     == m.uMsg)
    {
	if (m.lResult == 0)
	{
	    m.lResult = (w ->On_Create (((CREATESTRUCT *) m.lParam) ->lpCreateParams)) ? 0 : (w ->On_Destroy (), -1);
	}
	if (m.lResult == 0)
	{
	    w ->SetFlags (0, GWF_CREATED);
	}
    }
    else
    if (WM_NCDESTROY  == m.uMsg)
    {
	w ->UnSubclassWndProc ();

	w ->m_hWnd     = NULL;
	w ->m_pParent  = NULL;
    }
	return m.DispatchComplete ();
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
GWindow::~GWindow ()
{
    printf ("::~GWindow {%08X-%08X}\n", this, GetIdPtr ());

    Destroy ();

    if (m_pWndProc)
    {
	delete m_pWndProc;
	       m_pWndProc = NULL;
    }
};
//
//[]------------------------------------------------------------------------[]
//

DWord GWindow::SetCtrlStyles (int nCtrlId, DWord dClrMask, DWord dSetMask) const
{
    HWND	 hCtrl;

    if (NULL != (hCtrl = ::GetDlgItem (GetHWND (), nCtrlId)))
    {
	return ::SetWindowLong (hCtrl, GWL_STYLE,
	       ::GetWindowLong (hCtrl, GWL_STYLE) & (~dClrMask) | dSetMask);
    }
	return 0;
};

Bool GWindow::SetWindowPos (HWND    hAfter  ,
			    int	    x	    ,
			    int	    y	    ,
			    int	    cx	    ,
			    int	    cy	    ,
			    UINT    uFlags  )
{
        uFlags |= (SWP_NOACTIVATE);

    if (uFlags &   SWP_NOREDRAW)
    {
	uFlags |= (SWP_NOCOPYBITS);
    }
    if (NULL == hAfter)
    {
	uFlags |= (SWP_NOZORDER | SWP_NOOWNERZORDER);
    }
    return ::SetWindowPos (GetHWND ()  ,
			   hAfter      ,
			   x, y, cx, cy, uFlags);
};
//
//[]------------------------------------------------------------------------[]
//
void GWindow::On_Destroy ()
{
printf ("GWindow::On_Destroy (%08X:%08X)\n", this, GetHWND ());
};

Bool GWindow::On_Create (void *)
{
    return True;
};

void GWindow::Destroy ()
{
    if (GetHWND ())
    {
	       On_Destroy ();

	UnSubclassWndProc ();

	::DestroyWindow (GetHWND ());

	m_hWnd	      = NULL;
	m_pParent     = NULL;

	SetFlags (GWF_CREATED, 0);
    }
};

Bool GWindow::Create (GWindow * pParent, HWND hWnd, void * pParam)
{
    if (GetHWND () || (NULL == hWnd))
    {
	return False;
    }

    if (WS_CHILD & ::GetWindowLong (hWnd, GWL_STYLE))
    {
	if ((pParent		  == NULL			    )
	||  (pParent ->GetHWND () == NULL			    )
	||  (pParent ->GetHWND () != ::GetAncestor (hWnd, GA_PARENT)))
	{
	return False;
	}

	m_pParent = pParent;
    }
	m_hWnd	  = hWnd   ;

    if (! On_Create (pParam))
    {
	m_pParent = pParent;
	m_hWnd	  = NULL   ;

	return False;
    }
	SetFlags (0, GWF_CREATED);

	return True ;
};

static LRESULT WINAPI _failWndProc (HWND hWnd, UINT uMsg, WPARAM wParam ,
							  LPARAM lParam )
{
    if (WM_NCCREATE == uMsg)
    {
	return  0;		// Abort create window
    }
    if (WM_CREATE   == uMsg)
    {
	return -1;		// Abort create window
    }
	return ::DefWindowProc (hWnd, uMsg, wParam, lParam);
};

LRESULT WINAPI GWindow::_initWndProc (HWND hWnd, UINT uMsg, WPARAM wParam ,
							    LPARAM lParam )
{
    WNDPROC   t	    =		 _failWndProc ;
    void    * a [3] = {(void *)  _initWndProc ,
		       (void *)	  hWnd	      ,
		       (void *) & t	      };

    ::SetWindowLongPtr (hWnd, GWLP_WNDPROC, (LONG_PTR) t);

    ::RaiseException   (0, 3, a);

    ::SetWindowLongPtr (hWnd, GWLP_WNDPROC, (LONG_PTR) t);

    return (t) (hWnd, uMsg, wParam, lParam);
};

Bool GAPI GWindow::CreateException (void * pThis, GException_Record & r)
{
    if ((3			   <= r.nExParams    )
    &&  ((void    *) _initWndProc  == r.pExParams [0]))
    {
	((GWindow *) pThis) ->m_hWnd	 =
        ((HWND     )(r.pExParams [1]));

      *	((void   **) r.pExParams [2])    =
	((GWindow *) pThis) ->m_pWndProc = new GWndProcThunk 
	(DispatchWinMsg, pThis, ::DefWindowProc);

	return True ;
    }
	return False;
};

Bool GWindow::Create (GWindow * pParent, CreationParams & cParams)
{
    if (GetHWND ())
    {
	return False;
    }

    GWindowClass & hWndClass  = GetWindowClass ();
    HWND	   hWnd       = NULL ;
    HWND	   hWndParent = NULL ;
    DWord	   dStyle     = cParams.dStyle;

    if ((NULL ==   hWndClass.Registered	 )
    &&  (        ! hWndClass.Register  ()))
    {
	return False;
    }

    if (cParams.dStyle & WS_CHILD)
    {
	if ((NULL ==  pParent) 
	||  (NULL == (hWndParent = pParent ->GetHWND ())))
	{
	    return False;
	}
	    cParams.hMenuOrId  = (HMENU) GetId ();

	if (pParent ->IsVisible () && (cParams.dStyle & WS_VISIBLE))
	{
	    cParams.dStyle    &= (~(WS_VISIBLE));
	}
	    m_pParent = pParent;
    }
    else
    {
	if (pParent
	&& (NULL == (hWndParent = pParent ->GetHWND ())))
	{
	    return False;
	}
	if (cParams.dStyle    &    (WS_VISIBLE | WS_MINIMIZE))
	{
	    cParams.dStyle    &= (~(WS_VISIBLE | WS_MINIMIZE));
	}
    }

    {
	DEFINE_EXCEPTION_BLOCK (CreateException, this)
	{
	hWnd = ::CreateWindowEx  (cParams.dExStyle  ,
	      (hWndClass.GetInfo)(NULL)		    ,
				  cParams.pTitle    ,
				  cParams.dStyle    ,
				  cParams.x	    ,
				  cParams.y	    ,
				  cParams.cx	    ,
				  cParams.cy	    ,
				  hWndParent	    ,
				  cParams.hMenuOrId ,
	       ::GetModuleHandle (NULL)		    ,
				  cParams.pParam    );
	}
	ENDDEF_EXCEPTION_BLOCK ();

	if (NULL == GetHWND ())
	{
	    m_hWnd = hWnd ;
	}
    }

    if (hWnd)
    {
	if (GetHWND () == NULL)
	{
	      m_hWnd = hWnd;

	    if (On_Create (cParams.pParam))
	    {
		SetFlags  (0, GWF_CREATED);
	    }
	    else
	    {
		Destroy  ();

		hWnd = NULL;
	    }
	}
	if (GetHWND ())
	{
		dStyle ^= cParams.dStyle;

	    if (dStyle & WS_VISIBLE)
	    {
		::ShowWindow (hWnd, (dStyle & WS_MINIMIZE) ? SW_SHOWMINIMIZED : SW_SHOW);
	    }
	    else
	    if (dStyle & WS_MINIMIZE)
	    {{
		::ShowWindow (hWnd, SW_MINIMIZE);
		::ShowWindow (hWnd, SW_HIDE    );

    printf ("\t\t%s %s\n", HasStyles (0, WS_VISIBLE) ? "Visible" : "", HasStyles (0, WS_MINIMIZE) ? "Minimized" : "");
	    }}

	    return GetHWND () ? True : False;
	}
    }
	    m_pParent = NULL;

	    return GetHWND () ? True : False;
};

Bool GWindow::Create ( GWindow * pParent  ,
		       LPCTSTR	 pTitle   ,
		       DWord	 dStyle   ,
		       DWord	 dExStyle ,
		       int	 x	  ,
		       int	 y	  ,
		       int	 cx	  ,
		       int	 cy	  ,
		       void *	 pParam   )
{
	CreationParams		 cParams  ;

	cParams.pTitle	       = pTitle   ;
	cParams.dStyle	       = dStyle   ;
	cParams.dExStyle       = dExStyle ;
	cParams.x	       = x	  ;
	cParams.y	       = y	  ;
	cParams.cx	       = cx	  ;
	cParams.cy	       = cy	  ;
	cParams.hMenuOrId      = NULL	  ;
	cParams.pParam	       = pParam   ;

	return Create (pParent, cParams);
};
//
//[]------------------------------------------------------------------------[]
//
DWord GWindow::ExecWindow ()
{
    LONG    bExitFlag =	    1 ;
    DWord   dExitCode =	   -1 ;
    Bool    bEnabled  = False ;
    Bool    bHiden    = False ;
    HWND    hOwner    = NULL  ;

    if (NULL ==	GetHWND ())
    {
	return dExitCode;
    }
	hOwner = ::GetWindow (GetHWND (), GW_OWNER);

					// Save current visible state and...
	bHiden = ! IsVisible ();
					// Show window
	ShowWindow (True, True);
					// Disable owner if it exist & not disabled
    if (hOwner)
    {
	bEnabled = True;

    if (bEnabled = ::IsWindowEnabled (hOwner))
    {
	::EnableWindow (hOwner, False);
    }
    }
					// Get exec code
printf ("\t<<Exec: %08X\n",   GetHWND ());

	dExitCode = TCurrent () ->GetCurApartment () ->WorkLoop (& bExitFlag);

printf ("\t  Exec: %08X>>\n\n", GetHWND ());
					// Restore Owner's enable
    if (bEnabled)
    {
	::EnableWindow (hOwner, True );
    }
					// Hide if was not visible
    if (bHiden)
    {
	ShowWindow (False);
    }
	return dExitCode;
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
DEFINE_EXT_WINDOW_CLASS (GParentWindow);

Bool GParentWindow::PreprocMessage (MSG * msg)
{
    if (((WM_KEYFIRST   > msg ->message) || (msg ->message > WM_KEYLAST  ))
    &&  ((WM_MOUSEFIRST > msg ->message) || (msg ->message > WM_MOUSELAST)))
    {
	    return False;
    }

    HWND hCtrl = ::GetFocus ();

    if  (hCtrl && IsChild (GetHWND (), hCtrl))
    {
	for (; GetHWND () != ::GetParent (hCtrl); hCtrl = ::GetParent (hCtrl));

	if  (0 != ::SendMessage (hCtrl, WM_FORWARDMSG, (WPARAM) 0, (LPARAM) msg))
	{
	    return True;
	}
    }

    if (::IsDialogMessage  (GetHWND (), msg))
    {
	return True;
    }
	return GWindow::PreprocMessage (msg);
};

Bool GParentWindow::On_WinMsg (GWinMsg & m)
{
    if (WM_FORWARDMSG == m.uMsg)
    {
	return m.DispatchComplete (PreprocMessage ((MSG *) m.lParam));
    }
	return GWindow::On_WinMsg (m);
};


#if FALSE
Bool GParentWindow::DispatchWinMsg (GWinMsg::Type t, GWinMsg & m)
{
    switch (t)
    {
    case GWinMsg::Internal :

	DISPATCH_GM	(GM_RegisterCtrlNotify,	On_GM_RegisterCtrlNotify)

	return GWindow::DispatchWinMsg (m);
/*
    case GWinMsg::WndProcCrt :

	DISPATCH_WM	(WM_NOTIFY	      ,	On_WM_Notify	    )
	DISPATCH_WM	(WM_COMMAND	      ,	On_WM_Command	    )
	DISPATCH_WM	(WM_MEASUREITEM	      , On_WM_MeasureItem   )

	return GWindow::DispatchWinMsg (m);
*/
    case GWinMsg::WndProc :

    };
	return GWindow::DispatchWinMsg (m);
};
#endif
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
DEFINE_EXT_WINDOW_CLASS (GFrameWindow);
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
// Dialog template wrapper
//
//
#pragma pack (1)

    typedef struct
    {
	DWORD	    style	;
	DWORD	    exStyle	;
	short	    x		;
	short	    y		;
	short	    cx		;
	short	    cy		;
	WORD	    id		;
	WORD	    reserved	;
//	sz_Or_Ord   windowClass	;
//	sz_Or_Ord   title	;
//	WORD	    extraCount	;
    } GDLGITEMTEMPLATE;

    typedef struct
    { 
	DWORD	    helpID	;
	DWORD	    exStyle	;
	DWORD	    style	;
	short	    x		;
	short	    y		;
	short	    cx		;
	short	    cy		;
	WORD	    id		;
	WORD	    resereved	;
//	DWORD	    id		;
//	sz_Or_Ord   windowClass	;
//	sz_Or_Ord   title	;
//	WORD	    extraCount	;

    } GDLGITEMTEMPLATEEX; 

    typedef struct
    {
	DWORD	    style	;
	DWORD	    exStyle	;
	WORD	    cDlgItems	;
	short	    x		;
	short	    y		;
	short	    cx		;
	short	    cy		;
//	sz_Or_Ord   menu		;
//	sz_Or_Ord   windowClass		;
//	WCHAR	    title    [titleLen] ;
    } GDLGTEMPLATE;

    typedef struct
    { 
        WORD	    dlgVer	;
        WORD	    signature	;
	DWORD	    helpID	;
	DWORD	    exStyle	;
	DWORD	    style	;
	WORD	    cDlgItems	;
	short	    x		;
	short	    y		;
	short	    cx		;
	short	    cy		;
//	sz_Or_Ord   menu		;
//	sz_Or_Ord   windowClass		;
//	WCHAR	    title    [titleLen] ;
//	WORD	    pointsize		; if (DS_SETFONT or DS_SHELLFONT)
//	WORD	    weight		; if (DS_SETFONT or DS_SHELLFONT)
//	BYTE	    italic		; if (DS_SETFONT or DS_SHELLFONT)
//	BYTE	    charset		; if (DS_SETFONT or DS_SHELLFONT)
//	WCHAR	    typeface [stringLen];
    } GDLGTEMPLATEEX;

    typedef struct
    {
	WORD	    pointsize	;
	WORD	    weight	;
	BYTE	    italic	;
	BYTE	    charset	;
	WCHAR	    typeface [1];

    } GDLGTEMPLATEEX_FONT;

#pragma pack () 

static const void * __skip_sz_Or_Ord (const void * p)
{
    if	    (0x0000 == * (WORD *) p)
    {
	((WORD  *&) p) += 1;
    }
    else if (0xFFFF == * (WORD *) p)
    {   
	((WORD  *&) p) += 2;
    }
    else
    {   
	((WCHAR *&) p) += (1 + ::lstrlenW ((WCHAR *) p));
    }
	return p;
};

static const GDLGITEMTEMPLATE * __getDlgItemTemplate (const GDLGTEMPLATE * t, int nCtrlId)
{
    const void * p;
    int	      j, i;

    {
	p = __skip_sz_Or_Ord (t + 1);	// menu
	p = __skip_sz_Or_Ord (p	   );	// windowClass
	p = __skip_sz_Or_Ord (p	   );	// title
    }
    for (i = 0; i < t ->cDlgItems; i ++)
    {
	if (nCtrlId == ((const GDLGITEMTEMPLATE *) p) ->id)
	{
	    return	(const GDLGITEMTEMPLATE *) p;
	}
	p = __skip_sz_Or_Ord (((const GDLGITEMTEMPLATE *) p) + 1);
					// windowClass
	p = __skip_sz_Or_Ord (p	    );	// title
	j = 2 + 
	    * (const WORD  *)(p	    );	// extraCount
	     ((const BYTE *&) p) += j;

	p =   (const void * ) roundup (p, sizeof (DWORD));
    }
	    return NULL;
};

#ifndef DS_SHELLFONT
#define DS_SHELLFONT (DS_SETFONT | DS_FIXEDSYS)
#endif

static const GDLGITEMTEMPLATEEX * __getDlgItemTemplate (const GDLGTEMPLATEEX * e, int nCtrlId)
{
    const void * p;
    int	      j, i;

    {
	p = __skip_sz_Or_Ord (e + 1);	// menu
	p = __skip_sz_Or_Ord (p	   );	// windowClass
	p = __skip_sz_Or_Ord (p	   );	// title

	if ((DS_SETFONT | DS_SHELLFONT) & e ->style)
	{				
					// pointsize, weight, italic, charset
	    ((const Byte *&) p) += (2 + 2 + 1 + 1);

	p = __skip_sz_Or_Ord (p	   );
	}
    }
    for (i = 0; i < e ->cDlgItems; i ++)
    {
	if (nCtrlId == ((const GDLGITEMTEMPLATEEX *) p) ->id)
	{
	    return	(const GDLGITEMTEMPLATEEX *) p;
	}

	p = __skip_sz_Or_Ord (((const GDLGITEMTEMPLATEEX *) p) + 1);
					// windowClass
	p = __skip_sz_Or_Ord (p	    );	// title
	j = 2 + 
	    * (const WORD  *)(p	    );	// extraCount
	     ((const BYTE *&) p) += j;

	p =   (const void * ) roundup (p, sizeof (DWORD));
    }
	    return NULL;
};

Bool WINAPI GetDlgTemplateItemInfo (LPCDLGTEMPLATE pTemplate, int       nCtrlId	 ,
							      DWORD   * pStyle   ,
							      DWORD   * pExStyle ,
							      GRect   * pRect	 ,
							      LPCWSTR * pTitle  = NULL,
							      LPCWSTR * pClass	= NULL,
							      DWORD   * pHelpId = NULL)
{
    union
    {
	 const GDLGTEMPLATEEX *	 e;
	 const GDLGTEMPLATE   *  t;
    };
    union
    {
    const GDLGITEMTEMPLATEEX  * ie;
    const GDLGITEMTEMPLATE    * it;
    };
	 const void *		 p;

    p = e = (const GDLGTEMPLATEEX *) pTemplate;

    if (e)
    {
	if (0xFFFF != (e ->signature))	// This is the GDLGTEMPLATE record
	{				//
	    if (NULL != (p = it = __getDlgItemTemplate (t, nCtrlId)))
	    {
		if (pHelpId)
		{
		  * pHelpId  = NULL;
		}
		if (pStyle  )
		{
		  * pStyle   = it ->style  ;
		}
		if (pExStyle)
		{
		  * pExStyle = it ->exStyle;
		}
		if (pRect   )
		{
		    pRect ->Assign (it ->x, it ->y, it ->cx, it ->cy);
		}

		if (pClass  )
		{
		  * pClass   = (LPCWSTR) p;
		}
		p = __skip_sz_Or_Ord (p);

		if (pTitle  )
		{
		  * pTitle   = (LPCWSTR) p;
		}
		p = __skip_sz_Or_Ord (p);

		return True;
	    }
	}
	else				// This is the GDLGTEMPLATEEX record
	{				//
	    if (NULL != (p = ie = __getDlgItemTemplate (e, nCtrlId)))
	    {
		if (pHelpId)
		{
		  * pHelpId  = ie ->helpID ;
		}
		if (pStyle  )
		{
		  * pStyle   = ie ->style  ;
		}
		if (pExStyle)
		{
		  * pExStyle = ie ->exStyle;
		}
		if (pRect   )
		{
		    pRect ->Assign (ie ->x, ie ->y, ie ->cx, ie ->cy);
		}

		if (pClass  )
		{
		  * pClass   = (LPCWSTR) p;
		}
		p = __skip_sz_Or_Ord (p);

		if (pTitle  )
		{
		  * pTitle   = (LPCWSTR) p;
		}
		p = __skip_sz_Or_Ord (p);


		return True;
	    }
	}
    }
	return False;
};

Bool WINAPI GetDlgTemplateInfo (LPCDLGTEMPLATE pTemplate, DWORD   * pStyle   ,
							  DWORD   * pExStyle ,
							  GRect   * pRect    ,
							  LPCWSTR * pTitle    = NULL,
							  LPCWSTR * pClass    = NULL,
							  LPCVOID * pMenuOrId = NULL,
						    const GDLGTEMPLATEEX_FONT **
								    pFont     = NULL)
{
    union
    {
	 const GDLGTEMPLATEEX *	 e;
	 const GDLGTEMPLATE   *  t;
    };
	 const void *		 p;

    p = e = (const GDLGTEMPLATEEX *) pTemplate;

    if (e)
    {
	if (0xFFFF != (e ->signature))	// This is the GDLGTEMPLATE record
	{				//
	    if (pStyle	 ) 
	    {
	      * pStyle    = t ->style	 ;
	    }
	    if (pExStyle )
	    {
	      * pExStyle  = t ->exStyle;
	    }
	    if (pRect	 ) 
	    {
		pRect ->Assign (t ->x, t ->y, t ->cx, t ->cy);
	    }

	    if (pMenuOrId)
	    {
	      * pMenuOrId = t + 1;
	    }
	    p = __skip_sz_Or_Ord (t + 1);

	    if (pClass	 )
	    {
	      * pClass = (LPCWSTR) p;
	    }
	    p = __skip_sz_Or_Ord (p    );

	    if (pTitle	 )
	    {
	      * pTitle = (LPCWSTR) p;
	    }
	    p = __skip_sz_Or_Ord (p    );

	    if (pFont	 )
	    {
	      * pFont  = NULL;
	    }
	}
	else				// This is the GDLGTEMPLATEEX record
	{				//
	    if (pStyle	 ) 
	    {
	      * pStyle    = e ->style	 ;
	    }
	    if (pExStyle )
	    {
	      * pExStyle  = e ->exStyle;
	    }
	    if (pRect	 ) 
	    {
		pRect ->Assign (e ->x, e ->y, e ->cx, e ->cy);
	    }

	    if (pMenuOrId)
	    {
	      * pMenuOrId = e + 1;
	    }
	    p = __skip_sz_Or_Ord (e + 1);

	    if (pClass	 )
	    {
	      * pClass = (LPCWSTR) p;
	    }
	    p = __skip_sz_Or_Ord (p    );

	    if (pTitle	 )
	    {
	      * pTitle = (LPCWSTR) p;
	    }
	    p = __skip_sz_Or_Ord (p    );

	    if ((DS_SETFONT | DS_SHELLFONT) & e ->style)
	    {
		if  (pFont	    )
		{
		   * pFont  = (const GDLGTEMPLATEEX_FONT *) p;
		}
		((const Byte *&) p) += (2 + 2 + 1 + 1);

		p = __skip_sz_Or_Ord (p    );
	    }
	    else if (pFont)
	    {
		   * pFont  = NULL;
	    }
	}
	    return True;
    }
	    return False;
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
DEFINE_SYS_WINDOW_CLASS (GDialog);

GDialog::~GDialog ()
{
    Destroy ();

    if (m_pDlgProc)
    {
	delete m_pDlgProc;
	       m_pDlgProc = NULL;
    }
};

GWindow * GDialog::GetCreationWindow (int nCtrlId)
{
    return NULL;
};

static INT_PTR CALLBACK _nullDlgProc (HWND, UINT, WPARAM, LPARAM)
{
    return False;
};

INT_PTR WINAPI GDialog::_initDlgProc (HWND hWnd, UINT uMsg, WPARAM wParam ,
							    LPARAM lParam )
{
    DLGPROC   t	    =		  NULL	      ;
    void    * a [3] = {(void *)  _initDlgProc ,
		       (void *)	  hWnd	      ,
		       (void *) & t	      };

    ::RaiseException (0, 3, a);

    if (t)
    {
        ::SetWindowLongPtr (hWnd, DWLP_DLGPROC, (LONG_PTR) t);

        return (INT_PTR)(t)(hWnd, uMsg, wParam, lParam);
    }
	::SetWindowLongPtr (hWnd, DWLP_DLGPROC, (LONG_PTR) _nullDlgProc);

	::DestroyWindow	   (hWnd);

	return (INT_PTR) False;
};

Bool GAPI GDialog::CreateException (void * pThis, GException_Record & r)
{
    if ((3			  <= r.nExParams    )
    &&	((void	  *) _initDlgProc == r.pExParams [0]))
    {
	((GDialog *) pThis) ->m_hWnd	 =
      	((HWND     )(r.pExParams [1]));

      * ((void	 **) r.pExParams [2])	 =
	((GDialog *) pThis) ->m_pDlgProc = new GDlgProcThunk 
	(DispatchDlgMsg, pThis, _nullDlgProc);

	((GDialog *) pThis) ->SubclassWndProc 
	(DispatchWinMsg, pThis);

	return True ;
    }
    else
    if ((3			  <= r.nExParams    )
    &&	((void	  *) _initWndProc == r.pExParams [0]))
    {{
/*
	GWindow * w = GetCreationWindow 
       (::GetWindowLong ((HWND) r ->ExceptionInformation [1], GWL_ID));

	if (w && (NULL == w ->GetHWND ()))
	{
	    w ->m_pParent = this;

	    return GWindow::CreateException (w, r);
	}
*/
    }}
	return False;
};

Bool GDialog::Create (GWindow * pParent	  ,
		      HMODULE	hResModule,
		      LPCTSTR	pResName  ,
		      void *	pParam	  )
{
    if (GetHWND ())
    {
	return False;
    }

    HWND	   hWndParent = NULL;
    DWord	   dStyle     =    0;
    LPCDLGTEMPLATE pTemplate  = (LPCDLGTEMPLATE) LoadResource (hResModule, pResName, RT_DIALOG);

    if ((NULL == pTemplate) || (! GetDlgTemplateInfo (pTemplate, & dStyle, NULL, NULL)))
    {
	return False;
    }

    if (dStyle & WS_CHILD)
    {
	if ((NULL ==  pParent)
	||  (NULL == (hWndParent = pParent ->GetHWND ())))
	{
	    return False;
	}
	    m_pParent = pParent;
    }
    else
    {
	if  (	      pParent
	&&  (NULL == (hWndParent = pParent ->GetHWND ())))
	{
	    return False;
	}
    }

    DEFINE_EXCEPTION_BLOCK (CreateException, this)
    {
	::CreateDialogIndirectParam
       (::GetModuleHandle (NULL)       ,
			   pTemplate   ,
			   hWndParent  ,
			  _initDlgProc ,
		 (LPARAM ) pParam      );
    }
    ENDDEF_EXCEPTION_BLOCK ();

    return (GetHWND ()) ? True : (m_pParent = NULL, False);
};

Bool GDialog::On_InitDialog (GWinMsg & m)
{						// Set default focus
						//
	return m.DispatchCompleteDlgEx ((LRESULT) True);
};

Bool GDialog::On_DlgMsg (GWinMsg &)
{
	return False;
};

Bool GAPI GDialog::DispatchDlgMsg (void * oDsp, GWinMsg & m)
{
    GDialog * w = (GDialog *) oDsp;

    if (w ->HasFlag (GWF_CREATED))
    {
#if TRUE
	return w ->On_DlgMsg (m);
#else

printf ("<<DlgMsg (%08X) -> %08X {%08X %08X %08X}\n", m.hWnd, w ->GetHWND (), m.uMsg, m.wParam, m.lParam);

    Bool ret = w ->On_DlgMsg (m);

printf ("DlgMsg>>\n");

	return ret;
#endif
    }

    if (WM_INITDIALOG == m.uMsg)
    {
    if ((w ->On_Create ((void *) m.lParam)) ? True : (w ->On_Destroy (), False))
    {
	w ->SetFlags   (0, GWF_CREATED);

	return w ->On_InitDialog (m);
    }
	return False;
    }
    else
    if (WM_NCDESTROY   == m.uMsg)
    {
	w ->UnSubclassDlgProc ();

    if (w ->m_pWndProc == NULL  )
    {
	w ->m_hWnd      = NULL;
	w ->m_pParent   = NULL;
    }
    }
	return w ->On_DlgMsg (m);
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
DEFINE_EXT_WINDOW_CLASS (GFrameControl);

void GFrameControl::On_Destroy ()
{
    if (m_pWndProc ->oDefProc == ::DefDlgProc)
    {
	m_pWndProc ->oDefProc  = ::DefWindowProc;
    }

      GFrameWindow::On_Destroy ();
};

Bool GFrameControl::On_Create (void * pParam)
{
    if (! GFrameWindow::On_Create (pParam))
    {
	return False;
    }
	m_pWndProc ->oDefProc = ::DefDlgProc;

	return True ;
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
DEFINE_SYS_WINDOW_CLASS (GButton);
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
DEFINE_SYS_WINDOW_CLASS (GTreeView);

Bool GTreeView::On_Create (void * pParam)
{
    if (! GControl::On_Create (pParam))
    {
	return False;
    }
	return True ;
};

void GTreeView::On_Destroy ()
{
    printf ("Destroy TreeView\n");

    GControl::On_Destroy ();
};

HTREEITEM GTreeView::InsertItem (HTREEITEM  hParent	 ,
				 HTREEITEM  hInsertAfter ,
				 LPCTSTR    pItemName	 ,
			   const void *	    pItemContext )
{
    TV_INSERTSTRUCT	  i			;

    i.hParent		= hParent		;
    i.hInsertAfter	= hInsertAfter		;

    i.item.mask		= 0		        ;

    i.item.mask		= TVIF_PARAM
			| TVIF_TEXT 
    			| TVIF_IMAGE
			| TVIF_SELECTEDIMAGE
			| TVIF_CHILDREN		;

    i.item.lParam	= (LPARAM) pItemContext ;   // TVIF_PARAM
						    //
    i.item.pszText	=	  (pItemName)
			? (LPTSTR) pItemName
			: LPSTR_TEXTCALLBACK	;   // TVIF_TEXT
    i.item.cchTextMax	= 0			;   //

    i.item.iImage	=			    // TVIF_IMAGE
    i.item.iSelectedImage			    //
			= I_IMAGECALLBACK	;

    i.item.cChildren	= I_CHILDRENCALLBACK	;   // TVIF_CHILDREN
						    //

    return (HTREEITEM) Send_WM (TVM_INSERTITEM, (WPARAM) 0,(LPARAM) & i);
};

Bool GTreeView::TVN_GetDispInfo (GWinMsg & m, NMTVDISPINFO * di)
{
    if (TVIF_TEXT  & di ->item.mask)
    {}
    if (TVIF_IMAGE & di ->item.mask)
    {}
    if (TVIF_SELECTEDIMAGE & di ->item.mask)
    {}
    if (TVIF_CHILDREN & di ->item.mask)
    {}
    return m.DispatchComplete ();
};

Bool GTreeView::TVN_DeleteItem (GWinMsg & m, NMTREEVIEW * tv)
{
    printf ("Delteed TreeView item ...\n");

    return m.DispatchComplete ();
};

Bool GTreeView::TVN_SelChanging (GWinMsg & m, NMTREEVIEW * tv)
{
    return m.DispatchComplete ();
};

Bool GTreeView::TVN_SelChanged (GWinMsg & m, NMTREEVIEW * tv)
{
	m_SelItem.mask  = 0;
      *	m_SelItemText	= 0;

    if (tv ->itemNew.state & TVIS_SELECTED)
    {
	m_SelItem.mask    = TVIF_HANDLE
			 |  TVIF_PARAM
		         |  TVIF_TEXT 
    		         |  TVIF_IMAGE
		         |  TVIF_SELECTEDIMAGE
		         |  TVIF_CHILDREN	;

	m_SelItem.mask	 |= TVIF_STATE		;

	m_SelItem.hItem	  = tv ->itemNew.hItem	;

	m_SelItem.cchTextMax 
			  = sizeof (m_SelItemText) / sizeof (TCHAR);

	m_SelItem.pszText = m_SelItemText	;

	Send_WM (TVM_GETITEM, (WPARAM) 0, (LPARAM) & m_SelItem);

    {
//???	GM_LookAtChild_Param p = {this, 0x00000001};

//???	GetParent () ->Send_GM (GM_LookAtChild, (LPARAM) & p);
    }
	printf ("Selected {%s}\n", m_SelItem.pszText);
    }

    return m.DispatchComplete ();
};

Bool GTreeView::On_WinMsg (GWinMsg & m)
{
/*
    switch (m.uType)
    {
    case GWinMsg::CtrlNotify :
//  case GWinMsg::RefletNotify :

	DISPATCH_WM_NOTIFY_CODE	(TVN_GETDISPINFO    , TVN_GetDispInfo	, NMTVDISPINFO	)
//	DISPATCH_WM_NOTIFY_CODE	(TVN_SELCHANGING    , TVN_SelChanging	, NMTREEVIEW	)
	DISPATCH_WM_NOTIFY_CODE	(TVN_SELCHANGED	    , TVN_SelChanged	, NMTREEVIEW	)
	DISPATCH_WM_NOTIFY_CODE	(TVN_DELETEITEM	    , TVN_DeleteItem	, NMTREEVIEW	)

	return GControl::DispatchWinMsg (m);
    }
*/
	return GControl::On_WinMsg (m);
};
















/*
HTREEITEM GTreeView::InsertItem (HTREEITEM  hParent	 ,
				 HTREEITEM  hInsertAfter ,
				 int	    sItemNameMax ,
			   const void *	    pItemContext )
{
    TV_INSERTSTRUCT	  i			;

    i.hParent		= hParent		;
    i.hInsertAfter	= hInsertAfter		;

    i.item.mask		= TVIF_PARAM
			| TVIF_TEXT 
    			| TVIF_IMAGE
			| TVIF_SELECTEDIMAGE
			| TVIF_CHILDREN		;

    i.item.pszText	= LPSTR_TEXTCALLBACK	;
    i.item.cchTextMax	= sItemNameMax		;
    i.item.iImage	= 
    i.item.iSelectedImage 
			= I_IMAGECALLBACK	;
    i.item.cChildren	= I_CHILDRENCALLBACK	;

    i.item.lParam	= (LPARAM) pItemContext ;

    return (HTREEITEM) SendWinMsg (TVM_INSERTITEM , (WPARAM) 0  ,
						    (LPARAM) & i);
};
*/
//
//[]------------------------------------------------------------------------[]
