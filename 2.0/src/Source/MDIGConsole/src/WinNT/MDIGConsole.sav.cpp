










#if FALSE

//[]------------------------------------------------------------------------[]
// Win message dispatch filters
//
//
#define	DISPATCH_GM(Code,Method)		    \
    if	(Code == LOWORD(m.wParam))		    \
    {						    \
	return Method (m);			    \
    }

#define	DISPATCH_GM_(Code,Method,Type)		    \
    if	(Code == LOWORD(m.wParam))		    \
    {						    \
	return Method (m, (Type *) m.lParam);	    \
    }

#define	DISPATCH_GM_CODE(Code,Method,Type)	    \
    if  (Code == LOWORD(m.wParam))		    \
    {						    \
	return Method (m, (Type *) m.lParam);	    \
    }

#define DISPATCH_WM(msg,Method)			    \
    if  (msg == m.uMsg)				    \
    {						    \
	return Method (m);			    \
    }

/*
#define DISPATCH_WM(msg,Method)
    if	(msg == m.uMsg)
    {
	if (Method (m))
	{
	    return True;
	}
    }
*/



#define	DISPATCH_WM_WPARAM(msg,mwp,Method)	    \
    if ((msg == m.uMsg  )			    \
    &&  (mwp == m.wParam))			    \
    {						    \
	return Method (m);			    \
    }

#define DISPATCH_WM_WPARAM_LO(msg,mwp,Method)	    \
    if ((msg == m.uMsg		)		    \
    &&	(mwp == LOWORD (m.wParam)))		    \
    {						    \
	return Method (m);			    \
    }

#define	DISPATCH_WM_WPARAM_HI(msg,mwp,Method)	    \
    if ((msg == m.uMsg		)		    \
    &&	(mwp == HIWORD (m.wParam)))		    \
    {						    \
	return Method (m);			    \
    }

#define	DISPATCH_WM_LPARAM(msg,mlp,Method)	    \
    if ((msg == m.uMsg  )			    \
    &&  (mlp == m.lParam))			    \
    {						    \
	return Method (m);			    \
    }

#define	IS_WM_NOTIFY()				    \
      ((WM_NOTIFY  == m.uMsg) ? True : False)

#define IS_WM_NOTIFY_CODE(Code)			    \
      (Code  == ((NMHDR *) m.lParam) ->code)


#define IS_WM_COMMAND()				    \
      ((WM_COMMAND == m.uMsg) ? True : False)

#define	IS_WM_COMMAND_ID(Id)			    \
      ((Id   == LOWORD(m.wParam)) ? True : False)

#define IS_WM_COMMAND_CODE(Code)		    \
      ((Code == HIWORD(m.wParam)) ? True : False)

#define IS_WM_COMMAND_ID_CODE(Id,Code)		    \
     (((Id   == LOWORD(m.wParam))		    \
    && (Code == HIWORD(m.wParam))) ? True : False)

#define	DISPATCH_WM_COMMAND_ID(Id,Method)	    \
    if ((Id  == LOWORD(m.wParam)))		    \
    {						    \
	return Method (m);			    \
    }

#define	DISPATCH_WM_COMMAND_ID_CODE(Id,Code,Method) \
    if ((Id   == LOWORD(m.wParam))		    \
    &&	(Code == HIWORD(m.wParam)))		    \
    {						    \
	return Method (m);			    \
    }

#define	DISPATCH_WM_COMMAND_CODE(Code,Method)	    \
    if ((Code == HIWORD(m.wParam)))		    \
    {						    \
	return Method (m);			    \
    }

#define	DISPATCH_WM_NOTIFY_ID(Id,Method,Type)	    \
    if ((Id    == ((NMHDR *) m.lParam) ->idFrom))   \
    {						    \
	return Method (m, (Type *) m.lParam);	    \
    }

#define	DISPATCH_WM_NOTIFY_CODE(Code,Method,Type)   \
    if (Code  == ((NMHDR *) m.lParam) ->code)	    \
    {						    \
	return Method (m, (Type *) m.lParam);	    \
    }

#define	DISPATCH_WM_NOTIFY_ID_CODE(Id,Code,Method,Type) \
    if ((Id   == ((NMHDR *) m.lParam) ->idFrom)		\
    &&	(Code == ((NMHDR *) m.lParam) ->code  ))	\
    {							\
	return Method (m, (Type *) m.lParam);		\
    }
//
//[]------------------------------------------------------------------------[]






//[]------------------------------------------------------------------------[]
//
class CParamControl : public GButton
{
public :

	    CParamControl (const void * pWndId = NULL) :

	    GButton (pWndId)
	    {
		m_nBaseX   = 90;
		m_nSizeX   = 60;

		m_pTitleEx = NULL;

		m_dValue   =    0;
	    };

	Bool		On_WinMsg	    (GWinMsg &);

	Bool		On_Create	    (void *);

	Bool		On_ReflectWinMsg    (GWinMsg &);

	void		Init		    (int nBaseX, int nSizeX, const TCHAR * pTitleEx)
			{
			    m_nBaseX	= nBaseX   ;
			    m_nSizeX	= nSizeX   ;
			    m_pTitleEx	= pTitleEx ;
			};

	void		SetValue	    (int);

	void		SetValue	    (const TCHAR *);

protected :

	void		On_Draw		    (LPDRAWITEMSTRUCT);

protected :

	int		    m_nBaseX	  ;
	int		    m_nSizeX	  ;

	const TCHAR *	    m_pTitleEx	  ;

	union
	{
	_U32		    m_dValue	  ;
	TCHAR		    m_pValue [128];
	};
};
//
//[]------------------------------------------------------------------------[]

void CParamControl::SetValue (int n)
{
    wsprintf (m_pValue, "%d", n);

    InvalidateRect (NULL, True);
};

void CParamControl::SetValue (const TCHAR * pString)
{
    StrCpyN (m_pValue, pString, sizeof (m_pValue) / sizeof (TCHAR));

    InvalidateRect (NULL, True);
};

HFONT GetBoaldGUIFont ()
{
//  GetObject (...)
//
    static HFONT     s_hFont = NULL;

    if	  (s_hFont)
    {
    return s_hFont;
    }

    TEXTMETRIC	tm;
    LOGFONT	lf;

    HDC		hDC	= ::GetDC (NULL);
    HFONT	hDCFont = (HFONT) ::SelectObject (hDC, ::GetStockObject (DEFAULT_GUI_FONT));

	      memset (& lf, 0, sizeof (lf));

    ::GetTextFace    (hDC, sizeof (lf.lfFaceName) / sizeof (TCHAR), lf.lfFaceName);
    ::GetTextMetrics (hDC, & tm);

		      lf.lfHeight  = tm.tmHeight  ;
		      lf.lfWeight  = FW_BOLD	  ;
		      lf.lfCharSet = tm.tmCharSet ;

    ::SelectObject (hDC , hDCFont);
    ::ReleaseDC	   (NULL, hDC);

    return s_hFont = ::CreateFontIndirect (& lf);
};

HFONT CreateBoaldGUIFont ()
{
//  GetObject (...)
//
    TEXTMETRIC	tm;
    LOGFONT	lf;

    HDC		hDC	= ::GetDC (NULL);
    HFONT	hDCFont = (HFONT) ::SelectObject (hDC, ::GetStockObject (DEFAULT_GUI_FONT));

	      memset (& lf, 0, sizeof (lf));

    ::GetTextFace    (hDC, sizeof (lf.lfFaceName) / sizeof (TCHAR), lf.lfFaceName);
    ::GetTextMetrics (hDC, & tm);

		      lf.lfHeight  = tm.tmHeight  ;
		      lf.lfWeight  = FW_BOLD	  ;
		      lf.lfCharSet = tm.tmCharSet ;

    ::SelectObject (hDC , hDCFont);
    ::ReleaseDC	   (NULL, hDC);

    return ::CreateFontIndirect (& lf);
};

void CParamControl::On_Draw (LPDRAWITEMSTRUCT pdi)
{
    RECT    rc;
    RECT    rv;
    RECT    rd;
    TCHAR   pBuf [128];

	rc = pdi ->rcItem;
	rv = rc;
	rv.left   = rv.right - m_nBaseX;
	rv.right  = rv.left  + m_nSizeX;

    if (ODA_DRAWENTIRE & pdi ->itemAction)
    {
	rd = rc;
	rd.right  = rv.left  - 2;
	GetText (pBuf, sizeof (pBuf) / sizeof (TCHAR));
	::DrawText (pdi ->hDC, pBuf, -1, & rd, DT_SINGLELINE | DT_RIGHT | DT_VCENTER);

    if (m_pTitleEx)
    {
	rd = rc;
	rd.left   = rv.right + 2;

	::DrawText (pdi ->hDC, m_pTitleEx, -1, & rd, DT_SINGLELINE | DT_LEFT | DT_VCENTER);
    }

	HFONT hFont = (HFONT) ::SelectObject (pdi ->hDC, CreateBoaldGUIFont ());

	rd = rv;
	rd.left   += 2;
	rd.top	  += 2;
	rd.right  -= 4;
	rd.bottom -= 2;

	::DrawText (pdi ->hDC, m_pValue, -1, & rd, DT_SINGLELINE | DT_RIGHT | DT_VCENTER);

	rd.right  += 2;
			      ::DeleteObject (::SelectObject (pdi ->hDC, hFont));

	::FrameRect (pdi ->hDC, & rd, ::GetSysColorBrush (COLOR_BTNFACE));

	if (ODS_FOCUS & pdi ->itemState)
	{
	    ::DrawFocusRect (pdi ->hDC, & rd);
	}
	return;
    }
    if (ODA_FOCUS & pdi ->itemAction)
    {
	rd = rv;
	rd.left   += 2;
	rd.top	  += 2;
	rd.right  -= 2;
	rd.bottom -= 2;

	    ::DrawFocusRect (pdi ->hDC, & rd);
    }
};


Bool CParamControl::On_WinMsg (GWinMsg & m)
{
    if (WM_SETFOCUS == m.uMsg)
    {
	::SendMessage (GetParent () ->GetHWND (), DM_SETDEFID, (WPARAM) ::GetWindowLong (GetHWND (), GWL_ID), (LPARAM) 1);	    
    }
    else
    if (WM_KILLFOCUS == m.uMsg)
    {
	::SendMessage (GetParent () ->GetHWND (), DM_SETDEFID, (WPARAM) 8, (LPARAM) 1);
    }

    if (WM_GETDLGCODE == m.uMsg)
    {
	m.DispatchComplete (DLGC_BUTTON);
    }
    return m.DispatchDefault ();
};

Bool CParamControl::On_Create (void * pParam)
{
    if (! GButton::On_Create (pParam))
    {	return False;}

//  SubclassWndProc (GWindow::DispatchWinMsg, this);

	return True;
};



//[]------------------------------------------------------------------------[]
//
void RemoveDefault (HWND hDlg)
{
    LRESULT wParam = ::CallWindowProc (::DefDlgProc, hDlg, DM_GETDEFID, 0, 0L);
    HWND    lParam;

    if (DC_HASDEFID == HIWORD (wParam))
    {
	lParam = ::GetDlgItem (hDlg, (int)(short) LOWORD (wParam));
	// remove BS_DEFPUSHBUTTON
/*
    if (BS_OWNERDRAW != (::GetWindowLong (lParam, GWL_STYLE) & BS_TYPEMASK))
    {
//	::SendMessage   (lParam, BM_SETSTYLE, BS_DEFPUSHBUTTON, (LPARAM) 1);
    }
*/
    }
};

Bool CParamControl::On_ReflectWinMsg (GWinMsg & m)
{
    LPDRAWITEMSTRUCT  pdi = 
   (LPDRAWITEMSTRUCT) m.lParam	  ;
    RECT	      rc	  ;
    RECT	      rt	  ;
    RECT	      rv	  ;
    TCHAR	      pTitle [128];

    if (WM_COMMAND  == m.uMsg)
    {
	switch (HIWORD (m.wParam))
	{
	case BN_SETFOCUS :
	    ::SendMessage (GetParent () ->GetHWND (), DM_SETDEFID, (WPARAM) LOWORD (m.wParam), (LPARAM) 0);
	    break;

	case BN_KILLFOCUS :
	    ::SendMessage (GetParent () ->GetHWND (), DM_SETDEFID, (WPARAM) 0, (LPARAM) 0);
	    break;


#if FALSE
	case BN_SETFOCUS :

	    RemoveDefault (GetParent () ->GetHWND ());

	    ::SendMessage (GetParent () ->GetHWND (), DM_SETDEFID, (WPARAM) LOWORD (m.wParam), (LPARAM) 1);

printf ("\n\n\nSet focus %08X (%08X-%08X)\n\n\n", m.wParam, GetStyles (), ::SendMessage (GetHWND (), WM_GETDLGCODE, 0, 0));

	    break;

	case BN_KILLFOCUS :
/*
	    ::SendMessage   (GetParent () ->GetCtrlHWND (IDCLOSE), BM_SETSTYLE, 
	   (::GetWindowLong (GetParent () ->GetCtrlHWND (IDCLOSE), GWL_STYLE) & 0x0000FFFF) | BS_DEFPUSHBUTTON, (LPARAM) 1);

	    ::SendMessage (GetParent () ->GetHWND (), DM_SETDEFID, (WPARAM) IDCLOSE, (LPARAM) 1);
*/
printf ("\n\n\nKill focus %08X\n\n\n", m.wParam);

	    break;
#endif
	
#if FALSE
	    case BN_SETFOCUS :
	    {
		DWORD dwRet = (DWORD)::SendMessage(GetParent () ->GetHWND (), DM_GETDEFID, 0, 0L);

		if(HIWORD(dwRet) == DC_HASDEFID)
		{
		    HWND hOldDef = ::GetDlgItem(GetParent () ->GetHWND (), (int)(short)LOWORD(dwRet));
		    // remove BS_DEFPUSHBUTTON
		    ::SendMessage (hOldDef, BM_SETSTYLE, ::GetWindowLong (hOldDef, GWL_STYLE) & (~BS_DEFPUSHBUTTON), (LPARAM) 1);

//		    ::InvalidateRect (GetHWND (), NULL, True);


printf ("Default ID %d\n", LOWORD (dwRet));

::MessageBeep (-1);

		}
//		::SendMessage (GetHWND (), BM_SETSTYLE, GetStyles () | BS_DEFPUSHBUTTON, (LPARAM) 1);

//	    ::SendMessage (GetParent () ->GetHWND (), DM_SETDEFID, 0/*(WPARAM) LOWORD (m.wParam)*/, (LPARAM) 1);

//		    ::SendMessage(GetParent () ->GetHWND (), DM_SETDEFID, nID, 0L);
	    }
//	    ::SendMessage (GetParent () ->GetHWND (), DM_SETDEFID, (WPARAM) LOWORD (m.wParam), (LPARAM) 0);

	    break;
#endif

	};
	    return m.DispatchDefault  ();
    }    
    else
    if (WM_DRAWITEM == m.uMsg)
    {
	On_Draw (pdi);

	return m.DispatchComplete ();

	rc  = pdi ->rcItem;

	GetText (pTitle, sizeof (pTitle) / sizeof (TCHAR));

	rt	  = rc ;
	rt.right -= 100;

	rv.left   = rt.right  + 2;
	rv.top    = rc.top    + 2;
	rv.right  = rc.right  - 4;
	rv.bottom = rc.bottom - 2;

#if TRUE
	if (ODA_DRAWENTIRE & pdi ->itemAction)
	{
//	    ::DrawFrameControl	(pdi ->hDC, & pdi ->rcItem, DFC_BUTTON, DFCS_BUTTONPUSH | DFCS_PUSHED);

	    ::DrawText (pdi ->hDC, pTitle, -1, & rt, DT_SINGLELINE | DT_RIGHT | DT_VCENTER);

	    HFONT hFont = (HFONT) ::SelectObject (pdi ->hDC, CreateBoaldGUIFont ());

::wsprintf (pTitle, "%d ��.", GetCtrlId (m_hWnd));

	    ::DrawText (pdi ->hDC, pTitle, -1, & rv, DT_SINGLELINE | DT_RIGHT | DT_VCENTER);

	    ::DeleteObject (::SelectObject (pdi ->hDC, hFont));

	    rv.right = rc.right - 2;

	    ::FrameRect	    (pdi ->hDC, & rv, ::GetSysColorBrush (COLOR_BTNFACE));

	if (ODS_FOCUS & pdi ->itemState)
	{
	    ::DrawFocusRect (pdi ->hDC, & rv);
	}
	    return m.DispatchComplete ();
	}
	if (ODA_FOCUS & pdi ->itemAction)
	{
	    rv.right = rc.right - 2;

            ::DrawFocusRect (pdi ->hDC, & rv);

	    return m.DispatchComplete ();
	}
	    return m.DispatchComplete ();

#else
	if (ODA_DRAWENTIRE & pdi ->itemAction)
	{
	    ::DrawFrameControl	(pdi ->hDC, & pdi ->rcItem, DFC_BUTTON, DFCS_BUTTONPUSH | DFCS_PUSHED);

	    ::DrawCaption	(pdi ->hwndItem, 
				 pdi ->hDC, & pdi ->rcItem, DC_TEXT | DC_SMALLCAP | 
			       ((ODS_FOCUS  & pdi ->itemState) ? DC_ACTIVE : 0));

	    if (ODS_FOCUS & pdi ->itemState)
	    {
	    ::DrawFocusRect (pdi ->hDC, & rc);
	    }
	    return m.DispatchComplete ();
	}
	if (ODA_FOCUS & pdi ->itemAction)
	{
	    ::DrawCaption	(pdi ->hwndItem, 
				 pdi ->hDC, & pdi ->rcItem, DC_TEXT | DC_SMALLCAP | 
			       ((ODS_FOCUS  & pdi ->itemState) ? DC_ACTIVE : 0));

	if (ODS_FOCUS & pdi ->itemState)
	{
	    ::DrawFocusRect	(pdi ->hDC, & rc);
	}
	    return m.DispatchComplete ();
	}
#endif
    }
	    return False;
};
//
//[]------------------------------------------------------------------------[]













//[]------------------------------------------------------------------------[]
//
class CDialogSettings : public GDialog
{
public :

	    CDialogSettings () : GDialog (NULL),

		m_Content (NULL)
	    {		
	    };

	   ~CDialogSettings () 
	    {};

protected :


	Bool		On_WM_ControlNotify	(HWND hCtrl, int nCtrlId, GWinMsg &);

//	Bool		Get_WM_COMMAND_ID_CODE	(int CtrlId, 

	Bool		On_Create		(void *);

	void		On_Destroy		();

	Bool		On_WM_Command		(GWinMsg &);

	Bool		On_DlgMsg		(GWinMsg &);

	Bool		On_Control_UAMax	(GWinMsg &);

	Bool		On_EnterEditControl	(GWinMsg &);

	Bool		On_Control_IAMax	(GWinMsg &);

	Bool		On_Control_UA		(GWinMsg &);

	Bool		On_Param_UAnode		(GWinMsg &);
	Bool		On_Param_IAnode		(GWinMsg &);

protected :


	GTreeView		m_Content	;

	CParamControl		m_Param_UAnode	;
	CParamControl		m_Param_IAnode	;
};
//
//[]------------------------------------------------------------------------[]


//[]------------------------------------------------------------------------[]
//
Bool CDialogSettings::On_Create (void * pParam)
{
    if (! GDialog::On_Create (pParam))
    {	return False;}

    if (! m_Content.Create (this, GetCtrlHWND (IDC_CONTENT), pParam))
    {	return False;}

    ::SetMenu (GetHWND (), ::LoadMenu (::GetModuleHandle (NULL), MAKEINTRESOURCE (IDR_MAIN_MENU)));


    HTREEITEM	ti = NULL;
		ti =
    m_Content.InsertItem (NULL	, TVI_LAST,	_T("Roent service setttings")	, NULL);
    m_Content.InsertItem (ti	, TVI_LAST,	_T("Access control")		, NULL);
    m_Content.InsertItem (ti	, TVI_LAST,	_T("Ftdi settings")		, NULL);
    m_Content.Expand	 (ti);

		ti =
    m_Content.InsertItem (NULL	, TVI_LAST,	_T("Urp settings")		, NULL);
    m_Content.InsertItem (ti	, TVI_LAST,	_T("Global")			, NULL);
    m_Content.InsertItem (ti	, TVI_LAST,	_T("Some settings (1)")		, NULL);
    m_Content.InsertItem (ti	, TVI_LAST,	_T("Some settings (2)")		, NULL);
    m_Content.InsertItem (ti	, TVI_LAST,	_T("Some settings (3)")		, NULL);
    m_Content.Expand	 (ti);

		ti =
    m_Content.InsertItem (NULL	, TVI_LAST,	_T("KKM settings")		, NULL);
    m_Content.Expand	 (ti);

    m_Content.SelectItem (
    m_Content.GetItem	 (NULL, TVGN_ROOT));

    if (! m_Param_UAnode.Create (this, GetCtrlHWND (IDC_PARAM_UANODE)))
    {	return False;}
//  m_Param_UAnode.SetStyles (0, BS_PUSHLIKE);

/*
    ::SetWindowLong (GetCtrlHWND (IDCLOSE), GWL_STYLE,
    ::GetWindowLong (GetCtrlHWND (IDCLOSE), GWL_STYLE) & (~BS_DEFPUSHBUTTON));
*/


    if (! m_Param_IAnode.Create (this, GetCtrlHWND (IDC_PARAM_IANODE)))
    {	return False;}

/*
	{{
	GDialog * pDialog = new GDialog (NULL);
	pDialog ->Create (this, NULL, MAKEINTRESOURCE (IDD_CONTROL_URP_MINI), NULL);
	pDialog ->SetStyles (0, WS_TABSTOP);
	pDialog ->SetWindowPos (NULL, 220, 300, 0, 0, SWP_NOSIZE);

	}}
*/


	return True ;
};

void CDialogSettings::On_Destroy ()
{
    m_Content.DeleteItem (NULL);

    GDialog::On_Destroy ();
};

Bool CDialogSettings::On_WM_Command (GWinMsg & m)
{
    return False;
};

/*???
Bool CDialogSettings::On_LookAtChild (GWinMsg & m, GM_LookAtChild_Param * pParam)
{
    if (pParam ->pChild == & m_Content)
    {
	SetCtrlText (IDC_CONTENT_TEXT, m_Content.GetSelItemText ());
    }

    return True;
};
???*/









    typedef struct
    {
	Byte	    nPrecision : 8;
	Byte	    nWidth     : 8;
	Byte	    bFlags     : 8;
	char	    cType      : 8;

    }		    FormatCode	  ;

#define	GET_WM_COMMAND_CODE(m)	(HIWORD(m.wParam))

class EditControl
{
public :

	void		    Init		(HWND, const char * fmt, int   MinValue,
									 int   MaxValue);

	void		    Init		(HWND, const char * fmt, float MinValue,
									 float MaxValue);
	void		    Done		();

virtual	Bool		    PreprocMessage	(MSG *);

virtual	Bool		    On_WM_Dispatch	(GWinMsg &);

virtual	Bool		    DoValidate		();

	Bool		    On_EnterEditControl	(GWinMsg & m);

protected :

	HWND			m_hWnd	      ;

	Bool			m_bChanged    ;
	const char *		m_fmt	      ;

	union
	{
	    int			m_lMinValue   ;
	    float		m_fMinValue   ;
	};
	union
	{
	    int			m_lMaxValue   ;
	    float		m_fMaxValue   ;
	};

//	int			m_nTextLen    ;
	TCHAR			m_pText [256] ;
};



/*

    -dd.ddd	    //-99.999	 99.999
    +dd.ddd	    //-99.999	+99.999
     dd.ddd	    // 00.000	 99.999

    -xx.xxx
    +xx.xxx
     xx.xxx

    "%+

    "f2.3"
    "u3"
*/

__int64	GetFloatValue  (int uFloorValue, int uFractValue)
{
    __int64 v = uFloorValue;

/*
    for (t = uFractValue; 0 < t; t t <  (0 <  uFractValue)
    {
	v *= 10;
    };
*/
    return  v;
};

int Parse (int & Value, const char *& str);

int Parse (int & Value, const char *& str)
{
    Bool bRes;

    for (bRes = 0, Value = 0; * str; str ++)
    {
	if (('0' <= * str) && (* str <= '9'))
	{
	if ((INT_MAX / 10) >= Value)
	{
	    Value = (Value * 10) + (* str - '0');

	    bRes  =  1;

	    continue;
	}
	    bRes  = -1;

	    continue;
	}
	    break;
    }

	return bRes;
}


FormatCode ParseFormat (const char * fmt)
{
//%[flags] [width] [.precision] [{h | l | I | I32 | I64}]type
//
    FormatCode	 Code	 ;
	  int	 n	 ;
	  int	 phase	 ;
    const char * p  = fmt;

    Code.nPrecision = 0  ;
    Code.nWidth	    = 0	 ;
    Code.bFlags	    = 0	 ;
    Code.cType	    = 0	 ;

    if ('%' == * p ++)
    {
	for (phase = 0; * p;)
	{
	if (phase == 0)
	{
	    phase =  1;
	}
	if (phase == 1)
	{
	    phase =  2;

	    if (0 < Parse (n, p))
	    {
		Code.nWidth = (Byte) n;
	    }
		continue;
	}
	if (phase == 2)
	{
	    phase =  3;

	    if ('.' == * p)
	    {
		p ++;

		continue;
	    }
	    phase =  4;
	}
	if (phase == 3)
	{
	    phase =  4;

	    if (0 < Parse (n, p))
	    {
		Code.nPrecision = (Byte) n;
	    }
		continue;
	}
	if (phase == 4)
	{
	    phase =  5;
	}
	if (phase == 5)
	{
	    if (('i' == * p))
	    {
		Code.cType = 'd';
	    }
	    else
	    if (('f' == * p)
	    ||  ('d' == * p)
	    ||  ('X' == * p)
	    ||  ('x' == * p))
	    {
		Code.cType = * p;
	    }
	}
	    break;
	}
    }
	return  Code;
};

TCHAR * ParseValue (const char * fmt, LPCTSTR ps)
{
    struct  FloatPart
    {
	int nLen	;
	int nLenMax	;
	int nValue	;
    };

    FloatPart	 Floor	  = {0, 0, 0};
    FloatPart	 Fract	  = {0, 0, 0};

    FloatPart  * pCurrent = & Floor;

    FormatCode	 f = ParseFormat (fmt);

    Floor.nLenMax = (f.nWidth - f.nPrecision);
    Fract.nLenMax = (		f.nPrecision);

    for (pCurrent = & Floor; * ps; ps ++)
    {
	if (('0' <= * ps) && (* ps <= '9'))
	{
	    if (((INT_MAX / 10)	    >= pCurrent ->nValue)
	    &&  (pCurrent ->nLenMax >  pCurrent ->nLen	))
	    {
		pCurrent ->nValue = (pCurrent ->nValue * 10) + (* ps - '0');
		pCurrent ->nLen ++;

		continue;
	    }
	}
	else
	if ((f.cType  == 'f'	   )
	&&  (pCurrent == &  Floor  )
	&&  ('.'      == *  ps	   ))  // Get this char from LANGUIGE_ID
	{
	    pCurrent = & Fract;
	    continue;
	}

	return (TCHAR *) ps;
   }
	return NULL ;
};



Bool EditControl::PreprocMessage (MSG * msg)
{
    return False;
};









void EditControl::Init (HWND hWnd, const char * fmt, int MinValue,
						     int MaxValue)
{
    m_hWnd	= hWnd;
    m_fmt	= fmt ;

    m_lMinValue = MinValue;
    m_lMaxValue = MaxValue;
};

void EditControl::Init (HWND hWnd, const char * fmt, float MinValue,
						     float MaxValue)
{
    m_hWnd	= hWnd;
    m_fmt	= fmt ;

    m_fMinValue = MinValue;
    m_fMaxValue = MaxValue;
};

void EditControl::Done ()
{
    FormatCode  f = ParseFormat (m_fmt);

    union
    {
	int	lValue;
	float	fValue;
    };

    ::GetWindowText (m_hWnd, m_pText, sizeof (m_pText) / sizeof (TCHAR));

    sscanf (m_pText, m_fmt, & fValue);

    if (f.cType == 'f')
    {
	if (fValue < m_fMinValue)
	    fValue = m_fMinValue;
	if (fValue > m_fMaxValue)
	    fValue = m_fMaxValue;
    }
    else
    {
	if (lValue < m_lMinValue)
	    lValue = m_lMinValue;
	if (lValue > m_lMaxValue)
	    lValue = m_lMaxValue;
    }
    sprintf (m_pText, m_fmt,  lValue);

    ::SetWindowText (m_hWnd, m_pText);
};

Bool EditControl::DoValidate ()
{
    int	    nFloorLen   ,
	    nFloorValue ,
	    nFractLen   ,
	    nFractValue ;

    TCHAR * pBadChar	;
    Bool    bOk	 = True	;

    if (bOk)
    {
#if TRUE
    while (NULL != (pBadChar = ParseValue (m_fmt, m_pText)))
#else
    while (NULL != (pBadChar = ScanFloatValue (m_pFormat, & nFloorValue, & nFloorLen,
							  & nFractValue, & nFractLen, m_pText)))
#endif
    {
	    bOk  = False;

	::SendMessage	(m_hWnd, EM_SETSEL    , (WPARAM)(int)(pBadChar - m_pText    ),
						(LPARAM)(int)(pBadChar - m_pText + 1));

	::SendMessage	(m_hWnd, EM_REPLACESEL, (WPARAM) FALSE, (LPARAM) "");

	::GetWindowText (m_hWnd, m_pText, sizeof (m_pText) / sizeof (TCHAR));
    };
    }

    if (bOk)
    {{
//	__int64 v = 
    }}




//	printf ("\t\t\t\tUAMax Value :Value %d.%d (%d.%d)\n", nFloorValue, nFractValue,
//							      nFloorLen  , nFractLen  );
    return  bOk;
};

/* Remove !!!
#ifndef EM_SHOWBALLOONTIP

#define	EM_SHOWBALLOONTIP (ECM_FIRST + 3)

#pragma pack (4)

typedef struct tagEDITBALLOONTIP {
    DWORD cbStruct;
    LPCWSTR pszTitle;
    LPCWSTR pszText;
    INT ttiIcon;
} EDITBALLOONTIP, *PEDITBALLOONTIP;

#pragma pack ()

#endif
*/


Bool EditControl::On_WM_Dispatch (GWinMsg & m)
{
    EDITBALLOONTIP  ebt;

    switch (GET_WM_COMMAND_CODE  (m))
    {
	case EN_UPDATE :

	    ::GetWindowText (m_hWnd, m_pText, sizeof (m_pText) / sizeof (TCHAR));

	if (DoValidate ())
	{
	}
	else
	{
//	    ::SendMessage   (m_hWnd, EM_REPLACESEL    , (WPARAM) FALSE, (LPARAM) m_pText);

//	    ::SetWindowText (m_hWnd, m_pText);

	    ebt.cbStruct = sizeof (ebt)	;
	    ebt.pszTitle = L"������������ ������ ��� ��������";
	    ebt.pszText	 = L"����� ����� ������ ���� ������� dd.ddd";
	    ebt.ttiIcon  = TTI_ERROR	;

	    ::SendMessage   (m_hWnd, EM_SHOWBALLOONTIP, (WPARAM) 0, (LPARAM) & ebt);
	}
	default :;
    };
    return True ;
};


	EditControl *		p_Edit = NULL ;	
	EditControl		s_Edit	      ;


Bool CDialogSettings::On_Control_UAMax (GWinMsg & m)
{
    printf ("\t\tUAMax Edit : (%d) ->%04d.%04X\n", LOWORD (m.wParam), HIWORD (m.wParam),  HIWORD (m.wParam));

    switch (GET_WM_COMMAND_CODE (m))
    {
	case EN_SETFOCUS  :

	    p_Edit = & s_Edit;

	    p_Edit ->Init ((HWND) m.lParam, "%03d",  0, 140);
//	    p_Edit ->Init ((HWND) m.lParam, "%4.3f", (float) 0.0, (float) 3.720);
	    break;

	case EN_KILLFOCUS :

	    if (p_Edit == & s_Edit)
	    {
		p_Edit = NULL;

		s_Edit.Done ();
	    }
	    break;

	default :

	    if (p_Edit)
	    {
		p_Edit ->On_WM_Dispatch (m);
	    }

    }

    return m.DispatchComplete ();
};

















//[]------------------------------------------------------------------------[]
//
class GEdit : public GControl
{
DECLARE_SYS_WINDOW_CLASS (_T("Edit"));

public :
	    GEdit   (const void * pWndId = NULL);

virtual	DWord		Exec		();

protected :
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
DEFINE_SYS_WINDOW_CLASS (GEdit);

GEdit::GEdit (const void * pWndId) : GControl (pWndId)
{};


DWord GEdit::Exec ()
{
    printf ("\n\n\n====================================\n");
    printf ("...Exec...");
    printf ("\n\n\n====================================\n");
    return 0;
};
//
//[]------------------------------------------------------------------------[]





Bool CDialogSettings::On_Control_IAMax (GWinMsg & m)
{
    switch (GET_WM_COMMAND_CODE (m))
    {
	case EN_UPDATE :

	    if ((HWND) m.lParam == ::GetFocus ())
	    {{
		GEdit	       EditCtrl;

//		ExecControl (& EditCtrl);
	    }}

	    break;
    };
	return m.DispatchComplete ();
};


Bool CDialogSettings::On_WM_ControlNotify (HWND hCtrlId, int nCtrlId, GWinMsg & m)
{
    switch (nCtrlId)
    {
    case IDC_PARAM_UANODE :
	return m_Param_UAnode.On_ReflectWinMsg (m);

    case IDC_PARAM_IANODE :
	return m_Param_IAnode.On_ReflectWinMsg (m);
    };
	return False;
};

Bool CDialogSettings::On_Param_UAnode (GWinMsg & m)
{
//  ::MessageBox (GetHWND (), "���� ��������:", "Report", MB_OK);

    return m.DispatchComplete ();
};

Bool CDialogSettings::On_DlgMsg (GWinMsg & m)
{
	int	nCtrlId;

    if (0 !=   (nCtrlId = IsControlNotify  (m)))
    {
	switch (nCtrlId)
	{
	case IDC_RADIO1 :
	    break;
	case IDC_CHECK1 :
	    break;
	default :
	    break;
	};
    }

    if (0 !=   (nCtrlId = IsCommand (m)))
    {
printf ("\n\n\n\t\tTarget command (CDialogSettings) %08X\n\n\n", m.wParam);

	switch (nCtrlId)
	{
	case IDC_RADIO1 :
	    break;

	case IDC_CHECK1 :
	    break;

	case IDCLOSE	:
	case IDCANCEL	:
#if TRUE
	    TCurrent () ->GetCurApartment () ->BreakFromWorkLoop ();
#else

#endif
	    break;

	default :
	    ;
	}
	return m.DispatchComplete ();
    };

    if (WM_COMMAND == m.uMsg)
    {

//	DISPATCH_WM_COMMAND_ID	(IDC_UA_MAX	, On_Control_UAEditControl  )

//	DISPATCH_WM_COMMAND_ID	(IDC_UA_MAX	, On_Control_UAMax)
	DISPATCH_WM_COMMAND_ID	(IDC_IA_MAX	, On_Control_IAMax)


	DISPATCH_WM_COMMAND_ID_CODE (IDC_PARAM_UANODE,	BN_CLICKED, On_Param_UAnode)
    }
    
	return GDialog::On_DlgMsg (m);
};
//
//[]------------------------------------------------------------------------[]


//[]------------------------------------------------------------------------[]
//
class CMain : public GFrameControl
{
public :

		CMain ();

private :

	Bool	    On_Create		    (void *);

	void	    On_Destroy		    ();


	Bool	    PreprocMessage	    (MSG *);

	Bool	    On_WM_Close		    (GWinMsg &);

	DWORD	    DispatchException	    (PEXCEPTION_RECORD);

	GIrp *	    FileTest		    (GIrp *, void *);

	Bool	    On_WinMsg		    (GWinMsg &);

private :
};

CMain::CMain () : GFrameControl (NULL)
{};
//
//[]------------------------------------------------------------------------[]

Bool CMain::PreprocMessage (MSG * msg)
{
//  printf ("\n\n===============\nCMain::PreprocMessage\n================\n\n");

    return GFrameControl::PreprocMessage (msg);
};

//[]------------------------------------------------------------------------[]
//
Bool CMain::On_Create (void * pParam)
{
    if (! GFrameControl::On_Create (pParam))
    {	return False;}

    ::SetMenu (GetHWND (), ::LoadMenu (::GetModuleHandle (NULL), MAKEINTRESOURCE (IDR_MAIN_MENU)));

	return True ;

//
//[]------------------------------------------------------------------------[]
//
    {{
#if FALSE

	GDialog * pDialog = new GDialog (NULL);
	pDialog ->Create (this, NULL, MAKEINTRESOURCE (IDD_CONTROL_URP_FULL), NULL);
	pDialog ->SetStyles (0, WS_TABSTOP);

	GDialog * pDialogC = new GDialog (NULL);
	pDialogC ->Create (pDialog, NULL, MAKEINTRESOURCE (IDD_CONTROL_TITLE), NULL);
	pDialogC ->SetStyles (0, WS_TABSTOP | WS_GROUP);

		  pDialogC = new GDialog (NULL);
	pDialogC ->Create (pDialog, NULL, MAKEINTRESOURCE (IDD_CONTROL_TITLE), NULL);
	pDialogC ->SetStyles (0, WS_TABSTOP | WS_GROUP);
	pDialogC ->SetWindowPos (NULL, 0, 32, 0, 0, SWP_NOSIZE);

#else
	GDialog * pDialog = new GDialog (NULL);
	pDialog ->Create (this, NULL, MAKEINTRESOURCE (IDD_CONTROL_TITLE), NULL);
	pDialog ->SetStyles (0, WS_TABSTOP);

		  pDialog = new GDialog (NULL);
	pDialog ->Create (this, NULL, MAKEINTRESOURCE (IDD_CONTROL_URP_FULL), NULL);
	pDialog ->SetStyles (0, WS_TABSTOP | WS_GROUP);

	pDialog ->SetWindowPos (NULL, 0, 40, 0, 0, SWP_NOSIZE);
#endif	

    }}
	return True;
};


void CMain::On_Destroy ()
{
    GFrameControl::On_Destroy ();
};

//[]------------------------------------------------------------------------[]
//
    DECLARE_INTERFACE  (IWorkProtocol)
    {
#pragma pack (_M_PACK_LONG)

	struct Request				// All fields in Natural order
	{
	    _U16	uLength	    ;
	    _U16	uType	    ;
	    _U32	uRequestId  ;
	};

	struct WinMsg : public Request		// 0 - Type
	{
	    UINT	uMsg	    ;
	    WPARAM	wParam	    ;
	    LPARAM	lParam	    ;
	};

#pragma pack ()
    
    };
//
//[]------------------------------------------------------------------------[]

void On_CloseRequest (void * hKey, WPARAM, LPARAM);

Bool CMain::On_WM_Close (GWinMsg & m)
{
    On_CloseRequest (this, (WPARAM) IDCLOSE, (LPARAM) 0);

    return True;
};

DWORD CMain::DispatchException (PEXCEPTION_RECORD)
{
    return EXCEPTION_CONTINUE_EXECUTION;
};



/*

Bool CMain::On_DialogSettings (GWinMsg & m)
{
    if (WM_CLOSE
};

void ChoiseSettings (GWindow * pOwner)
{
    TModalWindow <CMain, >    ModlalLoop;

    if (ModlalLoop.Window ().Create (pOwner, NULL, MAKEINTRESOURCE (IDD_DIALOG4)))
    {
	AddPreprocHandler    (& Dlg);

	RemovePreprocHandler (& Dlg);
    }
};






*/


GIrp * CMain::FileTest (GIrp * pIrp, void * pParam)
{
printf ("\n\n\tExec Idle command (CMain) %08X\n\n", (int) pParam);

    CDialogSettings Dlg;

    if (Dlg.Create  (this, NULL, MAKEINTRESOURCE (IDD_DIALOG4)))
    {
	AddPreprocHandler (& Dlg);

	Dlg.ExecWindow ();

	RemPreprocHandler (& Dlg);
    }
	return pIrp;
};

Bool CMain::On_WinMsg (GWinMsg & m)
{
    if (WM_COMMAND     == m.uMsg)
    {
    if (MMID_FILE_TEST == LOWORD (m.wParam))
    {{
	GIrp * pIrp	   =  new GIrp ();

	SetCurCoProc <CMain, void *>
	      (pIrp, & CMain::FileTest, this, (void *) LOWORD (m.wParam));

	QueueIrpForComplete (pIrp);

printf ("\n\n\tQueue Idle command (CMain) %08X\n\n", m.wParam);

    }}
    }
	DISPATCH_WM	(WM_CLOSE			, On_WM_Close)

	return GFrameControl::On_WinMsg (m);
};
//
//[]------------------------------------------------------------------------[]








































//[]------------------------------------------------------------------------[]
//
class GModalDialog
{
public :
		GModalDialog    ()
		{
		    m_hWnd = NULL;
		};

	struct InitParam
	{
	    DWord	dFalgs;
	    int		x ,  y;
	    int		cx, cy;
	    void *	pParam;
	    DWord	dParam;
	};

	INT_PTR		Exec		    (HINSTANCE	 hInstance  ,
					     LPCTSTR	 pResName   ,
					     HWND	 hParent    ,
					     InitParam * pInitParam );

protected :

virtual	void		SetData		    (Bool bSetFlag);

virtual	Bool		On_InitDialog	    (GWinMsg & m);

virtual	Bool		DispatchWinMsg	    (GWinMsg & m);

static	Bool GAPI	DispatchDlgMsg	    (void *, GWinMsg &);

protected :

	HWND		m_hWnd		;
	InitParam *	m_pInitParam	;
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
Bool GModalDialog::On_InitDialog (GWinMsg & m)
{
    RECT       wRect;

    ::GetWindowRect (m_hWnd, & wRect);

    if (m_pInitParam)
    {
	::SetWindowPos (m_hWnd, NULL, m_pInitParam ->x - (wRect.right - wRect.left), m_pInitParam ->y ,
				      m_pInitParam ->cx, m_pInitParam ->cy, SWP_NOZORDER | SWP_NOSIZE);
	SetData (True);
    }

    return m.DispatchCompleteDlgEx (True);
};

void GModalDialog::SetData (Bool bSetFlag)
{};

Bool GModalDialog::DispatchWinMsg (GWinMsg & m)
{
    if (WM_INITDIALOG == m.uMsg)
    {
	m_hWnd	     =  m.hWnd;
	m_pInitParam = (InitParam *) m.lParam;

	return On_InitDialog (m);
    }
    else
    if (WM_COMMAND    == m.uMsg)
    {
	if (IS_WM_COMMAND_ID_CODE (IDOK	    , BN_CLICKED))
	{
	if (m_pInitParam)
	{
	    SetData	(False);
	}
	    ::EndDialog (m_hWnd, IDOK);
	}
	if (IS_WM_COMMAND_ID_CODE (IDCANCEL , BN_CLICKED))
	{
	    ::EndDialog (m_hWnd, IDCANCEL);
	}
    }
	return False;
};

Bool GAPI GModalDialog::DispatchDlgMsg (void * oDsp, GWinMsg & m)
{
    return ((GModalDialog *) oDsp) ->DispatchWinMsg (m);
};

INT_PTR GModalDialog::Exec (HINSTANCE	hInstance  ,
			    LPCTSTR	hResName   ,
			    HWND	hParent	   ,
			    InitParam *	pInitParam )
{
    GDlgProcThunk * pDlgProc = new GDlgProcThunk (DispatchDlgMsg, this, _nullDlgProc);

    INT_PTR	    pResult  = ::DialogBoxParam  (hInstance  ,
						  hResName   ,
						  hParent    ,
					(DLGPROC) pDlgProc   ,
					 (LPARAM) pInitParam );
    delete pDlgProc;

    return pResult ;
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
class GEditValueDialog : public GModalDialog
{
public :
	    GEditValueDialog () :

	    GModalDialog ()
	    {
		m_hEditCtrl	= NULL;
		m_nEditCtrlId	=    0;
	    };
protected :

	void		SetData		    (Bool);

	Bool		On_InitDialog	    (GWinMsg & m);
	    
public :

	HWND		m_hEditCtrl	;
	int		m_nEditCtrlId	;
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
void GEditValueDialog::SetData (Bool bSetFlag)
{
    if (m_pInitParam && m_pInitParam ->pParam)
    {
    if (bSetFlag)
    {
	::SetWindowText (m_hEditCtrl, (LPCTSTR) m_pInitParam ->pParam);
    }
    else
    {
	::GetWindowText (m_hEditCtrl, (LPTSTR ) m_pInitParam ->pParam, m_pInitParam ->dParam);
    }
    }
};

Bool GEditValueDialog::On_InitDialog (GWinMsg & m)
{
    if (NULL	     != 
       (m_hEditCtrl   = ::GetDlgItem (m_hWnd, IDC_EDIT)))
    {
	m_nEditCtrlId = IDC_EDIT;
    }
    return GModalDialog::On_InitDialog (m);
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
Bool GetValue (HWND hParent, HWND hCtrl, int x, int y, const char *, _U32 & Value)
{
    RECT		rCtrl	  ;

    GEditValueDialog	EditDialog;
    TCHAR		pEditString [256];

    GetWindowRect  (hCtrl, & rCtrl);
//  ClientToScreen (hCtrl, & rCtrl);

    GModalDialog::InitParam Param = {0, rCtrl.right, rCtrl.top, 0, 0, pEditString, sizeof (pEditString) / sizeof (TCHAR)};

    ::wsprintf (pEditString, "%d", Value);

    if (IDOK == EditDialog.Exec (NULL	     ,
				 MAKEINTRESOURCE (IDD_PARAM_EDIT),
				 hParent     ,
			       & Param	     ))
    {

printf ("\t\tRead %s\n", pEditString);

    ::sscanf   (pEditString, "%d", & Value);

	return True ;
    }
	return False;
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
class COutScreen : public GButton
{
public :
		COutScreen (void * pWndId = NULL) :

		GButton (pWndId)
		{
		    m_iLineHeight = 0;
		};

	void		On_Draw		    (LPDRAWITEMSTRUCT);

	void		On_IRRPParam	    (_U32 uParamId, _U32 uValue);

virtual	Bool		On_ReflectWinMsg    (GWinMsg & m);

protected :

	int		m_iLineHeight;
};

void COutScreen::On_Draw (LPDRAWITEMSTRUCT pdi)
{
    if (ODA_DRAWENTIRE != pdi ->itemAction)
    {
	return;
    }
	FillRect (pdi ->hDC, & pdi ->rcItem, (HBRUSH) ::GetStockObject (BLACK_BRUSH));

};

Bool COutScreen::On_ReflectWinMsg (GWinMsg & m)
{
    if (WM_DRAWITEM == m.uMsg)
    {
	On_Draw ((LPDRAWITEMSTRUCT) m.lParam);

	return m.DispatchComplete ();
    }

	return m.DispatchComplete ();
};
//
//[]------------------------------------------------------------------------[]


//[]------------------------------------------------------------------------[]
//
class CURPPanel : public GDialog
{
public :
		CURPPanel () : GDialog (NULL)
		{
		    m_pIRRPUrp	    = NULL ;
		    m_hIRRPCallBack = NULL ;

		    m_pCurDcrShow   = NULL ;
		};

	Bool		On_Create		(void *);

	void		On_Destroy		();

	Bool		On_CtrlDefault		(GWinMsg &);

	Bool		On_IndicatorState	(GWinMsg &);
	Bool		On_IndicatorBlock	(GWinMsg &);

	Bool		On_WM_MouseMove		(GWinMsg &);

	Bool		On_CtrlUAnode		(GWinMsg &);
	Bool		On_CtrlFocus		(GWinMsg &);
	Bool		On_CtrlIFilamentPre	(GWinMsg &);
	Bool		On_CtrlIFilament	(GWinMsg &);
	Bool		On_CtrlMAS		(GWinMsg &);
	Bool		On_CtrlExposeTime	(GWinMsg &);
	Bool		On_CtrlShot		(GWinMsg &);
	Bool		On_CtrlOutScreen	(GWinMsg &);

	void		On_IRRPParam		(_U32 uParamId,
						 _U32 uValue  );
	void		On_IRRPCallBack		(IRRPCallBack::Handle *);

	Bool		On_DlgMsg		(GWinMsg &);

protected :

	IRRPUrp	*		m_pIRRPUrp	    ;

	CWndCallBack		m_sIRRPCallBack	    ;
	IRRPCallBack::Handle *	m_hIRRPCallBack	    ;

	CParamIndicator		m_IndicatorState    ;
	CParamIndicator		m_IndicatorBlock    ;

	CParamControl		m_CtrlUAnode	    ;

	CParamControl		m_CtrlFocus	    ;
	CParamControl		m_CtrlIFilamentPre  ;
	CParamControl		m_CtrlIFilament	    ;

	CParamControl		m_CtrlMAS	    ;
	CParamControl		m_CtrlExposeTime    ;

	COutScreen		m_OutScreen	    ;

	const CParamIndicator::Descriptor *
				m_pCurDcrShow	    ;

	int			m_nGetHV	    ;
	int			m_nGetACN	    ;
	int			m_nGetACP	    ;
	int			m_nGetPFC	    ;
	int			m_nGetFC	    ;
	int			m_nGetMAS	    ;
	int			m_nGetTime	    ;
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
void CURPPanel::On_Destroy ()
{
    if (m_hIRRPCallBack)
    {
	m_hIRRPCallBack ->Close ();
	m_hIRRPCallBack  = NULL   ;
    }
};

Bool CURPPanel::On_Create (void * pParam)
{
    m_nGetHV	=
    m_nGetACN	=
    m_nGetACP	=
    m_nGetPFC	=
    m_nGetFC	=
    m_nGetMAS	=
    m_nGetTime	= 0 ;

    if (! GDialog::On_Create (pParam))
    {	return False;}

    static const CParamIndicator::Descriptor s_BlockDescriptor [] =
    {
	{0x00000001, _T("���������� : ��� ������� �����.")},
	{0x00000002, _T("���������� : ���������� ������� ����������.")},
	{0x00000004, _T("���������� : ����� ���.")},
	{0x00000008, _T("���������� : �� ����� ��������.")},
	{0x00000010, _T("���������� : �������� ������.")},
	{0x00000020, _T("���������� : ������� �����.")},
	{0x00000040, _T("���������� : ���������� ����������� ������� ���������.")},
	{0x00000100, _T("���������� : ���������� ���������� �������������.")},
	{0x00000200, _T("���������� : ��� ������.")},
	{0x00000400, _T("���������� : ���������� �������� ����������.")},
	{0x00000800, _T("���������� : ���������� �������� ����.")},
	{0x00001000, _T("���������� : ���������� ���� ������ � ������ ��������.")},
	{0x00002000, _T("���������� : ���������� ���� ������ � ������ ������.")},
	{0x00004000, _T("���������� : ��� �������� ������.")},
	{0x00010000, _T("���������� : ���������� ���� � ��������� ���� ��������������.")},
	{0x00020000, _T("���������� : ����� 10% ����������� �������� ����.")},
	{0x00040000, _T("���������� : ����� 10% ���������� �������� ����������.")},
	{0x00080000, _T("���������� : ����� 10% ���������� �������� ����.")},
	{0x00100000, _T("���������� : ���������� ���� ���� �����.")},
	{0x00200000, _T("���������� : ���������� ���� ���� �����.")},
	{0x00400000, _T("���������� : ���������� �������� ����������.")},
	{0x00000000, NULL}
    };

    m_IndicatorState	.Create (this, GetCtrlHWND (IDC_INDICATOR_STATE	    ), (void *) & g_StateDescriptor);
    m_IndicatorBlock	.Create (this, GetCtrlHWND (IDC_INDICATOR_BLOCK	    ), (void *) & s_BlockDescriptor);

    m_CtrlUAnode	.Init   (90, 60, "��.");
    m_CtrlUAnode	.Create (this, GetCtrlHWND (IDC_PARAM_UANODE	    ));

    m_CtrlFocus		.Init	(90, 60, "");
    m_CtrlFocus		.Create (this, GetCtrlHWND (IDC_PARAM_FOCUS	    ));
    m_CtrlIFilamentPre	.Init	(90, 60, "�.�.");
    m_CtrlIFilamentPre	.Create (this, GetCtrlHWND (IDC_PARAM_IFILAMENTPRE  ));
    m_CtrlIFilament	.Init	(90, 60, "�.�.");
    m_CtrlIFilament	.Create (this, GetCtrlHWND (IDC_PARAM_IFILAMENT	    ));

    m_CtrlMAS		.Init	(90, 60, "���.");
    m_CtrlMAS		.Create (this, GetCtrlHWND (IDC_PARAM_MAS	    ));
    m_CtrlExposeTime	.Init	(90, 60, "�c.");
    m_CtrlExposeTime	.Create (this, GetCtrlHWND (IDC_PARAM_EXPOSETIME    ));

    m_OutScreen		.Create (this, GetCtrlHWND (IDC_OUTSCREEN	    ));

    if (s_pIRRPService)
    {
    if (ERROR_SUCCESS == s_pIRRPService ->CreateRRPDevice 
				((void **) & m_pIRRPUrp	    ,
				  IID_IUnknown		    ,
				 (void  *) 0x80000000	    ,
				  IID_IUnknown		    ))
    {
	m_sIRRPCallBack.SetRecvHWND (GetHWND ());

	m_pIRRPUrp ->Connect (m_hIRRPCallBack, 
			    & m_sIRRPCallBack);
    }
    }

    return True;
};

Bool CURPPanel::On_IndicatorState (GWinMsg & m)
{
    return m_IndicatorState.On_ReflectWinMsg (m);
};

Bool CURPPanel::On_IndicatorBlock (GWinMsg & m)
{
    return m_IndicatorBlock.On_ReflectWinMsg (m);
};

Bool CURPPanel::On_CtrlUAnode (GWinMsg & m)
{
    _U32    uValue = 0;

    if ((WM_COMMAND == m.uMsg) && IS_WM_COMMAND_CODE (BN_CLICKED))
    {
    if (GetValue (GetHWND (), m_CtrlUAnode.GetHWND (), 0, 0, "ddd", uValue))
    {
	RRPSetParam (m_pIRRPUrp, IRRPUrp::ID_UAnode, uValue);
    }
    return m.DispatchComplete ();
    }
    return m_CtrlUAnode.On_ReflectWinMsg (m);
};

Bool CURPPanel::On_CtrlFocus (GWinMsg & m)
{
    _U32    uValue = 0;

    if ((WM_COMMAND == m.uMsg) && IS_WM_COMMAND_CODE (BN_CLICKED))
    {
    if (GetValue (GetHWND (), m_CtrlFocus.GetHWND (), 0, 0, "ddd", uValue))
    {
	RRPSetParam (m_pIRRPUrp, IRRPUrp::ID_Focus, uValue & 0x01);
//	RRPRefresh
    }
    return m.DispatchComplete ();
    }
    return m_CtrlFocus.On_ReflectWinMsg (m);
};

Bool CURPPanel::On_CtrlIFilamentPre (GWinMsg & m)
{
    _U32    uValue = 0;

    if ((WM_COMMAND == m.uMsg) && IS_WM_COMMAND_CODE (BN_CLICKED))
    {
    if (GetValue (GetHWND (), m_CtrlIFilamentPre.GetHWND (), 0, 0, "ddd", uValue))
    {
	RRPSetParam (m_pIRRPUrp, IRRPUrp::ID_IFilamentPre, uValue);
    }
    return m.DispatchComplete ();
    }
    return m_CtrlIFilamentPre.On_ReflectWinMsg (m);
};

Bool CURPPanel::On_CtrlIFilament (GWinMsg & m)
{
    _U32    uValue = 0;

    if ((WM_COMMAND == m.uMsg) && IS_WM_COMMAND_CODE (BN_CLICKED))
    {
    if (GetValue (GetHWND (), m_CtrlIFilament.GetHWND (), 0, 0, "ddd", uValue))
    {
	RRPSetParam (m_pIRRPUrp, IRRPUrp::ID_IFilament, uValue);
    }
    return m.DispatchComplete ();
    }
    return m_CtrlIFilament.On_ReflectWinMsg (m);
};

Bool CURPPanel::On_CtrlMAS (GWinMsg & m)
{
    _U32    uValue = 0;

    if ((WM_COMMAND == m.uMsg) && IS_WM_COMMAND_CODE (BN_CLICKED))
    {
    if (GetValue (GetHWND (), m_CtrlMAS.GetHWND (), 0, 0, "ddd", uValue))
    {
	RRPSetParam (m_pIRRPUrp, IRRPUrp::ID_MAS, uValue);
    }
    return m.DispatchComplete ();
    }
    return m_CtrlMAS.On_ReflectWinMsg (m);
};

Bool CURPPanel::On_CtrlExposeTime (GWinMsg & m)
{
    _U32    uValue = 0;

    if ((WM_COMMAND == m.uMsg) && IS_WM_COMMAND_CODE (BN_CLICKED))
    {
    if (GetValue (GetHWND (), m_CtrlExposeTime.GetHWND (), 0, 0, "ddd", uValue))
    {
	RRPSetParam (m_pIRRPUrp, IRRPUrp::ID_ExposeTime, uValue);
    }
    return m.DispatchComplete ();
    }
    return m_CtrlExposeTime.On_ReflectWinMsg (m);
};

Bool CURPPanel::On_CtrlShot (GWinMsg & m)
{
    if (m_pIRRPUrp)
    {
    IRRPUrp::HVCommand	HVCommand;

    switch (LOWORD (m.wParam))
    {
    case IDC_SHORT_RELEASE :
	HVCommand = IRRPUrp::HVRelease;
	break;

    case IDC_SHORT_ON :
	HVCommand = IRRPUrp::HVOn;
	break;

    case IDC_SHORT_PREPARE :
	HVCommand = IRRPUrp::HVPrepare;
	break;

    default:
	return m.DispatchComplete ();
    };

	m_pIRRPUrp ->SetHV (HVCommand);
    }
	return m.DispatchComplete ();
};

Bool CURPPanel::On_WM_MouseMove (GWinMsg & m)
{
    GRect   wr	;
    const CParamIndicator::Descriptor * 
	    pDcr;

    m_IndicatorState.GetWindowRect (& wr);
    if (wr.Contains (GET_X_LPARAM (m.lParam), GET_Y_LPARAM (m.lParam)))
    {
    if (NULL != (pDcr = m_IndicatorState.GetOverDescriptor (GET_X_LPARAM (m.lParam) - wr.x,
							    GET_Y_LPARAM (m.lParam) - wr.y)))
    {
	if (m_pCurDcrShow != pDcr)
	{
	    m_pCurDcrShow  = pDcr;

	    SetCtrlText (IDC_INDICATOR_HELP, pDcr ->pName);
	}
	return m.DispatchComplete ();
    }
    }

    m_IndicatorBlock.GetWindowRect (& wr);
    if (wr.Contains (GET_X_LPARAM (m.lParam), GET_Y_LPARAM (m.lParam)))
    {
    if (NULL != (pDcr = m_IndicatorBlock.GetOverDescriptor (GET_X_LPARAM (m.lParam) - wr.x,
							    GET_Y_LPARAM (m.lParam) - wr.y)))
    {
	if (m_pCurDcrShow != pDcr)
	{
	    m_pCurDcrShow  = pDcr;

	    SetCtrlText (IDC_INDICATOR_HELP, pDcr ->pName);
	}
	return m.DispatchComplete ();
    }
    }
	if (m_pCurDcrShow)
	{
	    m_pCurDcrShow = NULL;

	    SetCtrlText (IDC_INDICATOR_HELP, "");
	}

	return False;
};

Bool CURPPanel::On_CtrlOutScreen (GWinMsg & m)
{
    return m_OutScreen.On_ReflectWinMsg (m);
};
//
//[]------------------------------------------------------------------------[]
//
void CURPPanel::On_IRRPParam (_U32 uParamId, _U32 uValue)
{
    printf ("\tParam : %08X (%08X, %d)\n", uParamId, uValue, uValue);

    if (0x80000000 == uParamId)
    {
	m_IndicatorState.SetValue (uValue);
    }
    if (0x8000D1D0 == uParamId)
    {
	m_IndicatorBlock.SetValue (uValue);
    }
    if (0x8000D4D3 == uParamId)
    {
	EnableCtrl (IDC_LED_READY   , (uValue & 0x0001) ? True : False);
	EnableCtrl (IDC_LED_PREPARE , (uValue & 0x0100) ? True : False);
	EnableCtrl (IDC_LED_BEGSHOT , (uValue & 0x0200) ? True : False);
	EnableCtrl (IDC_LED_ENDSHOT , (uValue & 0x0400) ? True : False);
    }
    if (0x80009E9D == uParamId)
    {
	m_CtrlUAnode.SetValue (uValue);
    }
    if (0x80000097 == uParamId)
    {
	m_CtrlFocus.SetValue ((uValue & 0x01) ? "�������" : "�����");
    }
    if ((0x8000A2A1 == uParamId) || (0x8000A6A5 == uParamId))
    {
	m_CtrlIFilamentPre.SetValue (uValue);
    }
    if ((0x8000A4A3 == uParamId) || (0x8000A8A7 == uParamId))
    {
	m_CtrlIFilament.SetValue (uValue);
    }
    if (0x8000AAA9 == uParamId)
    {
	m_CtrlMAS.SetValue (uValue);
    }
    if (0x8000ACAB == uParamId)
    {
	m_CtrlExposeTime.SetValue (uValue);
    }
//
//............... Control
//
    int	    bDraw = -1;
    char    pBuf [256];

    if (0x8000E1E0 == uParamId)
    {
	m_nGetHV  = (short) uValue;
	bDraw	  = 0;
    }
    if (0x8000E5E4 == uParamId)
    {
	m_nGetACP = (short) uValue;
	bDraw	  = 0;
    }
    if (0x8000E7E6 == uParamId)
    {
	m_nGetACN = (short) uValue;
	bDraw	  = 0;
    }
    if (0x8000EFEE == uParamId)
    {
	m_nGetPFC = (short) uValue;
	bDraw	  = 1;
    }
    if (0x8000F1F0 == uParamId)
    {
	m_nGetFC  = (short) uValue ;
	bDraw	  = 1;
    }
    if (0x8000EBEA == uParamId)
    {
	m_nGetMAS  = (short) uValue;
	bDraw	   = 2;
    }
    if (0x8000E9E8 == uParamId)
    {
	m_nGetTime = (short) uValue;
	bDraw	   = 2;
    }

    if (0 == bDraw)
    {
	sprintf (pBuf, "UAnode : %3d      IAnode : +%4d -%4d", m_nGetHV, m_nGetACP, m_nGetACN);
	SetCtrlText (IDC_STATUS_0, pBuf);
    }
    if (1 == bDraw)
    {
	sprintf (pBuf, "IPre   : %3d      I : %3d", m_nGetPFC, m_nGetFC);
	SetCtrlText (IDC_STATUS_1, pBuf);
    }
    if (2 == bDraw)
    {
	sprintf (pBuf, "MAS    : %3d      Time : %3d", m_nGetMAS, m_nGetTime);
	SetCtrlText (IDC_STATUS_2, pBuf);
    }
};

void CURPPanel::On_IRRPCallBack (IRRPCallBack::Handle * hIRRPCallBack)
{
    Word	    uLength	;
    Word	    uType	;
    IRRPProtocol::DataPack *
		    pDataPack	;
    IRRPProtocol::ParamPack::Entry * 
		    pEntry	;

    if (hIRRPCallBack == m_hIRRPCallBack)
    {
	pDataPack = (IRRPProtocol::DataPack *) m_sIRRPCallBack.PeekHeadPacket ();

    while (pDataPack)
    {
	uLength = _GetBigEndianW (pDataPack, offsetof (IRRPProtocol::DataPack, uLength));
	uType	= _GetBigEndianW (pDataPack, offsetof (IRRPProtocol::DataPack, uType  ));

	if (0  == uType)
	{
		pEntry = (IRRPProtocol::ParamPack::Entry *)(pDataPack + 1);

	    while ((Byte *) pEntry < ((Byte *) pDataPack + uLength))
	    {
		On_IRRPParam (_GetBigEndianD (pEntry, offsetof (IRRPProtocol::ParamPack::Entry, uParamId)),
			      _GetBigEndianD (pEntry, offsetof (IRRPProtocol::ParamPack::Entry, uValue  )));
		pEntry ++;
	    }
	}

	pDataPack = (IRRPProtocol::DataPack *) m_sIRRPCallBack.PeekNextPacket ();
    }
    }
};
//
//[]------------------------------------------------------------------------[]
//
Bool CURPPanel::On_DlgMsg (GWinMsg & m)
{
    int	    nCtrlId;

    if (m_hIRRPCallBack && (CWndCallBack::RRPMSG == m.uMsg))
    {
	On_IRRPCallBack	 ((IRRPCallBack::Handle *) m.lParam);

	return m.DispatchComplete ();
    }

    if (WM_MOUSEMOVE == m.uMsg)
    {{
	On_WM_MouseMove (m);
    }}

    if (WM_COMMAND == m.uMsg)
    {
	if (IS_WM_COMMAND_ID_CODE (IDC_SHORT_PREPARE, BN_CLICKED)
	||  IS_WM_COMMAND_ID_CODE (IDC_SHORT_ON     , BN_CLICKED)
	||  IS_WM_COMMAND_ID_CODE (IDC_SHORT_RELEASE, BN_CLICKED))
	{
	    On_CtrlShot (m);

	    return m.DispatchComplete ();
	}
    }

    if (0 !=   (nCtrlId = IsControlNotify (m)))
    {
	switch (nCtrlId)
	{
	case IDC_INDICATOR_STATE    : return On_IndicatorState	 (m);
	case IDC_INDICATOR_BLOCK    : return On_IndicatorBlock	 (m);
	case IDC_PARAM_UANODE	    : return On_CtrlUAnode	 (m);
	case IDC_PARAM_FOCUS	    : return On_CtrlFocus	 (m);
	case IDC_PARAM_IFILAMENTPRE : return On_CtrlIFilamentPre (m);
	case IDC_PARAM_IFILAMENT    : return On_CtrlIFilament	 (m);
	case IDC_PARAM_MAS	    : return On_CtrlMAS		 (m);
	case IDC_PARAM_EXPOSETIME   : return On_CtrlExposeTime	 (m);
	case IDC_OUTSCREEN	    : return On_CtrlOutScreen	 (m);
	default :
	    break;
	};
    }
	return False;
};
//
//[]------------------------------------------------------------------------[]
























#endif // GLOBAL

























