//[]------------------------------------------------------------------------[]
// Main GWin User32 (GUI) extension declarations
//
//
// Created by GSh.
//[]------------------------------------------------------------------------[]

#ifndef __G_WINUSER_H
#define __G_WINUSER_H

#include "G_winbase.h"

//[]------------------------------------------------------------------------[]
// GWindow's Layout control flags
//
// CalcLayoutRect::layout param :
//

#define PushParam2(p1,p2)	    p1,p2
#define	PushParam4(p1,p2,p3,p4)	    p1,p2,p3,p4

#define SizeParam(p1,p2)	    PushParam2(p1,p2)
#define	LayoutParam(p1,p2,p3,p4)    PushParam4(p1,p2,p3,p4)
#define MinMaxParam(p1,p2,p3,p4)    PushParam4(p1,p2,p3,p4)


/*
#define GGF_CXX_STICKED_RLO	    0x00    // n_w[lo[x/y]/hi[x/y]] = n_rlo[x/y] + c[lo[x/y]/hi[x/y]]
#define GGF_CXX_STICKED_RHI	    0x80    // n_w[lo[x/y]/hi[x/y]] = n_rhi[x/y] - c[lo[x/y]/hi[x/y]]
#define	GGF_CXX_CONST_SIZE	    0x40    //


#define GGF_CXX_CONST		    0x00    //   c[lo[x/y]/hi[x/y]] = t_w[lo[x/y]/hi[x/y]] = const;
*/






#define GGF_LOLO_CONST_SIZE	    0x00
#define GGF_HIHI_CONST_SIZE	    0x80
#define GGF_LOHI		    0x81


#define	GetHorLayout(layout)	    (HIBYTE(LOWORD((DWord)layout)))
#define	GetVerLayout(layout)	    (LOBYTE(LOWORD((DWord)layout)))

/*
#define	GetGrowHor(layout)	    (HIWORD((DWord)layout))
#define	GetGrowVer(layout)	    (LOWORD((DWord)layout))

#define GetGrowFlags(growxxx)	    (HIBYTE((Word )growxxx))
#define GetGrawPriority(grawxxx)    (LOBYTE((Word )grawxxx))

#define	GetGrowHorFlags(layout)	    (HIBYTE(HIWORD((DWord)layout)))
#define	GetGrowVerFlags(layout)	    (HIBYTE(LOWORD((DWord)layout)))

#define GetGrowHorPriority(layout)  (LOBYTE(HIWORD((DWord)layout)))
#define GetGrowVerPriority(layout)  (LOBYTE(LOWORD((DWord)layout)))
*/
#define	GROW_PRIORITY_HI	    15
#define	GROW_PRIORITY_LO	    0

#define	UnlimitedSize		    0xFFFF

#define MakeLayout(lHor,lVer)	    ((((DWord)lHor)<<8)|(((DWord)lVer)<<0))


#define	Sticked_Top_Fixed_Height()	((GGF_LOHI << 16) | GGF_LOLO_CONST_SIZE)
#define	Sticked_Bottom_Fixed_Height()	((GGF_LOHI << 16) | GGF_HIHI_CONST_SIZE)



#define GGF_CXX_PROPORTIONAL	0x01	//   c[lo[x/y]/hi[x/y]] = abs(t_r[lo[x/y]/hi[x/y]] -
					//			      t_w[lo[x/y]/hi[x/y]])
#define	GetLoX_GF(layout)	(((DWord) layout)>>24)
#define	GetLoY_GF(layout)	(((DWord) layout)>>16)
#define	GetHiX_GF(layout)	(((DWord) layout)>> 8)
#define	GetHiY_GF(layout)	(((DWord) layout)>> 0)

#define	GGF_WLOX_RLO	0x0	    // wn.left   - rn.left   = wc.left   - rc.left   = clo_x;
#define	GGF_WLOX_RHI	0x8	    // wn.left	 - rn.right  = wc.left	 - rc.right  = clo_x;
#define	GGF_WLOY_RLO	0x0	    // wn.top	 - rn.top    = wc.top    - rc.top    = clo_y;
#define GGF_WLOY_RHI	0x4	    // wn.top	 - rn.bottom = wc.top	 - rc.bottom = clo_y;

#define GGF_WHIX_RLO	0x0	    // wn.right	 - rn.left   = wc.right  - rc.left   = chi_x;
#define	GGF_WHIX_RHI	0x2	    // wn.right	 - rn.right  = wc.right  - rc.right  = chi_x;
#define	GGF_WHIY_RLO	0x0	    // wn.bottom - rn.top    = wc.bottom - rc.top    = chi_y;
#define GGF_WHIY_RHI	0x1	    // wn.bottom - rn.bottom = wc.bootom - rc.bottom = chi_y;

#define GGF_CLOX_CONST	0x




/*
#define GGF_DEFAULT    (GGF_WLOX_RLO |	\
			GGF_WHIX_RLO |	\
		    	GGF_WLOY_RLO |	\
		    	GGF_WHIY_RLO )

	DWord WINAPI	CalcLayoutRect (      DWord   layout ,  // layout for template
					const GRect & rc     ,  // Current Base template
					const GRect & wc     ,  // Current Target for the Current Base template
					const GRect & rn     ,  // New Base
					      GRect & wn     ); // New Target for the New Base
// Z-order indexes
//
#define GZO_TOOL	((int) 255) // Topmost
#define	GZO_STATUS	((int) 254) // Top
#define	GZO_CLIENT	((int) 128)

	void  WINAPI	CalcLayoutSize (      int     n	     ,	// Item's num
					      LONG *& x	     ,	// [in/out] Target
					const int  *& z	     ,	// [in	  ] Z-order  array
					const LONG *& min    ,	// [in	  ] Min size array
					const LONG *& max    ,	// [in	  ] Max size array
					const LONG *& tmp    );	// [in	  ] Template array

#pragma pack (_M_PACK_LONG)

#pragma pack ()
*/
//
//[]------------------------------------------------------------------------[]

#endif//__G_WINUSER_H

//[]------------------------------------------------------------------------[]
/*
class GApplicationControl : public GControl
{
protected :
			GApplicationControl (const void * pWndId) :

			GControl	    (m_WndClass,  pWndId)
			{
			//  new (& m_WinMsgHandler			    )
			//	   TWinMsgHandler <GApplicationControl>
			//	 & GApplicationControl::DispatchWinMsg, this);

			    m_WndProcDef = ::DefWindowProc;
			    m_WinMsgProc = new GWinMsgThunk (m_WinMsgHandler, & m_WndProcDef);
			};
protected :

	DWORD		CreateException	    (PEXCEPTION_RECORD);

private :

static	LPCTSTR	GAPI	GetClassInfo	    (WNDCLASSEX * wc = NULL)
			{
			    LPCTSTR pName = _T("GWindow");

			    if (wc)
			    {
				wc ->cbSize	    = sizeof (WNDCLASSEX);
				wc ->style	    = 0    ;
				wc ->lpfnWndProc    = InitWndProc ;

				wc ->cbClsExtra	    = 0    ;
				wc ->cbWndExtra	    = 0    ;
				wc ->hInstance	    = NULL ;
				wc ->hIcon	    = NULL ;
				wc ->hCursor	    = ::LoadCursor (NULL, IDC_ARROW);

				wc ->hbrBackground  = NULL ;
				wc ->lpszMenuName   = NULL ;
				wc ->lpszClassName  = pName;
				wc ->hIconSm	    = NULL ;
			    };

			    return  pName;
			};
private :

static	GWindowClass		m_WndClass	;

protected :

	WNDPROC			m_WndProcDef	;
};

GWindowClass	   GApplicationControl::m_WndClass =
{THK_WINDOW_CLASS, GApplicationControl::GetClassInfo};
*/
//[]------------------------------------------------------------------------[]




/*

namespace DragProcReason
{
    typedef enum DragProcReason_tag
    {
	ShowGhost   = 0,
	HideGhost   = 1,
	End	    = 2,
	EndCancel   = 3,

    } Reason;
}
    typedef void	(GAPI * DragProc)	(GWindow *		  ,
						 DragProcReason::Reason   ,
					   const RECT *		 rGhost	  ,
						 void *		 pContext );



	GWindow *		m_pCaptureWindow   ;	// Dragging control members
	RECT			m_rCaptureGhost	   ;
	RECT			m_rCaptureLimit	   ;
	POINT			m_pCaptureStart	   ;
	POINT			m_pCaptureCatch	   ;

	DragProc		m_pDragProc	   ;
	void *			m_pDragProcContext ;














    TCurrent_BeginDragging	  (this, GET_X_LPARAM (m.dLParam),
				     GET_Y_LPARAM (m.dLParam),
				   & rGhost,
				   & rLimit,
				     GStickedWindow::DragWindowProc, NULL);



void GStickedWindow::DragWindowProc (      DragProcReason::Reason  Reason ,
				     const RECT *		   rGhost )
{

    switch (Reason)
    {
	case DragProcReason::ShowGhost :
	case DragProcReason::HideGhost :
	    {
		HDC	    dc;
		HBRUSH	    hBrush;

		if (NULL != (dc = ::GetDCEx   (GetHWND (), NULL, DCX_PARENTCLIP | DCX_LOCKWINDOWUPDATE)))
		{
		if (NULL != (hBrush = CreateHalftoneBrush ()))
		{
		    FrameRectEx	(dc, rGhost, hBrush,
				     IsDragBarVer ()
				  ? (rGhost ->right  - rGhost ->left)
				  : (rGhost ->bottom - rGhost ->top ), PATINVERT);

		    ::DeleteObject (hBrush);
		}
				  ::ReleaseDC (GetHWND (), dc);
		}
	    }
	    break;

	case DragProcReason::End       :
	case DragProcReason::EndCancel :
	    {
		GRect    rw	= GetWindowRect ();
		RECT	 rh	= * rGhost;
		
			       ClientToScreen (& rh);
		GetParent () ->ScreenToClient (& rh);

		switch (m_DBType)
		{
		    case WindowSide::Right :
			rw.cx = rh.right - rw.x;
			break;

		    case WindowSide::Top :
			rw.cy = rw.y + rw.cy - rh.top;
			rw. y = rh.top;
			break;

		    case WindowSide::Left :
			rw.cx = rw.x + rw.cx - rh.left;
			rw. x = rh.left;
			break;

		    case WindowSide::Bottom :
			rw.cy = rh.bottom - rw.y;
			break;
		};

		GWindow::SetWindowPos (& rw, IsDragBarVer () ? 0x02 : 0x01);
	    }
	    break;
    };    
};




void TCurrent_EndDragging (Bool bCancel)
{
    GWinMsgThread * t;

    if (NULL == (t = (GWinMsgThread *) & TCurrent ()) ->m_pCaptureWindow)
    {
	return;
    }

    if (bCancel)
    {
	TCurrent_DoDragging 
			 (t ->m_pCaptureStart.x,
			  t ->m_pCaptureStart.y);
    }
       (t ->m_pDragProc) (t ->m_pCaptureWindow	    ,
			  DragProcReason::HideGhost ,
			& t ->m_rCaptureGhost	    ,
			  t ->m_pDragProcContext    );

       (t ->m_pDragProc) (t ->m_pCaptureWindow	    ,
	      (bCancel) ? DragProcReason::EndCancel :
			  DragProcReason::End	    ,
			& t ->m_rCaptureGhost	    ,
			  t ->m_pDragProcContext    );

	t ->m_pCaptureWindow = NULL;

	::ReleaseCapture  ();
};

Bool TCurrent_BeginDragging  (	    GWindow *	pWindow	  ,
				    int		x	  ,
				    int		y	  ,
			      const RECT  *	rGhost	  ,
			      const RECT  *	rLimit	  ,
				    DragProc	pDragProc ,
				    void *	pDragProcContext)
{
    GWinMsgThread * t =
   (GWinMsgThread *)    & TCurrent ();

    if (t ->m_pCaptureWindow)
    {
	TCurrent_EndDragging (True);
    } 

	 ::SetCapture (	    pWindow ->GetHWND ());
    if	(::GetCapture () != pWindow ->GetHWND ())
    {
	return False;
    }
	t ->m_pCaptureWindow	   = pWindow ;

        t ->m_rCaptureGhost	   = * rGhost;
    if  (rLimit)
    {
	t ->m_rCaptureLimit	   = * rLimit;
    }
    else
    {
	t ->m_rCaptureLimit.left   =
	t ->m_rCaptureLimit.top    = - 32000;
	t ->m_rCaptureLimit.right  =
	t ->m_rCaptureLimit.bottom =   32000;
    }
	t ->m_pCaptureCatch.x	   =
       (t ->m_pCaptureStart.x	   = x) - rGhost ->left;
	t ->m_pCaptureCatch.y	   = 
       (t ->m_pCaptureStart.y	   = y) - rGhost ->top ;

	t ->m_pDragProc		   = pDragProc	       ;
	t ->m_pDragProcContext	   = pDragProcContext  ;

	
       (t ->m_pDragProc) (t ->m_pCaptureWindow	    ,
			  DragProcReason::ShowGhost ,
			& t ->m_rCaptureGhost	    ,
			  t ->m_pDragProcContext    );
	return True;
};

Bool TCurrent_DoDragging (int x , int y)
{
    GWinMsgThread * t;

    if (NULL == (t = (GWinMsgThread *) & TCurrent ()) ->m_pCaptureWindow)
    {
	return False;
    }

    RECT    rn = {x - t ->m_pCaptureCatch.x,
		  y - t ->m_pCaptureCatch.y,
		  x - t ->m_pCaptureCatch.x + (t ->m_rCaptureGhost.right  - t ->m_rCaptureGhost.left),
		  y - t ->m_pCaptureCatch.y + (t ->m_rCaptureGhost.bottom - t ->m_rCaptureGhost.top )};

						// Look at dragging limits
						//
    if (rn.left	  <  t ->m_rCaptureLimit.left)
    {
	MoveX (& rn, t ->m_rCaptureLimit.left	- rn.left  );
    }
    if (rn.top	  <  t ->m_rCaptureLimit.top )
    {
	MoveY (& rn, t ->m_rCaptureLimit.top	- rn.top   );
    }
    if (rn.right  >  t ->m_rCaptureLimit.right)
    {
	MoveX (& rn, t ->m_rCaptureLimit.right	- rn.right );
    }
    if (rn.bottom >  t ->m_rCaptureLimit.bottom)
    {
	MoveY (& rn, t ->m_rCaptureLimit.bottom - rn.bottom);
    }

    if ((t ->m_rCaptureGhost.left   != rn.left	 )
    ||  (t ->m_rCaptureGhost.right  != rn.right	 )
    ||	(t ->m_rCaptureGhost.top    != rn.top	 )
    ||	(t ->m_rCaptureGhost.bottom != rn.bottom ))
    {
        (t ->m_pDragProc) (t ->m_pCaptureWindow	     ,
			   DragProcReason::HideGhost ,
			 & t ->m_rCaptureGhost	     ,
			   t ->m_pDragProcContext    );

	 t ->m_rCaptureGhost = rn;

        (t ->m_pDragProc) (t ->m_pCaptureWindow	     ,
			   DragProcReason::ShowGhost ,
			 & t ->m_rCaptureGhost	     ,
			   t ->m_pDragProcContext    );
    }

	return True ;
};

*/

/*

//	    SetThemeAppProperties (STAP_ALLOW_NONCLIENT | STAP_ALLOW_CONTROLS);

	if (::IsThemeActive ())
	{
	    printf ("Theme active...\n");
	}


//  SetWindowTheme (GetHWND (), L"", L"");
//  SetWindowTheme (m_ControlBar.GetHWND (), L"Communications") L"", L"Rebar");
//  SetWindowTheme (m_MenuBar   .GetHWND (), L"", L"");

    ::SetWindowTheme(m_ControlBar.GetHWND (), L"Comctl32", L"Rebar");
*/




