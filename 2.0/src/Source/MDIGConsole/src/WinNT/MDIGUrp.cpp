//[]------------------------------------------------------------------------[]
// MDIGConsole CUrpPanel dialog defintion
//
//
// Created by GSh.
//[]------------------------------------------------------------------------[]

#include "MDIGUtils.h"

#define	CALTUBE_RELEASE	    1

//[]------------------------------------------------------------------------[]
//
class CUrpShotCount : public GDialog
{
public :

		CUrpShotCount () : GDialog (NULL)
		{
		    m_uExitCode = IDOK;
		};

	Bool		On_InitDialog		(GWinMsg &);
	Bool		On_DlgMsg		(GWinMsg &);


	int		m_uExitCode ;
};

Bool CUrpShotCount::On_InitDialog (GWinMsg & m)
{
    SYSTEMTIME sTime;
    FILETIME   fTime;
    int	       cGood;
    int	       cBad ;
    DWord      cQ   ;
    DWord      cMAS ;
    TCHAR      pBuf [128];

    GetShotCount (fTime, cGood, cBad, cQ, cMAS);

    ::FileTimeToLocalFileTime (& fTime, & fTime);
    ::FileTimeToSystemTime    (& fTime, & sTime);

    ::wsprintf  (pBuf, "%02d/%02d/%02d   %02d:%02d.%02d", sTime.wDay , sTime.wMonth , sTime.wYear   ,
							  sTime.wHour, sTime.wMinute, sTime.wSecond );
    SetCtrlText (IDC_STARTDATE, pBuf);

    ::wsprintf  (pBuf, "%d/%d", cGood, cBad);
    SetCtrlText (IDC_SHOTCOUNT, pBuf);

    SetCtrlText (IDC_MASCOUNT , cMAS);
    SetCtrlText (IDC_QCOUNT   , cQ  );

    return m.DispatchCompleteDlgEx (True);
};

Bool CUrpShotCount::On_DlgMsg (GWinMsg & m)
{
    switch (IsCommand (m))
    {
    case IDCANCEL :

	m_uExitCode = IDCANCEL ;

	ClearShotCount ();

    case IDOK :
	return (TCurrent () ->GetCurApartment () ->BreakFromWorkLoop (), True);
    }

    return GDialog::On_DlgMsg (m);
};

class CUrpPanel : public GDialog
{
public :
		CUrpPanel () : GDialog (NULL)
		{
		    m_bHeatForce    =
		    m_bHeatTube	    =
		    m_bNoCharge	    = False;

		    m_pIRRPUrp	    = NULL ;
		    m_hIRRPCallBack = NULL ;
		    m_pCurDcrShow   = 
		    m_pCurDcrShow2  = NULL ;

		    m_bKKMControl   = False;
		    m_bBigFocus	    = False;
		    m_bIAnodeMode   = False;

		    m_pShotIrp	    = NULL ;

		    m_bIAnodeRetention 
				    = False;
		    m_cShotCount    =
		    m_cShotCountBad =	  0;
		};

	void		SetEquMask		(DWord dEquMask);

protected :


	Bool		PreprocMessage		(MSG *);

	Bool		On_ShotCount		();

	Bool		On_Shot			(IRRPUrp::HVCommand);

	Bool		On_ShotCycle		();
	void		Do_ShotCycleCancel	();

	Bool		On_RadFocus		(GWinMsg &, int);
	Bool		On_CtrlMode		();

	Bool		On_EdtUAnode		(GWinMsg &);

	Bool		On_Edt			(GWinMsg &);

	Bool		On_WM_MouseMove		(GWinMsg &);

	Bool		On_DrawStatus		(DRAWITEMSTRUCT *);

	Bool		On_DlgMsg		(GWinMsg &);

	Bool		On_InitDialog		(GWinMsg &);

	void		On_Destroy		();
	Bool		On_Create		(void *);

protected :

	void		On_IRRPParam		(_U32, _U32);
	void		On_IRRPCallBack		(IRRPCallBack::Handle *);

	GIrp *		Co_ShotCycle		(GIrp *, void *);

protected :

	CIndicatorBar		m_IndBlock	;
	const CIndicatorBar::Descriptor *
				m_pCurDcrShow	;
	const CIndicatorBar::Descriptor *
				m_pCurDcrShow2	;

	CLedIndicator		m_oLedUrpOnline	;
	CLedIndicator		m_oLedKKMOnline	;

	CLedIndicator		m_oLedShotReady	;

	CLedIndicator		m_oLedShotPrepare ;
	CLedIndicator		m_oLedShotOn	  ;
	CLedIndicator		m_oLedShotRelease ;

	CLedIndicator		m_oLedHV	;

	Bool			m_bHeatForce	;
	Bool			m_bHeatTube	;
	Bool			m_bNoCharge	;

	IRRPUrp	*		m_pIRRPUrp      ;

	CWndCallBack		m_sIRRPCallBack ;
	IRRPCallBack::Handle *	m_hIRRPCallBack	;

	CEditHandler		m_hEdit		;

	Bool			m_bKKMControl	;
	Bool			m_bBigFocus	;
	Bool			m_bIAnodeMode	;

	GIrp *			m_pShotIrp    	;

	Bool			m_bIAnodeRetention ;

	int			m_cShotCount	;
	int			m_cShotCountBad ;
};
//
//[]------------------------------------------------------------------------[]
//
void CUrpPanel::SetEquMask (DWord dEquMask)
{
    m_bKKMControl = (dEquMask & IRRPUrp::EquMask_KKM) ? True : False;

    m_oLedKKMOnline.SetColor (NULL, CLedIndicator::Red);
    m_oLedKKMOnline.TurnOn 
		    (GetCtrlHWND (IDC_KKM_ONLINE_LED), m_bKKMControl);

    SetCtrlText (IDC_KKM_DISP_UINP,   "");
    SetCtrlText (IDC_KKM_DISP_UOUT,   "");
    SetCtrlText (IDC_KKM_DISP_IINP,   "");

    RRPSetParam (m_pIRRPUrp, IRRPUrp::ID_ParSetEquMask, dEquMask);
};

void CUrpPanel::On_Destroy ()
{
	Do_ShotCycleCancel ();

    if (m_pIRRPUrp)
    {
    if (m_hIRRPCallBack)
    {
	m_hIRRPCallBack ->Close ();
    }
	m_pIRRPUrp ->Release ();
	m_pIRRPUrp = NULL;
    }
};

Bool CUrpPanel::On_InitDialog (GWinMsg & m)
{
    return m.DispatchCompleteDlgEx (False);
};

Bool CUrpPanel::On_Create (void * pParam)
{
    if (! GDialog::On_Create (pParam))
    {	return False;};

    static const CIndicatorBar::Descriptor s_BlockDescriptor [] =
    {
	{0x00000001, LoadString (IDS_URPBLK_00000001)},
	{0x00000002, LoadString (IDS_URPBLK_00000002)},
	{0x00000004, LoadString (IDS_URPBLK_00000004)},
	{0x00000008, LoadString (IDS_URPBLK_00000008)},
	{0x00000010, LoadString (IDS_URPBLK_00000010)},
//	{0x00000020, _T("������� �����.")},
	{0x00000040, LoadString (IDS_URPBLK_00000040)},
	{0x00000100, LoadString (IDS_URPBLK_00000100)},
	{0x00000200, LoadString (IDS_URPBLK_00000200)},
	{0x00000400, LoadString (IDS_URPBLK_00000400)},
	{0x00000800, LoadString (IDS_URPBLK_00000800)},
	{0x00001000, LoadString (IDS_URPBLK_00001000)},
	{0x00002000, LoadString (IDS_URPBLK_00002000)},
//	{0x00004000, _T("��� �������� ������.")},
	{0x00010000, LoadString (IDS_URPBLK_00010000)},
	{0x00020000, LoadString (IDS_URPBLK_00020000)},
	{0x00040000, LoadString (IDS_URPBLK_00040000)},
	{0x00080000, LoadString (IDS_URPBLK_00080000)},
	{0x00100000, LoadString (IDS_URPBLK_00100000)},
	{0x00200000, LoadString (IDS_URPBLK_00200000)},
	{0x00400000, LoadString (IDS_URPBLK_00400000)},
	{0x00000000, NULL}
    };

    m_IndBlock	.Create (this, GetCtrlHWND (IDC_URP_BLOCK_IND	), (void *) & s_BlockDescriptor);

    ::SetWindowStyles 
    (GetCtrlHWND    (IDC_URP_STATUS_STC	  ), 0x0000000F, SS_OWNERDRAW);
    SendCtrlMessage (IDC_URP_STATUS_STC	   , WM_SETFONT, (WPARAM) ::GetBoaldGUIFont (), (LPARAM) 0);

    m_oLedUrpOnline.SetColor (NULL, CLedIndicator::Red);
    m_oLedUrpOnline.TurnOn   (NULL, True);

    m_oLedShotReady.SetColor (NULL, CLedIndicator::Red);
    m_oLedShotReady.TurnOn   (NULL, True);

    m_oLedHV	   .SetColor (NULL, CLedIndicator::Yellow);

    SendCtrlMessage (IDC_URP_SETFIELDSIZE, CB_ADDSTRING, (WPARAM) 0, (LPARAM) LoadString (IDS_FIELD_FULL));
    SendCtrlMessage (IDC_URP_SETFIELDSIZE, CB_ADDSTRING, (WPARAM) 0, (LPARAM) _T("  21 x 30"));
    SendCtrlMessage (IDC_URP_SETFIELDSIZE, CB_ADDSTRING, (WPARAM) 0, (LPARAM) _T("  18 x 24"));
    SendCtrlMessage (IDC_URP_SETFIELDSIZE, CB_ADDSTRING, (WPARAM) 0, (LPARAM) _T("  12 x 12"));
    SendCtrlMessage (IDC_URP_SETFIELDSIZE, CB_SETCURSEL, (WPARAM) 0, (LPARAM) 0);

    SendCtrlMessage (IDC_URP_DISP_SETFOCUS , WM_SETFONT, (WPARAM) ::GetBoaldGUIFont (), (LPARAM) 0);
    SendCtrlMessage (IDC_URP_DISP_FIELDSIZE, WM_SETFONT, (WPARAM) ::GetBoaldGUIFont (), (LPARAM) 0);

    SendCtrlMessage (IDC_URP_DISP_UA	   , WM_SETFONT, (WPARAM) ::GetBoaldGUIFont (), (LPARAM) 0);
    SendCtrlMessage (IDC_URP_DISP_IA	   , WM_SETFONT, (WPARAM) ::GetBoaldGUIFont (), (LPARAM) 0);

    if (s_pIRRPService)
    {
    if (ERROR_SUCCESS == s_pIRRPService ->CreateRRPDevice 
				((void **) & m_pIRRPUrp	    ,
				  IID_IUnknown		    ,
				 (void  *) 0x80000000	    ,
				  IID_IUnknown		    ))
    {
	m_sIRRPCallBack.SetRecvHWND (GetHWND ());

	m_pIRRPUrp ->Connect (m_hIRRPCallBack, 
			    & m_sIRRPCallBack);
    }
    }


//  SendCtrlMessage	(IDC_URP_SETUANODE_EDT, WM_SETFONT, (WPARAM) ::GetBoaldGUIFont (), (LPARAM) 0);

	return True ;
};

Bool CUrpPanel::On_WM_MouseMove (GWinMsg & m)
{
    GRect   wr	;
    const CIndicatorBar::Descriptor * 
	    pDcr;

    m_IndBlock.GetWindowRect (& wr);

    if (WM_MOUSEMOVE == m.uMsg)
    {
    if (wr.Contains (GET_X_LPARAM (m.lParam), GET_Y_LPARAM (m.lParam)))
    {
	pDcr = m_IndBlock.GetOverDescriptor 
				      (GET_X_LPARAM (m.lParam) - wr.x,
				       GET_Y_LPARAM (m.lParam) - wr.y);
	if (m_pCurDcrShow != pDcr)
	{
	    m_pCurDcrShow  = pDcr;

	    ::InvalidateRect (GetCtrlHWND (IDC_URP_STATUS_STC), NULL, True);
	}
//<<<
{{
    TRACKMOUSEEVENT    tme = {sizeof (TRACKMOUSEEVENT), TME_CANCEL | TME_LEAVE, GetHWND (), 0};

    TrackMouseEvent (& tme);
    tme.dwFlags &=  (~TME_CANCEL);
    TrackMouseEvent (& tme);
}}
//>>>
	return m.DispatchComplete ();
    }
    }
	if (m_pCurDcrShow)
	{
	    m_pCurDcrShow = NULL;

	    ::InvalidateRect (GetCtrlHWND (IDC_URP_STATUS_STC), NULL, True);
	}

	return False;
};


Bool CUrpPanel::On_EdtUAnode (GWinMsg & m)
{
    return False;
};

Bool CUrpPanel::On_RadFocus (GWinMsg & m, int iIndex)
{
    m_bBigFocus = (0 == iIndex) ? False : True;

    RRPSetParam	(m_pIRRPUrp, IRRPUrp::ID_Focus, 
   (m_bBigFocus)		? 0x01  : 0x00);

    return True;
};

Bool CUrpPanel::On_CtrlMode ()
{
    if (IsCtrlChecked (IDC_URP_IANODECTRL_CHK))
    {
	SendCtrlMessage (IDC_URP_SETIFILAMENT_EDT, EM_SETREADONLY, (WPARAM) 
					 /*(m_bIAnodeRetention) ? False :*/ True , (LPARAM) 0);
	SendCtrlMessage (IDC_URP_SETIANODE_EDT	 , EM_SETREADONLY, (WPARAM) False, (LPARAM) 0);

	RRPSetParam	(m_pIRRPUrp, IRRPUrp::ID_ParSetIAnode, GetCtrlText (IDC_URP_SETIANODE_EDT));
    }
    else
    {
	SendCtrlMessage (IDC_URP_SETIFILAMENT_EDT, EM_SETREADONLY, (WPARAM) False, (LPARAM) 0);
	SendCtrlMessage (IDC_URP_SETIANODE_EDT	 , EM_SETREADONLY, (WPARAM) True , (LPARAM) 0);
    }

    return True;
};


Bool CUrpPanel::On_Edt (GWinMsg & m)
{
    union
    {
	float	fValue	 ;
	int	nValue	 ;
    };
    int		nCtrlId  ;
    TCHAR	sBuf [64];

    GetCtrlText (nCtrlId = LOWORD (m.wParam), sBuf, sizeof (sBuf) / sizeof (TCHAR));

    switch (nCtrlId)
    {
    case IDC_URP_SETUANODE_EDT	     :
#if True
	if (sscanf (sBuf, "%f", & fValue) > 0)
	{
	    nValue  = (int)(fValue * 10 + 0.5);
	}
	else
	{
	    nValue  =  0;
	    sscanf (sBuf, "%d",    & nValue);
	    nValue *= 10;
	}
	RRPSetParam (m_pIRRPUrp, IRRPUrp::ID_ParSetUAnode10, nValue);
#else

	sscanf (sBuf, "%d", & nValue );
	RRPSetParam (m_pIRRPUrp, IRRPUrp::ID_ParSetUAnode, nValue);
#endif
	return True;

    case IDC_URP_SETIFILAMENTPRE_EDT :
	sscanf (sBuf, "%d", & nValue);
	RRPSetParam (m_pIRRPUrp, IRRPUrp::ID_IFilamentPre, nValue);
	return True;

    case IDC_URP_SETIFILAMENT_EDT    :
	sscanf (sBuf, "%d", & nValue);
	RRPSetParam (m_pIRRPUrp, IRRPUrp::ID_IFilament	 , nValue);
	return True;

    case IDC_URP_SETIANODE_EDT :
	sscanf (sBuf, "%d", & nValue);
	RRPSetParam (m_pIRRPUrp, IRRPUrp::ID_ParSetIAnode, nValue);
	return True;

    case IDC_URP_SETMAS_EDT	     :
	sscanf (sBuf, "%d", & nValue);
	RRPSetParam (m_pIRRPUrp, IRRPUrp::ID_MAS	 , nValue);
	return True;
	
    case IDC_URP_SETEXPOSETIME_EDT   :
	sscanf (sBuf, "%d", & nValue);
	RRPSetParam (m_pIRRPUrp, IRRPUrp::ID_ExposeTime	 , nValue);
	return True;

    case IDC_URP_SETHVDELAY_EDT	     :
	sscanf (sBuf, "%d", & nValue);
	RRPSetParam (m_pIRRPUrp, IRRPUrp::ID_HVDelay	 , nValue);
	return True;

    };
	return False;
};

Bool CUrpPanel::PreprocMessage (MSG * msg)
{
    m_hEdit.PreprocMessage (msg);

    return GDialog::PreprocMessage (msg);
};

Bool CUrpPanel::On_DrawStatus (DRAWITEMSTRUCT * pdi)
{
    RECT  rc  = pdi ->rcItem;
    rc.right -= 8;

    ::FillRect (pdi ->hDC, & pdi ->rcItem, ::GetSysColorBrush (COLOR_3DFACE));

    ::SetBkColor    (pdi ->hDC, ::GetSysColor (COLOR_3DFACE));
    ::SetTextColor  (pdi ->hDC, (  ( m_pCurDcrShow	    && (m_pCurDcrShow == m_pCurDcrShow2))
				|| ((m_pCurDcrShow == NULL) &&			 m_pCurDcrShow2))
			      ? RGB (128 + 16, 0, 0)
			      : RGB (	    0, 0, 0));
    ::SetBkMode	    (pdi ->hDC, TRANSPARENT);
    ::DrawText	    (pdi ->hDC, (m_pCurDcrShow ) ? m_pCurDcrShow  ->pName
					         :
			       ((m_pCurDcrShow2) ? m_pCurDcrShow2 ->pName : ""),
		    -1,
		  & rc, DT_RIGHT | DT_SINGLELINE | DT_VCENTER);

    return True;
};

Bool CUrpPanel::On_DlgMsg (GWinMsg & m)
{
    if (m_hIRRPCallBack && (CWndCallBack::RRPMSG == m.uMsg ))
    {
	On_IRRPCallBack	  ((IRRPCallBack::Handle *) m.lParam);

	return m.DispatchComplete ();
    }

    if ((WM_MOUSEMOVE  == m.uMsg)
    ||  (WM_MOUSELEAVE == m.uMsg))
    {{
	On_WM_MouseMove (m);
    }}

    int		nCtrlId;

    if (m_hEdit.On_ReflectWinMsg (m))
    {
	return  On_Edt (m);
    }

    if (0 !=   (nCtrlId = IsCommand (m)))
    {
	switch (nCtrlId)
	{
	case IDC_URP_SETFOCUS0_RAD	: return On_RadFocus  (m, 0);
	case IDC_URP_SETFOCUS1_RAD	: return On_RadFocus  (m, 1);
	case IDC_URP_IANODECTRL_CHK	: return On_CtrlMode  ();

	case IDC_URP_SHOT_PREPARE_BTN	: return On_Shot      (IRRPUrp::HVPrepare);
	case IDC_URP_SHOT_ON_BTN	: return On_Shot      (IRRPUrp::HVOn	 );
	case IDC_URP_SHOT_RELEASE_BTN	: return On_Shot      (IRRPUrp::HVRelease);

	case IDC_URP_SHOT_BTN		: return On_ShotCycle ();

	case IDC_URP_SHOT_BTN2		: return On_ShotCount ();
	};
    }
    
    if (0 !=   (nCtrlId = IsControlNotify (m)))
    {
	if (WM_DRAWITEM == m.uMsg)
	{
	switch (nCtrlId)
	{
	case IDC_URP_ONLINE_LED :
	    return (m_oLedUrpOnline.On_Draw (m.pDRAWITEMSTRUCT), True);
	case IDC_KKM_ONLINE_LED :
	    return (m_oLedKKMOnline.On_Draw (m.pDRAWITEMSTRUCT), True);
	case IDC_URP_READY_LED  :
	    return (m_oLedShotReady  .On_Draw (m.pDRAWITEMSTRUCT), True);
	case IDC_URP_SHOT_PREPARE_LED :
	    return (m_oLedShotPrepare.On_Draw (m.pDRAWITEMSTRUCT), True);
	case IDC_URP_SHOT_ON_LED :
	    return (m_oLedShotOn     .On_Draw (m.pDRAWITEMSTRUCT), True);
	case IDC_URP_SHOT_RELEASE_LED :
	    return (m_oLedShotRelease.On_Draw (m.pDRAWITEMSTRUCT), True);
	case IDC_URP_HV_LED :
	    return (m_oLedHV	     .On_Draw (m.pDRAWITEMSTRUCT), True);
	case IDC_URP_STATUS_STC  :
	    return  On_DrawStatus (m.pDRAWITEMSTRUCT);
	};
	}

	if (WM_CTLCOLORSTATIC == m.uMsg)
	{
	switch (nCtrlId)
	{
	case IDC_URP_DISP_HEATFORCETOUT :
	    ::SetTextColor (m.wHDC, (m_bHeatForce) ? RGB (128 + 16,0,0) : ::GetSysColor (COLOR_BTNTEXT));
	    ::SetBkColor   (m.wHDC, ::GetSysColor      (COLOR_3DFACE));
    	    return m.DispatchCompleteDlgEx ((LRESULT)
				    ::GetSysColorBrush (COLOR_3DFACE));

	case IDC_URP_DISP_HEATTUBETOUT :
	    ::SetTextColor (m.wHDC, (m_bHeatTube)  ? RGB (128 + 16,0,0) : ::GetSysColor (COLOR_BTNTEXT));
	    ::SetBkColor   (m.wHDC, ::GetSysColor      (COLOR_3DFACE));
	    return m.DispatchCompleteDlgEx ((LRESULT)
				    ::GetSysColorBrush (COLOR_3DFACE));

	case IDC_KKM_DISP_UOUT :
	    ::SetTextColor (m.wHDC, (m_bNoCharge)  ? RGB (128 + 16,0,0) : ::GetSysColor (COLOR_BTNTEXT));
	    ::SetBkColor   (m.wHDC, ::GetSysColor      (COLOR_3DFACE));
	    return m.DispatchCompleteDlgEx ((LRESULT)
				    ::GetSysColorBrush (COLOR_3DFACE));
	};
	}

/*
	if (WM_CTLCOLORSTATIC == m.uMsg)
	{
	switch (nCtrlId)
	{
	case IDC_URP_STATUS_STC :
	    SetTextColor ((HDC) m.wParam, COLOR_GRAYTEXT);	    
	    return m.DispatchCompleteDlgEx ((LRESULT) ::GetSysColorBrush (COLOR_ACTIVECAPTION));
	};
	}
*/

	switch (nCtrlId)
	{
	case IDC_URP_BLOCK_IND	    : return m_IndBlock .On_ReflectWinMsg (m);

	case IDC_URP_SETFIELDSIZE   :
	    if (CBN_SELCHANGE == HIWORD (m.wParam))
    	    {
		RRPSetParam (m_pIRRPUrp, IRRPUrp::ID_ParSetFieldSize,
			    (_U32) SendCtrlMessage (IDC_URP_SETFIELDSIZE, CB_GETCURSEL, (WPARAM) 0, (LPARAM) 0));

		return True;
	    }
	    break;
/*
//	case IDC_URP_DISP_SHOTTUBE  :
//	case IDC_URP_DISP_SHOTFORCE :
//	case IDC_KKM_DISP_UOUT	    :
	    if (WM_CTLCOLORSTATIC == m.uMsg)
	    {
		return m.DispatchCompleteDlgEx ((LRESULT) ::GetStockObject (BLACK_BRUSH));
	    }
	    break;
*/
	default :
	    break;
	}
    }

    return GDialog::On_DlgMsg (m);
};
//
//[]------------------------------------------------------------------------[]
//
Bool CUrpPanel::On_ShotCount ()
{
    CUrpShotCount	Dlg;

    if (! Dlg.Create (this,
		      NULL, MAKEINTRESOURCE (IDD_URP_SHOTCOUNT)))
    {	return False;}

    GRect rw, rc;

    this ->GetWindowRect (& rw);
    Dlg.GetWindowRect	 (& rc);

    Dlg.SetWindowPos (NULL, rw.x + ((rw.cx - rc.cx) / 2), 
			    rw.y + ((rw.cy - rc.cy) / 2), 0, 0, SWP_NOSIZE);

    Dlg.ExecWindow   ();

    if ((IDCANCEL == Dlg.m_uExitCode) && m_pIRRPUrp)
    {
	RRPSetParam (m_pIRRPUrp, IRRPUrp::ID_ParGetShotCount, 0);
    }

    Dlg.Destroy ();

    return True ;
};

Bool CUrpPanel::On_Shot (IRRPUrp::HVCommand HVCommand)
{
    if (m_pIRRPUrp)
    {
    switch (HVCommand)
    {
    case IRRPUrp::HVPrepare :
	EnableCtrl (IDC_URP_SHOT_BTN		, False);

	EnableCtrl (IDC_URP_SHOT_PREPARE_BTN	, False);
	EnableCtrl (IDC_URP_SHOT_ON_BTN		,  True);
//	EnableCtrl (IDC_URP_SHOT_RELEASE_BTN	,  True);
	break;

    case IRRPUrp::HVOn	    :
//	EnableCtrl (IDC_URP_SHOT_ON_BTN		, False);
	break;

    case IRRPUrp::HVRelease :
    if (m_pShotIrp)
    {
	Do_ShotCycleCancel ();
    }
    else
    {
	EnableCtrl (IDC_URP_SHOT_PREPARE_BTN	,  True);
	EnableCtrl (IDC_URP_SHOT_ON_BTN		, False);
//	EnableCtrl (IDC_URP_SHOT_RELEASE_BTN	, False);

	EnableCtrl (IDC_URP_SHOT_BTN		,  True);
    }
	break;
    };
	m_pIRRPUrp ->SetHV (HVCommand);
    }
	return True;
};
//
//[]------------------------------------------------------------------------[]
//
GIrp * CUrpPanel::Co_ShotCycle (GIrp * pIrp, void *)
{
    printf ("[%08X] ...CUrpPanel::Co_ShotCycle %08X...\n", TCurrent () ->GetId (), pIrp ->GetCurResult ());

    m_pShotIrp = NULL;

    return pIrp;
};

static GIrp * GAPI Co_RestoreCurApartment (GIrp * pIrp, void *, void * pArgument)
{
    GApartment *  a =
   (GApartment *) pArgument ;

    if (a != TCurrent () ->GetCurApartment ())
    {
	a ->IApartment_QueueIrpForComplete (pIrp, 0);

	return NULL;
    }
	return pIrp;
};

void CUrpPanel::Do_ShotCycleCancel ()
{
    if (m_pShotIrp)
    {
			      CancelIrp (m_pShotIrp);

	while (NULL != (volatile void *) m_pShotIrp)
	{
 	    if (TCurrent () ->GetCurApartment () ->PumpInputWork ())
	    {
		TCurrent () ->GetCurApartment () ->Work ();

		continue;
	    }
		TCurrent () ->GetCurApartment () ->WaitInputWork (20);
	}
    }
};

Bool CUrpPanel::On_ShotCycle ()
{
    if (m_pShotIrp == NULL)
    {{
	m_pShotIrp  = new GIrp;

	SetCurCoProc <CUrpPanel, void *>
       (m_pShotIrp, & CUrpPanel::Co_ShotCycle, this, NULL);

	SetCurCoProc
       (m_pShotIrp, Co_RestoreCurApartment, NULL, TCurrent () ->GetCurApartment ());

	m_pIRRPUrp ->RunHV (m_pShotIrp);
    }}
    else
    {{
	Do_ShotCycleCancel ();
    }}
	return True;
};
//
//[]------------------------------------------------------------------------[]
//
void CUrpPanel::On_IRRPParam (_U32 uParamId, _U32 uValue)
{
	TCHAR	      pBuf [64];

    if (0x80000000 == uParamId)
    {
	s_pMainPanel ->Send_WM (CWndCallBack::EQUMSG, (WPARAM) IRRPMDIG::EquMask_Urp, uValue & 0x0003);

	m_oLedUrpOnline.SetColor (GetCtrlHWND (IDC_URP_ONLINE_LED),
				 (uValue & IRRPUrp::IRRPDevice_DevOnline) ? CLedIndicator::Green : CLedIndicator::Red);

	if (uValue & IRRPUrp::IRRPDevice_DevOnline)
	{
	    m_oLedShotPrepare.TurnOn (GetCtrlHWND (IDC_URP_SHOT_PREPARE_LED), True);
	    m_oLedShotOn     .TurnOn (GetCtrlHWND (IDC_URP_SHOT_ON_LED     ), True);
	    m_oLedShotRelease.TurnOn (GetCtrlHWND (IDC_URP_SHOT_RELEASE_LED), True);
	}
	else
	{
	    m_pCurDcrShow2 = NULL;

	    m_oLedShotPrepare.TurnOn (GetCtrlHWND (IDC_URP_SHOT_PREPARE_LED), False);
	    m_oLedShotOn     .TurnOn (GetCtrlHWND (IDC_URP_SHOT_ON_LED     ), False);
	    m_oLedShotRelease.TurnOn (GetCtrlHWND (IDC_URP_SHOT_RELEASE_LED), False);

	    SetCtrlText	(IDC_URP_DISP_SETFOCUS, "");

	    ::CheckRadioButton (GetHWND (), IDC_URP_SETFOCUS0_RAD,
					    IDC_URP_SETFOCUS1_RAD, -1);
	}
    }
    if (IRRPUrp::ID_RegKKMStatus == uParamId)
    {
	s_pMainPanel ->Send_WM (CWndCallBack::EQUMSG, (WPARAM) IRRPMDIG::EquMask_KKM, uValue & 0x0003);

	m_oLedKKMOnline.SetColor (GetCtrlHWND (IDC_KKM_ONLINE_LED),
				 (uValue & IRRPKKM::IRRPDevice_DevOnline) ? CLedIndicator::Green : CLedIndicator::Red);
    }
    if (0x8000D1D0 == uParamId)
    {
	m_IndBlock.SetValue (uValue);

	m_pCurDcrShow2 = 
	m_IndBlock.GetHighDescriptor ();

	::InvalidateRect (GetCtrlHWND (IDC_URP_STATUS_STC), NULL, True);
    }
    if (0x8000D4D3 == uParamId)
    {
	m_oLedShotPrepare.SetColor (GetCtrlHWND (IDC_URP_SHOT_PREPARE_LED), (uValue & 0x0100) ? CLedIndicator::Yellow : CLedIndicator::Orange);
	m_oLedShotOn	 .SetColor (GetCtrlHWND (IDC_URP_SHOT_ON_LED     ), (uValue & 0x0200) ? CLedIndicator::Yellow : CLedIndicator::Orange);
	m_oLedShotRelease.SetColor (GetCtrlHWND (IDC_URP_SHOT_RELEASE_LED), (uValue & 0x0400) ? CLedIndicator::Yellow : CLedIndicator::Orange);

	return;
    }
    if (0x80000097 == uParamId)
    {
	m_bBigFocus = (uValue & 0x01) ? True : False;

	SetCtrlText	(IDC_URP_DISP_SETFOCUS, LoadString (m_bBigFocus ? IDS_FOCUS_LARGE : IDS_FOCUS_SMALL));

	CheckRadioButton (IDC_URP_SETFOCUS0_RAD,
			  IDC_URP_SETFOCUS1_RAD, IDC_URP_SETFOCUS0_RAD + ((m_bBigFocus) ? 1 : 0));
    }
    if (IRRPUrp::ID_RegGetHVReady == uParamId)
    {
	m_bNoCharge   = (uValue & IRRPUrp::HVReady_NotCharge) ? True : False;
	SendCtrlMessage (IDC_KKM_DISP_UOUT, WM_SETFONT, 
			(WPARAM)((m_bNoCharge) ? ::GetBoaldGUIFont () 
					       : ::GetStockObject  (DEFAULT_GUI_FONT)), (LPARAM) 0);

	m_oLedShotReady  .SetColor (GetCtrlHWND (IDC_URP_READY_LED       ), (0 == uValue) ? CLedIndicator::Green : CLedIndicator::Red);
    }
    if (IRRPUrp::ID_ParGetUAnode10 == uParamId)
    {
	wsprintf (pBuf, "%d.%d", (short) uValue / 10, (short) uValue % 10);
	SetCtrlText (IDC_URP_DISP_UA	    , pBuf);
    }
    if (0x8000E7E6 == uParamId)
    {
	wsprintf (pBuf, "%d", (short) uValue);
	SetCtrlText (IDC_URP_DISP_IA	    , pBuf);
    }
    if (0x8000E5E4 == uParamId)
    {
//	wsprintf (pBuf, "%d", (short) uValue);
//	SetCtrlText (IDC_URP_DISP_IA	    , pBuf);
    }
    if (0x8000EFEE == uParamId)
    {
	wsprintf (pBuf, "%d", (short) uValue);
	SetCtrlText (IDC_URP_DISP_IFPRE	    , pBuf);
    }
    if (0x8000F1F0 == uParamId)
    {
	wsprintf (pBuf, "%d", (short) uValue);
	SetCtrlText (IDC_URP_DISP_IF	    , pBuf);
    }
    if (0x8000EBEA == uParamId)
    {
	wsprintf (pBuf, "%d", (short) uValue);
	SetCtrlText (IDC_URP_DISP_MAS	    , pBuf);
    }
    if (0x8000E9E8 == uParamId)
    {
	wsprintf (pBuf, "%d", (short) uValue);
	SetCtrlText (IDC_URP_DISP_TEXP	    , pBuf);
    }
    if (0x800000FA == uParamId)
    {
	wsprintf (pBuf, "%d", (short) uValue);
	SetCtrlText (IDC_URP_DISP_TEMP	    , pBuf);
    }
    if (0x8000FB00 == uParamId)
    {
	wsprintf (pBuf, "%d", (short) uValue);
	SetCtrlText (IDC_URP_DISP_SA	    , pBuf);
    }
    if (IRRPUrp::ID_RegGetStatusHV == uParamId)
    {
	if (uValue & 0x01)
	{
	EnableCtrl (IDC_URP_SHOT_PREPARE_BTN, False);
//	EnableCtrl (IDC_URP_SHOT_RELEASE_BTN, True );
	EnableCtrl (IDC_URP_SHOT_BTN	    , False);
	}
	else
	{
	EnableCtrl (IDC_URP_SHOT_PREPARE_BTN, True );
//	EnableCtrl (IDC_URP_SHOT_RELEASE_BTN, False);
	EnableCtrl (IDC_URP_SHOT_BTN	    , True );
	}

	m_oLedHV.TurnOn (GetCtrlHWND (IDC_URP_HV_LED), (0x04 & uValue) ? True : False);
    }

    if (m_bKKMControl)
    {
	if (IRRPUrp::ID_RegGetU220 == uParamId)
	{
	    SetCtrlText (IDC_KKM_DISP_UINP, (short) uValue);
	}
	if (IRRPUrp::ID_RegGetUForce == uParamId)
	{
	    SetCtrlText (IDC_KKM_DISP_UOUT, (short) uValue);
	}
	if (IRRPUrp::ID_RegGetICharging == uParamId)
	{
	    SetCtrlText (IDC_KKM_DISP_IINP, (short) uValue);
	}
    }
    if (IRRPUrp::ID_RegGetTForceItems == uParamId)
    {
	    SetCtrlText (IDC_URP_DISP_TFORCE, (short) uValue);
    }

    if (IRRPUrp::ID_RegHeatTubeTimeout  == uParamId)
    {
	m_bHeatTube  = (0 < uValue) ? True : False;

	wsprintf (pBuf, "%d:%02d", (unsigned short) uValue / 60,
				   (unsigned short) uValue % 60);

        SendCtrlMessage (IDC_URP_DISP_HEATTUBETOUT, WM_SETFONT, 
	(WPARAM)(m_bHeatTube ? ::GetBoaldGUIFont () : ::GetStockObject (DEFAULT_GUI_FONT)), (LPARAM) 0);

	SetCtrlText	(IDC_URP_DISP_HEATTUBETOUT , pBuf);
    }
    if (IRRPUrp::ID_RegHeatForceTimeout == uParamId)
    {
	m_bHeatForce = (0 < uValue) ? True : False;

	wsprintf (pBuf, "%d:%02d", (unsigned short) uValue / 60,
				   (unsigned short) uValue % 60);

        SendCtrlMessage (IDC_URP_DISP_HEATFORCETOUT, WM_SETFONT, 
	(WPARAM)(m_bHeatForce ? ::GetBoaldGUIFont () : ::GetStockObject (DEFAULT_GUI_FONT)), (LPARAM) 0);

	SetCtrlText	(IDC_URP_DISP_HEATFORCETOUT, pBuf);
    }
//
//...
//
    if (IRRPUrp::ID_ParSetUAnode10 == uParamId)
    {
	sprintf  (pBuf, "%d.%d", (short) uValue / 10, (short) uValue % 10);
	SetCtrlText (IDC_URP_SETUANODE_EDT , pBuf);
    }

    if ((IRRPUrp::ID_IFilamentPreSF == uParamId) && (! m_bBigFocus))
    {
	wsprintf (pBuf, "%d", (short) uValue);
	SetCtrlText (IDC_URP_SETIFILAMENTPRE_EDT, pBuf);
    }
    if ((IRRPUrp::ID_IFilamentSF    == uParamId) && (! m_bBigFocus))
    {
	wsprintf (pBuf, "%d", (short) uValue);
	SetCtrlText (IDC_URP_SETIFILAMENT_EDT, pBuf);
    }
    
    if ((IRRPUrp::ID_IFilamentPreBF == uParamId) && (  m_bBigFocus))
    {
	wsprintf (pBuf, "%d", (short) uValue);
	SetCtrlText (IDC_URP_SETIFILAMENTPRE_EDT, pBuf);	
    }
    if ((IRRPUrp::ID_IFilamentBF    == uParamId) && (  m_bBigFocus))
    {
	wsprintf (pBuf, "%d", (short) uValue);
	SetCtrlText (IDC_URP_SETIFILAMENT_EDT, pBuf);
    }

    if (IRRPUrp::ID_ParSetIAnodeMode == uParamId)
    {
	m_bIAnodeRetention = (uValue & 0x01) ? True : False;

	On_CtrlMode ();
    }
    if (IRRPUrp::ID_ParSetIAnode == uParamId)
    {
	wsprintf (pBuf, "%d", (short) uValue);
	SetCtrlText (IDC_URP_SETIANODE_EDT, pBuf);
    }

    if (IRRPUrp::ID_MAS == uParamId)
    {
	wsprintf (pBuf, "%d", (short) uValue);
	SetCtrlText (IDC_URP_SETMAS_EDT, pBuf);
    }
    if (IRRPUrp::ID_ExposeTime == uParamId)
    {
	wsprintf (pBuf, "%d", (short) uValue);
	SetCtrlText (IDC_URP_SETEXPOSETIME_EDT, pBuf);
    }
    if (IRRPUrp::ID_HVDelay == uParamId)
    {
	wsprintf (pBuf, "%d", (short) uValue);
	SetCtrlText (IDC_URP_SETHVDELAY_EDT, pBuf);
    }

    if (IRRPUrp::ID_ParSetFieldSize == uParamId)
    {
	switch (uValue & 0x03)
	{
	default :
	case 0  : wsprintf (pBuf, LoadString (IDS_FIELD_FULL)); break;
	case 1  : wsprintf (pBuf, "21 x 30"); break;
	case 2  : wsprintf (pBuf, "18 x 24"); break;
	case 3  : wsprintf (pBuf, "12 x 12"); break;
	};

	SendCtrlMessage (IDC_URP_SETFIELDSIZE, CB_SETCURSEL, (WPARAM) uValue & 0x03, (LPARAM) 0);

	SetCtrlText (IDC_URP_DISP_FIELDSIZE, pBuf);
    }

    if (IRRPUrp::ID_ParGetShotCount    == uParamId)
    {
	m_cShotCount    = uValue;

	wsprintf (pBuf, "%d/%d", m_cShotCount, m_cShotCountBad);
	SetCtrlText (IDC_URP_SHOT_BTN2, pBuf);
    }
    if (IRRPUrp::ID_ParGetShotCountBad == uParamId)
    {
	m_cShotCountBad = uValue;

	wsprintf (pBuf, "%d/%d", m_cShotCount, m_cShotCountBad);
	SetCtrlText (IDC_URP_SHOT_BTN2, pBuf);
    }
};

void CUrpPanel::On_IRRPCallBack (IRRPCallBack::Handle * hIRRPCallBack)
{
    Word	    uLength	;
    Word	    uType	;
    IRRPProtocol::DataPack *
		    pDataPack	;
    IRRPProtocol::ParamPack::Entry * 
		    pEntry	;

    if (hIRRPCallBack == m_hIRRPCallBack)
    {
	pDataPack = (IRRPProtocol::DataPack *) m_sIRRPCallBack.PeekHeadPacket ();

    while (pDataPack)
    {
	uLength = getBigEndianW (pDataPack, offsetof (IRRPProtocol::DataPack, uLength));
	uType	= getBigEndianW (pDataPack, offsetof (IRRPProtocol::DataPack, uType  ));

	if (0  == uType)
	{
		pEntry = (IRRPProtocol::ParamPack::Entry *)(pDataPack + 1);

	    while ((Byte *) pEntry < ((Byte *) pDataPack + uLength))
	    {
		On_IRRPParam (getBigEndianD (pEntry, offsetof (IRRPProtocol::ParamPack::Entry, uParamId)),
			      getBigEndianD (pEntry, offsetof (IRRPProtocol::ParamPack::Entry, uValue  )));
		pEntry ++;
	    }
	}

	pDataPack = (IRRPProtocol::DataPack *) m_sIRRPCallBack.PeekNextPacket ();
    }
    }    
};
//
//[]------------------------------------------------------------------------[]






static Bool SaveIAnodeCalibration (Bool bFocus, const char * pBuf)
{
    TCHAR	pFileName [MAX_PATH] = {0};

    ::GetEnvironmentVariable (_T("XPROM_ROOT"), pFileName, sizeof (pFileName) / sizeof (TCHAR));

    ::wsprintf (pFileName + lstrlen (pFileName), _T("\\bin\\TubeCalib%s.ini"), (bFocus) ? "BF" : "SF");
    
    HANDLE	hFile;

    if (INVALID_HANDLE_VALUE == 
       (hFile = CreateFile      (pFileName		,
				 GENERIC_WRITE		,
				 0 /*FILE_SHARE_READ*/  ,
				 NULL			,
				 CREATE_ALWAYS		,
				 FILE_ATTRIBUTE_NORMAL  ,
				 NULL			)))
    {
	return False;
    }
	DWord  t    ;

	::WriteFile (hFile, pBuf, lstrlen (pBuf), & t, NULL);

	::CloseHandle (hFile);

	return (t == (DWord) ::lstrlen (pBuf)) ? True : False;
};

//[]------------------------------------------------------------------------[]
//
#define	UA_MIN_VALUE	20
#define UA_MAX_VALUE	35

#define IF_MIN_VALUE	280
#define IF_MAX_VALUE	480

class CCalTube : public GDialog
{
public :
		CCalTube ();

protected :

	void		On_SelectFocus		(int);

	void		On_Press		(int);

	Bool		On_Edt			(GWinMsg &);

	Bool		On_DlgMsg		(GWinMsg &);

	void		On_Destroy		();
	Bool		On_Create		(void *);
	
	void		On_IRRPParam		(_U32, _U32);
	void		On_IRRPCallBack		(IRRPCallBack::Handle *);

//.....................................
//

	GIrp *		Co_ShotCycle2		(GIrp *, void *);
	GIrp *		Co_ShotCycle		(GIrp *, void *);

	GIrp *		Co_CalStep		(GIrp *, void *);

	GIrp *		Co_CalLoop		(GIrp *, void *);
	GIrp *		Co_CalProc		(GIrp *, void *);

	Bool		On_Start		();

	void		Do_ShotCycleCancel	();
//
//.....................................

private :

	void		SetUAValue		(Bool bHi  , int);
	void		SetUAStep		(Bool bStep, int);

	void		SetIFValue		(Bool bHi  , int);
	void		SetIFStep		(Bool bStep, int);

protected :

	CEditHandler		m_hEdit		;

	int			m_nExposeTime	;
	int			m_nStatistic	;

	GRange			m_UARange	;
	GRange			m_IFRange	;

	enum CalState
	{
	    Ready    = 0,
	    Work	,
	    Paused	,
	    Completed	,

	}   			m_uCalState	;

	void		SetCalState		(CalState);

public :

	IRRPUrp *		m_pIRRPUrp	;

	CWndCallBack		m_sIRRPxCallBack;
	IRRPCallBack::Handle *  m_hIRRPUCallBack;
	_U32			m_uHVReady	;

	GIrp *			m_pShotIrp	;

	GIrp *			m_pStpIrp	;
	GIrp *			m_pCalIrp	;

	int			m_iUA_SF_Min	;
	int			m_iUA_SF_Max	;
	int			m_iIF_SF_Min	;
	int			m_iIF_SF_Max	;

	int			m_iUA_BF_Min	;
	int			m_iUA_BF_Max	;
	int			m_iIF_BF_Min	;
	int			m_iIF_BF_Max	;

	int			m_iCurUAPoint	;
	int			m_iCurIFPoint	;
	int			m_iCurStat	;
	int			m_nAccumulate	;

	TStreamBuf <16 * 1024>	m_sOutBuf	;
};
//
//[]------------------------------------------------------------------------[]
//
CCalTube::CCalTube () : GDialog (NULL)
{
    m_nExposeTime =   400;
    m_nStatistic  =   2  ;

    IRRPSettings * pIRRPSettings = NULL;
    s_pIRRPService ->CreateRRPSettings 
		  (pIRRPSettings);

    pIRRPSettings ->GetValue (IRRPSettings::ID_URP_SHOT_UAnodeMin    , & m_iUA_SF_Min, sizeof (int), NULL);
    pIRRPSettings ->GetValue (IRRPSettings::ID_URP_SF_UAnodeMax	     , & m_iUA_SF_Max, sizeof (int), NULL);
    pIRRPSettings ->GetValue (IRRPSettings::ID_URP_SF_IFilamentPreMax, & m_iIF_SF_Min, sizeof (int), NULL);
    pIRRPSettings ->GetValue (IRRPSettings::ID_URP_SF_IFilamentMax   , & m_iIF_SF_Max, sizeof (int), NULL);

    pIRRPSettings ->GetValue (IRRPSettings::ID_URP_SHOT_UAnodeMin    , & m_iUA_BF_Min, sizeof (int), NULL);
    pIRRPSettings ->GetValue (IRRPSettings::ID_URP_BF_UAnodeMax	     , & m_iUA_BF_Max, sizeof (int), NULL);
    pIRRPSettings ->GetValue (IRRPSettings::ID_URP_BF_IFilamentPreMax, & m_iIF_BF_Min, sizeof (int), NULL);
    pIRRPSettings ->GetValue (IRRPSettings::ID_URP_BF_IFilamentMax   , & m_iIF_BF_Max, sizeof (int), NULL);

    if (m_iUA_SF_Min >= m_iUA_SF_Max)
    {
	m_iUA_SF_Min = UA_MIN_VALUE;
	m_iUA_SF_Max = UA_MAX_VALUE;
    }
    if (m_iIF_SF_Min >= m_iIF_SF_Max)
    {
	m_iIF_SF_Min = IF_MIN_VALUE;
	m_iIF_SF_Max = IF_MAX_VALUE;
    }

    if (m_iUA_BF_Min >= m_iUA_BF_Max)
    {
	m_iUA_BF_Min = UA_MIN_VALUE;
	m_iUA_BF_Max = UA_MAX_VALUE;
    }
    if (m_iIF_BF_Min >= m_iIF_BF_Max)
    {
	m_iIF_BF_Min = IF_MIN_VALUE;
	m_iIF_BF_Max = IF_MAX_VALUE;
    }

    m_UARange.Init   (m_iUA_BF_Min, m_iUA_BF_Max, 1);
    m_UARange.SetNum (3);
   
    m_IFRange.Init   (m_iIF_BF_Min, m_iIF_BF_Max, 1);
    m_IFRange.SetNum (3);

    m_uCalState	  = Ready;

    m_pShotIrp	     =
    m_pStpIrp	     =
    m_pCalIrp	     = NULL;

    m_pIRRPUrp	     = NULL;
    m_hIRRPUCallBack = NULL;
    m_uHVReady	     = 0x03;
};

void CCalTube::On_SelectFocus (int bBigFocus)
{
    int	iUACurNum = m_UARange.GetNum ();
    int	iIFCurNum = m_IFRange.GetNum ();

    if (bBigFocus)
    {
	m_UARange.Init	 (m_iUA_BF_Min, m_iUA_BF_Max, 1);
	m_UARange.SetNum (iUACurNum);

	m_IFRange.Init	 (m_iIF_BF_Min, m_iIF_BF_Max, 1);
	m_IFRange.SetNum (iIFCurNum);
    }
    else
    {
	m_UARange.Init	 (m_iUA_SF_Min, m_iUA_SF_Max, 1);
	m_UARange.SetNum (iUACurNum);

	m_IFRange.Init	 (m_iIF_SF_Min, m_iIF_SF_Max, 1);
	m_IFRange.SetNum (iIFCurNum);
    }

	SetCtrlText (IDC_CALTUBE_UAN_EDT    , m_UARange.GetNum  ());
	SetCtrlText (IDC_CALTUBE_UASTEP_EDT , m_UARange.GetStep ());

	SetCtrlText (IDC_CALTUBE_UALO_EDT   , m_UARange.m_nLoValue);
	SetCtrlText (IDC_CALTUBE_UAHI_EDT   , m_UARange.m_nHiValue);

	SetCtrlText (IDC_CALTUBE_IFN_EDT    , m_IFRange.GetNum  ());
	SetCtrlText (IDC_CALTUBE_IFSTEP_EDT , m_IFRange.GetStep ());

	SetCtrlText (IDC_CALTUBE_IFLO_EDT   , m_IFRange.m_nLoValue);
	SetCtrlText (IDC_CALTUBE_IFHI_EDT   , m_IFRange.m_nHiValue);
};

Bool CCalTube::On_Create (void * pParam)
{
    if (! GDialog::On_Create (pParam))
    {	return False;}

    SetCtrlText (IDC_CALTUBE_EXPOSETIME_EDT, m_nExposeTime);
    SetCtrlText (IDC_CALTUBE_NSTATISTIC_EDT, m_nStatistic );

    CheckRadioButton (IDC_CALTUBE_SETFOCUS0_RAD,
		      IDC_CALTUBE_SETFOCUS1_RAD, IDC_CALTUBE_SETFOCUS1_RAD);
    On_SelectFocus (1);

    SetCalState (m_uCalState);

    return True ;
};

void CCalTube::On_Destroy ()
{};

void CCalTube::SetUAStep  (Bool bStep, int uValue)
{
    if (1 == bStep)
    {
	m_UARange.SetStep (uValue);
    }
    if (2 == bStep)
    {
	m_UARange.SetNum  (uValue);
    }
    if (3 == bStep)
    {
	m_UARange.SetStep (m_UARange.m_nStep);
    }

    SetCtrlText (IDC_CALTUBE_UAN_EDT   , m_UARange.GetNum  ());
    SetCtrlText (IDC_CALTUBE_UASTEP_EDT, m_UARange.GetStep ());
};

void CCalTube::SetUAValue (Bool bHi, int nValue)
{
    if (! bHi)
    {
	m_UARange.SetLoValue (nValue);
	SetCtrlText (IDC_CALTUBE_UALO_EDT, m_UARange.m_nLoValue);
    }
    else
    {
	m_UARange.SetHiValue (nValue);
	SetCtrlText (IDC_CALTUBE_UAHI_EDT, m_UARange.m_nHiValue);
    }
	SetUAStep (3, 0);
};

void CCalTube::SetIFStep  (Bool bStep, int uValue)
{
    if (1 == bStep)
    {
	m_IFRange.SetStep (uValue);
    }
    if (2 == bStep)
    {
	m_IFRange.SetNum  (uValue);
    }
    if (3 == bStep)
    {
	m_IFRange.SetStep (m_IFRange.m_nStep);
    }

    SetCtrlText (IDC_CALTUBE_IFN_EDT   , m_IFRange.GetNum  ());
    SetCtrlText (IDC_CALTUBE_IFSTEP_EDT, m_IFRange.GetStep ());
};

void CCalTube::SetIFValue (Bool bHi, int nValue)
{
    if (! bHi)
    {
	m_IFRange.SetLoValue (nValue);
	SetCtrlText (IDC_CALTUBE_IFLO_EDT, m_IFRange.m_nLoValue);
    }
    else
    {
	m_IFRange.SetHiValue (nValue);
	SetCtrlText (IDC_CALTUBE_IFHI_EDT, m_IFRange.m_nHiValue);
    }
	SetIFStep (3, 0);
};

Bool CCalTube::On_Edt (GWinMsg & m)
{
    int		nValue	 ;
    int		nCtrlId  ;
    TCHAR	sBuf [64];

    GetCtrlText (nCtrlId = LOWORD (m.wParam), sBuf, sizeof (sBuf) / sizeof (TCHAR));

    switch (nCtrlId)
    {
    case IDC_CALTUBE_EXPOSETIME_EDT :
	sscanf (sBuf, "%d", & m_nExposeTime);
	return True;
    case IDC_CALTUBE_NSTATISTIC_EDT :
	sscanf (sBuf, "%d", & m_nStatistic);
	return True;

    case IDC_CALTUBE_UASTEP_EDT :
	sscanf (sBuf, "%d", & nValue);
	SetUAStep (1, nValue);
	return True;
    case IDC_CALTUBE_UAN_EDT :
	sscanf (sBuf, "%d", & nValue);
	SetUAStep (2, nValue);
	return True;
    case IDC_CALTUBE_UALO_EDT :
	sscanf (sBuf, "%d", & nValue);
	SetUAValue (0, nValue);
	return True;
    case IDC_CALTUBE_UAHI_EDT :
	sscanf (sBuf, "%d", & nValue);
	SetUAValue (1, nValue);
	return True;

    case IDC_CALTUBE_IFSTEP_EDT :
	sscanf (sBuf, "%d", & nValue);
	SetIFStep (1, nValue);
	return True;
    case IDC_CALTUBE_IFN_EDT :
	sscanf (sBuf, "%d", & nValue);
	SetIFStep (2, nValue);
	return True;
    case IDC_CALTUBE_IFLO_EDT :
	sscanf (sBuf, "%d", & nValue);
	SetIFValue (0, nValue);
	return True;
    case IDC_CALTUBE_IFHI_EDT :
	sscanf (sBuf, "%d", & nValue);
	SetIFValue (1, nValue);
	return True;
    };
	return False;
};

void CCalTube::SetCalState (CCalTube::CalState State)
{
    if ((Work == m_uCalState)
    &&  (Work !=       State))
    {
	SetCtrlText (IDC_CALTUBE_START_BTN, LoadString (IDS_CALIB_START));
	SetCtrlText (IDC_CALTUBE_SAVE_BTN , LoadString (IDS_CALIB_SAVE));
//	EnableCtrl  (IDC_CALTUBE_CLOSE_BTN, True);
    }

    switch (m_uCalState = State)
    {
    case Ready :
	SendCtrlMessage (IDC_CALTUBE_UASTEP_BAR, PBM_SETPOS, (WPARAM) (m_iCurUAPoint = 0), (LPARAM) 0);
	SendCtrlMessage (IDC_CALTUBE_IFSTEP_BAR, PBM_SETPOS, (WPARAM) (m_iCurIFPoint = 0), (LPARAM) 0);

	SetCtrlText (IDC_CALTUBE_START_BTN, LoadString (IDS_CALIB_START));
	SetCtrlText (IDC_CALTUBE_SAVE_BTN , LoadString (IDS_CALIB_RESET));
	EnableCtrl  (IDC_CALTUBE_SAVE_BTN , False);

    PrintfCtrlText (IDC_CALTUBE_DISP_EDT, "");

	break;

    case Work :
	SetCtrlText (IDC_CALTUBE_START_BTN, LoadString (IDS_CALIB_SUSPEND));
	EnableCtrl  (IDC_CALTUBE_SAVE_BTN , False);
//	EnableCtrl  (IDC_CALTUBE_CLOSE_BTN, False);
	On_Start    (/*True */);
	break;

    case Paused :
	SetCtrlText (IDC_CALTUBE_START_BTN, LoadString (IDS_CALIB_RESUME));
	SetCtrlText (IDC_CALTUBE_SAVE_BTN , LoadString (IDS_CALIB_RESET));
	EnableCtrl  (IDC_CALTUBE_SAVE_BTN , True );
	On_Start    (/*False */);
	break;

    case Completed :
	SetCtrlText (IDC_CALTUBE_START_BTN, LoadString (IDS_CALIB_START));
	SetCtrlText (IDC_CALTUBE_SAVE_BTN , LoadString (IDS_CALIB_SAVE));
	EnableCtrl  (IDC_CALTUBE_SAVE_BTN , True );

    PrintfCtrlText (IDC_CALTUBE_DISP_EDT, "");

	break;
    };


    if ((Ready == m_uCalState) || (Completed == m_uCalState))
    {
	EnableCtrl (IDC_CALTUBE_CLOSE_BTN     , True );

	EnableCtrl (IDC_CALTUBE_SETFOCUS0_RAD , True );
	EnableCtrl (IDC_CALTUBE_SETFOCUS1_RAD , True );
	EnableCtrl (IDC_CALTUBE_EXPOSETIME_EDT, True );
	EnableCtrl (IDC_CALTUBE_NSTATISTIC_EDT, True );

	EnableCtrl (IDC_CALTUBE_UALO_EDT      , True );
	EnableCtrl (IDC_CALTUBE_UASTEP_EDT    , True );
	EnableCtrl (IDC_CALTUBE_UAN_EDT       , True );
	EnableCtrl (IDC_CALTUBE_UAHI_EDT      , True );

	EnableCtrl (IDC_CALTUBE_IFLO_EDT      , True );
	EnableCtrl (IDC_CALTUBE_IFSTEP_EDT    , True );
	EnableCtrl (IDC_CALTUBE_IFN_EDT       , True );
	EnableCtrl (IDC_CALTUBE_IFHI_EDT      , True );
    }
    else
    {
	EnableCtrl (IDC_CALTUBE_SETFOCUS0_RAD , False);
	EnableCtrl (IDC_CALTUBE_SETFOCUS1_RAD , False);
	EnableCtrl (IDC_CALTUBE_EXPOSETIME_EDT, False);
	EnableCtrl (IDC_CALTUBE_NSTATISTIC_EDT, False);

	EnableCtrl (IDC_CALTUBE_UALO_EDT      , False);
	EnableCtrl (IDC_CALTUBE_UASTEP_EDT    , False);
	EnableCtrl (IDC_CALTUBE_UAN_EDT       , False);
	EnableCtrl (IDC_CALTUBE_UAHI_EDT      , False);

	EnableCtrl (IDC_CALTUBE_IFLO_EDT      , False);
	EnableCtrl (IDC_CALTUBE_IFSTEP_EDT    , False);
	EnableCtrl (IDC_CALTUBE_IFN_EDT       , False);
	EnableCtrl (IDC_CALTUBE_IFHI_EDT      , False);

	EnableCtrl (IDC_CALTUBE_CLOSE_BTN     , False);
    }
};

void CCalTube::On_Press (int nCtrlId)
{
    if (IDC_CALTUBE_START_BTN == nCtrlId)
    {
	switch (m_uCalState)
	{
	case Ready  :
	case Paused :
	    SetCalState (Work  );
	    break;

	case Work  :
	    SetCalState (Paused);
	    break;

	case Completed :
	    SetCalState (Ready );
	    SetCalState (Work  );
	    break;
	};
    }
    else
    if (IDC_CALTUBE_SAVE_BTN  == nCtrlId)
    {
	switch (m_uCalState)
	{
	case Paused :
	if (m_pStpIrp != NULL)
	{
	    m_pStpIrp  ->SetResultInfo (ERROR_CANCELLED, NULL);
	    m_pStpIrp  = NULL;

	    CompleteIrp (m_pCalIrp);
	    break;
	}
					// Not entry to this point;
//	    SetCalState (Ready);
	    break;

	case Completed :
	{{
	    IRRPSettings * pIRRPSettings = NULL;

	    s_pIRRPService ->CreateRRPSettings 
			 (pIRRPSettings);

	    if (pIRRPSettings)
	    {
		pIRRPSettings ->SetValue (IsCtrlChecked (IDC_CALTUBE_SETFOCUS1_RAD)
					? IRRPSettings::ID_URP_BF_IAnodeTable
					: IRRPSettings::ID_URP_SF_IAnodeTable,
					  m_sOutBuf.head (), m_sOutBuf.sizeforget ());
		pIRRPSettings ->Checkin  (IRRPSettings::CLSID_URP);

printf ("\n\t\t...Save calibrate result...\n");
	    }
	}}
	    SetCalState (Ready);
	    break;
	};
    }
};

Bool CCalTube::On_DlgMsg (GWinMsg & m)
{
    if (CWndCallBack::RRPMSG == m.uMsg)
    {
	On_IRRPCallBack ((IRRPCallBack::Handle *) m.lParam);

	return m.DispatchComplete ();
    }

    if (m_hEdit.On_ReflectWinMsg (m))
    {
	return On_Edt (m);
    }

    int		nCtrlId;

    if (0 !=   (nCtrlId = IsCommand (m)))
    {
	switch (nCtrlId)
	{
	case IDC_CALTUBE_SETFOCUS0_RAD :
	case IDC_CALTUBE_SETFOCUS1_RAD :
	    return (On_SelectFocus (IsCtrlChecked (IDC_CALTUBE_SETFOCUS1_RAD) ? True : False), True);

	case IDC_CALTUBE_START_BTN :
	case IDC_CALTUBE_SAVE_BTN  :
	    return (On_Press (nCtrlId), True);

	case IDC_CALTUBE_CLOSE_BTN :
	    ::SetWindowLong (GetHWND (), GWL_USERDATA, TRUE);
	    return (TCurrent () ->GetCurApartment () ->BreakFromWorkLoop (), True);
	};
    }

    return GDialog::On_DlgMsg (m);
};
//
//[]------------------------------------------------------------------------[]
//
GIrp * CCalTube::Co_ShotCycle2 (GIrp * pIrp, void *)
{
    if (pIrp ->Cancelled () || (ERROR_SUCCESS == pIrp ->GetCurResult ()))
    {
	return pIrp;
    }
    PrintfCtrlText (IDC_CALTUBE_DISP_EDT, LoadString (IDS_CALURP_ERR_CYCLE2));

	QueueIrpForComplete (pIrp, 10000);	// Delay 10 c (Look at Error);
	return NULL;
};

GIrp * CCalTube::Co_ShotCycle (GIrp * pIrp, void *)
{
    m_pShotIrp = NULL;

    return pIrp;
};


GIrp * CCalTube::Co_CalStep (GIrp * pIrp, void *)
{
    if (pIrp ->Cancelled ())
    {
	pIrp ->pCancelProcLock.Exchange (NULL);

	return pIrp;
    }
//  if (FALSE)
    if (ERROR_SUCCESS != pIrp ->GetCurResult ())
    {
	printf ("...Co CalStep Fail...\n");
    }
    else
    {{
	printf ("\t\t\t...Co CalStep (%2d.%2d.%1d) - %3d:%3d {Ua = %3d mA}\n", 
								 m_iCurUAPoint ,
								 m_iCurIFPoint ,
								 m_iCurStat    ,
					     m_UARange.GetValue (m_iCurUAPoint),
					     m_IFRange.GetValue (m_iCurIFPoint),
					     (int)  pIrp ->GetCurResultInfo ());

//  Process Step result in this place !!!
//
//
//  .....................................

	   m_nAccumulate += (int) pIrp ->GetCurResultInfo ();

    if (++ m_iCurStat	 >= m_nStatistic       )
    {
	   m_sOutBuf.putD (m_UARange.GetValue (m_iCurUAPoint));
	   m_sOutBuf.putD (m_IFRange.GetValue (m_iCurIFPoint));
	   m_sOutBuf.putD (m_nAccumulate / m_nStatistic	     );

	   m_iCurStat	  = 0;
	   m_nAccumulate  = 0;

    if (++ m_iCurIFPoint >= m_IFRange.GetNum ())
    {
    if (++ m_iCurUAPoint  < m_UARange.GetNum ())
    {
	   m_iCurIFPoint = 0;
    }
    SendCtrlMessage (IDC_CALTUBE_UASTEP_BAR, PBM_SETPOS, (WPARAM) m_iCurUAPoint, (LPARAM) 0);
    }

    SendCtrlMessage (IDC_CALTUBE_IFSTEP_BAR, PBM_SETPOS, (WPARAM) m_iCurIFPoint, (LPARAM) 0);
    }
    }}

	QueueIrpForComplete (pIrp, 0/*5000*/);

	return NULL;
};

GIrp * CCalTube::Co_CalLoop (GIrp * pIrp, void *)
{
    if (   m_iCurUAPoint >= m_UARange.GetNum ())
    {
	return pIrp;
    }
    if (   m_uCalState   != Work)
    {
	return (m_pStpIrp = pIrp, NULL);
    }

    pIrp ->SetCurResultInfo (ERROR_SUCCESS, NULL);

    SetCurCoProc <CCalTube, void *>
   (pIrp, & CCalTube::Co_CalLoop, this, NULL);

    if (m_uHVReady)
    {
    PrintfCtrlText (IDC_CALTUBE_DISP_EDT, LoadString (IDS_CALURP_MSG_WAITRDY));

	printf ("\t\t\t...Wait Urp %04X\n", m_uHVReady);
	QueueIrpForComplete (pIrp, 1000);

	return NULL;
    }

    SetCurCoProc <CCalTube, void *>
   (pIrp, & CCalTube::Co_CalStep, this, NULL);

//  Build Shot request in this place !!!
//
    printf ("\t\t\t...Do CalStep (%2d.%2d.%1d) - %3d:%3d\n", m_iCurUAPoint ,
							     m_iCurIFPoint ,
							     m_iCurStat    ,
					 m_UARange.GetValue (m_iCurUAPoint),
					 m_IFRange.GetValue (m_iCurIFPoint));

    PrintfCtrlText (IDC_CALTUBE_DISP_EDT, LoadString (IDS_CALURP_MSG_POINT),
							     m_iCurUAPoint ,
							     m_iCurIFPoint ,
							     m_iCurStat    ,
					 m_UARange.GetValue (m_iCurUAPoint),
					 m_IFRange.GetValue (m_iCurIFPoint)); 


    RRPSetParam (m_pIRRPUrp, IRRPUrp::ID_ParSetFieldSize,  0);
    RRPSetParam (m_pIRRPUrp, IRRPUrp::ID_Focus	      ,	IsCtrlChecked (IDC_CALTUBE_SETFOCUS1_RAD) ? 1 : 0);
    RRPSetParam (m_pIRRPUrp, IRRPUrp::ID_RegSetUAnode , m_UARange.GetValue (m_iCurUAPoint));
    RRPSetParam (m_pIRRPUrp, IRRPUrp::ID_IFilamentPre ,   -1);
    RRPSetParam (m_pIRRPUrp, IRRPUrp::ID_IFilament    , m_IFRange.GetValue (m_iCurIFPoint));
    RRPSetParam (m_pIRRPUrp, IRRPUrp::ID_MAS	      , 1500);
    RRPSetParam (m_pIRRPUrp, IRRPUrp::ID_ExposeTime   , m_nExposeTime);

    m_pShotIrp = pIrp;

    SetCurCoProc <CCalTube, void *>
   (m_pShotIrp, & CCalTube::Co_ShotCycle , this, NULL);

    SetCurCoProc <CCalTube, void *>
   (m_pShotIrp, & CCalTube::Co_ShotCycle2, this, NULL);

#if (CALTUBE_RELEASE)

    SetCurCoProc
   (m_pShotIrp, Co_RestoreCurApartment, NULL, TCurrent () ->GetCurApartment ());

    m_pIRRPUrp ->RunHV  (pIrp);

#else

    QueueIrpForComplete (pIrp, 200);

#endif
//
//  ....................................

    return NULL;
};

GIrp * CCalTube::Co_CalProc (GIrp * pIrp, void *)
{
    RRPSetParam (m_pIRRPUrp, IRRPUrp::ID_ParSetMode, 0x00010000);

    printf ("[%08X]...CCalTube::Co_CalProc %08X...\n", TCurrent () ->GetId (), pIrp ->GetCurResult ());

    m_pCalIrp = NULL;

    if (ERROR_SUCCESS == pIrp ->GetCurResult ())
    {{
	int	n, i, nUCur, nU, nIF, nIA;

		n = m_sOutBuf.sizeforget () / sizeof (int) / 3;

// Not add extra values !!!
//
//		m_sOutBuf.printfA ("\r\n\r\n\t\t<%2d, <%3d,%3d>", 0, 0, 0);
//		m_sOutBuf.printfA (",\r\n\t\t     <%3d,%3d>>,", 999, 0);
//
	for (nUCur = -1, i = 0; i < n; i ++)
	{
	    nU  = m_sOutBuf.getD ();
	    nIF = m_sOutBuf.getD ();
	    nIA = m_sOutBuf.getD ();

	    if (nUCur != nU)
	    {	
// Not add extra values !!!
//
//	    if (nUCur == -1)
//	    {
//		m_sOutBuf.printfA ("\r\n\r\n\t\t<%2d, <%3d,%3d>", (0 < nU) ? nU - 1 : 0, 0, 0);
//		m_sOutBuf.printfA (",\r\n\t\t     <%3d,%3d>", 999, 0);
//
//
//		m_sOutBuf.printfA ("\r\n\r\n\t\t<%2d, <%3d,%3d>"    , nUCur = nU, nIF, nIA);
//
//	    }
//  		m_sOutBuf.printfA (">,\r\n""\r\n\t\t<%2d, <%3d,%3d>", nUCur = nU, nIF, nIA);
//
		m_sOutBuf.printfA ((nUCur == -1) ? "\r\n\r\n\t\t<%2d, <%3d,%3d>"
						 : ">,\r\n""\r\n\t\t<%2d, <%3d,%3d>", nUCur = nU, nIF, nIA);
	    }
	    else
	    {	
		m_sOutBuf.printfA (",\r\n\t\t     <%3d,%3d>", nIF, nIA);
	    }
	}
		m_sOutBuf.sethead (0);

	if (0 < n)
	{	m_sOutBuf.printfA (">");}

	SetCalState (Completed);

	printf   ("%s", m_sOutBuf.head ());
    }}
    else
    {
	SetCalState (Ready    );
    }

    return pIrp;
};

void CCalTube::Do_ShotCycleCancel ()
{
    if (m_pShotIrp)
    {
			      CancelIrp (m_pShotIrp);

	while (NULL != (volatile void *) m_pShotIrp)
	{
 	    if (TCurrent () ->GetCurApartment () ->PumpInputWork ())
	    {
		TCurrent () ->GetCurApartment () ->Work ();

		continue;
	    }
		TCurrent () ->GetCurApartment () ->WaitInputWork (20);
	}
    }
};

Bool CCalTube::On_Start ()
{
    if (m_pCalIrp == NULL)		// Start process
    {					//
	m_pCalIrp     = new GIrp;

	m_iCurUAPoint = 
	m_iCurIFPoint = 
	m_iCurStat    = 0;

	m_nAccumulate = 0;

	m_sOutBuf.clear ();

	SendCtrlMessage (IDC_CALTUBE_UASTEP_BAR, PBM_SETRANGE, 0, MAKELPARAM (0, m_UARange.GetNum ()));
	SendCtrlMessage (IDC_CALTUBE_IFSTEP_BAR, PBM_SETRANGE, 0, MAKELPARAM (0, m_IFRange.GetNum ()));

	SetCurCoProc <CCalTube, void *>
       (m_pCalIrp, & CCalTube::Co_CalProc, this, NULL);

	RRPSetParam (m_pIRRPUrp, IRRPUrp::ID_ParSetMode, 0x00000001);

	SetCurCoProc <CCalTube, void *>
       (m_pCalIrp, & CCalTube::Co_CalLoop, this, NULL);

	CompleteIrp (m_pCalIrp);

	return True;
    }
    else
    if (m_pStpIrp != NULL)		// Resume process
    {
	SetCurCoProc <CCalTube, void *>
       (m_pCalIrp, & CCalTube::Co_CalLoop, this, NULL);

	m_pStpIrp  = NULL;

	CompleteIrp (m_pCalIrp);

	return True;
    }
    else				// Pause  process
    {					//
    PrintfCtrlText (IDC_CALTUBE_DISP_EDT, LoadString (IDS_CALURP_MSG_PAUSE));

	    Do_ShotCycleCancel ();

	while ((NULL != (volatile void *) m_pCalIrp)
	&&     (NULL == (volatile void *) m_pStpIrp))
	{
 	    if (TCurrent () ->GetCurApartment () ->PumpInputWork ())
	    {
		TCurrent () ->GetCurApartment () ->Work ();

		continue;
	    }
		TCurrent () ->GetCurApartment () ->WaitInputWork (20);
	}
    }
	return (m_pCalIrp) ? True : False;
};
//
//[]------------------------------------------------------------------------[]
//
void CCalTube::On_IRRPParam (_U32 uParamId, _U32 uValue)
{
    if (0x80000000 == uParamId)
    {
	s_pMainPanel ->Send_WM (CWndCallBack::EQUMSG, (WPARAM) IRRPMDIG::EquMask_Urp, uValue & 0x0003);

	m_uHVReady &= (~0x01);
	m_uHVReady |= (uValue & IRRPUrp::IRRPDevice_DevOnline) ? 0x00 : 0x01;
    }
    if (IRRPUrp::ID_RegGetHVReady == uParamId)
    {
	m_uHVReady &= (~0x02);
	m_uHVReady |= (uValue & IRRPUrp::HVReady_HeatTimeout)  ? 0x02 : 0x00;
	m_uHVReady |= (uValue & IRRPUrp::HVReady_NotCharge  )  ? 0x02 : 0x00;
    }
};

void CCalTube::On_IRRPCallBack (IRRPCallBack::Handle * hIRRPCallBack)
{
    Word	    uLength	;
    Word	    uType	;
    IRRPProtocol::DataPack *
		    pDataPack	;
    IRRPProtocol::ParamPack::Entry * 
		    pEntry	;

    if ((hIRRPCallBack == m_hIRRPUCallBack))
    {
	pDataPack = (IRRPProtocol::DataPack *) m_sIRRPxCallBack.PeekHeadPacket ();

    while (pDataPack)
    {
	uLength = getBigEndianW (pDataPack, offsetof (IRRPProtocol::DataPack, uLength));
	uType	= getBigEndianW (pDataPack, offsetof (IRRPProtocol::DataPack, uType  ));

	if (0  == uType)
	{
		pEntry = (IRRPProtocol::ParamPack::Entry *)(pDataPack + 1);

	    while ((Byte *) pEntry < ((Byte *) pDataPack + uLength))
	    {
		On_IRRPParam (getBigEndianD (pEntry, offsetof (IRRPProtocol::ParamPack::Entry, uParamId)),
			      getBigEndianD (pEntry, offsetof (IRRPProtocol::ParamPack::Entry, uValue  )));
		pEntry ++;
	    }
	}

	pDataPack = (IRRPProtocol::DataPack *) m_sIRRPxCallBack.PeekNextPacket ();
    }
    }
};
//
//[]------------------------------------------------------------------------[]
//
Bool On_URP_Calibrate (int x, int y, GWindow * pParent)
{
    CCalTube	Dlg;

    if (! Dlg.Create (pParent,
		      NULL, MAKEINTRESOURCE (IDD_URP_CALIBRATE)))
    {	return False;}

	Dlg.SetWindowPos (NULL, x, y, 0, 0, SWP_NOSIZE);

	AddPreprocHandler (& Dlg);

	if (ERROR_SUCCESS == s_pIRRPService ->CreateRRPDevice 
			    ((void **) & Dlg.m_pIRRPUrp	  ,
			      IID_IUnknown		  ,
			     (void  *) 0x80000000	  ,
			      IID_IUnknown		  ))
	{
	    Dlg.m_sIRRPxCallBack.SetRecvHWND (Dlg.GetHWND ());
	    
	    Dlg.m_pIRRPUrp ->Connect (Dlg.m_hIRRPUCallBack,
				    & Dlg.m_sIRRPxCallBack);

	    Dlg.ExecWindow ();

	    if (Dlg.m_hIRRPUCallBack)
	    {
		Dlg.m_hIRRPUCallBack ->Close ();
		Dlg.m_hIRRPUCallBack = NULL;
	    }

	    Dlg.m_pIRRPUrp ->Release ();
	    Dlg.m_pIRRPUrp = NULL;
	}

	RemPreprocHandler (& Dlg);

	Dlg.Destroy ();

	return True ;
};
//
//[]------------------------------------------------------------------------[]
