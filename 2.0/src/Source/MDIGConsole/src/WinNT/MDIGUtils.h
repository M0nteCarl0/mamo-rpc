//[]------------------------------------------------------------------------[]
// MDIGConsole util classes declarations
//
//
// Created by GSh.
//[]------------------------------------------------------------------------[]

#ifndef __MDIGUTILS_H
#define __MDIGUTILS_H

#include "G_guibase_new.h"

#include <commdlg.h>
#include <shellapi.h>

#include "MDIGConsole.rc.h"

#include "R_rrpdevs.h"

#define	DISPLAY_SHOW_SELS   0

    extern  HIMAGELIST  g_hLedImages;

HFONT	    GAPI	GetBoaldGUIFont ();

void	    GAPI	RRPSetParam	(IRRPDevice * pInterface, DWord dParamId    ,
								  DWord dParamValue );

Bool	    GAPI	PromptFileName  (HWND hParent, LPTSTR  pFileName,
						       DWord   nFileName,
						 const TCHAR * pFilter	);

void	    GAPI	ExecContrlEng	(HANDLE hFile);

//[]------------------------------------------------------------------------[]
//
class CWndCallBack : public IRRPCallBack
{
public :

static	UINT		    RRPMSG	;
static	UINT		    EQUMSG	;

public :
			CWndCallBack (size_t sz = 4096)
			{
			    m_hWnd    = NULL;

			if (RRPMSG == 0)
			{
			    RRPMSG  = (UINT) ::RegisterWindowMessage (_T("WM_IRRPCallBack"));
			}
			if (EQUMSG == 0)
			{
			    EQUMSG  = (UINT) ::RegisterWindowMessage (_T("WM_IRRPEquStatus"));
			}
			    m_pHigh =
			   (m_pBase =
			    m_pHead = 
			    m_pTail = (Byte *) malloc (sz)) + sz;
			};

		       ~CWndCallBack ()
			{
			    SetRecvHWND (NULL);
			    free   (m_pBase);
			};

	void		SetRecvHWND  (HWND hWnd)
			{
			    m_hWnd = hWnd;
			};

	void *		PeekHeadPacket	();

	void *		PeekNextPacket	();

protected :

    STDMETHOD  (     QueryInterface)(REFIID, void **);

    STDMETHOD_ (ULONG,	     AddRef)();

    STDMETHOD_ (ULONG,	    Release)();

    STDMETHOD_ (void,	     Invoke)(const void * hICallBack ,
				     const 
				     IRRPProtocol::DataPack *);
protected :

	GCriticalSection	m_Lock	;   // Lock for (*)
	HWND			m_hWnd	;   // (*)

	Byte *			m_pBase ;
	Byte *			m_pHead ;
	Byte *			m_pTail ;
	Byte *			m_pHigh ;
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
class GOwnerDrawHandler
{
protected :

virtual	void		On_Draw		    (DRAWITEMSTRUCT *) = NULL;

protected :
};

class CLedIndicator : public GOwnerDrawHandler
{
public :
		CLedIndicator ()
		{
		    m_bCurState = False  ;
		    m_uCurColor = Yellow ;
		    m_iCurIndex = 0	 ;
		};

	void		TurnOn		(HWND, Bool bOnState);

	typedef	enum Color
	{
	    Gray    =	0,
	    Green   =	1,
	    Yellow  =	2,
	    Orange  =	3,
	    Red	    =	4
	};

	void		SetColor	(HWND, Color);

	void		On_Draw		(DRAWITEMSTRUCT *);

protected :

	int		GetColorIndex	(Color c) const
			{
			    return ((m_bCurState) ? (int)(c) : 0);
			};

protected :

	Bool		m_bCurState  ;
	Color		m_uCurColor  ;

	int		m_iCurIndex  ;
};

class CIndicatorBar : public GButton
{
public :

	    struct Descriptor
	    {
		_U32	    uMask   ;
		LPCTSTR     pName   ;
	    };

	    CIndicatorBar (void * pWndId = NULL) :

	    GButton (pWndId)
	    {
		m_iSize.x     = 16;//::GetSystemMetrics (SM_CXMENUCHECK);
		m_iSize.y     = 16;//::GetSystemMetrics (SM_CYMENUCHECK);

		m_dValBitMask = 0;
		m_dAndBitMask = 0;
		m_pDescriptor = NULL;
	    };

	Bool		On_Create	    (void *);

	void		On_Draw		    (DRAWITEMSTRUCT *);

virtual	Bool		On_ReflectWinMsg    (GWinMsg &);

  const Descriptor *	GetOverDescriptor   (int, int);

  const Descriptor *	GetHighDescriptor   (DWord dAndMask = 0xFFFFFFFF) const;

	void		SetValue	    (DWord dValMask);

	void		SetBits		    (DWord dClrMask, DWord dSetMask);

protected :

	typedef	enum Color
	{
	    Gray    =	0,
	    Green   =	1,
	    Yellow  =	2,
	    Orange  =	3,
	    Red	    =	4
	};

  const Descriptor *	GetFirstItem	    (RECT &);

  const Descriptor *	GetNextItem	    (const Descriptor *, RECT &);

protected :

	GPoint		    m_iSize	  ;

	DWord		    m_dAndBitMask ;
	DWord		    m_dValBitMask ;
	const Descriptor *  m_pDescriptor ;
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
class CDisplay : public GButton
{
public :
		CDisplay ()
		{
		    m_dStateFlags = 0x0000;

		    m_oSell.Assign (0, 0, 0, 0);
		};

	void		InitSells		(int nSellX, int nSellY);

	Bool		On_ReflectWinMsg	(GWinMsg &);

protected :

	Bool		On_Create		(void * pParam);

private :

	void		DrawBkGnd		(HDC, RECT &);

	void		On_ReflectDrawItem	(DRAWITEMSTRUCT *);

protected :

	DWord		m_dStateFlags ;

	GRect		m_oSell	      ;
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
class CEditHandler
{
public :
		CEditHandler ()
		{
		    m_nCtrlId  =     0;
		    m_hCtrl    =  NULL;
		};

	Bool		PreprocMessage	    (MSG *);

	Bool		On_ReflectWinMsg    (GWinMsg &);

protected :

	int		m_nCtrlId   ;
	HWND		m_hCtrl	    ;
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
	struct GRange
	{
	    int		GetNum		() const;
	    int		GetStep		() const;

	    void	Init		(int nMin, int nMax, int nMinRange)
			{
			    m_nMin      =
			    m_nLoValue  = nMin;
			    m_nMax      =
			    m_nHiValue  = nMax;

			    SetStep
			   (m_nMinRange = nMinRange);
			}

	    void	SetHiValue	(int);
	    void	SetLoValue	(int);
	    void	SetStep		(int);
	    void	SetNum		(int);

	    int		GetValue	(int);

	    int		m_nMin	    ;
	    int		m_nMax	    ;
	    int		m_nMinRange ;

	    int		m_nStep	    ;
	    int		m_nLoValue  ;
	    int		m_nHiValue  ;
	};
//
//[]------------------------------------------------------------------------[]










#if FALSE

#define DECLARE_EXT_WINDOW_CLASS(theName,theStyle)			\
    public:								\
    static  Bool RegisterWindowClass ()					\
	    {								\
	        return (NULL == s_WndClass.Registered)			\
			      ? s_WndClass.Register () : False;		\
	    };								\
    protected :								\
    virtual GWindowClass &  GetWindowClass () const			\
	    {								\
		    return  s_WndClass;					\
	    };								\
    private :								\
    static  LPCTSTR GAPI    GetWindowClassInfo (WNDCLASSEX * wc = NULL)	\
	    {								\
		    LPCTSTR pName      = theName      ;			\
									\
		if (wc)							\
		{							\
		    ::GetWindowClassInfo (NULL, wc);			\
									\
		    wc ->style	       = theStyle     ;			\
									\
		    wc ->lpfnWndProc   = _initWndProc ;			\
		    wc ->lpszClassName = pName	      ;			\
		}							\
		    return  pName;					\
	    };								\
    static  GWindowClass    s_WndClass

//[]------------------------------------------------------------------------[]
//
class GLedWindow : public GWindow
{
    public:								\
    static  Bool RegisterWindowClass ()					\
	    {								\
	        return (NULL == s_WndClass.Registered)			\
			      ? s_WndClass.Register () : False;		\
	    };								\
    protected :								\
    virtual GWindowClass &  GetWindowClass () const			\
	    {								\
		    return  s_WndClass;					\
	    };								\
    private :								\
    static  LPCTSTR GAPI    GetWindowClassInfo (WNDCLASSEX * wc = NULL)	\
	    {								\
		    LPCTSTR pName      = theName      ;			\
									\
		if (wc)							\
		{							\
		    ::GetWindowClassInfo (NULL, wc);			\
									\
		    wc ->style	       = theStyle     ;			\
									\
		    wc ->lpfnWndProc   = _initWndProc ;			\
		    wc ->lpszClassName = pName	      ;			\
		}							\
		    return  pName;					\
	    };								\
    static  GWindowClass    s_WndClass	    ;
};
//
//[]------------------------------------------------------------------------[]

#endif


#endif//__MDIGUTILS_H
