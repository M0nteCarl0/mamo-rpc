//[]------------------------------------------------------------------------[]
// Main GWin::GUI definition
//
//
// Created by GSh.
//[]------------------------------------------------------------------------[]

/* Remember :

    WS_EX_CONTROLPARENT		Must be set for keyboard processing.
    
    WS_EX_NOINHERITLAYOUT	???
    WS_EX_COMPOSITED		???

    DS_SYSMODAL			???
*/

#ifndef	WM_FORWARDMSG
#define	WM_FORWARDMSG		    0x037F

#define	WM_PREPROCMSG		    WM_FORWARDMSG
//
//  WPARAM - Defined by user
//  LPARAM - (MSG *)
//
//  Return : == 0 - if not processed
//	     != 0 - if	   processed
#endif

#ifndef WM_BACKWARDMSG
#define	WM_BACKWARDMSG		    0x037E
//
//  WPARAM - Source Id
//  LPARAM - GWinMsg *
//
//  Return : not used;
//
#endif

#if FALSE
#define	 ISOLATION_AWARE_ENABLED    1
#define	 OEMRESOURCE
#endif

#include "g_winuser.h"
#include "MDIGConsole.rc.h"

#include <stdio.h>
#include <shlwapi.h>
#include <commctrl.h>
#include <uxtheme.h>

    static HWND	g_hWnd = NULL;

//[]------------------------------------------------------------------------[]
// GWin message processing model declarations
//
//
    struct GWinMsg;

    typedef Bool    (GAPI * GWinMsgProc )(void *, GWinMsg &);

    typedef LRESULT (GAPI * GWinHookProc)(void *, int, WPARAM, LPARAM);

    typedef void    (GAPI * GWinBackProc)(void *, int, WPARAM, LPARAM);

#pragma pack (_M_PACK_VPTR)
//
// Next flags declared for GWinMsg ored	 ::fResult
//
//
#define DISPATCH_LRESULT	    ((DWORD)0x00000001)
#define	DISPATCH_BRESULT	    ((DWORD)0x00000002)

    struct GWinMsg
    {
	LRESULT		lResult	;	// 4 / 8
	LRESULT		fResult ;	// 4 / 8

	typedef enum 
	{
	    Internal	    = -1,
	    WndHook		,
	    MSG			,
	    WndProc		,
	    DlgProc		,

	}		 Type	; 

//	Type		uType	;	// 4 / 4
	UINT		uMsg	;	// 4 / 4
	WPARAM		wParam	;	// 4 / 8
	LPARAM		lParam	;	// 4 / 8

	HWND		hWnd	;	// 4 / 8

	GWinMsgProc	pDefProc;	// 4 / 8
	void *		oDefProc;	// 4 / 8

// Public methods only this methods can be used in the Dispatchers ---------
//
        Bool    DispatchDefault		()
		{
		    return (pDefProc) ? (pDefProc)(oDefProc, * this) : False;
		};

        Bool    DispatchComplete	()
		{
                    return True;
		};

        Bool    DispatchComplete	(LRESULT r)
		{
		    fResult |= DISPATCH_LRESULT;
		    lResult  = r; return True  ;
		};

        Bool    DispatchCompleteDlgEx	(LRESULT r)
		{
		    fResult |= DISPATCH_BRESULT;
		    lResult  = r; return True  ;
		};
	//
	//.......
	//
        Bool    DispatchContinue	() const
		{
		    return False;
		};

        Bool    DispatchContinue	(LRESULT r)
		{
		    fResult |= DISPATCH_LRESULT;
		    lResult  = r; return False ;
		};

//	Bool	IsControlNotify		(HWND & hCtrl, int & nCtrlId);
//
// Public methods ----------------------------------------------------------
    };

    struct GWinMsgHandler
    {
	typedef	TSLink <GWinMsgHandler, 0>	SLink;
	typedef	TXList <GWinMsgHandler,
		TSLink <GWinMsgHandler, 0> >	SList;

	    SLink		m_SLink	;
	    SList *		m_PList	;

	    GWinMsgProc		m_pProc	;
    	    void *		m_oProc	;

		    GWinMsgHandler  (GWinMsgProc pProc = NULL, void * oProc = NULL)
		    {
			m_PList = NULL ;
			Init	 (pProc, oProc);
		    };

	    void    Init	    (GWinMsgProc pProc, void * oProc)
		    {
			m_pProc = pProc;
			m_oProc = oProc;
		    };

	    void    UnRegister	()
		    {
			if (m_PList)
			{
			    m_PList ->extract (this);
			    m_PList = NULL ;

			    m_pProc = NULL ;
			    m_oProc = NULL ;
			}
		    };

	    Bool    Dispatch	(GWinMsg & m)
		    {
			// GASSERT (NULL != m_pProc)
			//
			return (m_pProc)(m_oProc, m);
		    };
/*
	    Bool    DispatchAs	(GWinMsg & m, GWinMsg::Type t)
		    {
			Bool   bRes	= False ;

			GWinMsg::Type c = m.uType;

					  m.uType = t;

			bRes  = (m_pProc)(m_oProc, m);

					  m.uType = c;
			return bRes;
		    }
*/
    };
/*
    template <class T, typename P> 
    struct TWinMsgHandler : public GWinMsgHandler
    {
	    void *		m_oProcT;

	    void    Init	(Bool (T::* tProc)(P &), T * aProc)
		    {
			m_aProc	   = aProc;
			m_TProc () = tProc;

			GWinMsgHandler::Init ((Bool (GAPI *)(void *, P &)) Dispatch, this);
		    };

	    Bool    (T::*&  m_TProc () const)(P &)
		    {
			return (Bool (T::*&)(P &)) m_tProc;
		    };

   static Bool GAPI Dispatch	(TWinMsgHandler <T, P> * h, P & m)
		    {
			return (((T *) h ->m_aProc   ) ->*
				       h ->m_TProc ()) (m);
		    };
    };
*/
#pragma pack ()

/*
Bool GWinMsg::IsControlMouse  (HWND & hCtrl, int & nCtrlId, Bool bLocal = True)
{
};
*/

/*
Bool GWinMsg::IsControlNotify (HWND & hCtrl, int & nCtrlId)
{
    switch (uMsg)
    {
    case WM_COMMAND :
	if (NULL != (hCtrl   = (HWND)  lParam))
	{
		     nCtrlId = LOWORD (wParam);
	}
	    break;

    case WM_NOTIFY :
	if (NULL != (hCtrl   = ((NMHDR *) lParam) ->hwndFrom))
	{
		     nCtrlId = ((NMHDR *) lParam) ->idFrom  ;
	}
	    break;

    case WM_PARENTNOTIFY :
	    break;

    case WM_DRAWITEM :
	if (NULL != (hCtrl   = (wParam) ? ((DRAWITEMSTRUCT *) lParam) ->hwndItem : NULL))
	{
		     nCtrlId = ((DRAWITEMSTRUCT *) lParam) ->CtlID;
	}
	    break;

    case WM_MEASUREITEM :
	if (wParam && (ODT_MENU != ((MEASUREITEMSTRUCT *) lParam) ->CtlType))
	{
		     hCtrl   = ::GetDlgItem (hWnd, (nCtrlId = (int) wParam));
	}
	    break;

    case WM_COMPAREITEM :
	if (NULL != (hCtrl   = ((COMPAREITEMSTRUCT *) lParam) ->hwndItem))
	{
		     nCtrlId = ((COMPAREITEMSTRUCT *) lParam) ->CtlID;
	}
	    break;

    case WM_DELETEITEM :
	if (NULL != (hCtrl   = ((DELETEITEMSTRUCT  *) lParam) ->hwndItem))
	{
		     nCtrlId = ((DELETEITEMSTRUCT  *) lParam) ->CtlID;
	}
	    break;
    };

    if (hCtrl)
    {
	return True ;
    }
	return False;
};
*/











//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
void CalcLayoutRect (DWord dLayout, const GRect  & rt,
				          LONG	   cx,
				          LONG	   cy,
				          GRect  & wn)
{
    switch (GetHorLayout (dLayout))
    {
	case (GGF_LOLO_CONST_SIZE) :

	    wn. x = rt. x;
	    wn.cx = rt.cx;
	    break;

	case (GGF_HIHI_CONST_SIZE) :

	    wn. x = cx - rt.x - rt.cx;
	    wn.cx = rt.cx;
	    break;

	case (GGF_LOHI) :

	    wn. x = rt. x;
	    wn.cx = cx - rt.x - rt. x;
	    break;
    };
    switch (GetVerLayout (dLayout))
    {
	case (GGF_LOLO_CONST_SIZE) :

	    wn. y = rt. y;
	    wn.cy = rt.cy;
	    break;

	case (GGF_HIHI_CONST_SIZE) :

	    wn. y = cy - rt.y - rt.cy;
	    wn.cy = rt.cy;
	    break;

	case (GGF_LOHI) :

	    wn. y = rt. y;
	    wn.cy = cy - rt.y - rt.y;
	    break;
    };
};


//[]------------------------------------------------------------------------[]
//
#pragma pack (_M_PACK_VPTR)

#define DPF_VISIBLE	    0x00000001
#define DPF_UPDATED	    0x00000002
#define	DPF_VER		    0x00000080
#define	DPF_CTRL_BAR	    0x00000040	    // This fixed size control bar

//#define	DPF_DOCK_BAR	    0x00000004
//#define	DPF_DRAG_BAR	    0x00000008

#define	DPS_MAX		    16


//	struct GViewPane
	struct GDockPane
	{
	typedef TDLink <GDockPane, 0>	SLink	  ;
	typedef TXList <GDockPane, 
		TDLink <GDockPane, 0> >	SList	  ;
//	typedef enum
//	{	Horizontal  = 0,
//		Vertical	  }	SplitType ;

	    SLink			m_SLink	  ;
	    SList			m_SList	  ;

	    GDockPane *			m_pParent ;
	    DWord			m_dFlags  ;

	    void *			m_pOwner  ;
	    GRect			m_PRect	  ;

	    LONG	      m_lxWOff, m_hxWOff  ;
	    LONG	      m_lyWOff, m_hyWOff  ;
	    LONG	      m_cxWMin, m_cyWMin  ;
	    LONG	      m_cxWUsr, m_cyWUsr  ;

		    GDockPane ()
		    {
			m_pParent   = NULL;
			m_dFlags    =	 0;

			m_pOwner    = NULL;
			m_PRect.Assign (0, 0, 0, 0);

			m_lxWOff    =
			m_hxWOff    =
			m_lyWOff    =
			m_hyWOff    =
			m_cxWMin    =
			m_cyWMin    =
			m_cxWUsr    =
			m_cyWUsr    =	0;
		    };

	    GDockPane *	    GetParent	() const { return m_pParent;};

	    Bool	    IsRoot	() const { return NULL == GetParent ();};

	    Bool	    ChildOfRoot () const { return GetParent () ->IsRoot ();};

	    Bool	    ChildOfVer	() const { return GetParent () ->m_dFlags & DPF_VER;};

	    Bool	    ChildOfHor	() const { return ! ChildOfVer ();};

	    Bool	    IsVisible	() const { return m_dFlags & DPF_VISIBLE;};

	    Bool	    IsGrowable	() const { return ! (m_dFlags & DPF_CTRL_BAR);}

	    int		    GetHorGrowPriority	() const { return (HIBYTE(LOWORD(m_dFlags)) & 0xF0) >> 4;};

	    int		    GetVerGrowPriority	() const { return (HIBYTE(LOWORD(m_dFlags)) & 0x0F) >> 0;};

	    LONG	    HasDragBar		() const { return  LOBYTE(HIWORD(m_dFlags));};

	    int		    GetZorder		() const { return  HIBYTE(HIWORD(m_dFlags));};

	    void	    GetMinSize	(LONG * cxPMin, LONG * cyPMin) const;

	    void	    PRect	(GRect & pr) const 
			    {
				pr.Assign (0, 0, m_PRect.cx, m_PRect.cy);
			    };

	    const GPoint &  PRectPos	() const { return m_PRect.Pos  ();};

	    const GPoint &  PRectSize	() const { return m_PRect.Size ();};

	    void	    DragBarRect	(GRect & pr) const
			    {
				// GASSERT (this has DragBar !!!)
				//
				    PRect (pr);

				if (ChildOfVer ())
				{   
				    pr.x = pr.cx - HasDragBar ();
					   pr.cx = HasDragBar ();
				}
				else
				{
				    pr.y = pr.cy - HasDragBar ();
					   pr.cy = HasDragBar ();
				}
			    }

	    LONG	    WRectPosX	() const { return m_lxWOff;};

	    LONG	    WRectPosY	() const { return m_lyWOff;};

	    LONG	    WRectSizeX	() const { return m_PRect.cx - m_lxWOff - m_hxWOff;};

	    LONG	    WRectSizeY	() const { return m_PRect.cy - m_lyWOff - m_hyWOff;};

	    void	    WRect	(GRect & wr) const 
			    {
				wr.Assign (WRectPosX (), WRectPosY (), WRectSizeX (), WRectSizeY ());
			    };

	    void *	    GetOwner	() const
			    {
				for (GDockPane * i = (GDockPane *) GetParent (); i ; i = i ->GetParent ())
				{
				    if (i ->m_pOwner)
				    {
					return i ->m_pOwner;
				    }
				}
					return	   m_pOwner;
			    };

	    void	    PToOwner	(GPoint & p) const
			    {
				for (GDockPane * i = (GDockPane *) this; i ; i = i ->GetParent ())
				{
					p.Move (i ->m_PRect.x, i ->m_PRect.y);

				    if (i ->m_pOwner)
				    {
					return;
				    }
				}
			    };

	    void	    OwnerToP	(GPoint & p) const
			    {
				for (GDockPane * i = (GDockPane *) this; i ; i = i ->GetParent ())
				{
					p.Move (- i ->m_PRect.x, - i ->m_PRect.y);

				    if (i ->m_pOwner)
				    {
					return;
				    };
				}
			    };

	    void	    GetGrowableRect (GRect &) const;


	    void	    initWRect		(LONG  lxWOff, LONG hxWOff,
						 LONG  lyWOff, LONG hyWOff,
						 LONG  cxWMin, LONG cyWMin,
						 LONG  cxWUsr, LONG cyWUsr)
			    {
				m_lxWOff =  lxWOff;
				m_hxWOff =  hxWOff;
				m_lyWOff =  lyWOff;
				m_hyWOff =  hyWOff;

				m_cxWMin =  cxWMin;
				m_cyWMin =  cyWMin;
				m_cxWUsr =  cxWUsr;
				m_cyWUsr =  cyWUsr;
			    };

	    void	    InitRoot		(void *	      pOwner     ,
						 LONG cxP   , LONG cyP   ,
						 LONG lxWOff, LONG hxWOff,
						 LONG lyWOff, LONG hyWOff,
						 LONG cxWMin, LONG cyWMin)
			    {
				m_pOwner = pOwner;

				if ((lxWOff + cxWMin + hxWOff) > cxP) {cxP = cxWMin;}
				if ((lyWOff + cyWMin + hyWOff) > cyP) {cyP = cyWMin;}

				initWRect (lxWOff, hxWOff, lyWOff, hyWOff,
							   cxWMin, cyWMin,
						    cxP - lxWOff - hxWOff,
						    cyP - lyWOff - hyWOff);

				m_PRect.Assign (0, 0, cxP, cyP);

				m_dFlags = DPF_VISIBLE;
			    };

	    void	    InsertCtrlBar	(GDockPane *  pPane	 ,
						 GDockPane *  pBefore	 ,
						 void *	      pOwner	 ,
						 LONG lxWOff, LONG hxWOff,
						 LONG lyWOff, LONG hyWOff,
						 LONG czW   , LONG czWMin)
			    {
				pPane ->m_pOwner  = pOwner;
			       (pPane ->m_pParent = this) ->m_SList.insert
							   (pPane, pBefore);

				pPane ->m_dFlags  = DPF_CTRL_BAR;

				if (pPane ->ChildOfVer ())
				{
				    pPane ->initWRect (lxWOff, hxWOff, lyWOff, hyWOff,
								       czW   , czWMin,
								       czW   , czWMin);

				    pPane ->m_PRect.Assign (0, 0, 0, 0);
				}
				else
				{
				    pPane ->initWRect (lxWOff, hxWOff, lyWOff, hyWOff,
								       czWMin, czW   ,
								       czWMin, czW   );

				    pPane ->m_PRect.Assign (0, 0, 0, 0);
				}

				if (True/*pOwner ->HasStyle (WS_VISIBLE)*/)
				{
				    pPane ->Show (True);
				}
				else
				{
				    RecalcLayout ();
				}
			    };

	    void	    Insert		(GDockPane *  pPane	 ,
						 GDockPane *  pBefore	 ,
						 void *	      pOwner	 ,
						 LONG cxW   , LONG cyW	 ,
						 LONG lxWOff, LONG hxWOff,
						 LONG lyWOff, LONG hyWOff,
						 LONG cxWMin, LONG cyWMin)
			    {
				pPane ->m_pOwner  = pOwner;
			       (pPane ->m_pParent = this) ->m_SList.insert 
							   (pPane, pBefore);

				if (cxW < cxWMin) { cxW = cxWMin;}
				if (cyW < cyWMin) { cyW = cyWMin;}

				pPane ->initWRect (lxWOff, hxWOff, lyWOff, hyWOff,
								   cxWMin, cyWMin,
								   cxW	 , cyW   );

				pPane ->m_PRect.Assign (0, 0, 0, 0);

				if (True/*pOwner ->HasStyle (WS_VISIBLE)*/)
				{
				    pPane ->Show (True);
				}
				else
				{
				    RecalcLayout ();
				}
			    };

	    LONG	    GetDragBarWidth	(Bool bVertical)
			    {
				return 4;
			    };

	    LONG	    SetVerDragBar	(Bool bSet)
			    {
				m_hxWOff   -= HasDragBar ();
				m_PRect.cx -= HasDragBar ();
				m_dFlags   &= 0xFF00FFFF   ;

			    if (bSet)
			    {
				m_dFlags   |= (((DWord)((Byte) GetDragBarWidth (True))) << 16);

				m_hxWOff   += HasDragBar ();
				m_PRect.cx += HasDragBar ();
			    }
				return HasDragBar ();
			    };

	    LONG	    SetHorDragBar	(Bool bSet)
			    {
				m_hyWOff   -= HasDragBar ();
				m_PRect.cy -= HasDragBar ();
				m_dFlags   &= 0xFF00FFFF   ;

			    if (bSet)
			    {
				m_dFlags   |= (((DWord)((Byte) GetDragBarWidth (False))) << 16);

				m_hyWOff   += HasDragBar ();
				m_PRect.cy += HasDragBar ();
			    }
				return HasDragBar ();
			    };

	    void	    SetZorder		(int iZorder)
			    {
				m_dFlags  = (m_dFlags & 0x00FFFFFF) | ((DWord)((Byte) iZorder << 24));
			    };

	    void	    Show		(Bool bShow)
			    {
				m_dFlags &=	    (~DPF_VISIBLE);
				m_dFlags |= (bShow) ? DPF_VISIBLE : 0;

				//GASSERT (NULL != GetParent ()) Root pane always visible !!!
				//
				GetParent () ->RecalcLayout ();
			    };

	    void	    RecalcLayout	();

//	    void	    Validate		();

	    void	    UpdateLayout	();

	    void	    Grow		(LONG dx, LONG dy);

	    void	    UpdateContext	();

	    GDockPane *	    GetPaneByCursor	(LONG px, LONG py) const;

	    Bool	    IsCursorOverDragBar	(LONG px, LONG py) const;
	};

	struct GLayoutCtrlEntry
	{
	    int		    m_nCtrlId;
	    DWord	    m_dLayout;
	};

	struct GLayoutRect : public GDockPane
	{
	    const GLayoutCtrlEntry *
			    m_pEnties;
	};
/*
#define DECLARE_LAYOUT_RECT

#define	DEFINE_LAYOUT_RECT(theClass,theName)		    \
    const GLayoutCtrlEntry theClass::theNameGLayoutCtrl
*/
/*
	template <int N> struct TLayoutRect : public GLayoutRect
	{
			    TLayoutRect (DWord ) {};

	    CtrlEntry	    m_pForEntries [N - 1];
	};
*/
#pragma pack ()
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
#pragma pack (_M_PACK_VPTR)

    struct GWinProcThunk : public __stdcallThunk
    {
	    void *	pDspProc ;
	    void *	oDspProc ;
	    void *	oDefProc ;

		void *	    operator new    (size_t s);
		void	    operator delete (void * p);

	    GWinProcThunk (ThunkProc thunkProc, void * pDsp, void * oDsp, void * oDef)
	    {
		__stdcallThunk::SetDispPtr (thunkProc);

		pDspProc = pDsp;
		oDspProc = oDsp;
		oDefProc = oDef;
	    };

	    Bool	    IsThunkPtr	(ThunkProc pProc) const 
	    {
		return __stdcallThunk::IsThunkPtr (sizeof (this), pProc);
	    };
    };

#pragma pack ()

    struct GWndHookThunk : public GWinProcThunk
    {
	   GWndHookThunk    (GWinHookProc pDsp, void * oDsp) : 

	   GWinProcThunk    ((ThunkProc) Dispatch, pDsp, oDsp, NULL) {};

	   Bool		    IsThunkPtr	() const

			    { return GWinProcThunk::IsThunkPtr ((ThunkProc) Dispatch);};

    static LRESULT CALLBACK Dispatch	(const GWndHookThunk *, int, WPARAM, LPARAM);
    };

    struct GWndProcThunk : public GWinProcThunk
    {
	   GWndProcThunk    (GWinMsgProc pDsp, void * oDsp, WNDPROC pDef) : 

	   GWinProcThunk    ((ThunkProc) Dispatch, pDsp, oDsp, pDef) {};

	   Bool		    IsThunkPtr	() const

			    { return GWinProcThunk::IsThunkPtr ((ThunkProc) Dispatch);};

    static LRESULT CALLBACK Dispatch	(const GWndProcThunk *, HWND, UINT, WPARAM, LPARAM);

    static Bool	   GAPI	    DispatchDef (void *, GWinMsg &);
    };

    struct GDlgProcThunk : public GWinProcThunk
    {
	   GDlgProcThunk    (GWinMsgProc pDsp, void * oDsp, DLGPROC pDef) : 

	   GWinProcThunk    ((ThunkProc) Dispatch, pDsp, oDsp, pDef) {};

	   Bool		    IsThunkPtr	() const

			    { return GWinProcThunk::IsThunkPtr ((ThunkProc) Dispatch);};

    static INT_PTR CALLBACK Dispatch	(const GDlgProcThunk *, HWND, UINT, WPARAM, LPARAM);

    static Bool	   GAPI	    DispatchDef (void *, GWinMsg &);
    };
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
void * GWinProcThunk::operator new (size_t sz)
{
    return ::operator new (sz);
};

void GWinProcThunk::operator delete (void * p)
{
   ::operator delete (p);
};
//
//[]------------------------------------------------------------------------[]
//
LRESULT CALLBACK GWndHookThunk::Dispatch (const GWndHookThunk * thnk, int    nCode  ,
								      WPARAM wParam ,
								      LPARAM lParam )
{
	 thnk = (const GWndHookThunk *) thnk ->GetThisPtr ();

    return  ((GWinHookProc) thnk ->pDspProc)(thnk ->oDspProc, nCode, wParam, lParam);
};
//
//[]------------------------------------------------------------------------[]
//
LRESULT CALLBACK GWndProcThunk::Dispatch (const GWndProcThunk * thnk, HWND   hWnd   ,
								      UINT   uMsg   ,
								      WPARAM wParam ,
								      LPARAM lParam )
{
	 thnk = (const GWndProcThunk *) thnk ->GetThisPtr ();

    GWinMsg m = { 0, 0, uMsg, wParam, lParam, hWnd, GWndProcThunk::DispatchDef, thnk ->oDefProc};

    // GASSERT (thnk ->pDspProc != NULL)
    //
    if (! ((GWinMsgProc) thnk ->pDspProc)(thnk ->oDspProc, m))
    {
	    m.DispatchDefault ();
    }

    return  m.lResult;
};

Bool GAPI GWndProcThunk::DispatchDef (void * pWndProcDef, GWinMsg & m)
{
//	Do this call only once !!!
//
    {
	m.pDefProc = NULL;
	m.oDefProc = NULL;
    }
//	Not work with stdctrl32::windows !!!
//
//	return m.DispatchComplete (((WNDPROC) pWndProcDef)(m.hWnd, m.uMsg, m.wParam, m.lParam));
//
	return m.DispatchComplete (::CallWindowProc 
				   ((WNDPROC) pWndProcDef, m.hWnd, m.uMsg, m.wParam, m.lParam));
};
//
//[]------------------------------------------------------------------------[]
//
INT_PTR CALLBACK GDlgProcThunk::Dispatch (const GDlgProcThunk * thnk, HWND   hWnd   ,
								      UINT   uMsg   ,
								      WPARAM wParam ,
								      LPARAM lParam )
{
	 thnk = (const GDlgProcThunk *) thnk ->GetThisPtr ();

    GWinMsg m = { 0, 0, uMsg, wParam, lParam, hWnd, GDlgProcThunk::DispatchDef, thnk ->oDefProc};

    // GASSERT (thnk ->pDspProc != NULL)
    //
    INT_PTR r = ((GWinMsgProc) thnk ->pDspProc)(thnk ->oDspProc, m);

    if (!   r)
    {
	r = m.DispatchDefault ();
    }

    if (DISPATCH_BRESULT & m.fResult)   // Microsoft exception for DialogProc...
    {					//
	    r = (INT_PTR)  m.lResult;
    }
    else 
    if (DISPATCH_LRESULT & m.fResult)
    {
	::SetWindowLong (hWnd, DWL_MSGRESULT, m.lResult);
    }

    return  r;
};

Bool GAPI GDlgProcThunk::DispatchDef (void * pDlgProcDef, GWinMsg & m)
{
//	Do this call only once !!!
//
    {
	m.pDefProc = NULL;
	m.oDefProc = NULL;
    }
	return ((DLGPROC) pDlgProcDef)(m.hWnd, m.uMsg, m.wParam, m.lParam);
};

#pragma pack (_M_PACK_VPTR)

    struct HookDispatchProcContext
    {
	GWinMsgProc	pOrgDspProc ;
	void *		oOrgDspProc ;
	GWinMsgProc	pUsrDspProc ;
	void *		oUsrDspProc ;
    };

#pragma pack ()

/*
static Bool GAPI hookDispatchProc (HookDispatchProcContext * pContext, GWinMsg & m)
{

    m.pDefProc = pContext ->pOrgDspProc;
    m.oDefProc = pContext ->oOrgDspProc;

    return (pContext ->pUsrDspProc)(pContext ->oUsrDspProc, m);
};

DWord GWinProcThunk::ExecDispatchProc (GWinMsgProc pUsrProc, void * oUsrProc)
{
    DWord  dExitCode;

    HookDispatchProcContext Context = {pDspProc, oDspProc,
				       pUsrProc, oUsrProc};

    SetDispatchProc ((GWinMsgProc) hookDispatchProc, & Context);

	   dExitCode = GWinMsgLoop::Exec ();

    SetDispatchProc (Context.pOrgDspProc, Context.oOrgDspProc);

    return dExitCode;
};
*/
//
//[]------------------------------------------------------------------------[]

    class GWindow;

//[]------------------------------------------------------------------------[]
//
#pragma pack (_M_PACK_VPTR)

// Next flags declared for HIWORD of the GM_MESSAGE::wParam 
//
//
#define	GM_MESSAGE_BASE	     0

//  uMsg   : ---------------------------------------------------------------[]
//
#define	GM_MESSAGE	    (WM_APP + GM_MESSAGE_BASE)

//  wParam : ---------------------------------------------------------------[]
//
//#define	GM_Create		    1	// Send to Child same as WM_CREATE
//  lParam  : m_hWnd
//
//  lResult : 0 : success, ! = 0 Abort creation
//
//[]------------------------------------------------------------------------[]

//  wParam : ---------------------------------------------------------------[]
//
//#define	GM_Destroy		    2	// Send to Child
//  lParam  : m_hWnd
//
//[]------------------------------------------------------------------------[]

//  wParam : ---------------------------------------------------------------[]
//
#define	GM_RegisterCtrlNotify		    1
//
//  lParam  : GWinMsgHandler <GWindow, GWinMsg> *
//
//  lResult : True if Registered False : None

//  wParam : ---------------------------------------------------------------[]
//
#define GM_LookAtChild			    2
//
//  lParam  :  * GM_LookAtChild_Param

    typedef struct 
    {
	GWindow *	pChild	;	// Some action was happened
	DWord		dFlags  ;

    }	GM_LookAtChild_Param	;

/*
//  wParam : ---------------------------------------------------------------[]
//
#define	GM_Old_Child		    4	// Send to Parent
//  lParam  : pChild
//
//[]------------------------------------------------------------------------[]

//  wParam : ---------------------------------------------------------------[]
//
#define GM_New_Parent		    4	// Send to Child
//  lParam  : pOldParent
//
//[]------------------------------------------------------------------------[]

//  wParam : ---------------------------------------------------------------[]
//
#define GM_New_RClient		    5	// Send to Child
//  lParam  : GRect
//
//[]------------------------------------------------------------------------[]
*/
//[]------------------------------------------------------------------------[]
//
#define	GM_Get_RootViewPane
//  wParma  : none
//  lParam  : none
//
//  lResult : Pointer to the root ViewPane
//
//[]------------------------------------------------------------------------[]
#pragma pack ()

//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
// Win message dispatch filters
//
//
#define	DISPATCH_GM(Code,Method)		    \
    if	(Code == LOWORD(m.wParam))		    \
    {						    \
	return Method (m);			    \
    }

#define	DISPATCH_GM_(Code,Method,Type)		    \
    if	(Code == LOWORD(m.wParam))		    \
    {						    \
	return Method (m, (Type *) m.lParam);	    \
    }

#define	DISPATCH_GM_CODE(Code,Method,Type)	    \
    if  (Code == LOWORD(m.wParam))		    \
    {						    \
	return Method (m, (Type *) m.lParam);	    \
    }

#define DISPATCH_WM(msg,Method)			    \
    if  (msg == m.uMsg)				    \
    {						    \
	return Method (m);			    \
    }

/*
#define DISPATCH_WM(msg,Method)
    if	(msg == m.uMsg)
    {
	if (Method (m))
	{
	    return True;
	}
    }
*/



#define	DISPATCH_WM_WPARAM(msg,mwp,Method)	    \
    if ((msg == m.uMsg  )			    \
    &&  (mwp == m.wParam))			    \
    {						    \
	return Method (m);			    \
    }

#define DISPATCH_WM_WPARAM_LO(msg,mwp,Method)	    \
    if ((msg == m.uMsg		)		    \
    &&	(mwp == LOWORD (m.wParam)))		    \
    {						    \
	return Method (m);			    \
    }

#define	DISPATCH_WM_WPARAM_HI(msg,mwp,Method)	    \
    if ((msg == m.uMsg		)		    \
    &&	(mwp == HIWORD (m.wParam)))		    \
    {						    \
	return Method (m);			    \
    }

#define	DISPATCH_WM_LPARAM(msg,mlp,Method)	    \
    if ((msg == m.uMsg  )			    \
    &&  (mlp == m.lParam))			    \
    {						    \
	return Method (m);			    \
    }

#define IS_WM_COMMAND()				    \
      ((WM_COMMAND == m.uMsg) ? True : False)

#define	IS_WM_COMMAND_ID(Id)			    \
      ((Id   == LOWORD(m.wParam)) ? True : False)

#define IS_WM_COMMAND_CODE(Code)		    \
      ((Code == HIWORD(m.wParam)) ? True : False)

#define IS_WM_COMMAND_ID_CODE(Id,Code)		    \
     (((Id   == LOWORD(m.wParam))		    \
    && (Code == HIWORD(m.wParam))) ? True : False)

#define	DISPATCH_WM_COMMAND_ID(Id,Method)	    \
    if ((Id  == LOWORD(m.wParam)))		    \
    {						    \
	return Method (m);			    \
    }

#define	DISPATCH_WM_COMMAND_ID_CODE(Id,Code,Method) \
    if ((Id   == LOWORD(m.wParam))		    \
    &&	(Code == HIWORD(m.wParam)))		    \
    {						    \
	return Method (m);			    \
    }

#define	DISPATCH_WM_COMMAND_CODE(Code,Method)	    \
    if ((Code == HIWORD(m.wParam)))		    \
    {						    \
	return Method (m);			    \
    }

#define	DISPATCH_WM_NOTIFY_ID(Id,Method,Type)	    \
    if ((Id    == ((NMHDR *) m.lParam) ->idFrom))   \
    {						    \
	return Method (m, (Type *) m.lParam);	    \
    }

#define	DISPATCH_WM_NOTIFY_CODE(Code,Method,Type)   \
    if (Code  == ((NMHDR *) m.lParam) ->code)	    \
    {						    \
	return Method (m, (Type *) m.lParam);	    \
    }

#define	DISPATCH_WM_NOTIFY_ID_CODE(Id,Code,Method,Type) \
    if ((Id   == ((NMHDR *) m.lParam) ->idFrom)		\
    &&	(Code == ((NMHDR *) m.lParam) ->code  ))	\
    {							\
	return Method (m, (Type *) m.lParam);		\
    }
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
	Bool GAPI	    GetWindowClassInfo	(LPCTSTR, WNDCLASSEX *);

#define SYS_WINDOW_CLASS    ((HINSTANCE) sizeof (void *))
#define EXT_WINDOW_CLASS    ((HINSTANCE)	(NULL  ))

    struct GWindowClass
    {
	HINSTANCE	Registered		   ;	// Initiated SYS_WINDOW_CLASS or
							//	     EXT_WINDOW_CLASS
	LPCTSTR (GAPI * GetInfo) (WNDCLASSEX *	  );

	Bool		Register (HINSTANCE = NULL);
	void	      UnRegister ();
    };

#define DECLARE_SYS_WINDOW_CLASS(theName)				\
    protected :								\
    virtual GWindowClass &  GetWindowClass () const			\
	    {								\
		    return  s_WndClass;					\
	    };								\
    private :								\
    static  LPCTSTR GAPI    GetWindowClassInfo (WNDCLASSEX * wc = NULL)	\
	    {								\
		    LPCTSTR pName      = theName      ;			\
									\
		if (wc)							\
		{							\
		    ::GetWindowClassInfo (pName, wc);			\
		}							\
		    return  pName;					\
	    };								\
    static  GWindowClass    s_WndClass

#define DEFINE_SYS_WINDOW_CLASS(theName)				\
									\
    GWindowClass   theName::s_WndClass =				\
									\
    { SYS_WINDOW_CLASS, theName::GetWindowClassInfo }

#define DECLARE_EXT_WINDOW_CLASS(theName,theStyle)			\
    public:								\
    static  Bool RegisterWindowClass ()					\
	    {								\
	        return (NULL == s_WndClass.Registered)			\
			      ? s_WndClass.Register () : False;		\
	    };								\
    protected :								\
    virtual GWindowClass &  GetWindowClass () const			\
	    {								\
		    return  s_WndClass;					\
	    };								\
    private :								\
    static  LPCTSTR GAPI    GetWindowClassInfo (WNDCLASSEX * wc = NULL)	\
	    {								\
		    LPCTSTR pName      = theName      ;			\
									\
		if (wc)							\
		{							\
		    ::GetWindowClassInfo (NULL, wc);			\
									\
		    wc ->style	       = theStyle     ;			\
									\
		    wc ->lpfnWndProc   = _initWndProc ;			\
		    wc ->lpszClassName = pName	      ;			\
		}							\
		    return  pName;					\
	    };								\
    static  GWindowClass    s_WndClass

#define DEFINE_EXT_WINDOW_CLASS(theName)				\
									\
    GWindowClass   theName::s_WndClass =				\
									\
    { EXT_WINDOW_CLASS, theName::GetWindowClassInfo }
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
Bool GAPI GetWindowClassInfo (LPCTSTR pClassName, WNDCLASSEX * wc)
{
    wc ->cbSize		= sizeof (WNDCLASSEX);

    if (pClassName)
    {					    // Predefined Windows or global classes
					    //
    return ::GetClassInfoEx (			NULL , pClassName, wc)
    ||	   ::GetClassInfoEx (::GetModuleHandle (NULL), pClassName, wc);
    }
					    // EXT, User's local classes
					    //
    wc ->style		= 0    ;	    // May  be overwrited !!!

    wc ->lpfnWndProc	= ::DefWindowProc;  // Must be overwrited !!!

    wc ->cbClsExtra     = 0    ;
    wc ->cbWndExtra     = 0    ;
    wc ->hInstance      = NULL ;
    wc ->hIcon		= NULL ;
    wc ->hCursor	= ::LoadCursor (NULL, IDC_ARROW);

    wc ->hbrBackground  = NULL ;
    wc ->lpszMenuName   = NULL ;
    wc ->lpszClassName  = NULL ;	    // Must be overwrited !!!
    wc ->hIconSm	= NULL ;

    return True;
};

Bool GWindowClass::Register (HINSTANCE hInstance)
{
	WNDCLASSEX  wc;

//  GWinLock.LockAcquire ();
//
	if ((NULL == Registered) && GetInfo)
	{
	   (GetInfo)(& wc);

	    if ((   wc.lpszClassName != NULL )
	    &&  (   wc.lpfnWndProc   != NULL ))
	    {
		    wc.hInstance = (hInstance)
				 ?  hInstance : ::GetModuleHandle (NULL);

		if (RegisterClassEx (& wc))
		{
		    Registered = wc.hInstance;
		}
	    }
	}
//
//  GWinLock.LockRelease ();


#if FALSE

    if (NULL == Registered)
    {
    GWinLock.LockAcquire ();

    if (NULL == Registered)
    {
	if (GetInfo)
	{
	   (GetInfo)(& wc);

	    if ((   wc.lpszClassName != NULL )
	    &&  (   wc.lpfnWndProc   != NULL ))
	    {
		    wc.hInstance = (hInstance)
				 ?  hInstance : ::GetModuleHandle (NULL);

		if (RegisterClassEx (& wc))
		{
		    _Sys_InterlockedExchangePtr ( & wc,(Registered = wc.hInstance;
		}
	    }
	}
    }

    GWinLock.LockRelease ();
    }

#endif

	    return NULL != Registered;
};

void GWindowClass::UnRegister ()
{
//  GWinLock.LockAcquire ();
//
	if (SYS_WINDOW_CLASS < Registered)
	{
	    ::UnregisterClass ((GetInfo)(NULL),
			       Registered);

			       Registered = NULL;
	}
//
//  GWinLock.LockRelease ();
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
// Some User32 API extension
//
//  Return window rectangle relative from hFrom's Client area (NULL for screen).
//
Bool WINAPI	    GetWindowRect   (HWND hWnd, RECT *  pr, HWND hFrom);

Bool WINAPI	    GetWindowRect   (HWND hWnd, GRect * pr, HWND hFrom = NULL);
//
//...................

//  Return client rectangle in the GRect terms
//
Bool GFORCEINLINE   GetClientRect   (HWND hWnd, GRect * pr)
		    {
			return ::GetClientRect (hWnd, & pr ->Rect ());
		    };

Bool WINAPI	    GetClientRectEx (HWND hWnd, PRECT	pr);

Bool GFORCEINLINE   GetClientRectEx (HWND hWnd, GRect * pr)
		    {
			return ::GetClientRectEx (hWnd, & pr ->Rect ());
		    };
//...................

//  Converts the client coordinates of a specified rect to screen coordinates. 
//
Bool WINAPI	    ClientToScreen  (HWND hWnd, RECT *	pr);

Bool GFORCEINLINE   ClientToScreen  (HWND hWnd, GRect * pr)
		    {	return ::ClientToScreen (hWnd, (PPOINT) & pr ->Pos ());};
//...................

//  Converts the screen coordinates of a specified rect on the screen to client 
//  coordinates.
//
Bool WINAPI	    ScreenToClient  (HWND hWnd, RECT *  pr);

Bool GFORCEINLINE   ScreenToClient  (HWND hWnd, GRect * pr)
		    {	return ::ScreenToClient (hWnd, & pr ->Pos ());};
//...................

//  Converts the window coordinates of a specified rect to screen coordinates. 
//
Bool WINAPI	    WindowToScreen  (HWND hWnd, POINT *	pp);

Bool WINAPI	    WindowToScreen  (HWND hWnd, RECT  *	pr);

Bool GFORCEINLINE   WindowToScreen  (HWND hWnd, GRect * pr)
		    {	return ::WindowToScreen (hWnd, & pr ->Pos ());};
//...................

const void * WINAPI LoadResource    (HMODULE hResModule,    // NULL for current
				     LPCTSTR pResName  ,
				     LPCTSTR pResType  );
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
// User32 API extention
//
//
Bool WINAPI GetClientRectEx (HWND hWnd, RECT * pr)
{

//  if (!(::GetWindowLong (hWnd, GWL_STYLE) & WS_MINIMIZE))
    {
	::GetClientRect	  (hWnd, pr);

	return True;
    }
/*
	WINDOWPLACEMENT  wp; wp.length = sizeof (wp);

	::GetWindowPlacement (hWnd, & wp);

	pr ->left   = wp.rcNormalPosition.left	 ;
	pr ->top    = wp.rcNormalPosition.top	 ;
	pr ->right  = wp.rcNormalPosition.right	 ;
	pr ->bottom = wp.rcNormalPosition.bottom ;

	::SendMessage (hWnd, WM_NCCALCSIZE, (WPARAM) FALSE, (LPARAM) pr);

	pr ->right  -= pr ->left ; pr ->left = 0;
	pr ->bottom -= pr ->top  ; pr ->top  = 0;
*/
	return False;
};

Bool WINAPI ClientToScreen  (HWND hWnd, RECT * pr)
{
    pr ->right  -= pr ->left;
    pr ->bottom -= pr ->top ;

    ::ClientToScreen (hWnd, (PPOINT) & pr ->left);

    pr ->right  += pr ->left;
    pr ->bottom += pr ->top ;

    return True;
};

Bool WINAPI ScreenToClient (HWND hWnd, RECT * pr)
{
    pr ->right  -= pr ->left;
    pr ->bottom -= pr ->top ;

    ::ScreenToClient (hWnd, (PPOINT) & pr ->left);

    pr ->right  += pr ->left;
    pr ->bottom += pr ->top ;

    return True ;
};

Bool WINAPI WindowToScreen (HWND hWnd, POINT * pp)
{
    RECT    rw;

    ::GetWindowRect (hWnd, & rw);

    Move (pp, rw.left, rw.top);

    return True;
};

Bool WINAPI WindowToScreen (HWND hWnd, RECT * pr)
{
    RECT    rw;

    ::GetWindowRect (hWnd, & rw);

    Move (pr, rw.left, rw.top);

    return True;
};

Bool WINAPI GetWindowRect (HWND hWnd, GRect * pr, HWND hFrom)
{
	::GetWindowRect   (hWnd, & pr ->Rect ());

    pr ->cx -= pr ->x;
    pr ->cy -= pr ->y;

    if (hFrom)
    {
	::MapWindowPoints (NULL, hFrom, & pr ->Pos (), 1);
    }

    return True;
};

Bool WINAPI GetWindowRect (HWND hWnd, RECT * pr, HWND hFrom)
{
    GetWindowRect (hWnd, (GRect *) pr, hFrom);

    pr ->right  += pr ->left ;
    pr ->bottom += pr ->top  ;

    return True;
};

const void * WINAPI LoadResource (HMODULE hResModule, LPCTSTR pResName,
						      LPCTSTR pResType)
{
    hResModule = (hResModule) ? hResModule : ::GetModuleHandle (NULL);

    return (LPCDLGTEMPLATE) ::LockResource (
			    ::LoadResource (hResModule,
			    ::FindResource (hResModule, pResName, pResType)));
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
// Next flags declared for GWindow ored	 ::m_dFlags

#define GWF_CREATED		((DWord) 0x80000000)
/*
#define	GWF_ACTIVE		((DWord) 0x00080000)
*/
#define	GWF_NEEDUPDATE		((DWord) 0x00010000)

/*
#define GWF_NODESTROYHWND	((DWord) 0x00000001)
#define GWF_THUNKEDCLASS	((DWord) 0x00000002)
#define	GWF_SUBCLASSED		((DWord) 0x00000004)
#define GWF_UPDATELAYOUT	((DWord) 0x00008000)

#define GPF_PARENTLAYOUT	((DWord) 0x00000001)
*/

#ifdef _MSC_BUG_C4003
#ifdef GetFirstChild
#undef GetFirstChild
#endif
#endif

class GWindow
{
	friend class GDialog;

protected :
		GWindow	    (const void * pWndId)
		{
		    m_hWnd	    = NULL   ;
		    m_pWndId	    = pWndId ;
		    m_dFlags	    = 0	     ;

		    m_pParent	    = NULL   ;

		    m_pWndProc	    = NULL   ;
		};

virtual	       ~GWindow	    ();

public :

// Base methods --------------------------------------------------------------
//
	HWND		GetHWND		() const { return m_hWnd  ;};

	int		GetId		() const { return (int)(LOWORD(m_pWndId));};

	const void *	GetIdPtr	() const { return m_pWndId;};

	int		GetText		(LPTSTR pText, int uTextLen) const
			    { return ::GetWindowText (m_hWnd, pText, uTextLen);};

	GWindow *	GetParent	() const { return m_pParent;};

	GWndProcThunk *	GetWndProc	() const { return m_pWndProc;};

	DWord		GetFlags	() const { return m_dFlags;}

	DWord		SetFlags	(DWord dClrMask, DWord dSetMask)
			    { DWord  f = m_dFlags; m_dFlags = (m_dFlags & (~dClrMask)) | dSetMask; return f;};

	Bool		HasFlag		(DWord dFlags) const
			    { return dFlags & GetFlags ();};

	Bool		HasFlags	(DWord dFlags) const
			    { return ((dFlags & GetFlags ()) == dFlags) ? True : False;};
//
// Base methods --------------------------------------------------------------

// User32 API wrapper --------------------------------------------------------
//
	DWord		GetStyles	() const
			    { return ::GetWindowLong (GetHWND (), GWL_STYLE);};

	DWord		SetStyles	(DWord dClrMask, DWord dSetMask) const
			    { return ::SetWindowLong (GetHWND (), GWL_STYLE,
				     ::GetWindowLong (GetHWND (), GWL_STYLE) & (~dClrMask) | dSetMask);};
	Bool		HasStyle	(DWord	dStyle  ) const
			    { return   dStyle  & GetStyles ();};

	Bool		HasStyles	(DWord	dStyle	) const
			    { return ((dStyle  & GetStyles ()) == dStyle) ? True : False;};

	DWord		GetExStyles	() const
			    { return ::GetWindowLong (GetHWND (), GWL_EXSTYLE);};

	DWord		SetExStyles	(DWord dClrMask, DWord dSetMask) const
			    { return ::SetWindowLong (GetHWND (), GWL_EXSTYLE,
				     ::GetWindowLong (GetHWND (), GWL_EXSTYLE) & (~dClrMask) | dSetMask);};
	Bool		HasExStyle	(DWord	dExStyle) const
			    { return   dExStyle & GetExStyles ();};

	Bool		HasExStyles	(DWord	dExStyle) const
			    { return ((dExStyle & GetExStyles ()) == dExStyle) ? True : False;};

	Bool		IsVisible	() const			// Return true if this window & all parents
			    { return ::IsWindowVisible (GetHWND ());};	// has style WS_VISIBLE;

	Bool		IsEnabled	() const
			    { return ::IsWindowEnabled (GetHWND ());};

	Bool		IsActive	() const
			    { return (GetHWND () && (GetHWND () == ::GetActiveWindow ())) ? True : False;};

	Bool		EnableWindow	(Bool bEnable) const
			    { return ::EnableWindow (GetHWND (), bEnable);};

	Bool		ShowWindow	(Bool bShow, Bool bActivate = True) const
			    {
			      return ::ShowWindow (GetHWND (), bShow ? ((bActivate) ? SW_SHOW : SW_SHOWNA)
										    : SW_HIDE		 );
			    };

	Bool		Show		(Bool bActivate = False) const
			    {
			      return ShowWindow (True , bActivate);
			    };

	Bool		Hide		() const
			    {
			      return ShowWindow (False, False);
			    };
//
// Dialog control's wrapper --------------------------------------------------
//
	    HWND	GetCtrlHWND	(int nCtrlId) const
			    { return ::GetDlgItem (GetHWND (), nCtrlId);};

	    int		GetCtrlId	(HWND hCtrl) const
			    { return ::GetDlgCtrlID (hCtrl);};

	    int		GetCtrlText	(int nCtrlId, TCHAR * pTitle, int sTitle)
			    { return (int) ::GetDlgItemText (GetHWND (), nCtrlId, pTitle, sTitle);};

	    Bool	SetCtrlText	(int nCtrlId, const TCHAR * pTitle)
			    { return ::SetDlgItemText (GetHWND (), nCtrlId, pTitle);};

	    Bool	EnableCtrl	(int nCtrlId, Bool bEnable)
			    { return ::EnableWindow (GetCtrlHWND (nCtrlId), bEnable);};

	    Bool	ShowCtrl	(int nCtrlId, Bool bShow  )
			    {
			      return ::ShowWindow   (GetCtrlHWND (nCtrlId), bShow ? SW_SHOW : SW_HIDE); 
			    };

	    Bool	IsCtrlChecked	(int nCtrlId) const
			    { return (BST_CHECKED == ::IsDlgButtonChecked (GetHWND (), nCtrlId)) ? True : False;};

	    Bool	CheckCtrl	(int nCtrlId, Bool bCheck)
			    { return ::CheckDlgButton (GetHWND (), nCtrlId, (bCheck) ? BST_CHECKED : BST_UNCHECKED);};

	    LRESULT	SendCtrlMessage	(int nCtrlId, UINT uMsg, WPARAM wParam, LPARAM lParam);
//
// Dialog control's wrapper --------------------------------------------------
//
//	    ...
//
// User32 API wrapper --------------------------------------------------------

// Size/Position wrapper -----------------------------------------------------
//
//	void		GetClientRect	    (PRECT   pRect) const
//			    { CopyRect (pRect, & GetClientRect ());};

//	void		GetClientRect	(GRect * pRect) const
//			    { * pRect = GetClientRect ();};

	void		GetClientRect	(PRECT	 pRect ) const
			    { ::GetClientRect (GetHWND (), pRect);};

	void		GetClientRect	(GRect * pRect ) const
			    { ::GetClientRect (GetHWND (), pRect);};

	Bool		GetClientRectEx (PRECT	 pRect ) const
			    { return ::GetClientRectEx (GetHWND (), pRect);}

	Bool		GetClientRectEx (GRect * pRect ) const
			    { return ::GetClientRectEx (GetHWND (), pRect);};

	Bool		ClientToScreen	(PPOINT  pPoint) const
			    { return ::ClientToScreen (GetHWND (), pPoint);};

	Bool		ClientToScreen  (PRECT   pRect ) const
			    { return ::ClientToScreen (GetHWND (), pRect );};

	Bool		ClientToScreen  (GRect * pRect ) const
			    { return ::ClientToScreen (GetHWND (), pRect );};

	Bool		ScreenToClient  (PPOINT pPoint) const
			    { return ::ScreenToClient (GetHWND (), pPoint);};

	Bool		ScreenToClient	(PRECT pRect)   const
			    { return ::ScreenToClient (GetHWND (), pRect);};

	Bool		ScreenToClient  (GRect * pRect) const
			    { return ::ScreenToClient (GetHWND (), pRect);};
//
//----------------------------------------------------------------------------
//
//	void 		GetWindowRect	(GRect * pRect)  const
//			    { * pRect = GetWindowRect ();};

//	void		GetWindowRect	(PRECT	 pRect)  const
//			    { CopyRect (pRect, & GetWindowRect ());};

	void		GetWindowRect	(PRECT	 pRect ) const
			    { ::GetWindowRect (GetHWND (), pRect, GetParent () ? GetParent () ->GetHWND () : NULL);};

	void		GetWindowRect	(GRect * pRect ) const
			    { ::GetWindowRect (GetHWND (), pRect, GetParent () ? GetParent () ->GetHWND () : NULL);};

	Bool		WindowToScreen	(PPOINT	 pPoint) const
			    { return ::WindowToScreen (GetHWND (), pPoint);};

	Bool		WindowToScreen	(PRECT	 pRect ) const
			    { return ::WindowToScreen (GetHWND (), pRect );};

	Bool		WindowToScreen	(GRect * pRect ) const
			    { return ::WindowToScreen (GetHWND (), pRect );};
/*
	Bool		ScreenToWindow	(PPOINT	 pPoint) const;

	Bool		ScreenToWindow	(PRECT	 pRect ) const;

	Bool		ScreenToWindow	(GRect * pRect ) const;
*/
	HWND		GetFirstChild	()
			    { return ::GetWindow (GetHWND (), GW_CHILD);};

	HWND		GetLastChild	()
			    { return ::GetWindow (
				     ::GetWindow (GetHWND (), GW_CHILD   )
							    , GW_HWNDLAST);};
static	HWND		GetNextChild	(HWND hWnd)
			    { return ::GetWindow (hWnd	    , GW_HWNDNEXT);};

static	HWND		GetPrevChild	(HWND hWnd)
			    { return ::GetWindow (hWnd	    , GW_HWNDPREV);};

	Bool		SetWindowPos	(HWND	hAfter	,
					 int	x	,
					 int	y	,
					 int	cx	,
					 int	cy	,
					 UINT	uFlags	);
//
// Size/Position wrapper -----------------------------------------------------

// Paint wrapper -------------------------------------------------------------
//
	Bool		InvalidateRect	(const GRect * pRect, Bool bErase) const
			    {
				RECT	r;
				RECT *	p = (pRect) ? & r : NULL;

				if (p)
				{
				    CopyRect (p, pRect);
				}

				return ::InvalidateRect (GetHWND (), p, bErase);
			    };

	Bool		ValidateRect	(const GRect * pRect) const
			    {
				RECT	r;
				RECT *	p = (pRect) ? & r : NULL;

				if (p)
				{
				    CopyRect (p, pRect);
				}

				return ::ValidateRect (GetHWND (), p);
			    };
//
// Paint wrapper -------------------------------------------------------------

	LRESULT		Send_GM		(UINT Code, LPARAM lParam = 0, LRESULT lDefault  = 0);

	LRESULT		Send_WM		(UINT uMsg, WPARAM wParam ,
						    LPARAM lParam );

	Bool		Post_WM		(UINT uMsg, WPARAM wParam ,
						    LPARAM lParam )
			{
			    return (NULL !=  GetHWND ()) 
			    ? ::PostMessage (GetHWND (), uMsg, wParam, lParam) : False;
			};

// Size/Position wrapper -----------------------------------------------------
//
//    const GRect &	GetWindowRect	()		 const
//			    { return m_WRect;};
//
//	    void	GetWindowRect	(PRECT	 pRect)  const
//			    { CopyRect (pRect, & GetWindowRect ());};
//
//	    void 	GetWindowRect	(GRect * pRect)  const
//			    { * pRect = GetWindowRect ();};
//
// Size/Position wrapper -----------------------------------------------------

virtual	DWord		Exec		    ();

	DWord		ExecControl	    (GWindow *);

	DWord		ExecWindow	    ();

	void *	    	DoModal		    (GWinBackProc pBackProc	   ,
					     void *	  pBackProcContext );

	GWindow *	InsertChild	    (GWindow *  pChild  );

	void		Destroy		    ();

	void		DestroyChilds	    (HWND);

	void		SubclassWndProc	    (GWinMsgProc pWinProc, void * oWinProc)
			{
			    // GASSERT (GetHWND ())
			    //
			    ::SetWindowLongPtr (GetHWND (),   GWLP_WNDPROC,
							      (LONG_PTR) 
			    (m_pWndProc	  = new	GWndProcThunk (pWinProc   ,
							       oWinProc   ,
							      (WNDPROC )
			    ::GetWindowLongPtr (GetHWND (),   GWLP_WNDPROC))));
			};

	void		UnSubclassWndProc   ()
			{
			    // GASSERT (GetHWND ())
			    //
			    GWndProcThunk * thnk;

			    if (m_pWndProc)
			    {
				thnk = (GWndProcThunk *)  m_pWndProc ->oDefProc;

				::SetWindowLongPtr (GetHWND (), GWLP_WNDPROC,
							       (LONG_PTR) thnk);
				delete m_pWndProc;

				m_pWndProc = thnk ->IsThunkPtr () ? thnk : NULL;
			    }
			};
protected :

virtual	GWindowClass &	GetWindowClass	    () const =  NULL;

virtual	Bool		On_Create	    (void * pParam);

virtual	void		On_Destroy	    ();

	DWORD		CreateException	    (PEXCEPTION_RECORD);

static	LRESULT WINAPI _initWndProc	    (HWND, UINT, WPARAM, LPARAM);

	struct CreationParams
	{
	    LPCTSTR	    pTitle    ;
	    DWORD	    dStyle    ;
	    DWORD	    dExStyle  ;
	    int		    x , y     ;
	    int		    cx, cy    ;
	    HMENU	    hMenuOrId ;
	    void *	    pParam    ;
	};

	Bool		Create	    (GWindow *	pParent ,
				     HWND		,
				     void *	pParam	);

	Bool		Create	    (GWindow *	pParent	,
				     CreationParams   &	);

	Bool		Create	    (GWindow *	pParent	,
				     LPCTSTR    pTitle	,
				     DWord      dStyle	,
				     DWord      dExStyle,
				     int	x       ,
				     int	y       ,
				     int	cx      ,
				     int	cy	,
				     void *	pParam	);

// Window placement ----------------------------------------------------------
//
	void		SyncZOrderFor	    (GWindow *	pChild  ,
					     Bool	bShow	);

//
// Window placement ----------------------------------------------------------

// Message Dispatching -------------------------------------------------------
//
public :

virtual	Bool		PreprocMessage	    (    MSG *);

protected :

	int		IsCommand	    (GWinMsg &);

	int		IsControlNotify	    (GWinMsg &, HWND * hCtrl = NULL);

virtual	Bool		On_ReflectWinMsg    (GWinMsg &);

virtual	Bool		On_WinMsg	    (GWinMsg &);

virtual	Bool		DispatchWinMsg	    (GWinMsg::Type, GWinMsg &);

static	Bool GAPI	DispatchWinMsg	    (void *, GWinMsg &);







/*
virtual	LRESULT		DispatchWinMsg	    (	     HWND, UINT, WPARAM, LPARAM);

static	LRESULT	GAPI	DispatchWinMsg	    (void *, HWND, UINT, WPARAM, LPARAM);
*/

	DWord		ExecCaptureMouse    (GWinMsgProc, void *);

virtual	Bool		On_WM_ControlNotify (HWND hCtrl, int nCtrlId, GWinMsg &);

	Bool		On_GM_RegisterCtrlNotify      (GWinMsg &);
//
// Message Dispatching -------------------------------------------------------

// Dragging ------------------------------------------------------------------
//
#ifdef _MSC_BUG_C2248
public :
#endif
	enum DragProcReason
	{
	    Drag_EndCancel   = 0,
	    Drag_End		,
	    Drag_Begin		,
	    Drag_ShowGhost	,
	    Drag_HideGhost	,
	    Drag_MoveGhost	,
	};

#ifdef _MSC_BUG_C2248
protected :
#endif
	struct DragProcContext;

	typedef void	(GAPI *	DragProc)(DragProcReason    reason ,
					  DragProcContext * context);
	struct DragProcContext
	{
	    DragProc	m_pDragProc ;
	    GWindow *	m_pWindow   ;

	    GRect	m_rLimit    ;
	    GRect	m_rGhost    ;
	    GPoint	m_pBase     ;
	    GPoint	m_pCatch    ;

	union
	{
	    void *	m_pArgument ;
	    HBRUSH	m_hBrush    ;
	};
	    void *	m_pArgument2;

			DragProcContext (DragProc   pProc   ,
					 GWindow *  pWindow )
			{
			    m_pDragProc  = pProc	;
			    m_pWindow    = pWindow	;
			    m_rLimit       .Assign (-32000, -32000, 64000, 64000);

			    m_pBase	   .Assign (0, 0);
			    m_rGhost       .Assign (0, 0, 0, 0);
			    m_pCatch       .Assign (m_pBase.x - m_rGhost.x, 
						    m_pBase.y - m_rGhost.y);
			    m_pArgument  = NULL	;
			};

        void		InitGhost	(const GRect & r ,
					 LONG	       px,
					 LONG	       py)
			{
			    m_rGhost	   = r;
			    m_pCatch.Assign ((m_pBase.x = px) - m_rGhost.x,
					     (m_pBase.y = py) - m_rGhost.y);
			};

	const GPoint &	Base		() const { return m_pBase;};

	const GRect &	Ghost		() const { return m_rGhost;};

	void		MoveGhost	(LONG px, LONG py);

	void		InitLimit	(const GRect & r) { m_rLimit = r  ;};

	void		CallDragProc	(DragProcReason r) { (m_pDragProc)(r, this);};

	GWindow *	Window		() const { return m_pWindow;};

	void 		SetArgument	(void * pArgument) { m_pArgument = pArgument;};

	void *		GetArgument	() const { return m_pArgument; };

	void		SetBrush	(HBRUSH hBrush) { m_hBrush = hBrush;};

	HBRUSH		GetBrush	() const { return m_hBrush;};

	};

static	Bool GAPI	DragDispatchProc    (void * pDragProcContext, GWinMsg &);

	Bool		ExecCaptureMouseDragging
					    (GWinMsgProc, DragProcContext &);
//
// Dragging ------------------------------------------------------------------

// Child's layout control ----------------------------------------------------
//
	void		UpdateLayout	(GDockPane *);
//
// Child's layout control ----------------------------------------------------

protected :

	    HWND		    m_hWnd	;
	    const void *	    m_pWndId	;
	    DWord		    m_dFlags	;

	    GWindow *		    m_pParent	;

	    GWndProcThunk *	    m_pWndProc	;
};
//
//[]------------------------------------------------------------------------[]








//[]------------------------------------------------------------------------[]
//
static void PrintWCRect (GWindow * w)
{
    GRect		 r ;

    w ->GetWindowRect (& r);
    printf ("\t-Current WinPos {%4d.%4d}{%4d.%4d}\n", r.x, r.y, r.cx, r.cy);

    w ->GetClientRect (& r);
    printf ("\t-Current Client {%4d.%4d}{%4d.%4d}\n", r.x, r.y, r.cx, r.cy);
};

static void PrintWINDOWPOS (DWORD dStyle, WINDOWPOS * wp)
{
    printf ("%c", (wp ->flags & SWP_FRAMECHANGED) ? 'f' : '.');
    printf ("%c", (wp ->flags & SWP_HIDEWINDOW	) ? 'h' : '.');
    printf ("%c", (wp ->flags & SWP_SHOWWINDOW	) ? 's' : '.');
    printf ("%c", (wp ->flags & SWP_NOACTIVATE	) ? '.' : 'a');
    printf ("%c", (wp ->flags & SWP_NOMOVE	) ? '.' : 'm');
    printf ("%c", (wp ->flags & SWP_NOSIZE	) ? '.' : 's');

    if (!(wp ->flags & SWP_NOMOVE))
    printf ("{%4d.%4d}", wp ->x,  wp ->y);
    if (!(wp ->flags & SWP_NOSIZE))
    printf ("{%4d.%4d}", wp ->cx, wp ->cy);

    if (wp ->flags & SWP_FRAMECHANGED)
    {
	switch (dStyle & (WS_MINIMIZE | WS_MAXIMIZE))
	{
	    case 0:
		printf ("\n\t->Normal");
		break;
	    case WS_MINIMIZE :
		printf ("\n\t->Minimize");
		break;
	    case WS_MAXIMIZE :
		printf ("\n\t->Maximize");
		break;
	};
    }

    printf ("%s\n", (wp ->flags & SWP_NOCOPYBITS) ? ", No copy" : ", Copy");
};
//
//[]------------------------------------------------------------------------[]







//[]------------------------------------------------------------------------[]
//
LRESULT	GWindow::Send_GM  (UINT Code, LPARAM lParam, LRESULT lDefault)
{
    GWinMsg  m = { lDefault, 0, GM_MESSAGE, (WPARAM) Code, lParam, NULL, NULL};

    DispatchWinMsg (GWinMsg::Internal, m);

    return   m.lResult;
};

LRESULT GWindow::Send_WM (UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    if (GetHWND ())
    {
    return (m_pWndProc) ? ((WNDPROC)  m_pWndProc )(GetHWND (), uMsg, wParam, lParam)
			:	    ::SendMessage (GetHWND (), uMsg, wParam, lParam);
    }
    return 0L;
};

Bool GWindow::On_GM_RegisterCtrlNotify (GWinMsg & m)
{
/*
    if (GetParent ())
    {
    return GetParent () ->DispatchWinMsg (m);
    }
*/
    return m.DispatchComplete ((LRESULT) False);
};

Bool GWindow::PreprocMessage (MSG *)
{
    return False;
};


Bool GWindow::On_WM_ControlNotify (HWND hCtrlId, int nCtrlId, GWinMsg & m)
{
    return False;
};


//[]------------------------------------------------------------------------[]
//
int GWindow::IsCommand (GWinMsg & m)
{
    if ((m.hWnd == GetHWND ()) && (WM_COMMAND == m.uMsg))
    {
	return (int) LOWORD (m.wParam);
    } 
	return 0;
};

int GWindow::IsControlNotify (GWinMsg & m, HWND * pHandle)
{
    HWND    hCtrl   = NULL;
    int	    nCtrlId = 0	  ;

    if (m.hWnd == GetHWND ())
    {
    switch (m.uMsg)
    {
    case WM_COMMAND :
	if (NULL != (hCtrl   = (HWND)  m.lParam))
	{
		     nCtrlId = LOWORD (m.wParam);
	}
	    break;

    case WM_NOTIFY :
	if (NULL != (hCtrl   = ((NMHDR *) m.lParam) ->hwndFrom))
	{
		     nCtrlId = ((NMHDR *) m.lParam) ->idFrom  ;
	}
	    break;

    case WM_PARENTNOTIFY :
	    break;

    case WM_DRAWITEM :

	if (m.wParam 
	&& (NULL != (hCtrl   = ((DRAWITEMSTRUCT *) m.lParam) ->hwndItem)))
	{
		     nCtrlId = (int) m.wParam;
	}
	    break;

    case WM_MEASUREITEM :
	if (m.wParam && (ODT_MENU != ((MEASUREITEMSTRUCT *) m.lParam) ->CtlType))
	{
		     nCtrlId = (int) m.wParam;
	}
	    break;

    case WM_COMPAREITEM :
	if (NULL != (hCtrl   = ((COMPAREITEMSTRUCT *) m.lParam) ->hwndItem))
	{
		     nCtrlId = (int) m.wParam;
//		     nCtrlId = ((COMPAREITEMSTRUCT *) m.lParam) ->CtlID;
	}
	    break;

    case WM_DELETEITEM :
	if (NULL != (hCtrl   = ((DELETEITEMSTRUCT  *) m.lParam) ->hwndItem))
	{
		     nCtrlId = (int) m.wParam;
//		     nCtrlId = ((DELETEITEMSTRUCT  *) m.lParam) ->CtlID;
	}
	    break;

    case WM_VKEYTOITEM		:
    case WM_CHARTOITEM		:
    case WM_HSCROLL		:
    case WM_VSCROLL		:
    case WM_CTLCOLORBTN		:
    case WM_CTLCOLORDLG		:
    case WM_CTLCOLOREDIT	:
    case WM_CTLCOLORLISTBOX	:
    case WM_CTLCOLORMSGBOX	:
    case WM_CTLCOLORSCROLLBAR	:
    case WM_CTLCOLORSTATIC	:

	if (NULL != (hCtrl   = (HWND) m.lParam))
	{
		     nCtrlId = GetCtrlId (hCtrl);
	}
	    break;
    };
    }

    if (nCtrlId && pHandle)
    {
	return (NULL != (* pHandle = (hCtrl) ? hCtrl : GetCtrlHWND (nCtrlId))) ? nCtrlId : 0;
    }
	return nCtrlId;
};

Bool GWindow::On_ReflectWinMsg (GWinMsg & m)
{
    return False;
};

Bool GWindow::On_WinMsg (GWinMsg & m)
{
    if (WM_MOUSEACTIVATE == m.uMsg)
    {
    printf ("\t\t\t\t...Mouse activate %08X (%08X)\n", m.hWnd, m.wParam);
    }
    if (WM_SETFOCUS  == m.uMsg)
    {
    printf ("\t\t\t\t...Set  focus (%08X <- %08X)\n", m.hWnd, m.wParam);
    }
    if (WM_KILLFOCUS == m.uMsg)
    {
    printf ("\t\t\t\t...Lost focus (%08X -> %08X)\n", m.hWnd, m.wParam);
    }


    if (WM_USER + 3000 == m.uMsg)
    {
	printf ("<<Enter User\n");
	
//	    TCurrent () ->GetCurApartment () ->WaitInputWork ();

	return m.DispatchComplete ();
    }
    if (WM_ENTERIDLE == m.uMsg)
    {
	printf ("<<Enter Idle\n");

//	    while (TCurrent () ->GetCurApartment () ->PumpInputWork ())
//	    {}
//		   TCurrent () ->GetCurApartment () ->WaitInputWork ();

	return m.DispatchComplete ();
    }
    if (WM_ENTERMENULOOP == m.uMsg)
    {
	printf ("<<Enter MenuLoop\n");

//	    TCurrent () ->GetCurApartment () ->WaitInputWork ();

	return m.DispatchComplete ();
    }
    if (WM_ENTERSIZEMOVE == m.uMsg)
    {
	printf ("<<Enter SizeMove\n");

//	    TCurrent () ->GetCurApartment () ->WaitInputWork ();

	return m.DispatchComplete ();
    }
    if (WM_EXITMENULOOP == m.uMsg)
    {
	printf ("  Leave MenuLoop>>\n");

	return m.DispatchComplete ();
    }
	return False;
};

Bool GWindow::DispatchWinMsg (GWinMsg::Type t, GWinMsg & m)
{
    if (GWinMsg::WndProc == t)
    {
	return On_WinMsg (m);
    }
	return False;
};

Bool GAPI GWindow::DispatchWinMsg (void * oDsp, GWinMsg & m)
{
    GWindow * w = (GWindow *) oDsp;

    if (w ->HasFlag (GWF_CREATED))
    {
//	printf ("<<WinMsg (%08X) -> %08X {%08X %08X %08X}>>\n", m.hWnd, w ->GetHWND (), m.uMsg, m.wParam, m.lParam);

	return (w ->DispatchWinMsg (GWinMsg::WndProc, m)) ? True : m.DispatchDefault ();
    }
	       (w ->DispatchWinMsg (GWinMsg::WndProc, m)) ? True : m.DispatchDefault ();

    if (WM_CREATE     == m.uMsg)
    {
	if (m.lResult == 0)
	{
	    m.lResult = (w ->On_Create (((CREATESTRUCT *) m.lParam) ->lpCreateParams)) ? 0 : (w ->On_Destroy (), -1);
	}
	if (m.lResult == 0)
	{
	    w ->SetFlags (0, GWF_CREATED);
	}
    }
    else
    if (WM_NCDESTROY  == m.uMsg)
    {
	w ->UnSubclassWndProc ();

	w ->m_hWnd     = NULL;
	w ->m_pParent  = NULL;
    }
	return m.DispatchComplete ();
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
GWindow::~GWindow ()
{
    printf ("::~GWindow {%08X-%08X}\n", this, GetIdPtr ());

    Destroy ();

    if (m_pWndProc)
    {
	delete m_pWndProc;
	       m_pWndProc = NULL;
    }
};
//
//[]------------------------------------------------------------------------[]
//
Bool GWindow::SetWindowPos (HWND    hAfter  ,
			    int	    x	    ,
			    int	    y	    ,
			    int	    cx	    ,
			    int	    cy	    ,
			    UINT    uFlags  )
{
        uFlags |= (SWP_NOACTIVATE);

    if (uFlags &   SWP_NOREDRAW)
    {
	uFlags |= (SWP_NOCOPYBITS);
    }
    if (NULL == hAfter)
    {
	uFlags |= (SWP_NOZORDER | SWP_NOOWNERZORDER);
    }
    return ::SetWindowPos (GetHWND ()  ,
			   hAfter      ,
			   x, y, cx, cy, uFlags);
};

void GWindow::SyncZOrderFor (GWindow * pChild, Bool bZOrder)
{
// GASSERT (NULL != pChild)
//
//  for (GWindow * i = GetChild (

    if (pChild ->GetHWND () && pChild ->GetParent () ->GetHWND ())
    {
/*
	GWindow * pChildAfter = m_ChildList.getprev (pChild);

	::SetWindowPos (pChild ->GetHWND ()				    ,
		       (pChildAfter) ? pChildAfter ->GetHWND () : HWND_TOP  ,
			0, 0, 0, 0, 
			SWP_NOACTIVATE 
		      | SWP_NOMOVE    
		      | SWP_NOSIZE    
		      | SWP_NOCOPYBITS
		      | SWP_NOREDRAW  );
*/
    }
};

GWindow * GWindow::InsertChild (GWindow * pChild)
{
// GASSERT (NULL != pChild)
//
	Bool	    bShow   = False;
	GWindow *   pParent = pChild ->GetParent ();

    if     (pParent)			// pChild currently has parent
    {					//
	if (pParent != this)		// pChild lost current parent
	{				//
//	    pParent ->m_WinMsgHandler.SList.extract (& pChild ->m_WinMsgHandler);
//
//		      pChild ->m_pParent = NULL;

	    if (pChild ->GetHWND ())
	    {
		if (pChild ->IsVisible ())
		{
		    bShow  = True;

		    ::ShowWindow (pChild ->GetHWND (), SW_HIDE   );
		}
		    ::SetParent  (pChild ->GetHWND (), GetHWND ());
	    }
	}
    }
    {
	if (pParent != this)		// pChild accepts new parent
	{				//
//		      m_WinMsgHandler.SList.insert (& pChild ->m_WinMsgHandler);
//
//	    pParent = pChild ->m_pParent = this;

	    SyncZOrderFor  (pChild, bShow);
/*
	    if (pChild ->GetHWND ())
	    {
		SyncZOrder (pChild, bShow);

		if (bShow)
		{
		    ::ShowWindow (pChild ->GetHWND (), SW_SHOW	);
		}
	    }
*/
	}
	else				// pChild has not change parent
	{}				//
    }

    return  pChild;
};
//
//[]------------------------------------------------------------------------[]
//
void GWindow::On_Destroy ()
{
printf ("GWindow::On_Destroy (%08X:%08X)\n", this, GetHWND ());
};

Bool GWindow::On_Create (void *)
{
    return True;
};

void GWindow::Destroy ()
{
    if (GetHWND ())
    {
	       On_Destroy ();

	UnSubclassWndProc ();

	::DestroyWindow (GetHWND ());

	m_hWnd	      = NULL;
	m_pParent     = NULL;

	SetFlags (GWF_CREATED, 0);
    }
};

Bool GWindow::Create (GWindow * pParent, HWND hWnd, void * pParam)
{
    if (GetHWND () || (NULL == hWnd))
    {
	return False;
    }

    if (WS_CHILD & ::GetWindowLong (hWnd, GWL_STYLE))
    {
	if ((pParent		  == NULL			    )
	||  (pParent ->GetHWND () == NULL			    )
	||  (pParent ->GetHWND () != ::GetAncestor (hWnd, GA_PARENT)))
	{
	return False;
	}

	m_pParent = pParent;
    }
	m_hWnd	  = hWnd   ;

    if (! On_Create (pParam))
    {
	m_pParent = pParent;
	m_hWnd	  = NULL   ;

	return False;
    }
	SetFlags (0, GWF_CREATED);

	return True ;
};

static LRESULT CALLBACK _failWndProc (HWND hWnd, UINT uMsg, WPARAM wParam ,
							    LPARAM lParam )
{
    if (WM_NCCREATE == uMsg)
    {
	return  0;		// Abort create window
    }
    if (WM_CREATE   == uMsg)
    {
	return -1;		// Abort create window
    }
	return ::DefWindowProc (hWnd, uMsg, wParam, lParam);
};

LRESULT WINAPI GWindow::_initWndProc (HWND hWnd, UINT uMsg, WPARAM wParam ,
							    LPARAM lParam )
{
    WNDPROC   t	    =		 _failWndProc ;
    void    * a [3] = {(void *)  _initWndProc ,
		       (void *)	  hWnd	      ,
		       (void *) & t	      };

    ::RaiseException   (EXCEPTION_ACCESS_VIOLATION, 0, 3, (ULONG_PTR *) a);

    ::SetWindowLongPtr (hWnd, GWLP_WNDPROC, (LONG_PTR) t);

    return (t) (hWnd, uMsg, wParam, lParam);
};

DWORD GWindow::CreateException (PEXCEPTION_RECORD r)
{
    if ((EXCEPTION_ACCESS_VIOLATION ==		r ->ExceptionCode	    )
    &&  (3			    <=		r ->NumberParameters	    )
    &&	((void  *) _initWndProc	    == (void *) r ->ExceptionInformation [0]))
    {
	m_hWnd					 = (HWND)
	((void **) r ->ExceptionInformation [1]);
      * ((void **) r ->ExceptionInformation [2]) = 

	m_pWndProc = new GWndProcThunk (DispatchWinMsg, this, ::DefWindowProc);

	return EXCEPTION_CONTINUE_EXECUTION ;
    }
	return EXCEPTION_CONTINUE_SEARCH    ;
};

Bool GWindow::Create (GWindow * pParent, CreationParams & cParams)
{
    if (GetHWND ())
    {
	return False;
    }

    GWindowClass & hWndClass  = GetWindowClass ();
    HWND	   hWnd       = NULL ;
    HWND	   hWndParent = NULL ;
    DWord	   dStyle     = cParams.dStyle;

    if ((NULL ==   hWndClass.Registered	 )
    &&  (        ! hWndClass.Register  ()))
    {
	return False;
    }

    if (cParams.dStyle & WS_CHILD)
    {
	if ((NULL ==  pParent) 
	||  (NULL == (hWndParent = pParent ->GetHWND ())))
	{
	    return False;
	}
	    cParams.hMenuOrId  = (HMENU) GetId ();

	if (pParent ->IsVisible () && (cParams.dStyle & WS_VISIBLE))
	{
	    cParams.dStyle    &= (~(WS_VISIBLE));
	}
	    m_pParent = pParent;
    }
    else
    {
	if (pParent
	&& (NULL == (hWndParent = pParent ->GetHWND ())))
	{
	    return False;
	}
	if (cParams.dStyle    &    (WS_VISIBLE | WS_MINIMIZE))
	{
	    cParams.dStyle    &= (~(WS_VISIBLE | WS_MINIMIZE));
	}
    }

    __try
    {
	hWnd = ::CreateWindowEx  (cParams.dExStyle  ,
	      (hWndClass.GetInfo)(NULL)		    ,
				  cParams.pTitle    ,
				  cParams.dStyle    ,
				  cParams.x	    ,
				  cParams.y	    ,
				  cParams.cx	    ,
				  cParams.cy	    ,
				  hWndParent	    ,
				  cParams.hMenuOrId ,
	       ::GetModuleHandle (NULL)		    ,
				  cParams.pParam    );
    }
    __except (CreateException  ((GetExceptionInformation ()) ->ExceptionRecord))
    {}

    if (hWnd)
    {
	if (GetHWND () == NULL)
	{
	      m_hWnd = hWnd;

	    if (On_Create (cParams.pParam))
	    {
		SetFlags  (0, GWF_CREATED);
	    }
	    else
	    {
		Destroy  ();

		hWnd = NULL;
	    }
	}
	if (GetHWND ())
	{
		dStyle ^= cParams.dStyle;

	    if (dStyle & WS_VISIBLE)
	    {
		::ShowWindow (hWnd, (dStyle & WS_MINIMIZE) ? SW_SHOWMINIMIZED : SW_SHOW);
	    }
	    else
	    if (dStyle & WS_MINIMIZE)
	    {{
		::ShowWindow (hWnd, SW_MINIMIZE);
		::ShowWindow (hWnd, SW_HIDE    );

    printf ("\t\t%s %s\n", HasStyle (WS_VISIBLE) ? "Visible" : "", HasStyle (WS_MINIMIZE) ? "Minimized" : "");
	    }}

	    return GetHWND () ? True : False;
	}
    }
	    m_pParent = NULL;

	    return GetHWND () ? True : False;
};

Bool GWindow::Create ( GWindow * pParent  ,
		       LPCTSTR	 pTitle   ,
		       DWord	 dStyle   ,
		       DWord	 dExStyle ,
		       int	 x	  ,
		       int	 y	  ,
		       int	 cx	  ,
		       int	 cy	  ,
		       void *	 pParam   )
{
	CreationParams		 cParams  ;

	cParams.pTitle	       = pTitle   ;
	cParams.dStyle	       = dStyle   ;
	cParams.dExStyle       = dExStyle ;
	cParams.x	       = x	  ;
	cParams.y	       = y	  ;
	cParams.cx	       = cx	  ;
	cParams.cy	       = cy	  ;
	cParams.hMenuOrId      = NULL	  ;
	cParams.pParam	       = pParam   ;

	return Create (pParent, cParams);
};
//
//[]------------------------------------------------------------------------[]
//
DWord GWindow::ExecCaptureMouse (GWinMsgProc pDspProc, void * oDspProc)
{
    DWord   dExitCode	 = 0;

	::SetCapture	  (GetHWND ());

    if (::GetCapture () == GetHWND ())
    {
	SubclassWndProc	  (pDspProc, oDspProc);

//	    dExitCode	 = GWinMsgLoop::Exec ();

	UnSubclassWndProc ();

    if (::GetCapture () == GetHWND ())
    {
	::ReleaseCapture  ();
    }
    }
	return dExitCode;
};

void GWindow::DragProcContext::MoveGhost (LONG px, LONG py)
{
    GPoint  nBase (px - m_pCatch.x, py - m_pCatch.y);

				// Look at dragging limits
				//
    if (nBase.x < m_rLimit.x)
    {	nBase.x = m_rLimit.x;}

    if (nBase.y < m_rLimit.y)
    {	nBase.y = m_rLimit.y;}

    if (nBase.x + m_rGhost.cx > m_rLimit.x + m_rLimit.cx)
    {
	nBase.x = m_rLimit.x + m_rLimit.cx - m_rGhost.cx;
    }
    if (nBase.y + m_rGhost.cy > m_rLimit.y + m_rLimit.cy)
    {
	nBase.y = m_rLimit.y + m_rLimit.cy - m_rGhost.cy;
    }

    if (m_rGhost.Pos () != nBase)
    {
	CallDragProc (Drag_HideGhost);

	m_rGhost.Pos ()  = nBase;

	CallDragProc (Drag_ShowGhost);
    }
};

Bool GAPI GWindow::DragDispatchProc (void * pContext, GWinMsg & m)
{
/*
    if (m.uType == GWinMsg::WndProc)
    {
	DragProcContext *  dpc =
       (DragProcContext *) pContext;

	if ((WM_CAPTURECHANGED == m.uMsg)
	|| ((WM_KEYDOWN	       == m.uMsg) && ((int) m.wParam == VK_ESCAPE)))
	{
		dpc ->CallDragProc (Drag_HideGhost);
		dpc ->CallDragProc (Drag_EndCancel);

	    printf ("...Captrue Change or Escape Exit\n");

//		GWinMsgLoop::Exit (False);

	    return m.DispatchComplete ();
	}

	if (!((WM_MOUSEFIRST > m.uMsg)
	   || (WM_MOUSELAST  < m.uMsg)))
	{
	    printf ("...MouseCaptureProc...%08X:{%04X %4d.%4d}\n", m.hWnd, LOWORD(m.wParam), (short) LOWORD(m.lParam), (short) HIWORD(m.lParam));

	    if (!(m.wParam & MK_LBUTTON))
	    {
		dpc ->CallDragProc (Drag_HideGhost);
		dpc ->CallDragProc (Drag_End);

	    printf ("...Captrue Release Exit\n");

//		GWinMsgLoop::Exit (True );
	    }
	    else if (WM_MOUSEMOVE == m.uMsg)
	    {
		dpc ->MoveGhost (GET_X_LPARAM (m.lParam),
				 GET_Y_LPARAM (m.lParam));
	    }
	    return m.DispatchComplete ();
	}
    }
*/
	    return m.DispatchDefault  ();
};

static LRESULT GAPI CaptureHook (GWindow * w, int nCode, WPARAM wParam,
							 LPARAM lParam)
{
    if ((HC_ACTION == nCode) && (PM_REMOVE == wParam) && (NULL != lParam))
    {
    printf ("GetMsgHook:: %08X, %08X %08X %08X", ((MSG *) lParam) ->hwnd   ,
						 ((MSG *) lParam) ->message,
						 ((MSG *) lParam) ->wParam ,
						 ((MSG *) lParam) ->lParam );
    if ((WM_KEYFIRST <= (((MSG *) lParam) ->message))
    && (		(((MSG *) lParam) ->message) <= WM_KEYLAST))
    {
	((MSG *) lParam) ->hwnd = w ->GetHWND ();

    printf (" (Replace HWND)\n");

    return 0L;
    }
    printf ("\n");

    }
    return ::CallNextHookEx (NULL, nCode, wParam, lParam);
};

/*
static LRESULT GAPI CaptureHook (GWindow * w, int nCode, WPARAM wParam,
							 LPARAM lParam)
{
    if (0 <= nCode)
    {
    printf ("GetMsgHook:: %08X, %08X %08X %08X", ((MSG *) lParam) ->hwnd   ,
						 ((MSG *) lParam) ->message,
						 ((MSG *) lParam) ->wParam ,
						 ((MSG *) lParam) ->lParam );
    if ((WM_KEYFIRST <= (((MSG *) lParam) ->message))
    && (		(((MSG *) lParam) ->message) <= WM_KEYLAST))
    {
	((MSG *) lParam) ->hwnd = w ->GetHWND ();

    printf (" (Replace HWND)\n");

    return ::CallNextHookEx (NULL, nCode, wParam, lParam);
    }
    printf ("\n");

    }
    return ::CallNextHookEx (NULL, nCode, wParam, lParam);
};
*/
Bool GWindow::ExecCaptureMouseDragging (GWinMsgProc pDspProc, DragProcContext & dpc)
{
    Bool    bNotCancel	 = False;
    HHOOK   hh		 = NULL;
    GWndHookThunk   * th = new GWndHookThunk
			   ((GWinHookProc) CaptureHook, this);

	::SetCapture (GetHWND ());

    if (::GetCapture () == GetHWND ())
    {
    if (NULL !=
       (hh    = ::SetWindowsHookEx (WH_GETMESSAGE,
				   (HOOKPROC)  th, NULL, TCurrent () ->GetId ())))
    {
    printf ("...Enter CaptureMouse %08X %08X<<\n", m_pWndProc, m_pWndProc ->oDefProc);

	dpc.CallDragProc  (Drag_Begin    );
	dpc.CallDragProc  (Drag_ShowGhost);

	SubclassWndProc   (pDspProc, & dpc);

//	    bNotCancel   = GWinMsgLoop::Exec ();

	UnSubclassWndProc ();

    printf ("...Leave CaptureMouse %08X %08X>>\n", m_pWndProc, m_pWndProc ->oDefProc);

		::UnhookWindowsHookEx (hh);
    }

    if (::GetCapture () == GetHWND ())
    {
	::ReleaseCapture ();
    }
    }
	delete (th);

	return bNotCancel;
};

DWord GWindow::Exec ()
{
    return 0;
};

#if FALSE

void * GWindow::DoModal (GWinBackProc pBackProc, void * pBackProcContext)
{
    LONG    bExitFlag =	    1 ;
    DWord   dExitCode = (DWord) -1;
    Bool    bEnabled  = False ;
    Bool    bHiden    = False ;
    HWND    hOwner    = NULL  ;

    if (NULL ==	GetHWND ())
    {
	return dExitCode;
    }
	hOwner = ::GetParent (GetHWND ());
					// Save current visible state and...
	bHiden = ! IsVisible ();
					// Show window
	ShowWindow (True, True);
					// Disable owner if it exist & not disabled
    if (hOwner)
    {
	bEnabled = True;

    if (bEnabled = ::IsWindowEnabled (hOwner))
    {
	::EnableWindow (hOwner, False);
    }
    }
					// Get exec code
printf ("\t<<Exec: %08X\n",   GetHWND ());

	dExitCode = TCurrent () ->GetCurApartment () ->WorkLoop (& bExitFlag);

printf ("\t  Exec: %08X>>\n\n", GetHWND ());
					// Restore Owner's enable
    if (bEnabled)
    {
	::EnableWindow (hOwner, True );
    }
					// Hide if was not visible
    if (bHiden)
    {
	ShowWindow (False);
    }
	return dExitCode;
};

#endif
/*
INT_PTR GWindow::ExecWindow (void *, 
*/
DWord GWindow::ExecWindow ()
{
    LONG    bExitFlag =	    1 ;
    DWord   dExitCode = (DWord) -1;
    Bool    bEnabled  = False ;
    Bool    bHiden    = False ;
    HWND    hOwner    = NULL  ;

    if (NULL ==	GetHWND ())
    {
	return dExitCode;
    }
	hOwner = ::GetParent (GetHWND ());
					// Save current visible state and...
	bHiden = ! IsVisible ();
					// Show window
	ShowWindow (True, True);
					// Disable owner if it exist & not disabled
    if (hOwner)
    {
	bEnabled = True;

    if (bEnabled = ::IsWindowEnabled (hOwner))
    {
	::EnableWindow (hOwner, False);
    }
    }
					// Get exec code
printf ("\t<<Exec: %08X\n",   GetHWND ());

	dExitCode = TCurrent () ->GetCurApartment () ->WorkLoop (& bExitFlag);

printf ("\t  Exec: %08X>>\n\n", GetHWND ());
					// Restore Owner's enable
    if (bEnabled)
    {
	::EnableWindow (hOwner, True );
    }
					// Hide if was not visible
    if (bHiden)
    {
	ShowWindow (False);
    }
	return dExitCode;
};

/*
//[]------------------------------------------------------------------------[]
//
DWord GWinMsgThread::ExecWindow (GWindow * pWindow, int iCmdShow)
{
    DWord	    dExitCode	    = (DWord) -1;
    Bool	    bOwnerEnabled   = False;
    Bool	    bHiden	    = False;
    GWindow *	    pExecWindow	    = NULL ;

    if ((NULL == pWindow	       )
    ||  (NULL == pWindow ->GetHWND   ())
    ||	(NULL != pWindow ->GetParent ()))
    {
	    return dExitCode;
    }
					// Save current visible state and
	    bHiden	  = 
	  ! pWindow ->IsWindowVisible    ();
					// Show window with iShowCmd
					//					
	    pWindow ->ShowWindow (iCmdShow);

    if	   (pWindow ->GetOwner ())
    {					// Disable owner if it exist & not disabled
	if (bOwnerEnabled = 
	    pWindow ->GetOwner () ->IsWindowEnabled (	  ))
	{
	    pWindow ->GetOwner () ->EnableWindow    (False);
	}
    }
    else
    {
	if (NULL == m_pMainWindow)	// Set this window as Main window
			  m_pMainWindow = pWindow;
    }
					// Save & set new Exec window
	    pExecWindow	= m_pExecWindow ;
			  m_pExecWindow = pWindow;

					// Run Dispatch processing
	    dExitCode	= RunDispatchLoop ();

					// Restore old m_ExecWindow
			  m_pExecWindow = pExecWindow;

					// Check if this is Main window
	if (m_pMainWindow = pWindow)
	{
			  m_pMainWindow = NULL;
	}

	if (bOwnerEnabled)		// If was disabled at entry, enable owner !!!
	{
	    pWindow ->GetOwner () ->EnableWindow    (True );
	}

	if (bHiden)			// If was hiden in entry, hide it !!!
	{
	    pWindow ->ShowWindow (SW_HIDE);
	}
	    return dExitCode;
};
//
//[]------------------------------------------------------------------------[]
*/


//[]------------------------------------------------------------------------[]
//
class GParentWindow : public GWindow
{
DECLARE_EXT_WINDOW_CLASS (_T("GWin.ParentWindow"), CS_DBLCLKS);

public :

	    GParentWindow (const void * pWndId) :

	    GWindow	  (pWndId)
	    {};

	Bool		PreprocMessage		    (MSG * msg);

protected :


//	Bool		DispatchWinMsg		    (GWinMsg &);

	Bool		On_GM_RegisterCtrlNotify    (GWinMsg &);

	Bool		On_WM_PosChanged	    (GWinMsg &);
	Bool		On_WM_Activate		    (GWinMsg &);

	Bool		On_WM_Notify		    (GWinMsg &);
	Bool		On_WM_Command		    (GWinMsg &);
	Bool		On_WM_MeasureItem	    (GWinMsg &);
	Bool		On_WM_DrawItem		    (GWinMsg &);

	Bool		On_WinMsg		    (GWinMsg &);

virtual	Bool		DispatchReflectWinMsg	    (GWinMsg &);

	GWinMsgHandler *    getCtrlNotifyHandler    (HWND) const;

	GWinMsgHandler *    getCtrlNotifyHandler    (int ) const;

protected :

	    GWinMsgHandler::SList   m_CtrlNotifyList ;
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
DEFINE_EXT_WINDOW_CLASS (GParentWindow);

GWinMsgHandler * GParentWindow::getCtrlNotifyHandler (HWND hChild) const
{
    GWinMsgHandler * h = NULL;

    for (h = m_CtrlNotifyList.first (); h; h = m_CtrlNotifyList.getnext (h))
    {
	if (((GWindow *) h ->m_oProc) ->GetHWND () == hChild)
	{
	    break;
	}
    }
	return  h;
};

GWinMsgHandler * GParentWindow::getCtrlNotifyHandler (int nCtrlId) const
{
    GWinMsgHandler * h = NULL;

    for (h = m_CtrlNotifyList.first (); h; h = m_CtrlNotifyList.getnext (h))
    {
	if (((GWindow *) h ->m_oProc) ->GetId () == nCtrlId)
	{
	    break;
	}
    }
	return  h;
};

Bool GParentWindow::On_WM_Notify (GWinMsg & m)
{
/*
    GWinMsgHandler * h;

    if (NULL != (h = getCtrlNotifyHandler (((NMHDR *) m.lParam) ->hwndFrom)))
    {
	return h ->DispatchAs (m, GWinMsg::CtrlNotify);
    }
*/
	return False;
};

Bool GParentWindow::On_WM_Command (GWinMsg & m)
{
/*
    GWinMsgHandler * h;

    if ((HWND) m.lParam && (NULL != (h = getCtrlNotifyHandler ((HWND) m.lParam))))
    {
	return h ->DispatchAs (m, GWinMsg::CtrlNotify);
    }
*/
	return False;
};

Bool GParentWindow::On_WM_MeasureItem (GWinMsg & m)
{
/*
    GWinMsgHandler * h;

    if ((int) m.wParam && (NULL != (h = getCtrlNotifyHandler ((int) m.wParam))))
    {
	return h ->DispatchAs (m, GWinMsg::CtrlNotify);
    }
*/
	return False;
};

Bool GParentWindow::On_WM_DrawItem (GWinMsg & m)
{
/*
    GWinMsgHandler * h;

    if ((int) m.wParam && (NULL != (h = getCtrlNotifyHandler (((DRAWITEMSTRUCT *) m.lParam) ->hwndItem))))
    {
	return h ->DispatchAs (m, GWinMsg::CtrlNotify);
    }
*/
	return False;
};




Bool GParentWindow::On_WM_PosChanged (GWinMsg & m)
{
//-------------------------------
//
{
    printf ("GParentWindow::WM_PosChanged <----------------\n");

    PrintWCRect (this);

    PrintWINDOWPOS  (GetStyles (), (WINDOWPOS *) m.lParam);

    printf ("----------------------------------------------\n\n");
}
//
//-------------------------------

    if (! HasStyle (WS_MINIMIZE))
    {
	if ((((WINDOWPOS *) m.lParam) ->flags & SWP_NOSIZE) == 0)
	{
	    if (HasFlag  (GWF_NEEDUPDATE))
	    {
	printf ("May be update layout\n");
	    }
	}
    }

//	return m.DispatchComplete ();

	return m.DispatchDefault  ();
};

Bool GParentWindow::On_WM_Activate (GWinMsg & m)
{
/*
    if (WA_INACTIVE == LOWORD(m.wParam))
    {
	SetFlags ((~GWF_ACTIVE) & GetFlags ());
    }
    else
    {
	SetFlags (( GWF_ACTIVE) | GetFlags ());

    if (WA_ACTIVE   == LOWORD(m.wParam))
    {					// Throw any other than mouse 
					// activation method
    }
    }
*/

    printf ("\t\t\t\t...%s (%08X)\n", (LOWORD (m.wParam) == WA_INACTIVE) ? "Disactivate" 
									 : "Activate   ", m.hWnd);

    if (WA_INACTIVE == m.wParam)
    {
	if (g_hWnd == GetHWND ())
	{
	    g_hWnd  = NULL;
	}
    }
    else
    {
	    g_hWnd  = GetHWND ();
    }




/*
    g_hWnd = (WA_INACTIVE == LOWORD (m.wParam)) ? NULL : (::SetFocus (GetHWND ()), GetHWND ());

*/
//	return ::DefDlgProc (m.hWnd, m.uMsg, m.wParam, m.lParam);


//	GWinMsgLoop::SetActiveWindow (m.wParam == WA_INACTIVE) ? NULL : this);

/*
    if (WM_SETFOCUS == m.uMsg)
    {
	printf ("WM_Accept focus\n");
    }
    if (WM_KILLFOCUS == m.uMsg)
    {
	printf ("WM_Lost focus\n");
    }
*/
	return m.DispatchDefault ();
};

Bool GParentWindow::On_GM_RegisterCtrlNotify (GWinMsg & m)
{
    m_CtrlNotifyList.append

    ((GWinMsgHandler *) m.lParam);

    ((GWinMsgHandler *) m.lParam) ->m_PList = & m_CtrlNotifyList;

    return m.DispatchComplete ((LRESULT) True);
};

Bool GParentWindow::PreprocMessage (MSG * msg)
{
    if (((WM_KEYFIRST   > msg ->message) || (msg ->message > WM_KEYLAST  ))
    &&  ((WM_MOUSEFIRST > msg ->message) || (msg ->message > WM_MOUSELAST)))
    {
	    return False;
    }

    HWND hWndCtrl = ::GetFocus ();

    if  (hWndCtrl && IsChild (GetHWND (), hWndCtrl))
    {
	for (; GetHWND () != ::GetParent (hWndCtrl); hWndCtrl = ::GetParent (hWndCtrl));

	if  (0 != ::SendMessage (hWndCtrl, WM_FORWARDMSG, (WPARAM) 0, (LPARAM) msg))
	{
	    return True;
	}
    }

    if (::IsDialogMessage    (GetHWND (), msg))
    {
/*
printf ("\t\t\t\t\t<<IsDialogMessage Processed by %08X\n", GetHWND ());
*/
	    return True;
    }

	    return GWindow::PreprocMessage (msg);
/*
printf ("\t\t\t\t\t<<Check...(%08X << %08X\n", GetHWND (), msg ->hwnd);

    if  (::IsDialogMessage   (GetHWND (), msg))
    {
printf ("\t\t\t\t\t  Check... True >>\n");
	    return True ;
    }
printf ("\t\t\t\t\t  Check... False>>\n");

	    return False;
*/
};

Bool GParentWindow::DispatchReflectWinMsg (GWinMsg & m)
{
    HWND    hCtrl   = NULL  ;
    int	    nCtrlId = 0	    ;
    
    switch (m.uMsg)
    {
    case WM_COMMAND :
	if (NULL != (hCtrl   = (HWND) m.lParam))
	{
		     nCtrlId = LOWORD (m.wParam);
	}
	    break;

    case WM_NOTIFY :
	if (NULL != (hCtrl   = ((NMHDR *) m.lParam) ->hwndFrom))
	{
		     nCtrlId = ((NMHDR *) m.lParam) ->idFrom  ;
	}
	    break;

    case WM_DRAWITEM :
	if (NULL != (hCtrl   = (m.wParam) ? ((DRAWITEMSTRUCT *) m.lParam) ->hwndItem : NULL))
	{
		     nCtrlId = ((DRAWITEMSTRUCT *) m.lParam) ->CtlID;
	}
	    break;

    case WM_MEASUREITEM :
	if (m.wParam)
	{
		     hCtrl   = GetCtrlHWND 
		    (nCtrlId = (int) m.wParam);
	}
	    break;

    case WM_COMPAREITEM :
	if (NULL != (hCtrl   = ((COMPAREITEMSTRUCT *) m.lParam) ->hwndItem))
	{
		     nCtrlId = ((COMPAREITEMSTRUCT *) m.lParam) ->CtlID;
	}
	    break;

    case WM_DELETEITEM :
	if (NULL != (hCtrl   = ((DELETEITEMSTRUCT *) m.lParam) ->hwndItem))
	{
		     nCtrlId = ((DELETEITEMSTRUCT *) m.lParam) ->CtlID;
	}
	    break;
    };

    if (hCtrl)
    {
	return On_WM_ControlNotify (hCtrl, nCtrlId, m);
    }
	return False;
};

Bool GParentWindow::On_WinMsg (GWinMsg & m)
{
    if (WM_FORWARDMSG == m.uMsg)
    {
	return m.DispatchComplete (PreprocMessage ((MSG *) m.lParam));
    }
	DISPATCH_WM	(WM_NOTIFY	      ,	On_WM_Notify	    )
	DISPATCH_WM	(WM_COMMAND	      ,	On_WM_Command	    )
	DISPATCH_WM	(WM_MEASUREITEM	      , On_WM_MeasureItem   )
	DISPATCH_WM	(WM_DRAWITEM	      , On_WM_DrawItem	    )

	DISPATCH_WM	(WM_WINDOWPOSCHANGED  ,	On_WM_PosChanged    )
	DISPATCH_WM	(WM_ACTIVATE	      , On_WM_Activate	    )

/*
	DISPATCH_REFLECT    ()

*/
	return GWindow::On_WinMsg (m);
};


#if FALSE
Bool GParentWindow::DispatchWinMsg (GWinMsg::Type t, GWinMsg & m)
{
    switch (t)
    {
    case GWinMsg::Internal :

	DISPATCH_GM	(GM_RegisterCtrlNotify,	On_GM_RegisterCtrlNotify)

	return GWindow::DispatchWinMsg (m);
/*
    case GWinMsg::WndProcCrt :

	DISPATCH_WM	(WM_NOTIFY	      ,	On_WM_Notify	    )
	DISPATCH_WM	(WM_COMMAND	      ,	On_WM_Command	    )
	DISPATCH_WM	(WM_MEASUREITEM	      , On_WM_MeasureItem   )

	return GWindow::DispatchWinMsg (m);
*/
    case GWinMsg::WndProc :

    };
	return GWindow::DispatchWinMsg (m);
};
#endif

/*
Bool GParentWindow::GetReflectHandler (GWinMsgHandler ** pHandler, GWinMsg & m)
{
    HWND    hTarget = NULL;

    switch (m.uMsg)
    {
    case WM_COMMAND :

	hTarget = (HWND) m.lParam;
	break;

    case WM_NOTIFY :

	hTarget = ((NMHDR *) m.lParam) ->hwndFrom;
	break;
    };

    if (pHandler)
    {
      * pHandler = (hTarget) ? getReflectHandler (hTarget) : NULL;
    }
	return	   (hTarget) ? True : False;
};

    ..
    if (GetReflectHandler (


*/

//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
class GControl : public GWindow
{
public :
		GControl    (const void * pWndId) :
		
		GWindow (pWndId)
		{
		    m_CtrlNotify.Init (GWindow::DispatchWinMsg, this);
		};

	Bool		Create	    (GWindow *  pParent	      ,
				     HWND	hWnd	      ,
				     void *	pParam	= NULL)
			{
			    return GWindow::Create (pParent, hWnd, pParam);
			};

	Bool		Create	    (GWindow *	pParent	      ,
				     LPCTSTR    pTitle	      ,
				     DWord      dStyle	      ,
				     DWord      dExStyle      ,
				     int	x       = 0   ,
				     int	y       = 0   ,
				     int	cx      = 0   ,
				     int	cy      = 0   ,
				     void *	pParam  = NULL)
			{
			    return GWindow::Create (pParent	      , pTitle   ,
						   (dStyle | WS_CHILD), dExStyle ,
						    x, y, cx, cy, pParam);
			};
protected :

	Bool		On_Create	    (void *);
	void		On_Destroy	    ();

	Bool		On_WinMsg	    (GWinMsg &);

protected :

	    GWinMsgHandler	    m_CtrlNotify ;
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
void GControl::On_Destroy ()
{
//  m_CtrlNotify.UnRegister ();

    GWindow::On_Destroy ();
};

Bool GControl::On_Create  (void * pParam)
{
    if (! GWindow::On_Create (pParam))
    {	return False; }

//  if (! GetParent () ->Send_GM ( GM_RegisterCtrlNotify, (LPARAM) & m_CtrlNotify))
//  {	return False; }

	return True ;
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
class GStatusBar : public GControl
{
DECLARE_SYS_WINDOW_CLASS (STATUSCLASSNAME);

public :
	    GStatusBar (const void * pWndId);
protected :
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
DEFINE_SYS_WINDOW_CLASS (GStatusBar);

GStatusBar::GStatusBar (const void * pWndId) : GControl (pWndId)
{};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
class GReBar : public GControl
{
DECLARE_SYS_WINDOW_CLASS (REBARCLASSNAME);

public :
	    GReBar	(const void * pWndId);

	LONG		GetBoundRect	    ();

protected :

protected :

	Bool		DispatchWinMsg	    (GWinMsg &);

	Bool		RBN_HeightChange    (GWinMsg &, NMHDR *);
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
DEFINE_SYS_WINDOW_CLASS (GReBar);

GReBar::GReBar (const void * pWndId) : GControl (pWndId)
{};

LONG GReBar::GetBoundRect ()
{
    int	    i;
    GRect   rl = {0, 0, 0, 0};

    for (i = 0; i < Send_WM (RB_GETROWCOUNT , (WPARAM) 0, (LPARAM) 0); i ++)
    {
	rl.cy +=    Send_WM (RB_GETROWHEIGHT, (WPARAM) i, (LPARAM) 0);
    }
	return rl.cy;
};

Bool GReBar::RBN_HeightChange (GWinMsg & m, NMHDR *)
{
    GRect	     wr;

    GetWindowRect (& wr);

    return m.DispatchComplete ();
};

Bool GReBar::DispatchWinMsg (GWinMsg & m)
{
/*
    switch (m.uType)
    {
    case GWinMsg::CtrlNotify :

	printf ("Rebar notify %d Capture = %08X\n", ((NMHDR *) m.lParam) ->code, ::GetCapture ());

	DISPATCH_WM_NOTIFY_CODE (RBN_HEIGHTCHANGE   , RBN_HeightChange	, NMHDR)

	return GControl::DispatchWinMsg (m);
    };
*/
	return GControl::DispatchWinMsg (m);
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
class GToolBar : public GControl
{
DECLARE_SYS_WINDOW_CLASS (TOOLBARCLASSNAME);

public :
	    GToolBar	(const void * pWndId);
/*
	Bool		Create	    (GWindow *	pParent	      ,
				     LPCTSTR    pTitle	      ,
				     DWord      dStyle	      ,
				     DWord      dExStyle      ,
				     int	x       = 0   ,
				     int	y       = 0   ,
				     int	cx      = 0   ,
				     int	cy      = 0   ,
				     void *	pParam  = NULL)
			{
			    if (GControl::Create (pParent	    , pTitle   ,
						 (dStyle | WS_CHILD), dExStyle ,
						  x, y, cx, cy, pParam))
			    {
				Send_WM (TB_BUTTONSTRUCTSIZE, (WPARAM) sizeof (TBBUTTON), 
							      (LPARAM) 0);
				return True ;
			    };
				return False;
			};
*/

protected :

	void		On_Destroy	    ();
	Bool		On_Create	    (void * pParam);

	Bool		DispatchWinMsg	    (GWinMsg &);

protected :
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
DEFINE_SYS_WINDOW_CLASS (GToolBar);

GToolBar::GToolBar (const void * pWndId) : GControl (pWndId)
{};

Bool GToolBar::On_Create (void * pParam)
{
    if (! GControl::On_Create (pParam))
    {
	return False;
    }
	Send_WM (TB_BUTTONSTRUCTSIZE, (WPARAM) sizeof (TBBUTTON), 
				      (LPARAM) 0);
	return True ;
};

void GToolBar::On_Destroy ()
{
//  UnRegisterChildNotifyHandler ();

    GControl::On_Destroy ();
};

Bool GToolBar::DispatchWinMsg (GWinMsg & m)
{
/*
    switch (m.uType)
    {
    case GWinMsg::CtrlNotify :

	printf ("Toolb notify %d Capture = %08X\n", ((NMHDR *) m.lParam) ->code, ::GetCapture ());

	return GControl::DispatchWinMsg (m);
    };
*/
	return GControl::DispatchWinMsg (m);
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
typedef const MENUITEMTEMPLATEHEADER * LPCMENUTEMPLATEHEADER;

class GToolBarMenu : public GToolBar
{
public :
	    GToolBarMenu    (const void * pWndId);

	Bool		Create		(GWindow *  pParent	 ,
					 HMODULE    hResModule	 ,
					 LPCTSTR    pResName	 ,
					 void *	    pParam = NULL);

	LONG		GetBoundRect	();

protected :

#ifdef _MSC_BUG_C2248
public :
#endif

	struct TrackPopupContext
	{
	    int		iCurItem ;
	    Bool	bTrack	 ;
	    Bool	bPopup	 ;

//	    MENUITEMINFO    mii	;
//	    TBBUTTON	    bt	;
	};

#ifdef _MSC_BUG_C2248
protected :
#endif

protected :

	Bool		DispatchTrackPopup  (	     GWinMsg &);
static	Bool GAPI	DispatchTrackPopup  (void *, GWinMsg &);

static	LRESULT GAPI	GetMessageHook	    (TrackPopupContext *, int	 cCode ,
								  WPARAM wParam,
								  LPARAM lParam);
	void		DoTrackPopupMenu    (int, Bool);
static 	void GAPI	DoTrackPopupMenu    (void * oDsp, void * pContext)
			{
			    ((GToolBarMenu *) oDsp) ->DoTrackPopupMenu ((int )(LOWORD((DWORD)pContext)),
									(Bool)(HIWORD((DWORD)pContext)));
			};

	Bool		DispatchWinMsg	    (GWinMsg &);

	Bool		TBN_DropDown	    (GWinMsg &, NMTOOLBAR   *);
	Bool		TBN_HotItemChange   (GWinMsg &, NMTBHOTITEM *);

protected :

	    GWindow *		m_pThunked  ;

	    HMENU		m_hMenu	    ;
	    int			m_nMenuItem ;

private :
/*
	    int			m_iCurItem  ;

	    Bool		m_bTracking ;
	    Bool		m_bBreak    ;
	    DWord		m_vKey	    ;
*/
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
GToolBarMenu::GToolBarMenu (const void * pWndId) : GToolBar (pWndId)
{
    m_pThunked	= NULL	;
    m_hMenu	= NULL	;
    m_nMenuItem = 0	;
/*
    m_iCurItem	= -1	;
    m_bTracking	= 
    m_bBreak	= False ;
    m_vKey	= 0	;
*/
};

LONG GToolBarMenu::GetBoundRect ()
{
    int	    i = 0;
    GRect   rl, rr;

	Send_WM (TB_GETITEMRECT,(WPARAM) i ++, (LPARAM) & rl);

    for (; i < m_nMenuItem; i ++)
    {
	Send_WM (TB_GETITEMRECT,(WPARAM) i,    (LPARAM) & rr);
    
	if (rl.cx < rr.cx) { rl.cx = rr.cx; }
	if (rl.cy < rr.cy) { rl.cy = rr.cy; }
    }
	rl.cx += rl.x;
	rl.cy += rl.y;

	Send_WM (TB_GETMAXSIZE, (WPARAM) 0, (LPARAM) & rr.Size ());

    printf ("Bound rect %d.%d : %d.%d (%d) {Max size %d.%d}\n", rl.x, rl.y, rl.cx, rl.cy,
								Send_WM (TB_GETROWS, (WPARAM) 0, (LPARAM) 0),
								rr.cx, rr.cy);

    return rl.cy;
};

Bool GToolBarMenu::Create (GWindow * pParent   ,
			   HMODULE   hResModule,
			   LPCTSTR   pResName  ,
			   void *    pParam    )
{
    if (! GToolBar::Create (pParent, NULL, WS_VISIBLE 
					 | CCS_NOPARENTALIGN | CCS_NORESIZE
					 | CCS_TOP | TBSTYLE_FLAT | TBSTYLE_LIST
					 | CCS_NODIVIDER | TBSTYLE_WRAPABLE,
					   0,
					   0, 0, 0, 0, pParam))
    {	return False;}
/*
    Send_WM (TB_SETEXTENDEDSTYLE, (WPARAM) 0, (LPARAM) (TBSTYLE_EX_HIDECLIPPEDBUTTONS |
    Send_WM (TB_GETEXTENDEDSTYLE, (WPARAM) 0, (LPARAM) 0)));
*/
/*
    if (GetHWND () || (NULL == pMenuOwner) || (NULL == pMenuOwner ->GetHWND    ()) 
					   || (NULL == pMenuOwner ->GetWndProc ()))
    {
	return False;
    }
*/

    LPCMENUTEMPLATEHEADER pTemplate = 
   (LPCMENUTEMPLATEHEADER) ::LoadResource (hResModule, pResName, RT_MENU);

    if ((NULL == pTemplate) || (NULL == (m_hMenu = ::LoadMenuIndirect (pTemplate))))
    {	return False;}


    int		    i  ;
    MENUITEMINFO    mii;
    TBBUTTON	    bt ;
    TCHAR	    buf [128];

    m_nMenuItem = ::GetMenuItemCount (m_hMenu);
/*
    MENUINFO	    mi ;
    mi.cbSize = sizeof (mi);
    mi.fMask  = MIM_MAXHEIGHT;
    ::GetMenuInfo (m_hMenu, & mi);
    mi.cyMax  = 16;
    mi.fMask |= MIM_APPLYTOSUBMENUS;
    ::SetMenuInfo (m_hMenu, & mi);
*/
    for (i = 0; i < m_nMenuItem; i ++)
    {
	mii.cbSize	= sizeof (mii);
	mii.fMask	= MIIM_TYPE | MIIM_STATE | MIIM_ID;
	mii.cch		= sizeof (buf)/sizeof (TCHAR);
	mii.dwTypeData  = buf;

	::GetMenuItemInfo (m_hMenu, i, True, & mii);

	bt.iBitmap	= I_IMAGENONE ;
	bt.idCommand	= mii.wID     ;	// == iItem
	bt.fsState	= (MFS_DISABLED & mii.fState) ? 0 : TBSTATE_ENABLED;
	bt.fsStyle	= BTNS_BUTTON | BTNS_DROPDOWN | BTNS_AUTOSIZE;
#ifdef _WIN32
	bt.bReserved[0] =
	bt.bReserved[1] = 0;
#else
#error	Not supported...
#endif
	bt.dwData	= i;
	bt.iString	= (INT_PTR) buf;

    if (! Send_WM (TB_INSERTBUTTON, (WPARAM) i, (LPARAM) & bt))
    {   
	return False;
    }
    }

    GRect wr;

    GetWindowRect (& wr);


	Send_WM	(TB_AUTOSIZE    , (WPARAM) 0, (LPARAM) 0);

    GetWindowRect (& wr);


    GetBoundRect ();


    return True;



//	SetStyle (CCS_NORESIZE | GetStyle ());
#if FALSE

    GRect w;

    GetWindowRect (& w);

    printf ("Menu bar size : (%08X) = %d.%d\n", GetHWND (), w.cx, w.cy);

	::SetWindowPos (GetHWND ()      ,
		        NULL	        ,
			0, 0, w.cx, w.cy,
		        SWP_NOZORDER 	);

    GetWindowRect (& w);

    printf ("Menu bar size : (%08X) = %d.%d\n", GetHWND (), w.cx, w.cy);

    return True;


//	SetStyle (0x984D/*CCS_NORESIZE*/ | GetStyle ());

    GetWindowRect (& w);

    printf ("Menu bar size : %d.%d\n", w.cx, w.cy);

	Send_WM  (TB_GETMAXSIZE  , (WPARAM) 0, (LPARAM) & w.Size ());

	::SetWindowPos (GetHWND ()      ,
		        NULL	        ,
			0, 0, w.cx, w.cy,
		        SWP_NOZORDER 
		      | SWP_NOACTIVATE 
		      | SWP_NOMOVE    
		      | SWP_NOCOPYBITS
		      | SWP_NOREDRAW	);

    w.cx -= 2;

    GetWindowRect (& w);

    printf ("Menu bar size : %d.%d\n", w.cx, w.cy);

	return True ;
#endif
};

Bool GToolBarMenu::DispatchTrackPopup (GWinMsg & m)
{
    GPoint	p  ;

//	printf ("SelfMessage %08X: %08X\n", m.hWnd ,
//					    m.uMsg );
/*
    return m.DispatchDefault ();

lBreak:

    if (m_bBreak)
    {
	m_bTracking = True ;
	m_bBreak    = False;

    printf ("X:Break with %d\n", m_iCurItem);

	::SendMessage (m_pThunked ->GetHWND (), WM_CANCELMODE, (WPARAM) 0, (LPARAM) 0);
    }
    if (m_bTracking)
    {
        return m.DispatchDefault ();
    }
*/
/*
    if ((WM_MENUSELECT	    == m.uMsg))
    {
	    m_vKey &= (~0x03);

	if (HIWORD(m.wParam) & MF_POPUP)
	{
    	    printf ("Pupup selected \n");
	}
	else
	{
	    m_vKey |= ( 0x02);
	    printf ("Menu item \n");
	}
    }
*/
/*
    if ((WM_UNINITMENUPOPUP == m.uMsg)
    ||  (WM_INITMENUPOPUP   == m.uMsg))
    {
    if  (WM_INITMENUPOPUP   == m.uMsg)
    {
    printf ("Pupup %08X\n", m.wParam);
    }

    printf ("Clear VK....\n");

	m_vKey	&= (~0x05);

	return m.DispatchDefault ();
    }
    
    if (m_vKey & 0x05)
    {
	if (m_vKey & 0x04)
	{
	    if (0 > -- m_iCurItem)
	    {
		m_iCurItem  = m_nMenuItem - 1;
	    }
	}
	if (m_vKey & 0x01)
	{
	    if (m_nMenuItem <= ++ m_iCurItem)
	    {
		m_iCurItem  = 0;
	    }
	}

	m_bBreak  = True;
	m_vKey   &= (~0x05);

	Post_WM (WM_KEYDOWN, VK_DOWN, 0);

	goto lBreak;
    }
*/
    if (WM_ENTERIDLE == m.uMsg)
    {
/*
    while (True)
    {
    printf ("Wait <\n");

	GWinMsgLoop::Wait ();

    printf ("> Wait\n");

	if (::PeekMessage (& msg, m_pThunked ->GetHWND (), 0, 0, PM_NOREMOVE))
	{
	    if (WM_MOUSEMOVE == msg.message)
	    {
		p.Assign (msg.pt.x, msg.pt.y); ScreenToClient (& p);

		i = Send_WM (TB_HITTEST, (WPARAM) 0, (LPARAM) & p);

		if ((0 <= i) && (i != m_iCurItem) && (i < m_nMenuItem))
		{
		    m_iCurItem = i   ;
		    m_bBreak   = True;

	printf ("Set MK.... %d\n", m_iCurItem);
		}
	    }
	    else 
	    if (WM_KEYDOWN == msg.message)
	    {
	    if ((VK_LEFT   == msg.wParam ) && (m_vKey & 0x08)) { m_vKey |= 0x04;}
	    if ((VK_RIGHT  == msg.wParam ) && (m_vKey & 0x02)) { m_vKey |= 0x01;}
	    }
	    return m.DispatchComplete ();
	}
    };
*/
    }
	    return m.DispatchDefault ();
};

Bool GAPI GToolBarMenu::DispatchTrackPopup (void * oDsp, GWinMsg & m)
{
    return ((GToolBarMenu *) oDsp) ->DispatchTrackPopup (m);
};

LRESULT GAPI GToolBarMenu::GetMessageHook (TrackPopupContext * tp, int	  nCode ,
								   WPARAM wParam,
								   LPARAM lParam)
{
    if ((HC_ACTION == nCode) && (PM_REMOVE == wParam) && (NULL != lParam))
    {
    printf ("GetMsgHook:: %08X, %08X %08X %08X\n", ((MSG *) lParam) ->hwnd   ,
						   ((MSG *) lParam) ->message,
						   ((MSG *) lParam) ->wParam ,
						   ((MSG *) lParam) ->lParam );
//	tp ->bTrack = False;

    }
    return ::CallNextHookEx (NULL, nCode, wParam, lParam);
};

void GToolBarMenu::DoTrackPopupMenu (int iFromItem, Bool bPopup)
{
    GWndProcThunk * pth;
    RECT	    r  ;

    MENUITEMINFO    mii;
    TBBUTTON	    bt ;
    TPMPARAMS	    tpm;
    MSG		    msg;

    TrackPopupContext  tc = { iFromItem, True, bPopup };
    HHOOK	       hh = NULL;
    GWndHookThunk    * th = new GWndHookThunk
			    ((GWinHookProc) GetMessageHook, & tc);

	    printf ("TrackPupup <<<\n");

    if (NULL != 
       (hh    = ::SetWindowsHookEx (WH_GETMESSAGE, 
				   (HOOKPROC) th, NULL, TCurrent () ->GetId ())))
    {
	do
	{
	    if (tc.bPopup)
	    {
	    }
	    else
	    {
	do
	{
	    if (::GetMessage (& msg, NULL, 0, 0))
	    {
		::TranslateMessage (& msg);
		::DispatchMessage  (& msg);
	    }

	} while (tc.bTrack && (! tc.bPopup));

	    }

	} while (tc.bTrack);

		::UnhookWindowsHookEx (hh);
    }

    delete (th);

	    printf ("TrackPopup >>>\n");















//	m_pTrackPopupContext = & tc ;

/*
    if (NULL == (pth = m_pThunked ->GetWndProc ()))
    {	return; }

GWinMsgLoop::BegModalLoop ();
*/



//  m_pThunked ->SubclassWndProc (DispatchTrackPopup, this);

#if FALSE

    do
    {
//	    m_vKey	= 0x0A ;
//	    m_bBreak	=
//	    m_bTracking = False;

    if (Send_WM (TB_GETBUTTON  , (WPARAM)   iCurItem  , (LPARAM) & bt))
    {
	Send_WM (TB_GETRECT    , (WPARAM) bt.idCommand, (LPARAM) & r );

	ClientToScreen	      (& r);

r.top	 += 10;
r.bottom += 10;


	tpm.cbSize   = sizeof (tpm);

	GetClientRect	    (& tpm.rcExclude);
	ClientToScreen	    (& tpm.rcExclude);

			       tpm.rcExclude.bottom = r.bottom;

	    printf ("[%d.%d]:[%d.%d]\n", tpm.rcExclude.left ,
					 tpm.rcExclude.right,
					 tpm.rcExclude.top  ,
					 tpm.rcExclude.bottom);


	mii.cbSize   = sizeof (mii);
	mii.fMask    = MIIM_SUBMENU;
	mii.hSubMenu = NULL;

	::GetMenuItemInfo (m_hMenu, bt.idCommand, False, & mii);

	if (mii.hSubMenu)
	{
	    printf ("ToolBarMenu : DropDown %d %d.%d\n", iCurItem, r.left, r.bottom);

//	    Send_WM (TB_PRESSBUTTON, (WPARAM) bt.idCommand, (LPARAM) True );

	    ::TrackPopupMenuEx (mii.hSubMenu, 
				TPM_LEFTBUTTON | TPM_VERTICAL | TPM_LEFTALIGN | TPM_TOPALIGN,
				r.left, r.bottom, m_pThunked ->GetHWND (), & tpm);

//	    Send_WM (TB_PRESSBUTTON, (WPARAM) bt.idCommand, (LPARAM) False);
	}
/*
	if (m_bTracking)
	{   
	    continue;
	}
*/
	    break;
    }

    } while (True);

#endif

//  m_pThunked ->UnSubclassWndProc ();



/*
	pth ->SetDispatchProc (m_pOrgProc , 
			       m_oOrgProc );
	m_pOrgProc = NULL;
	m_oOrgProc = NULL;
*/

/*
GWinMsgLoop::EndModalLoop ();
*/
};

Bool GToolBarMenu::TBN_DropDown (GWinMsg & m, NMTOOLBAR * tb)
{
    int		   i   ;
    TBBUTTONINFO   bti ;

    bti.cbSize	 = sizeof (bti);
    bti.dwMask	 = 0	       ;

    printf ("Drop Down (%08X) %d item\n", tb ->iItem, tb ->iItem);

    i = Send_WM (TB_GETBUTTONINFO, (WPARAM) tb ->iItem, (LPARAM) & bti);

    if ((0 <= i) && (i < m_nMenuItem))
    {{
//	DoTrackPopupMenu (i);

//	GWinMsgLoop::SetPostProc  (DoTrackPopupMenu, this, (void *) ((False << 16) | i));

	return m.DispatchComplete (TBDDRET_DEFAULT);
    }}
	return m.DispatchComplete (TBDDRET_NODEFAULT);

//  printf ("Drop Down %d item\n", i);

#if FALSE

    TBBUTTONINFO	   bti ;

    bti.cbSize	 = sizeof (bti);
    bti.dwMask	 = 0	       ;


//  ::SetFocus (GetHWND ());


    m_iCurItem	 = Send_WM (TB_GETBUTTONINFO, (WPARAM) tb ->iItem, (LPARAM) & bti);

    if ((0 <= m_iCurItem) && (m_iCurItem < m_nMenuItem))
    {
//		   Send_WM (TB_SETHOTITEM   ,  (WPARAM) -1	 , (LPARAM) 0);

//	GWinMsgLoop::SetPostProc (TrackPopupProc, this);

	TrackPopupProc ();
    }

    return m.DispatchComplete (TBDDRET_DEFAULT);

#endif
};

Bool GToolBarMenu::TBN_HotItemChange (GWinMsg & m, NMTBHOTITEM * hi)
{
    printf ("HotItem change..%d -> %d\n", hi ->idOld, hi ->idNew);

//  return m.DispatchComplete (-1);

    return m.DispatchComplete (0);

    return m.DispatchComplete (IsActive () ? 0 : -1);
};

Bool GToolBarMenu::DispatchWinMsg (GWinMsg & m)
{
/*
    switch (m.uType)
    {
    case GWinMsg::CtrlNotify :

	DISPATCH_WM_NOTIFY_CODE (TBN_DROPDOWN	   , TBN_DropDown	, NMTOOLBAR  )
	DISPATCH_WM_NOTIFY_CODE (TBN_HOTITEMCHANGE , TBN_HotItemChange	, NMTBHOTITEM)

	return GToolBar::DispatchWinMsg (m);
    };
*/
	return GToolBar::DispatchWinMsg (m);
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
class GTreeView : public GControl
{

DECLARE_SYS_WINDOW_CLASS (WC_TREEVIEW);

public :

	    GTreeView (const void * pWndId) :

	    GControl  (pWndId)
	    {
		m_SelItem.mask  = 0;
	      *	m_SelItemText	= 0;
	    };

	DECLARE_INTERFACE_ (IItem, XHandle)
	{
	STDMETHOD_	(void,		    Destroy)() PURE;
	};


	HTREEITEM	InsertItem	    (HTREEITEM	    hParent	       ,
					     HTREEITEM	    hInsertAfter       ,
					     LPCTSTR	    pItemName	       ,
				       const void *	    pItemContext = NULL);
/*
					    hParent : 
					    NULL	    - Root

					    hInsertAfter : 
					    TVI_ROOT
					    TVI_FIRST
					    TVI_LAST
					    TVI_SORT

					    pItemName : 
					    NULL	    - LPSTR_TEXTCALLBACK used !!!
*/
	void 		DeleteItem	    (HTREEITEM	    hItem  )
			{
			    Send_WM (TVM_DELETEITEM, (WPARAM) 0, (LPARAM) hItem);
			};

	HTREEITEM	GetItem		    (HTREEITEM	    hFrom  ,
					     UINT	    uFlags )
			{
			    return (HTREEITEM) 

			    Send_WM (TVM_GETNEXTITEM, (WPARAM) uFlags, (LPARAM) hFrom);
			};
/*					    GetItem::uFlags :

					    TVGN_CARET
					    TVGN_CHILD
					    TVGN_DROPHILITE
					    TVGN_FIRSTVISIBLE
					    TVGN_LASTVISIBLE
					    TVGN_NEXT
					    TVGN_NEXTVISIBLE
					    TVGN_PARENT
					    TVGN_PREVIOUS
					    TVGN_PREVIOUSVISIBLE
					    TVGN_ROOT
*/
	Bool		Expand		    (HTREEITEM	    hItem)
			{
			    return (Bool) Send_WM (TVM_EXPAND, (WPARAM) TVE_EXPAND, (LPARAM) hItem);
			};

	Bool		SelectItem	    (HTREEITEM	    hItem	       ,
					     UINT	    uFlags = TVGN_CARET)
			{
			    return (Bool) Send_WM (TVM_SELECTITEM, (WPARAM) uFlags, (LPARAM) hItem);
			};
/*
					    SelectItem::uFlags :

					    TVGN_CARET
					    TVGN_DROPHILITE
					    TVGN_FIRSTVISIBLE
					    TVSI_NOSINGLEEXPAND
*/

	const TCHAR *	GetSelItemText	    () { return m_SelItemText;};


protected :

	Bool		On_Create	    (void * pParam);
	void		On_Destroy	    ();

	Bool		TVN_GetDispInfo	    (GWinMsg &, NMTVDISPINFO *);
	Bool		TVN_SelChanging	    (GWinMsg &, NMTREEVIEW   *);
	Bool		TVN_SelChanged	    (GWinMsg &, NMTREEVIEW   *);
	Bool		TVN_DeleteItem	    (GWinMsg &, NMTREEVIEW   *);

	Bool		DispatchWinMsg	    (GWinMsg &);

protected :

	    TVITEM		    m_SelItem		;
	    TCHAR		    m_SelItemText [256]	;
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
DEFINE_SYS_WINDOW_CLASS (GTreeView);

Bool GTreeView::On_Create (void * pParam)
{
    if (! GControl::On_Create (pParam))
    {
	return False;
    }
	return True ;
};

void GTreeView::On_Destroy ()
{
    printf ("Destroy TreeView\n");

    GControl::On_Destroy ();
};

HTREEITEM GTreeView::InsertItem (HTREEITEM  hParent	 ,
				 HTREEITEM  hInsertAfter ,
				 LPCTSTR    pItemName	 ,
			   const void *	    pItemContext )
{
    TV_INSERTSTRUCT	  i			;

    i.hParent		= hParent		;
    i.hInsertAfter	= hInsertAfter		;

    i.item.mask		= 0		        ;

    i.item.mask		= TVIF_PARAM
			| TVIF_TEXT 
    			| TVIF_IMAGE
			| TVIF_SELECTEDIMAGE
			| TVIF_CHILDREN		;

    i.item.lParam	= (LPARAM) pItemContext ;   // TVIF_PARAM
						    //
    i.item.pszText	=	  (pItemName)
			? (LPTSTR) pItemName
			: LPSTR_TEXTCALLBACK	;   // TVIF_TEXT
    i.item.cchTextMax	= 0			;   //

    i.item.iImage	=			    // TVIF_IMAGE
    i.item.iSelectedImage			    //
			= I_IMAGECALLBACK	;

    i.item.cChildren	= I_CHILDRENCALLBACK	;   // TVIF_CHILDREN
						    //

    return (HTREEITEM) Send_WM (TVM_INSERTITEM, (WPARAM) 0,(LPARAM) & i);
};

Bool GTreeView::TVN_GetDispInfo (GWinMsg & m, NMTVDISPINFO * di)
{
    if (TVIF_TEXT  & di ->item.mask)
    {}
    if (TVIF_IMAGE & di ->item.mask)
    {}
    if (TVIF_SELECTEDIMAGE & di ->item.mask)
    {}
    if (TVIF_CHILDREN & di ->item.mask)
    {}
    return m.DispatchComplete ();
};

Bool GTreeView::TVN_DeleteItem (GWinMsg & m, NMTREEVIEW * tv)
{
    printf ("Delteed TreeView item ...\n");

    return m.DispatchComplete ();
};

Bool GTreeView::TVN_SelChanging (GWinMsg & m, NMTREEVIEW * tv)
{
    return m.DispatchComplete ();
};

Bool GTreeView::TVN_SelChanged (GWinMsg & m, NMTREEVIEW * tv)
{
	m_SelItem.mask  = 0;
      *	m_SelItemText	= 0;

    if (tv ->itemNew.state & TVIS_SELECTED)
    {
	m_SelItem.mask    = TVIF_HANDLE
			 |  TVIF_PARAM
		         |  TVIF_TEXT 
    		         |  TVIF_IMAGE
		         |  TVIF_SELECTEDIMAGE
		         |  TVIF_CHILDREN	;

	m_SelItem.mask	 |= TVIF_STATE		;

	m_SelItem.hItem	  = tv ->itemNew.hItem	;

	m_SelItem.cchTextMax 
			  = sizeof (m_SelItemText) / sizeof (TCHAR);

	m_SelItem.pszText = m_SelItemText	;

	Send_WM (TVM_GETITEM, (WPARAM) 0, (LPARAM) & m_SelItem);

    {
	GM_LookAtChild_Param p = {this, 0x00000001};

	GetParent () ->Send_GM (GM_LookAtChild, (LPARAM) & p);
    }
	printf ("Selected {%s}\n", m_SelItem.pszText);
    }

    return m.DispatchComplete ();
};

Bool GTreeView::DispatchWinMsg (GWinMsg & m)
{
/*
    switch (m.uType)
    {
    case GWinMsg::CtrlNotify :
//  case GWinMsg::RefletNotify :

	DISPATCH_WM_NOTIFY_CODE	(TVN_GETDISPINFO    , TVN_GetDispInfo	, NMTVDISPINFO	)
//	DISPATCH_WM_NOTIFY_CODE	(TVN_SELCHANGING    , TVN_SelChanging	, NMTREEVIEW	)
	DISPATCH_WM_NOTIFY_CODE	(TVN_SELCHANGED	    , TVN_SelChanged	, NMTREEVIEW	)
	DISPATCH_WM_NOTIFY_CODE	(TVN_DELETEITEM	    , TVN_DeleteItem	, NMTREEVIEW	)

	return GControl::DispatchWinMsg (m);
    }
*/
	return GControl::DispatchWinMsg (m);
};
















/*
HTREEITEM GTreeView::InsertItem (HTREEITEM  hParent	 ,
				 HTREEITEM  hInsertAfter ,
				 int	    sItemNameMax ,
			   const void *	    pItemContext )
{
    TV_INSERTSTRUCT	  i			;

    i.hParent		= hParent		;
    i.hInsertAfter	= hInsertAfter		;

    i.item.mask		= TVIF_PARAM
			| TVIF_TEXT 
    			| TVIF_IMAGE
			| TVIF_SELECTEDIMAGE
			| TVIF_CHILDREN		;

    i.item.pszText	= LPSTR_TEXTCALLBACK	;
    i.item.cchTextMax	= sItemNameMax		;
    i.item.iImage	= 
    i.item.iSelectedImage 
			= I_IMAGECALLBACK	;
    i.item.cChildren	= I_CHILDRENCALLBACK	;

    i.item.lParam	= (LPARAM) pItemContext ;

    return (HTREEITEM) SendWinMsg (TVM_INSERTITEM , (WPARAM) 0  ,
						    (LPARAM) & i);
};
*/
//
//[]------------------------------------------------------------------------[]
















//[]------------------------------------------------------------------------[]
// Dialog template wrapper
//
//
#pragma pack (1)

    typedef struct
    {
	DWORD	    style	;
	DWORD	    exStyle	;
	short	    x		;
	short	    y		;
	short	    cx		;
	short	    cy		;
	WORD	    id		;
	WORD	    reserved	;
//	sz_Or_Ord   windowClass	;
//	sz_Or_Ord   title	;
//	WORD	    extraCount	;
    } GDLGITEMTEMPLATE;

    typedef struct
    { 
	DWORD	    helpID	;
	DWORD	    exStyle	;
	DWORD	    style	;
	short	    x		;
	short	    y		;
	short	    cx		;
	short	    cy		;
	WORD	    id		;
	WORD	    resereved	;
//	DWORD	    id		;
//	sz_Or_Ord   windowClass	;
//	sz_Or_Ord   title	;
//	WORD	    extraCount	;

    } GDLGITEMTEMPLATEEX; 

    typedef struct
    {
	DWORD	    style	;
	DWORD	    exStyle	;
	WORD	    cDlgItems	;
	short	    x		;
	short	    y		;
	short	    cx		;
	short	    cy		;
//	sz_Or_Ord   menu		;
//	sz_Or_Ord   windowClass		;
//	WCHAR	    title    [titleLen] ;
    } GDLGTEMPLATE;

    typedef struct
    { 
        WORD	    dlgVer	;
        WORD	    signature	;
	DWORD	    helpID	;
	DWORD	    exStyle	;
	DWORD	    style	;
	WORD	    cDlgItems	;
	short	    x		;
	short	    y		;
	short	    cx		;
	short	    cy		;
//	sz_Or_Ord   menu		;
//	sz_Or_Ord   windowClass		;
//	WCHAR	    title    [titleLen] ;
//	WORD	    pointsize		; if (DS_SETFONT or DS_SHELLFONT)
//	WORD	    weight		; if (DS_SETFONT or DS_SHELLFONT)
//	BYTE	    italic		; if (DS_SETFONT or DS_SHELLFONT)
//	BYTE	    charset		; if (DS_SETFONT or DS_SHELLFONT)
//	WCHAR	    typeface [stringLen];
    } GDLGTEMPLATEEX;

    typedef struct
    {
	WORD	    pointsize	;
	WORD	    weight	;
	BYTE	    italic	;
	BYTE	    charset	;
	WCHAR	    typeface [1];

    } GDLGTEMPLATEEX_FONT;

#pragma pack () 

static const void * __skip_sz_Or_Ord (const void * p)
{
    if	    (0x0000 == * (WORD *) p)
    {
	((WORD  *&) p) += 1;
    }
    else if (0xFFFF == * (WORD *) p)
    {   
	((WORD  *&) p) += 2;
    }
    else
    {   
	((WCHAR *&) p) += (1 + ::lstrlenW ((WCHAR *) p));
    }
	return p;
};

static const GDLGITEMTEMPLATE * __getDlgItemTemplate (const GDLGTEMPLATE * t, int nCtrlId)
{
    const void * p;
    int	      j, i;

    {
	p = __skip_sz_Or_Ord (t + 1);	// menu
	p = __skip_sz_Or_Ord (p	   );	// windowClass
	p = __skip_sz_Or_Ord (p	   );	// title
    }
    for (i = 0; i < t ->cDlgItems; i ++)
    {
	if (nCtrlId == ((const GDLGITEMTEMPLATE *) p) ->id)
	{
	    return	(const GDLGITEMTEMPLATE *) p;
	}
	p = __skip_sz_Or_Ord (((const GDLGITEMTEMPLATE *) p) + 1);
					// windowClass
	p = __skip_sz_Or_Ord (p	    );	// title
	j = 2 + 
	    * (const WORD  *)(p	    );	// extraCount
	     ((const BYTE *&) p) += j;

	p =   (const void * ) roundup (p, sizeof (DWORD));
    }
	    return NULL;
};

#ifndef DS_SHELLFONT
#define DS_SHELLFONT (DS_SETFONT | DS_FIXEDSYS)
#endif

static const GDLGITEMTEMPLATEEX * __getDlgItemTemplate (const GDLGTEMPLATEEX * e, int nCtrlId)
{
    const void * p;
    int	      j, i;

    {
	p = __skip_sz_Or_Ord (e + 1);	// menu
	p = __skip_sz_Or_Ord (p	   );	// windowClass
	p = __skip_sz_Or_Ord (p	   );	// title

	if ((DS_SETFONT | DS_SHELLFONT) & e ->style)
	{				
					// pointsize, weight, italic, charset
	    ((const Byte *&) p) += (2 + 2 + 1 + 1);

	p = __skip_sz_Or_Ord (p	   );
	}
    }
    for (i = 0; i < e ->cDlgItems; i ++)
    {
	if (nCtrlId == ((const GDLGITEMTEMPLATEEX *) p) ->id)
	{
	    return	(const GDLGITEMTEMPLATEEX *) p;
	}

	p = __skip_sz_Or_Ord (((const GDLGITEMTEMPLATEEX *) p) + 1);
					// windowClass
	p = __skip_sz_Or_Ord (p	    );	// title
	j = 2 + 
	    * (const WORD  *)(p	    );	// extraCount
	     ((const BYTE *&) p) += j;

	p =   (const void * ) roundup (p, sizeof (DWORD));
    }
	    return NULL;
};

Bool WINAPI GetDlgTemplateItemInfo (LPCDLGTEMPLATE pTemplate, int       nCtrlId	 ,
							      DWORD   * pStyle   ,
							      DWORD   * pExStyle ,
							      GRect   * pRect	 ,
							      LPCWSTR * pTitle  = NULL,
							      LPCWSTR * pClass	= NULL,
							      DWORD   * pHelpId = NULL)
{
    union
    {
	 const GDLGTEMPLATEEX *	 e;
	 const GDLGTEMPLATE   *  t;
    };
    union
    {
    const GDLGITEMTEMPLATEEX  * ie;
    const GDLGITEMTEMPLATE    * it;
    };
	 const void *		 p;

    p = e = (const GDLGTEMPLATEEX *) pTemplate;

    if (e)
    {
	if (0xFFFF != (e ->signature))	// This is the GDLGTEMPLATE record
	{				//
	    if (NULL != (p = it = __getDlgItemTemplate (t, nCtrlId)))
	    {
		if (pHelpId)
		{
		  * pHelpId  = NULL;
		}
		if (pStyle  )
		{
		  * pStyle   = it ->style  ;
		}
		if (pExStyle)
		{
		  * pExStyle = it ->exStyle;
		}
		if (pRect   )
		{
		    pRect ->Assign (it ->x, it ->y, it ->cx, it ->cy);
		}

		if (pClass  )
		{
		  * pClass   = (LPCWSTR) p;
		}
		p = __skip_sz_Or_Ord (p);

		if (pTitle  )
		{
		  * pTitle   = (LPCWSTR) p;
		}
		p = __skip_sz_Or_Ord (p);

		return True;
	    }
	}
	else				// This is the GDLGTEMPLATEEX record
	{				//
	    if (NULL != (p = ie = __getDlgItemTemplate (e, nCtrlId)))
	    {
		if (pHelpId)
		{
		  * pHelpId  = ie ->helpID ;
		}
		if (pStyle  )
		{
		  * pStyle   = ie ->style  ;
		}
		if (pExStyle)
		{
		  * pExStyle = ie ->exStyle;
		}
		if (pRect   )
		{
		    pRect ->Assign (ie ->x, ie ->y, ie ->cx, ie ->cy);
		}

		if (pClass  )
		{
		  * pClass   = (LPCWSTR) p;
		}
		p = __skip_sz_Or_Ord (p);

		if (pTitle  )
		{
		  * pTitle   = (LPCWSTR) p;
		}
		p = __skip_sz_Or_Ord (p);


		return True;
	    }
	}
    }
	return False;
};

Bool WINAPI GetDlgTemplateInfo (LPCDLGTEMPLATE pTemplate, DWORD   * pStyle   ,
							  DWORD   * pExStyle ,
							  GRect   * pRect    ,
							  LPCWSTR * pTitle    = NULL,
							  LPCWSTR * pClass    = NULL,
							  LPCVOID * pMenuOrId = NULL,
						    const GDLGTEMPLATEEX_FONT **
								    pFont     = NULL)
{
    union
    {
	 const GDLGTEMPLATEEX *	 e;
	 const GDLGTEMPLATE   *  t;
    };
	 const void *		 p;

    p = e = (const GDLGTEMPLATEEX *) pTemplate;

    if (e)
    {
	if (0xFFFF != (e ->signature))	// This is the GDLGTEMPLATE record
	{				//
	    if (pStyle	 ) 
	    {
	      * pStyle    = t ->style	 ;
	    }
	    if (pExStyle )
	    {
	      * pExStyle  = t ->exStyle;
	    }
	    if (pRect	 ) 
	    {
		pRect ->Assign (t ->x, t ->y, t ->cx, t ->cy);
	    }

	    if (pMenuOrId)
	    {
	      * pMenuOrId = t + 1;
	    }
	    p = __skip_sz_Or_Ord (t + 1);

	    if (pClass	 )
	    {
	      * pClass = (LPCWSTR) p;
	    }
	    p = __skip_sz_Or_Ord (p    );

	    if (pTitle	 )
	    {
	      * pTitle = (LPCWSTR) p;
	    }
	    p = __skip_sz_Or_Ord (p    );

	    if (pFont	 )
	    {
	      * pFont  = NULL;
	    }
	}
	else				// This is the GDLGTEMPLATEEX record
	{				//
	    if (pStyle	 ) 
	    {
	      * pStyle    = e ->style	 ;
	    }
	    if (pExStyle )
	    {
	      * pExStyle  = e ->exStyle;
	    }
	    if (pRect	 ) 
	    {
		pRect ->Assign (e ->x, e ->y, e ->cx, e ->cy);
	    }

	    if (pMenuOrId)
	    {
	      * pMenuOrId = e + 1;
	    }
	    p = __skip_sz_Or_Ord (e + 1);

	    if (pClass	 )
	    {
	      * pClass = (LPCWSTR) p;
	    }
	    p = __skip_sz_Or_Ord (p    );

	    if (pTitle	 )
	    {
	      * pTitle = (LPCWSTR) p;
	    }
	    p = __skip_sz_Or_Ord (p    );

	    if ((DS_SETFONT | DS_SHELLFONT) & e ->style)
	    {
		if  (pFont	    )
		{
		   * pFont  = (const GDLGTEMPLATEEX_FONT *) p;
		}
		((const Byte *&) p) += (2 + 2 + 1 + 1);

		p = __skip_sz_Or_Ord (p    );
	    }
	    else if (pFont)
	    {
		   * pFont  = NULL;
	    }
	}
	    return True;
    }
	    return False;
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
class GDialog : public GParentWindow
{
DECLARE_SYS_WINDOW_CLASS (WC_DIALOG);
//[]------------------------------------------------------------------------[]
//  Windows Dialog class subclassing :
/*
    protected :
	    GWindowClass &  GetWindowClass  () const
	    {
		    return s_WndClass;
	    };
    private :
    static  LPCTSTR GAPI    GetWindowClassInfo	(WNDCLASSEX * wc = NULL)
	    {
		    LPCTSTR pName	= _T("GWin.Dialog");

		if (wc)
		{
		    ::GetWindowClassInfo (WC_DIALOG, wc);

		    wc ->lpfnWndProc	= _initWndProc	;
		    wc ->lpszClassName	= pName		;

		    wc ->cbWndExtra	= DLGWINDOWEXTRA;
		}
		    return  pName;
	    };
    static  GWindowClass    s_WndClass;
*/
//  Windows Dialog class end subclassing
//[]------------------------------------------------------------------------[]
//
public :

	    GDialog	   (const void * pWndId) :

	    GParentWindow  (pWndId)
	    {
		m_pDlgProc  = NULL;
	    };

	   ~GDialog	();

	GDlgProcThunk *	GetDlgProc	() const { return m_pDlgProc;};

	Bool		Create		(GWindow *  pParent	 ,
					 HMODULE    hResModule	 ,
					 LPCTSTR    pResName	 ,
					 void *	    pParam = NULL);

	void		SubclassDlgProc	    (GWinMsgProc pDspProc, void * oDspProc)
			{
			    // GASSERT (GetHWND ())
			    //
			    ::SetWindowLongPtr (GetHWND (),   DWLP_DLGPROC,
							      (LONG_PTR) 
			    (m_pDlgProc	=  new  GDlgProcThunk (pDspProc   , 
						oDspProc  ,   (DLGPROC )
			    ::GetWindowLongPtr (GetHWND (),   DWLP_DLGPROC))));
			};

	void		UnSubclassDlgProc   ()
			{
			    // GASSERT (GetHWND ())
			    //
			    GDlgProcThunk * thnk;

			    if (m_pDlgProc)
			    {
				thnk = (GDlgProcThunk *)  m_pDlgProc ->oDefProc;

				::SetWindowLongPtr (GetHWND (), DWLP_DLGPROC,
							       (LONG_PTR) thnk);
				delete m_pDlgProc;

				m_pDlgProc = thnk ->IsThunkPtr () ? thnk : NULL;
			    }
			};

protected :

virtual	GWindow *	GetCreationWindow   (int nCtrlId);

	DWORD		CreateException	    (PEXCEPTION_RECORD);

static	BOOL WINAPI    _initDlgProc	    (HWND, UINT, WPARAM, LPARAM);

// Message Dispatching -------------------------------------------------------
//
	Bool		On_Create	    (void *);

virtual	Bool		On_InitDialog	    (GWinMsg &);

static	Bool GAPI	DispatchDlgMsg	    (GWinMsg &);

virtual	Bool		On_DlgMsg	    (GWinMsg &);

	Bool		DispatchWinMsg	    (GWinMsg &);
//
// Message Dispatching -------------------------------------------------------

protected :

	    GDlgProcThunk *	    m_pDlgProc	;
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
DEFINE_SYS_WINDOW_CLASS (GDialog);

GDialog::~GDialog ()
{
    Destroy ();

    if (m_pDlgProc)
    {
	delete m_pDlgProc;
	       m_pDlgProc = NULL;
    }
};

GWindow * GDialog::GetCreationWindow (int nCtrlId)
{
    return NULL;
};

static INT_PTR CALLBACK _nullDlgProc (HWND, UINT, WPARAM, LPARAM)
{
    return False;
};

BOOL CALLBACK GDialog::_initDlgProc (HWND hWnd, UINT uMsg, WPARAM wParam ,
							   LPARAM lParam )
{
    DLGPROC   t	    =		  NULL	      ;
    void    * a [3] = {(void *)  _initDlgProc ,
		       (void *)	  hWnd	      ,
		       (void *) & t	      };

    ::RaiseException (EXCEPTION_ACCESS_VIOLATION, 0, 3, (ULONG_PTR *) a);

    if (t)
    {
        ::SetWindowLongPtr (hWnd, DWLP_DLGPROC, (LONG_PTR) t);

        return (BOOL)(t)   (hWnd, uMsg, wParam, lParam);
    }
	::SetWindowLongPtr (hWnd, DWLP_DLGPROC, (LONG_PTR) _nullDlgProc);

	::DestroyWindow	   (hWnd);

	return (BOOL) False;
};

DWORD GDialog::CreateException (PEXCEPTION_RECORD r)
{
    if ((EXCEPTION_ACCESS_VIOLATION ==		r ->ExceptionCode	    )
    &&  (3			    <=		r ->NumberParameters	    )
    &&	((void  *) _initDlgProc	    == (void *) r ->ExceptionInformation [0]))
    {
	m_hWnd				     = (HWND)
	((void **) r ->ExceptionInformation [1]);
      * ((void **) r ->ExceptionInformation [2]) = 
	m_pDlgProc = new GDlgProcThunk (DispatchDlgMsg, this, _nullDlgProc);

	SubclassWndProc (GWindow::DispatchWinMsg, this);

	return EXCEPTION_CONTINUE_EXECUTION  ;
    }
    else
    if ((EXCEPTION_ACCESS_VIOLATION ==		r ->ExceptionCode	    )
    &&  (3			    <=		r ->NumberParameters	    )
    &&	((void  *) _initWndProc	    == (void *) r ->ExceptionInformation [0]))
    {{
	GWindow * w = GetCreationWindow 
       (::GetWindowLong ((HWND) r ->ExceptionInformation [1], GWL_ID));

	if (w && (NULL == w ->GetHWND ()))
	{
	    w ->m_pParent = this;

	    w ->CreateException (r);
	}
	return EXCEPTION_CONTINUE_EXECUTION ;
    }}
	return EXCEPTION_CONTINUE_SEARCH    ;
};

Bool GDialog::Create (GWindow * pParent	  ,
		      HMODULE	hResModule,
		      LPCTSTR	pResName  ,
		      void *	pParam	  )
{
    if (GetHWND ())
    {
	return False;
    }

    HWND	   hWnd	      = NULL;
    HWND	   hWndParent = NULL;
    DWord	   dStyle     =    0;
    LPCDLGTEMPLATE pTemplate  = (LPCDLGTEMPLATE) LoadResource (hResModule, pResName, RT_DIALOG);

    if ((NULL == pTemplate) || (! GetDlgTemplateInfo (pTemplate, & dStyle, NULL, NULL)))
    {
	return False;
    }

    if (dStyle & WS_CHILD)
    {
	if ((NULL ==  pParent)
	||  (NULL == (hWndParent = pParent ->GetHWND ())))
	{
	    return False;
	}
	    m_pParent = pParent;
    }
    else
    {
	if  (	      pParent
	&&  (NULL == (hWndParent = pParent ->GetHWND ())))
	{
	    return False;
	}
    }
    __try
    {
	if (NULL != (hWnd = ::CreateDialogIndirectParam
			   (::GetModuleHandle (NULL)	   ,
					       pTemplate   ,
					       hWndParent  ,
					      _initDlgProc ,
				     (LPARAM ) pParam	   )))
	{
	    return GetHWND () ? True : False;
	}
    }
    __except (CreateException  ((GetExceptionInformation ()) ->ExceptionRecord))
    {}
	    m_pParent = NULL;

	    return GetHWND () ? True : False;
};

Bool GDialog::On_InitDialog (GWinMsg & m)
{						// Set default focus
						//
	return m.DispatchCompleteDlgEx ((LRESULT) True);
};

Bool GDialog::On_Create (void * pParam)
{
    if (! GParentWindow::On_Create (pParam))
    {
	return False;
    }
	return True;
};

Bool GAPI GDialog::DispatchDlgMsg (void * oDsp, GWinMsg & m)
{
    GDialog * w = (GDialog *) oDsp;

    switch (m.uType)
    {
    case GWinMsg::DlgProc :

	if (w ->HasFlag (GWF_CREATED))
	{
//	    printf ("<<DlgMsg (%08X) <- %08X {%08X %08X}>>\n", w ->GetHWND (), m.hWnd, m.uMsg);

	    return w ->On_DlgMsg (m);
	}
//	    m.uType = GWinMsg::DlgProcCrt;
//
	if (WM_INITDIALOG == m.uMsg)
	{
	if ((w ->On_Create ((void *) m.lParam)) ? True : (w ->On_Destroy (), False))
	{
	    w ->SetFlags   (0, GWF_CREATED);

	    m.uType = GWinMsg::DlgProc;

	    return w ->On_InitDialog (m);
	}
	    return False;
	}
	else
	if (WM_NCDESTROY   == m.uMsg)
	{
	    w ->UnSubclassDlgProc ();

	if (w ->m_pWndProc == NULL  )
	{
	    w ->m_hWnd      = NULL;
	    w ->m_pParent   = NULL;
	}
	}
    };
	    return w ->On_DlgMsg (m);
};

Bool GDialog::DispatchWinMsg (GWinMsg::Type t, GWinMsg & m)
{
    if (GWinMsg::WndProc == t)
    {
	return (m_pDlgProc) ? False : GWindow::if (m_pDlgProc)

	return False;
    }
	return False;
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
// Some Shell wrappers
//
//
    Bool	    GAPI    InitCtrl32	(DWord dICC, DWord dCtrl32VerReq = MAKELONG (71, 4));
//
//  ICC_LISTVIEW_CLASSES    Load list-view and header control classes. 
//  ICC_TREEVIEW_CLASSES    Load tree-view and ToolTip control classes. 
//  ICC_BAR_CLASSES	    Load toolbar, status bar, trackbar, and 
//  ICC_TAB_CLASSES	    Load tab and ToolTip control classes. 
//  ICC_UPDOWN_CLASS	    Load up-down control class. 
//  ICC_PROGRESS_CLASS	    Load progress bar control class. 
//  ICC_HOTKEY_CLASS	    Load hot key control class. 
//  ICC_ANIMATE_CLASS	    Load animate control class. 
//  			    ToolTip control classes. 
//  ICC_WIN95_CLASSES	    Load animate control, header, hot key, list-view, 
//  			    progress bar, status bar, tab, ToolTip, toolbar, 
//  			    trackbar, tree-view, and up-down control classes. 
//  ICC_DATE_CLASSES	    Load date and time picker control class. 
//  ICC_USEREX_CLASSES	    Load ComboBoxEx class. 
//  ICC_COOL_CLASSES	    Load rebar control class. 
//
//			    0x0400 <= IE :
//  ICC_INTERNET_CLASSES    Load IP address class. 
//  ICC_PAGESCROLLER_CLASS  Load pager control class. 
//  ICC_NATIVEFNTCTL_CLASS  Load a native font control class. 
//
//			    0x0560 <= IE :
//  ICC_STANDARD_CLASSES    Load one of the intrinsic User32 control 
//  			    classes. The user controls include button, 
//  			    edit, static, listbox, combobox, and scrollbar. 
//  ICC_LINK_CLASS	    Load a hyperlink control class. 
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
// Some Shell wrappers
//
Bool GAPI InitCtrl32 (DWord dICC, DWord dCtrl32VerReq)
{
    DLLVERSIONINFO  dvi		  ;
    void *	    pDllGetVersion;
    HMODULE	    hModule	  ;
    DWORD	    dCtrl32Ver = 0;

    if (NULL != (hModule	= ::LoadLibrary	   (	   _T("comctl32.dll"))))
    {
    if (NULL != (pDllGetVersion = ::GetProcAddress (hModule, "DllGetVersion")))
    {
	memset (& dvi, 0, sizeof (dvi));
	dvi.cbSize	= sizeof (dvi) ;

	if (ERROR_SUCCESS == ((DLLGETVERSIONPROC) pDllGetVersion)(& dvi))
	{
	    dCtrl32Ver	   = MAKELONG (dvi.dwMinorVersion, 
				       dvi.dwMajorVersion);
	}
    }
	::FreeLibrary (hModule);
    }

printf ("InitCtrl32 : version %d.%d\n", HIWORD(dCtrl32Ver   ), LOWORD(dCtrl32Ver   ));
printf ("Required   :	     %d.%d\n" , HIWORD(dCtrl32VerReq), LOWORD(dCtrl32VerReq));

    if (dCtrl32Ver < dCtrl32VerReq)
    {	
	return False;
    }

    INITCOMMONCONTROLSEX  i ;

    i.dwSize	= sizeof (i);
    i.dwICC	= dICC	    ;

	return ::InitCommonControlsEx (& i);
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
class GDockParent : public GParentWindow
{
public :
	    GDockParent	  (const void * pWndId) :

	    GParentWindow (pWndId)
	    {
		m_hNSGrowCursor = ::LoadCursor (NULL, IDC_SIZENS);
		m_hWEGrowCursor = ::LoadCursor (NULL, IDC_SIZEWE);
	    };

	GDockPane &	RootPane ()	    { return m_RootPane;};

protected :

	GDockPane *	GetPaneByCursor	    (LONG px, LONG py, Bool * pOverDragBar = NULL);

static	void GAPI	DragPaneProc	    (DragProcReason    dpr,
					     DragProcContext * dpc);

virtual	void		On_PaintPaneBkGnd   (HDC, GDockPane *, const GRect & pr,
							       const GRect & wr,
							       const GRect * db);

virtual	void		On_PaintPane	    (HDC, GDockPane *, const GRect & wr);

protected :

	Bool		On_Create	    (void *);

	Bool		DispatchWinMsg	    (GWinMsg &);

	Bool		On_WM_SetCursor	    (GWinMsg &);
	Bool		On_WM_MouseLBDown   (GWinMsg &);

	Bool		On_WM_PosChanging   (GWinMsg &);
	Bool		On_WM_NcCalcSize    (GWinMsg &);
	Bool		On_WM_PosChanged    (GWinMsg &);

	Bool		On_WM_PaintBkGnd    (GWinMsg &);
	Bool		On_WM_Paint	    (GWinMsg &);

//	Bool		On_WM_Notify	    (GWinMsg &);

protected :

	    GDockPane		m_RootPane	;

	    HCURSOR		m_hNSGrowCursor ;
	    HCURSOR		m_hWEGrowCursor	;
};
//
//[]------------------------------------------------------------------------[]


//[]------------------------------------------------------------------------[]
//
Bool GDockParent::On_Create (void * pParam)
{
    if (! GParentWindow::On_Create (pParam))
    {
	return False;
    }

    GRect	     rc;

    GetClientRect (& rc);

    m_RootPane.InitRoot (this, SizeParam   (rc.cx, rc.cy),
//			       LayoutParam (5, 5, 5, 5),
//			       LayoutParam (5, 10, 10, 5),
			       LayoutParam (0, 0, 0, 0),
			       SizeParam   (0, 0));

	return True ;
};

Bool GDockParent::DispatchWinMsg (GWinMsg & m)
{
    switch (m.uType)
    {
    case GWinMsg::WndProc :

	DISPATCH_WM	(WM_SETCURSOR,		On_WM_SetCursor	    )
	DISPATCH_WM	(WM_LBUTTONDOWN,	On_WM_MouseLBDown   )

	DISPATCH_WM	(WM_WINDOWPOSCHANGING,	On_WM_PosChanging   )
	DISPATCH_WM	(WM_NCCALCSIZE,		On_WM_NcCalcSize    )
	DISPATCH_WM	(WM_WINDOWPOSCHANGED,	On_WM_PosChanged    )

	DISPATCH_WM	(WM_ERASEBKGND,		On_WM_PaintBkGnd    )
	DISPATCH_WM	(WM_PAINT,		On_WM_Paint	    )

	return GParentWindow::DispatchWinMsg (m);
    };
	return GParentWindow::DispatchWinMsg (m);
};

GDockPane * GDockParent::GetPaneByCursor (LONG px, LONG py, Bool * pOverDragBar)
{
    GPoint  cp (px, py);

    GDockPane * p = m_RootPane.GetPaneByCursor (cp.x, cp.y);

    if (pOverDragBar)
    {
	p ->OwnerToP (cp);			// p != NULL in any case

      * pOverDragBar = p ->IsCursorOverDragBar (cp.x, cp.y) ? True :False;
    }
	return  p;
};

Bool GDockParent::On_WM_SetCursor (GWinMsg & m)
{
//  Do this for controls
//
//  if (GetParent () && (0 != GetParent () ->Send_WM (m.uMsg, wParam, lParam)))
//  {
//	return m.DispatchComplete (True);
//  }
//
    if (GetHWND () == (HWND)  m.wParam)
    {
    if (HTCLIENT   == LOWORD (m.lParam))
    {{
	Bool	bOverDragBar;
	GPoint		  cp;
	GDockPane *	   p;

	    ::GetCursorPos   (		  & cp);
	    ::ScreenToClient (GetHWND (), & cp);

	p = GetPaneByCursor  (cp.x, cp.y, & bOverDragBar);

	if (bOverDragBar)
	{
	    ::SetCursor (p ->ChildOfVer () ? m_hWEGrowCursor 
					   : m_hNSGrowCursor);
	    return m.DispatchComplete (True);
	}
    }}
    }
	    return m.DispatchDefault  ();
};

void GAPI GDockParent::DragPaneProc (DragProcReason    dpr,
				     DragProcContext * dpc)
{
    HDC	    dc;

    switch (dpr)
    {
	case Drag_Begin :

	    dpc ->SetBrush (::CreateHalftoneBrush ());
	    break;

	case Drag_EndCancel :
	case Drag_End :

	    ::DeleteObject (dpc ->GetBrush ());
	    dpc ->SetBrush (NULL);
	    break;

	case Drag_ShowGhost :
	case Drag_HideGhost :

	    dc = ::GetDCEx   (dpc ->Window () ->GetHWND (), NULL,   DCX_PARENTCLIP 
								  | DCX_CLIPSIBLINGS 
								  | DCX_LOCKWINDOWUPDATE);
		 ::PatBlt    (dc, & dpc ->Ghost	   (), 
				    dpc ->GetBrush (), PATINVERT);

		 ::ReleaseDC (dpc ->Window () ->GetHWND (), dc);
	    break;
    };
};

Bool GDockParent::On_WM_MouseLBDown (GWinMsg & m)
{
    Bool    bOverDragBar;
    GDockPane *	       p;

	p = GetPaneByCursor  (GET_X_LPARAM (m.lParam),
			      GET_Y_LPARAM (m.lParam),
			    & bOverDragBar	    );
    if (bOverDragBar)
    {{
	DragProcContext	 dpc (DragPaneProc, this);
	GRect		 pr;

	p ->DragBarRect (pr);
	p ->PToOwner	(pr.Pos ());

	dpc.InitGhost	(pr,  GET_X_LPARAM (m.lParam),
			      GET_Y_LPARAM (m.lParam));

	if (ExecCaptureMouseDragging 
			(DragDispatchProc,  dpc))
	{
	    pr.Pos    () -= dpc.Ghost ().Pos ();

	    p ->Grow  (- pr.Pos ().x, - pr.Pos ().y);
	}
	if (HasFlag  (GWF_NEEDUPDATE))
	{
	    m_RootPane.UpdateContext ();

	    SetFlags (GWF_NEEDUPDATE, 0);
	}


        return m.DispatchComplete ();
    }}
        return m.DispatchDefault  ();
};

Bool GDockParent::On_WM_PosChanging (GWinMsg & m)
{
    printf ("\n\nWM_PosChanging <------------------------------\n");

    PrintWINDOWPOS (GetStyles (), (WINDOWPOS *) m.lParam);

    m.DispatchDefault ();

    if (((WINDOWPOS *) m.lParam) ->flags & SWP_NOSIZE)
    {}
    else
    {{
	GRect		 wr;

	GetWindowRect (& wr);

	if ((wr.cx != ((WINDOWPOS *) m.lParam) ->cx)
	||  (wr.cy != ((WINDOWPOS *) m.lParam) ->cy))
	{
	((WINDOWPOS *) m.lParam) ->flags |= SWP_NOCOPYBITS;
	}
    }}

    PrintWINDOWPOS (GetStyles (), (WINDOWPOS *) m.lParam);
    
    printf ("\n\nWM_PosChanging (%08X) >------------------------------\n", GetHWND ());

    return m.DispatchComplete ();
};

Bool GDockParent::On_WM_NcCalcSize (GWinMsg & m)
{
    printf ("\n\nWM_NcCalcSize <-------------------------------\n");

    if (m.wParam)
    {
	m.DispatchDefault ();
/*
    // Bypath any Client's copy
    //
       ((NCCALCSIZE_PARAMS *) m.lParam) ->rgrc [1] =
       ((NCCALCSIZE_PARAMS *) m.lParam) ->rgrc [0];

       ((NCCALCSIZE_PARAMS *) m.lParam) ->rgrc [2] =
       ((NCCALCSIZE_PARAMS *) m.lParam) ->rgrc [0];

	m.lResult = WVR_VALIDRECTS;
*/
    }
    else
    {
	m.DispatchDefault ();
    }

    if (! HasStyle (WS_MINIMIZE))
    {
	m_RootPane.Grow (((NCCALCSIZE_PARAMS *) m.lParam) ->rgrc [0].right  -
			 ((NCCALCSIZE_PARAMS *) m.lParam) ->rgrc [0].left   -
			 m_RootPane.PRectSize ().x,
			 ((NCCALCSIZE_PARAMS *) m.lParam) ->rgrc [0].bottom -
			 ((NCCALCSIZE_PARAMS *) m.lParam) ->rgrc [0].top    -
			 m_RootPane.PRectSize ().y);
/*
::Sleep (60);
*/

// ???
//	m_RootPane.m_dFlags |= DPF_UPDATED;
//	SetFlags (GWF_NEEDUPDATE | GetFlags ());
    }

    printf ("WM_NcCalcSize (%08X) >-------------------------------\n", GetHWND ());

    return m.DispatchComplete ();
};

Bool GDockParent::On_WM_PosChanged (GWinMsg & m)
{
//-------------------------------
//
{
    printf ("WM_PosChanged <-------------------------------\n");

    PrintWCRect (this);

    PrintWINDOWPOS  (GetStyles (), (WINDOWPOS *) m.lParam);

    printf ("----------------------------------------------\n\n");
}
//
//-------------------------------

    if ((((WINDOWPOS *) m.lParam) ->flags & SWP_NOSIZE) || (HasStyle (WS_MINIMIZE)))
    {}
    else
    {
	if (HasFlag  (GWF_NEEDUPDATE))
	{
    printf ("Update layouts <<\n\n");

	    m_RootPane.UpdateContext  ();

	    SetFlags (GWF_NEEDUPDATE, 0);

    printf ("Update layouts >>\n\n");
	}
    }

    printf ("WM_PosChanged (%08X) >-------------------------------\n", GetHWND ());

//	return m.DispatchComplete ();

	return m.DispatchDefault  ();
};


Bool GDockParent::On_WM_PaintBkGnd (GWinMsg & m)
{
    printf ("WM_PaintBkGnd <-------------------------------\n");

    PrintWCRect (this);

    GRect   pr;
    GRect   wr;
    GRect   db;
    GRect * pd;

    if (True/*::GetUpdateRect (GetHWND (), & pr.Rect (), False)*/)
    {

//  printf ("Update : {%d.%d},{%d.%d}\n", pr.x, pr.y, pr.cx, pr.cy);

//	    ::SetWindowOrgEx ((HDC) m.wParam, 40, 40, & org);

	    m_RootPane.PRect	(pr);
	    m_RootPane.PToOwner	(pr.Pos ());

	    m_RootPane.WRect	(wr);
	    wr.Move	(pr.Pos ());

	    On_PaintPaneBkGnd ((HDC) m.wParam, & m_RootPane, pr, wr, NULL);

    for (GDockPane * p = m_RootPane.m_SList.first (); p; p = m_RootPane.m_SList.getnext (p))
    {
	if (p ->IsVisible ())
	{
	    p ->PRect    (pr);
	    p ->PToOwner (pr.Pos ());

	    p ->WRect    (wr);
	    wr.Move	 (pr.Pos ());

	    if (p ->HasDragBar ())
	    {
		db = pr;

		if (p ->ChildOfVer ())
		{
		    pr.cx -= (db.cx = p ->HasDragBar ());
		    db.x  +=  pr.cx;
		}
		else
		{
		    pr.cy -= (db.cy = p ->HasDragBar ());
		    db.y  +=  pr.cy;
		}
		pd = & db;
	    }
	    else
	    {
		pd = NULL;
	    }

	    On_PaintPaneBkGnd ((HDC) m.wParam, p, pr, wr, pd);
	}
    }

    }
else
{
    printf ("Update : Empty\n");
}
{
    void * p;	_asm { mov p,esp}

    printf ("WM_PaintBkGnd (%08X:%08X)>-------------------------------\n", this, GetHWND ());

}
    return m.DispatchComplete (True);
};

Bool GDockParent::On_WM_Paint (GWinMsg & m)
{
    PAINTSTRUCT	    ps;
    GRect	     r;
    RECT	    rc;
    LONG	    mix;
    LONG	    miy;


{
    printf ("WM_Paint <------------------------------------\n");

    ::GetUpdateRect (GetHWND (), & r.Rect (), False);

    printf ("Update : {%d.%d},{%d.%d}\n", r.x, r.y, r.cx, r.cy);

}
    ::BeginPaint (GetHWND (), & ps);

    if (ps.fErase)
    {
	printf ("Need erase BkGnd\n");

//	Send_WM (WM_ERASEBKGND, (WPARAM) ps.hdc, (LPARAM) NULL);
    }

	GetClientRect (& rc);

//	FillRect  (ps.hdc, & rc, (HBRUSH) ::GetStockObject (BLACK_BRUSH));

	m_RootPane.WRect  (r);
    
	m_RootPane.PToOwner (r.Pos ());

	CopyRect  (& rc, & r);

//	FrameRectEx (ps.hdc, & rc, (HBRUSH) ::GetStockObject (LTGRAY_BRUSH), 1, PATCOPY);

	m_RootPane.GetMinSize (& mix, & miy);

    printf ("Root :  {%4d.%4d}{%4d.%4d}:[%4d.%4d]\n", r.x, r.y, r.cx, r.cy, mix, miy);

	for (GDockPane * p = m_RootPane.m_SList.first (); p; p = m_RootPane.m_SList.getnext (p))
	{
	    p ->WRect (r);

	    p ->PToOwner (r.Pos ());

	    CopyRect (& rc, & r);

//	    FrameRectEx (ps.hdc, & rc, (HBRUSH) ::GetStockObject (GRAY_BRUSH), 1, PATCOPY);


    printf ("\t{%4d.%4d}{%4d.%4d} User:{%4d.%4d}\n", r.x, r.y, r.cx, r.cy, p ->m_cxWUsr, p ->m_cyWUsr);
/*
	if (p ->HasDragBar ())
	{
	    p ->PRect (r);

	    r.y  = r.cy - p ->HasDragBar ();
	    r.cy =	  p ->HasDragBar ();

	    p ->PToOwner (r.Pos ());

	    CopyRect (& rc, & r);

	    FrameRectEx (ps.hdc, & rc, (HBRUSH) ::GetStockObject (WHITE_BRUSH), 1, PATCOPY);
	}
*/
	    if (p ->m_pOwner)
	    {
//		::UpdateWindow (((GWindow *) p ->m_pOwner) ->GetHWND ());
	    }
	}


    ::EndPaint	 (GetHWND (), & ps);

{
    printf ("WM_Paint >------------------------------------\n");
}
    return m.DispatchComplete ();
};


void GDockParent::On_PaintPaneBkGnd (HDC dc, GDockPane * p, const GRect & pr,
							    const GRect & wr,
							    const GRect * db)
{
//  FillRect	(dc, & pr, (HBRUSH) ::GetStockObject (WHITE_BRUSH));

    FillRect	(dc, & pr, (HBRUSH) ::GetStockObject (BLACK_BRUSH));

    FrameRectEx (dc, & pr, (HBRUSH) ::GetStockObject (LTGRAY_BRUSH), 1, PATCOPY);

    FrameRectEx (dc, & wr, (HBRUSH) ::GetStockObject (GRAY_BRUSH  ), 1, PATCOPY);

    if (db)
    {
    FrameRectEx (dc,   db, (HBRUSH) ::GetStockObject (WHITE_BRUSH ), 1, PATCOPY);
    }

    printf ("\t\t...PaintBkGnd [(%d,%d).(%d,%d)], %08X\n", pr.x, pr.y, pr.cx, pr.cy, db);

};

void GDockParent::On_PaintPane (HDC dc, GDockPane * p, const GRect & wr)
{
};
//
//[]------------------------------------------------------------------------[]













//[]------------------------------------------------------------------------[]
//
template <class T = GWindow> class TDockingWindow : public GDockPane
{
public :
		TDockingWindow	(const void * pWndId) : m_Window (pWndId) {};

	T &		Window	    ()	    { return m_Window;};
/*
	Bool		Create	    (GDockParent *  pParent  ,
				     LPCTSTR	    pTitle   ,
				     DWord	    dStyle   ,
				     DWord	    dExStyle ,
				     int	    x       = 0   ,
				     int	    y       = 0   ,
				     int	    cx      = 0   ,
				     int	    cy      = 0   ,
				     void *	    pParam  = NULL)
			{
			    if (m_Window.Create (pParent, pTitle     ,
						 dStyle , dExStyle   ,
						 x, y, cx, cy, pParam))
			    {
				return True ;
			    }
				return False;
			};
*/
/*
	Bool		Create	    (GWindow *	pParent	      ,
				     LPCTSTR    pTitle	      ,
				     DWord      dStyle	      ,
				     DWord      dExStyle      ,
				     int	x       = 0   ,
				     int	y       = 0   ,
				     int	cx      = 0   ,
				     int	cy      = 0   ,
				     void *	pParam  = NULL)
			{
			};
*/
protected :
	    T			m_Window	;
};
//
//[]------------------------------------------------------------------------[]










//[]------------------------------------------------------------------------[]
//
class GFrameWindow : public GDockParent
{
DECLARE_EXT_WINDOW_CLASS (_T("GWin.FrameWindow"), CS_DBLCLKS);

public :
	    GFrameWindow (const void * pWndId) : 

	    GDockParent	 (pWndId) 
	    {
	    };

	Bool		Create	    (GWindow *	pOwner	      ,
				     LPCTSTR    pTitle	      ,
				     DWord      dStyle	      ,
				     DWord      dExStyle      ,
				     int	x       = 0   ,
				     int	y       = 0   ,
				     int	cx      = 0   ,
				     int	cy      = 0   ,
				     void *	pParam  = NULL)
			{
			    return GWindow::Create (pOwner		 , pTitle  ,
						     dStyle & (~WS_CHILD), dExStyle,
						     x, y, cx, cy,	   pParam  );
			};
protected :

	Bool		On_Create	    (void *);

	void		On_PaintBkGnd	    (HDC, GDockPane *);

protected :
	Bool		DispatchWinMsg	    (GWinMsg &);

	Bool		On_WM_PaintBkGnd    (GWinMsg &);
	Bool		On_WM_Paint	    (GWinMsg &);


virtual	Bool		On_WM_Close	    (GWinMsg &);

protected :
};
//
//[]------------------------------------------------------------------------[]


//[]------------------------------------------------------------------------[]
//
DEFINE_EXT_WINDOW_CLASS (GFrameWindow);

Bool GFrameWindow::DispatchWinMsg (GWinMsg & m)
{
    switch (m.uType)
    {
    case GWinMsg::WndProc :

//	DISPATCH_WM	(WM_NCCALCSIZE,		On_WM_NcCalcSize     	)
//	DISPATCH_WM	(WM_WINDOWPOSCHANGING,	On_WM_PosChanging	)
//	DISPATCH_WM	(WM_WINDOWPOSCHANGED,	On_WM_PosChanged	)

//	DISPATCH_WM	(WM_ERASEBKGND,		On_WM_PaintBkGnd	)
//	DISPATCH_WM	(WM_PAINT,		On_WM_Paint		)

	DISPATCH_WM	(WM_CLOSE,		On_WM_Close		)

	return GDockParent::DispatchWinMsg (m);
    };
	return GDockParent::DispatchWinMsg (m);
};

Bool GFrameWindow::On_Create (void * pParam)
{
    if (! GDockParent::On_Create (pParam))
    {
	return False;
    }

//  HWND hWnd = 
//  ::CreateWindow ("Static", "Static", WS_CHILD | WS_VISIBLE | WS_TABSTOP, 10, 10,120, 100, GetHWND (), 0, ::GetModuleHandle (NULL), NULL);

//  ::CreateWindow ("Button", "OK"    , WS_CHILD | WS_VISIBLE | WS_TABSTOP, 20, 20, 80, 28, hWnd/*GetHWND ()*/, (HMENU) IDOK, ::GetModuleHandle (NULL), NULL);

//  ::CreateWindow ("Button", "Cancel", WS_CHILD | WS_VISIBLE | WS_TABSTOP, 20, 54, 80, 28, hWnd/*GetHWND ()*/, (HMENU) IDOK, ::GetModuleHandle (NULL), NULL);
/*
    ::CreateWindow ("Button", "OK"    , WS_CHILD | WS_VISIBLE | WS_TABSTOP, 30,120, 80, 28, GetHWND (), (HMENU) IDOK, ::GetModuleHandle (NULL), NULL);

    ::CreateWindow ("Button", "Cancel", WS_CHILD | WS_VISIBLE | WS_TABSTOP, 30,154, 80, 28, GetHWND (), (HMENU) IDOK, ::GetModuleHandle (NULL), NULL);
*/
//-------------------------------
//
{
    printf ("GFrameWindow::Created (On_Create ------------\n");

    PrintWCRect (this);

    printf ("---------------------------------------------\n\n");
}
//
//-------------------------------

    return True;
};

Bool GFrameWindow::On_WM_Close (GWinMsg & m)
{
    ::PostQuitMessage (IDCANCEL);


//  Destroy ();

    return m.DispatchComplete ();
};
//
//[]------------------------------------------------------------------------[]


//[]------------------------------------------------------------------------[]
//
class GFrameControl : public GFrameWindow
{
#if FALSE

DECLARE_EXT_WINDOW_CLASS (_T("GWin.FrameControl"), 0);

#else ////////////////////////////////////////////////////

    public :
    static  Bool RegisterWindowClass ()
	    {
		return (NULL == s_WndClass.Registered)
			      ? s_WndClass.Register () : False;
	    };
    protected :
	    GWindowClass &  GetWindowClass  () const
	    {
		    return s_WndClass;
	    };
    private :
    static  LPCTSTR GAPI    GetWindowClassInfo	(WNDCLASSEX * wc = NULL)
	    {
		    LPCTSTR pName	= _T("GWin.FrameControl");

		if (wc)
		{
		    ::GetWindowClassInfo (WC_DIALOG, wc);

		    wc ->cbWndExtra	= DLGWINDOWEXTRA;

		    wc ->lpfnWndProc	= _initWndProc	;
		    wc ->lpszClassName	= pName		;
		}
		    return  pName;
	    };
    static  GWindowClass    s_WndClass;

#endif //////////////////////////////////////////////////

public :
	    GFrameControl (const void * pWndId) : 

	    GFrameWindow  (pWndId) 
	    {
	    };

	Bool		Create	    (GWindow *	pOwner	      ,
				     LPCTSTR    pTitle	      ,
				     DWord      dStyle	      ,
				     DWord      dExStyle      ,
				     int	x       = 0   ,
				     int	y       = 0   ,
				     int	cx      = 0   ,
				     int	cy      = 0   ,
				     void *	pParam  = NULL)
			{
			    return GWindow::Create  (pOwner		 , pTitle  ,
						     dStyle & (~WS_CHILD), dExStyle,
						     x, y, cx, cy,	   pParam  );
			};
protected :

	void		On_Destroy		    ();
	Bool		On_Create		    (void *);
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
DEFINE_EXT_WINDOW_CLASS (GFrameControl);

void GFrameControl::On_Destroy ()
{
    if (m_pWndProc ->oDefProc == ::DefDlgProc)
    {
	m_pWndProc ->oDefProc  = ::DefWindowProc;
    }

      GFrameWindow::On_Destroy ();
};

Bool GFrameControl::On_Create (void * pParam)
{
    if (! GFrameWindow::On_Create (pParam))
    {
	return False;
    }
	m_pWndProc ->oDefProc = ::DefDlgProc;

	return True ;
};
//
//[]------------------------------------------------------------------------[]





//[]------------------------------------------------------------------------[]
//
void GDockPane::GetMinSize (LONG * cxPMin, LONG * cyPMin) const
{
    if (!(m_dFlags & DPF_VISIBLE))
    {
	if (cxPMin)
	{ * cxPMin = 0;}
	if (cyPMin)
	{ * cyPMin = 0;}

	return;
    }

    LONG    cxCMin;
    LONG    cyCMin;

    LONG    cxWMin = 0;
    LONG    cyWMin = 0;

    for (GDockPane * p = m_SList.first (); p; p = m_SList.getnext (p))
    {
	    p ->GetMinSize (& cxCMin, & cyCMin);

	if (m_dFlags & DPF_VER)
	{
		cxWMin += cxCMin;

	    if (cyWMin  < cyCMin)
	    {	cyWMin  = cyCMin;}
	}
	else
	{
	    if (cxWMin  < cxCMin)
	    {	cxWMin	= cxCMin;}

		cyWMin += cyCMin;
	}
    }
	if (cxWMin < m_cxWMin)
	{   cxWMin = m_cxWMin;}

	if (cyWMin < m_cyWMin)
	{   cyWMin = m_cyWMin;}

	if (cxPMin)
	{ * cxPMin = m_lxWOff + cxWMin + m_hxWOff;}
	if (cyPMin)
	{ * cyPMin = m_lyWOff + cyWMin + m_hyWOff;}
};
//
//[]------------------------------------------------------------------------[]
//
#pragma pack (_M_PACK_LONG)

    struct GDockPaneEntry
    {
	GDockPane * pPane     ;
	Bool	    bGrowable ;
	int	    gPriority ;
	LONG 	    czT	      ;
	LONG 	    czWUsr    ;
	LONG 	    czWMin    ;
	LONG	    czW	      ;
    };

#pragma pack ()

static int selectEntriesWithHiPriority (GDockPaneEntry ** pp, int cPriority, int n, GDockPaneEntry * pe)
{
    GDockPaneEntry *  e ;
    int		   i, k ;

    int	lPriority  = GROW_PRIORITY_LO - 1;

    for (e = pe, i = 0, k = 0; i < n; i ++, e ++)
    {
	if (e ->bGrowable && (e ->gPriority < cPriority)
			  && (e ->gPriority > lPriority))
	{
	    lPriority = e ->gPriority; k ++;
	}
    }

    if (0 == k)
    {	return 0;}

    for (e = pe, i = 0, k = 0; i < n; i ++, e ++)
    {
	if (e ->bGrowable && (e ->gPriority == lPriority))
	{
	    pp [k ++] = e;
	}
    }

    for (n = 0    ; n < k; n ++)
    {
    for (i = n + 1; i < k; i ++)
    {
	if (pp [n] ->czWUsr > pp [i] ->czWUsr)
    	{
	    e = pp [i]; pp [i] = pp [n]; pp [n] = e;
	}
    }
    }
	return k;
};

static void calcLayout (int n, GDockPaneEntry * pe, LONG W)
{
    GDockPaneEntry *  e ;
    int		   i, k ;
    int	      cPriority ;
    LONG	      W2;
    LONG     czW, czWUsr;

    GDockPaneEntry *  ps [DPS_MAX];

lRepeat:

    for (e = pe, i = 0, czW = czWUsr = 0; i < n; i ++, e ++)
    {
	if (e ->bGrowable)
	{
	    czW	+= e ->czW;
        }
    }
	    W2 = W; i = -1;

    cPriority  = GROW_PRIORITY_HI + 1;

    while (0 < (k = selectEntriesWithHiPriority (ps, cPriority, n, pe)))
    {
	    czW -= (W - W2);

	for (i = 0, czWUsr  = 0; i < k; i ++)
	{
		    czWUsr += ((ps [i] ->czWUsr) * czW / W);
	}

	for (i = 0; i < k; i ++)
	{
		 ps [i] ->czW = (czWUsr) ? (ps [i] ->czWUsr * czW / czWUsr) : (czW / k);

	    if ( ps [i] ->czW > W2)
	    {
		 ps [i] ->czW = W2;
	    }

	    if ( ps [i] ->czW <  ps [i] ->czWMin)
	    {
		 ps [i] ->czW =  ps [i] ->czWMin;

	    W -= ps [i] ->czW;

		 ps [i] ->bGrowable = False;

		 goto lRepeat;
	    }

	    if ((ps [i] ->gPriority	       )

	    &&  (ps [i] ->czW > ps [i] ->czWUsr))
	    {
		 ps [i] ->czW = ps [i] ->czWUsr;
	    }

	    if (0 > (W2 -= ps [i] ->czW))
	    {
		 ps [i] += W2;
		     W2 += W2;
	    }
	}
	    cPriority = (* ps) ->gPriority;
    };

    if ((0 < W2) && (0 <= i))
    {
	ps [i - 1] ->czW += W2;
    }
};

static int buildPaneEntry (GDockPaneEntry * pe, GDockPane * p)
{
    if (p ->IsVisible ())
    {
	    pe ->pPane	    = p		       ;
	    pe ->bGrowable  = p ->IsGrowable ();

	if (p ->ChildOfVer ())
	{
	    pe ->gPriority  = p ->GetVerGrowPriority ()  ;
	    pe ->czT	    = p ->m_lxWOff + p ->m_hxWOff;
	    pe ->czWUsr	    = p ->m_cxWUsr		 ;
			      p ->GetMinSize (& pe ->czWMin, NULL);
	    pe ->czWMin    -= pe ->czT			 ;
	    pe ->czW	    = p ->WRectSizeX ()		 ;
	}
	else
	{
	    pe ->gPriority  = p ->GetHorGrowPriority ()  ;
	    pe ->czT	    = p ->m_lyWOff + p ->m_hyWOff;
	    pe ->czWUsr	    = p ->m_cyWUsr		 ;
			      p ->GetMinSize (NULL, & pe ->czWMin);
	    pe ->czWMin    -= pe ->czT			 ;
	    pe ->czW	    = p ->WRectSizeY ()		 ;
	}
	return 1;
    }
	return 0;
};

void GDockPane::RecalcLayout ()
{
    GDockPane *	     p ;
    GDockPane *	  prev ;
    LONG	  d, z ;

    z = (m_dFlags & DPF_VER) ? WRectPosX () : WRectPosY ();

    for (p = m_SList.first (), prev = NULL; p; p = m_SList.getnext (p))
    {
	if (p ->IsVisible ())
	{
	    if (m_dFlags & DPF_VER)
	    {
		if (p ->IsGrowable () && prev && prev ->IsGrowable ())
		{
		    d  = prev ->SetVerDragBar (True);

		    for  (; prev != p; prev = prev ->m_SLink.next ())
		    {
			if (prev ->IsVisible ())
			{
			    prev ->m_PRect.x += d;
			}
		    }
		    z += d;
		}
		    p ->m_PRect.Assign (z, WRectPosY (), p ->m_lxWOff + p ->m_cxWUsr + p ->m_hxWOff, WRectSizeY ());

		    z += p ->m_PRect.cx ;
	    }
	    else
	    {
		if (p ->IsGrowable () && prev && prev ->IsGrowable ())
		{
		    d  = prev ->SetHorDragBar (True);

		    for  (; prev != p; prev = prev ->m_SLink.next ())
		    {
			if (prev ->IsVisible ())
			{
			    prev ->m_PRect.y += d;
			}
		    }
		    z += d;
		}
		    p ->m_PRect.Assign (WRectPosX (), z, WRectSizeX (), p ->m_lyWOff + p ->m_cyWUsr + p ->m_hyWOff);

		    z += p ->m_PRect.cy ;
	    }
	    if (p ->IsGrowable ())
	    {
		prev = p;
	    }
	}
	else
	{
		    p ->m_dFlags &= (~DPF_UPDATED);

	    if (m_dFlags & DPF_VER)
	    {
		    p ->SetVerDragBar  (False);

		    p ->m_PRect.Assign (0, 0, p ->m_lxWOff + p ->m_cxWUsr + p ->m_hxWOff, WRectSizeY ());
	    }
	    else
	    {
		    p ->SetHorDragBar  (False);

		    p ->m_PRect.Assign (0, 0, WRectSizeX (), p ->m_lyWOff + p ->m_cyWUsr + p ->m_hyWOff);
	    }
	}
    }
	UpdateLayout ();

	Grow (0, 0);
};

void GDockPane::UpdateLayout ()
{
    LONG	     z ;
    LONG	  C, W ;

    GDockPane *	     p ;
    int		  i, n ;
    union
    {
    GDockPaneEntry * e ;
    void	   * o ;
    };
    GDockPaneEntry   pe [DPS_MAX];

		     m_dFlags |= DPF_UPDATED;

    if (NULL != (o = GetOwner ()))
    {
    ((GWindow *) o) ->SetFlags  (0, GWF_NEEDUPDATE);
    }

    for (p = m_SList.first (), e = pe; p; p = m_SList.getnext (p))
    {
	if (buildPaneEntry (e, p))
	{
	    e ++;
	}
    }

    if (0 == (n = e - pe))
    {
	return;
    }

    for (e = pe, i = 0, C = 0; i < n; i ++, e ++)
    {
	    C += e ->czT ;

	if (! e ->bGrowable)
	{
	    C += e ->czW ;
	}
    }

    W = ((m_dFlags & DPF_VER) ? WRectSizeX () : WRectSizeY ()) - C;

    calcLayout (n, pe, W);

    z = (m_dFlags & DPF_VER)  ? WRectPosX  () : WRectPosY  ();

    for (e = pe, i = 0; i < n; i ++, e ++)
    {
	 p = e ->pPane;

	if (m_dFlags & DPF_VER)
	{
	    p ->m_PRect.x  = z;
	    p ->m_PRect.y  = WRectPosY  ();
	    p ->m_PRect.cy = WRectSizeY ();
	    z		  +=
	   (p ->m_PRect.cx = p ->m_lxWOff + e ->czW + p ->m_hxWOff);
	}
	else
	{
	    p ->m_PRect.x  = WRectPosX  ();
	    p ->m_PRect.y  = z;
	    p ->m_PRect.cx = WRectSizeX ();
	    z		  +=
	   (p ->m_PRect.cy = p ->m_lyWOff + e ->czW + p ->m_hyWOff);
	}

//	if (NULL == p ->m_pOwner)
	{
	    p ->UpdateLayout ();
	}
    }
};

void GDockPane::Grow (LONG dx, LONG dy)
{
    LONG *	     pz;
    LONG 	   micz;
    LONG	   micx;
    LONG	   micy;
    LONG	   P, W;
    LONG	      z;

    GDockPane *	     p ;
    int	       g, i, n ;
    GDockPaneEntry * e ;

    GDockPaneEntry   pe [DPS_MAX];

    if (IsRoot ())
    {
	GetMinSize (& micx, & micy);

	if ((m_PRect.cx + dx) < micx)
	{
	    dx = micx - m_PRect.cx;
	}
	if ((m_PRect.cy + dy) < micy)
	{
	    dy = micy - m_PRect.cy;
	}
	if ((0 == dx) && (0 == dy))
	{
	    return;					// !!!!!!!!
	}

	m_PRect.Grow (dx, dy);

	UpdateLayout ();

	m_cxWUsr = WRectSizeX ();
	m_cyWUsr = WRectSizeY ();
    }
    else
    {
	if (! IsVisible  ())
	{
	    return;
	}
	if (! IsGrowable ())
	{
	    if (ChildOfVer ())
	    {
		m_cxWMin  =
		m_cxWUsr += dx;
	    }
	    else
	    {
		m_cyWMin  =
		m_cyWUsr += dy;
	    }

	    GetParent () ->RecalcLayout ();

	    return;
	}

	if (ChildOfVer ())
	{
	    dy = 0; pz = & dx;
	}
	else
	{
	    dx = 0; pz = & dy;
	}

	if (0 < * pz)
	{
		    buildPaneEntry (e = pe, this);

		    e ++;

	    for (p = GetParent () ->m_SList.getnext (this), n = 0; p; p = GetParent () ->m_SList.getnext (p))
	    {
		if (buildPaneEntry (e, p))
		{
		    if (e ->bGrowable)
		    {
			if (0 == n)
			{
			    e ->gPriority = 1;
			}
			else 
			if (e ->gPriority > 0)
			{
			    e ->bGrowable = False;
			}
			n ++;
		    }
		    e ++;
		}
	    }
	    if  (1 > n)
	    {
		return;
	    }
		n = e - pe;
		pe [g =	     0].bGrowable = False;
	}
	else
	if  (0 > * pz)
	{
	    for (p = GetParent () ->m_SList.first (), e = pe; p != this; p = p ->m_SLink.next ())
	    {
		if (buildPaneEntry (e, p))
		{
		    if (e ->bGrowable)
		    {
			if (e ->gPriority > 0)
			{
			    e ->bGrowable = False;
			}
		    }
		    e ++;
		}
	    }
		    buildPaneEntry (e, p);

		    e ++;

	    for (p = GetParent () ->m_SList.getnext (this), n = 0; (n == 0) && p; p = GetParent () ->m_SList.getnext (p))
	    {
		if (buildPaneEntry (e, p))
		{
		    if (e ->bGrowable)
		    {	{
			    n ++;

			    e ->gPriority = 1;
		    }	}
		    e ++;
		}
	    }
	    if (1 > n)
	    {
		return;
	    }
		n = e - pe;
		pe [g = n -  1].bGrowable = False;
	}
	else
	{
		return;
	}

	for (e = pe, i = 0, micz = 0, P = 0, W = 0; i < n; i ++, e ++)
	{
	    if (e ->bGrowable)
	    {
		micz += (e ->czWMin + e ->czT);
		W    += (e ->czW	     );
		P    += (e ->czW    + e ->czT);
	    }
	}

	    * pz = gabs (* pz);

	if ((P - * pz) < micz)
	{
	    * pz = P   - micz;
	}

	if (0 == * pz)
	{
	    return;
	}

	calcLayout (n, pe, W  - * pz);

	if (ChildOfVer ())
	{
	    pe [g].czW += * pz;

	    z = pe ->pPane ->m_PRect.x;
	}
	else
	{
	    pe [g].czW += * pz;

	    z = pe ->pPane ->m_PRect.y;
	}

	for (e = pe, i = 0; i < n; i ++, e ++)
	{
	     p = e ->pPane;

	    if (ChildOfVer ())
	    {
		p ->m_PRect.x  = z;
		z	      +=
	       (p ->m_PRect.cx = p ->m_lxWOff + e ->czW + p ->m_hxWOff);

		p ->m_cxWUsr   = p ->WRectSizeX ();
	    }
	    else
	    {
		p ->m_PRect.y  = z;
		z	      +=
	       (p ->m_PRect.cy = p ->m_lyWOff + e ->czW + p ->m_hyWOff);

		p ->m_cyWUsr   = p ->WRectSizeY ();
	    }

//	    if (NULL == p ->m_pOwner)
	    {
		p ->UpdateLayout ();
	    }
	}
    }
};








#define USE_DEFERWINDOPOS

#ifdef  USE_DEFERWINDOPOS

void GDockPane::UpdateContext ()
{
    GWindow   *	     pOwner   ;
    HDWP	     pContext ;

    GDockPane *	     p ;
    GRect	     pr;
    GRect	     wr;
    GRect	     wo;

		    pContext = NULL;

    if  (m_dFlags & DPF_UPDATED)
    {
    for (p = m_SList.last (); p; p = m_SList.getprev (p))
    {
	if  (p ->m_dFlags & DPF_UPDATED)
	{
	    if (NULL != (pOwner = (GWindow *) p ->m_pOwner))
	    {
		p ->PRect    (pr);
		p ->PToOwner (pr.Pos ());
		p ->WRect    (wr);
		    wr.Move  (pr.Pos ());

		if (pContext == NULL)
		{
		    printf  ("\t********BegDeferWindowPos -----------------------\n");

		    pContext = ::BeginDeferWindowPos (DPS_MAX);
		}
		if (pContext)
		{

		    pOwner ->GetWindowRect (& wo);

		    printf ("\t********");
		    printf ("Defer  %08X {%4d.%4d}{%4d.%4d}->{%4d.%4d}{%4d.%4d}\n", pOwner ->GetHWND (),
										    wo.x, wo.y, wo.cx, wo.cy,
										    wr.x, wr.y, wr.cx, wr.cy);

		    pContext = ::DeferWindowPos ((HDWP) pContext, pOwner ->GetHWND (), NULL,
						 wr.x, wr.y, wr.cx, wr.cy, 
						 SWP_NOZORDER
					       | SWP_NOACTIVATE
//					       | SWP_NOCOPYBITS
//					       | SWP_NOREDRAW
					       | 0	       );
		}
	    }
	}
    }
	m_dFlags &= (~DPF_UPDATED);
    }

    if (pContext)
    {
	printf  ("\n\t********EndDeferWindowPos <<---------------------\n\n");

	::EndDeferWindowPos ((HDWP) pContext);

	printf  ("\n\t********EndDeferWindowPos >>---------------------\n\n");

	pContext = NULL;
    }
};





















#else

void * GDockPane::UpdateContext (void * pContext)
{
    GDockPane *	     p ;
    GRect	     pr;
    GRect	     wr;
    GRect	     wo;

    if (True/*m_dFlags & DPF_UPDATED*/)
    {
	if (/*! IsRoot () &&*/ m_pOwner)
	{
	    PRect    (pr);
	    PToOwner (pr.Pos ());
	    WRect    (wr);
	    wr.Move  (pr.Pos ());

	    if (pContext == NULL)
	    {
		pContext = & p/*::BeginDeferWindowPos (DPS_MAX)*/;
	    }
	    if (pContext)
	    {
		((GWindow *) m_pOwner) ->GetWindowRect (& wo);

		if  (wo != wr)
		{
//    		((GWindow *) GetOwner ()) ->InvalidateRect (& pr, False);

		    if  (wo.Size () == wr.Size ())
		    {
			printf ("\t********");
			printf ("Defer %08X ->{%4d.%4d}{ No . No }\n", ((GWindow *) m_pOwner) ->GetHWND (), wr.x, wr.y);

				   ::SetWindowPos   (((GWindow *) m_pOwner) ->GetHWND (), NULL,
						     wr.x, wr.y, 0, 0, SWP_NOZORDER | SWP_NOSIZE);
/*
			pContext = ::DeferWindowPos ((HDWP) pContext, ((GWindow *) m_pOwner) ->GetHWND (), NULL,
						     wr.x, wr.y, 0, 0, SWP_NOZORDER | SWP_NOSIZE);
*/
		    }
		    else
		    {
			printf ("\t********");
			printf ("Defer  %08X {%4d.%4d}{%4d.%4d}->{%4d.%4d}{%4d.%4d}\n", ((GWindow *) m_pOwner) ->GetHWND (),
											 wo.x, wo.y, wo.cx, wo.cy,
											 wr.x, wr.y, wr.cx, wr.cy);
/*
			pContext = ::DeferWindowPos ((HDWP) pContext, ((GWindow *) m_pOwner) ->GetHWND (), NULL,
						     wr.x, wr.y, wr.cx, wr.cy, SWP_NOZORDER);//| SWP_NOCOPYBITS | SWP_NOREDRAW);
*/


				   ::SetWindowPos   (((GWindow *) m_pOwner) ->GetHWND (), NULL,
						     wr.x, wr.y, wr.cx, wr.cy, SWP_NOZORDER);

		      ((GWindow *) m_pOwner) ->GetWindowRect (& wr);

			printf ("\t========");
			printf ("Defer  %08X {%4d.%4d}{%4d.%4d}\n",			((GWindow *) m_pOwner) ->GetHWND (),
											 wr.x, wr.y, wr.cx, wr.cy);
		    }

    //		wr.x = wr.y = 0;
    //		wr.cx /= 2;
    //		wr.cy /= 2;

    //		    ((GWindow *) GetOwner ()) ->InvalidateRect (& pr, False);

    //	        ((GWindow *) GetOwner ()) ->ValidateRect   (& wr);
		}
		else
		{
		    printf ("\t********");
		    printf ("No Def %08X {%4d.%4d}{%4d.%4d}=={%4d.%4d}{%4d.%4d}\n", ((GWindow *) m_pOwner) ->GetHWND (),
										     wo.x, wo.y, wo.cx, wo.cy,
										     wr.x, wr.y, wr.cx, wr.cy);
		}
	    }
	}
	else if (m_pOwner)
	{
//	       ((GWindow *) m_pOwner   ) ->InvalidateRect (NULL, True);
	}

	m_dFlags &= (~DPF_UPDATED);
    }

	return  pContext;
};

void GDockPane::UpdateContext ()
{
    GDockPane *	    p ;
    void *    pContext;

	printf  ("\t********BegDeferWindowPos -----------------------\n");

    for (p = m_SList.last (); p; p = m_SList.getprev (p))
    {
    if  (True/*p ->m_dFlags & DPF_UPDATED*/)
    {
	pContext = p ->UpdateContext (pContext);
    }
    }

    if  (pContext)
    {
	if (m_pOwner)
	{
//+++	    ((GWindow *) m_pOwner   ) ->InvalidateRect (NULL, True);
	}

	printf  ("\n\t********EndDeferWindowPos <<---------------------\n\n");

//	::EndDeferWindowPos ((HDWP) pContext);
    
	printf  ("\n\t********EndDeferWindowPos >>---------------------\n\n");

	pContext = NULL;
    }
    else
    {
	printf  ("\t********NulDeferWindowPos -----------------------\n");
    }
};

#endif














#if FALSE

void * GDockPane::UpdateContext (void * pContext)
{
    GDockPane *	     p ;
    GRect	     pr;
    GRect	     wr;
    GRect	     wo;

    for (p = m_SList.last (); p; p = m_SList.getprev (p))
    {
	if (p ->m_dFlags & DPF_UPDATED)
	{
	    pContext = p ->UpdateContext (pContext);
	}
    }
    if (m_dFlags & DPF_UPDATED)
    {
	if (! IsRoot () && m_pOwner)
	{
	    PRect    (pr);
	    PToOwner (pr.Pos ());
	    WRect    (wr);
	    wr.Move  (pr.Pos ());

	    if (pContext == NULL)
	    {
		pContext = ::BeginDeferWindowPos (DPS_MAX);
	    }
	    if (pContext)
	    {
		((GWindow *) m_pOwner) ->GetWindowRect (& wo);

		if  (wo != wr)
		{
//    		((GWindow *) GetOwner ()) ->InvalidateRect (& pr, False);

		    if  (wo.Size () == wr.Size ())
		    {
			pContext = ::DeferWindowPos ((HDWP) pContext, ((GWindow *) m_pOwner) ->GetHWND (), NULL,
						     wr.x, wr.y, 0, 0, SWP_NOZORDER | SWP_NOSIZE);

			printf ("\t********");
			printf ("Defer %08X ->{%4d.%4d}{ No . No }\n", ((GWindow *) m_pOwner) ->GetHWND (), wr.x, wr.y);
		    }
		    else
		    {
			pContext = ::DeferWindowPos ((HDWP) pContext, ((GWindow *) m_pOwner) ->GetHWND (), NULL,
						     wr.x, wr.y, wr.cx, wr.cy, SWP_NOZORDER);//| SWP_NOCOPYBITS | SWP_NOREDRAW);

			printf ("\t********");
			printf ("Defer  %08X {%4d.%4d}{%4d.%4d}->{%4d.%4d}{%4d.%4d}\n", ((GWindow *) m_pOwner) ->GetHWND (),
											 wo.x, wo.y, wo.cx, wo.cy,
											 wr.x, wr.y, wr.cx, wr.cy);
		    }

    //		wr.x = wr.y = 0;
    //		wr.cx /= 2;
    //		wr.cy /= 2;

    //		    ((GWindow *) GetOwner ()) ->InvalidateRect (& pr, False);

    //	        ((GWindow *) GetOwner ()) ->ValidateRect   (& wr);
		}
		else
		{
		    printf ("\t********");
		    printf ("No Def %08X {%4d.%4d}{%4d.%4d}=={%4d.%4d}{%4d.%4d}\n", ((GWindow *) m_pOwner) ->GetHWND (),
										     wo.x, wo.y, wo.cx, wo.cy,
										     wr.x, wr.y, wr.cx, wr.cy);
		}
	    }
	}
	else if (m_pOwner)
	{
//	       ((GWindow *) m_pOwner   ) ->InvalidateRect (NULL, True);
	}

	m_dFlags &= (~DPF_UPDATED);
    }

	return  pContext;
};

void GDockPane::UpdateContext ()
{
	   void *   pContext;

	printf  ("\t********BegDeferWindowPos -----------------------\n");

    if	  (NULL != (pContext = UpdateContext (NULL)))
    {
	if (m_pOwner)
	{
//+++	    ((GWindow *) m_pOwner   ) ->InvalidateRect (NULL, True);
	}

	printf  ("\n\t********EndDeferWindowPos <<---------------------\n\n");

	::EndDeferWindowPos ((HDWP) pContext);
    
	printf  ("\n\t********EndDeferWindowPos >>---------------------\n\n");

		    pContext = NULL;
    }
    else
    {
	printf  ("\t********NulDeferWindowPos -----------------------\n");
    }
};
#endif











//
//[]------------------------------------------------------------------------[]
//
GDockPane * GDockPane::GetPaneByCursor (LONG px, LONG py) const
{
    GRect   pr ;

    WRect  (pr);

    if (pr.Contains (px, py))
    {
	for (GDockPane * p = m_SList.first (); p; p = m_SList.getnext (p))
	{
	    if (p ->m_PRect.Contains (px, py))
	    {
		return p ->GetPaneByCursor (px, py);
	    }
	}
    }
	return (GDockPane *) this;
};

Bool GDockPane::IsCursorOverDragBar (LONG px, LONG py) const
{
    LONG    d;

    if ((0 == (d = HasDragBar ())) || IsRoot ())
    {
	return False;
    }
    
    if (ChildOfVer ())
    {
	return (((m_PRect.cx - d) <= px) && (px < m_PRect.cx)) ? True : False;
    }
	return (((m_PRect.cy - d) <= py) && (py < m_PRect.cy)) ? True : False;
};

void GDockPane::GetGrowableRect (GRect & gr) const
{
    gr.Assign (0, 0, 0, 0);

    if (IsRoot () || ! IsVisible () || ! IsGrowable ())
    {
	return;
    }
};
//
//[]------------------------------------------------------------------------[]


//[]------------------------------------------------------------------------[]
//
class GParamBox : public GControl
{
DECLARE_SYS_WINDOW_CLASS (_T("LISTBOX"));

public :
		GParamBox   ();

	Bool		InsertItem	    (int nItem, LPCTSTR pItemName    , 
							void *  pItemContext );

	Bool		AppendItem	    (		LPCTSTR pItemName    ,
							void *  pItemContext )
			{
			    return InsertItem (-1, pItemName, pItemContext);
			};

	Bool		SetItemHeight	    (int nItem, int nHeight);

protected :

	Bool		On_Create		    (void *);

	void		On_Destroy		    ();

	Bool		DispatchWinMsg		    (GWinMsg &);

	Bool		On_CtrlNotify_MeasureItem   (GWinMsg &);

	Bool		On_CtrlNotify_DrawItem	    (GWinMsg &);

protected :
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
DEFINE_SYS_WINDOW_CLASS (GParamBox);

GParamBox::GParamBox () : GControl (NULL)
{
};

Bool GParamBox::InsertItem (int nItem, LPCTSTR pItemName, void * pItemContext)
{
    LRESULT lResult = 

    ::SendMessage (GetHWND (), LB_INSERTSTRING, (WPARAM) nItem  , (LPARAM) pItemName);

    if ((LB_ERR == lResult) || (LB_ERRSPACE == lResult))
    {	return False; }

    ::SendMessage (GetHWND (), LB_SETITEMDATA , (WPARAM) lResult, (LPARAM) pItemName);

	return True ;
};

Bool GParamBox::SetItemHeight (int nItem, int nHeight)
{
    LRESULT lResult =

    ::SendMessage (GetHWND (), LB_SETITEMHEIGHT, (WPARAM) nItem, (LPARAM) nHeight);

    if ((LB_ERR == lResult) || (LB_ERRSPACE == lResult))
    {	return False; }

	return True ;
};

Bool GParamBox::On_Create (void * pParam)
{
    if (! GControl::On_Create (pParam))
    {	return False; }


    for (int i = 0; i < 2; i ++)
    {
	AppendItem    (_T(""), NULL);

	SetItemHeight (i, 64); 
    }


	return True;
};

void GParamBox::On_Destroy ()
{
    GControl::On_Destroy ();
};

Bool GParamBox::On_CtrlNotify_MeasureItem (GWinMsg & m)
{
    return m.DispatchDefault ();
};

Bool GParamBox::On_CtrlNotify_DrawItem (GWinMsg & m)
{
    printf ("\t\t\tGParentBox::On_CtrlNotify_DrawItem\n");

//  return m.DispatchComplete (TRUE);

    return m.DispatchDefault ();
};

Bool GParamBox::DispatchWinMsg (GWinMsg & m)
{
/*
    switch (m.uType)
    {
    case GWinMsg::CtrlNotify :

//	DISPATCH_WM	(WM_MEASUREITEM		    , On_CtrlNotify_MeasureItem);
	DISPATCH_WM	(WM_DRAWITEM		    , On_CtrlNotify_DrawItem);

	return GControl::DispatchWinMsg (m);
    };
*/
	return GControl::DispatchWinMsg (m);
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
class TestClient : public GParentWindow
{
DECLARE_EXT_WINDOW_CLASS (_T("GWin.TestClient"), CS_DBLCLKS);

public :
		TestClient    (const void * pWndId) : 

		GParentWindow (pWndId) 
		{};

	       ~TestClient () {printf ("::~TestClient:%08X\n", this);};

	Bool		Create		    (GWindow *	pOwner	      ,
					     LPCTSTR    pTitle	      ,
					     DWord      dStyle	      ,
					     DWord      dExStyle      ,
					     int	x       = 0   ,
					     int	y       = 0   ,
					     int	cx      = 0   ,
					     int	cy      = 0   ,
					     void *	pParam  = NULL)
			{
			    return GParentWindow::Create (pOwner, pTitle   ,
							  dStyle | WS_CHILD, dExStyle,
							  x, y, cx, cy,	     pParam  );
			};
protected :

	Bool		On_Create	    (void *)
			{
			    printf ("TestClient::On_Create\n");

			    return True;
			};

	void		On_Destroy	    ()
			{
			    printf ("TestClient::On_Destroy\n");
			};

protected :
	Bool		DispatchWinMsg	    (GWinMsg &);

	Bool		On_WM_PaintBkGnd    (GWinMsg &);

//	Bool		On_WM_EraseBkGnd    (GWinMsg &);
	Bool		On_WM_Paint	    (GWinMsg &);
};
DEFINE_EXT_WINDOW_CLASS (TestClient);

Bool TestClient::DispatchWinMsg (GWinMsg & m)
{
    switch (m.uType)
    {
    case GWinMsg::WndProc :

	DISPATCH_WM	(WM_ERASEBKGND,		On_WM_PaintBkGnd	)
	DISPATCH_WM	(WM_PAINT,		On_WM_Paint		)

	return GParentWindow::DispatchWinMsg (m);
    };
	return GParentWindow::DispatchWinMsg (m);
};

Bool TestClient::On_WM_PaintBkGnd (GWinMsg & m)
{
    GRect	     rc;

    GetClientRect (& rc);

    FillRect ((HDC) m.wParam, & rc, (HBRUSH) ::GetStockObject (LTGRAY_BRUSH));

    return m.DispatchComplete (True);
};

Bool TestClient::On_WM_Paint (GWinMsg & m)
{
	PAINTSTRUCT	 ps;
	GRect		 rc;

    ::BeginPaint (GetHWND (), & ps);

	GetClientRect (& rc);

	for (; (0 < rc.cx) && (0 < rc.cy);)
	{
	    FrameRectEx (ps.hdc, & rc, (HBRUSH) ::GetStockObject (BLACK_BRUSH), 1, PATCOPY);

	    rc.Move ( 4,  4);
	    rc.Grow (-8, -8);
	}

    ::EndPaint   (GetHWND (), & ps);

    return m.DispatchComplete ();
};
//
//[]------------------------------------------------------------------------[]


//[]------------------------------------------------------------------------[]
//
class GButton : public GControl
{
DECLARE_SYS_WINDOW_CLASS (_T("Button"));

public :
	    GButton (const void * pWndId = NULL) :

	    GControl (pWndId)
	    {
	    };
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
DEFINE_SYS_WINDOW_CLASS (GButton);
//
//[]------------------------------------------------------------------------[]


//[]------------------------------------------------------------------------[]
//
class CParamControl : public GButton
{
public :

	    CParamControl (const void * pWndId = NULL) :

	    GButton (pWndId)
	    {
		m_nBaseX   = 90;
		m_nSizeX   = 60;

		m_pTitleEx = NULL;

		m_dValue   =    0;
	    };

	Bool		DispatchWinMsg	    (GWinMsg &);

	Bool		On_Create	    (void *);

virtual	Bool		On_ReflectWinMsg    (GWinMsg &);

	void		Init		    (int nBaseX, int nSizeX, const TCHAR * pTitleEx)
			{
			    m_nBaseX	= nBaseX   ;
			    m_nSizeX	= nSizeX   ;
			    m_pTitleEx	= pTitleEx ;
			};

	void		SetValue	    (int);

	void		SetValue	    (const TCHAR *);

protected :

	void		On_Draw		    (LPDRAWITEMSTRUCT);

protected :

	int		    m_nBaseX	  ;
	int		    m_nSizeX	  ;

	const TCHAR *	    m_pTitleEx	  ;

	union
	{
	_U32		    m_dValue	  ;
	TCHAR		    m_pValue [128];
	};
};
//
//[]------------------------------------------------------------------------[]

void CParamControl::SetValue (int n)
{
    wsprintf (m_pValue, "%d", n);

    InvalidateRect (NULL, True);
};

void CParamControl::SetValue (const TCHAR * pString)
{
    StrCpyN (m_pValue, pString, sizeof (m_pValue) / sizeof (TCHAR));

    InvalidateRect (NULL, True);
};

HFONT CreateBoaldGUIFont ()
{
//  GetObject (...)
//
    TEXTMETRIC	tm;
    LOGFONT	lf;

    HDC		hDC	= ::GetDC (NULL);
    HFONT	hDCFont = (HFONT) ::SelectObject (hDC, ::GetStockObject (DEFAULT_GUI_FONT));

	      memset (& lf, 0, sizeof (lf));

    ::GetTextFace    (hDC, sizeof (lf.lfFaceName) / sizeof (TCHAR), lf.lfFaceName);
    ::GetTextMetrics (hDC, & tm);

		      lf.lfHeight  = tm.tmHeight  ;
		      lf.lfWeight  = FW_BOLD	  ;
		      lf.lfCharSet = tm.tmCharSet ;

    ::SelectObject (hDC , hDCFont);
    ::ReleaseDC	   (NULL, hDC);

    return ::CreateFontIndirect (& lf);
};

void CParamControl::On_Draw (LPDRAWITEMSTRUCT pdi)
{
    RECT    rc;
    RECT    rv;
    RECT    rd;
    TCHAR   pBuf [128];

	rc = pdi ->rcItem;
	rv = rc;
	rv.left   = rv.right - m_nBaseX;
	rv.right  = rv.left  + m_nSizeX;

    if (ODA_DRAWENTIRE & pdi ->itemAction)
    {
	rd = rc;
	rd.right  = rv.left  - 2;
	GetText (pBuf, sizeof (pBuf) / sizeof (TCHAR));
	::DrawText (pdi ->hDC, pBuf, -1, & rd, DT_SINGLELINE | DT_RIGHT | DT_VCENTER);

    if (m_pTitleEx)
    {
	rd = rc;
	rd.left   = rv.right + 2;

	::DrawText (pdi ->hDC, m_pTitleEx, -1, & rd, DT_SINGLELINE | DT_LEFT | DT_VCENTER);
    }

	HFONT hFont = (HFONT) ::SelectObject (pdi ->hDC, CreateBoaldGUIFont ());

	rd = rv;
	rd.left   += 2;
	rd.top	  += 2;
	rd.right  -= 4;
	rd.bottom -= 2;

	::DrawText (pdi ->hDC, m_pValue, -1, & rd, DT_SINGLELINE | DT_RIGHT | DT_VCENTER);

	rd.right  += 2;
			      ::DeleteObject (::SelectObject (pdi ->hDC, hFont));

	::FrameRect (pdi ->hDC, & rd, ::GetSysColorBrush (COLOR_BTNFACE));

	if (ODS_FOCUS & pdi ->itemState)
	{
	    ::DrawFocusRect (pdi ->hDC, & rd);
	}
	return;
    }
    if (ODA_FOCUS & pdi ->itemAction)
    {
	rd = rv;
	rd.left   += 2;
	rd.top	  += 2;
	rd.right  -= 2;
	rd.bottom -= 2;

	    ::DrawFocusRect (pdi ->hDC, & rd);
    }
};


Bool CParamControl::DispatchWinMsg (GWinMsg & m)
{
    switch (m.uType)
    {
    case GWinMsg::WndProc :

	if (WM_SETFOCUS == m.uMsg)
	{
	    ::SendMessage (GetParent () ->GetHWND (), DM_SETDEFID, (WPARAM) ::GetWindowLong (GetHWND (), GWL_ID), (LPARAM) 1);	    
	}
	else
	if (WM_KILLFOCUS == m.uMsg)
	{
	    ::SendMessage (GetParent () ->GetHWND (), DM_SETDEFID, (WPARAM) 8, (LPARAM) 1);
	}

	if (WM_GETDLGCODE == m.uMsg)
	{
	    m.DispatchComplete (DLGC_BUTTON);
	}

	break;
    };

    return m.DispatchDefault ();
};

Bool CParamControl::On_Create (void * pParam)
{
    if (! GButton::On_Create (pParam))
    {	return False;}

//  SubclassWndProc (GWindow::DispatchWinMsg, this);
	return True;
};



//[]------------------------------------------------------------------------[]
//
void RemoveDefault (HWND hDlg)
{
    LRESULT wParam = ::CallWindowProc (::DefDlgProc, hDlg, DM_GETDEFID, 0, 0L);
    HWND    lParam;

    if (DC_HASDEFID == HIWORD (wParam))
    {
	lParam = ::GetDlgItem (hDlg, (int)(short) LOWORD (wParam));
	// remove BS_DEFPUSHBUTTON
/*
    if (BS_OWNERDRAW != (::GetWindowLong (lParam, GWL_STYLE) & BS_TYPEMASK))
    {
//	::SendMessage   (lParam, BM_SETSTYLE, BS_DEFPUSHBUTTON, (LPARAM) 1);
    }
*/
    }
};

Bool CParamControl::On_ReflectWinMsg (GWinMsg & m)
{
    LPDRAWITEMSTRUCT  pdi = 
   (LPDRAWITEMSTRUCT) m.lParam	  ;
    RECT	      rc	  ;
    RECT	      rt	  ;
    RECT	      rv	  ;
    TCHAR	      pTitle [128];

    if (WM_COMMAND  == m.uMsg)
    {
	switch (HIWORD (m.wParam))
	{
	case BN_SETFOCUS :
	    ::SendMessage (GetParent () ->GetHWND (), DM_SETDEFID, (WPARAM) LOWORD (m.wParam), (LPARAM) 0);
	    break;

	case BN_KILLFOCUS :
	    ::SendMessage (GetParent () ->GetHWND (), DM_SETDEFID, (WPARAM) 0, (LPARAM) 0);
	    break;


#if FALSE
	case BN_SETFOCUS :

	    RemoveDefault (GetParent () ->GetHWND ());

	    ::SendMessage (GetParent () ->GetHWND (), DM_SETDEFID, (WPARAM) LOWORD (m.wParam), (LPARAM) 1);

printf ("\n\n\nSet focus %08X (%08X-%08X)\n\n\n", m.wParam, GetStyles (), ::SendMessage (GetHWND (), WM_GETDLGCODE, 0, 0));

	    break;

	case BN_KILLFOCUS :
/*
	    ::SendMessage   (GetParent () ->GetCtrlHWND (IDCLOSE), BM_SETSTYLE, 
	   (::GetWindowLong (GetParent () ->GetCtrlHWND (IDCLOSE), GWL_STYLE) & 0x0000FFFF) | BS_DEFPUSHBUTTON, (LPARAM) 1);

	    ::SendMessage (GetParent () ->GetHWND (), DM_SETDEFID, (WPARAM) IDCLOSE, (LPARAM) 1);
*/
printf ("\n\n\nKill focus %08X\n\n\n", m.wParam);

	    break;
#endif
	
#if FALSE
	    case BN_SETFOCUS :
	    {
		DWORD dwRet = (DWORD)::SendMessage(GetParent () ->GetHWND (), DM_GETDEFID, 0, 0L);

		if(HIWORD(dwRet) == DC_HASDEFID)
		{
		    HWND hOldDef = ::GetDlgItem(GetParent () ->GetHWND (), (int)(short)LOWORD(dwRet));
		    // remove BS_DEFPUSHBUTTON
		    ::SendMessage (hOldDef, BM_SETSTYLE, ::GetWindowLong (hOldDef, GWL_STYLE) & (~BS_DEFPUSHBUTTON), (LPARAM) 1);

//		    ::InvalidateRect (GetHWND (), NULL, True);


printf ("Default ID %d\n", LOWORD (dwRet));

::MessageBeep (-1);

		}
//		::SendMessage (GetHWND (), BM_SETSTYLE, GetStyles () | BS_DEFPUSHBUTTON, (LPARAM) 1);

//	    ::SendMessage (GetParent () ->GetHWND (), DM_SETDEFID, 0/*(WPARAM) LOWORD (m.wParam)*/, (LPARAM) 1);

//		    ::SendMessage(GetParent () ->GetHWND (), DM_SETDEFID, nID, 0L);
	    }
//	    ::SendMessage (GetParent () ->GetHWND (), DM_SETDEFID, (WPARAM) LOWORD (m.wParam), (LPARAM) 0);

	    break;
#endif

	};
	    return m.DispatchDefault  ();
    }    
    else
    if (WM_DRAWITEM == m.uMsg)
    {
	On_Draw (pdi);

	return m.DispatchComplete ();

	rc  = pdi ->rcItem;

	GetText (pTitle, sizeof (pTitle) / sizeof (TCHAR));

	rt	  = rc ;
	rt.right -= 100;

	rv.left   = rt.right  + 2;
	rv.top    = rc.top    + 2;
	rv.right  = rc.right  - 4;
	rv.bottom = rc.bottom - 2;

#if TRUE
	if (ODA_DRAWENTIRE & pdi ->itemAction)
	{
//	    ::DrawFrameControl	(pdi ->hDC, & pdi ->rcItem, DFC_BUTTON, DFCS_BUTTONPUSH | DFCS_PUSHED);

	    ::DrawText (pdi ->hDC, pTitle, -1, & rt, DT_SINGLELINE | DT_RIGHT | DT_VCENTER);

	    HFONT hFont = (HFONT) ::SelectObject (pdi ->hDC, CreateBoaldGUIFont ());

::wsprintf (pTitle, "%d ��.", GetCtrlId (m_hWnd));

	    ::DrawText (pdi ->hDC, pTitle, -1, & rv, DT_SINGLELINE | DT_RIGHT | DT_VCENTER);

	    ::DeleteObject (::SelectObject (pdi ->hDC, hFont));

	    rv.right = rc.right - 2;

	    ::FrameRect	    (pdi ->hDC, & rv, ::GetSysColorBrush (COLOR_BTNFACE));

	if (ODS_FOCUS & pdi ->itemState)
	{
	    ::DrawFocusRect (pdi ->hDC, & rv);
	}
	    return m.DispatchComplete ();
	}
	if (ODA_FOCUS & pdi ->itemAction)
	{
	    rv.right = rc.right - 2;

            ::DrawFocusRect (pdi ->hDC, & rv);

	    return m.DispatchComplete ();
	}
	    return m.DispatchComplete ();

#else
	if (ODA_DRAWENTIRE & pdi ->itemAction)
	{
	    ::DrawFrameControl	(pdi ->hDC, & pdi ->rcItem, DFC_BUTTON, DFCS_BUTTONPUSH | DFCS_PUSHED);

	    ::DrawCaption	(pdi ->hwndItem, 
				 pdi ->hDC, & pdi ->rcItem, DC_TEXT | DC_SMALLCAP | 
			       ((ODS_FOCUS  & pdi ->itemState) ? DC_ACTIVE : 0));

	    if (ODS_FOCUS & pdi ->itemState)
	    {
	    ::DrawFocusRect (pdi ->hDC, & rc);
	    }
	    return m.DispatchComplete ();
	}
	if (ODA_FOCUS & pdi ->itemAction)
	{
	    ::DrawCaption	(pdi ->hwndItem, 
				 pdi ->hDC, & pdi ->rcItem, DC_TEXT | DC_SMALLCAP | 
			       ((ODS_FOCUS  & pdi ->itemState) ? DC_ACTIVE : 0));

	if (ODS_FOCUS & pdi ->itemState)
	{
	    ::DrawFocusRect	(pdi ->hDC, & rc);
	}
	    return m.DispatchComplete ();
	}
#endif
    }
	    return False;
};
//
//[]------------------------------------------------------------------------[]



//[]------------------------------------------------------------------------[]
//
class CDialogSettings : public GDialog
{
public :

	    CDialogSettings () : GDialog (NULL),

		m_Content (NULL)
	    {		
	    };

	   ~CDialogSettings () 
	    {};

protected :


	Bool		On_WM_ControlNotify	(HWND hCtrl, int nCtrlId, GWinMsg &);

//	Bool		Get_WM_COMMAND_ID_CODE	(int CtrlId, 

	Bool		On_Create		(void *);

	void		On_Destroy		();

	Bool		On_LookAtChild		(GWinMsg &, GM_LookAtChild_Param *);


	Bool		On_WM_Command		(GWinMsg &);

	Bool		DispatchWinMsg		(GWinMsg &);

	Bool		On_Control_UAMax	(GWinMsg &);

	Bool		On_EnterEditControl	(GWinMsg &);

	Bool		On_Control_IAMax	(GWinMsg &);

	Bool		On_Control_UA		(GWinMsg &);

	Bool		On_Param_UAnode		(GWinMsg &);
	Bool		On_Param_IAnode		(GWinMsg &);

protected :


	GTreeView		m_Content	;

	CParamControl		m_Param_UAnode	;
	CParamControl		m_Param_IAnode	;
};
//
//[]------------------------------------------------------------------------[]


//[]------------------------------------------------------------------------[]
//
Bool CDialogSettings::On_Create (void * pParam)
{
    if (! GDialog::On_Create (pParam))
    {	return False;}

    if (! m_Content.Create (this, GetCtrlHWND (IDC_CONTENT), pParam))
    {	return False;}

    ::SetMenu (GetHWND (), ::LoadMenu (::GetModuleHandle (NULL), MAKEINTRESOURCE (IDR_MAIN_MENU)));


    HTREEITEM	ti = NULL;
		ti =
    m_Content.InsertItem (NULL	, TVI_LAST,	_T("Roent service setttings")	, NULL);
    m_Content.InsertItem (ti	, TVI_LAST,	_T("Access control")		, NULL);
    m_Content.InsertItem (ti	, TVI_LAST,	_T("Ftdi settings")		, NULL);
    m_Content.Expand	 (ti);

		ti =
    m_Content.InsertItem (NULL	, TVI_LAST,	_T("Urp settings")		, NULL);
    m_Content.InsertItem (ti	, TVI_LAST,	_T("Global")			, NULL);
    m_Content.InsertItem (ti	, TVI_LAST,	_T("Some settings (1)")		, NULL);
    m_Content.InsertItem (ti	, TVI_LAST,	_T("Some settings (2)")		, NULL);
    m_Content.InsertItem (ti	, TVI_LAST,	_T("Some settings (3)")		, NULL);
    m_Content.Expand	 (ti);

		ti =
    m_Content.InsertItem (NULL	, TVI_LAST,	_T("KKM settings")		, NULL);
    m_Content.Expand	 (ti);

    m_Content.SelectItem (
    m_Content.GetItem	 (NULL, TVGN_ROOT));

    if (! m_Param_UAnode.Create (this, GetCtrlHWND (IDC_PARAM_UANODE)))
    {	return False;}
//  m_Param_UAnode.SetStyles (0, BS_PUSHLIKE);

    ::SetWindowLong (GetCtrlHWND (IDCLOSE), GWL_STYLE,
    ::GetWindowLong (GetCtrlHWND (IDCLOSE), GWL_STYLE) & (~BS_DEFPUSHBUTTON));



    if (! m_Param_IAnode.Create (this, GetCtrlHWND (IDC_PARAM_IANODE)))
    {	return False;}

/*
	{{
	GDialog * pDialog = new GDialog (NULL);
	pDialog ->Create (this, NULL, MAKEINTRESOURCE (IDD_CONTROL_URP_MINI), NULL);
	pDialog ->SetStyles (0, WS_TABSTOP);
	pDialog ->SetWindowPos (NULL, 220, 300, 0, 0, SWP_NOSIZE);

	}}
*/


	return True ;
};

void CDialogSettings::On_Destroy ()
{
    m_Content.DeleteItem (NULL);

    GDialog::On_Destroy ();
};

Bool CDialogSettings::On_WM_Command (GWinMsg & m)
{
    return False;
};

Bool CDialogSettings::On_LookAtChild (GWinMsg & m, GM_LookAtChild_Param * pParam)
{
    if (pParam ->pChild == & m_Content)
    {
	SetCtrlText (IDC_CONTENT_TEXT, m_Content.GetSelItemText ());
    }

    return True;
};









    typedef struct
    {
	Byte	    nPrecision : 8;
	Byte	    nWidth     : 8;
	Byte	    bFlags     : 8;
	char	    cType      : 8;

    }		    FormatCode	  ;

#define	GET_WM_COMMAND_CODE(m)	(HIWORD(m.wParam))

class EditControl
{
public :

	void		    Init		(HWND, const char * fmt, int   MinValue,
									 int   MaxValue);

	void		    Init		(HWND, const char * fmt, float MinValue,
									 float MaxValue);
	void		    Done		();

virtual	Bool		    PreprocMessage	(MSG *);

virtual	Bool		    On_WM_Dispatch	(GWinMsg &);

virtual	Bool		    DoValidate		();

	Bool		    On_EnterEditControl	(GWinMsg & m);

protected :

	HWND			m_hWnd	      ;

	Bool			m_bChanged    ;
	const char *		m_fmt	      ;

	union
	{
	    int			m_lMinValue   ;
	    float		m_fMinValue   ;
	};
	union
	{
	    int			m_lMaxValue   ;
	    float		m_fMaxValue   ;
	};

//	int			m_nTextLen    ;
	TCHAR			m_pText [256] ;
};



/*

    -dd.ddd	    //-99.999	 99.999
    +dd.ddd	    //-99.999	+99.999
     dd.ddd	    // 00.000	 99.999

    -xx.xxx
    +xx.xxx
     xx.xxx

    "%+

    "f2.3"
    "u3"
*/

__int64	GetFloatValue  (int uFloorValue, int uFractValue)
{
    __int64 v = uFloorValue;

/*
    for (t = uFractValue; 0 < t; t t <  (0 <  uFractValue)
    {
	v *= 10;
    };
*/
    return  v;
};

int Parse (int & Value, const char *& str);

int Parse (int & Value, const char *& str)
{
    Bool bRes;

    for (bRes = 0, Value = 0; * str; str ++)
    {
	if (('0' <= * str) && (* str <= '9'))
	{
	if ((INT_MAX / 10) >= Value)
	{
	    Value = (Value * 10) + (* str - '0');

	    bRes  =  1;

	    continue;
	}
	    bRes  = -1;

	    continue;
	}
	    break;
    }

	return bRes;
}


FormatCode ParseFormat (const char * fmt)
{
//%[flags] [width] [.precision] [{h | l | I | I32 | I64}]type
//
    FormatCode	 Code	 ;
	  int	 n	 ;
	  int	 phase	 ;
    const char * p  = fmt;

    Code.nPrecision = 0  ;
    Code.nWidth	    = 0	 ;
    Code.bFlags	    = 0	 ;
    Code.cType	    = 0	 ;

    if ('%' == * p ++)
    {
	for (phase = 0; * p;)
	{
	if (phase == 0)
	{
	    phase =  1;
	}
	if (phase == 1)
	{
	    phase =  2;

	    if (0 < Parse (n, p))
	    {
		Code.nWidth = (Byte) n;
	    }
		continue;
	}
	if (phase == 2)
	{
	    phase =  3;

	    if ('.' == * p)
	    {
		p ++;

		continue;
	    }
	    phase =  4;
	}
	if (phase == 3)
	{
	    phase =  4;

	    if (0 < Parse (n, p))
	    {
		Code.nPrecision = (Byte) n;
	    }
		continue;
	}
	if (phase == 4)
	{
	    phase =  5;
	}
	if (phase == 5)
	{
	    if (('i' == * p))
	    {
		Code.cType = 'd';
	    }
	    else
	    if (('f' == * p)
	    ||  ('d' == * p)
	    ||  ('X' == * p)
	    ||  ('x' == * p))
	    {
		Code.cType = * p;
	    }
	}
	    break;
	}
    }
	return  Code;
};

TCHAR * ParseValue (const char * fmt, LPCTSTR ps)
{
    struct  FloatPart
    {
	int nLen	;
	int nLenMax	;
	int nValue	;
    };

    FloatPart	 Floor	  = {0, 0, 0};
    FloatPart	 Fract	  = {0, 0, 0};

    FloatPart  * pCurrent = & Floor;
    int		 i ;
    const char * p ;

    FormatCode	 f = ParseFormat (fmt);

    Floor.nLenMax = (f.nWidth - f.nPrecision);
    Fract.nLenMax = (		f.nPrecision);

    for (pCurrent = & Floor; * ps; ps ++)
    {
	if (('0' <= * ps) && (* ps <= '9'))
	{
	    if (((INT_MAX / 10)	    >= pCurrent ->nValue)
	    &&  (pCurrent ->nLenMax >  pCurrent ->nLen	))
	    {
		pCurrent ->nValue = (pCurrent ->nValue * 10) + (* ps - '0');
		pCurrent ->nLen ++;

		continue;
	    }
	}
	else
	if ((f.cType  == 'f'	   )
	&&  (pCurrent == &  Floor  )
	&&  ('.'      == *  ps	   ))  // Get this char from LANGUIGE_ID
	{
	    pCurrent = & Fract;
	    continue;
	}

	return (TCHAR *) ps;
   }
	return NULL ;
};



Bool EditControl::PreprocMessage (MSG * msg)
{
    return False;
};









void EditControl::Init (HWND hWnd, const char * fmt, int MinValue,
						     int MaxValue)
{
    m_hWnd	= hWnd;
    m_fmt	= fmt ;

    m_lMinValue = MinValue;
    m_lMaxValue = MaxValue;
};

void EditControl::Init (HWND hWnd, const char * fmt, float MinValue,
						     float MaxValue)
{
    m_hWnd	= hWnd;
    m_fmt	= fmt ;

    m_fMinValue = MinValue;
    m_fMaxValue = MaxValue;
};

void EditControl::Done ()
{
    FormatCode  f = ParseFormat (m_fmt);

    union
    {
	int	lValue;
	float	fValue;
    };

    ::GetWindowText (m_hWnd, m_pText, sizeof (m_pText) / sizeof (TCHAR));

    sscanf (m_pText, m_fmt, & fValue);

    if (f.cType == 'f')
    {
	if (fValue < m_fMinValue)
	    fValue = m_fMinValue;
	if (fValue > m_fMaxValue)
	    fValue = m_fMaxValue;
    }
    else
    {
	if (lValue < m_lMinValue)
	    lValue = m_lMinValue;
	if (lValue > m_lMaxValue)
	    lValue = m_lMaxValue;
    }
    sprintf (m_pText, m_fmt,  lValue);

    ::SetWindowText (m_hWnd, m_pText);
};

Bool EditControl::DoValidate ()
{
    int	    nFloorLen   ,
	    nFloorValue ,
	    nFractLen   ,
	    nFractValue ;

    TCHAR * pBadChar	;
    Bool    bOk	 = True	;

    if (bOk)
    {
#if TRUE
    while (NULL != (pBadChar = ParseValue (m_fmt, m_pText)))
#else
    while (NULL != (pBadChar = ScanFloatValue (m_pFormat, & nFloorValue, & nFloorLen,
							  & nFractValue, & nFractLen, m_pText)))
#endif
    {
	    bOk  = False;

	::SendMessage	(m_hWnd, EM_SETSEL    , (WPARAM)(int)(pBadChar - m_pText    ),
						(LPARAM)(int)(pBadChar - m_pText + 1));

	::SendMessage	(m_hWnd, EM_REPLACESEL, (WPARAM) FALSE, (LPARAM) "");

	::GetWindowText (m_hWnd, m_pText, sizeof (m_pText) / sizeof (TCHAR));
    };
    }

    if (bOk)
    {{
//	__int64 v = 
    }}




	printf ("\t\t\t\tUAMax Value :Value %d.%d (%d.%d)\n", nFloorValue, nFractValue,
							      nFloorLen  , nFractLen  );
    return  bOk;
};

/* Remove !!!
#ifndef EM_SHOWBALLOONTIP

#define	EM_SHOWBALLOONTIP (ECM_FIRST + 3)

#pragma pack (4)

typedef struct tagEDITBALLOONTIP {
    DWORD cbStruct;
    LPCWSTR pszTitle;
    LPCWSTR pszText;
    INT ttiIcon;
} EDITBALLOONTIP, *PEDITBALLOONTIP;

#pragma pack ()

#endif
*/


Bool EditControl::On_WM_Dispatch (GWinMsg & m)
{
    EDITBALLOONTIP  ebt;

    switch (GET_WM_COMMAND_CODE  (m))
    {
	case EN_UPDATE :

	    ::GetWindowText (m_hWnd, m_pText, sizeof (m_pText) / sizeof (TCHAR));

	if (DoValidate ())
	{
	}
	else
	{
//	    ::SendMessage   (m_hWnd, EM_REPLACESEL    , (WPARAM) FALSE, (LPARAM) m_pText);

//	    ::SetWindowText (m_hWnd, m_pText);

	    ebt.cbStruct = sizeof (ebt)	;
	    ebt.pszTitle = L"������������ ������ ��� ��������";
	    ebt.pszText	 = L"����� ����� ������ ���� ������� dd.ddd";
	    ebt.ttiIcon  = TTI_ERROR	;

	    ::SendMessage   (m_hWnd, EM_SHOWBALLOONTIP, (WPARAM) 0, (LPARAM) & ebt);
	}
	default :;
    };
    return True ;
};


	EditControl *		p_Edit = NULL ;	
	EditControl		s_Edit	      ;


Bool CDialogSettings::On_Control_UAMax (GWinMsg & m)
{
    printf ("\t\tUAMax Edit : (%d) ->%04d.%04X\n", LOWORD (m.wParam), HIWORD (m.wParam),  HIWORD (m.wParam));

    switch (GET_WM_COMMAND_CODE (m))
    {
	case EN_SETFOCUS  :

	    p_Edit = & s_Edit;

	    p_Edit ->Init ((HWND) m.lParam, "%03d",  0, 140);
//	    p_Edit ->Init ((HWND) m.lParam, "%4.3f", (float) 0.0, (float) 3.720);
	    break;

	case EN_KILLFOCUS :

	    if (p_Edit == & s_Edit)
	    {
		p_Edit = NULL;

		s_Edit.Done ();
	    }
	    break;

	default :

	    if (p_Edit)
	    {
		p_Edit ->On_WM_Dispatch (m);
	    }

    }

    return m.DispatchComplete ();
};

















//[]------------------------------------------------------------------------[]
//
class GEdit : public GControl
{
DECLARE_SYS_WINDOW_CLASS (_T("Edit"));

public :
	    GEdit   (const void * pWndId = NULL);

virtual	DWord		Exec		();

protected :
};
//
//[]------------------------------------------------------------------------[]


DWord GWindow::ExecControl (GWindow * pControl)
{
    DWord   dExitCode = (DWord) -1;

    pControl ->Exec ();

    return  dExitCode;
};



//[]------------------------------------------------------------------------[]
//
DEFINE_SYS_WINDOW_CLASS (GEdit);

GEdit::GEdit (const void * pWndId) : GControl (pWndId)
{};


DWord GEdit::Exec ()
{
    printf ("\n\n\n====================================\n");
    printf ("...Exec...");
    printf ("\n\n\n====================================\n");
    return 0;
};
//
//[]------------------------------------------------------------------------[]





Bool CDialogSettings::On_Control_IAMax (GWinMsg & m)
{
    switch (GET_WM_COMMAND_CODE (m))
    {
	case EN_UPDATE :

	    if ((HWND) m.lParam == ::GetFocus ())
	    {{
		GEdit	       EditCtrl;

		ExecControl (& EditCtrl);
	    }}

	    break;
    };
	return m.DispatchComplete ();
};


Bool CDialogSettings::On_WM_ControlNotify (HWND hCtrlId, int nCtrlId, GWinMsg & m)
{
    switch (nCtrlId)
    {
    case IDC_PARAM_UANODE :
	return m_Param_UAnode.On_ReflectWinMsg (m);

    case IDC_PARAM_IANODE :
	return m_Param_IAnode.On_ReflectWinMsg (m);
    };
	return False;
};

Bool CDialogSettings::On_Param_UAnode (GWinMsg & m)
{
//  ::MessageBox (GetHWND (), "���� ��������:", "Report", MB_OK);

    return m.DispatchComplete ();
};

Bool CDialogSettings::DispatchWinMsg (GWinMsg & m)
{

    switch (m.uType)
    {
    case GWinMsg::Internal :

	DISPATCH_GM_	    (GM_LookAtChild	, On_LookAtChild,   GM_LookAtChild_Param)

	return GDialog::DispatchWinMsg (m);

    case GWinMsg::WndProc :

    if (WM_COMMAND == m.uMsg)
    {
	if (IDC_PARAM_UANODE == LOWORD (m.wParam))
	{
//	    ::MessageBeep (-1);
	}
    }
	return GDialog::DispatchWinMsg (m);

    case GWinMsg::DlgProc :

	int	nCtrlId;

    if (0 !=   (nCtrlId = IsControlNotify  (m)))
    {
	switch (nCtrlId)
	{
	case IDC_RADIO1 :
	    break;
	case IDC_CHECK1 :
	    break;
	default :
	    break;
	};
    }

    if (0 !=   (nCtrlId = IsCommand (m)))
    {
printf ("\n\n\n\t\tTarget command (CDialogSettings) %08X\n\n\n", m.wParam);

	switch (nCtrlId)
	{
	case IDC_RADIO1 :
	    break;

	case IDC_CHECK1 :
	    break;

	case IDCLOSE	:
	case IDCANCEL	:
#if TRUE
	    TCurrent () ->GetCurApartment () ->BreakFromWorkLoop ();
#else

#endif
	    break;

	default :
	    ;
	}
	return m.DispatchComplete ();
    };

    if (WM_COMMAND == m.uMsg)
    {

//	DISPATCH_WM_COMMAND_ID	(IDC_UA_MAX	, On_Control_UAEditControl  )

//	DISPATCH_WM_COMMAND_ID	(IDC_UA_MAX	, On_Control_UAMax)
	DISPATCH_WM_COMMAND_ID	(IDC_IA_MAX	, On_Control_IAMax)


	DISPATCH_WM_COMMAND_ID_CODE (IDC_PARAM_UANODE,	BN_CLICKED, On_Param_UAnode)
    }

//	DISPATCH_WM	    (WM_COMMAND		, On_WM_Command	)
	DISPATCH_WM	    (WM_NOTIFY		, On_WM_Notify	)
    
	return GDialog::DispatchWinMsg (m);
    };
	return GDialog::DispatchWinMsg (m);
};
//
//[]------------------------------------------------------------------------[]


//[]------------------------------------------------------------------------[]
//
class GDataItem
{
public :
	    GDataItem (DWord dValue = 0) : m_dValue (dValue) {};

	    GDataItem (LONG  lValue    ) : m_lValue (lValue) {};

	void		SetValue	(DWord dValue, Bool bForceUpdate = False);

protected :

	union
	{
	    DWORD		m_dValue ;
	    LONG		m_lValue ;
	    int			m_iValue ;
	    unsigned short	m_hValue ;
	    short		m_sValue ;
	};
};

void GDataItem::SetValue (DWord dValue, Bool bForceUpdate)
{
    DWord   dOldVal  =  m_dValue;

    if (   (dOldVal != (m_dValue = dValue)) || bForceUpdate)
    {
//	OnDataUpdate   (bForceUpdate);
    }
};

class GBitsField : public GDataItem
{
public :
	    GBitsField (DWord dValue = 0) : GDataItem (dValue) {};

	void		SetBits		(DWord dClrMask, 
					 DWord dSetMask, Bool bForceUpdate = False);

	void		XorBits		(DWord dXorMask, Bool bForceUpdate = False)
			{
			    SetBits	(m_dValue & dXorMask, m_dValue ^ dXorMask);
			};
protected :

	DWord			m_dValue ;
};

void GBitsField::SetBits (DWord dClrMask, DWord dSetMask, Bool bForceUpdate)
{
    DWord   dValue  =  m_dValue;

    if (   (dValue != (m_dValue = (m_dValue & (~ dClrMask)) | dSetMask)) || bForceUpdate)
    {
//	OnDataUpdate  (bForceUpdate)
    }
};


















class CControlTitle : public GDialog
{
public :

	    CControlTitle (const void * pWndId);

	   ~CControlTitle ();

	Bool		Create		(GWindow *  pParent	,
					 HMODULE    hResModule	,
					 LPCTSTR    pText	);

protected :

	Bool		On_Create		(void *);

	void		On_Destroy		();

	Bool		On_DrawTitle		(GWinMsg &, DRAWITEMSTRUCT *);
/*
	void		On_UpdateState		(DWord dSyncMask,
						 DWord dSateMask,
						 DWord dSate	);
*/
/*
	Bool		OnCreate		();

	void		OnDestroy		();
*/


	void		GUIUpdate		(DWord dMask = 0xFFFFFFFF);

	void		OnSelect		(Bool);

	void		OnDropDown		(Bool);



	Bool		DispatchWinMsg		(GWinMsg &);

protected :

	DWord			m_dState    ;
	TCHAR			m_Text [128];
};

CControlTitle::CControlTitle (const void * pWndId) : GDialog (pWndId)
{
    m_dState = 0x00 ;
  * m_Text   = 0    ;
};

CControlTitle::~CControlTitle ()
{};

Bool CControlTitle::Create (GWindow * pParent	 ,
			    HMODULE   hResModule ,
			    LPCTSTR   pText	 )
{
    if (! GDialog::Create (pParent, hResModule, MAKEINTRESOURCE (IDD_CONTROL_TITLE)))
    {	return False;}

//  On_UpdateState (0x00, 0x03, 0x01);

    strcpy (m_Text, pText);

      	return True ;
};

Bool CControlTitle::On_Create (void * pParam)
{
    if (! GDialog::On_Create (pParam))
    {	return False;}

	GUIUpdate ();

	return True ;
};

void CControlTitle::On_Destroy ()
{
    GDialog::On_Destroy ();
};

Bool CControlTitle::On_DrawTitle (GWinMsg & m, DRAWITEMSTRUCT * pdi)
{
//  FillRect (pdi ->hDC, & pdi ->rcItem, (HBRUSH) ::GetStockObject (BLACK_BRUSH));    

    return m.DispatchComplete (True);
};

/*
void CControlTitle::On_UpdateState (DWord dSyncMask, DWord dStateMask, DWord dState)
{
    if (dStateMask)
    {
	m_dState &= (~dStateMask);
	m_dState |= ( dState	);

    if (m_dState    ==   0x01)
    {
	m_dState    &= (~0x01);
	  dSyncMask &= (~0x01);
    }
	EnableCtrl     (IDC_DROP_CHK	, (m_dState & 0x02) ? True : False);
    }

    if (! (dSyncMask & 0x02))
    {
	CheckCtrl      (IDC_SELECT_CHK	, (m_dState & 0x02) ? True : False);
    }
    if (! (dSyncMask & 0x01))
    {
	CheckCtrl      (IDC_DROP_CHK	, (m_dState & 0x01) ? True : False);
    }
};
*/

void CControlTitle::GUIUpdate (DWord dMask)
{
    if (0x01 & dMask)
    {
    EnableCtrl  (IDC_DROP_CHK  , (m_dState & 0x02) ? True : False);
    CheckCtrl   (IDC_SELECT_CHK, (m_dState & 0x02) ? True : False);
    CheckCtrl   (IDC_DROP_CHK  , (m_dState & 0x01) ? True : False);
    SetCtrlText (IDC_DROP_CHK  , (m_dState & 0x01) ? "Hide" : "Show");
    }
};

void CControlTitle::OnSelect (Bool bSelect)
{
	m_dState = ((m_dState & (~0x02)) | ((bSelect) ? 0x02 : 0x00));

    if (0x01 == (m_dState & 0x03))
    {
	OnDropDown (False);
    }
	GUIUpdate  (0x01);
};

void CControlTitle::OnDropDown (Bool bShow)
{
	  bShow	       = (m_dState & 0x02) ? bShow : False;

    if (  bShow && (0 == (m_dState & 0x01)))
    {
printf ("\t\t\t\tShow Window...\n");

	m_dState |=   0x01;

	GUIUpdate (0x01);
    }
    else
    if (! bShow && (0 != (m_dState & 0x01)))
    {
printf ("\t\t\t\tHide window...\n");

	m_dState &= (~0x01);
    }
	GUIUpdate (0x01);
};

Bool CControlTitle::DispatchWinMsg (GWinMsg & m)
{
    switch (m.uType)
    {
    case GWinMsg::WndProc :

	if ((WM_NOTIFY	      == m.uMsg)
	&&  (NM_CUSTOMDRAW    == ((NMHDR *) m.lParam) ->code))
	{}

	if ((WM_COMMAND	      == m.uMsg))
	{
	    if (IS_WM_COMMAND_ID_CODE (IDC_SELECT_CHK, BN_CLICKED))
	    {
		OnSelect   (((m_dState & 0x02) ^ 0x02) ? True : False);
	    }
	    else
	    if (IS_WM_COMMAND_ID_CODE (IDC_DROP_CHK  , BN_CLICKED))
	    {
		OnDropDown (((m_dState & 0x01) ^ 0x01) ? True : False);
	    }
	}

	if ((WM_CTLCOLORDLG    ==  m.uMsg)
	||  (WM_CTLCOLORSTATIC ==  m.uMsg))
	{
//	    ::SelectObject ((HDC) m.wParam, (HBRUSH) ::GetStockObject (BLACK_BRUSH));

//	return m.DispatchComplete ((LRESULT) (HBRUSH) ::GetStockObject (WHITE_BRUSH));
	}
/*
	if ((WM_DRAWITEM      ==       m.uMsg  )
	&&  (IDC_BUTTON_TITLE == (int) m.wParam))
	{
	return On_DrawTitle (m, (DRAWITEMSTRUCT *) m.lParam);
	}
	return GDialog::DispatchWinMsg (m);
*/
	if ((NM_CUSTOMDRAW    ==       m.uMsg  ))
	{
	return GDialog::DispatchWinMsg (m);
	}
	return GDialog::DispatchWinMsg (m);



    case GWinMsg::DlgProc :
/*
	if ((WM_CTLCOLORDLG   ==       m.uMsg)
	||  (WM_CTLCOLORBTN   ==       m.uMsg))
	{
//	return m.DispatchCompleteDlgEx ((LRESULT) (HBRUSH) ::GetStockObject (BLACK_BRUSH));
	}

	if ((NM_CUSTOMDRAW    ==       m.uMsg))
	{
	return GDialog::DispatchWinMsg (m);
	}
*/
	return GDialog::DispatchWinMsg (m);
    }
	return GDialog::DispatchWinMsg (m);
};

/*
template <class M, class F> class TControlPage : public TDockingWindow <CControlPage>
{
public :

	    TControlPage (const void * pWndId) :

		m_MiniBar (NULL),
		m_FullBar (NULL)

	    {}

	Bool		Create		(GWindow *  pParent	,
					 HMODULE    hResModule	,
					 LPCTSTR    pResMiniBar ,
					 LPCTSTR    pResFullBar )
	{
	    if (! Window ().::Create (pParent, hResModule, CControl
	};
protected :

	TDockingWindow <M>		m_MiniBar ;
	TDockingWindow <F>		m_FullBar ;
};
*/
//
//[]------------------------------------------------------------------------[]
//















class CMainControl : public GDockParent
{
public :
		CMainControl ();

	       ~CMainControl ();

	Bool	    Create		    (GWindow * pParent)
		    {
			return GDockParent::Create (pParent, NULL,
						    WS_VISIBLE | WS_CHILD, 0,
						    0, 0, 0, 0, NULL);
		    };

protected :

	Bool	    On_Create		    (void *);

	void	    On_Destroy		    ();

private :

	TDockingWindow <CControlTitle>  m_KkmTitle  ;
	TDockingWindow <CControlTitle>	m_UrpTitle  ;

	TDockingWindow <TestClient>	m_Test	    ;
};

CMainControl::CMainControl () : GDockParent (NULL),

    m_KkmTitle	(NULL),
    m_UrpTitle  (NULL),

    m_Test	(NULL)
{};

CMainControl::~CMainControl ()
{
};

Bool CMainControl::On_Create (void * pParam)
{
    if (! GParentWindow::On_Create (pParam))
    {	return False;}

    GRect   wr;

//.......
//
    if (! m_KkmTitle.Window ().Create (this, NULL, "Kkm control"))
    {	return False;}

    m_KkmTitle.Window ().GetWindowRect (& wr);

    RootPane ().InsertCtrlBar (& m_KkmTitle, NULL,
			       & m_KkmTitle.Window (),
				 LayoutParam (10, 10, 10, 2),
				 SizeParam   (wr.cy, wr.cx));

	return True;
//......
//
    if (! m_UrpTitle.Window ().Create (this, NULL, "Urp control"))
    {	return False;}

    m_UrpTitle.Window ().GetWindowRect (& wr);

    RootPane ().InsertCtrlBar (& m_UrpTitle, NULL,
			       & m_UrpTitle.Window (),
				 LayoutParam (10, 10, 2, 10),
				 SizeParam   (wr.cy, wr.cx));

//.......
//

    m_Test.Window ().Create (this, NULL,
		   WS_VISIBLE | WS_CHILD, 0,
		   0, 0, 0, 0, NULL);

    RootPane ().Insert	    (& m_Test, NULL,
			     & m_Test.Window (),
			       SizeParam (0, 0),
			       LayoutParam (10, 10, 10, 10),
//			       LayoutParam (0, 0, 0, 0),
			       SizeParam (100, 100));
//.......
//

    if (HasFlag  (GWF_NEEDUPDATE))
    {
	m_RootPane.UpdateContext  ();

	SetFlags (GWF_NEEDUPDATE, 0);
    }

	return True;
};

void CMainControl::On_Destroy ()
{
    GDockParent::On_Destroy ();
};
//
//[]------------------------------------------------------------------------[]
//
class CMain : public GFrameControl
{
public :

		CMain ();

private :

	Bool	    On_Create		    (void *);

	void	    On_Destroy		    ();


	Bool	    PreprocMessage	    (MSG *);


//	Bool	    DispatchWinMsg	    (GWinMsg &);

	Bool	    On_WM_Close		    (GWinMsg &);
	Bool	    On_WM_FileTest	    (GWinMsg &);


	DWORD	    DispatchException	    (PEXCEPTION_RECORD);

	GIrp *	    FileTest		    (GIrp *, void *);

	Bool	    On_WinMsg		    (GWinMsg &);

private :

	TDockingWindow <GToolBar>	m_ToolBar   ;
	TDockingWindow <GStatusBar>	m_StatusBar ;

/*
	TDockPane <GToolBar>		m_ToolBar   ;
	TDockPane <GStatusBar>		m_StatusBar ;
*/

	GDockParent *			m_pClient   ;
};

CMain::CMain () : GFrameControl (NULL),

    m_ToolBar   (NULL),
    m_StatusBar (NULL)

{
    m_pClient = NULL;

    m_RootPane.InitRoot (this, SizeParam   (0, 0),
			       LayoutParam (0, 0, 0, 0),
			       SizeParam   (0, 0));
};
//
//[]------------------------------------------------------------------------[]

Bool CMain::PreprocMessage (MSG * msg)
{
//  printf ("\n\n===============\nCMain::PreprocMessage\n================\n\n");

    return GFrameControl::PreprocMessage (msg);
};

//[]------------------------------------------------------------------------[]
//
Bool CMain::On_Create (void * pParam)
{
    GRect   wr;

    if (! GFrameControl::On_Create (pParam))
    {	return False;}

    ::SetMenu (GetHWND (), ::LoadMenu (::GetModuleHandle (NULL), MAKEINTRESOURCE (IDR_MAIN_MENU)));

	return True ;

//
//[]------------------------------------------------------------------------[]
//

    {{
#if FALSE

	GDialog * pDialog = new GDialog (NULL);
	pDialog ->Create (this, NULL, MAKEINTRESOURCE (IDD_CONTROL_URP_FULL), NULL);
	pDialog ->SetStyles (0, WS_TABSTOP);

	GDialog * pDialogC = new GDialog (NULL);
	pDialogC ->Create (pDialog, NULL, MAKEINTRESOURCE (IDD_CONTROL_TITLE), NULL);
	pDialogC ->SetStyles (0, WS_TABSTOP | WS_GROUP);

		  pDialogC = new GDialog (NULL);
	pDialogC ->Create (pDialog, NULL, MAKEINTRESOURCE (IDD_CONTROL_TITLE), NULL);
	pDialogC ->SetStyles (0, WS_TABSTOP | WS_GROUP);
	pDialogC ->SetWindowPos (NULL, 0, 32, 0, 0, SWP_NOSIZE);

#else
	GDialog * pDialog = new GDialog (NULL);
	pDialog ->Create (this, NULL, MAKEINTRESOURCE (IDD_CONTROL_TITLE), NULL);
	pDialog ->SetStyles (0, WS_TABSTOP);

		  pDialog = new GDialog (NULL);
	pDialog ->Create (this, NULL, MAKEINTRESOURCE (IDD_CONTROL_URP_FULL), NULL);
	pDialog ->SetStyles (0, WS_TABSTOP | WS_GROUP);

	pDialog ->SetWindowPos (NULL, 0, 40, 0, 0, SWP_NOSIZE);
#endif	

    }}
//	return True;


    if (! m_StatusBar.Window ().    Create (this, NULL,
					    WS_VISIBLE | CCS_NODIVIDER | CCS_NOPARENTALIGN | CCS_BOTTOM | SBARS_SIZEGRIP, 0))
    {	return False;}

    m_StatusBar.Window ().SetStyles (0, CCS_NORESIZE);

    m_StatusBar.Window ().GetWindowRect (& wr);

    RootPane ().InsertCtrlBar (& m_StatusBar, NULL,
			       & m_StatusBar.Window (),
				 LayoutParam (0, 0, 0, 0),
				 SizeParam   (wr.cy, 0));

	return True;

#if FALSE

    if (! m_ToolBar.Window ().	    Create (this, NULL,
					    WS_VISIBLE | CCS_NODIVIDER | CCS_NOPARENTALIGN | CCS_TOP | TBSTYLE_LIST, 0,
					    0, 0, 0, 0))
    {	return False;}

    m_ToolBar.Window ().GetWindowRect (& wr);


//  RootPane ().m_dFlags |= (DPF_VISIBLE /*| DPF_VER*/);

    TBBUTTON bt [4] = {{STD_FILENEW,  5000, TBSTATE_ENABLED, BTNS_BUTTON | BTNS_DROPDOWN | BTNS_AUTOSIZE, {0,0}, 0, (INT_PTR) "Button(&New)"},
		       {STD_FILEOPEN, 5001, TBSTATE_ENABLED, BTNS_BUTTON | BTNS_DROPDOWN | BTNS_AUTOSIZE, {0,0}, 0, (INT_PTR) "Button(&Open)"},
		       {STD_FILESAVE, 5002, TBSTATE_ENABLED, BTNS_BUTTON | BTNS_DROPDOWN | BTNS_AUTOSIZE, {0,0}, 0, (INT_PTR) "Button(&Save)"},
		       {STD_HELP,     5003, TBSTATE_ENABLED, BTNS_BUTTON | BTNS_DROPDOWN | BTNS_AUTOSIZE, {0,0}, 0, (INT_PTR) "Button(&Help)"}};

    HIMAGELIST  il = ImageList_Create (24, 24, ILC_COLORDDB | ILC_MASK, 0, 16);

    m_ToolBar.Window ().Send_WM   (TB_SETIMAGELIST, (WPARAM) 0, (LPARAM) il);
    m_ToolBar.Window ().Send_WM   (TB_LOADIMAGES, (WPARAM) IDB_STD_LARGE_COLOR, (LPARAM) HINST_COMMCTRL);

    m_ToolBar.Window ().Send_WM   (TB_ADDBUTTONS, (WPARAM) 4, (LPARAM) & bt);
    m_ToolBar.Window ().Send_WM   (TB_AUTOSIZE  , (WPARAM) 0, (LPARAM) 0);

    m_ToolBar.Window ().SetStyles (0, CCS_NORESIZE);

    m_ToolBar.Window ().GetWindowRect (& wr);

/*
#if FALSE

    m_ToolBar.Window ().SetWindowPos (NULL, 20, 15, 400, wr.cy, 0);

#else

    m_ToolBar.Window ().SetWindowPos (NULL, 20, 15, 400, wr.cy, SWP_NOMOVE);

    m_ToolBar.Window ().SetWindowPos (NULL, 20, 15, 400, wr.cy, SWP_NOSIZE);

#endif
*/

    RootPane ().InsertCtrlBar (& m_ToolBar, & m_StatusBar,
			       & m_ToolBar.Window (),
//				 NULL,
				 LayoutParam (10, 10, 10, 10),
				 SizeParam   (wr.cy, 0));

#endif


/*
    if (HasFlag  (GWF_NEEDUPDATE))
    {
	m_RootPane.UpdateContext ();

	SetFlags ((~GWF_NEEDUPDATE) & GetFlags ());
    }
//	return True;
*/
    if (! ((CMainControl *) (m_pClient = new CMainControl ())) ->Create (this))
    {	return False;}

    RootPane ().Insert	      (& m_pClient ->RootPane (), & m_StatusBar,
			         m_pClient,
//				 NULL,
				 SizeParam   (200, 200),
//				 LayoutParam (5, 0, 0, 0),
				 LayoutParam (0, 0, 0, 0),
				 SizeParam   (200, 200));

    if (HasFlag  (GWF_NEEDUPDATE))
    {
	m_RootPane.UpdateContext  ();

	SetFlags (GWF_NEEDUPDATE, 0);
    }
	return True;
};


void CMain::On_Destroy ()
{
    GFrameControl::On_Destroy ();
};

//[]------------------------------------------------------------------------[]
//
    DECLARE_INTERFACE  (IWorkProtocol)
    {
#pragma pack (_M_PACK_LONG)

	struct Request				// All fields in Natural order
	{
	    _U16	uLength	    ;
	    _U16	uType	    ;
	    _U32	uRequestId  ;
	};

	struct WinMsg : public Request		// 0 - Type
	{
	    UINT	uMsg	    ;
	    WPARAM	wParam	    ;
	    LPARAM	lParam	    ;
	};

#pragma pack ()
    
    };
//
//[]------------------------------------------------------------------------[]

/*
void QueueRequest (const void * IWorkProtocol
*/

void On_WorkRequest (void * hKey, WPARAM, LPARAM);


Bool CMain::On_WM_Close (GWinMsg & m)
{
printf ("\n\n............................Post  Exit from Application\n\n");

/*
    GStream * s = OpenIdleMessage ();

    _PutD (s, WM_COMMAND


    ::PostIdleReq (WM_
*/

/*
    ::PostMessage (GetHWND (), WM_COMMAND, (WPARAM) (0x00000000 | IDCLOSE), (LPARAM) GetHWND ());

    ::PostMessage (NULL	     , WM_COMMAND, (WPARAM) (0x00020000 | IDCLOSE), (LPARAM) GetHWND ());
*/
/*
    TCurrent () ->GetCurApartment () ->On_Command (
*/
    On_WorkRequest (this, (WPARAM) IDCLOSE, (LPARAM) 0);

    return True;
};

DWORD CMain::DispatchException (PEXCEPTION_RECORD)
{
    return EXCEPTION_CONTINUE_EXECUTION;
};

void AddPreprocHandler (GWindow *);

void RemPreprocHandler (GWindow *);



/*

Bool CMain::On_DialogSettings (GWinMsg & m)
{
    if (WM_CLOSE
};

void ChoiseSettings (GWindow * pOwner)
{
    TModalWindow <CMain, >    ModlalLoop;

    if (ModlalLoop.Window ().Create (pOwner, NULL, MAKEINTRESOURCE (IDD_DIALOG4)))
    {
	AddPreprocHandler    (& Dlg);

	RemovePreprocHandler (& Dlg);
    }
};






*/

Bool CMain::On_WM_FileTest (GWinMsg & m)
{
    CDialogSettings Dlg;

    if (Dlg.Create  (this, NULL, MAKEINTRESOURCE (IDD_DIALOG4)))
    {
//	Window position/size may set in this point
//
//	Some sync get can be call in this point
//
//	Fill control's data for first show.
//
/*
    {{
	CDialogSettings * pDlg;

	for (int i = 0; i < 2; i ++)
	{
	    pDlg = new CDialogSettings ();

	    pDlg ->Create (& Dlg, NULL, MAKEINTRESOURCE (IDD_DIALOG4));

	    AddPreprocHandler (pDlg);

	    pDlg ->ShowWindow (True, True);
	}
    }}
*/
//  __try
//  {

#if TRUE

	AddPreprocHandler (& Dlg);

	Dlg.ExecWindow ();

	RemPreprocHandler (& Dlg);

#else

	AddPreprocHandler (& Dlg);

	RemPreprocHandler (& Dlg);

#endif

//  }
//  __except (DispatchException ((GetExceptionInformation ()) ->ExceptionRecord))
//  {}
    }
	return True;
};

GIrp * CMain::FileTest (GIrp * pIrp, void * pParam)
{
printf ("\n\n\tExec Idle command (CMain) %08X\n\n", (int) pParam);

    CDialogSettings Dlg;

    if (Dlg.Create  (this, NULL, MAKEINTRESOURCE (IDD_DIALOG4)))
    {
	AddPreprocHandler (& Dlg);

	Dlg.ExecWindow ();

	RemPreprocHandler (& Dlg);
    }
	return pIrp;
};

Bool CMain::On_WinMsg (GWinMsg & m)
{
    if (WM_COMMAND     == m.uMsg)
    {
    if (MMID_FILE_TEST == LOWORD (m.wParam))
    {{
	GIrp * pIrp	   =  new GIrp ();

	SetCurCoProc <CMain, void *>
	      (pIrp, & CMain::FileTest, this, (void *) LOWORD (m.wParam));

	QueueIrpForComplete (pIrp);

/*
	SetCurCoProc <GSerialConnector, void *>
	      (pIrp, & GSerialConnector::Co_CreationLoop, this, NULL);
*/
printf ("\n\n\tQueue Idle command (CMain) %08X\n\n", m.wParam);

    }}


/*
printf ("\n\n\n\t\tTarget command (CMain) %08X\n\n\n", m.wParam);

	    DISPATCH_WM_COMMAND_ID  (MMID_FILE_TEST,	On_WM_FileTest);
*/
    }
	return GFrameControl::On_WinMsg (m);
};
//
//[]------------------------------------------------------------------------[]


//[]------------------------------------------------------------------------[]
//
class MainDialog : public GDialog
{
public :
	    MainDialog	() : GDialog (NULL), m_TestClient (NULL) 
	    {};

	    MainDialog	(const void * pWndId) : GDialog (pWndId), m_TestClient (NULL) 
	    {};

	   ~MainDialog	() { printf ("::~MainDialog:%08X\n", this);};

protected :

	GWindow *	GetCreationWindow   (int nCtrlId)
			{
			    if (IDC_TESTCLIENT == nCtrlId)
			    {
				return & m_TestClient;
			    }
			    else
			    if (False)
			    {}
				return NULL;
			};
protected :

	Bool		On_Create	    (void *);

	void		On_Destroy	    ();

	Bool		DispatchWinMsg	    (GWinMsg &);

	Bool		On_DM_CtlColorDlg   (GWinMsg &);
	Bool		On_DM_CtlColorBtn   (GWinMsg &);

	Bool		On_DCommand_Ok	    (GWinMsg &);
	Bool		On_DCommand_Cancel  (GWinMsg &);


	Bool		On_WM_MeasureItem   (GWinMsg &);
	Bool		On_WM_DrawItem	    (GWinMsg &);

protected :

	TestClient		m_TestClient	;
	GParamBox		m_ParamBox	;
};

Bool MainDialog::On_Create (void * pParam)
{
    if (! GDialog::On_Create (pParam))
    {	return False;}

    if (! m_ParamBox.Create (this, GetCtrlHWND (IDC_PARAMBOX), pParam))
    {	return False;}


	return True;
};

void MainDialog::On_Destroy ()
{
    GDialog::On_Destroy ();
};

Bool MainDialog::On_WM_MeasureItem (GWinMsg & m)
{
    return m.DispatchDefault ();
};

Bool MainDialog::On_WM_DrawItem (GWinMsg & m)
{
    printf ("DlgProc::WM_DRAWITEM\n");

    return m.DispatchDefault ();
};

Bool MainDialog::DispatchWinMsg (GWinMsg & m)
{
    switch (m.uType)
    {
    case GWinMsg::WndProc :

	if (WM_COMMAND == m.uMsg)
	{
//	    printf ("WndProc::WM_COMMAND\n");

//	    DISPATCH_WM_COMMAND_ID  (IDOK	    , On_ID_Ok		    )
//	    DISPATCH_WM_COMMAND_ID  (IDCANCEL	    , On_ID_Cancel	    )

	    return GDialog::DispatchWinMsg (m);
	}
	else
	if (WM_NOTIFY  == m.uMsg)
	{
//	    printf ("WndProc::WM_NOTIFY\n");
	}
//	    DISPATCH_WM		    (WM_MEASUREITEM , On_WM_MeasureItem	    )
//	    DISPATCH_WM		    (WM_DRAWITEM    , On_WM_DrawItem	    )

	    return GDialog::DispatchWinMsg (m);

    case GWinMsg::DlgProc :

	if (WM_COMMAND == m.uMsg)
	{
	    printf ("DlgProc::WM_COMMAND %04Xh [%04Xh = %5d]\n", HIWORD (m.wParam),
								 LOWORD (m.wParam),
								 LOWORD (m.wParam));

	    DISPATCH_WM_COMMAND_ID  (IDOK	    , On_DCommand_Ok	    )
	    DISPATCH_WM_COMMAND_ID  (IDCANCEL	    , On_DCommand_Ok	    )
	    DISPATCH_WM_COMMAND_ID  (IDNO	    , On_DCommand_Cancel    )

	    return GDialog::DispatchWinMsg (m);
	}
	else
	if (WM_NOTIFY  == m.uMsg)
	{
//	    DISPATCH_WM_NOTIFY_CODE (...	    , On_WM_Command_Ok	    )
//	    DISPATCH_WM_NOTIFY_CODE (...	    , On_WM_Command_Cancel  )

	    printf ("DlgProc::WM_NOTIFY  ..... [%04Xh = %5d] %04Xh = %05d\n", ((NMHDR *) m.lParam) ->idFrom,
									      ((NMHDR *) m.lParam) ->idFrom,
									      ((NMHDR *) m.lParam) ->code  ,
									      ((NMHDR *) m.lParam) ->code  );
	}
	else
	if (WM_HSCROLL == m.uMsg)
	{
	    printf ("DlgProc::WM_HSCROLL .....\n");
	}
	else
	if (WM_VSCROLL == m.uMsg)
	{
	    printf ("DlgProc::WM_HSCROLL .....\n");
	}


	    DISPATCH_WM		    (WM_CTLCOLORDLG , On_DM_CtlColorDlg	    )
	    DISPATCH_WM		    (WM_CTLCOLORBTN , On_DM_CtlColorBtn	    )

	    return GDialog::DispatchWinMsg (m);
    };
	    return GDialog::DispatchWinMsg (m);
};

Bool MainDialog::On_DM_CtlColorDlg (GWinMsg & m)
{
/*
    if (GetHWND () == (HWND) m.lParam)
    {
	return m.DispatchCompleteDlgEx ((LRESULT) ::GetStockObject (BLACK_BRUSH));
    }
    else
*/
    {
	return m.DispatchDefault  ();
    }
};

Bool MainDialog::On_DM_CtlColorBtn (GWinMsg & m)
{
/*
    if (IDOK == GetCtrlId ((HWND) m.lParam))
    {
	return m.DispatchCompleteDlgEx ((LRESULT) ::GetStockObject (WHITE_BRUSH));
    }
    else
*/
    {
	return m.DispatchDefault  ();
    }
};


static void GAPI _break (void *, void * Command)
{
    ::PostQuitMessage ((int) Command);
};


Bool MainDialog::On_DCommand_Ok (GWinMsg & m)
{
#if FALSE


#else

    if (! HasStyle (WS_CHILD))
    {
#if TRUE
	::PostQuitMessage (LOWORD (m.wParam));
//	GWinMsgLoop::Exit (0);
#else

	GWinMsgLoop::SetPostProc (_break, NULL, (void *) IDOK);

#endif
    }
    ::MessageBeep (MB_ICONASTERISK);

    return m.DispatchComplete ();

#endif
};

Bool MainDialog::On_DCommand_Cancel (GWinMsg & m)
{
//  ::MessageBeep (MB_ICONHAND);

    return m.DispatchComplete ();
};
//
//[]------------------------------------------------------------------------[]
//
class MainFrame : public GFrameControl
{
public :
		MainFrame () : GFrameControl (NULL),

		m_ControlBar	(NULL),
		m_MenuBar	(NULL),
		m_ToolBarMenu	(NULL),
		m_ToolBar	(NULL),

		m_TreeView	(NULL),
		m_StatusBar	(NULL),
		m_TestClient	(NULL),
		m_TestDialog	(NULL)
		{
		    m_1STimer = 0;
		};

	       ~MainFrame () { printf ("MainFrame::~MainFrame %08X\n", this);};
protected :

	Bool	    DispatchWinMsg	    (GWinMsg &);


	void	    On_Destroy		    ();
	Bool	    On_Create		    (void *);

	Bool	    On_WM_Notify	    (GWinMsg &);

	Bool	    On_WM_SysKeyDown	    (GWinMsg &);
	Bool	    On_WM_KeyDown	    (GWinMsg &);

protected :

	    TDockingWindow <GReBar>
				m_ControlBar	;

	    TDockingWindow <GToolBarMenu>
				m_MenuBar	;

	    GToolBarMenu	m_ToolBarMenu	;
	    GToolBar		m_ToolBar	;

	    GDockPane		m_TreeViewPane	;
	    GDockPane		m_TreeViewPane2 ;
	    GTreeView		m_TreeView	;

	    TestClient		m_TestClient	;

	    TDockingWindow <MainDialog>
				m_TestDialog	;

//	    GDockPane		m_StatusBarPane	;
//	    GStatusBar		m_StatusBar	;

	    UINT_PTR		m_1STimer	;

	    TDockingWindow <GStatusBar>	    
				m_StatusBar	;
};

Bool MainFrame::On_WM_Notify (GWinMsg & m)
{
    if (WM_NOTIFY == m.uMsg)
    {
    if (((NMHDR *) m.lParam) ->hwndFrom == m_ControlBar.Window ().GetHWND ())
    {
    if (((NMHDR *) m.lParam) ->code	== RBN_HEIGHTCHANGE		 )
    {{

	GRect	rw;

	m_ControlBar.Window ().GetWindowRect (& rw);
/*
	RootPane ().m_SList.extract (& m_ToolBar);

	RootPane ().InsertCtrlBar (& m_ToolBar, & m_TreeViewPane,
				   & m_ToolBar.Window (),
				     LayoutParam (5, 5, 5, 5),
				     SizeParam   (rw.cy,   0));
*/
	rw.cy -= m_ControlBar.WRectSizeY ();

	m_ControlBar.Grow (0, rw.cy);

	if (HasFlag  (GWF_NEEDUPDATE))
	{
    printf ("Update layouts <<\n\n");

	    m_RootPane.UpdateContext  ();

	    SetFlags (GWF_NEEDUPDATE, 0);

    printf ("Update layouts >>\n\n");

	}
    }}
    }
    }
	return GFrameControl::On_WM_Notify (m);
};

Bool MainFrame::On_WM_SysKeyDown (GWinMsg & m)
{
    printf ("SysKeyDown %08X %08X\n", m.wParam, m.lParam);

    return m.DispatchComplete ();
};

Bool MainFrame::On_WM_KeyDown (GWinMsg & m)
{
    printf ("KeyDown %08X %08X\n", m.wParam, m.lParam);

    if (VK_SPACE == m.wParam)
    {
//	::SetFocus (m_ToolBarMenu.GetHWND ());
    }


    return m.DispatchComplete ();
};

Bool MainFrame::DispatchWinMsg (GWinMsg & m)
{
    switch (m.uType)
    {
    case GWinMsg::WndProc :

//	printf ("MainFrame   %08X: %08X\n", m.hWnd ,
//					    m.uMsg );

	DISPATCH_WM	(WM_NOTIFY,		On_WM_Notify	    )
	DISPATCH_WM	(WM_SYSKEYDOWN,		On_WM_SysKeyDown    )
	DISPATCH_WM	(WM_KEYDOWN,		On_WM_KeyDown	    )
	DISPATCH_WM	(WM_ACTIVATE,		On_WM_Activate	    )

    if (WM_WINDOWPOSCHANGED == m.uMsg)
    {
	m_MenuBar.Window ().Send_WM (TB_AUTOSIZE, 0, 0);

	GDockParent::On_WM_PosChanged (m);

    {
	GRect	w;

	m_MenuBar.Window ().GetWindowRect (& w);

	printf ("Menu bar size %d.%d\n", w.cx, w.cy);
    }

	return m.DispatchComplete ();
    }
    if (WM_COMMAND == m.uMsg)
    {
	printf ("MainFrame WM_COMMAND %08X %08X\n", m.wParam, m.lParam);
    }
    if (WM_ENTERIDLE == m.uMsg)
    {
	printf ("MenuEnterIdle %08X ??? %08X\n", m.hWnd, ::GetCapture ());
    }

    if ((WM_COMMAND == m.uMsg) && (MMID_FILE_TEST == m.wParam))
    {{
	MainDialog  DMain;

	if (DMain.Create (this, NULL, MAKEINTRESOURCE (IDD_DIALOG1)))
	{
/*
	    DMain.ExecWindow (...
*/
	    DMain.ExecWindow ();
	};
    }}

	return GFrameControl::DispatchWinMsg (m);
    }
	return GFrameControl::DispatchWinMsg (m);
};

static void Enum (GWindow * pWindow)
{
HWND	hWnd = NULL;

printf ("-> : ");
for (hWnd = pWindow ->GetFirstChild (); hWnd; hWnd = GWindow::GetNextChild (hWnd))
{
printf ("%08X ", hWnd);
};
printf ("\n");

printf ("<- : ");
for (hWnd = pWindow ->GetLastChild (); hWnd; hWnd = GWindow::GetPrevChild (hWnd))
{
printf ("%08X ", hWnd);
};
printf ("\n");

};

void MainFrame::On_Destroy ()
{
    if (m_1STimer)
    {
	::KillTimer (NULL, m_1STimer);
			   m_1STimer = 0;
    }
};


static void CALLBACK _1STimerProc (HWND, UINT, UINT_PTR, DWORD)
{
    static int cPost = 0;

printf ("Post timer message %d\n", ++ cPost);

    ::PostMessage (NULL, 0xC000, cPost, 0x00007777);
};

Bool MainFrame::On_Create (void *)
{
    if (! GFrameControl::On_Create (NULL))
    {	return False;}


//  m_1STimer = ::SetTimer (NULL, 0, 1000, _1STimerProc);

    ::SetMenu (GetHWND (), ::LoadMenu (::GetModuleHandle (NULL), MAKEINTRESOURCE (IDR_MAIN_MENU)));


    m_RootPane.m_dFlags = DPF_VISIBLE;

    if (! m_StatusBar.Window ().Create (this, NULL,
					WS_VISIBLE | CCS_NOPARENTALIGN | CCS_BOTTOM | SBARS_SIZEGRIP, 0))
    {	return False;}

    GRect   wr = {0, 0, 0, 20};

    m_StatusBar.Window ().GetWindowRect (& wr);

    RootPane ().InsertCtrlBar (& m_StatusBar, NULL,
			       & m_StatusBar.Window (),
				 LayoutParam (5, 5, 5, 5),
				 SizeParam   (wr.cy, 0	));

/*
    GRect   rw = {0, 0, 0, 20};

    if (! m_StatusBar.Create (this, NULL,
			      WS_VISIBLE | CCS_NOPARENTALIGN | CCS_BOTTOM | SBARS_SIZEGRIP, 0))
    {	return False;}

    m_StatusBar.GetWindowRect (& rw);

    m_RootPane.InsertCtrlBar (& m_StatusBarPane, NULL	,
			      & m_StatusBar		,
				1			,
				LayoutParam (5, 5, 5, 5),
				SizeParam   (rw.cy, 0));
*/
    if (! m_TreeView.Create (this, NULL,
			     WS_VISIBLE /*| WS_CAPTION*/, 0,
			     0, 0, 0, 0))
    {	return False;}

#if TRUE

    {{
	
    }}    


#endif


    m_RootPane.Insert	     (& m_TreeViewPane, & m_StatusBar,
			      & m_TreeView		,
				SizeParam   (0, 20)	,
				LayoutParam (5, 5, 5, 5),
//				LayoutParam (10, 10, 10, 10),
				SizeParam   (80, 80));

#if TRUE


    m_TreeViewPane.m_dFlags |= 0x00001100;

    if (! m_TestClient.Create (this, _T("Test Client"),
			       WS_VISIBLE /*| WS_CAPTION*/, 0,// WS_EX_TOOLWINDOW,
			       0, 0, 0, 0))
    {	return False;}

    m_RootPane.Insert	     (& m_TreeViewPane2, & m_StatusBar,
			      & m_TestClient		,
				SizeParam   (0, 20)	,
				LayoutParam (5, 5, 5, 5),
//				LayoutParam (10, 10, 10, 10),
				SizeParam   (80, 80));

    if (! m_TestDialog.Window ().Create (this, NULL, MAKEINTRESOURCE (IDD_DIALOG2)))
    {	return False;}

    m_TestDialog.Window ().GetWindowRect (& wr);
/*
    RootPane ().InsertCtrlBar (& m_TestDialog, & m_StatusBar,
			       & m_TestDialog.Window (),
			         LayoutParam (5, 5, 5, 5),
				 SizeParam   (wr.cy, 0  ));
*/
    RootPane ().Insert	      (& m_TestDialog, & m_StatusBar,
			       & m_TestDialog.Window (),
				 SizeParam   (200, wr.cy),
				 LayoutParam (5, 5, 5, 5),
				 SizeParam   (200, wr.cy));


    m_TreeViewPane2.m_dFlags |= 0x00002200;

    m_TestDialog.m_dFlags    |= 0x00002200;

#endif


    if (! m_MenuBar.Window ().Create (this, NULL, MAKEINTRESOURCE (IDR_MAIN_MENU)))
    {	return False;}

    m_MenuBar.Window ().GetWindowRect (& wr);

    RootPane ().InsertCtrlBar (& m_MenuBar, & m_TreeViewPane,
			       & m_MenuBar.Window (),
				 LayoutParam (5, 5, 5, 5),
				 SizeParam   (wr.cy,   0));

    
	if (HasFlag  (GWF_NEEDUPDATE))
	{
	    m_RootPane.UpdateContext  ();

	    SetFlags (GWF_NEEDUPDATE, 0);
	}



    m_MenuBar.Window ().GetWindowRect (& wr);

    printf ("Menu bar size : (%08X) = %d.%d\n", m_MenuBar.Window ().GetHWND (), wr.cx, wr.cy);




	return True ;



#if TRUE
    {

    if (! m_ControlBar.Window ().Create (this, NULL,
					 WS_VISIBLE | CCS_NOPARENTALIGN | CCS_TOP | CCS_NODIVIDER | RBS_BANDBORDERS | RBS_VARHEIGHT | RBS_AUTOSIZE, 0,
					 0, 0, 0, 0))
    {	return False;}


    if (! m_ToolBarMenu.Create (& m_ControlBar.Window (), NULL, MAKEINTRESOURCE (IDR_MAIN_MENU)))
    {	return False;}

#if TRUE

    if (! m_ToolBar.Create (& m_ControlBar.Window (), NULL,
			    WS_VISIBLE | CCS_NOPARENTALIGN | CCS_TOP | CCS_NODIVIDER | TBSTYLE_FLAT | TBSTYLE_LIST, 0,
			    0, 0, 0, 0))
    {	return False;}


	TBBUTTON bt [6] = {{STD_FILENEW,  5000, TBSTATE_ENABLED, BTNS_BUTTON | BTNS_DROPDOWN | BTNS_AUTOSIZE, {0,0}, 0, (INT_PTR) "Button(&New)"},
			   {STD_FILEOPEN, 5001, TBSTATE_ENABLED, BTNS_BUTTON | BTNS_DROPDOWN | BTNS_AUTOSIZE, {0,0}, 0, (INT_PTR) "Button(&Open)"},
			   {STD_FILESAVE, 5002, TBSTATE_ENABLED, BTNS_BUTTON | BTNS_DROPDOWN | BTNS_AUTOSIZE, {0,0}, 0, (INT_PTR) "Button(&Save)"},
			   {I_IMAGENONE,  0   , TBSTATE_ENABLED, BTNS_SEP, {0,0}, 0, (INT_PTR) 0},
			   {STD_HELP,	  5003, TBSTATE_ENABLED, BTNS_BUTTON | BTNS_DROPDOWN | BTNS_AUTOSIZE, {0,0}, 0, (INT_PTR) "Button(&Help)"},
			   {I_IMAGENONE,  0   , TBSTATE_ENABLED, BTNS_SEP, {0,0}, 0, (INT_PTR) 0}};


#if FALSE

	HIMAGELIST  il = ImageList_Create (16, 16, ILC_COLORDDB | ILC_MASK, 0, 16);

	m_ToolBar.Send_WM (TB_SETIMAGELIST, (WPARAM) 0, (LPARAM) il);
	m_ToolBar.Send_WM (TB_LOADIMAGES, (WPARAM) IDB_STD_SMALL_COLOR, (LPARAM) HINST_COMMCTRL);
#else

	HIMAGELIST  il = ImageList_Create (24, 24, ILC_COLORDDB | ILC_MASK, 0, 16);

	m_ToolBar.Send_WM (TB_SETIMAGELIST, (WPARAM) 0, (LPARAM) il);
	m_ToolBar.Send_WM (TB_LOADIMAGES, (WPARAM) IDB_STD_LARGE_COLOR, (LPARAM) HINST_COMMCTRL);
#endif

	m_ToolBar.Send_WM (TB_ADDBUTTONS, (WPARAM) 6, (LPARAM) & bt);
	m_ToolBar.Send_WM (TB_AUTOSIZE  , (WPARAM) 0, (LPARAM) 0);

//	m_ToolBar.SetStyle (CCS_NORESIZE | m_ToolBar.GetStyle ());

#endif

    REBARBANDINFO   rbi;

    memset (& rbi, 0, sizeof (rbi));
    rbi.cbSize	    = sizeof (rbi) ;

    rbi.fMask	    = RBBIM_STYLE | RBBIM_CHILD | RBBIM_COLORS;
    rbi.fMask	   |= (/*RBBIM_HEADERSIZE |*/ RBBIM_SIZE | RBBIM_CHILDSIZE | RBBIM_IDEALSIZE/*| RBBIM_TEXT*/);

    rbi.fStyle	    = RBBS_BREAK | RBBS_GRIPPERALWAYS | RBBS_USECHEVRON | RBBS_CHILDEDGE | RBBS_VARIABLEHEIGHT /*| RBBS_FIXEDSIZE*/;

    rbi.clrFore	    = GetSysColor (COLOR_MENUTEXT);
    rbi.clrBack	    = GetSysColor (COLOR_WINDOW);
    rbi.hwndChild   = m_ToolBarMenu.GetHWND ();
    rbi.lpText	    = _T("Menu Bar:");

    m_ToolBarMenu.GetWindowRect (& wr);

    rbi.cxHeader    = 80;

    rbi.cxMinChild  = wr.cx;
    rbi.cx	    =
    rbi.cxIdeal	    = wr.cx;

    rbi.cyChild	    =
    rbi.cyMinChild  = wr.cy - 4;
    rbi.cyMaxChild  = 128;//wr.cy - 4;

    m_ControlBar.Window ().Send_WM (RB_INSERTBAND, (WPARAM) -1, (LPARAM) & rbi);
//
//  -------------------------------
//
    rbi.fMask	   |= RBBIM_HEADERSIZE;
    rbi.fStyle	    = RBBS_BREAK | RBBS_GRIPPERALWAYS | RBBS_USECHEVRON | RBBS_CHILDEDGE | RBBS_VARIABLEHEIGHT /*| RBBS_FIXEDSIZE*/;

    rbi.hwndChild   = m_ToolBar.GetHWND ();

    m_ToolBar.GetWindowRect (& wr);

    rbi.cxMinChild  = wr.cx;
    rbi.cx	    =
    rbi.cxIdeal	    = wr.cx;

    rbi.cyChild	    =
    rbi.cyMinChild  =
    rbi.cyMaxChild  = wr.cy - 4;

    m_ControlBar.Window ().Send_WM (RB_INSERTBAND, (WPARAM) -1, (LPARAM) & rbi);
//
//  -------------------------------
//
    rbi.fMask	   &= (~ RBBIM_CHILD);

    rbi.hwndChild   = NULL;

    rbi.fStyle	    = RBBS_BREAK | RBBS_GRIPPERALWAYS /*| RBBS_USECHEVRON*/ | RBBS_CHILDEDGE | RBBS_VARIABLEHEIGHT | RBBS_FIXEDSIZE;

//  m_ControlBar.Window ().Send_WM (RB_INSERTBAND, (WPARAM) -1, (LPARAM) & rbi);
//
//  -------------------------------
//

    m_ControlBar.Window ().GetWindowRect (& wr);

    RootPane ().InsertCtrlBar (& m_ControlBar, & m_TreeViewPane,
			       & m_ControlBar.Window (),
				 LayoutParam (5, 5, 5, 5),
				 SizeParam   (wr.cy,   0));

    }

#endif

/*
	::SetWindowPos (m_TreeView.GetHWND  (), NULL, m_TreeViewPane.WRectPosX	(),
						      m_TreeViewPane.WRectPosY	(),
						      m_TreeViewPane.WRectSizeX (),
						      m_TreeViewPane.WRectSizeY (), SWP_NOZORDER | SWP_NOREDRAW);

	::SetWindowPos (m_StatusBar.GetHWND (), NULL, m_StatusBarPane.WRectPosX	 (),
						      m_StatusBarPane.WRectPosY	 (),
						      m_StatusBarPane.WRectSizeX (),
						      m_StatusBarPane.WRectSizeY (), SWP_NOZORDER | SWP_NOREDRAW);
*/
/*
    if (m_StatusBar.HasStyle (WS_VISIBLE))
    {
	m_StatusBarPane.Show (True);


	::SetWindowPos (m_StatusBar.GetHWND (), NULL, m_StatusBarPane.m_WRect.x ,
						      m_StatusBarPane.m_WRect.y ,
						      m_StatusBarPane.m_WRect.cx / 2,
						      m_StatusBarPane.m_WRect.cy, SWP_NOZORDER | SWP_NOREDRAW);

	::Sleep (2000);
	::MessageBeep (-1);

	::SetWindowPos (m_StatusBar.GetHWND (), NULL, m_StatusBarPane.m_WRect.x ,
						      m_StatusBarPane.m_WRect.y ,
						      m_StatusBarPane.m_WRect.cx,
						      m_StatusBarPane.m_WRect.cy, SWP_NOZORDER | SWP_NOREDRAW);
	::Sleep (2000);
	::MessageBeep (-1);

    }
*/
	if (HasFlag  (GWF_NEEDUPDATE))
	{
	    m_RootPane.UpdateContext  ();

	    SetFlags (GWF_NEEDUPDATE, 0);
	}

	return True ;
};
//
//[]------------------------------------------------------------------------[]

GResult GAPI GWinMain (int, void **, HANDLE)
{
#if TRUE
    {
//	TestClient::RegisterWindowClass ();
    {{
//	MainDialog  WMain;
	CDialogSettings	  WMain;

//	if (WMain.Create (NULL, NULL, MAKEINTRESOURCE (IDD_DIALOG1)))
	if (WMain.Create (NULL, NULL, MAKEINTRESOURCE (IDD_DIALOG4)))
	{
	    printf ("Full Created....\n");

	    WMain.ExecWindow ();
	}
	else
	{
	    printf ("Can not create MainFrame\n");
	}
    }}
	    printf ("\n\n------------------\nDestroyed WMain\n");

//	    GWinMsgLoop::Run  ();
    }
#else
    {
	    MainFrame   WMain;

	    if (WMain.Create (NULL, "MainFrame... ������� ����...",
//			      WS_VISIBLE | WS_MINIMIZE | WS_OVERLAPPEDWINDOW, 0,
//					   WS_MINIMIZE | WS_OVERLAPPEDWINDOW, 0,
//			      WS_VISIBLE | WS_MAXIMIZE | WS_OVERLAPPEDWINDOW, 0,
//					   WS_MAXIMIZE | WS_OVERLAPPEDWINDOW, 0,
			      WS_VISIBLE	       | WS_OVERLAPPEDWINDOW, 0,
//	  WS_CLIPCHILDREN |   WS_VISIBLE	       | WS_OVERLAPPEDWINDOW, 0,
//							 WS_OVERLAPPEDWINDOW, 0,
//			      WS_OVERLAPPEDWINDOW & (~(WS_MAXIMIZEBOX | WS_MINIMIZEBOX)), 0,
//  WS_THICKFRAME | WS_CAPTION | WS_VISIBLE	       | WS_POPUP | WS_SYSMENU  , 
//  WS_EX_TOOLWINDOW /*| WS_EX_WINDOWEDGE*/,

//			      800, 600,  60,  60))
//			      800, 600, 300, 300))
			      100, 600, 600, 400))
	    {
		if (! WMain.IsVisible ())
		{
		      Sleep (2000);

		      WMain.ShowWindow (True, False );

		      Sleep (1000);

		      WMain.ShowWindow (False);

		      Sleep (1000);

		      WMain.ShowWindow (True);
		}
	    }
	    else
	    {
		printf ("Can not create MainFrame\n");
	    }
/*
	    MainDialog	DMain;

	    if (DMain.Create  (& WMain, NULL, MAKEINTRESOURCE (IDD_DIALOG1)))
	    {
		DMain.ShowWindow (True, False);
	    };

	    MainDialog	DMain2;

	    if (DMain2.Create (   NULL, NULL, MAKEINTRESOURCE (IDD_DIALOG1)))
	    {
		DMain2.ShowWindow (True, False);
	    };
*/
	    GWinMsgLoop::Run ();
    }
#endif

    return 0;
};

//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
class TestFrame : public GFrameControl
{
public :
		TestFrame () : GFrameControl (NULL),

		    m_ControlBar (NULL),
		    m_MenuBar	 (NULL)
		{
		    m_pControl =  NULL;
		};

	       ~TestFrame () { printf ("TestFrame::~TestFrame\n");};

protected :

	Bool	    On_Create		    (void *);

	Bool	    DispatchWinMsg	    (GWinMsg &);

	Bool	    On_WM_PosChanged	    (GWinMsg &);

protected :

	GWindow *		m_pControl  ;
	GReBar			m_ControlBar;
	GToolBarMenu		m_MenuBar   ;
};

Bool TestFrame::On_Create (void *)
{
    if (! ::SetMenu (GetHWND (), ::LoadMenu (::GetModuleHandle (NULL), MAKEINTRESOURCE (IDR_MAIN_MENU))))
    {	return False;}

    if (! GFrameControl::On_Create (NULL))
    {	return False;}

	return True ;

    if (! m_ControlBar.Create (this, NULL,
			       WS_VISIBLE | CCS_NOPARENTALIGN | CCS_TOP | CCS_NODIVIDER /*| CCS_NORESIZE*/ | RBS_BANDBORDERS | RBS_DBLCLKTOGGLE | RBS_REGISTERDROP | RBS_VARHEIGHT | RBS_AUTOSIZE, 0/*WS_EX_TOOLWINDOW*/,
			       0, 0, 0, 0))
    {	return False;}

    if (! m_MenuBar.Create (& m_ControlBar, NULL, MAKEINTRESOURCE (IDR_MAIN_MENU)))
    {	return False;}

	m_pControl = & m_ControlBar;

    GRect	    wr ;
    REBARBANDINFO   rbi;

    memset (& rbi, 0, sizeof (rbi));
    rbi.cbSize	    = sizeof (rbi) ;

    rbi.fMask	    = RBBIM_STYLE | RBBIM_CHILD /*| RBBIM_COLORS*/;
    rbi.fMask	   |= (0 /*| RBBIM_HEADERSIZE*/ | RBBIM_IDEALSIZE | RBBIM_SIZE | RBBIM_CHILDSIZE /*| RBBIM_TEXT*/);

    rbi.fStyle	    = RBBS_BREAK | RBBS_GRIPPERALWAYS | RBBS_USECHEVRON | RBBS_CHILDEDGE | RBBS_VARIABLEHEIGHT /*| RBBS_FIXEDSIZE*/;

    rbi.clrFore	    = GetSysColor (COLOR_MENUTEXT);
    rbi.clrBack	    = GetSysColor (COLOR_WINDOW);
    rbi.hwndChild   = m_MenuBar.GetHWND ();
    rbi.lpText	    = _T("Menu Bar:");

    m_MenuBar.GetWindowRect (& wr);

    rbi.cxHeader    = 80;

    rbi.cxMinChild  = 100;//wr.cx;
    rbi.cx	    = 600;
    rbi.cxIdeal	    = 400;

    rbi.cyChild	    =
    rbi.cyMinChild  =
    rbi.cyMaxChild  = m_MenuBar.GetBoundRect ();
    rbi.cyIntegral  =   0;

    m_ControlBar.Send_WM (RB_INSERTBAND, (WPARAM) -1, (LPARAM) & rbi);

//.........................
#if FALSE
    memset (& rbi, 0, sizeof (rbi));
    rbi.cbSize	    = sizeof (rbi) ;

    rbi.fMask	    = RBBIM_STYLE | RBBIM_CHILD | RBBIM_HEADERSIZE | RBBIM_IDEALSIZE | RBBIM_SIZE | RBBIM_CHILDSIZE;
    rbi.fStyle	    = RBBS_BREAK | RBBS_GRIPPERALWAYS | RBBS_USECHEVRON | RBBS_CHILDEDGE /*| RBBS_VARIABLEHEIGHT*/ /*| RBBS_FIXEDSIZE*/;

    rbi.cxHeader    = 80;

    rbi.hwndChild   = ::CreateWindowEx(0, WC_COMBOBOXEX, NULL,
					WS_BORDER | WS_VISIBLE |
					WS_CHILD | CBS_DROPDOWN,
					// No size yet--resize after setting image list.
					0,      // Vertical position of Combobox
					0,      // Horizontal position of Combobox
					0,      // Sets the width of Combobox
					100,    // Sets the height of Combobox
					m_ControlBar.GetHWND (),
					NULL,
					NULL,
					NULL);

    ::GetWindowRect (rbi.hwndChild, & wr);

    rbi.cxMinChild  = 100;//wr.cx;
    rbi.cx	    = 600;
    rbi.cxIdeal	    = 300 - 80;//wr.cx;

    rbi.cyChild	    =
    rbi.cyMinChild  =
    rbi.cyMaxChild  = wr.cy;
    rbi.cyIntegral  = 0	   ;

    m_ControlBar.Send_WM (RB_INSERTBAND, (WPARAM) -1, (LPARAM) & rbi);

#endif






    GRect   cr;

    GetClientRect (& cr);

//  m_pControl ->SetWindowPos (NULL, 11, 11, cr.cx - 22, cr.cy - 22, SWP_NOREDRAW);

    m_ControlBar.GetWindowRect (& wr);

//  m_ControlBar.Send_WM (RB_GETRECT, (WPARAM) 0, (LPARAM) & br);

    m_ControlBar.SetWindowPos  (NULL, 11, 11, cr.cx - 22, wr.cy, 0);
/*

	{{
	    printf ("<<<<<<<<<<<<<<<<Height changed !!!>>>>>>>>>>>>>>\n");

	    REBARBANDINFO   rbi;

	    memset (& rbi, 0, sizeof (rbi));
	    rbi.cbSize	    = sizeof (rbi) ;

	    rbi.fMask	    = RBBIM_CHILDSIZE;

	    m_pControl ->Send_WM (RB_GETBANDINFO, (WPARAM) 0, (LPARAM) & rbi);

//	    rbi.cyChild	    =
//	    rbi.cyMinChild  =
//	    rbi.cyMaxChild  = m_MenuBar.GetBoundRect ();

	    m_pControl ->Send_WM (RB_SETBANDINFO, (WPARAM) 0, (LPARAM) & rbi);


//	    m_pControl ->InvalidateRect (NULL, False);
	}}
*/
	return True ;
};

Bool TestFrame::On_WM_PosChanged (GWinMsg & m)
{
    if (m_pControl && ((((WINDOWPOS *) m.lParam) ->flags & SWP_NOSIZE) == 0))
    {
	int	oh, ch;
	GRect	wr, br, cr;

	printf ("\n\n...........................................\n");

	GetClientRect (& cr);

	printf ("Bound client size : %d.%d\n", cr.cx - 22, cr.cy - 22);

	m_pControl ->GetWindowRect (& br);
	printf ("Ctrl bar     size : %d.%d\n", br.cx, br.cy);
	m_pControl ->Send_WM (RB_GETRECT, (WPARAM) 0, (LPARAM) & br);
	printf ("Band bar     size : %d.%d (%d)\n", br.cx, br.cy, m_pControl ->Send_WM (RB_GETROWHEIGHT, (WPARAM) 0, (LPARAM) 0));
	m_MenuBar.GetWindowRect	   (& wr);
	printf ("Menu bar     size : %d.%d\n", wr.cx, wr.cy);
	oh = m_MenuBar.GetBoundRect  ();
	printf ("\t\t\t\tSet to %d Height\n", br.cy);
/*
	br.Pos  () = GPoint (	0,    0);
	br.Size () = GPoint (1024, 1024);

	m_pControl ->Send_WM (RB_SIZETORECT, (WPARAM) 0, (LPARAM) & br);
	printf ("BestRect     size : %d.%d (%d.%d)\n", br.x, br.y, br.cx, br.cy);
*/

//	m_MenuBar.Send_WM (TB_AUTOSIZE, (WPARAM) 0, (LPARAM) 0);

	m_pControl ->SetWindowPos (NULL, 11, 11, cr.cx - 22, cr.cy - 22, SWP_NOREDRAW);

//	m_pControl ->SetWindowPos (NULL, 11, 11, cr.cx - 22, br.cy     , SWP_NOREDRAW);

	{{
	    m_pControl ->GetWindowRect (& wr);

	    m_pControl ->SetWindowPos  (NULL, 11, 11, cr.cx - 22,
						      m_ControlBar.GetBoundRect (), SWP_NOREDRAW);

//	    m_pControl ->SetWindowPos  (NULL, wr.x, wr.y, wr.cx + ((WINDOWPOS *) m.lParam) ->cx,
//							  wr.cy + ((WINDOWPOS *) m.lParam) ->cy, SWP_NOREDRAW);
	}}









	m_pControl ->GetWindowRect (& br);
	printf ("Ctrl bar (1) size : %d.%d\n", br.cx, br.cy);
	m_pControl ->Send_WM (RB_GETRECT, (WPARAM) 0, (LPARAM) & br);
	printf ("Band bar (1) size : %d.%d\n", br.cx, br.cy);
	m_MenuBar.GetWindowRect	   (& wr);
	printf ("Menu bar (1) size : %d.%d\n", wr.cx, wr.cy);
	ch = m_MenuBar.GetBoundRect  ();
/*
	if (ch != oh)
	if (True)
	{{
	    printf ("<<<<<<<<<<<<<<<<Height changed !!!>>>>>>>>>>>>>>\n");

	    REBARBANDINFO   rbi;

	    memset (& rbi, 0, sizeof (rbi));
	    rbi.cbSize	    = sizeof (rbi) ;

	    rbi.fMask	    = RBBIM_CHILDSIZE;

	    m_pControl ->Send_WM (RB_GETBANDINFO, (WPARAM) 0, (LPARAM) & rbi);

	    rbi.cyChild	    = ch;
	    rbi.cyMinChild  = ch;
	    rbi.cyMaxChild  = ch;

	    m_pControl ->Send_WM (RB_SETBANDINFO, (WPARAM) 0, (LPARAM) & rbi);


//	    m_pControl ->InvalidateRect (NULL, False);
	}}
	else
	{
//	    m_pControl ->ValidateRect   (NULL);
	}

	    m_pControl ->InvalidateRect (NULL, False);
*/

//	m_MenuBar.SetWindowPos (NULL, 11, 11, cr.cx - 22, m_MenuBar.GetBoundRect (), 0);


	m_pControl ->GetWindowRect (& br);
	printf ("Ctrl bar (2) size : %d.%d\n", br.cx, br.cy);
	m_pControl ->Send_WM (RB_GETRECT, (WPARAM) 0, (LPARAM) & br);
	printf ("Band bar (2) size : %d.%d\n", br.cx, br.cy);
	m_MenuBar.GetWindowRect	   (& wr);
	printf ("Menu bar (2) size : %d.%d\n", wr.cx, wr.cy);
	m_MenuBar.GetBoundRect  ();

	printf ("...........................................\n\n\n");
	
    }

    return m.DispatchDefault ();
};

Bool TestFrame::DispatchWinMsg (GWinMsg & m)
{
    switch (m.uType)
    {
    case GWinMsg::WndProc :

	DISPATCH_WM	(WM_WINDOWPOSCHANGED,	On_WM_PosChanged    )

    if ((WM_COMMAND == m.uMsg) && (MMID_FILE_TEST == m.wParam))
    {{
	MainDialog  DMain;

	if (DMain.Create (this, NULL, MAKEINTRESOURCE (IDD_DIALOG1)))
	{
	    DMain.ExecWindow ();
	};
    }}

	return GFrameControl::DispatchWinMsg (m);
    };
	return GFrameControl::DispatchWinMsg (m);
};
//
//[]------------------------------------------------------------------------[]
//
GResult GAPI GWinTest (int, void **, HANDLE)
{
#if TRUE

	    CMain	WMain;

	    if (WMain.Create (NULL, _T("UrpHF Next"),
			      WS_VISIBLE | WS_OVERLAPPED
					 | WS_CAPTION
					 | WS_THICKFRAME	      ,
			      0					      ,
			      300, 200, 800, 600))
	    {}
	    WMain.ExecWindow ();

//	    GWinMsgLoop::Run ();

#endif

#if FALSE
    {
	    TestFrame * pMain = NULL;
	    TestFrame	WMain;

	    if ((pMain = new TestFrame ()) ->Create (NULL, "TestFrame... ������� ����...",
//			      WS_VISIBLE | WS_MINIMIZE | WS_OVERLAPPEDWINDOW, 0,
//					   WS_MINIMIZE | WS_OVERLAPPEDWINDOW, 0,
//			      WS_VISIBLE | WS_MAXIMIZE | WS_OVERLAPPEDWINDOW, 0,
//					   WS_MAXIMIZE | WS_OVERLAPPEDWINDOW, 0,
			      WS_VISIBLE	       | WS_OVERLAPPEDWINDOW, 0,
//							 WS_OVERLAPPEDWINDOW, 0,

//			      800, 600,  60,  60))
//			      800, 600, 300, 300))
			      300, 600, 100, 400))
	    {}

	    if (WMain.Create (pMain, _T("TestFrame... ������� ����..."),
//			      WS_VISIBLE | WS_MINIMIZE | WS_OVERLAPPEDWINDOW, 0,
//					   WS_MINIMIZE | WS_OVERLAPPEDWINDOW, 0,
//			      WS_VISIBLE | WS_MAXIMIZE | WS_OVERLAPPEDWINDOW, 0,
//					   WS_MAXIMIZE | WS_OVERLAPPEDWINDOW, 0,
//			      WS_VISIBLE	       | WS_OVERLAPPEDWINDOW, 0,
							 WS_OVERLAPPEDWINDOW, 0,

//			      800, 600,  60,  60))
//			      800, 600, 300, 300))
			      100, 600, 100, 400))
	    {}
	    WMain.ExecWindow ();

	    GWinMsgLoop::Run ();

	    if (pMain)
	    {
		delete pMain;

		       pMain = NULL;
	    }
    }
#endif

#if FALSE
    {
	    if ((new TestFrame ()) ->Create (NULL, _T("TestFrame... ������� ����..."),
//			      WS_VISIBLE | WS_MINIMIZE | WS_OVERLAPPEDWINDOW, 0,
//					   WS_MINIMIZE | WS_OVERLAPPEDWINDOW, 0,
//			      WS_VISIBLE | WS_MAXIMIZE | WS_OVERLAPPEDWINDOW, 0,
//					   WS_MAXIMIZE | WS_OVERLAPPEDWINDOW, 0,
			      WS_VISIBLE	       | WS_OVERLAPPEDWINDOW, 0,
//							 WS_OVERLAPPEDWINDOW, 0,

//			      800, 600,  60,  60))
//			      800, 600, 300, 300))
			      100, 600, 100, 400))
	    {}
    }
	    GWinMsgLoop::Run ();
#endif


    return 0;
};

/*
Bool TestFrame::On_WM_Size (GWinMsg & m)
{
    return m.DispatchComplete ();
};

Bool TestFrame::DispatchWinMsg (GWinMsg & m)
{
    return GFrameControl::DispatchWinMsg (m);
};
*/







/*

GResult GThread::RunApartment (GApartment * pa, int nArgs, void ** pArgs, HANDLE hResume)
{
    GResult	 dResult;
    GApartment * pOldApp;

    pOldApp =  m_pCurApartment	   ;
	       m_pCurApartment = pa;

    pa ->m_pCoThread   = this;

    pa ->m_XUnknown  .Init    (pa);
    pa ->m_IApartment.Init    (pa, IID_IApartment, & pa ->m_XUnknown);

    pa ->m_XUnknown  .Release ();

    if (m_pWakeUpPort)
    {
	m_pWakeUpPort ->EnterWaitLoop ();
    }

    dResult =  pa ->Main (nArgs, pArgs, hResume);

    pa ->m_IApartment.Release ();

    while (0 < pa ->GetRefCount   ())
    {
	if (!  pa ->PumpInputWork ())
	{
	       pa ->WaitInputWork (INFINITE);
	}
    }

    if (m_pWakeUpPort)
    {
	m_pWakeUpPort ->LeaveWaitLoop ();
    }
	       pa ->Done ();

    pa ->m_pCoThread   = NULL;

	       m_pCurApartment = pOldApp;

    return dResult;
};

GResult GAPI RunApartment (GApartment * pa, int nArgs, void ** pArgs, HANDLE hResume)
{
    GResult	   dResult;
    GThread * th = TCurrent ();

    if (th && pa)
    {
	if (GThread::User == th ->GetType ())
	{
	    dResult = th ->RunApartment (pa, nArgs, pArgs, hResume);
	}
	else
	{
//	    return Run
	}
    }
    else
    {
	    dResult = ERROR_INVALID_PARAMETER;
    }

    return  dResult;
};
*/

/*
GResult GAPI CRRPDevClient::Main (int void **, HANDLE hResume)
{
};

    a.Run (TCurrent (), 0, NULL, hResume);

	   RunApartment (

    Application a (NULL)

    return new (newAllocator) Application ->Run (0, NULL, hResume);
*/

int __main (int nArgs, void ** pArgs)
{
    int	iResult = -1;

#if FALSE

    if (GWinInit ())
    {
	if (InitCtrl32 (ICC_BAR_CLASSES | ICC_COOL_CLASSES))
	{
	    TestClient::RegisterWindowClass ();

	    printf ("%d - %d\n", sizeof (GWinProcThunk), sizeof (GWinMsgHandler));

//	    PrimaryThread <TWinMsgThread <GWinMsgLoop> > (GWinMain, NULL, NULL);

	    PrimaryThread <TWinMsgThread <GWinMsgLoop> > (GWinTest, NULL, NULL);

#if FALSE

#if TRUE
	    CreateThread  <TWinMsgThread <GWinMsgLoop> > (NULL	  ,
							  GWinMain, NULL, NULL);
#else
	    HANDLE	   hGUI;

	    CreateThread  <TWinMsgThread <GWinMsgLoop> > (& hGUI  ,
							  GWinMain, NULL, NULL);

	    PrimaryThread <TWinMsgThread <GWinMsgLoop> > (GWinTest, NULL, NULL);

	    ::WaitForAny  (1, hGUI, INFINITE);

	    ::CloseHandle (hGUI);
#endif

#endif
	    iResult = 0;
	}
	else
        {
	    printf ("Can not init Shell controls\n");
	}

	GWinDone ();

	    printf ("Done...\n");
    }
    else
    {
	    printf ("Can not init GWin\n");
    }

#endif

    return iResult;
};
//
//[]------------------------------------------------------------------------[]















class GIdleLoop
{
public :

virtual	void *		Run		();

virtual	Bool		WaitInputWork	(DWord dTimeout = INFINITE);

virtual	Bool		PumpInputWork	();

protected :

	Bool		m_bExitFlag	;
	void *		m_pExitCode	;
};


Bool GIdleLoop::WaitInputWork (DWord dTimeout)
{
    if (! m_bExitFlag)
    {
	return (TCurrent () ->GetCurApartment () ->WaitInputWork (dTimeout), True);
    }
	return False;
};

/*
void * GIdleLoop::Run ()
{
    do
    {
	do {} while (! PumpInputWork ());

    }	      while (

};
*/

#if FALSE

Bool GInputLoop::WaitInputWork (DWord dTimeout)
{
    if (m_bExitFlag)
    {
	return (m_pParentLoop)
    }

};



Bool GApartment::RunInputLoop ()
{
    do 
    {
	do {} while (! PumpInputWork ());

    } 	      while (m_pTopInputLoop ->WaitInputWork (INFINITE);
};





    while (! 


#endif


//[]------------------------------------------------------------------------[]
//
class GWinMsgLoop
{
public :

	void		AddPreprocHandler	(GWindow *);

	void		RemovePreprocHandler	(GWindow *);

	INT_PTR		Exec			();

virtual	void		Exit			(INT_PTR  );

protected :

	int		    m_cPreprocArr	;
	GWindow *	    m_pPreprocArr [32]	;
};

/*

void 


void CMain::On_DialogSettings (GWinMsgHandler * h, GWindow * pSoruce, GWinMsg & m)
{

    while (True)
    {
	if (! PumpInputWork ())
	{
	      WaitInputWork (INFINITE);
	}
    };
};


    while (;;)
    {
    }

    if (WM_COMMAND == m.uMsg)
    {
	if (IS_WM_COMMAND_ID (IDCLOSE))
	{
	    ExecExit ();
	};
    }
}

    typedef Bool    (GAPI * GWinPreproc )(void *, MSG *);

    struct GWinPreprocHandler
    {
	    GWinPreproc		m_pProc ;
	    void *		m_oProc ;
    }
}

Bool CMain::On_DialogSettings (GWinMsg & m)


Bool CMain::On_FileTest (GWinMsg &)
{
    Exec (this, 

    INT_PTR dResult = -1;

    CDialogSettings Dlg	;

    

    if (Dlg.Create (this, NULL, MAKEINTRESOURCE (IDD_DIALOG4)))
    {
	dResult = ExecWindow (Dlg, GWinMsgHandler *);
    }
};

    ...


    INT_PTR uResult = (GWinMsgApartment *) TCurrent () ->GetCurApartment () ->Run (MsgLoop);
}

*/


void GWinMsgLoop::AddPreprocHandler (GWindow * pWindow)
{
    if (m_cPreprocArr < (sizeof (m_pPreprocArr) / sizeof (GWindow *)))
    {
	m_pPreprocArr [m_cPreprocArr] = pWindow;

        m_cPreprocArr ++;
    }
};

void GWinMsgLoop::RemovePreprocHandler (GWindow * pWindow)
{
    for (int i = 0; i < m_cPreprocArr; i ++)
    {
    if (m_pPreprocArr [i] == pWindow)
    {
	if (i + 1  < m_cPreprocArr)
	{
	    memmove (m_pPreprocArr + i, m_pPreprocArr + i + 1, (Byte *)(m_pPreprocArr + m_cPreprocArr) -
							       (Byte *)(m_pPreprocArr + i + 1	     ));
	}

	m_cPreprocArr --;

        return;
    }
    }
};




























//[]------------------------------------------------------------------------[]
//
#ifdef GSH_REMOVE

class GWinMsgLoop;

class GWinMsgThread : public GThread
{
friend class GWinMsgLoop;

protected :
			GWinMsgThread	(GWinMsgLoop * pWinMsgLoop)
			{
			    m_pWinMsgLoop = pWinMsgLoop;
			};

friend	GWinMsgLoop *	GetWinMsgLoop	();

private :

	GWinMsgLoop *	    m_pWinMsgLoop ;
};

GINLINE GWinMsgLoop *	GetWinMsgLoop	() 
			{
			    return ((GWinMsgThread *) TCurrent ()) ->m_pWinMsgLoop;
			};
//
//[]------------------------------------------------------------------------[]
//
class GWinMsgLoop
{
public :

typedef void (GAPI *	PostProc)(void *, void *);

static	void  GAPI	RegisterPreprocHandler	     (GWinMsgProc pProc, void * oProc);

static	void  GAPI	UnRegisterPreprocHandler     (GWinMsgProc pProc, void * oProc);

static	int   GAPI	IncLockCount () { return  ++ (GetWinMsgLoop () ->m_cLockCount);};

static	int   GAPI	DecLockCount () { return  -- (GetWinMsgLoop () ->m_cLockCount);};

static	void  GAPI	Run	     () { GetWinMsgLoop () ->Run 
						   (& GetWinMsgLoop () ->m_cLockCount);};
static	DWord GAPI	Exec	     ();
/*
static	void  GAPI	Exit	     (DWord dExitCode);
*/
static	void  GAPI	SetPostProc  (PostProc, void *, void * = NULL);

static	void  GAPI	BegModalLoop ();

static	void  GAPI	EndModalLoop ();

			GWinMsgLoop  ()
			{
			    m_pExitCode   = NULL;
			    m_pExitFlag   = NULL;

			    m_cLockCount  =    0;

			    m_pPostProc	  = NULL;
			    m_oPostProc	  =
			    m_cPostProc	  = NULL;

//			    m_cGetMsgHook =    0;
//			    m_hGetMsgHook = NULL;
			};

//static	void  GAPI	Post_GM	     ()

protected :

virtual	void		WaitMessage	    (	   DWord    dTimeout);

	Bool		PreprocMessage	    (MSG &);

static	DWord		WaitPerform	    (      int	    nWait   ,
					     const HANDLE * pWait   ,
						   DWord    dTimeout);
private :

	DWord 		    Run		    (int *);

static	LRESULT CALLBACK    GetMessageHook  (int, WPARAM, LPARAM);

private :

	    int   *		    m_pExitFlag	 ;
	    DWord *		    m_pExitCode	 ;

	    int			    m_cLockCount ;

	    GWinMsgHandler::SList   m_SPreproc	 ;

	    PostProc		    m_pPostProc	 ;
	    void *		    m_oPostProc	 ;
	    void *		    m_cPostProc	 ;

//	    int			    m_cGetMsgHook  ;
//	    HHOOK		    m_hGetMsgHook   ;
};

template <class T = GWinMsgLoop> class TWinMsgThread : public GWinMsgThread
{
public  :
			TWinMsgThread () : GWinMsgThread (& m_WinMsgLoop)
			{};
private :

	    T			    m_WinMsgLoop;
};

#endif
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
#ifdef GSH_REMOVE

DWord GWinMsgLoop::WaitPerform (int nWait, const HANDLE * pWait, DWord dTimeout)
{
    return ::MsgWaitForMultipleObjectsEx (nWait, pWait, dTimeout ,
					  QS_ALLINPUT
					| QS_ALLPOSTMESSAGE
					| 0			 ,  // Wakeup by any event in the queue
					  MWMO_ALERTABLE
    					| MWMO_INPUTAVAILABLE	    // Alertable & No wait if has input !!!
					| 0);
};

void GWinMsgLoop::WaitMessage (DWord dTimeout)
{
    WaitPerform (0, NULL, dTimeout);
};

/*
Bool GWinMsgLoop::DispatchMessage (MSG & msg)
{
    if (NULL == msg.hwnd)
    {
	printf ("Null HWND Message !!! %08X %08X %08X\n", msg.message, msg.wParam, msg.lParam);

	::MessageBeep (MB_ICONHAND);

	return (::DispatchMessage (& msg), True);
    }

	return False;
};
*/

void GAPI GWinMsgLoop::RegisterPreprocHandler	(GWinMsgProc pProc, void * oProc)
{
    GWinMsgLoop	   * pLoop = GetWinMsgLoop ();

    pLoop ->m_SPreproc.insert (new GWinMsgHandler (pProc, oProc)) ->m_PList = 
  & pLoop ->m_SPreproc;
};

void GAPI GWinMsgLoop::UnRegisterPreprocHandler (GWinMsgProc pProc, void * oProc)
{
    GWinMsgLoop * pLoop = GetWinMsgLoop ();

    for (TXList_Iterator <GWinMsgHandler> 
	I   = pLoop ->m_SPreproc.head (); I.next ();)
    {
	if ((I.next () ->m_pProc == pProc)
	&&  (I.next () ->m_oProc == oProc))
	{
	    delete pLoop ->m_SPreproc.extract (I);

	    break;
	}
	I <<= pLoop ->m_SPreproc.getnext (I.next ());
    };
};

void GAPI GWinMsgLoop::SetPostProc (PostProc pPostProc, void * oPostProc, void * pContext)
{
    GWinMsgLoop * pLoop = GetWinMsgLoop ();

    pLoop ->m_pPostProc = pPostProc;
    pLoop ->m_oPostProc = oPostProc;
    pLoop ->m_cPostProc = pContext ;
};




/*
static LRESULT CALLBACK GetMsgHookProc (int code, WPARAM wParam, LPARAM lParam)
{
    if (HC_ACTION == code)
    {
    if (PM_REMOVE == wParam)
    {
    printf ("GetMsgHook:: %08X, %08X %08X %08X\n", ((MSG *) lParam) ->hwnd   ,
						   ((MSG *) lParam) ->message,
						   ((MSG *) lParam) ->wParam ,
						   ((MSG *) lParam) ->lParam );

    }
    }
    return ::CallNextHookEx (NULL, code, wParam, lParam);
};

    HHOOK	h = ::SetWindowsHookEx (WH_GETMESSAGE, GetMsgHookProc, NULL, ::GetCurrentThreadId ());

    ::UnhookWindowsHookEx (h);
*/

LRESULT CALLBACK GWinMsgLoop::GetMessageHook (int code, WPARAM wParam, LPARAM lParam)
{
    if ((HC_ACTION == code) && (PM_REMOVE == wParam) && (NULL != lParam))
    {
    printf ("GetMsgHook:: %08X, %08X %08X %08X\n", ((MSG *) lParam) ->hwnd   ,
						   ((MSG *) lParam) ->message,
						   ((MSG *) lParam) ->wParam ,
						   ((MSG *) lParam) ->lParam );

//	if (GetWinMsgLoop () ->PreprocMessage (* (MSG *) lParam))
//	{
//	}
    }
    return ::CallNextHookEx (NULL, code, wParam, lParam);
};

/*
void GWinMsgLoop::BegModalLoop ()
{
    GWinMsgLoop * pLoop = GetWinMsgLoop ();

    if (0 == pLoop ->m_cGetMsgHook)
    {
	pLoop ->m_cGetMsgHook += ((NULL != 
       (pLoop ->m_hGetMsgHook  = ::SetWindowsHookEx (WH_GETMESSAGE , 
						     GetMessageHook, 
						     NULL, TCurrent () ->GetId ()))) ? 1 : 0);
    }
};

void GWinMsgLoop::EndModalLoop ()
{
    GWinMsgLoop * pLoop = GetWinMsgLoop ();

    if (0 == -- pLoop ->m_cGetMsgHook)
    {
	::UnhookWindowsHookEx (pLoop ->m_hGetMsgHook);
			       pLoop ->m_hGetMsgHook = NULL;
    }
};
*/

DWord GAPI GWinMsgLoop::Exec ()
{
    int	   bExitFlag = 1;

    return GetWinMsgLoop () ->Run  (& bExitFlag);
};

/*
void GAPI GWinMsgLoop::Exit (DWord dExitCode)
{
    GWinMsgLoop * pLoop = GetWinMsgLoop ();

    if (pLoop ->m_pExitCode)
    {
      * pLoop ->m_pExitCode = dExitCode;
    }
    if (pLoop ->m_pExitFlag
    && (pLoop ->m_pExitFlag != & pLoop ->m_cLockCount))
    {
      * pLoop ->m_pExitFlag = 0;
    }
};
*/


#if FALSE

DWord GWinMsgLoop::Exec (GWindow *)
{
    GWinMsgLoop	Loop;

    while (pWindow.HasFalg
};



#endif






DWord GWinMsgLoop::Run (int * pExitFlag)
{
    MSG		msg ;

    DWord	dExitCode = 0;

    int   *	pOldExitFlag = m_pExitFlag ;
    DWord *	pOldExitCode = m_pExitCode ;

    PostProc	pPostProc ;
    void  *	oPostProc ;
    void  *	cPostProc ;

    m_pExitFlag =   pExitFlag  ;
    m_pExitCode = & dExitCode  ;

    while (True)
    {
	while ((pPostProc = m_pPostProc) != NULL)
	{
		oPostProc = m_oPostProc;
		cPostProc = m_cPostProc;
			    m_pPostProc = NULL;
			    m_oPostProc = NULL;
			    m_cPostProc = NULL;
	    
	       (pPostProc)(oPostProc, cPostProc);
	};

//	if (* pExitFlag)
	{
	    if (::PeekMessage (& msg, NULL, 0, 0, PM_REMOVE))
	    {
#if TRUE
		if (WM_QUIT == msg.message)
		{
		    dExitCode = msg.wParam;

printf ("\tQuit exit %08X...................................\n", dExitCode);
		    break;
		}
#endif

printf ("<<Process for msg : %08X %08X [%08X,%08X]..................\n", msg.hwnd, msg.message, msg.wParam, msg.lParam);

if (::GetCapture ())
printf ("\t\tWith Capture : %08X\n", ::GetCapture ());


		if (PreprocMessage  (  msg))
		{	
printf (">>Process by IsDialogMessage\n");

			continue;
		}
		::TranslateMessage  (& msg);
		::DispatchMessage   (& msg);
printf (">>Process by DispatchMessage\n");
		{	continue;}
	    }

printf ("\t\t\t\t<<Wait");

	    WaitMessage  (INFINITE);

printf (">>\n");

			continue;
	}
			break;
    };

    m_pExitFlag = pOldExitFlag;
    m_pExitCode = pOldExitCode;

	return	dExitCode;
};


#endif
//
//[]------------------------------------------------------------------------[]

/*
BOOL [!output WTL_FRAME_CLASS]::PreTranslateMessage(MSG* pMsg)
{
[!if WTL_APPTYPE_MDI]
	if([!output WTL_FRAME_BASE_CLASS]<[!output WTL_FRAME_CLASS]>::PreTranslateMessage(pMsg))
		return TRUE;

	HWND hWnd = MDIGetActive();
	if(hWnd != NULL)
		return (BOOL)::SendMessage(hWnd, WM_FORWARDMSG, 0, (LPARAM)pMsg);

	return FALSE;
[!else]
[!if WTL_USE_VIEW]
	if([!output WTL_FRAME_BASE_CLASS]<[!output WTL_FRAME_CLASS]>::PreTranslateMessage(pMsg))
		return TRUE;

	return m_view.PreTranslateMessage(pMsg);
[!else]
	return [!output WTL_FRAME_BASE_CLASS]<[!output WTL_FRAME_CLASS]>::PreTranslateMessage(pMsg);
[!endif]
[!endif]
}





BOOL [!output WTL_MAINDLG_CLASS]::PreTranslateMessage(MSG* pMsg)
{
[!if WTL_HOST_AX]
	if((pMsg->message < WM_KEYFIRST || pMsg->message > WM_KEYLAST) &&
	   (pMsg->message < WM_MOUSEFIRST || pMsg->message > WM_MOUSELAST))
		return FALSE;

	HWND hWndCtl = ::GetFocus();
	if(IsChild(hWndCtl))
	{
		// find a direct child of the dialog from the window that has focus
		while(::GetParent(hWndCtl) != m_hWnd)
			hWndCtl = ::GetParent(hWndCtl);

		// give control a chance to translate this message
		if(::SendMessage(hWndCtl, WM_FORWARDMSG, 0, (LPARAM)pMsg) != 0)
			return TRUE;
	}

[!endif]
	return IsDialogMessage(pMsg);
}



BOOL [!output WTL_VIEW_CLASS]::PreTranslateMessage(MSG* pMsg)
{
[!if WTL_HOST_AX]
	if((pMsg->message < WM_KEYFIRST || pMsg->message > WM_KEYLAST) &&
	   (pMsg->message < WM_MOUSEFIRST || pMsg->message > WM_MOUSELAST))
		return FALSE;

	HWND hWndCtl = ::GetFocus();
	if(IsChild(hWndCtl))
	{
		// find a direct child of the dialog from the window that has focus
		while(::GetParent(hWndCtl) != m_hWnd)
			hWndCtl = ::GetParent(hWndCtl);

		// give control a chance to translate this message
		if(::SendMessage(hWndCtl, WM_FORWARDMSG, 0, (LPARAM)pMsg) != 0)
			return TRUE;
	}

[!endif]
[!if WTL_VIEWTYPE_HTML]
	if((pMsg->message < WM_KEYFIRST || pMsg->message > WM_KEYLAST) &&
	   (pMsg->message < WM_MOUSEFIRST || pMsg->message > WM_MOUSELAST))
		return FALSE;

	// give HTML page a chance to translate this message
	return (BOOL)SendMessage(WM_FORWARDMSG, 0, (LPARAM)pMsg);
[!else]
[!if WTL_VIEWTYPE_FORM]
	return IsDialogMessage(pMsg);
[!else]
	pMsg;
	return FALSE;
[!endif]
[!endif]
}
*/