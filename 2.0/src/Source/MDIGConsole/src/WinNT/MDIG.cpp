//[]------------------------------------------------------------------------[]
// MDIGConsole MDIG dialog defintion
//
//
// Created by GSh.
//[]------------------------------------------------------------------------[]

#include "MDIGUtils.h"

//[]--------------------------------------------------------------------------
//
void SaveFLR (HANDLE f, IRRPMDIG * pIRRPMDIG)
{
	int	iWidth  = (int) pIRRPMDIG ->GetParam (IRRPMDIG::ID_ParImageWidth );
	int	iHeight = (int) pIRRPMDIG ->GetParam (IRRPMDIG::ID_ParImageHeight);

	FLRFILEHEADER flrHead;
	DWORD		  t;

	Word	pBuf [6 * 1024];

	flrHead.ffType	 = FLR_HEADER_MARKER;

	flrHead.ffWidth	 = iWidth  ;
	flrHead.ffHeight = iHeight ;

	flrHead.ffBpp	     = 16;
	flrHead.ffBitsOffset =  0;
    {
	::WriteFile (f, & flrHead, sizeof (flrHead)    , & t, NULL);

	for (int i = 0; i < iHeight; i ++)
	{
	    pIRRPMDIG ->GrabImage (pBuf, iWidth * 2, i, 1);

	    ::WriteFile (f, pBuf, iWidth * 2, & t, NULL);

	if (0 == (i % 5))
	{
 	    while (TCurrent () ->GetCurApartment () ->PumpInputWork ())
	    {
		   TCurrent () ->GetCurApartment () ->Work ();
	    }
		   TCurrent () ->GetCurApartment () ->WaitInputWork (0);
	}
	}
    }
};

void SaveImage (HWND hParent, IRRPMDIG * pIRRPMScan)
{
    HANDLE  hFile			;
    TCHAR   pFileName [MAX_PATH] = {""} ;

    if (pIRRPMScan)
    {{
    if (PromptFileName (hParent, pFileName, MAX_PATH, "flr\0*.flr\0"))
    {
    if (INVALID_HANDLE_VALUE != (hFile = ::CreateFile 
				(pFileName		,
				 GENERIC_WRITE		,
				 0			,
				 NULL			,
				 CREATE_ALWAYS		,
				 FILE_ATTRIBUTE_NORMAL 
			       | FILE_FLAG_SEQUENTIAL_SCAN,
				 NULL		       )))
    {
	HCURSOR	       hOld = 
	::SetCursor   (::LoadCursor (NULL, IDC_WAIT));

	SaveFLR	      (hFile, pIRRPMScan);	    

	::SetCursor   (hOld);

	::CloseHandle (hFile);
    }
    }
    }}
};
//
//[]--------------------------------------------------------------------------
//
void SendImage (HWND hParent, IRRPMDIG * pIRRPMScan)
{
    HANDLE  hFile ;

    if (pIRRPMScan)
    {{
    SECURITY_ATTRIBUTES sa = {sizeof (SECURITY_ATTRIBUTES), NULL, True};

    TCHAR   pPathName [MAX_PATH] = {""} ;
    TCHAR   pFileName [MAX_PATH] = {""} ;

    ::GetTempPath (sizeof (pPathName) / sizeof (TCHAR), pPathName);
    ::GetTempFileName	  (pPathName, _T("clr"),     0,	pFileName);

    if (True)
    {
    if (INVALID_HANDLE_VALUE != (hFile = ::CreateFile 
				(pFileName	       ,
			         GENERIC_WRITE
			       | GENERIC_READ	       ,
				 FILE_SHARE_READ       ,
			       & sa		       ,
				 CREATE_ALWAYS	       ,
				 FILE_ATTRIBUTE_NORMAL 
			       | FILE_FLAG_SEQUENTIAL_SCAN
			       | FILE_FLAG_DELETE_ON_CLOSE,
				 NULL		       )))
    {
	HCURSOR	       hOld = 
	::SetCursor   (::LoadCursor (NULL, IDC_WAIT));

	SaveFLR	      (hFile, pIRRPMScan);	    

	ExecContrlEng (hFile);

	::SetCursor   (hOld);

	::CloseHandle (hFile);
    }
    }
    }}
};
//
//[]--------------------------------------------------------------------------

//[]------------------------------------------------------------------------[]
//
class CToolTipEx : public GWindow
{
DECLARE_EXT_WINDOW_CLASS (_T("GWin.CToolTipEx"), 0);

public :
		CToolTipEx () : GWindow (NULL)
		{
		    m_uStatus	= 0;
		};

	Bool		Create	    (GWindow *	pParent )
			{
			    if (GWindow::Create (pParent       ,
						 NULL	       ,
			    			 WS_POPUP      ,
						 WS_EX_TOOLWINDOW 
					       | WS_EX_LAYERED ,
						 0, 0, 400, 100,
						 NULL	       ))
			    {
				::SetLayeredWindowAttributes  
			       (GetHWND (), 0, (128 + 32), LWA_ALPHA);

				return True;
			    }
				return False;
			};

protected :

	void		On_Paint	    (GWinMsg &);

	Bool		On_WinMsg	    (GWinMsg &);

public :

	_U32		m_uStatus   ;
};

DEFINE_EXT_WINDOW_CLASS (CToolTipEx);

void CToolTipEx::On_Paint (GWinMsg & m)
{
    HDC		    dc;
    PAINTSTRUCT	    ps;

    int		    i ;
    TCHAR	  * pHead;
    TCHAR	    pText [4096];

    if (NULL == (dc = ::BeginPaint (GetHWND (), & ps)))
    	return;

    RECT	    rc;

    static struct   Report
    {
	_U32	    uMask    ;
	LPCTSTR	    pMessage ;

    }		    ReportTbl [] =
    {
	{0x00000001, LoadString (IDS_MDIGNR_00000001)},
	{0x00000002, LoadString (IDS_MDIGNR_00000002)},
	{0x00000004, LoadString (IDS_MDIGNR_00000004)},
	{0x00000010, LoadString (IDS_MDIGNR_00000010)},
	{0x00000020, LoadString (IDS_MDIGNR_00000020)},
	{0x00000040, LoadString (IDS_MDIGNR_00000040)},
	{0x00000100, LoadString (IDS_MDIGNR_00000100)},
	{0x00000200, LoadString (IDS_MDIGNR_00000200)},
	{0x00000400, LoadString (IDS_MDIGNR_00000400)},
	{0x00000800, LoadString (IDS_MDIGNR_00000800)},
	{0x00001000, LoadString (IDS_MDIGNR_00001000)},
	{0x00002000, LoadString (IDS_MDIGNR_00002000)},
	{0x00010000, LoadString (IDS_MDIGNR_00010000)},
	{0x00000000, NULL}
    };

    for (i = 0, * (pHead = pText) = '\0'; i < 32; i ++)
    {
	if (m_uStatus &  (((_U32) 1) << i))
	{
    for (Report * r = ReportTbl; r ->uMask; r ++)
    {
	if (r ->uMask == (((_U32) 1) << i))
	{
	pHead += _snprintf (pHead, (pText + sizeof (pText)) - pHead, "%08X : %s\n", r ->uMask, r ->pMessage);
	goto lNext;
	}
    }
	pHead += _snprintf (pHead, (pText + sizeof (pText)) - pHead, "%08X : ���������� ��� ������������ ������������.\n", (((_U32) 1) << i));
	}
lNext:;
    }

    GetClientRect  (& rc);
    Move (& rc, 10, 10);

    ::FillRect	    (dc, & ps.rcPaint, (HBRUSH) ::GetStockObject (BLACK_BRUSH));

    ::SetTextColor  (dc, RGB (255, 255, 255));
    ::SetBkMode	    (dc, TRANSPARENT);
    ::DrawText	    (dc, pText,
		    -1,
		  & rc, DT_LEFT);

    ::EndPaint  (GetHWND (), & ps);
};

Bool CToolTipEx::On_WinMsg (GWinMsg & m)
{
    if (WM_PAINT == m.uMsg)
    {
	return (On_Paint (m), True);
    }
	return GWindow::On_WinMsg (m);
};
//
//[]------------------------------------------------------------------------[]



//[]------------------------------------------------------------------------[]
//
class CMDIGImage : public GDialog
{
	friend class CMDIGPanel;

public :
		CMDIGImage () : GDialog (NULL)
		{
		    m_hWait	= NULL;
		    m_pIRRPMDIG = NULL;
		};

protected :

	Bool		On_Create	    (void *);

	Bool		On_DlgMsg	    (GWinMsg &);

protected :

	UINT_PTR	m_hWait	    ;

	IRRPMDIG *	m_pIRRPMDIG ;
};

Bool CMDIGImage::On_Create (void * pParam)
{
    if (! GDialog::On_Create (pParam))
    {	return False;}

    m_hWait	    = ::SetTimer (m_hWnd, 1, 40, NULL);

	return True ;
};

Bool CMDIGImage::On_DlgMsg (GWinMsg & m)
{
    if ((WM_TIMER == m.uMsg) && (1 == m.wParam))
    {{
	GResult	dResult	 ;

	int	iPhase   ;
	int	iStep    ;
	int	iStepNum ;

	if (ERROR_SUCCESS == 
	   (dResult	   = m_pIRRPMDIG ->PeekImageStatus (& iPhase, & iStep, & iStepNum)))
	{
	if (0		  != iPhase)
	{
	    return True;
	}
	}
	    ::KillTimer	    (m_hWnd, 1);

	    ::SetWindowLong (GetHWND (), GWL_USERDATA, dResult);
	    return (TCurrent () ->GetCurApartment () ->BreakFromWorkLoop (), True);
    }}
	    return GDialog::On_DlgMsg (m);
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
class CMDIGShot : public GDialog
{
	friend class CMDIGPanel;

public :
		CMDIGShot () : GDialog (NULL)
		{
		    m_pMDIGPanel = NULL;
		};
protected :

	Bool		On_DlgMsg	    (GWinMsg &);

protected :

	CMDIGPanel *	    m_pMDIGPanel;

};

Bool CMDIGShot::On_DlgMsg (GWinMsg & m)
{
    int		nCtrlId;

    if (0 !=   (nCtrlId = IsCommand (m)))
    {
	switch (nCtrlId)
	{
	case IDOK     :
	case IDCANCEL :

	    ::SetWindowLong (GetHWND (), GWL_USERDATA, (nCtrlId));
	    return (TCurrent () ->GetCurApartment () ->BreakFromWorkLoop (), True);
	};
    }
	    return GDialog::On_DlgMsg (m);
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
class CMDIGPanel : public GDialog
{
	friend class CMDIGShot;

public :
		CMDIGPanel () : GDialog (NULL)
		{
		    m_pIRRPMDIG	    = NULL ;
		    m_hIRRPCallBack = NULL ;

		    m_pShotDialog   = NULL ;
		    m_hShotCallBack = NULL ;

		    m_bHVOn	    = False;
		    m_nHVPhase	    = 0	   ;
		};

	void		SetEquMask		(_U32 dEquMask);

protected :

	Bool		PreprocMessage		(MSG *);

//	void		On_SetEquMask		();
	void		On_LockThick		();

	void		On_SetFieldSize		();

	void		On_SetPressMode		(int nIndex);
	void		On_SetFocusSize		(int nIndex);
	void		On_SetExposeMode	(int nIndex);

	Bool		On_Edit			(GWinMsg &);

	void		On_SetThick		(GWinMsg &);

	void		On_SetShot		();
	void		On_SaveImage		();
	void		On_Decompress		();

	Bool		On_DlgMsg		(GWinMsg &);

	Bool		On_InitDialog		(GWinMsg &);

	void		On_Destroy		();
	Bool		On_Create		(void *);

	Bool		On_WM_MouseMove		(GWinMsg &);

protected :

	void		On_IRRPParam		(_U32, _U32);
	void		On_IRRPCallBack		(IRRPCallBack::Handle *);

protected :

	CLedIndicator		m_oLedHVReady	;

	IRRPMDIG *		m_pIRRPMDIG	;

	CWndCallBack		m_sIRRPCallBack ;
	IRRPCallBack::Handle *	m_hIRRPCallBack	;

	CEditHandler		m_hEdit		;

	CMDIGShot *		m_pShotDialog	;
	IRRPCallBack::Handle *	m_hShotCallBack ;

	Bool			m_bHVOn		;
	int			m_nHVPhase	;

	CToolTipEx		m_StatusPopup	;
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
void CMDIGPanel::On_Destroy ()
{
    if (m_pIRRPMDIG)
    {
	m_hIRRPCallBack ->Close ();

	m_pIRRPMDIG ->Release ();
	m_pIRRPMDIG = NULL;
    }
	m_StatusPopup.Destroy ();
};

Bool CMDIGPanel::On_InitDialog (GWinMsg & m)
{
    return m.DispatchCompleteDlgEx (False);
};

Bool CMDIGPanel::PreprocMessage (MSG * msg)
{
    m_hEdit.PreprocMessage (msg);

    return GDialog::PreprocMessage (msg);
};

Bool CMDIGPanel::On_Create (void * pParam)
{
    if (! GDialog::On_Create (pParam))
    {	return False;}

	m_StatusPopup.Create (this);

    SendCtrlMessage (IDC_MDIG_FIELD, CB_ADDSTRING, (WPARAM) 0, (LPARAM) LoadString (IDS_FIELD_FULL));
    SendCtrlMessage (IDC_MDIG_FIELD, CB_ADDSTRING, (WPARAM) 0, (LPARAM) _T("  21 x 30"));
    SendCtrlMessage (IDC_MDIG_FIELD, CB_ADDSTRING, (WPARAM) 0, (LPARAM) _T("  18 x 24"));
    SendCtrlMessage (IDC_MDIG_FIELD, CB_ADDSTRING, (WPARAM) 0, (LPARAM) _T("  12 x 12"));
    SendCtrlMessage (IDC_MDIG_FIELD, CB_SETCURSEL, (WPARAM) 0, (LPARAM) 0);

    SendCtrlMessage (IDC_MDIG_THICK_SLD, TBM_SETRANGE	, (WPARAM) 1, (LPARAM) MAKELONG (0, 210));
    SendCtrlMessage (IDC_MDIG_THICK_SLD, TBM_SETPAGESIZE, (WPARAM) 0, (LPARAM) 10);

    for (int i = 210/10; i < 210; i += (210/10))
    SendCtrlMessage (IDC_MDIG_THICK_SLD, TBM_SETTIC	, (WPARAM) 0, (LPARAM) i);

    m_oLedHVReady.SetColor (NULL, CLedIndicator::Orange);
    m_oLedHVReady.TurnOn   (NULL, True);

    SendCtrlMessage (IDC_MDIG_STATE_STS, WM_SETFONT, (WPARAM) ::GetBoaldGUIFont (), (LPARAM) 0);
    SendCtrlMessage (IDC_MDIG_STATE_STT, WM_SETFONT, (WPARAM) ::GetBoaldGUIFont (), (LPARAM) 0);

    if (s_pIRRPService)
    {
    if (ERROR_SUCCESS == s_pIRRPService ->CreateRRPDevice 
				((void **) & m_pIRRPMDIG    ,
				  IID_IUnknown		    ,
				 (void  *) 0x50000000	    ,
				  IID_IUnknown		    ))
    {
	m_sIRRPCallBack.SetRecvHWND (GetHWND ());

	m_pIRRPMDIG ->Connect (m_hIRRPCallBack, 
			     & m_sIRRPCallBack);
    }
    }

//  On_SetEquMask	( );

    On_SetFieldSize	( );

    CheckRadioButton	(IDC_MDIG_PRESSMODE0_RAD ,
			 IDC_MDIG_PRESSMODE1_RAD ,  IDC_MDIG_PRESSMODE0_RAD);
    On_SetPressMode	(IDC_MDIG_PRESSMODE0_RAD  - IDC_MDIG_PRESSMODE0_RAD);

    On_SetFocusSize	(1);

    CheckRadioButton	(IDC_MDIG_EXPOSEMODE0_RAD,
			 IDC_MDIG_EXPOSEMODE2_RAD,  IDC_MDIG_EXPOSEMODE2_RAD);
    On_SetExposeMode	(IDC_MDIG_EXPOSEMODE2_RAD - IDC_MDIG_EXPOSEMODE0_RAD);

    On_LockThick	();

    RRPSetParam		(m_pIRRPMDIG, IRRPMDIG::ID_ParSetUAnode, 0);
    RRPSetParam		(m_pIRRPMDIG, IRRPMDIG::ID_ParSetIAnode, 0);

	return True ;
};

Bool CMDIGPanel::On_Edit (GWinMsg & m)
{
    union
    {
	float	fValue	 ;
	int	nValue	 ;
    };
    int		nCtrlId	 ;
    TCHAR	sBuf [64];

    GetCtrlText (nCtrlId = LOWORD (m.wParam), sBuf, sizeof (sBuf) / sizeof (TCHAR));

    switch (nCtrlId)
    {
    case IDC_MDIG_UANODE_EDT	    :
#if True
	if (sscanf (sBuf, "%f", & fValue) > 0)
	{
	    nValue  = (int)(fValue * 10 + 0.5);
	}
	else
	{
	    nValue  =  0;
	    sscanf (sBuf, "%d",    & nValue);
	    nValue *= 10;
	}
	RRPSetParam (m_pIRRPMDIG, IRRPMDIG::ID_ParSetUAnode10  , nValue);
#else
	sscanf (sBuf, "%d", & nValue);
	RRPSetParam (m_pIRRPMDIG, IRRPMDIG::ID_ParSetUAnode    , nValue);
#endif
	return True;

    case IDC_MDIG_IANODE_EDT	    :
	sscanf (sBuf, "%d", & nValue);
	RRPSetParam (m_pIRRPMDIG, IRRPMDIG::ID_ParSetIAnode    , nValue);
	return True;

    case IDC_MDIG_PRESS_HIGH_EDT    :
	sscanf (sBuf, "%d", & nValue);
	RRPSetParam (m_pIRRPMDIG, IRRPMDIG::ID_ParSetPressHigh , nValue);
	return True;
    };
	return False;
};

void CMDIGPanel::SetEquMask (_U32 uEquMask)
{
    RRPSetParam (m_pIRRPMDIG, IRRPMDIG::ID_ParSetEquMask  , uEquMask);

    if (uEquMask & IRRPMDIG::EquMask_MM2)
    {
	EnableCtrl  (IDC_MDIG_THICK_CHK, True );
	CheckCtrl   (IDC_MDIG_THICK_CHK, False);
	EnableCtrl  (IDC_MDIG_THICK_SLD, False);
	RRPSetParam (m_pIRRPMDIG, IRRPMDIG::ID_ParSetThick, IRRPMDIG::PressDetector_On);
    }
    else
    {
	EnableCtrl  (IDC_MDIG_THICK_CHK, False);
	CheckCtrl   (IDC_MDIG_THICK_CHK, True );
	EnableCtrl  (IDC_MDIG_THICK_SLD, True );
	RRPSetParam (m_pIRRPMDIG, IRRPMDIG::ID_ParSetThick, IRRPMDIG::PressDetector_Off);
    }
};

void CMDIGPanel::On_LockThick ()
{
    EnableCtrl  (IDC_MDIG_THICK_SLD, IsCtrlChecked (IDC_MDIG_THICK_CHK));

    RRPSetParam (m_pIRRPMDIG, IRRPMDIG::ID_ParSetThick,
		 IsCtrlChecked (IDC_MDIG_THICK_CHK) ? IRRPMDIG::PressDetector_Off
						    : IRRPMDIG::PressDetector_On );
};

void CMDIGPanel::On_SetFieldSize ()
{
    IRRPMDIG::FiledSize FieldSize = IRRPMDIG::FieldSize_Full;

    switch (SendCtrlMessage (IDC_MDIG_FIELD, CB_GETCURSEL, (WPARAM) 0, (LPARAM) 0))
    {
	case 0 :
	    break;
	case 1 :
	    FieldSize = IRRPMDIG::FieldSize_21x30;
	    break;
	case 2 :
	    FieldSize = IRRPMDIG::FieldSize_18x24;
	    break;
	case 3 :
	    FieldSize = IRRPMDIG::FieldSize_12x12;
	    break;
	default :
	    return;
    };

    RRPSetParam (m_pIRRPMDIG, IRRPMDIG::ID_ParSetFieldSize, FieldSize);
};

void CMDIGPanel::On_SetPressMode (int nIndex)
{
    RRPSetParam (m_pIRRPMDIG, IRRPMDIG::ID_ParSetPressMode, IsCtrlChecked (IDC_MDIG_PRESSMODE1_RAD) ? 1 : 0);
};

void CMDIGPanel::On_SetFocusSize (int nIndex)
{
    RRPSetParam (m_pIRRPMDIG, IRRPMDIG::ID_ParSetFocusSize, (0 == nIndex) ? 0x00 : 0x01);
};

void CMDIGPanel::On_SetExposeMode (int nIndex)
{
    int	    nMode = IRRPMDIG::ExposeMode_Manual;

    switch (nIndex)
    {
    case 0  : nMode = IRRPMDIG::ExposeMode_Auto	   ; break;
    case 1  : nMode = IRRPMDIG::ExposeMode_AutoHalf; break;
    case 2  :
    default : nMode = IRRPMDIG::ExposeMode_Manual  ; break;
    };

    RRPSetParam (m_pIRRPMDIG, IRRPMDIG::ID_ParSetExposeMode, nMode);
/*
    if (IsCtrlChecked (IDC_MDIG_EXPOSEMODE1_RAD))
    {	nIndex = 1;}
    else
    if (IsCtrlChecked (IDC_MDIG_EXPOSEMODE2_RAD))
    {	nIndex = 2;}
    else 
//  if (IsCtrlChecked (IDC_MDIG_EXPOSEMODE0_RAD))
    {	nIndex = 0;}

    EnableCtrl (IDC_MDIG_UANODE_EDT, (0 < nMode) ? True : False);
    EnableCtrl (IDC_MDIG_IANODE_EDT, (1 < nMode) ? True : False);

    if (2 > nMode)
    {
    CheckRadioButton (IDC_MDIG_FOCUS0_RAD,
		      IDC_MDIG_FOCUS1_RAD, IDC_MDIG_FOCUS1_RAD);
    }

    EnableCtrl (IDC_MDIG_FOCUS0_RAD, (2 > nMode) ? False : True);

*/    
};

void CMDIGPanel::On_Decompress ()
{
    RRPSetParam (m_pIRRPMDIG, IRRPMDIG::ID_ParDecompress, 0);
};

void CMDIGPanel::On_SetThick (GWinMsg & m)
{
    int	 iValue = -1;

    if ((WM_HSCROLL == m.uMsg)
    ||  (WM_VSCROLL == m.uMsg))
    {
    switch (LOWORD (m.wParam))
    {
    case SB_THUMBTRACK	  :
    case SB_THUMBPOSITION :
	iValue = HIWORD (m.wParam);
	break; 

    case SB_ENDSCROLL	  :
	iValue = SendCtrlMessage (IDC_MDIG_THICK_SLD, TBM_GETPOS, (WPARAM) 0, (LPARAM) 0);
	break;

    default :
	break;
    };
    }
    if (0 <= iValue)
    {
	RRPSetParam (m_pIRRPMDIG, IRRPMDIG::ID_ParSetThick, iValue);
    }
};

#define STAT_OFFSET_X	10
#define STAT_OFFSET_Y	20

Bool CMDIGPanel::On_WM_MouseMove (GWinMsg & m)
{
    GRect   wr	;
    GRect   wr2 ;

    ::GetWindowRect (GetCtrlHWND (IDC_MDIG_STATE_STS), & wr , GetHWND ());
    ::GetWindowRect (GetCtrlHWND (IDC_MDIG_STATE_LED), & wr2, GetHWND ());

    wr2.cx = wr.x + wr.cx - wr2.x;

    if ((WM_MOUSEMOVE == m.uMsg) && (0 != m_StatusPopup.m_uStatus) && wr2.Contains (GET_X_LPARAM (m.lParam), GET_Y_LPARAM (m.lParam)))
    {
	if (m_StatusPopup.HasStyles (WS_VISIBLE, 0))
	{
	    GetClientRect  (& wr);
	    ClientToScreen (& wr);

	    wr.cx = wr2.x - (STAT_OFFSET_X * 2);
	    wr.cy = wr.cy - (STAT_OFFSET_Y * 2);

	    m_StatusPopup.SetWindowPos (NULL, wr.x + STAT_OFFSET_X, 
					      wr.y + STAT_OFFSET_Y, 
					      wr.cx,
					      wr.cy, SWP_SHOWWINDOW);
//<<<
{{
    TRACKMOUSEEVENT    tme = {sizeof (TRACKMOUSEEVENT), TME_CANCEL | TME_LEAVE, GetHWND (), 0};

    TrackMouseEvent (& tme);
    tme.dwFlags &=  (~TME_CANCEL);
    TrackMouseEvent (& tme);
}}
//>>>
	}
    }
    else
    {
	if (m_StatusPopup.HasStyles (0, WS_VISIBLE))
	{
	    m_StatusPopup.ShowWindow (False, False);
	}
    }

	return False;
};


Bool CMDIGPanel::On_DlgMsg (GWinMsg & m)
{
    if (m_hIRRPCallBack && (CWndCallBack::RRPMSG == m.uMsg ))
    {
	On_IRRPCallBack	  ((IRRPCallBack::Handle *) m.lParam);

	return m.DispatchComplete ();
    }

    if ((WM_MOUSEMOVE  == m.uMsg)
    ||  (WM_MOUSELEAVE == m.uMsg))
    {{
	On_WM_MouseMove (m);
    }}

    int		nCtrlId;

    if (m_hEdit.On_ReflectWinMsg (m))
    {
	return	On_Edit (m);
    }

    if (0 !=   (nCtrlId = IsCommand (m)))
    {
	switch (nCtrlId)
	{
	case IDC_MDIG_FIELD	      : return (On_SetFieldSize	 (), True);

	case IDC_MDIG_PRESSMODE0_RAD  :
	case IDC_MDIG_PRESSMODE1_RAD  : return (On_SetPressMode	 (nCtrlId - IDC_MDIG_PRESSMODE0_RAD), True);

	case IDC_MDIG_FOCUS0_RAD      :
	case IDC_MDIG_FOCUS1_RAD      : return (On_SetFocusSize  (nCtrlId - IDC_MDIG_FOCUS0_RAD), True);

	case IDC_MDIG_EXPOSEMODE0_RAD :
	case IDC_MDIG_EXPOSEMODE1_RAD :
	case IDC_MDIG_EXPOSEMODE2_RAD : return (On_SetExposeMode (nCtrlId - IDC_MDIG_EXPOSEMODE0_RAD), True);

	case IDC_MDIG_THICK_CHK	      : return (On_LockThick	 (), True);

	case IDC_MDIG_SHOT_BTN	      : return (On_SetShot (), True);

	case IDC_MDIG_SAVE_BTN	      : return (On_SaveImage (), True);

	case IDC_MDIG_PRESSUP_BTN     : return (On_Decompress (), True);
	};
    }

    if (0 !=   (nCtrlId = IsControlNotify (m)))
    {
	if (WM_DRAWITEM == m.uMsg)
	{
	switch (nCtrlId)
	{
	case IDC_MDIG_STATE_LED :
	    return (m_oLedHVReady.On_Draw (m.pDRAWITEMSTRUCT), True);
	};
	}

	switch (nCtrlId)
	{
	case IDC_MDIG_THICK_SLD	      : On_SetThick (m); break;
	};
/*
	switch (nCtrlId)
	{
	case IDC_MM2_STATE_IND	    : return m_IndState .On_ReflectWinMsg (m);
	default :
	    break;
	}
*/
    }

	return GDialog::On_DlgMsg (m);
};

void CMDIGPanel::On_SetShot ()
{
    if (m_pIRRPMDIG == NULL)
    {	return;}

    CMDIGShot	Dlg;
    int		res;

    GResult dResult;
    int	    iPhase ;

    if (Dlg.Create (this, 
		    NULL, MAKEINTRESOURCE (IDD_MDIG_SHOT)))
    {
	EnableCtrl (IDC_MDIG_SAVE_BTN, False);

	m_pShotDialog = & Dlg;

	m_pIRRPMDIG ->SetHV (IRRPMDIG::HVOn);

	    Dlg.ExecWindow ();

	res = ::GetWindowLong (Dlg.GetHWND (), GWL_USERDATA);

	m_pIRRPMDIG ->SetHV (IRRPMDIG::HVRelease);

	m_pShotDialog	   =  NULL;

	if     (ERROR_SUCCESS ==
	       (dResult	       = m_pIRRPMDIG ->PeekImageStatus (& iPhase, NULL, NULL)))
	{
	    if (ERROR_SUCCESS ==
	       (dResult	       = m_pIRRPMDIG ->PeekImageStatus (& iPhase, NULL, NULL)))
	    {
	    if (0	       < iPhase)
	    {{
		CMDIGImage   Dlg ;

	    if (Dlg.Create  (this,
			     NULL, MAKEINTRESOURCE (IDD_MDIG_IMAGE)))
	    {
		Dlg.m_pIRRPMDIG = m_pIRRPMDIG;

		Dlg.ExecWindow ();

		Dlg.m_pIRRPMDIG = NULL;

		dResult = (Dlg.GetHWND (), GWL_USERDATA);
	    }
	    }}
	    }
	}

	if (IDOK == res)
	{{
	}}
    }
};

void CMDIGPanel::On_SaveImage ()
{
    SendImage (GetHWND (), m_pIRRPMDIG);
};
//
//[]------------------------------------------------------------------------[]
//
void CMDIGPanel::On_IRRPParam (_U32 uParamId, _U32 uValue)
{
//	int	      i;
	TCHAR	      pBuf [64];

    if (uParamId == IRRPMDIG::ID_RegGetEquStatus)
    {
	s_pMainPanel ->Send_WM (CWndCallBack::EQUMSG, (WPARAM) IRRPMDIG::EquMask_Urp, ((uValue & IRRPMDIG::EquMask_Urp) ? 0x02 : 0x00) | (uValue >> 31));
	s_pMainPanel ->Send_WM (CWndCallBack::EQUMSG, (WPARAM) IRRPMDIG::EquMask_KKM, ((uValue & IRRPMDIG::EquMask_KKM) ? 0x02 : 0x00) | (uValue >> 31));

	s_pMainPanel ->Send_WM (CWndCallBack::EQUMSG, (WPARAM) IRRPMDIG::EquMask_MDiafr, ((uValue & IRRPMDIG::EquMask_MDiafr) ? 0x02 : 0x00) | (uValue >> 31));
	s_pMainPanel ->Send_WM (CWndCallBack::EQUMSG, (WPARAM) IRRPMDIG::EquMask_MScan , ((uValue & IRRPMDIG::EquMask_MScan ) ? 0x02 : 0x00) | (uValue >> 31));
	s_pMainPanel ->Send_WM (CWndCallBack::EQUMSG, (WPARAM) IRRPMDIG::EquMask_MFG   , ((uValue & IRRPMDIG::EquMask_MFG   ) ? 0x03 : 0x01));
	s_pMainPanel ->Send_WM (CWndCallBack::EQUMSG, (WPARAM) IRRPMDIG::EquMask_MM2   , ((uValue & IRRPMDIG::EquMask_MM2   ) ? 0x02 : 0x00) | (uValue >> 31));
    }

    if (uParamId == IRRPMDIG::ID_RegGetStatus)
    {
	m_StatusPopup.m_uStatus	= uValue;

	m_oLedHVReady.SetColor (GetCtrlHWND (IDC_MDIG_STATE_LED),
			       (0 == uValue) ? CLedIndicator::Green : CLedIndicator::Red);

	if (0x00000000 == uValue)
	{   wsprintf (pBuf, LoadString (IDS_SHOT_READY));}
	else
	{   wsprintf (pBuf, "%08X", uValue);}
//	{   wsprintf (pBuf, "�� �����");}

	SetCtrlText (IDC_MDIG_STATE_STS	    , pBuf);

	if (m_pShotDialog)
	{
	    if (uValue & 0x0002)
	    {
		m_bHVOn = True  ;
		m_pShotDialog ->SendCtrlMessage (IDC_MDIG_SHOT_ICN, STM_SETICON, (WPARAM) ::LoadIcon (NULL, IDI_INFORMATION), (LPARAM) NULL);
		m_pShotDialog ->ShowCtrl	(IDC_MDIG_SHOT_ICN, True );
	    }
	    else
	    if (m_bHVOn)
	    {
		m_bHVOn = False ;
		m_pShotDialog ->ShowCtrl	(IDC_MDIG_SHOT_ICN, False);
	    }
	}
	if ((0 == m_StatusPopup.m_uStatus) && m_StatusPopup.HasStyles (0, WS_VISIBLE))
	{
	    m_StatusPopup.ShowWindow (False, False);
	}

	if (m_StatusPopup.HasStyles (0, WS_VISIBLE))
	{
	    m_StatusPopup.InvalidateRect (NULL, True);
	}
    }

    if (uParamId == IRRPMDIG::ID_ParSetFocusSize)
    {
	CheckRadioButton (IDC_MDIG_FOCUS0_RAD,
			  IDC_MDIG_FOCUS1_RAD, IDC_MDIG_FOCUS0_RAD + ((0 == uValue) ? 0 : 1));
    }
    if (uParamId == IRRPMDIG::ID_ParSetExposeMode)
    {
	SendCtrlMessage (IDC_MDIG_UANODE_EDT, EM_SETREADONLY, 
			(WPARAM)((IRRPMDIG::ExposeMode_Auto	< uValue) ? False : True), (LPARAM) 0);

	SendCtrlMessage (IDC_MDIG_IANODE_EDT, EM_SETREADONLY, 
			(WPARAM)((IRRPMDIG::ExposeMode_AutoHalf < uValue) ? False : True), (LPARAM) 0);
    }
    if (uParamId == IRRPMDIG::ID_ParSetUAnode10)
    {
	sprintf  (pBuf, "%d.%d", (short) uValue / 10, (short) uValue % 10);
	SetCtrlText (IDC_MDIG_UANODE_EDT    , pBuf);
    }
    if (uParamId == IRRPMDIG::ID_ParSetIAnode)
    {
	wsprintf (pBuf, "%d", (short) uValue);
	SetCtrlText (IDC_MDIG_IANODE_EDT    , pBuf);
    }
    if (uParamId == IRRPMDIG::ID_ParGetRotPos)
    {
	wsprintf (pBuf, "%d", (short) uValue);
	SetCtrlText (IDC_MDIG_DISP_ROT	    , pBuf);
    }
    if (uParamId == IRRPMDIG::ID_ParGetThick )
    {
    if (IsCtrlChecked (IDC_MDIG_THICK_CHK))
    {}
//  else
    {
	SendCtrlMessage (IDC_MDIG_THICK_SLD, TBM_SETPOS, (WPARAM) 1, (LPARAM) uValue);
	wsprintf (pBuf, "%d", (short) uValue);
	SetCtrlText (IDC_MDIG_DISP_THICK    , pBuf);
    }
    }
    if (uParamId == IRRPMDIG::ID_ParGetPress )
    {
	wsprintf (pBuf, "%d.%d", (short) uValue / 10, (short) uValue % 10);
	SetCtrlText (IDC_MDIG_DISP_PRESS    , pBuf);
    }
    if (uParamId == IRRPMDIG::ID_ParSetPressHigh)
    {
	wsprintf (pBuf, "%d", (short) uValue);
	SetCtrlText (IDC_MDIG_PRESS_HIGH_EDT, pBuf);
    }
    if (uParamId == IRRPMDIG::ID_ParGetUAnode10)
    {
	wsprintf (pBuf, "%d.%d", (short) uValue / 10, (short) uValue % 10);
	SetCtrlText (IDC_MDIG_DISP_UANODE   , pBuf);
    }
    if (uParamId == IRRPMDIG::ID_ParGetIAnode)
    {
	wsprintf (pBuf, "%d", (short) uValue);
	SetCtrlText (IDC_MDIG_DISP_IANODE   , pBuf);
    }
    if (uParamId == IRRPMDIG::ID_ParGetExposeTime)
    {
	wsprintf (pBuf, "%d", (short) uValue);
	SetCtrlText (IDC_MDIG_DISP_EXPTIME  , pBuf);
    }
    if (uParamId == IRRPMDIG::ID_ParGetHeatTimeout)
    {
	wsprintf (pBuf, "%d:%02d", (unsigned short) uValue / 60,
				   (unsigned short) uValue % 60);
	SetCtrlText (IDC_MDIG_STATE_STT	    , pBuf);
	ShowCtrl    (IDC_MDIG_STATE_STT, (0 < uValue) ? True : False);
    }

    if (uParamId == IRRPMDIG::ID_RegGetStatusHV)
    {
	switch (HIWORD (uValue))
	{
	case 0x0001 :
	    m_nHVPhase = HIWORD (uValue);
	    wsprintf (pBuf, LoadString (IDS_SHOT_WAITREADY),	LOWORD (uValue));
	    break;

	case 0x0002 :
	    m_nHVPhase = HIWORD (uValue);
	    wsprintf (pBuf, LoadString (IDS_SHOT_STARTSGNS),	LOWORD (uValue));
	    break;

	case 0x0003 :
	    m_nHVPhase = HIWORD (uValue);
	    wsprintf (pBuf, LoadString (IDS_SHOT_DONESGNS),	LOWORD (uValue));
	    break;

	case 0x0004 :
	    m_nHVPhase = HIWORD (uValue);
	    wsprintf (pBuf, LoadString (IDS_SHOT_IMAGE),	LOWORD (uValue));
	if (m_pShotDialog)
	{
// Ugrich Pasha bug fixing...
//
//	    m_pShotDialog ->EnableCtrl	    (IDCANCEL, False);
// Ugrich Pasha bug fixing...
	}
	    break;

	case 0x0005 :
	    m_nHVPhase = HIWORD (uValue);
	if (0 == LOWORD (uValue))
	{	 
//	    EnableCtrl (IDC_MDIG_SAVE_BTN, True);
	}
	    break;

	case 0x0000 :
	    if (0 == LOWORD (uValue))
	    {
	if (m_pShotDialog)
	{
		EnableCtrl (IDC_MDIG_SAVE_BTN, True);
		wsprintf   (pBuf, LoadString (IDS_SHOT_SUCCESS));
	}
	    }
	    else
	    {
	if (m_pShotDialog)
	{
	    m_pShotDialog ->SendCtrlMessage (IDC_MDIG_SHOT_ICN, STM_SETICON, (WPARAM) ::LoadIcon (NULL, IDI_ERROR), (LPARAM) NULL);
	    m_pShotDialog ->ShowCtrl	    (IDC_MDIG_SHOT_ICN, True);
	}
		wsprintf (pBuf, LoadString (IDS_SHOT_FAIL), m_nHVPhase, LOWORD (uValue),
							    m_nHVPhase, LOWORD (uValue));
	    }
	    break;
	};

	if (m_pShotDialog)
	{
	    m_pShotDialog ->SetCtrlText (IDC_MDIG_SHOT_DISPLAY, pBuf);

	if (uValue == ERROR_SUCCESS)
	{
	    m_pShotDialog ->Send_WM (WM_COMMAND, IDOK, NULL);
	}
	}
    }
/*
    if (uParamId == IRRPMDIG::ID_ParGetShotCount)
    {
printf ("................%d Control2\n", uValue);
    }
    if (uParamId == IRRPMDIG::ID_ParGetShotCountBad)
    {
printf ("................%d Control2\n", uValue);
    }
*/
};

void CMDIGPanel::On_IRRPCallBack (IRRPCallBack::Handle * hIRRPCallBack)
{
    Word	    uLength	;
    Word	    uType	;
    IRRPProtocol::DataPack *
		    pDataPack	;
    IRRPProtocol::ParamPack::Entry * 
		    pEntry	;

    if (hIRRPCallBack == m_hIRRPCallBack)
    {
	pDataPack = (IRRPProtocol::DataPack *) m_sIRRPCallBack.PeekHeadPacket ();

    while (pDataPack)
    {
	uLength = getBigEndianW (pDataPack, offsetof (IRRPProtocol::DataPack, uLength));
	uType	= getBigEndianW (pDataPack, offsetof (IRRPProtocol::DataPack, uType  ));

	if (0  == uType)
	{
		pEntry = (IRRPProtocol::ParamPack::Entry *)(pDataPack + 1);

	    while ((Byte *) pEntry < ((Byte *) pDataPack + uLength))
	    {
		On_IRRPParam (getBigEndianD (pEntry, offsetof (IRRPProtocol::ParamPack::Entry, uParamId)),
			      getBigEndianD (pEntry, offsetof (IRRPProtocol::ParamPack::Entry, uValue  )));
		pEntry ++;
	    }
	}

	pDataPack = (IRRPProtocol::DataPack *) m_sIRRPCallBack.PeekNextPacket ();
    }
    }
};
//
//[]------------------------------------------------------------------------[]












#if FALSE

//[]--------------------------------------------------------------------------
//
static HANDLE _createFile (HWND hParent, const TCHAR * pFilter, TCHAR * szTitle = NULL)
{
    HANDLE		hFile;
    TCHAR		szFileName [256];
    OPENFILENAME	ofn ;

    memset (szFileName, 0, sizeof (szFileName));
    memset (& ofn     , 0, sizeof (ofn	     ));

    hFile		= NULL;

    ofn.lStructSize     = sizeof(OPENFILENAME);
    ofn.hwndOwner	= hParent;

    ofn.lpstrFile	= szFileName;
    ofn.nMaxFile	= sizeof(szFileName);

    ofn.lpstrFileTitle	= szTitle;
    ofn.nMaxFileTitle	= MAX_PATH ;

    ofn.lpstrFilter     = pFilter;
    ofn.nFilterIndex    = 1;
    ofn.Flags		= OFN_PATHMUSTEXIST | OFN_CREATEPROMPT | OFN_HIDEREADONLY;
    ofn.lpstrDefExt     = pFilter;

    if (GetSaveFileName (& ofn))
    {
	if (INVALID_HANDLE_VALUE != 
	   (hFile = CreateFile	    (szFileName		    ,
				     GENERIC_WRITE	    ,
				     0 /*FILE_SHARE_READ*/  ,
				     NULL		    ,
				     CREATE_ALWAYS	    ,
				     FILE_ATTRIBUTE_NORMAL  ,
				     NULL		    )))
	{
	    return hFile;
	}
    }
	    return NULL ;
};
//
//[]--------------------------------------------------------------------------

#endif
