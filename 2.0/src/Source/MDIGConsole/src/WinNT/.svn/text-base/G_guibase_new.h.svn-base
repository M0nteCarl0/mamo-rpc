//[]------------------------------------------------------------------------[]
// Main GWin::GUI declarations
//
//
// Created by GSh.
//[]------------------------------------------------------------------------[]

#ifndef __G_GUIBASE_H
#define __G_GUIBASE_H

#include "g_winuser.h"


#include <stdio.h>
#include <shlwapi.h>
#include <commctrl.h>
#include <uxtheme.h>


//[]------------------------------------------------------------------------[]
//
#ifdef _MSC_BUG_C4003
#ifdef GetFirstChild
#undef GetFirstChild
#endif
#endif

#ifndef	WM_FORWARDMSG
#define	WM_FORWARDMSG		    0x037F

#define	WM_PREPROCMSG		    WM_FORWARDMSG
//
//  WPARAM - Defined by user
//  LPARAM - (MSG *)
//
//  Return : == 0 - if not processed
//	     != 0 - if	   processed
#endif

#ifndef WM_BACKWARDMSG
#define	WM_BACKWARDMSG		    0x037E
//
//  WPARAM - Source Id
//  LPARAM - GWinMsg *
//
//  Return : not used;
//
#endif

//[]------------------------------------------------------------------------[]
// Some Shell wrappers
//
//
    Bool	    GAPI    InitCtrl32	(DWord dICC, DWord dCtrl32VerReq = MAKELONG (71, 4));
//
//  ICC_LISTVIEW_CLASSES    Load list-view and header control classes. 
//  ICC_TREEVIEW_CLASSES    Load tree-view and ToolTip control classes. 
//  ICC_BAR_CLASSES	    Load toolbar, status bar, trackbar, and 
//  ICC_TAB_CLASSES	    Load tab and ToolTip control classes. 
//  ICC_UPDOWN_CLASS	    Load up-down control class. 
//  ICC_PROGRESS_CLASS	    Load progress bar control class. 
//  ICC_HOTKEY_CLASS	    Load hot key control class. 
//  ICC_ANIMATE_CLASS	    Load animate control class. 
//  			    ToolTip control classes. 
//  ICC_WIN95_CLASSES	    Load animate control, header, hot key, list-view, 
//  			    progress bar, status bar, tab, ToolTip, toolbar, 
//  			    trackbar, tree-view, and up-down control classes. 
//  ICC_DATE_CLASSES	    Load date and time picker control class. 
//  ICC_USEREX_CLASSES	    Load ComboBoxEx class. 
//  ICC_COOL_CLASSES	    Load rebar control class. 
//
//			    0x0400 <= IE :
//  ICC_INTERNET_CLASSES    Load IP address class. 
//  ICC_PAGESCROLLER_CLASS  Load pager control class. 
//  ICC_NATIVEFNTCTL_CLASS  Load a native font control class. 
//
//			    0x0560 <= IE :
//  ICC_STANDARD_CLASSES    Load one of the intrinsic User32 control 
//  			    classes. The user controls include button, 
//  			    edit, static, listbox, combobox, and scrollbar. 
//  ICC_LINK_CLASS	    Load a hyperlink control class. 
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
// Some User32 API extension
//
//  Return window rectangle relative from hFrom's Client area (NULL for screen).
//
Bool WINAPI	    GetWindowRect   (HWND hWnd, RECT *  pr, HWND hFrom);

Bool WINAPI	    GetWindowRect   (HWND hWnd, GRect * pr, HWND hFrom = NULL);
//
//...................

//  Return client rectangle in the GRect terms
//
Bool GFORCEINLINE   GetClientRect   (HWND hWnd, GRect * pr)
		    {
			return ::GetClientRect (hWnd, & pr ->Rect ());
		    };

Bool WINAPI	    GetClientRectEx (HWND hWnd, RECT *	pr);

Bool GFORCEINLINE   GetClientRectEx (HWND hWnd, GRect * pr)
		    {
			return ::GetClientRectEx (hWnd, & pr ->Rect ());
		    };
//...................

//  Converts the client coordinates of a specified rect to screen coordinates. 
//
Bool WINAPI	    ClientToScreen  (HWND hWnd, RECT *	pr);

Bool GFORCEINLINE   ClientToScreen  (HWND hWnd, GRect * pr)
		    {	return ::ClientToScreen (hWnd, (PPOINT) & pr ->Pos ());};
//...................

//  Converts the screen coordinates of a specified rect on the screen to client 
//  coordinates.
//
Bool WINAPI	    ScreenToClient  (HWND hWnd, RECT *  pr);

Bool GFORCEINLINE   ScreenToClient  (HWND hWnd, GRect * pr)
		    {	return ::ScreenToClient (hWnd, & pr ->Pos ());};
//...................

//  Converts the window coordinates of a specified rect to screen coordinates. 
//
Bool WINAPI	    WindowToScreen  (HWND hWnd, POINT *	pp);

Bool WINAPI	    WindowToScreen  (HWND hWnd, RECT  *	pr);

Bool GFORCEINLINE   WindowToScreen  (HWND hWnd, GRect * pr)
		    {	return ::WindowToScreen (hWnd, & pr ->Pos ());};
//...................

DWord GFORCEINLINE  GetWindowStyles (HWND hWnd)
		    {	return ::GetWindowLong (hWnd, GWL_STYLE);};

DWord GFORCEINLINE  SetWindowStyles (HWND hWnd, DWord dClrMask, DWord dSetMask)
		    {	return ::SetWindowLong (hWnd, GWL_STYLE,
			       ::GetWindowLong (hWnd, GWL_STYLE) & (~dClrMask) | dSetMask);};

const void * WINAPI LoadResource    (HMODULE hResModule	   ,	// NULL for current
				     LPCTSTR pResName	   ,
				     LPCTSTR pResType	   ,
				     DWORD * pResSize = NULL);

HWND WINAPI	    CreateDialogEx  (HINSTANCE hInstance   ,	// NULL for current
				     LPCTSTR   pResName	   ,
				     HWND      hWndParent  ,
				     DLGPROC   pDialogProc );

LPCTSTR WINAPI	    LoadString	    (UINT      uResId	   );

//
//[]------------------------------------------------------------------------[]
// Some GDI32 API extension
//
// The FrameRectEx function draws a border around the specified rectangle by 
// using the specified brush and width. dRop can be 
//
// PATCOPY	Copies the specified pattern into the destination bitmap.
// PATINVERT	Combines the colors of the specified pattern with the colors 
//		of the destination rectangle by using the Boolean OR operator.
// DSTINVERT	Inverts the destination rectangle.
// BLACKNESS	Fills the destination rectangle using the color associated with 
//		index 0 in the physical palette. (This color is black for the 
//		default physical palette.)
// WHITENESS	Fills the destination rectangle using the color associated with 
//		index 1 in the physical palette. (This color is white for the 
//		default physical palette.)
//
int	WINAPI	FrameRectEx	(HDC, const RECT  *, HBRUSH, int iWidth, DWORD dRop);

int	WINAPI	FrameRectEx	(HDC, const GRect *, HBRUSH, int iWidth, DWORD dRop);

int	WINAPI	PatBlt		(HDC, const GRect *, HBRUSH, DWORD dRop);

int	WINAPI	FillRect	(HDC, const GRect *, HBRUSH);

Bool	WINAPI	DrawEdge	(HDC, const GRect *, UINT edge, UINT eFlags);

HBRUSH	WINAPI	CreateHalftoneBrush ();
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
// GWin message processing model declarations
//
//
    struct GWinMsg;

    typedef Bool    (GAPI * GWinMsgProc )(void *, GWinMsg &);

    typedef LRESULT (GAPI * GWinHookProc)(void *, int, WPARAM, LPARAM);

//  typedef void    (GAPI * GWinBackProc)(void *, int, WPARAM, LPARAM);

// Next flags declared for GWinMsg ored	 ::fResult
//
//
#define DISPATCH_LRESULT	    ((DWORD)0x00000001)
#define	DISPATCH_BRESULT	    ((DWORD)0x00000002)

#pragma pack (_M_PACK_VPTR)

    struct GWinMsg
    {
	LRESULT		lResult	;	// 4 / 8
	LRESULT		fResult ;	// 4 / 8

	UINT		uMsg	;	// 4 / 4

    union
    {
	WPARAM		wParam	;	// 4 / 8

#if (_M_BIGENDIAN)
    struct {union { WORD    LowParam ;};
	    union { WORD    HiwParam ;};};
#else
    struct {union { WORD    LowParam ;};
	    union { WORD    HiwParam ;};};
#endif

	HDC		    wHDC		;
    };

    union
    {
	LPARAM		lParam ;	// 4 / 8

#if (_M_BIGENDIAN)
    struct {union { WORD    LolParam ;};
	    union { WORD    HilParam ;};};
#else
    struct {union { WORD    LolParam ;};
	    union { WORD    HilParam ;};};
#endif
	HWND		    lWnd		;
	HDC		    lHDC		;
	DRAWITEMSTRUCT	  * pDRAWITEMSTRUCT	;
	MEASUREITEMSTRUCT * pMEASUREITEMSTRUCT	;
	COMPAREITEMSTRUCT * pCOMPAREITEMSTRUCT	;
	DELETEITEMSTRUCT  * pDELETEITEMSTRUCT	;

	NMHDR		  * pNMHDR		;
	NMCUSTOMDRAW	  * pNMCUSTOMDRAW	;
	NMTREEVIEW	  * pNMTREEVIEW		;
	NMTVDISPINFO	  * pNMTVDISPINFO	;
    };
	HWND		hWnd	;	// 4 / 8

	GWinMsgProc	pDefProc;	// 4 / 8
	void *		oDefProc;	// 4 / 8

// Public methods only this methods can be used in the Dispatchers ---------
//
        Bool    DispatchDefault		()
		{
		    return (pDefProc) ? (pDefProc)(oDefProc, * this) : False;
		};

        Bool    DispatchComplete	()
		{
                    return True;
		};

        Bool    DispatchComplete	(LRESULT r)
		{
		    fResult |= DISPATCH_LRESULT;
		    lResult  = r; return True  ;
		};

        Bool    DispatchCompleteDlgEx	(LRESULT r)
		{
		    fResult |= DISPATCH_BRESULT;
		    lResult  = r; return True  ;
		};
//
//.......
//
        Bool    DispatchContinue	() const
		{
		    return False;
		};

        Bool    DispatchContinue	(LRESULT r)
		{
		    fResult |= DISPATCH_LRESULT;
		    lResult  = r; return False ;
		};

	int	IsCommand		()
		{
		    if ((WM_COMMAND == uMsg) && (1 >= (int) HiwParam))
		    {
			return (int) LowParam;
		    } 
			return 0;
		};

	int	IsCtrlNotify		(HWND * = NULL);
//
// Public methods ----------------------------------------------------------
    };



    class GWinMsgHandlerBase
    {
    public :
    virtual    ~GWinMsgHandlerBase  () {};
    };

    class GWinMsgHandler : public GWinMsgHandlerBase
    {
	typedef	TSLink <GWinMsgHandler, sizeof (GWinMsgHandlerBase)>	SLink;
	typedef	TXList <GWinMsgHandler,
		TSLink <GWinMsgHandler, sizeof (GWinMsgHandlerBase)> >	SList;

	SLink		    m_SLink	;
	SList 		    m_SList	;
	GWinMsgHandler *    m_pParent	;

	GWinMsgProc	    m_pProc	;
	void *		    m_oProc	;

		GWinMsgHandler  ()
		{
		    m_pParent = NULL;
		    m_pProc	  = NULL;
		    m_oProc	  = NULL;
		};

		GWinMsgHandler  (GWinMsgHandler * pParent, GWinMsgProc pProc = NULL,
							   void	     * oProc = NULL)
		{
		    Init (pParent, pProc, oProc);
		};

		GWinMsgHandler * Init 
				(GWinMsgHandler * pParent, GWinMsgProc pProc,
							   void *      oProc)
    		{
		    m_pParent = pParent ;
		    m_pProc   = pProc   ;
		    m_oProc   = oProc   ;

		    if (m_pParent)
		    {
			m_pParent ->Append (this);
		    }
			return this;
		};

	    GWinMsgHandler *	Append	    (GWinMsgHandler *);

	    GWinMsgHandler *	Detach	    (GWinMsgHandler *);

	    Bool		Dispatch    (GWinMsg & m)
				{
				// GASSERT (NULL != m_pProc)
				//
				    return (m_pProc)(m_oProc, m);
				};
    };

    template <typename T>
    class TWinMsgHandler : public GWinMsgHandler
    {
	FOO_PMF		    m_tProc	;

	    void		Init	
			       (Bool (T::* tProc)(GWinMsg &), T * = NULL)
		    {
			m_aProc	   = aProc;
			m_TProc () = tProc;

			GWinMsgHandler::Init ((Bool (GAPI *)(void *, GWinMsg &)) Dispatch, this);
		    };

	    Bool    (T::*&	m_TProc	    () const)(GWinMsg &)
		    {
			return (Bool  (T::*&)(GWinMsg &)) m_tProc;
		    };

    static Bool GAPI		Dispatch    (TWinMsgHandler <GWinMsg &> * h, GWinMsg & m)
		    {
			return (((T *) h ->m_aProc   ) ->*
				       h ->m_TProc ()) (m);
		    };
    };

    class GCtrlHandler : public TWinMsgHandler <GCtrlHandler>
    {
    public :

	HWND		GetHWND		() const { return m_hWnd  ;};

	int		GetId		() const { return (int)(LOWORD(m_pWndId));};

	const void *	GetIdPtr	() const { return m_pWndId;};

    protected :

	HWND		    m_hWnd	;
	const void *	    m_pWndId	;
    };

#pragma pack ()
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
#pragma pack (_M_PACK_VPTR)

    struct GWinProcThunk : public __stdcallThunk
    {
	void *	pDspProc ;
	void *	oDspProc ;
	void *	oDefProc ;

	void *	operator new    (size_t s);
	void	operator delete (void * p);

	GWinProcThunk (ThunkProc thunkProc, void * pDsp, void * oDsp, void * oDef)
	{
	    __stdcallThunk::SetDispPtr (thunkProc);

	    pDspProc = pDsp;
	    oDspProc = oDsp;
	    oDefProc = oDef;
	};

	Bool	    IsThunkPtr	(ThunkProc pProc) const 
	{
	    return __stdcallThunk::IsThunkPtr (sizeof (this), pProc);
	};
    };

#pragma pack ()
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
    struct GWndHookThunk : public GWinProcThunk
    {
	GWndHookThunk   (GWinHookProc pDsp, void * oDsp) : 

	GWinProcThunk	((ThunkProc) Dispatch, pDsp, oDsp, NULL) {};

	Bool		IsThunkPtr	() const

			{ return GWinProcThunk::IsThunkPtr ((ThunkProc) Dispatch);};

    static LRESULT CALLBACK Dispatch	(const GWndHookThunk *, int, WPARAM, LPARAM);
    };

    struct GWndProcThunk : public GWinProcThunk
    {
	GWndProcThunk	(GWinMsgProc pDsp, void * oDsp, WNDPROC pDef) : 

	GWinProcThunk	((ThunkProc) Dispatch, pDsp, oDsp, pDef) {};

	Bool		IsThunkPtr	() const

			{ return GWinProcThunk::IsThunkPtr ((ThunkProc) Dispatch);};

    static LRESULT CALLBACK Dispatch	(const GWndProcThunk *, HWND, UINT, WPARAM, LPARAM);

    static Bool	   GAPI	    DispatchDef (void *, GWinMsg &);
    };

    struct GDlgProcThunk : public GWinProcThunk
    {
	GDlgProcThunk	(GWinMsgProc pDsp, void * oDsp, DLGPROC pDef) : 

	GWinProcThunk	((ThunkProc) Dispatch, pDsp, oDsp, pDef) {};

	Bool		IsThunkPtr	() const

			{ return GWinProcThunk::IsThunkPtr ((ThunkProc) Dispatch);};

    static INT_PTR CALLBACK Dispatch	(const GDlgProcThunk *, HWND, UINT, WPARAM, LPARAM);

    static Bool	   GAPI	    DispatchDef (void *, GWinMsg &);
    };
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
	Bool GAPI	    GetWindowClassInfo	(LPCTSTR, WNDCLASSEX *);

#define SYS_WINDOW_CLASS    ((HINSTANCE) sizeof (void *))
#define EXT_WINDOW_CLASS    ((HINSTANCE)	(NULL  ))

    struct GWindowClass
    {
	HINSTANCE	Registered		   ;	// Initiated SYS_WINDOW_CLASS or
							//	     EXT_WINDOW_CLASS
	LPCTSTR (GAPI * GetInfo) (WNDCLASSEX *	  );

	Bool		Register (HINSTANCE = NULL);
	void	      UnRegister ();
    };

#define DECLARE_SYS_WINDOW_CLASS(theName)				\
    protected :								\
    virtual GWindowClass &  GetWindowClass () const			\
	    {								\
		    return  s_WndClass;					\
	    };								\
    private :								\
    static  LPCTSTR GAPI    GetWindowClassInfo (WNDCLASSEX * wc = NULL)	\
	    {								\
		    LPCTSTR pName      = theName      ;			\
									\
		if (wc)							\
		{							\
		    ::GetWindowClassInfo (pName, wc);			\
		}							\
		    return  pName;					\
	    };								\
    static  GWindowClass    s_WndClass

#define DEFINE_SYS_WINDOW_CLASS(theName)				\
									\
    GWindowClass   theName::s_WndClass =				\
									\
    { SYS_WINDOW_CLASS, theName::GetWindowClassInfo }

#define DECLARE_EXT_WINDOW_CLASS(theName,theStyle)			\
    public:								\
    static  Bool RegisterWindowClass ()					\
	    {								\
	        return (NULL == s_WndClass.Registered)			\
			      ? s_WndClass.Register () : False;		\
	    };								\
    protected :								\
    virtual GWindowClass &  GetWindowClass () const			\
	    {								\
		    return  s_WndClass;					\
	    };								\
    private :								\
    static  LPCTSTR GAPI    GetWindowClassInfo (WNDCLASSEX * wc = NULL)	\
	    {								\
		    LPCTSTR pName      = theName      ;			\
									\
		if (wc)							\
		{							\
		    ::GetWindowClassInfo (NULL, wc);			\
									\
		    wc ->style	       = theStyle     ;			\
									\
		    wc ->lpfnWndProc   = _initWndProc ;			\
		    wc ->lpszClassName = pName	      ;			\
		}							\
		    return  pName;					\
	    };								\
    static  GWindowClass    s_WndClass

#define DEFINE_EXT_WINDOW_CLASS(theName)				\
									\
    GWindowClass   theName::s_WndClass =				\
									\
    { EXT_WINDOW_CLASS, theName::GetWindowClassInfo }
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
//[]------------------------------------------------------------------------[]
//
// Next flags declared for GWindow ored	 ::m_dFlags

#define GWF_CREATED	    ((DWord) 0x80000000)

class GWindow
{
	friend class GDialog;

protected :
		GWindow	    (const void * pWndId)
		{
		    m_hWnd	    = NULL   ;
		    m_pWndId	    = pWndId ;
		    m_dFlags	    = 0	     ;

		    m_pParent	    = NULL   ;

		    m_pWndProc	    = NULL   ;
		};

virtual	       ~GWindow	    ();

public :

// Base methods --------------------------------------------------------------
//
	HWND		GetHWND		() const { return m_hWnd  ;};

	int		GetId		() const { return (int)(LOWORD(m_pWndId));};

	const void *	GetIdPtr	() const { return m_pWndId;};

	int		GetText		(LPTSTR pText, int uTextLen) const
			    { return ::GetWindowText (m_hWnd, pText, uTextLen);};

	int		SetText		(LPTSTR pText) const
			    { return ::SetWindowText (m_hWnd, pText);};

	GWindow *	GetParent	() const { return m_pParent;};

	GWndProcThunk *	GetWndProc	() const { return m_pWndProc;};

	DWord		GetFlags	() const { return m_dFlags;}

	DWord		SetFlags	(DWord dClrMask, DWord dSetMask)
			    { DWord  f = m_dFlags; m_dFlags = (m_dFlags & (~dClrMask)) | dSetMask; return f;};

	Bool		HasFlag		(DWord dFlags) const
			    { return dFlags & GetFlags ();};

	Bool		HasFlags	(DWord dFlags) const
			    { return ((dFlags & GetFlags ()) == dFlags) ? True : False;};
//
// Base methods --------------------------------------------------------------

// User32 API wrapper --------------------------------------------------------
//
	DWord		GetStyles	() const
			    { return ::GetWindowLong (GetHWND (), GWL_STYLE);};

	DWord		SetStyles	(DWord dClrMask, DWord dSetMask) const
			    { return ::SetWindowLong (GetHWND (), GWL_STYLE,
				     ::GetWindowLong (GetHWND (), GWL_STYLE) & (~dClrMask) | dSetMask);};

	Bool		HasStyles	(DWord dClrMask, DWord dSetMask) const
			    { return ((GetStyles () & (dSetMask | dClrMask)) == dSetMask) ? True : False;};

	DWord		GetExStyles	() const
			    { return ::GetWindowLong (GetHWND (), GWL_EXSTYLE);};

	DWord		SetExStyles	(DWord dClrMask, DWord dSetMask) const
			    { return ::SetWindowLong (GetHWND (), GWL_EXSTYLE,
				     ::GetWindowLong (GetHWND (), GWL_EXSTYLE) & (~dClrMask) | dSetMask);};

	Bool		HasExStyles	(DWord dClrMask, DWord dSetMask) const
			    { return ((GetExStyles () & (dSetMask | dClrMask)) == dSetMask) ? True : False;};

	Bool		IsVisible	() const			// Return true if this window & all parents
			    { return ::IsWindowVisible (GetHWND ());};	// has style WS_VISIBLE; != HasStyle (WS_VISIBLE);

	Bool		IsEnabled	() const
			    { return ::IsWindowEnabled (GetHWND ());};

	Bool		IsActive	() const
			    { return (GetHWND () && (GetHWND () == ::GetActiveWindow ())) ? True : False;};

	Bool		EnableWindow	(Bool bEnable) const
			    { return ::EnableWindow (GetHWND (), bEnable);};

	Bool		ShowWindow	(Bool bShow, Bool bActivate = True) const
			    {
			      return ::ShowWindow (GetHWND (), bShow ? ((bActivate) ? SW_SHOW : SW_SHOWNA)
										    : SW_HIDE		 );
			    };

	Bool		Show		(Bool bActivate = False) const
			    {
			      return ShowWindow (True , bActivate);
			    };

	Bool		Hide		() const
			    {
			      return ShowWindow (False, False);
			    };
//
// Dialog control's wrapper --------------------------------------------------
//
	HWND		GetCtrlHWND	(int nCtrlId) const
			    { return ::GetDlgItem (GetHWND (), nCtrlId);};

	int		GetCtrlId	(HWND hCtrl) const
			    { return ::GetDlgCtrlID (hCtrl);};

	int		GetCtrlText	(int nCtrlId, LPTSTR pText, int uTextLen)
			    { return (int) ::GetDlgItemText (GetHWND (), nCtrlId, pText, uTextLen);};

	Bool		SetCtrlText	(int nCtrlId, LPCTSTR pText)
			    { return ::SetDlgItemText (GetHWND (), nCtrlId, pText);};

	Bool		PrintfCtrlText	(int nCtrlId, LPCTSTR fmt, ...);

	int		GetCtrlText	(int nCtrlId);

	Bool		SetCtrlText	(int nCtrlId, int nValue);

	DWord		SetCtrlStyles	(int nCtrlId, DWord dClrMask, DWord dSetMask) const;

	Bool		EnableCtrl	(int nCtrlId, Bool bEnable)
			    { return ::EnableWindow (GetCtrlHWND (nCtrlId), bEnable);};

	Bool		ShowCtrl	(int nCtrlId, Bool bShow  )
			    {
			      return ::ShowWindow   (GetCtrlHWND (nCtrlId), bShow ? SW_SHOW : SW_HIDE); 
			    };

	Bool		IsCtrlChecked	 (int nCtrlId) const
			    { return (BST_CHECKED == ::IsDlgButtonChecked (GetHWND (), nCtrlId)) ? True : False;};

	Bool		CheckCtrl	 (int nCtrlId, Bool bCheck)
			    { return ::CheckDlgButton (GetHWND (), nCtrlId, (bCheck) ? BST_CHECKED : BST_UNCHECKED);};

	Bool		CheckRadioButton (int nCtrlId0, int nCtrlIdN, int nCtrlId)
			    { return ::CheckRadioButton (GetHWND (), nCtrlId0,
								     nCtrlIdN, nCtrlId);}

	LRESULT		SendCtrlMessage	 (int nCtrlId, UINT uMsg, WPARAM wParam, LPARAM lParam)
			    { return ::SendDlgItemMessage (GetHWND (), nCtrlId, uMsg, wParam, lParam);};
//
// Dialog control's wrapper --------------------------------------------------
//
//	    ...
//
// User32 API wrapper --------------------------------------------------------

// Size/Position wrapper -----------------------------------------------------
//
//	void		GetClientRect	    (PRECT   pRect) const
//			    { CopyRect (pRect, & GetClientRect ());};

//	void		GetClientRect	(GRect * pRect) const
//			    { * pRect = GetClientRect ();};

	void		GetClientRect	(PRECT	 pRect ) const
			    { ::GetClientRect (GetHWND (), pRect);};

	void		GetClientRect	(GRect * pRect ) const
			    { ::GetClientRect (GetHWND (), pRect);};

	Bool		GetClientRectEx (PRECT	 pRect ) const
			    { return ::GetClientRectEx (GetHWND (), pRect);}

	Bool		GetClientRectEx (GRect * pRect ) const
			    { return ::GetClientRectEx (GetHWND (), pRect);};

	Bool		ClientToScreen	(PPOINT  pPoint) const
			    { return ::ClientToScreen (GetHWND (), pPoint);};

	Bool		ClientToScreen  (PRECT   pRect ) const
			    { return ::ClientToScreen (GetHWND (), pRect );};

	Bool		ClientToScreen  (GRect * pRect ) const
			    { return ::ClientToScreen (GetHWND (), pRect );};

	Bool		ScreenToClient  (PPOINT pPoint) const
			    { return ::ScreenToClient (GetHWND (), pPoint);};

	Bool		ScreenToClient	(PRECT pRect)   const
			    { return ::ScreenToClient (GetHWND (), pRect);};

	Bool		ScreenToClient  (GRect * pRect) const
			    { return ::ScreenToClient (GetHWND (), pRect);};
//
//----------------------------------------------------------------------------
//
//	void 		GetWindowRect	(GRect * pRect)  const
//			    { * pRect = GetWindowRect ();};

//	void		GetWindowRect	(PRECT	 pRect)  const
//			    { CopyRect (pRect, & GetWindowRect ());};

	void		GetWindowRect	(PRECT	 pRect ) const
			    { ::GetWindowRect (GetHWND (), pRect, GetParent () ? GetParent () ->GetHWND () : NULL);};

	void		GetWindowRect	(GRect * pRect ) const
			    { ::GetWindowRect (GetHWND (), pRect, GetParent () ? GetParent () ->GetHWND () : NULL);};

	Bool		WindowToScreen	(PPOINT	 pPoint) const
			    { return ::WindowToScreen (GetHWND (), pPoint);};

	Bool		WindowToScreen	(PRECT	 pRect ) const
			    { return ::WindowToScreen (GetHWND (), pRect );};

	Bool		WindowToScreen	(GRect * pRect ) const
			    { return ::WindowToScreen (GetHWND (), pRect );};
/*
	Bool		ScreenToWindow	(PPOINT	 pPoint) const;

	Bool		ScreenToWindow	(PRECT	 pRect ) const;

	Bool		ScreenToWindow	(GRect * pRect ) const;
*/
	HWND		GetFirstChild	()
			    { return ::GetWindow (GetHWND (), GW_CHILD);};

	HWND		GetLastChild	()
			    { return ::GetWindow (
				     ::GetWindow (GetHWND (), GW_CHILD   )
							    , GW_HWNDLAST);};
static	HWND		GetNextChild	(HWND hWnd)
			    { return ::GetWindow (hWnd	    , GW_HWNDNEXT);};

static	HWND		GetPrevChild	(HWND hWnd)
			    { return ::GetWindow (hWnd	    , GW_HWNDPREV);};

	Bool		SetWindowPos	(HWND	hAfter	,   // SWP_NOMOVE
					 int	x	,   // SWP_NOSIZE
					 int	y	,
					 int	cx	,
					 int	cy	,
					 UINT	uFlags	);
//
// Size/Position wrapper -----------------------------------------------------

// Paint wrapper -------------------------------------------------------------
//
	Bool		InvalidateRect	(const GRect * pRect, Bool bErase) const
			{
			    RECT    r;
			    RECT *  p = (pRect) ? & r : NULL;

			    if (p)
			    {
				CopyRect (p, pRect);
			    }

			    return ::InvalidateRect (GetHWND (), p, bErase);
			};

	Bool		ValidateRect	(const GRect * pRect) const
			{
			    RECT    r;
			    RECT *  p = (pRect) ? & r : NULL;

			    if (p)
			    {
				CopyRect (p, pRect);
			    }

			    return ::ValidateRect (GetHWND (), p);
			};
//
// Paint wrapper -------------------------------------------------------------

	LRESULT		Send_WM		(UINT uMsg, WPARAM wParam ,
						    LPARAM lParam );

	Bool		Post_WM		(UINT uMsg, WPARAM wParam ,
						    LPARAM lParam )
			{
			    return (NULL !=  GetHWND ()) 
			    ? ::PostMessage (GetHWND (), uMsg, wParam, lParam) : False;
			};

// Size/Position wrapper -----------------------------------------------------
//
//    const GRect &	GetWindowRect	()		 const
//			    { return m_WRect;};
//
//	    void	GetWindowRect	(PRECT	 pRect)  const
//			    { CopyRect (pRect, & GetWindowRect ());};
//
//	    void 	GetWindowRect	(GRect * pRect)  const
//			    { * pRect = GetWindowRect ();};
//
// Size/Position wrapper -----------------------------------------------------

	DWord		ExecWindow	    ();

	void		Destroy		    ();

	void		DestroyChilds	    (HWND);

	void		SubclassWndProc	    (GWinMsgProc pWinProc, void * oWinProc)
			{
			    // GASSERT (GetHWND ())
			    //
			    ::SetWindowLongPtr (GetHWND (), GWLP_WNDPROC,
							    (LONG_PTR)
			    (m_pWndProc = new GWndProcThunk (pWinProc   ,
							     oWinProc   ,
							    (WNDPROC )
			    ::GetWindowLongPtr (GetHWND (), GWLP_WNDPROC))));
			};

	void		UnSubclassWndProc   ()
			{
			    // GASSERT (GetHWND ())
			    //
			    GWndProcThunk * thnk;

			    if (m_pWndProc)
			    {
				thnk = (GWndProcThunk *)  m_pWndProc ->oDefProc;

				::SetWindowLongPtr (GetHWND (), GWLP_WNDPROC,
							       (LONG_PTR) thnk);
				delete m_pWndProc;

				m_pWndProc = thnk ->IsThunkPtr () ? thnk : NULL;
			    }
			};
protected :

virtual	GWindowClass &	GetWindowClass	    () const =  NULL;

virtual	Bool		On_Create	    (void * pParam);

virtual	void		On_Destroy	    ();

static	Bool	  GAPI	CreateException	    (void *, GException_Record &);

static	LRESULT WINAPI _initWndProc	    (HWND, UINT, WPARAM, LPARAM);

	struct CreationParams
	{
	    LPCTSTR	    pTitle    ;
	    DWORD	    dStyle    ;
	    DWORD	    dExStyle  ;
	    int		    x , y     ;
	    int		    cx, cy    ;
	    HMENU	    hMenuOrId ;
	    void *	    pParam    ;
	};

	Bool		Create	    (GWindow *	pParent ,
				     HWND		,
				     void *	pParam	);

	Bool		Create	    (GWindow *	pParent	,
				     CreationParams   &	);

	Bool		Create	    (GWindow *	pParent	,
				     LPCTSTR    pTitle	,
				     DWord      dStyle	,
				     DWord      dExStyle,
				     int	x       ,
				     int	y       ,
				     int	cx      ,
				     int	cy	,
				     void *	pParam	);

// Message Dispatching -------------------------------------------------------
//
public :

virtual	Bool		PreprocMessage	    (    MSG *);

protected :

	int		IsCommand	    (GWinMsg &);

	int		IsControlNotify	    (GWinMsg &, HWND * hCtrl = NULL);

virtual	Bool		On_ReflectWinMsg    (GWinMsg &);

virtual	Bool		On_WinMsg	    (GWinMsg &);

static	Bool GAPI	DispatchWinMsg	    (void *, GWinMsg &);
//
// Message Dispatching -------------------------------------------------------

protected :

	    HWND		    m_hWnd	;
	    const void *	    m_pWndId	;
	    DWord		    m_dFlags	;

	    GWindow *		    m_pParent	;

	    GWndProcThunk *	    m_pWndProc	;
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
class GParentWindow : public GWindow
{
DECLARE_EXT_WINDOW_CLASS (_T("GWin.ParentWindow"), CS_DBLCLKS);

public :

	    GParentWindow (const void * pWndId) :

	    GWindow	  (pWndId)
	    {};

	Bool		PreprocMessage		    (MSG * msg);

protected :

	Bool		On_WinMsg		    (GWinMsg &);
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
class GDialog : public GParentWindow
{
DECLARE_SYS_WINDOW_CLASS (WC_DIALOG);
//[]------------------------------------------------------------------------[]
//  Windows Dialog class subclassing :
/*
    protected :
	    GWindowClass &  GetWindowClass  () const
	    {
		    return s_WndClass;
	    };
    private :
    static  LPCTSTR GAPI    GetWindowClassInfo	(WNDCLASSEX * wc = NULL)
	    {
		    LPCTSTR pName	= _T("GWin.Dialog");

		if (wc)
		{
		    ::GetWindowClassInfo (WC_DIALOG, wc);

		    wc ->lpfnWndProc	= _initWndProc	;
		    wc ->lpszClassName	= pName		;

		    wc ->cbWndExtra	= DLGWINDOWEXTRA;
		}
		    return  pName;
	    };
    static  GWindowClass    s_WndClass;
*/
//  Windows Dialog class end subclassing
//[]------------------------------------------------------------------------[]
//
public :

	    GDialog	   (const void * pWndId) :

	    GParentWindow  (pWndId)
	    {
		m_pDlgProc  = NULL;
	    };

	   ~GDialog	();

	GDlgProcThunk *	GetDlgProc	() const { return m_pDlgProc;};

	Bool		Create		(GWindow *  pParent	 ,
					 HMODULE    hResModule	 ,
					 LPCTSTR    pResName	 ,
					 void *	    pParam = NULL);

	void		SubclassDlgProc	    (GWinMsgProc pDspProc, void * oDspProc)
			{
			    // GASSERT (GetHWND ())
			    //
			    ::SetWindowLongPtr (GetHWND (),   DWLP_DLGPROC,
							      (LONG_PTR) 
			    (m_pDlgProc	=  new  GDlgProcThunk (pDspProc   , 
						oDspProc  ,   (DLGPROC )
			    ::GetWindowLongPtr (GetHWND (),   DWLP_DLGPROC))));
			};

	void		UnSubclassDlgProc   ()
			{
			    // GASSERT (GetHWND ())
			    //
			    GDlgProcThunk * thnk;

			    if (m_pDlgProc)
			    {
				thnk = (GDlgProcThunk *)  m_pDlgProc ->oDefProc;

				::SetWindowLongPtr (GetHWND (), DWLP_DLGPROC,
							       (LONG_PTR) thnk);
				delete m_pDlgProc;

				m_pDlgProc = thnk ->IsThunkPtr () ? thnk : NULL;
			    }
			};

protected :

virtual	GWindow *	GetCreationWindow   (int nCtrlId);

static	Bool	  GAPI	CreateException	    (void *, GException_Record &);

static	BOOL WINAPI    _initDlgProc	    (HWND, UINT, WPARAM, LPARAM);

// Message Dispatching -------------------------------------------------------
//
virtual	Bool		On_InitDialog	    (GWinMsg &);

virtual	Bool		On_DlgMsg	    (GWinMsg &);

static	Bool GAPI	DispatchDlgMsg	    (void *, GWinMsg &);
//
// Message Dispatching -------------------------------------------------------

protected :

	    GDlgProcThunk *	    m_pDlgProc	;
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
class GFrameWindow : public GParentWindow
{
DECLARE_EXT_WINDOW_CLASS (_T("GWin.FrameWindow"), CS_DBLCLKS);

public :
		GFrameWindow  (const void * pWndId) : 

		GParentWindow (pWndId) 
		{};

	Bool		Create	    (GWindow *	pOwner	      ,
				     LPCTSTR    pTitle	      ,
				     DWord      dStyle	      ,
				     DWord      dExStyle      ,
				     int	x       = 0   ,
				     int	y       = 0   ,
				     int	cx      = 0   ,
				     int	cy      = 0   ,
				     void *	pParam  = NULL)
			{
			    return GWindow::Create (pOwner		 , pTitle  ,
						     dStyle & (~WS_CHILD), dExStyle,
						     x, y, cx, cy,	   pParam  );
			};
protected :
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
class GFrameControl : public GFrameWindow
{
#if FALSE

DECLARE_EXT_WINDOW_CLASS (_T("GWin.FrameControl"), 0);

#else ////////////////////////////////////////////////////

    public :
    static  Bool RegisterWindowClass ()
	    {
		return (NULL == s_WndClass.Registered)
			      ? s_WndClass.Register () : False;
	    };
    protected :
	    GWindowClass &  GetWindowClass  () const
	    {
		    return s_WndClass;
	    };
    private :
    static  LPCTSTR GAPI    GetWindowClassInfo	(WNDCLASSEX * wc = NULL)
	    {
		    LPCTSTR pName	= _T("GWin.FrameControl");

		if (wc)
		{
		    ::GetWindowClassInfo (WC_DIALOG, wc);

		    wc ->cbWndExtra	= DLGWINDOWEXTRA;

		    wc ->lpfnWndProc	= _initWndProc	;
		    wc ->lpszClassName	= pName		;
		}
		    return  pName;
	    };
    static  GWindowClass    s_WndClass;

#endif //////////////////////////////////////////////////

public :
	    GFrameControl (const void * pWndId) : 

	    GFrameWindow  (pWndId) 
	    {
	    };

	Bool		Create	    (GWindow *	pOwner	      ,
				     LPCTSTR    pTitle	      ,
				     DWord      dStyle	      ,
				     DWord      dExStyle      ,
				     int	x       = 0   ,
				     int	y       = 0   ,
				     int	cx      = 0   ,
				     int	cy      = 0   ,
				     void *	pParam  = NULL)
			{
			    return GWindow::Create  (pOwner		 , pTitle  ,
						     dStyle & (~WS_CHILD), dExStyle,
						     x, y, cx, cy,	   pParam  );
			};
protected :

	void		On_Destroy		    ();
	Bool		On_Create		    (void *);
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
class GControl : public GWindow
{
public :
		GControl    (const void * pWndId) :

		GWindow	    (pWndId)
		{};

	Bool		Create	    (GWindow *  pParent	      ,
				     HWND	hWnd	      ,
				     void *	pParam	= NULL)
			{
			    return GWindow::Create (pParent, hWnd, pParam);
			};

	Bool		Create	    (GWindow *	pParent	      ,
				     LPCTSTR    pTitle	      ,
				     DWord      dStyle	      ,
				     DWord      dExStyle      ,
				     int	x       = 0   ,
				     int	y       = 0   ,
				     int	cx      = 0   ,
				     int	cy      = 0   ,
				     void *	pParam  = NULL)
			{
			    return GWindow::Create (pParent	      , pTitle   ,
						   (dStyle | WS_CHILD), dExStyle ,
						    x, y, cx, cy, pParam);
			};
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
class GButton : public GControl
{
DECLARE_SYS_WINDOW_CLASS (_T("Button"));

public :
		GButton	    (const void * pWndId = NULL) :

		GControl    (pWndId)
		{};
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
class GTreeView : public GControl
{

DECLARE_SYS_WINDOW_CLASS (WC_TREEVIEW);

public :

	    GTreeView (const void * pWndId) :

	    GControl  (pWndId)
	    {
		m_SelItem.mask  = 0;
	      *	m_SelItemText	= 0;
	    };

	DECLARE_INTERFACE_ (IItem, XHandle)
	{
	STDMETHOD_	(void,		    Destroy)() PURE;
	};


	HTREEITEM	InsertItem	    (HTREEITEM	    hParent	       ,
					     HTREEITEM	    hInsertAfter       ,
					     LPCTSTR	    pItemName	       ,
				       const void *	    pItemContext = NULL);
/*
					    hParent : 
					    NULL	    - Root

					    hInsertAfter : 
					    TVI_ROOT
					    TVI_FIRST
					    TVI_LAST
					    TVI_SORT

					    pItemName : 
					    NULL	    - LPSTR_TEXTCALLBACK used !!!
*/
	void 		DeleteItem	    (HTREEITEM	    hItem  )
			{
			    Send_WM (TVM_DELETEITEM, (WPARAM) 0, (LPARAM) hItem);
			};

	HTREEITEM	GetItem		    (HTREEITEM	    hFrom  ,
					     UINT	    uFlags )
			{
			    return (HTREEITEM) 

			    Send_WM (TVM_GETNEXTITEM, (WPARAM) uFlags, (LPARAM) hFrom);
			};
/*					    GetItem::uFlags :

					    TVGN_CARET
					    TVGN_CHILD
					    TVGN_DROPHILITE
					    TVGN_FIRSTVISIBLE
					    TVGN_LASTVISIBLE
					    TVGN_NEXT
					    TVGN_NEXTVISIBLE
					    TVGN_PARENT
					    TVGN_PREVIOUS
					    TVGN_PREVIOUSVISIBLE
					    TVGN_ROOT
*/
	Bool		Expand		    (HTREEITEM	    hItem	       ,
					     UINT	    uFlags = TVE_EXPAND)
			{
			    return (Bool) Send_WM (TVM_EXPAND, (WPARAM) TVE_EXPAND, (LPARAM) hItem);
			};
/*
	    				    Expand::uFlags :

					    TVE_COLLAPSE
					    TVE_COLLAPSERESET
					    TVE_EXPAND
					    TVE_EXPANDPARTIAL
					    TVE_TOGGLE
*/



	Bool		SelectItem	    (HTREEITEM	    hItem	       ,
					     UINT	    uFlags = TVGN_CARET)
			{
			    return (Bool) Send_WM (TVM_SELECTITEM, (WPARAM) uFlags, (LPARAM) hItem);
			};
/*
					    SelectItem::uFlags :

					    TVGN_CARET
					    TVGN_DROPHILITE
					    TVGN_FIRSTVISIBLE
					    TVSI_NOSINGLEEXPAND
*/

	const TCHAR *	GetSelItemText	    () { return m_SelItemText;};


protected :

	Bool		On_Create	    (void * pParam);
	void		On_Destroy	    ();

	Bool		TVN_GetDispInfo	    (GWinMsg &, NMTVDISPINFO *);
	Bool		TVN_SelChanging	    (GWinMsg &, NMTREEVIEW   *);
	Bool		TVN_SelChanged	    (GWinMsg &, NMTREEVIEW   *);
	Bool		TVN_DeleteItem	    (GWinMsg &, NMTREEVIEW   *);

	Bool		On_WinMsg	    (GWinMsg &);

protected :

	    TVITEM		    m_SelItem		;
	    TCHAR		    m_SelItemText [256]	;
};
//
//[]------------------------------------------------------------------------[]

#endif//__G_GUIBASE_H
