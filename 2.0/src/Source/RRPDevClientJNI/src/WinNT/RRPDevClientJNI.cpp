//[]------------------------------------------------------------------------[]
// RRPDevClientJNI Java interface definition
//
//
// Created by GSh.
//[]------------------------------------------------------------------------[]

#include "RRPDevClientJNI.h"

#include "R_rrpdevs.h"

static void	JNI_LockAcquire (); // (G*)
static void	JNI_LockRelease (); // (G*)
static JNIEnv *	GetJNIEnv	();

static JavaVM *	s_jvm       = NULL;
static HANDLE	s_hJNI_Lock = NULL; // Lock for (G*)

namespace RRPDevClientJNI
{
//[]------------------------------------------------------------------------[]
//
class CRRPCallBack : public IRRPCallBack
{
public :
			CRRPCallBack ()
			{
			    m_pCallBack = NULL;
			    m_pContext  = NULL;
			};

		       ~CRRPCallBack ()
			{};

	void		SetHandler  (void (GAPI * pCallBack)(void *, const void *, DWord, DWord), void * pContext)
			{
			    m_pCallBack = pCallBack;
			    m_pContext  = pContext;			    
			};

	LONG		GetRefCount () const { return m_cRefCount;};

protected :

    STDMETHOD  (     QueryInterface)(REFIID, void **);

    STDMETHOD_ (ULONG,	     AddRef)();

    STDMETHOD_ (ULONG,	    Release)();

    STDMETHOD_ (void,	     Invoke)(const void * hICallBack ,
				     const 
				     IRRPProtocol::DataPack *);
protected :

	TInterlocked <LONG>	    m_cRefCount;

	void		    (GAPI * m_pCallBack)(void *, const void *, DWord, DWord);
	void *			    m_pContext ;
};
//
//[]------------------------------------------------------------------------[]
//
HRESULT CRRPCallBack::QueryInterface (REFIID, void ** pi)
{
    if (pi)
    {
      * pi = NULL;
    }
	return ERROR_NOT_SUPPORTED;
};

ULONG CRRPCallBack::AddRef ()
{
	return ++ m_cRefCount;
};

ULONG CRRPCallBack::Release ()
{
	return -- m_cRefCount;;
};

void CRRPCallBack::Invoke (const void * hICallBack	 ,
			   const IRRPProtocol::DataPack *
					pDataPack	 )
{
    Word    uLength = getBigEndianW (pDataPack, offsetof (IRRPProtocol::DataPack, uLength));
    Word    uType   = getBigEndianW (pDataPack, offsetof (IRRPProtocol::DataPack, uType  ));
    IRRPProtocol::ParamPack::Entry *
	    pEntry;

    if (0 == uType)
    {
	pEntry = (IRRPProtocol::ParamPack::Entry *)(pDataPack + 1);

	while ((Byte *) pEntry < ((Byte *) pDataPack + uLength))
	{
	    (m_pCallBack)(m_pContext, hICallBack, 
				      getBigEndianD (pEntry, offsetof (IRRPProtocol::ParamPack::Entry, uParamId)),
				      getBigEndianD (pEntry, offsetof (IRRPProtocol::ParamPack::Entry, uValue  )));
	    pEntry ++;
	}
    }
};
//
//[]------------------------------------------------------------------------[]
}
using RRPDevClientJNI::CRRPCallBack;



//[]------------------------------------------------------------------------[]
//
class JObject
{
public :

	typedef TSLink <JObject, sizeof (void *)>   Link ;
	typedef TXList <JObject,
		TSLink <JObject, sizeof (void *)> > List ;
public :

		    JObject () : m_cRefCount (1)   ,
				 m_pParent   (NULL),
				 m_pObject   (NULL) {};

	LONG			GetRefCount	    () const { return m_cRefCount;};

	LONG			AddRef		    () { return ++ m_cRefCount; };

virtual	LONG			Release		    ();

virtual	LONG			ReleaseChild	    (JObject *);

public :

	Link			m_qLink	    ;
	TInterlocked <LONG>	m_cRefCount ;
	JObject *		m_pParent   ;
	void *			m_pObject   ;
};

LONG JObject::Release ()
{
    if (0 == -- m_cRefCount)
    {
	return (m_pParent) ? m_pParent ->ReleaseChild (this) : 0;
    }
	return  m_cRefCount;
};

LONG JObject::ReleaseChild (JObject * o)
{
    return 0;
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
class RRPClient : public JObject
{
public :
		    RRPClient (jobject o) :

			m_jobject (GetJNIEnv () ->NewGlobalRef (o))
		    {
			m_pObject	= this;
			m_hRun		=   DuplicateHandle
					 (::GetCurrentThread   ());
			m_dRunId	= ::GetCurrentThreadId ();

			m_pIRRPService	= NULL;
		    };

		   ~RRPClient ()
		    {
		    if (m_pIRRPService)
		    {
			m_pIRRPService  ->Release ();
			m_pIRRPService  = NULL;
		    }
			::CloseHandle	(m_hRun);
					 m_hRun = NULL;
			m_pObject = NULL;

			GetJNIEnv () ->DeleteGlobalRef 
					(m_jobject);
					 m_jobject = NULL;
		    }
friend
JNIEXPORT jobject JNICALL Java_com_xprom_hardware_XPDeviceService_ntCreateInstance
				(JNIEnv * env, jclass cls);

	jobject			CreateXPDevice	    (jint devId);


	void			Run		    ();

static	GResult GAPI		Main		    (int, void **, HANDLE);

#pragma pack (_M_PACK_LONG)

	struct Param
	{
	    _U32	uParamId ;

	union
	{
	    _I32	i32Value ;
	    _U32	u32Value ;
	    _R32	r32Value ;
        }			 ;
	};

#pragma pack ()

//.............................
//
class CallBack : public JObject
{
public :
		    CallBack (jobject o)
		    {
			m_cbhandle	= GetJNIEnv () ->NewGlobalRef (o);

			m_pGetQueueHead =
			m_pGetQueueTail =
			m_pGetQueue	  ;
		    };

		   ~CallBack ()
		    {
			GetJNIEnv () ->DeleteGlobalRef (m_cbhandle);
			m_cbhandle	= NULL;
		    };

	void			SelectForListen	    (jsize, jint *, Bool);

	void			RequestForGet	    (jsize, jint *);

	Bool			GetQueuedParam	    (Param *);

	void			PutParam	    (_U32, _U32);
public :

	GCriticalSection	m_oLock		 ;

	jobject			m_cbhandle	 ;

	IRRPDevice::Param *	m_pGetQueueHead	 ;
	IRRPDevice::Param *	m_pGetQueueTail	 ;
	IRRPDevice::Param	m_pGetQueue [256];
};
//
//.............................
//
class Device : public JObject
{
public :
		    Device (jobject o, jint devId) :

			m_jobject (GetJNIEnv () ->NewGlobalRef (o)),
			m_uDevId  (devId)
		    {
			m_pObject	= this;
			m_pIRRPDevice	= NULL;
			m_hIRRPCallBack = NULL;
		    };

		   ~Device ()
		    {
		    if (m_pIRRPDevice)
		    {
		    {{
			TThread <GThread> th;

		    if (m_hIRRPCallBack)
		    {
			m_hIRRPCallBack ->Close ();
		    }
			m_pIRRPDevice ->Release ();
		    }}
			m_pIRRPDevice = NULL;
		    }

			m_pObject     = NULL;
				   GetJNIEnv () ->DeleteGlobalRef
		       (m_jobject);
			m_jobject     = NULL;
		    };
public :

	LONG			ReleaseChild	    (JObject * pCallBack);

	jobject			ConnectCb	    (jobject);

	void			DisconnectCb	    (CallBack *);


	void		On_IRRPCallBack	    (	     const void *, _U32, _U32);

static	void GAPI	On_IRRPCallBack	    (void *, const void *, _U32, _U32);

public :

	GCriticalSection	m_oLock		 ;   // Lock for (**)
	JObject::List		m_uCbpList	 ;   // (**)

	jobject			m_jobject	 ;

	jint			m_uDevId	 ;
	IRRPDevice *		m_pIRRPDevice	 ;

	RRPDevClientJNI::CRRPCallBack
				m_sIRRPCallBack	 ;
	IRRPCallBack::Handle *	m_hIRRPCallBack	 ;
};
//
//.............................

	LONG			Release		    ();

protected :

	LONG			ReleaseChild	    (JObject * pDevice);

public :

static	RRPClient *		s_pRRPClient	;

protected :

	GCriticalSection	m_oLock		;    // Lock for (*)
	JObject::List		m_uDevList	;    // (*)

	jobject			m_jobject	;

	HANDLE			m_hRun		;
	DWORD			m_dRunId	;

public :

	IRRPDeviceService *	m_pIRRPService	;
};
//
//[]------------------------------------------------------------------------[]
//
LONG RRPClient::Device::ReleaseChild (JObject * pCallBack)
{
    return 0;
};

jobject RRPClient::Device::ConnectCb (jobject cbhandle)
{
    JNIEnv * env = GetJNIEnv ();

    RRPClient::CallBack	* cb = new RRPClient::CallBack (cbhandle);

    env ->SetLongField	 (cb ->m_cbhandle,
    env ->GetFieldID     (env ->GetObjectClass (cb ->m_cbhandle), "m_ntHandle", "J"), (jlong) cb);

    m_oLock.LockAcquire  ();

	m_uCbpList.append (cb);

    m_oLock.LockRelease  ();

    return cbhandle;
};

void RRPClient::Device::DisconnectCb (RRPClient::CallBack * cb)
{
    JNIEnv * env = GetJNIEnv ();

    m_oLock.LockAcquire ();

	m_uCbpList.extract (cb);

    m_oLock.LockRelease ();

    env ->SetLongField	  (cb ->m_cbhandle,
    env ->GetFieldID	  (env ->GetObjectClass (cb ->m_cbhandle), "m_ntHandle", "J"), (jlong) 0);

    delete cb;
};

void RRPClient::CallBack::SelectForListen (jsize nIds, jint * pIds, Bool bSelect)
{
};

void RRPClient::CallBack::RequestForGet (jsize nIds, jint * pIds)
{
};

Bool RRPClient::CallBack::GetQueuedParam (RRPClient::Param * p)
{
	Bool	bResult  = False;

    m_oLock.LockAcquire ();

	if (p)
	{
	    if (m_pGetQueueHead < m_pGetQueueTail)
	    {
		p ->uParamId    = m_pGetQueueHead ->uParamId;
		p ->u32Value    = m_pGetQueueHead ->u32Value;

	    if (++ m_pGetQueueHead == m_pGetQueueTail)
	    {
		m_pGetQueueHead =
		m_pGetQueueTail = m_pGetQueue;
	    }
		bResult		= True;
	    }
	}
	else
	{
		m_pGetQueueHead =
		m_pGetQueueTail = m_pGetQueue;
	}

    m_oLock.LockRelease ();

    return (jboolean) bResult;
};

void RRPClient::CallBack::PutParam (_U32 uParamId, _U32 uValue)
{
    Bool    bNotify	     = False;

    m_oLock.LockAcquire ();

	if (m_pGetQueueHead == m_pGetQueueTail)
	{
	    bNotify	     = True ;
	}
	if (m_pGetQueueTail < m_pGetQueue + (sizeof (m_pGetQueue) / sizeof (IRRPDevice::Param)))
	{
	    m_pGetQueueTail ->uParamId = uParamId;
	    m_pGetQueueTail ->u32Value = uValue  ;

	    m_pGetQueueTail ++;
	}

    m_oLock.LockRelease ();

	if (bNotify)
	{{
	    Bool     ext = False;
	    JNIEnv * env = GetJNIEnv ();

	if (NULL ==  env)
	{
	    ext   = True;
	    s_jvm ->AttachCurrentThread ((void **) & env, NULL);
	}
	    jmethodID  m = env ->GetMethodID    (
			   env ->GetObjectClass (m_cbhandle), "OnDataAvailable", "()V");

			   env ->CallVoidMethod (m_cbhandle , m);
	if (ext)
	{
	    s_jvm ->DetachCurrentThread ();
	}
	}}
};

void RRPClient::Device::On_IRRPCallBack (const void *, _U32 uParamId, _U32 uValue)
{
    m_oLock.LockAcquire ();

	for (RRPClient::CallBack * cb = (RRPClient::CallBack *) m_uCbpList.first (); 
				   cb; 
				   cb = (RRPClient::CallBack *) m_uCbpList.getnext (cb))
	{
	    cb ->PutParam (uParamId, uValue);
	}

    m_oLock.LockRelease ();
};

void RRPClient::Device::On_IRRPCallBack (void * pContext, const void *, _U32 uParamId, _U32 uValue)
{
    ((RRPClient::Device *) pContext) ->On_IRRPCallBack (NULL, uParamId, uValue);
};
//
//[]------------------------------------------------------------------------[]
//
void RRPClient::Run ()
{
    while (0 < GetRefCount ())
    {
	::SleepEx (INFINITE, True);
    }
};

GResult GAPI RRPClient::Main (int, void ** pArgs, HANDLE hResume)
{
    JNIEnv * env = NULL;
    jclass   cls = NULL;

    s_jvm ->AttachCurrentThread ((void **) & env, NULL);

    {
	cls	    = (jclass) pArgs [0];
	jmethodID m = env ->GetMethodID (cls, "<init>", "()V");
	jobject   o = (m) ? env ->NewObject   (cls, m) :  NULL;
	jfieldID  h = NULL;

	if (o)
	{{
	    RRPClient *	   pRRPClient   = NULL;
	    IRRPDeviceService *
			   pIRRPService = NULL;

	    if (NULL != (s_pRRPClient = pRRPClient = new RRPClient (o)))
	    {
		CreateRRPDeviceService (pIRRPService, NULL);

		if (pIRRPService)
		{
		   (pRRPClient ->m_pIRRPService = 
		    pIRRPService) ->AddRef ();
		}

		env ->SetLongField (o,
		env ->GetFieldID   (env ->GetObjectClass (o), "m_ntHandle", "J"), (jlong) pRRPClient);

		if (hResume)
		{
		    ::SetEvent (hResume);
		}

		    pRRPClient ->Run ();

		    s_pRRPClient = NULL;

		delete pRRPClient  ;

		if (pIRRPService)
		{
		    pIRRPService ->Release ();
		    pIRRPService = NULL	    ;
		}

//		::MessageBox (NULL, "Hello from XPDeviceService_ntClose", "Message", MB_OK | MB_ICONHAND);
	    }
	}}
    }

    s_jvm ->DetachCurrentThread ();

    return ERROR_SUCCESS;
};

LONG RRPClient::Release ()
{
    LONG      cRefCount;

    if (0 == (cRefCount = JObject::Release ()))
    {
    JNI_LockAcquire ();

	if (0 == (cRefCount = m_cRefCount))
	{{
	    JNIEnv * env    = GetJNIEnv ();

	    env ->SetLongField (m_jobject,
	    env ->GetFieldID   (env ->GetObjectClass (m_jobject), "m_ntHandle", "J"), 0);

	    s_pRRPClient    = NULL;

    JNI_LockRelease ();

	    QueueUserAPC (m_hRun, NULL, 0, NULL, NULL);

	    return 0;
	}}

    JNI_LockRelease ();
    }

    return  cRefCount;
};

LONG RRPClient::ReleaseChild (JObject * pDevice)
{
    LONG    cRefCount;

    m_oLock.LockAcquire (True);

	if (0 == (cRefCount = pDevice ->m_cRefCount))
	{{
	    JNIEnv * env    = GetJNIEnv ();

	    m_uDevList.extract (pDevice);

	    env ->SetLongField (((Device *) pDevice) ->m_jobject,
	    env ->GetFieldID   (env ->GetObjectClass (((Device *) pDevice) ->m_jobject), "m_ntHandle", "J"), 0);

    m_oLock.LockRelease ();

// GASSERT (pDevice ->m_pParent == this)
//
	    pDevice ->m_pParent = NULL;

	    delete ((Device *) pDevice);

    		      this ->Release ();

	    return 0;
	}}

    m_oLock.LockRelease ();

    return  cRefCount;
};
//
//[]------------------------------------------------------------------------[]
//
jobject RRPClient::CreateXPDevice (jint devId)
{
    TXList_Iterator <JObject> I;

    Device *	 pDevice = NULL;

    m_oLock.LockAcquire ();

	for (I = m_uDevList.head (); I.next ();)
	{
	    if (((Device *) I.next ()) ->m_uDevId == devId)
	    {
		pDevice = (Device *) I.next ();

		pDevice ->AddRef ();

    m_oLock.LockRelease ();

		return pDevice ->m_jobject;
	    }
	    I <<= m_uDevList.getnext (I.next ());
	}

	JNIEnv *     env = GetJNIEnv ();
	jclass	     cls = NULL;

	IRRPDevice * pIRRPDevice = NULL;

	switch (devId)
	{
	case 0x50000000 :
		cls = env ->FindClass ("com/xprom/hardware/XPDMSDevice");

	{{
		TThread <GThread> th;

		m_pIRRPService ->CreateRRPDevice ((void **) & pIRRPDevice,
						   IID_IUnknown		 ,
						  (void  *)   0x50000000 ,
						   IID_IUnknown		 );
	}}
		break;
	};

	if (cls)
	{
	jmethodID  m = env ->GetMethodID (cls, "<init>", "()V");
	jobject	   o = (m) ? env ->NewObject (cls, m) : NULL;

	if (o)
	{
	    if (NULL != (pDevice = new Device (o, devId)))
	    {
		env ->SetLongField (o  ,
		env ->GetFieldID   (env ->GetObjectClass (o), "m_ntHandle", "J"), (jlong) pDevice);

	        pDevice ->m_pParent = this;
				      this ->AddRef ();

		m_uDevList.append  (pDevice);

	       (pDevice ->m_pIRRPDevice = pIRRPDevice) ->AddRef ();

	    if (pIRRPDevice)
	    {
		    pDevice ->m_sIRRPCallBack.SetHandler 
				     (Device::On_IRRPCallBack, pDevice);

		{{
		    TThread <GThread> th;

		    pIRRPDevice ->Connect (pDevice ->m_hIRRPCallBack ,
					 & pDevice ->m_sIRRPCallBack );
		}}
	    }
	    }
	}
	}

	if (pIRRPDevice)
	{
	    pIRRPDevice ->Release ();
	}

    m_oLock.LockRelease ();

	return (pDevice) ? pDevice ->m_jobject : NULL;
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
JNIEXPORT void JNICALL Java_com_xprom_hardware_XPDevice_ntClose
		      (JNIEnv * env, jobject dev)
{
    RRPClient::Device * pDevice = (RRPClient::Device *)
    env ->GetLongField (dev,
    env ->GetFieldID   (env ->GetObjectClass (dev), "m_ntHandle", "J"));

    if (pDevice)
    {
	pDevice ->Release ();
    }
};

JNIEXPORT jobject JNICALL Java_com_xprom_hardware_XPDevice_ntConnect
			 (JNIEnv * env, jobject dev, jobject cbhandle)
{
    RRPClient::Device * pDevice = (RRPClient::Device *)
    env ->GetLongField (dev,
    env ->GetFieldID   (env ->GetObjectClass (dev), "m_ntHandle", "J"));

    if (pDevice)
    {
	return pDevice ->ConnectCb (cbhandle);
    }
	return NULL	;
};

JNIEXPORT void JNICALL Java_com_xprom_hardware_XPDevice_ntDisconnect
		      (JNIEnv * env, jobject dev, jobject cbhandle)
{
    RRPClient::Device * pDevice = (RRPClient::Device *)
    env ->GetLongField (dev,
    env ->GetFieldID   (env ->GetObjectClass (dev), "m_ntHandle", "J"));

    RRPClient::CallBack * pCallBack = (RRPClient::CallBack *)
    env ->GetLongField (cbhandle,
    env ->GetFieldID   (env ->GetObjectClass (cbhandle), "m_ntHandle", "J"));

    if (pDevice && pCallBack)
    {
	pDevice ->DisconnectCb (pCallBack);
    }
};

JNIEXPORT void JNICALL Java_com_xprom_hardware_XPDevice_ntSetParam
		      (JNIEnv * env, jobject dev, jobjectArray params)
{
    jclass		cls	      ;
    jobject		p	= NULL;
    jsize		i, n	=    0;
    IRRPDevice::Param	pParams   [32];

    env ->GetObjectArrayElement (params, 0);

    for (i = 0, n = env ->GetArrayLength  (params); i < n; i ++)
    {{
	cls = env ->GetObjectClass  
       (p   = env ->GetObjectArrayElement (params, i));

	pParams [i].uParamId = env ->GetIntField (p,
			       env ->GetFieldID  (cls, "m_uParamId", "I"));

	pParams [i].i32Value = env ->GetIntField (p,
			       env ->GetFieldID  (cls, "m_u32Value", "I"));
    }}

    RRPClient::Device * pDevice = (RRPClient::Device *)
    env ->GetLongField (dev,
    env ->GetFieldID   (env ->GetObjectClass (dev), "m_ntHandle", "J"));

    if (pDevice &&     (pDevice ->m_pIRRPDevice))
    {{
	TThread <GThread> th;

	if (pParams [0].uParamId == 0x5000FFFF)
	{
	    ((IRRPMDIG *) pDevice ->m_pIRRPDevice) ->SetHV ((IRRPMDIG::HVCommand) pParams [0].u32Value);
	    return;
	}
	    ((IRRPMDIG *) pDevice ->m_pIRRPDevice) ->SetParam (n, pParams);
    }}
};
//
//[]------------------------------------------------------------------------[]
//
JNIEXPORT void JNICALL Java_com_xprom_hardware_XPBindingHandle_ntSelectForListen
		      (JNIEnv * env, jobject cbhandle, jboolean select, jintArray paramIds)
{
    RRPClient::CallBack * pCallBack = (RRPClient::CallBack *)
    env ->GetLongField  (cbhandle,
    env ->GetFieldID	(env ->GetObjectClass (cbhandle), "m_ntHandle", "J"));

    if (pCallBack)
    {{
	jsize  sBeg, sNum, sIds;
	jint   pIds [128];

	sIds = env ->GetArrayLength (paramIds);

	for (sBeg = 0; sBeg < sIds;)
	{
	    env ->GetIntArrayRegion (paramIds, sBeg, sNum = ((sizeof (pIds) / sizeof (jint)) < sIds	 ) 
							  ?  (sizeof (pIds) / sizeof (jint)) : sIds, pIds);

	    pCallBack ->SelectForListen (sNum, pIds, select);

	    sBeg += sNum;
	}
    }}
};

JNIEXPORT void JNICALL Java_com_xprom_hardware_XPBindingHandle_ntRequestForGet
		      (JNIEnv * env, jobject cbhandle, jintArray paramIds)
{
    RRPClient::CallBack * pCallBack = (RRPClient::CallBack *)
    env ->GetLongField  (cbhandle,
    env ->GetFieldID	(env ->GetObjectClass (cbhandle), "m_ntHandle", "J"));

    if (pCallBack)
    {
	jsize  sBeg, sNum, sIds;
	jint   pIds [128];

	sIds = env ->GetArrayLength (paramIds);

	for (sBeg = 0; sBeg < sIds;)
	{
	    env ->GetIntArrayRegion (paramIds, sBeg, sNum = ((sizeof (pIds) / sizeof (jint)) < sIds	 ) 
							  ?  (sizeof (pIds) / sizeof (jint)) : sIds, pIds);

	    pCallBack ->RequestForGet (sNum, pIds);

	    sBeg += sNum;
	}
    }
};

JNIEXPORT jobject JNICALL Java_com_xprom_hardware_XPBindingHandle_ntGetQueuedParam
			 (JNIEnv * env, jobject cbhandle, jobject param)
{
    RRPClient::CallBack * pCallBack = (RRPClient::CallBack *)

    env ->GetLongField  (cbhandle,
    env ->GetFieldID	(env ->GetObjectClass (cbhandle), "m_ntHandle", "J"));

    if (pCallBack)
    {{
	RRPClient::Param p;

	if (param && (pCallBack ->GetQueuedParam (& p)))
	{{
	    jclass cls = env ->GetObjectClass (param);

	    env ->SetIntField	(param,
	    env ->GetFieldID	(cls  , "m_uParamId", "I"), p.uParamId);

	    env ->SetIntField	(param,
	    env ->GetFieldID	(cls  , "m_u32Value", "I"), p.u32Value);

	    env ->SetFloatField (param,
	    env ->GetFieldID	(cls  , "m_r32Value", "F"), p.r32Value);

	    return param;
	}}
	else
	{
	    return (pCallBack ->GetQueuedParam (NULL), NULL);
	}
    }}
	    return NULL;
};
//
//[]------------------------------------------------------------------------[]
//
JNIEXPORT jint	    JNICALL Java_com_xprom_hardware_XPDMSDevice_ntGetParam
			   (JNIEnv * env, jobject dev, jint nParamId)
{
    RRPClient::Device * pDevice = (RRPClient::Device *)
    env ->GetLongField (dev,
    env ->GetFieldID   (env ->GetObjectClass (dev), "m_ntHandle", "J"));

    if (pDevice && pDevice -> m_pIRRPDevice)
    {
	return (jint) ((IRRPMDIG *) pDevice ->m_pIRRPDevice) ->GetParam (nParamId);
    }
	return -1;
};

JNIEXPORT jstring    JNICALL Java_com_xprom_hardware_XPDMSDevice_ntGetString
			   (JNIEnv * env, jobject dev, jint nClassId, jint nParamId)
{
    jstring jStr = NULL;
    DWord   dTmp       ;
    char    pBuf  [128];

    RRPClient::Device * pDevice = (RRPClient::Device *)
    env ->GetLongField (dev,
    env ->GetFieldID   (env ->GetObjectClass (dev), "m_ntHandle", "J"));

    IRRPSettings * pIRRPSettings  = NULL;

	* pBuf = '\0';

    if (pDevice && (ERROR_SUCCESS == 
	RRPClient::s_pRRPClient ->m_pIRRPService ->CreateRRPSettings (pIRRPSettings)))
    {
	pIRRPSettings ->GetValue ((IRRPSettings::ParamId) MAKE_SETTING_ID (IRRPSettings::CLSID_MDIG, nParamId), pBuf, sizeof (pBuf), & dTmp);
    }
	jStr = env ->NewStringUTF (pBuf);

	return jStr;
};

JNIEXPORT jint	    JNICALL Java_com_xprom_hardware_XPDMSDevice_ntGrabImage
			   (JNIEnv * env, jobject dev, jint nFrom, jint nNum, jshortArray pBuf)
{
    RRPClient::Device * pDevice = (RRPClient::Device *)
    env ->GetLongField (dev,
    env ->GetFieldID   (env ->GetObjectClass (dev), "m_ntHandle", "J"));

    if (pDevice && pDevice -> m_pIRRPDevice)
    {{
	short *	pAddr = env ->GetShortArrayElements (pBuf, NULL);
	int     nRes  = 0;

	if (pAddr)
	{
		nRes  = ((IRRPMDIG *) pDevice ->m_pIRRPDevice) ->GrabImage (pAddr, -1, nFrom, nNum);

			env ->ReleaseShortArrayElements (pBuf, pAddr, 0);
	}
	return  (jint)  nRes;
    }}
	return 0;
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
	RRPClient * RRPClient::s_pRRPClient = NULL;    // (G*)

JNIEXPORT jobject JNICALL Java_com_xprom_hardware_XPDeviceService_ntCreateInstance
			 (JNIEnv * env, jclass cls)
{
	RRPClient *  pRRPClient = NULL;

    JNI_LockAcquire ();

	if (NULL != (pRRPClient = RRPClient::s_pRRPClient))
	{
	    pRRPClient ->AddRef ();
	}
	else
	{{
	    void * pArgs [1]   = {cls};

	    CreateThread <GThread> (NULL, RRPClient::Main,
					  sizeof (pArgs ) /
					  sizeof (void *) , pArgs);
	    {
		     pRRPClient = RRPClient::s_pRRPClient;
	    }
	}}

    JNI_LockRelease ();

	if (pRRPClient)
	{
	    return  pRRPClient ->m_jobject;
	}
	    return  NULL;
};

JNIEXPORT void JNICALL Java_com_xprom_hardware_XPDeviceService_ntClose
		      (JNIEnv * env, jobject srv)
{
    RRPClient *	pRRPClient = (RRPClient *)

    env ->GetLongField (srv,
    env ->GetFieldID   (env ->GetObjectClass (srv), "m_ntHandle", "J"));

    if (pRRPClient)
    {
	pRRPClient ->Release ();
    }
};

JNIEXPORT jobject JNICALL Java_com_xprom_hardware_XPDeviceService_ntCreateXPDevice
			 (JNIEnv * env, jobject srv, jint devId)
{
    RRPClient *	pRRPClient = (RRPClient *)

    env ->GetLongField (srv,
    env ->GetFieldID   (env ->GetObjectClass (srv), "m_ntHandle", "J"));

    if (pRRPClient)
    {
	return pRRPClient ->CreateXPDevice (devId);
    }
	return NULL;
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
static void JNI_LockAcquire ()
{
    ::WaitForSingleObject (s_hJNI_Lock, INFINITE);
};

static void JNI_LockRelease ()
{
    ::ReleaseMutex	  (s_hJNI_Lock);
};

static JNIEnv * GetJNIEnv ()
{
    JNIEnv * env = NULL;

    s_jvm ->GetEnv ((void **) & env, JNI_VERSION);

    return   env;
};
//
//[]------------------------------------------------------------------------[]
//
#ifdef _WIN32

JNIEXPORT jint JNICALL JNI_OnLoad(JavaVM * jvm, void *reserved)
{
    JNIEnv *	 env = NULL;

    if (JNI_OK == jvm ->GetEnv ((void **) & env, JNI_VERSION))
    {
    if (NULL != (s_hJNI_Lock = ::CreateMutex (NULL, False, NULL)))
    {
	s_jvm   = jvm;

	return JNI_VERSION_1_2;
    }
    }
	return JNI_ERR;
};

JNIEXPORT void JNICALL JNI_OnUnload(JavaVM * jvm, void *reserved)
{
    if (s_jvm)
    {
    if (NULL != (s_hJNI_Lock))
    {
	::CloseHandle (s_hJNI_Lock);

		       s_hJNI_Lock = NULL;
    }
	s_jvm   = NULL;
    }
};

BOOL APIENTRY DllMain (HINSTANCE hInstance, DWORD dReason, LPVOID)
{
    switch (dReason)
    {
    case DLL_PROCESS_ATTACH :

	if (! GWinInit (hInstance))
	{
	    return FALSE;
	}
	break;

    case DLL_PROCESS_DETACH :

	      GWinDone ();
	break;
    };
	return TRUE;
};

#endif
//
//[]------------------------------------------------------------------------[]
