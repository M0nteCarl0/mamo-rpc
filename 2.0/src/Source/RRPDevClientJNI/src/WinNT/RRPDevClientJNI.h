//[]------------------------------------------------------------------------[]
// RRPDevClientJNI Java interface declaration
//
//
// Created by GSh.
//[]------------------------------------------------------------------------[]

#ifndef __RRPDEVCLIENTJNI_H
#define	__RRPDEVCLIENTJNI_H

#define	JNI_VERSION	    JNI_VERSION_1_2

#ifdef _WIN32

#include "g_winbase.h"

#endif

#include <jni.h>

//[]------------------------------------------------------------------------[]
//
JNIEXPORT jobject   JNICALL Java_com_xprom_hardware_XPDeviceService_ntCreateInstance
			   (JNIEnv *, jclass);

JNIEXPORT jobject   JNICALL Java_com_xprom_hardware_XPDeviceService_ntCreateXPDevice
			   (JNIEnv *, jobject, jint);

JNIEXPORT void	    JNICALL Java_com_xprom_hardware_XPDeviceService_ntClose
			   (JNIEnv *, jobject);
//
//[]------------------------------------------------------------------------[]
//
JNIEXPORT void	    JNICALL Java_com_xprom_hardware_XPDevice_ntClose
			   (JNIEnv *, jobject);

JNIEXPORT jobject   JNICALL Java_com_xprom_hardware_XPDevice_ntConnect
			   (JNIEnv *, jobject, jobject);

JNIEXPORT void	    JNICALL Java_com_xprom_hardware_XPDevice_ntDisconnect
			   (JNIEnv *, jobject, jobject);

JNIEXPORT void	    JNICALL Java_com_xprom_hardware_XPDevice_ntSetParam
			   (JNIEnv *, jobject, jobjectArray);
//
//[]------------------------------------------------------------------------[]
//
JNIEXPORT void	    JNICALL Java_com_xprom_hardware_XPBindingHandle_ntSelectForListen
			   (JNIEnv *, jobject, jboolean, jintArray);

JNIEXPORT void	    JNICALL Java_com_xprom_hardware_XPBindingHandle_ntRequestForGet
			   (JNIEnv *, jobject, jintArray);

JNIEXPORT jobject   JNICALL Java_com_xprom_hardware_XPBindingHandle_ntGetQueuedParam
			   (JNIEnv *, jobject, jobject);
//
//[]------------------------------------------------------------------------[]
//
JNIEXPORT jint	    JNICALL Java_com_xprom_hardware_XPDMSDevice_ntGetParam
			   (JNIEnv *, jobject, jint);

JNIEXPORT jstring   JNICALL Java_com_xprom_hardware_XPDMSDevice_ntGetString
			   (JNIEnv *, jobject, jint, jint);

JNIEXPORT jint	    JNICALL Java_com_xprom_hardware_XPDMSDevice_ntGrabImage
			   (JNIEnv *, jobject, jint, jint, jshortArray);
//
//[]------------------------------------------------------------------------[]

#endif//__RRPDEVCLIENTJNI_H