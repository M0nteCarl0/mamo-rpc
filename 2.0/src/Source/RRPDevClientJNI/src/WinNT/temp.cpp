#if FALSE

class RRPObject
{

public :

	typedef TSLink <RRPObject, sizeof (void *)>   Link ;
	typedef TXList <RRPObject,
		TSLink <RRPObject, sizeof (void *)> > List ;

		 RRPObject (jobject o) : m_cRefCount (1), m_jobject (o) {};

	jobject	    GetJObject		() const { return m_jobject;};

	void	    AddRef		() {m_cRefCount ++;};

virtual	LONG	    Release		() = NULL;

protected :

	Link			m_ObjLink   ;

	TInterlocked <LONG>	m_cRefCount ;
	jobject 		m_jobject   ;
};
//
//[]------------------------------------------------------------------------[]
//
class RRPClient //: public RRPObject
{
public :

	void *	operator new	(size_t, void * ptr)
				{return ptr;};
	void	operator delete (void *)	 {};
	void	operator delete (void *, void *) {};

class Object
{
public :
	typedef TSLink <Object, sizeof (void *)>   Link ;
	typedef TXList <Object,
		TSLink <Object, sizeof (void *)> > List ;

virtual		   ~Object ();

	Link			m_Link	    ;
};

class Device : public RRPObject
{
public :

		Device (RRPClient * pClient, jobject dev, jint devId) :

		RRPObject (dev)
		{
		    (m_pClient = pClient) ->AddRef ();

		     m_uDevId  = devId;
		};


static	void	    Destroy		(Device *);

	LONG	    Release		();

public :

	RRPClient *		m_pClient   ;	

	jint			m_uDevId    ;
};

class CallBack : public RRPObject
{
public :
};
	jobject	    CreateXPDevice	(jobject, jint);

public :

    	Bool	    LockAcquire		() { return m_cLock.LockAcquire ();};

	void	    LockRelease		() {        m_cLock.LockRelease ();};

	Device *    GetDeviceByJObject	(JNIEnv *, jobject);

static	void	    Destroy		(RRPClient *);

	LONG	    Release		();

public :

	GCriticalSection	m_cLock	    ; // Lock for (*)

//	List			m_cDevList  ; // (*)
};

void RRPClient::Destroy (RRPClient * pRRPClient)
{
    delete pRRPClient;
};
//
//[]------------------------------------------------------------------------[]
//

//
//[]------------------------------------------------------------------------[]
//
void RRPClient::Device::Destroy (RRPClient::Device * pDevice)
{
    delete pDevice;
};

LONG RRPClient::Device::Release ()
{
    m_pClient ->LockAcquire ();

	if (0 == -- m_cRefCount)
	{
	    m_pClient ->m_cDevList.extract (this);

    m_pClient ->LockRelease ();

	    Destroy (this);

	    return 0;
	}

    m_pClient ->LockRelease ();

	return m_cRefCount;
};
//
//[]------------------------------------------------------------------------[]
//
RRPClient::Device * RRPClient::GetDeviceByJObject (JNIEnv * env, jobject dev)
{
    TXList_Iterator <RRPObject> I;

    LockAcquire ();

	for (I = m_cDevList.head (); I.next ();)
	{
	    if (env ->IsSameObject (I.next () ->GetJObject (), dev))
	    {
    LockRelease ();

		return (Device *) I.next ();
	    }
	    I <<= m_cDevList.getnext (I.next ());
	}

    LockRelease ();

		return NULL;
};

jobject RRPClient::CreateXPDevice (jobject dev, jint devId)
{
    TXList_Iterator <RRPObject> I;

    Device *	pDevice = NULL;

    LockAcquire ();

	for (I = m_cDevList.head (); I.next ();)
	{
	    if (((Device *) I.next ()) ->m_uDevId == devId)
	    {
		pDevice = (Device *) I.next ();

		pDevice ->AddRef ();

		break;
	    }
	    I <<= m_cDevList.getnext (I.next ());
	}

	if (NULL ==  pDevice)
	{
	if (NULL != (pDevice = new Device (this, dev, devId)))
	{
	    m_cDevList.append (pDevice);
	}
	}

    LockRelease ();

	return (pDevice) ? dev : NULL;
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
LONG RRPClient::Release ()
{
    JNI_LockAcquire ();

	if (0 == -- m_cRefCount)
	{
	    s_pRRPClient = NULL;

    JNI_LockRelease ();

	    Destroy (this); 

	    return 0;
	}

    JNI_LockRelease ();

	    return m_cRefCount;
};
//
//[]------------------------------------------------------------------------[]

    static void *   b_RRPClient [roundup (sizeof (RRPClient), sizeof (void *)) /
							      sizeof (void *)] = {NULL};


#endif
