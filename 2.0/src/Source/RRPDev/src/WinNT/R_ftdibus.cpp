//[]------------------------------------------------------------------------[]
// RRP Ftdi devices definitions
//
//
// Created by GSh.
//[]------------------------------------------------------------------------[]

#include "r_ftdibus.h"

//[]------------------------------------------------------------------------[]
//
#define USE_FGXOUT  False

#if (USE_FGXOUT)

void GetFgxOutFileName (TCHAR *);

static HANDLE s_hFgxOutFile = NULL;

static void FgxOut_Create ()
{
    TCHAR pFileName [MAX_PATH];
    GetFgxOutFileName (pFileName);

    if (INVALID_HANDLE_VALUE == 
       (s_hFgxOutFile         = ::CreateFile 
			        (pFileName	      ,
				 GENERIC_WRITE	      ,
				 FILE_SHARE_READ      ,
				 NULL		      ,
				 CREATE_ALWAYS	      ,
				 0
// 			       | FILE_FLAG_OVERLAPPED
			       | FILE_ATTRIBUTE_NORMAL,
				 NULL		      )))
    {
	s_hFgxOutFile = NULL;

	return;
    }
printf ("...Create FgxOut {%s}\n", pFileName);
};

static void FgxOut_Close ()
{
    if (s_hFgxOutFile)
    {
	s_hFgxOutFile = (::CloseHandle (s_hFgxOutFile), NULL);

printf ("...Close  FgxOut\n");
    }
};

static void FgxOut_Reset ()
{
    if (s_hFgxOutFile)
    {
printf ("...Reset  FgxOut\n");

	::SetFilePointer (s_hFgxOutFile, 0, NULL, FILE_BEGIN);

	::SetEndOfFile	 (s_hFgxOutFile);
    }
};

static void FgxOut_Flush ()
{
    if (s_hFgxOutFile)
    {
printf ("...Flush  FgxOut\n");

	::FlushFileBuffers (s_hFgxOutFile);
    }
};

static DWord FgxOut_Write (const void * pBuf, DWord dBufSize)
{
    DWord  dTransfered = 0;

    if (s_hFgxOutFile)
    {
	::WriteFile (s_hFgxOutFile, pBuf, dBufSize, & dTransfered, NULL);
    }
    return dTransfered;
};

#else
static void  FgxOut_Create  () {};
static void  FgxOut_Close   () {};
static void  FgxOut_Reset   () {};
static void  FgxOut_Flush   () {};
static DWord FgxOut_Write   (const void * pBuf, DWord dBufSize) { return 0; };
#endif
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
#define	COMM_READ_INTERVAL_TIMEOUT	-1
#define COMM_READ_MULTIPLY_TIMEOUT	-1
#define	COMM_READ_CONSTANT_TIMEOUT	20

GFtdiFile::GFtdiFile ()
{
    m_hFile	= NULL;

    m_hXferIoCo = ::CreateEvent (NULL, True, False, NULL);
    m_wXfer	= NULL;

    CreateWakeUp (m_wXfer);

    m_fCloseLock.Open ();
};

GFtdiFile::~GFtdiFile ()
{
    if (m_wXfer)
    {
	m_wXfer ->Release ();
	m_wXfer = NULL;
    }

    if (m_hXferIoCo)
    {
	::CloseHandle (m_hXferIoCo);
		       m_hXferIoCo = NULL;
    }
};

void GFtdiFile::Close ()
{
    if (m_hFile)
    {
	::FT_W32_CloseHandle (m_hFile);
			      m_hFile = NULL;
    }
};

GResult GFtdiFile::Create (const char * pDescriptionOrId, Bool bDescription)
{
    if (INVALID_HANDLE_VALUE == 
       (m_hFile		      = ::FT_W32_CreateFile 
		      ((LPCTSTR) pDescriptionOrId	,
	    			 GENERIC_READ 
			       | GENERIC_WRITE		,
				 0			,
				 NULL			,
				 OPEN_EXISTING		,
				 FILE_ATTRIBUTE_NORMAL
			       | FILE_FLAG_OVERLAPPED 
			       |
	       ((bDescription) ? FT_OPEN_BY_DESCRIPTION
			       : FT_OPEN_BY_LOCATION   ),
				 NULL		       )))
    {
	return ERROR_FILE_NOT_FOUND;
    }

	FTDCB	  dcb; 

	memset (& dcb, 0,   sizeof (dcb))   ;
	dcb.DCBlength	  = sizeof (dcb)    ;

	dcb.BaudRate	  = FT_BAUD_115200  ;
	dcb.ByteSize	  = FT_BITS_8	    ;
	dcb.Parity	  = FT_PARITY_ODD   ;
	dcb.StopBits	  = FT_STOP_BITS_2  ;

    if (dcb.Parity != FT_PARITY_NONE)
    {
	dcb.fParity       = True ;
    }
//	dcb.fAbortOnError = True ;
//
    ::FT_W32_SetCommState    (m_hFile,  & dcb);

//  ::FT_W32_SetCommMask     (m_hFile, EV_ERR);
//
    FTTIMEOUTS t = {(DWORD) COMM_READ_INTERVAL_TIMEOUT,
		    (DWORD) COMM_READ_MULTIPLY_TIMEOUT,
		    (DWORD) COMM_READ_CONSTANT_TIMEOUT, 0, 0};

    ::FT_W32_SetCommTimeouts (m_hFile,   & t);

    ::FT_SetLatencyTimer     (m_hFile,	   8);

//	m_fCloseLock.Open ();

	return ERROR_SUCCESS;
};
//
//[]------------------------------------------------------------------------[]
//
static Bool GAPI FT_W32_GrabOverlappedResult (FT_HANDLE hFile, GOverlapped & o)
{
    DWord   dResult	= ERROR_IO_INCOMPLETE;
    DWord   dResultInfo = 0;

    if (ERROR_IO_INCOMPLETE == 
       (dResult		     = (::FT_W32_GetOverlappedResult
			       (hFile, & o, & dResultInfo   , False))

			      ? ERROR_SUCCESS 
			      : ::FT_W32_GetLastError	     (hFile)))
    {
        return False;
    }
        o.SetIoCoResult (dResult, (void *) dResultInfo);

        return True ;
};
//
//[]------------------------------------------------------------------------[]
//
void GFtdiFile::SetIoCoResult (GIrp * pIrp)
{
    if (! m_oXfer.GrabIoCoResult ())
    {
	if (m_fCloseLock.LockAcquire ())
	{
	    ::FT_W32_GrabOverlappedResult (m_hFile, m_oXfer);

	    m_fCloseLock.LockRelease ();
	}
	else
	{
	    pIrp ->SetCurResultInfo (ERROR_INVALID_HANDLE, NULL);

	    return;
	}
    }
	    pIrp ->SetCurResultInfo (m_oXfer.GetIoCoResult     (),
				     m_oXfer.GetIoCoResultInfo ());
};

Bool GFtdiFile::XxxxIoCo_Send (IWakeUp *, IWakeUp::Reason, void * pThis, void * pIrp)
{
    ((GFtdiFile *) pThis) ->SetIoCoResult ((GIrp *) pIrp);

    QueueIrpForComplete ((GIrp *) pIrp);

    return False;
};

void GFtdiFile::QueueForSend (GIrp * pIrp, void * pSendBuf, DWord dSendBufLen)
{
    DWord		    dResult	= 0;
    DWord		    dResultInfo = 0;
    GOverlapped::IoStatus   oStatus	   ;

    if (m_fCloseLock.LockAcquire ())
    {
	m_oXfer.StartIo (0, 0, m_hXferIoCo);

	dResult = ::FT_W32_WriteFile (m_hFile, pSendBuf   	  ,
					       dSendBufLen	  ,
					     & dResultInfo	  ,
					     & m_oXfer.SetIoCoProc
					      (NULL, NULL, NULL))

		 ? ERROR_SUCCESS : ::FT_W32_GetLastError (m_hFile);

	oStatus = m_oXfer.GetIoCoStatus (dResult, (void *) dResultInfo);

	m_fCloseLock.LockRelease ();

	if (GOverlapped::IoPending == oStatus)
	{
	    m_wXfer ->QueueWaitFor (m_hXferIoCo, XxxxIoCo_Send, this, pIrp);
	}
	else
	{
	    XxxxIoCo_Send (m_wXfer, IWakeUp::OnOccured, this, pIrp);
	}

	return;
    }
	pIrp ->SetCurResultInfo (ERROR_INVALID_HANDLE);

	QueueIrpForComplete (pIrp);
};
//
//[]------------------------------------------------------------------------[]
//
Bool GFtdiFile::XxxxIoCo_Recv (IWakeUp *, IWakeUp::Reason, void * pThis, void * pIrp)
{
    ((GFtdiFile *) pThis) ->SetIoCoResult ((GIrp *) pIrp);

    QueueIrpForComplete ((GIrp *) pIrp);

    return False;
};

void GFtdiFile::QueueForRecv (GIrp * pIrp, void * pRecvBuf, DWord dRecvBufSize)
{
    DWord		    dResult	= 0;
    DWord		    dResultInfo = 0;
    GOverlapped::IoStatus   oStatus	   ;

    if (m_fCloseLock.LockAcquire ())
    {
	m_oXfer.StartIo (0, 0, m_hXferIoCo);

	dResult = ::FT_W32_ReadFile  (m_hFile, pRecvBuf		  ,
					       dRecvBufSize	  ,
					     & dResultInfo	  ,
					     & m_oXfer.SetIoCoProc
					      (NULL, NULL, NULL))

		 ? ERROR_SUCCESS : ::FT_W32_GetLastError (m_hFile);

	oStatus = m_oXfer.GetIoCoStatus (dResult, (void *) dResultInfo);

	m_fCloseLock.LockRelease ();

	if (GOverlapped::IoPending == oStatus)
	{
	    m_wXfer ->QueueWaitFor (m_hXferIoCo, XxxxIoCo_Recv, this, pIrp);
	}
	else
	{
	    XxxxIoCo_Recv (m_wXfer, IWakeUp::OnOccured, this, pIrp);
	}

	return;
    }   
	pIrp ->SetCurResultInfo (ERROR_INVALID_HANDLE);

	QueueIrpForComplete (pIrp);
};
//
//[]------------------------------------------------------------------------[]

#include <stdio.h>
#include <setupapi.h>

//[]------------------------------------------------------------------------[]
//
class GSysDeviceInfoSet
{
public :
		GSysDeviceInfoSet ()
		{
		    m_hDevInfo	   = NULL;
		    m_iDevInfo	   =   -1;
		};

	       ~GSysDeviceInfoSet ()
		{
		    Close ();
	    	};

	GResult		Create		(const GUID * pIfaceOrClassId       ,
					       Bool   bForPresend   = False ,
					       Bool   bForInterface = True  );
	void		Close		();

	Bool		SelectDevInfo   (int,  Bool   bParentDevId  = False );

	Bool		SelectInterface (REFIID uInterfaceId );

							// Next function may be called if
							// SelectDevInfo && SelectInterface
							//
	Bool		IsDevPresent	() const 
	
	{ return    (m_sDevIface.Flags & SPINT_ACTIVE) ? True : False; };

	const TCHAR *	GetInstancePath	() const { return m_sDevIfaceDet.DevicePath; };

protected :

	HDEVINFO		    m_hDevInfo     ;
	int			    m_iDevInfo     ;

	SP_DEVINFO_DATA		    m_sDevInfo     ;
	SP_DEVICE_INTERFACE_DATA    m_sDevIface    ;

	union
	{
	SP_INTERFACE_DEVICE_DETAIL_DATA	    
			m_sDevIfaceDet		   ;  
	TCHAR		m_pReserved [MAX_PATH * 4] ;
	};
};
//
//[]------------------------------------------------------------------------[]
//
GResult GSysDeviceInfoSet::Create (const GUID * pIfaceOrClassId ,
					 Bool	bForPresent	,
					 Bool	bForInterface	)
{
    GResult dResult;

    if (ERROR_SUCCESS	     == (  dResult  = 
    
       (INVALID_HANDLE_VALUE != (m_hDevInfo = 

       ::SetupDiGetClassDevsA   (pIfaceOrClassId, NULL, NULL,

	      ((bForPresent  ) ? DIGCF_PRESENT	         : 0) |
	      ((bForInterface) ? DIGCF_INTERFACEDEVICE	 : 0))))
	      
      ? ERROR_SUCCESS : (m_hDevInfo = NULL, ::GetLastError ())))
    {
    }

    return dResult;
};

void GSysDeviceInfoSet::Close ()
{
    if (m_hDevInfo)
    {
	m_hDevInfo = (::SetupDiDestroyDeviceInfoList (m_hDevInfo), NULL);
    }
};

Bool GSysDeviceInfoSet::SelectDevInfo (int iDevInfo, Bool bParentDevId)
{
    m_sDevInfo.cbSize = sizeof (m_sDevInfo);

    if (0 <= (m_iDevInfo = (::SetupDiEnumDeviceInfo 

    	     (m_hDevInfo, iDevInfo, & m_sDevInfo)) ? iDevInfo : -1))
    {
		return True ;
    }
		return False;
};

Bool GSysDeviceInfoSet::SelectInterface (REFIID uInterfaceId)
{
    m_sDevIface.cbSize = sizeof (m_sDevIface);

    if (::SetupDiEnumDeviceInterfaces	  (m_hDevInfo	  ,
					   NULL 	  ,
					 & uInterfaceId	  ,
					   m_iDevInfo	  ,
					 & m_sDevIface	 ))
    {
	m_sDevIfaceDet.cbSize = sizeof (m_sDevIfaceDet);

    if (::SetupDiGetInterfaceDeviceDetail (m_hDevInfo	  ,
					 & m_sDevIface    ,
					 & m_sDevIfaceDet ,
				sizeof (   m_pReserved	 ),
					   NULL		  ,
					   NULL	         ))
    {
	return True;
    }
    }
	return False;
};
//
//[]------------------------------------------------------------------------[]











//[]------------------------------------------------------------------------[]
//
    static const Word s_crctbl [] = {

    0x0000, 0x8005, 0x800F, 0x000A, 0x801B, 0x001E, 0x0014, 0x8011,
    0x8033, 0x0036, 0x003C, 0x8039, 0x0028, 0x802D, 0x8027, 0x0022,
    0x8063, 0x0066, 0x006C, 0x8069, 0x0078, 0x807D, 0x8077, 0x0072,
    0x0050, 0x8055, 0x805F, 0x005A, 0x804B, 0x004E, 0x0044, 0x8041,
    0x80C3, 0x00C6, 0x00CC, 0x80C9, 0x00D8, 0x80DD, 0x80D7, 0x00D2,
    0x00F0, 0x80F5, 0x80FF, 0x00FA, 0x80EB, 0x00EE, 0x00E4, 0x80E1,
    0x00A0, 0x80A5, 0x80AF, 0x00AA, 0x80BB, 0x00BE, 0x00B4, 0x80B1,
    0x8093, 0x0096, 0x009C, 0x8099, 0x0088, 0x808D, 0x8087, 0x0082,
    0x8183, 0x0186, 0x018C, 0x8189, 0x0198, 0x819D, 0x8197, 0x0192,
    0x01B0, 0x81B5, 0x81BF, 0x01BA, 0x81AB, 0x01AE, 0x01A4, 0x81A1,
    0x01E0, 0x81E5, 0x81EF, 0x01EA, 0x81FB, 0x01FE, 0x01F4, 0x81F1,
    0x81D3, 0x01D6, 0x01DC, 0x81D9, 0x01C8, 0x81CD, 0x81C7, 0x01C2,
    0x0140, 0x8145, 0x814F, 0x014A, 0x815B, 0x015E, 0x0154, 0x8151,
    0x8173, 0x0176, 0x017C, 0x8179, 0x0168, 0x816D, 0x8167, 0x0162,
    0x8123, 0x0126, 0x012C, 0x8129, 0x0138, 0x813D, 0x8137, 0x0132,
    0x0110, 0x8115, 0x811F, 0x011A, 0x810B, 0x010E, 0x0104, 0x8101,
    0x8303, 0x0306, 0x030C, 0x8309, 0x0318, 0x831D, 0x8317, 0x0312,
    0x0330, 0x8335, 0x833F, 0x033A, 0x832B, 0x032E, 0x0324, 0x8321,
    0x0360, 0x8365, 0x836F, 0x036A, 0x837B, 0x037E, 0x0374, 0x8371,
    0x8353, 0x0356, 0x035C, 0x8359, 0x0348, 0x834D, 0x8347, 0x0342,
    0x03C0, 0x83C5, 0x83CF, 0x03CA, 0x83DB, 0x03DE, 0x03D4, 0x83D1,
    0x83F3, 0x03F6, 0x03FC, 0x83F9, 0x03E8, 0x83ED, 0x83E7, 0x03E2,
    0x83A3, 0x03A6, 0x03AC, 0x83A9, 0x03B8, 0x83BD, 0x83B7, 0x03B2,
    0x0390, 0x8395, 0x839F, 0x039A, 0x838B, 0x038E, 0x0384, 0x8381,
    0x0280, 0x8285, 0x828F, 0x028A, 0x829B, 0x029E, 0x0294, 0x8291,
    0x82B3, 0x02B6, 0x02BC, 0x82B9, 0x02A8, 0x82AD, 0x82A7, 0x02A2,
    0x82E3, 0x02E6, 0x02EC, 0x82E9, 0x02F8, 0x82FD, 0x82F7, 0x02F2,
    0x02D0, 0x82D5, 0x82DF, 0x02DA, 0x82CB, 0x02CE, 0x02C4, 0x82C1,
    0x8243, 0x0246, 0x024C, 0x8249, 0x0258, 0x825D, 0x8257, 0x0252,
    0x0270, 0x8275, 0x827F, 0x027A, 0x826B, 0x026E, 0x0264, 0x8261,
    0x0220, 0x8225, 0x822F, 0x022A, 0x823B, 0x023E, 0x0234, 0x8231,
    0x8213, 0x0216, 0x021C, 0x8219, 0x0208, 0x820D, 0x8207, 0x0202};

static Word WINAPI getCRC16_8005 (Word crc, const Byte * pBufHead,
					    const Byte * pBufTail,
					    size_t	 tBufInc )
{
    for (; pBufHead < pBufTail; pBufHead += tBufInc)
    {
	crc = (crc * 256) ^ s_crctbl [(crc / 256) ^ (* pBufHead)];
    }
    return crc;
};

/*
static DWord WINAPI getCRC16_8005_x3 (Word, const Byte * pBufHead,
					    const Byte * pBufTail,
					    size_t	 tBufInc )
{
    Word crc [3] = {0, 0, 0};

    for (; pBufHead < pBufTail;)
    {
	crc [0] = (crc [0] * 256) ^ s_crctbl [(crc [0] / 256) ^ (* pBufHead)]; pBufHead += tBufInc;
	crc [1] = (crc [1] * 256) ^ s_crctbl [(crc [1] / 256) ^ (* pBufHead)]; pBufHead += tBufInc;
	crc [2] = (crc [2] * 256) ^ s_crctbl [(crc [2] / 256) ^ (* pBufHead)]; pBufHead += tBufInc;
    }

    return (DWord) crc [0];

    return (DWord) crc [0] + crc [1] + crc [2];
};
*/
//
//[]------------------------------------------------------------------------[]
//
    struct SRowHeader
    {
	Word	nLength	  ;
	Word	nVersion  ;
	Word	nFrame    ;
	Word	nRow      ;
	Word	nRowSize  ;
	Word	uDataCRC2 ;
	Word	uDataCRC3 ;
	Word	uDataCRC1 ;

	DWord	dCRCError ;
    };

static const Word * GetRowHeader (SRowHeader & rhdr, const Word * pHead, const Word * pTail)
{
//  static const Word CRCData   [] = { 0x01c1, 0x01c2, 0x01c3,
//				       0x02c1, 0x02c2, 0x02c3 };
//
//  static const Word CRCHeader [] = { 0x0300, 0x0300, 0x0300,
//				       0x0418, 0x0419, 0x0417,	Channel_2, Channel_3, Channel_1
//				       0x0545, 0x0545, 0x0545,
//				       0x0691, 0x0691, 0x0691 };

    int c; const Word * p = pHead; pTail -= (3 * 6);

    for (; p < pTail; )
    {
	if (((* (p + (3 * 2) + 0) != 0x0300) ||
	     (* (p + (3 * 3) + 0) != 0x0418) ||
	     (* (p + (3 * 4) + 0) != 0x0545) ||
	     (* (p + (3 * 5) + 0) != 0x0691) )

	   )
/*
	&&  ((* (p + (3 * 2) + 1) != 0x0300) ||
	     (* (p + (3 * 3) + 1) != 0x0417) ||
	     (* (p + (3 * 4) + 1) != 0x0545) ||
	     (* (p + (3 * 5) + 1) != 0x0691) )
	
	&&  ((* (p + (3 * 2) + 2) != 0x0300) ||
	     (* (p + (3 * 3) + 2) != 0x0419) ||
	     (* (p + (3 * 4) + 2) != 0x0545) ||
	     (* (p + (3 * 5) + 2) != 0x0691) ))
*/
	{ p ++  ; continue; }

	if ((p + (3 * 16)) > (pTail + (3 * 6)))
	{	  break	  ; }

	if (0x70 == (0x70 & 
	
	(rhdr.dCRCError = ((((((Word) LOBYTE (*(p + (3 * 13) + 2))) << 8)
			   | (((Word) LOBYTE (*(p + (3 * 14) + 2))) << 0))
		
	 == getCRC16_8005 (0,(Byte *)(p + (3 *  2) + 2),
			     (Byte *)(p + (3 * 13) + 2), 6) && (*(p + (3 * 3) + 2) == 0x0419)) ? (c = 2, 0x00) : 0x10)

	|		  ((((((Word) LOBYTE (*(p + (3 * 13) + 1))) << 8)
			   | (((Word) LOBYTE (*(p + (3 * 14) + 1))) << 0))
	 == getCRC16_8005 (0,(Byte *)(p + (3 *  2) + 1),
			     (Byte *)(p + (3 * 13) + 1), 6) && (*(p + (3 * 3) + 1) == 0x0417)) ? (c = 1, 0x00) : 0x20)

	|		  ((((((Word) LOBYTE (*(p + (3 * 13) + 0))) << 8)
			   | (((Word) LOBYTE (*(p + (3 * 14) + 0))) << 0))
	 == getCRC16_8005 (0,(Byte *)(p + (3 *  2) + 0),
		 	     (Byte *)(p + (3 * 13) + 0), 6) && (*(p + (3 * 3) + 0) == 0x0418)) ? (c = 0, 0x00) : 0x40))))

	{ p += (3 * 6); continue; } 

	//
        // RowHeader encoded, grab it
	//
	rhdr.nLength   = 3 * 16 * 2;
	rhdr.nVersion  =	   LOBYTE (*(p + (3 *  7) + c));
	rhdr.nFrame    =	   LOBYTE (*(p + (3 *  8) + c));

	rhdr.nRow      = ((((Word) LOBYTE (*(p + (3 * 11) + c))) << 8) 
		       |  (((Word) LOBYTE (*(p + (3 * 12) + c))) << 0));

	rhdr.nRowSize  = ((((Word) LOBYTE (*(p + (3 *  9) + c))) << 8)
		       |  (((Word) LOBYTE (*(p + (3 * 10) + c))) << 0)) * 3;

	rhdr.uDataCRC2 = ((((Word) LOBYTE (*(p + (3 *  0) + 0))) << 8) 
		       |  (((Word) LOBYTE (*(p + (3 *  1) + 0))) << 0));

	rhdr.uDataCRC1 = ((((Word) LOBYTE (*(p + (3 *  0) + 1))) << 8) 
		       |  (((Word) LOBYTE (*(p + (3 *  1) + 1))) << 0));

	rhdr.uDataCRC3 = ((((Word) LOBYTE (*(p + (3 *  0) + 2))) << 8) 
		       |  (((Word) LOBYTE (*(p + (3 *  1) + 2))) << 0));

//	rhdr.dCRCError |= (10 < (rhdr.nRow % 20) ? 0x00 : 0x40); // Debug Head error generation !!!
//	rhdr.uDataCRC2 += (10 < (rhdr.nRow % 20) ?    1 :    0); // Debug data error generation !!!

	return p;
    };

    return NULL;
};

static DWord WINAPI CheckDataCRC16_8005 (SRowHeader & rhdr, const Byte * pBufHead,
							    const Byte * pBufTail)
{
    Word CRC2 = 0;
    Word CRC1 = 0;
    Word CRC3 = 0;

    for (; pBufHead < pBufTail; pBufHead += 6)
    {
	CRC2 = (CRC2 * 256) ^ s_crctbl [(CRC2 / 256) ^ (* (pBufHead + 0))];
	CRC1 = (CRC1 * 256) ^ s_crctbl [(CRC1 / 256) ^ (* (pBufHead + 2))];
	CRC3 = (CRC3 * 256) ^ s_crctbl [(CRC3 / 256) ^ (* (pBufHead + 4))];
    }

    const Word * p = (const Word *) pBufTail;

    return

    rhdr.dCRCError |= (((rhdr.dCRCError & 0x40) ? 0x00 : ((CRC2 == rhdr.uDataCRC2) ? 0x00 : 0x04))
		   |   ((rhdr.dCRCError & 0x20) ? 0x00 : ((CRC1 == rhdr.uDataCRC1) ? 0x00 : 0x02))
		   |   ((rhdr.dCRCError & 0x10) ? 0x00 : ((CRC3 == rhdr.uDataCRC3) ? 0x00 : 0x01)));
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
#define	XP_RECV_SIZE	    (( 4 * (   4 * 1024)))
#define XP_RECV_SIZE_1_3    (((XP_RECV_SIZE / 3) / 16) * 16)

#define	XP_BUFFER_SIZE	    ((64 * (1024 * 1024)) - XP_RECV_SIZE)

#define W_RAW_LENGTH	    (1537 * 3	 )
#define	B_RAW_LENGTH	    (1537 * 3 * 2)


    static const _GUID CLSID_SysFgxIface = 

   {0x29542307,0x43BE,0x4dcb,0xA0,0x06,0x98,0x9E,0x98,0x40,0xA7,0x0D};

Bool XPGrabber::s_bCRCMode = False;

XPGrabber::XPGrabber (void (GAPI * pCBProc)(void *, DWord, DWord), void * pCBProcContext)
{
    m_pCBProc		= pCBProc	 ;
    m_pCBProcContext	= pCBProcContext ;

    m_pIoIrp		= NULL ;
    m_bOpened		= False;
    m_cContinueDelay	=     0;
    ::memset 
 (& m_oRecvIdle, 0, sizeof (m_oRecvIdle));
						    // Event must be Sync Event
						    // manual reset !!!
    m_oRecvIdle.hEvent  = ::CreateEvent (NULL, False, False, NULL);

    m_wRecvIdle		= NULL;
    CreateWakeUp (m_wRecvIdle);
    m_pRecvIdleCytx	= NULL;

    if ((m_pImageSof = malloc ((2 * XP_RECV_SIZE) + XP_BUFFER_SIZE)) != NULL)
    {
	 m_sRecvBuf.SetBuf ((Byte *) m_pImageSof + (1 * XP_RECV_SIZE), XP_RECV_SIZE);
	 m_pImageBuf	=   (Byte *) m_pImageSof + (2 * XP_RECV_SIZE);
    }
    else
    {
	 m_sRecvBuf.SetBuf (NULL, 0);
	 m_pImageBuf    =   NULL    ;
    }

    m_dImageOff		= 
    m_dImageOffLo	=
    m_dImageOffEx	= 0;

    memset 
 (& m_sRecvResult, 0, sizeof (m_sRecvResult));

    FgxOut_Create ();
};

XPGrabber::~XPGrabber ()
{
    if (m_wRecvIdle)
    {
	m_wRecvIdle ->Release ();
	m_wRecvIdle = NULL;
    }

    if (m_oRecvIdle.hEvent)
    {
	::CloseHandle 
       (m_oRecvIdle.hEvent);
	m_oRecvIdle.hEvent = NULL;
    }

    if (m_pImageSof)
    {
	::free 
       (m_pImageSof);
	m_pImageSof = NULL;
    }

    FgxOut_Close ();
};
//
//[]------------------------------------------------------------------------[]
//
void CCyUSBDevice::Close ()
{
    if (m_hFile)
    {	m_hFile  = (::CloseHandle (m_hFile ), NULL);}

    if (m_hIface)
    {	m_hIface = (WinUsb_Free	  (m_hIface), NULL);}
};

Bool CCyUSBDevice::Open ()
{
    GSysDeviceInfoSet    DevInfo;

    if (ERROR_SUCCESS == DevInfo.Create (& CLSID_SysFgxIface))
    {
    for (int i = 0; DevInfo.SelectDevInfo (i); i ++)
    {
    if (DevInfo.SelectInterface (CLSID_SysFgxIface)
    &&  DevInfo.IsDevPresent    ( ))
    {
	if (INVALID_HANDLE_VALUE != 
	   (		 m_hFile  = ::CreateFile 

		        (DevInfo.GetInstancePath (),
		         GENERIC_WRITE 
		       | GENERIC_READ		 ,
			 0			 ,
			 NULL			 ,
			 OPEN_EXISTING		 ,
			 FILE_FLAG_OVERLAPPED |
			 0			 ,
			 NULL		       )))
	{
	if (WinUsb_Initialize (m_hFile, & m_hIface))
	{
	    return True;
	}
	}
    }}}

    Close ();

    return False;
};

Bool CCyUSBDevice::BeginDataXfer (Byte * pBuf, DWORD dBufSize, OVERLAPPED * o)
{
    GResult dResult; DWord dTransfered;
    
    dResult = WinUsb_ReadPipe 
    
   (m_hIface, 0x86, pBuf, dBufSize, & dTransfered, o) ? ERROR_SUCCESS : ::GetLastError ();

    return ((ERROR_SUCCESS    == dResult)
	 || (ERROR_IO_PENDING == dResult)) ? True : False;
};

void CCyUSBDevice::GetXferResult (DWord & dTransfered, OVERLAPPED * o)
{
    WinUsb_GetOverlappedResult (m_hIface, o, & dTransfered, True);
};

void CCyUSBDevice::AbortDataXfer ()
{
    WinUsb_AbortPipe (m_hIface, 0x86);
};
//
//[]------------------------------------------------------------------------[]
//
GIrp * XPGrabber::Co_Create (GIrp * pIrp, void *)
{
    if (pIrp ->Cancelled ())
    {	
	    (m_pCBProc)(m_pCBProcContext, 0x00, 0);
	    return pIrp;
    }

    SetCurCoProc <XPGrabber, void *>
   (m_pIoIrp,	& XPGrabber::Co_Create, this, NULL);
    
    if (m_CyDevice.Open ())
    {
	m_dImageOff   =	   
	m_dImageOffLo =
	m_dImageOffEx =	   0 ;

	memset 
     (& m_sRecvResult, 0, sizeof (m_sRecvResult));

	m_bOpened     = True ;

	if (Listen ())
	{
	    (m_pCBProc)(m_pCBProcContext, 0x08, 0);

	    return NULL;
	}

        m_bOpened   = False;

	m_CyDevice.Close ();
    }

	   (m_pCBProc)(m_pCBProcContext, 0x00, 0);
	    QueueIrpForComplete (pIrp, 500);

printf ("\tXPGrabber::Wait Creation \n");

	    return NULL;
};

GIrp * XPGrabber::Co_Close (GIrp * pIrp, void *)
{
    m_pIoIrp = NULL;

printf ("\tXPGrabber::Closed\n");

    return pIrp;
};

void XPGrabber::Create ()
{
    m_pIoIrp  = new GIrp ();

    SetCurCoProc <XPGrabber, void *>
   (m_pIoIrp, & XPGrabber::Co_Close, this, NULL);

    SetCurCoProc <XPGrabber, void *>
   (m_pIoIrp, & XPGrabber::Co_Create, this, NULL);

    CompleteIrp (m_pIoIrp);
};

void XPGrabber::Close ()
{
	m_bOpened = False;

    if (m_pIoIrp)
    {
	CancelIrp (m_pIoIrp);
    }

    if (True)
    {
	m_CyDevice.AbortDataXfer ();

printf ("\tXPGrabber::Abort\n");

	{
	    while (NULL != (volatile void *) m_pRecvIdleCytx)
	    {
 		if (TCurrent () ->GetCurApartment () ->PumpInputWork ())
		{
		    TCurrent () ->GetCurApartment () ->Work ();

		    continue;
		}
		    TCurrent () ->GetCurApartment () ->WaitInputWork (20);
	    }
	}

	m_CyDevice.Close ();

printf ("\tXPGrabber::CyDevice::Close\n");

    }
};
//
//[]------------------------------------------------------------------------[]
//
DWord XPGrabber::ContinueImage (DWord dImageOff)
{
    if (m_cContinueDelay)
    {
    if (  dImageOff + XP_RECV_SIZE >= XP_BUFFER_SIZE)
    {
	  dImageOff = XP_BUFFER_SIZE - XP_RECV_SIZE;
    }
	m_dImageOff   = 
	m_dImageOffEx = dImageOff;

printf ("\tXPGrabber::Continue %d\n", m_dImageOff);
    }

    return m_dImageOff;
};

static void ClearChannel (Word * pHead, const Word * pTail)
{
    for (; pHead < pTail; pHead += 3) {* pHead = 0x0000; }
};

Bool XPGrabber::Co_Idle (IWakeUp::Reason Reason)
{
    SRowHeader rhdr; const Word * p; DWord dImageOff; DWord dTransfered = 0;

    if (IWakeUp::OnOccured == Reason)
    {
	if     (m_oRecvIdle.Internal == 0x103)
	{
printf ("{F=%08X:%08X}",  m_oRecvIdle.Internal, m_oRecvIdle.InternalHigh);

	    return True;
	}

	m_CyDevice.GetXferResult (dTransfered, & m_oRecvIdle);

	m_pRecvIdleCytx = NULL;


// ---------------------------------------- Debug data lost generation !!! 
/*
	if (dTransfered > 2)
	{
	    dTransfered -= 2;
	}
*/
// ---------------------------------------- Debug data lost generation !!!

	if (dTransfered > 0)
	{

if ((0 == m_dImageOff) && (0 == m_cContinueDelay))
{
    FgxOut_Reset ();
}
    FgxOut_Write (m_pImageSof, dTransfered);

	    m_sRecvResult.dRecvSize +=  
	   (dTransfered		     = (dTransfered / 2) * 2);

if (s_bCRCMode)
{{
	    const void * pImageSof   = m_pImageSof;

    for (;  dTransfered > 0;  )
    {
	if (0 == m_sRecvBuf.sizeforput ())  // flush if no space for write
	{				    //
	    m_sRecvResult.RejectData 
	   (m_sRecvBuf.Read    (NULL, XP_RECV_SIZE_1_3));
	    m_sRecvBuf.sethead ();
	}
	    m_sRecvBuf.Write   (pImageSof, dTransfered);

lNextHeader :

	if (NULL == (p = GetRowHeader (rhdr, (Word *) m_sRecvBuf.head (),
					     (Word *) m_sRecvBuf.tail ())))
	{   continue;  }

	    m_sRecvResult.nErrCRC2      += (rhdr.dCRCError & 0x40) ? 1 : 0;
	    m_sRecvResult.nErrCRC1      += (rhdr.dCRCError & 0x20) ? 1 : 0;
	    m_sRecvResult.nErrCRC3      += (rhdr.dCRCError & 0x10) ? 1 : 0;

	if     ((Byte *)(p - rhdr.nRowSize) < m_sRecvBuf.head ())
	{
		m_sRecvResult.RejectData (((Byte *) p + rhdr.nLength) - m_sRecvBuf.head ());

		goto lSkipHeader;
	}
	//
	//     ((Byte *)(p - rhdr.nRowSize) >= m_sRecvBuf.head ())
	//

	if (0x00 == CheckDataCRC16_8005 (rhdr, (Byte *)(p - rhdr.nRowSize), (Byte *) p))
	{
	    m_sRecvResult.nRowOk ++;
	}
	else
	{
	    m_sRecvResult.nErrDataCRC2  += (rhdr.dCRCError & 0x04) ? 1 : 0;
	    m_sRecvResult.nErrDataCRC1  += (rhdr.dCRCError & 0x02) ? 1 : 0;
	    m_sRecvResult.nErrDataCRC3  += (rhdr.dCRCError & 0x01) ? 1 : 0;

	    if (((Byte *)(p - rhdr.nRowSize) > m_sRecvBuf.head ()) 
	    && ((rhdr.dCRCError & 0x44) != 0x00)
	    && ((rhdr.dCRCError & 0x22) != 0x00)
	    && ((rhdr.dCRCError & 0x11) != 0x00))
	    {
		m_sRecvResult.RejectData (((Byte *) p + rhdr.nLength) - m_sRecvBuf.head ());

		goto lSkipHeader;
	    }

	    if (rhdr.dCRCError & 0x40) { ClearChannel ((Word *)(p - rhdr.nRowSize + 0), p); }
	    if (rhdr.dCRCError & 0x20) { ClearChannel ((Word *)(p - rhdr.nRowSize + 1), p); }
	    if (rhdr.dCRCError & 0x10) { ClearChannel ((Word *)(p - rhdr.nRowSize + 2), p); }
	}

	if ((XP_BUFFER_SIZE - XP_RECV_SIZE) < (dImageOff = m_dImageOffEx + B_RAW_LENGTH * rhdr.nRow))
	{
	    dImageOff = XP_BUFFER_SIZE - XP_RECV_SIZE;
	}

	if (m_dImageOff > dImageOff)
	{
	    memcpy (m_bImageBuf + dImageOff, (Byte *)(p - rhdr.nRowSize), B_RAW_LENGTH);

	    m_sRecvResult.nOverwrited ++;
/*
printf ("_Row %d.%d [%d], %d.%d\n", rhdr.nFrame, rhdr.nRow, rhdr.nRowSize * 2, m_dImageOffEx, m_dImageOff);
*/
	}
	else
        {
	    for (;  m_dImageOff < dImageOff;)
	    {
		if (   m_dImageOffLo < dImageOff )
		{
		    memset (m_bImageBuf + m_dImageOff, 0x00, B_RAW_LENGTH);

		    m_sRecvResult.nSkiped ++;
/*
printf ("-Row %d.%d [%d], %d\n", rhdr.nFrame, rhdr.nRow, rhdr.nRowSize * 2, m_dImageOff);
*/
		}
		    m_dImageOff += B_RAW_LENGTH;
	    }
	    //
	    //	    m_dImageOff == dImageOff !!!
	    //

	    if (0 == m_dImageOff)
	    {
printf ("\tXPGrabber::Sof %d [%d]\n", m_dImageOff, rhdr.nRow);

	       (m_pCBProc)(m_pCBProcContext, 0x02, dImageOff + B_RAW_LENGTH);
	    }
	    else
	    {
	       (m_pCBProc)(m_pCBProcContext, 0x03, dImageOff + B_RAW_LENGTH);
	    }
	    
	    memcpy (m_bImageBuf + dImageOff, (Byte *)(p - rhdr.nRowSize), B_RAW_LENGTH);

	    m_dImageOffLo = 
	   (m_dImageOff  += B_RAW_LENGTH);
/*
printf ("+Row %d.%d [%d], %d\n", rhdr.nFrame, rhdr.nRow, rhdr.nRowSize * 2, m_dImageOff);
*/
	}

	m_sRecvResult.RejectData (((Byte *)(p - rhdr.nRowSize)) - m_sRecvBuf.head ());

	m_sRecvResult.bErrDataSync = False;
	m_sRecvResult.nRowAccepted ++;

	m_sRecvResult.dRecvAccepted += ((rhdr.nRowSize * 2) + rhdr.nLength);
	    
lSkipHeader :

	m_sRecvBuf.pHead = (Byte *) p + rhdr.nLength;
	m_sRecvBuf.sethead ();

	goto lNextHeader;

    } // for (dTransfered > 0);
}}
else					// ! s_bCRCMode --------------- <<<
{{
	m_sRecvResult.dRecvAccepted += dTransfered;

	    if (0 == m_dImageOff)	// This is first data packet
	    {
printf ("\tXPGrabber::Sof %d\n", m_dImageOff);

	       (m_pCBProc)(m_pCBProcContext, 0x02, m_dImageOff + dTransfered);
	    }
	    else
	    {
	       (m_pCBProc)(m_pCBProcContext, 0x03, m_dImageOff + dTransfered);
	    }
		memcpy (m_bImageBuf + m_dImageOff, m_pImageSof,  dTransfered);

			m_dImageOff += dTransfered;

}}					// ! s_bSCRMode --------------- >>>
	} // (dTransfered > 0)

	else				// This is eof or may be cancel !!!
	{
	    if (0 < m_dImageOff)
	    {
		m_sRecvResult.RejectData 
	       (m_sRecvBuf.Read (NULL, (DWord) m_sRecvBuf.sizeforget ()));

printf ("\tXPGrabber::Eof %d (%d)\n", m_dImageOff, dTransfered);

printf ("\tXPGrabber::Re0 %d - %d - %d.%d = %d, [%d.{%d.%d.%d}.%d]\n",

		m_sRecvResult.dRecvSize     ,
		m_sRecvResult.dRecvAccepted ,
		m_sRecvResult.dRecvRejected ,
		m_sRecvBuf.sizeforget     (),

		m_sRecvResult.dRecvSize     -
		m_sRecvResult.dRecvAccepted -
		m_sRecvResult.dRecvRejected -
		m_sRecvBuf.sizeforget     (),
			    
		m_sRecvResult.nRowAccepted  ,
		m_sRecvResult.nErrCRC2	+
		m_sRecvResult.nErrDataCRC2  ,
		m_sRecvResult.nErrCRC1	+
		m_sRecvResult.nErrDataCRC1  ,
		m_sRecvResult.nErrCRC3	+
		m_sRecvResult.nErrDataCRC3  ,
		m_sRecvResult.nErrDataSync  );

	       (m_pCBProc)(m_pCBProcContext, 0x01, m_dImageOff + dTransfered);

		m_dImageOff = 0;

		memset (& m_sRecvResult, 0, sizeof (m_sRecvResult));
		m_sRecvBuf.clear (); 
	    }
	}
	if (! Listen ())
	{
printf ("\tXPGrabber::End of session cancel Io\n");

	if (m_pIoIrp)
	{
	    CompleteIrp (m_pIoIrp);
	}
	    return False ;
	}
    }
    else
    if (IWakeUp::OnTimeout == Reason)
    {
/*
printf ("\nXPGrabber::Timeout\n");
*/
	if (0 < m_cContinueDelay)
	{
	if (0 == -- m_cContinueDelay)
	{

FgxOut_Flush ();

printf ("\tXPGrabber::Eof %d (finaly !!!)\n", m_dImageOff);

	       (m_pCBProc)(m_pCBProcContext, 0x10, m_dImageOff);

		m_dImageOff   = 
		m_dImageOffLo = 
		m_dImageOffEx = 0;

		memset (& m_sRecvResult, 0, sizeof (m_sRecvResult));
		m_sRecvBuf.clear (); 
	}
	}
	if (0 < m_dImageOff)
	{
		m_cContinueDelay = 5;

		m_sRecvResult.RejectData
	       (m_sRecvBuf.Read (NULL, (DWord) m_sRecvBuf.sizeforget ()));

printf ("\tXPGrabber::Eof %d (T)\n", m_dImageOff);

printf ("\tXPGrabber::Re1 %d - %d - %d.%d = %d, [%d.{%d.%d.%d}.%d]\n",

		m_sRecvResult.dRecvSize     ,
		m_sRecvResult.dRecvAccepted ,
		m_sRecvResult.dRecvRejected ,
		0			    ,

		m_sRecvResult.dRecvSize     -
		m_sRecvResult.dRecvAccepted -
		m_sRecvResult.dRecvRejected ,
			    
		m_sRecvResult.nRowAccepted  ,
		m_sRecvResult.nErrCRC2	+
		m_sRecvResult.nErrDataCRC2  ,
		m_sRecvResult.nErrCRC1	+
		m_sRecvResult.nErrDataCRC1  ,
		m_sRecvResult.nErrCRC3	+
		m_sRecvResult.nErrDataCRC3  ,
		m_sRecvResult.nErrDataSync  );

	       (m_pCBProc)(m_pCBProcContext, 0x01, m_dImageOff + dTransfered);
 
		m_dImageOff = 0;

		memset (& m_sRecvResult, 0, sizeof (m_sRecvResult));
		m_sRecvBuf.clear (); 
	}
    }
	    return True ;
};

Bool XPGrabber::XxxxIoCo_Idle (IWakeUp *, IWakeUp::Reason Reason, void * pThis, void *)
{
    return ((XPGrabber *) pThis) ->Co_Idle (Reason);
};

Bool XPGrabber::Listen ()
{
    if (m_dImageOff + XP_RECV_SIZE >= XP_BUFFER_SIZE)
    {
printf ("\tXPGrabber::Buf Overflow %d\n", m_dImageOff);

       (m_pCBProc)(m_pCBProcContext, 0x04, m_dImageOff);

	m_dImageOff   =
	m_dImageOffLo =
	m_dImageOffEx = XP_BUFFER_SIZE - XP_RECV_SIZE;
    }

    if (! m_bOpened)
    {
	return False;
    }

    if (NULL		!=
       (m_pRecvIdleCytx  = (PUCHAR) m_CyDevice.BeginDataXfer (
			   (PUCHAR) m_pImageSof, XP_RECV_SIZE, & m_oRecvIdle)))
    {
	m_wRecvIdle ->QueueWaitFor
			 (m_oRecvIdle.hEvent, XxxxIoCo_Idle, this, NULL, 500, 500);
	return True ;
    }

printf ("\tXPGrabber::BeginDataXfer fail start\n");
	return False;
};
//
//[]------------------------------------------------------------------------[]
