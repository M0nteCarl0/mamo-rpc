//[]------------------------------------------------------------------------[]
// RRP Ftdi devices declarations
//
//
// Created by GSh.
//[]------------------------------------------------------------------------[]

#ifndef __R_FTDIBUS_H
#define __R_FTDIBUS_H

#include "g_winfile.h"

#include "ftd2xx.h"

#include "winusb.h"
#pragma comment	(lib, "winusb.lib")

//[]------------------------------------------------------------------------[]
//
class GSerialFtdiFile : public GSerialIoDevice
{
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
class GSerialBus
{
public :

#pragma pack (_M_PACK_VPTR)

    struct TransactRequest : public GIrp_Request
    {
    };

#pragma pack ()

protected :
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
class GFtdiFile
{
public :

			GFtdiFile   ();
		       ~GFtdiFile   ();

	GResult		Create		    (const char * pDescriptionOrId   ,
						   Bool	  bDescription	     );

	void		Close		    ();

	void		QueueForSend	    (GIrp * pIrp, void * pSendBuf    ,
							  DWord	 dSendBufLen );

	void		QueueForRecv	    (GIrp * pIrp, void * pRecvBuf    ,
							  DWord	 dRecvBufSize);
protected :

	void	   SetIoCoResult    (GIrp *);

static	Bool GAPI  XxxxIoCo_Recv    (IWakeUp *, IWakeUp::Reason, void *, void *);

static	Bool GAPI  XxxxIoCo_Send    (IWakeUp *, IWakeUp::Reason, void *, void *);

protected :

	GCloseLock		m_fCloseLock	;

	FT_HANDLE		m_hFile		;

	HANDLE			m_hXferIoCo	;
	IWakeUp *		m_wXfer		;
	GOverlapped		m_oXfer		;
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
class CCyUSBDevice
{
public :
			CCyUSBDevice	()
			{
			    m_hFile  = NULL;
			    m_hIface = NULL;
			};

	Bool		Open 		();
	void		Close		();

	Bool		BeginDataXfer	(Byte *, DWord  , OVERLAPPED *);
	void		GetXferResult	(	 DWord &, OVERLAPPED *);

	void		AbortDataXfer	();

protected :

	HANDLE		m_hFile	    ;
	void *		m_hIface    ;
};

class XPGrabber
{
public :
			XPGrabber   (void (GAPI *)(void *, DWord, DWord), void *);

		       ~XPGrabber   ();

	void		Create		    ();

	void		Close		    ();

	void *		GetImageBuf	    () const { return m_pImageBuf; };

	DWord		ContinueImage	    (DWord dImageOff);

static	Bool			s_bCRCMode	 ;

protected :
/*
#pragma pack (_M_PACK_VPTR)

	struct RecvRequest : public GIrp_Request
	{
	    void *		pRecvBuf	;
	    DWord		dRecvBufSize	;
	    DWord		dSeqOffset	;

	    HANDLE		hIoCoEvent	;
    	    IWakeUp *		wIoCoEvent	;
	    UCHAR *		pIoCoCytx	;
	    GOverlapped		o		;

	    RecvRequest *	Init ()
				{
				    GIrp_Request::Init (0x0000);

				    this ->pRecvBuf	= NULL      ;
				    this ->dRecvBufSize = 0	    ;
				    this ->dSeqOffset	= 0	    ;

				    this ->hIoCoEvent   = ::CreateEvent (NULL, True, False, NULL);
				    this ->wIoCoEvent   = NULL	    ;
				    CreateWakeUp 
				   (this ->wIoCoEvent);
				    this ->pIoCoCytx	= NULL	    ;
				    this ->o		  .Init ()  ;

				    return this;
				};

	    static void GAPI	Destroy (RecvRequest * pThis)
				{
				    if (pThis ->wIoCoEvent)
				    {
					pThis ->wIoCoEvent ->Release ();
					pThis ->wIoCoEvent = NULL;
				    }
				    if (pThis ->hIoCoEvent)
				    {
					::CloseHandle 
				       (pThis ->hIoCoEvent);
					pThis ->hIoCoEvent = NULL;
				    }
				};
	};

#pragma pack ()
*/
protected :

	Bool		 Co_Idle    (IWakeUp::Reason);

static	Bool GAPI  XxxxIoCo_Idle    (IWakeUp *, IWakeUp::Reason, void *, void *);

	Bool		  Listen    ();

	GIrp *	       Co_Create    (GIrp *, void *);

	GIrp *	       Co_Close	    (GIrp *, void *);

protected :

	void	        (GAPI * m_pCBProc)(void *, DWord, DWord);
	void *			m_pCBProcContext ;

	GIrp *			m_pIoIrp	 ;
	Bool			m_bOpened	 ;
	int			m_cContinueDelay ;

	IWakeUp *		m_wRecvIdle	 ;
	UCHAR   *		m_pRecvIdleCytx	 ;
	OVERLAPPED		m_oRecvIdle	 ;

	void *			m_pImageSof	 ;

	union
	{
	    void *		m_pImageBuf	 ;
	    Byte *		m_bImageBuf	 ;
	};
	DWord			m_dImageOff	 ;

	CCyUSBDevice		m_CyDevice	 ;

	DWord			m_dImageOffLo	 ;
	DWord			m_dImageOffEx	 ;

	GStreamBuf		m_sRecvBuf	 ;

public :

	struct
	{
	    void    RejectData (size_t dDataSize)
	    {
		if (0 < dDataSize)
		{
		    nErrDataSync  += ((bErrDataSync) ? 0 : (bErrDataSync = 1, 1));
		    dRecvRejected += ((DWord) dDataSize);
		}
	    };

	    DWord		dRecvSize	 ;  // Number incomming bytes in the frame
	    DWord		dRecvAccepted	 ;  // Number accepted bytes in the frame
	    DWord		dRecvRejected	 ;  // Number rejected bytes in the frame

	    int			nRowAccepted	 ;  // Row accepted
	    int			nRowOk		 ;  // Row accepted + CRC Header + CRC Data
	    int			nSkiped		 ;  // No rows
	    int			nOverwrited	 ;

	    int			nErrCRC2	 ;  // CRC2 header
	    int			nErrCRC3	 ;  // CRC3 header
	    int			nErrCRC1	 ;  // CRC1 header
	    int			nErrDataCRC2	 ;  // CRC2 header ?	     , row accepted
	    int			nErrDataCRC3	 ;  // CRC3 header ?	     , row accepted
	    int			nErrDataCRC1	 ;  // CRC1 header ?	     , row accepted

	    Bool		bErrDataSync	 ;  // Current sync state
	    int			nErrDataSync	 ;  // row not accepted

	}			m_sRecvResult    ;
};
//
//[]------------------------------------------------------------------------[]

#endif//__R_FTDIBUS_H