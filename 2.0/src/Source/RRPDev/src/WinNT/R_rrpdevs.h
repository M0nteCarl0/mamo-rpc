//[]------------------------------------------------------------------------[]
// RRP Serial interface devices declarations
//
//
// Created by GSh.
//[]------------------------------------------------------------------------[]

#ifndef __R_DEVDEFS_H
#define	__R_DEVDEFS_H

#ifdef	__R_RRPDEVS_BUILD_DLL
#define	IRRPDEVS_EXPORT __declspec(dllexport)
#define	INITGUID
#else
#define IRRPDEVS_EXPORT
#endif

#include "RRPCalibrate.h"

//[]------------------------------------------------------------------------[]
//
    DECLARE_INTERFACE  (IRRPProtocol)
    {
#pragma pack (_M_PACK_BYTE)

	struct DataPack				// All fields in BigEndian
	{
	    _U16	uLength	    ;
	    _U16	uType	    ;
	    _U32	uRequestId  ;
	};

	struct ParamPack : public DataPack	// 0 - Type 
	{
	struct		Entry
	{
	    _U32	uParamId    ;
	    _U32	uValue	    ;

//	}		Entries [1] ;
	}		Entries	    ;
	};

#pragma pack ()    
    };
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
    DECLARE_INTERFACE_ (IRRPCallBack, IUnknown)
    {
    DECLARE_INTERFACE  (Handle)
    {
    STDMETHOD_ (GResult,  Duplicate)(Handle *&	  hICallBack,
				     IRRPCallBack *
						  pICallBack) PURE;
    STDMETHOD_ (void,	      Close)() PURE;

    STDMETHOD_ (void,	     Select)(_U32 *	  pParamIds ) PURE;

    STDMETHOD_ (void,	   UnSelect)(_U32 *	  pParamIds ) PURE;

    STDMETHOD_ (void,	    Refresh)(_U32 *	  pParamIds ) PURE;
    };

    STDMETHOD_ (void,	     Invoke)(const void * hICallBack ,
				     const 
				     IRRPProtocol::DataPack *) PURE;
    };
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
    DEFINE_GUID	       (IID_IRRPDevice, 0x47f7cc38,0xcf04,0x4712,0x89,0x47,0x8b,0x26,0x0b,0x87,0xd6,0xbf);

    DECLARE_INTERFACE_ (IRRPDevice, IUnknown)
    {
	typedef enum
	{
	    IRRPDevice_BusOnline    = 0x0001,
	    IRRPDevice_DevOnline    = 0x0002,	// Device's IoLoop worked
	    IRRPDevice_CoData	    = 0x0004,	// All data in the Cogerent state
	    IRRPDevice_Locked	    = 0x0008,	// Device Locked by some slave operation
						// any write or set mode operation disabled
	}   IRRPDevice_StatusMask;

#pragma pack (_M_PACK_LONG)

	typedef struct
	{
	    _U32	    uParamId ;

	union
	{
	    _I32	    i32Value ;
	    _U32	    u32Value ;
	    _R32	    r32Value ;
	}			     ;

	    void	    Init    (_U32 uParamId, _U32 u32Value)
	    {
		this ->uParamId = uParamId;
		this ->u32Value = u32Value;
	    };
	}		    Param    ;

#pragma pack ()

    STDMETHOD_ (GResult,    Connect)(IRRPCallBack::Handle *&
						    hICallBack ,
				     IRRPCallBack * pICallBack ) PURE;

    STDMETHOD_ (void,	   SetParam)(	   int	    nParams    ,
				     const Param *  pParams    ) PURE;

    STDMETHOD_ (void,	   GetParam)(	   int	    nParams    ,
					   Param *  pParams    ) PURE;
    };
//
//[]------------------------------------------------------------------------[]


//[]------------------------------------------------------------------------[]
//
    DEFINE_GUID	       (IID_IRRPUrp, 0xf327dc9f,0x100c,0x42fd,0x8b,0xe9,0x03,0x1e,0x1d,0xa2,0x48,0x6a);

    DECLARE_INTERFACE_ (IRRPUrp, IRRPDevice)
    {
	typedef enum
	{
	    EquMask_KKM		= 0x0001,

	}   EquMask	;

	typedef enum
	{
	    HVReady_Block	= 0x0001,
	    HVReady_HeatTimeout	= 0x0002,
	    HVReady_NotCharge	= 0x0004,
	    HVReady_RedButton	= 0x0008,
	    HVReady_NotConnect	= 0x0080

	}   HVReadyMask	;

	typedef enum
	{
	    HVRelease		= 0,
	    HVPrepare		= 1,
	    HVOn		= 2,

	}   HVCommand	;

    STDMETHOD_ (void,		      SetHV)(HVCommand)	    PURE;

#pragma pack (push, _M_PACK_VPTR)

	struct RunHV_Request : public GIrp_Request
	{
	    enum { Id = 0x0001};

	    RunHV_Request * Init ()
			    {
				GIrp_Request::Init (Id);

				return this;
			    };
	};

#pragma pack (pop)

    STDMETHOD_ (void,		      RunHV)(GIrp *)		PURE;

    STDMETHOD_ (void,	    CheckExposition)(Bool   bBigFocus ,
					     int  * nUAnode   ,
					     int  * nIAnode   ) PURE;

    STDMETHOD_ (int,	       GetUAnodeX10)(int    fUAnodeR10) PURE;

    STDMETHOD_ (int,	       GetUAnodeR10)(int    nUAnodeX10) PURE;

	typedef	enum
	{
	    ID_RegGetStatusHV		= 0x8000007F,
	    ID_ParSetEquMask		= 0x8000007E,
	    ID_RegGetHVReady		= 0x8000007D,

	    ID_ParSetIAnode		= 0x80000007,
	    ID_ParSetIAnodeMode		= 0x80000008,
	    ID_ParSetMode		= 0x8000000A, // ClrMask, SetMask

//	    ID_RegSetUAnode		= 0x80009E9D,
	    ID_RegSetUAnode		= 0x8000000B,
	    ID_RegSetIAnode		= 0x8000A09F,

	    ID_UAnodeMin		= 0x80002E00,
	    ID_UAnodeMax		= 0x8000AE00,
	    ID_UAnodeCoef		= 0x80001E1D, // 9E 9D

	    ID_Status			= 0x80005453, // D4 93

	    ID_Focus			= 0x80000097,

	    ID_IFilamentPre		= 0x80002221, // A2 A1 / A6 A5
	    ID_IFilament		= 0x80002423, // A4 A3 / A8 A7

	    ID_IFilamentPreSF		= 0x8000A2A1,
	    ID_IFilamentSF		= 0x8000A4A3,

	    ID_IFilamentPreBF		= 0x8000A6A5,
	    ID_IFilamentBF		= 0x8000A8A7,

	    ID_MAS			= 0x8000AAA9,

	    ID_ExposeTime		= 0x8000ACAB,

	    ID_Block			= 0x80000000,

	    ID_HVDelay			= 0x800000B7,

	    ID_ParSetFieldSize		= 0x80000101,

	    ID_RegGetPower		= 0x80000109,
	    ID_RegKKMStatus		= 0x80000108,
	    ID_RegGetU220		= 0x80000102,
	    ID_RegGetUForce		= 0x80000103,
	    ID_RegGetICharging		= 0x80000104,
	    ID_RegGetTForceItems	= 0x800000F9,

	    ID_RegHeatTubeTimeout	= 0x80000106,
	    ID_RegHeatForceTimeout	= 0x80000107,

	    ID_RegGetUAnodePos		= 0x8000E1E0,
	    ID_RegGetUAnodeNeg		= 0x8000E3E2,

	    ID_RegGetUAnode		= 0x8000E1E0,

	    ID_ParSetUAnode		= 0x8000010A,
	    ID_ParGetUAnode		= 0x8000010B,

	    ID_ParGetShotCount		= 0x8000010C,
	    ID_ParGetShotCountBad	= 0x8000010D,

	    ID_ParSetUAnode10		= 0x8000010E,
	    ID_ParGetUAnode10		= 0x8000010F,

/*
	struct SendRequest : public GIrp_Request
	{
	    void *		pSendBuf	;
	    DWord		dSendBufSize	;

	    SendRequest *   Init (void * pBuf, DWord dBufSize)
			    {
				GIrp_Request::Init (0x0000);

				this ->pSendBuf	    = pBuf     ;
				this ->dSendBufSize = dBufSize ;

				return this;
			    };
	};
*/
	}   ParamId ;
    };
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
    DECLARE_INTERFACE_ (IRRPKKM, IRRPDevice)
    {
	typedef enum
	{
	    ID_RegGetPower		= 0x000000CC,

	    ID_RegGetU220		= 0xCC00FE00,
	    ID_RegGetUForce		= 0xCC00F7F6,
	    ID_RegGetICharging		= 0xCC00CD00,
	    ID_RegGetTForceItems	= 0xCC00FF00,

	}   ParamId ;
    };
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
    DECLARE_INTERFACE_ (IRRPMM2, IRRPDevice)
    {
	typedef enum
	{
	    MoveUpPin		= 0x0004,
	    MoveDnPin		= 0x0008,

	    RotLtPin		= 0x0400,
	    RotLt45Pin		= 0x2000,
	    RotMdPin		= 0x0800,
	    RotRt45Pin		= 0x4000,
	    RotRtPin		= 0x1000,

	}   MovePin_Mask ;  // For ID_RegGetMoveStatus

	typedef enum
	{
	    RotLt90		= 0x04,
	    RotLt45		= 0x20,
	    RotMd		= 0x08,
	    RotRt45		= 0x40,
	    RotRt90		= 0x10
	    
	}   RotateEx	 ;


	typedef enum
	{
	    ID_RegGetMoveStatus		= 0x91009291,
	    ID_RegGetPressStatus	= 0x910000BF,

	    ID_RegGetMotorsStatus	= 0x91000001,

	    ID_RegGetRotPos		= 0x9100BDBC,
	    ID_RegGetThick		= 0x91009300,
	    ID_RegGetPress		= 0x91009400,

	    ID_RegSetPressHigh		= 0x910000C3,

	    ID_SetPressMode		= 0x91009201,
	    ID_RunMove			= 0x91009103,
	    ID_RunRotate		= 0x91009203,
	    ID_RunRotateFix		= 0x91009204,

	    ID_RunPress			= 0x9100BF03,

	}   ParamId	;
    };
//
//[]------------------------------------------------------------------------[]


//[]------------------------------------------------------------------------[]
//
    DECLARE_INTERFACE_ (IRRPMMotor, IRRPDevice)
    {
	typedef enum
	{
	    Stop	= 0x00,
	    RunForward  = 0x02,
	    RunBackward	= 0x03,

	}   RunCommand ;

    STDMETHOD_ (void,		    Run)(RunCommand, int nStep)	      PURE;

    STDMETHOD_ (void,	   SetParamBits)(_U32 uParamId, _U32 ClrMask,
							_U32 SetMask) PURE;
    STDMETHOD_ (_U32,	       GetParam)(_U32 uParamId)		      PURE;

	typedef enum
	{
	    ID_RegGetStatus		= 0x0000D7D6,

	    ID_RegGetPosition		= 0x0000DEDF,

	    ID_RegSetStep		= 0x0000D8D9,
	    ID_RegSetVelocity		= 0x0000DADB,
	    ID_RegSetAccelTime		= 0x0000DCDD,
	    ID_RegSetDefCoeff		= 0x000000BD,
	    ID_RegSetSyncDelay		= 0x000000BE,
	    ID_RegSetShotRows		= 0x000000B8,

	}   ParamId ;
    };
//
//[]------------------------------------------------------------------------[]
//
    DECLARE_INTERFACE_ (IRRPMScan, IRRPMMotor)
    {
	typedef enum
	{
	    IRRPMScan_FGOnline	= 0x0010,	// FGX device Online state

	    IRRPMScan_FGDataSof	= 0x0020,
	    IRRPMScan_FGDataEof	= 0x0040,
	    IRRPMScan_FGDataOvf	= 0x0080,

	}   IRRPMScan_StatusMask;

	typedef enum
	{
	    FGStatus_Online	= 0x01,
	    FGStatus_DataSof	= 0x02,
	    FGStatus_DataEof	= 0x04,
	    FGStatus_Overflow	= 0x08,
	    FGStatus_DataReady	= 0x10

	}   StatusMask	 ;

	typedef enum
	{
	    ID_RegGetFGStatus	= 0xD6000057,
	    ID_RegGetFGOffset	= 0xD6000058,
	    ID_RegGetFGOffsetEx	= 0xD6000059,

	    ID_RegGetVersion	= 0xD6000060,	    // 0x40 Name + HW/FW version (beg) ...
	    ID_RegGetVersion2	= 0xD6000061,	    // 0x40 Name + HW/FW version ... (end)
	    ID_RegGetVersion2a	= 0xD6000067,	    // 0x5C Altera version
	    ID_RegGetVersion3	= 0xD6000062,	    // 0x7F Serial number
	    ID_RegGetVersion4	= 0xD6000063,	    // 0x46 Active SizeY / Active Step / Delay Step
	    ID_RegGetVersion5	= 0xD6000064,	    // 0x44 Active SizeY Ext
	    ID_RegGetVersion5a	= 0xD6000066,	    // 0x45 Delay  SizeY Ext
	    ID_RegGetVersion6   = 0xD6000065,	    // 0x47 Error  Byte

	}   ParamId	 ;

    STDMETHOD_ (DWord,	     GetDataPtr)(void *& pHead  , DWord dOffset,
							  DWord dLength) PURE;

    STDMETHOD_ (void,	   GetTestImage)() PURE;
    };
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
    DECLARE_INTERFACE_ (IRRPMDIG, IRRPDevice)
    {
	typedef enum
	{
	    ID_RegGetStatus		= 0x50000000,
	    ID_RegGetStatusHV		= 0x5000007F,

	    ID_ParSetEquMask		= 0x5000007E,

	    ID_RegGetEquStatus		= 0x5000007C,

	    ID_ParSetFieldSize		= 0x50000001,
	    ID_ParSetPressMode		= 0x50000002,
	    ID_ParSetFocusSize		= 0x50000003,
	    ID_ParSetExposeMode		= 0X50000004,

	    ID_ParSetThick		= 0x50000005,
	    ID_ParSetUAnode		= 0x50000006,
	    ID_ParSetIAnode		= 0x50000007,
	    ID_ParDecompress		= 0x50000008,
	    ID_ParSetPressHigh		= 0x50000009,

	    ID_ParSetUAnode10		= 0x5000000A,

	    ID_ParGetRotPos		= 0x50000101,
	    ID_ParGetPress		= 0x50000102,
	    ID_ParGetThick		= 0x50000103,

	    ID_ParGetUAnode		= 0x50000104,
	    ID_ParGetIAnode		= 0x50000105,

	    ID_ParGetHeatTimeout	= 0x50000106,

	    ID_ParGetExposeTime		= 0x50000107,

	    ID_ParGetUAnode10		= 0x50000108,

//	    ID_ParImageHeightFlow	= 0x50000015,
//
	    ID_ParImageWidth		= 0x50000201,
	    ID_ParImageHeight		= 0x50000202,
	    ID_ParImageExposeTimeEff	= 0x50000203,

	    ID_ParGetShotCount		= 0x50000204,
	    ID_ParGetShotCountBad	= 0x50000205,

	}   ParamId	;

	typedef enum
	{
	    EquMask_Urp		= 0x0001,
	    EquMask_KKM		= 0x0002,
	    EquMask_MDiafr	= 0x0010,
	    EquMask_MScan	= 0x0020,
	    EquMask_MFG		= 0x0040,
	    EquMask_MM2		= 0x0100,

	}   EquMask	;

	typedef enum
	{
	    FieldSize_Full	= 0x00,
	    FieldSize_21x30	= 0x01,
	    FieldSize_18x24	= 0x02,
	    FieldSize_12x12	= 0x03,

	}   FiledSize	;

	typedef enum
	{
	    PressMode_Auto	= 0x00,
	    PressMode_Manual	= 0x01,

	}   PressMode	;

	typedef enum
	{
	    FocusSize_SF	= 0x00,
	    FocusSize_BF	= 0x01,

	}   FocusSize	;

	typedef enum
	{
	    ExposeMode_Auto	= 0x00,
	    ExposeMode_AutoHalf	= 0x01,
	    ExposeMode_Manual	= 0x03,

	}   ExposeMode	;

	typedef enum
	{
	    PressDetector_Off	= -2,
	    PressDetector_On	= -1,

	}   PressDetector   ;

	typedef enum
	{
	    HVRelease		= 0,
	    HVOn		= 1,

	}   HVCommand	;

    STDMETHOD_ (_U32,		   GetParam)(_U32   uParamId)	PURE;

    STDMETHOD_ (GResult,	      SetHV)(HVCommand)		PURE;

    STDMETHOD_ (GResult,    PeekImageStatus)(int *  pPhase    ,
					     int *  pStep     ,
					     int *  pStepNum  ) PURE;

    STDMETHOD_ (int,		  GrabImage)(void * pDstBuf   ,
					     DWord  dDstPitch ,
					     int    nFromRow  ,
					     int    nNumRows  ) PURE;
    };
//
//[]------------------------------------------------------------------------[]


//[]------------------------------------------------------------------------[]
//
#define	    MAKE_SETTING_ID(clsid, id)((((_U32) clsid) << 16) | (_U32) id)

    DECLARE_INTERFACE  (IRRPSettings)
    {
	typedef enum
	{
	    CLSID_URP		     = 1,
	    CLSID_MSC		     = 2,
	    CLSID_MDIG		     = 3,
	    CLSID_IMG		     = 4

	}   ClassId ;

	typedef enum
	{
	    ID_MDIG_SerialNum		= MAKE_SETTING_ID (CLSID_MDIG, 0x0001),
	    ID_MDIG_EquMask		= MAKE_SETTING_ID (CLSID_MDIG, 0x0002),
	    ID_MDIG_ThickTable		= MAKE_SETTING_ID (CLSID_MDIG, 0xFF00),

	    ID_MDIG_AnodeFilter		= MAKE_SETTING_ID (CLSID_MDIG, 0x0003),
	    ID_MDIG_SFocusSize		= MAKE_SETTING_ID (CLSID_MDIG, 0x0004),
	    ID_MDIG_BFocusSize		= MAKE_SETTING_ID (CLSID_MDIG, 0x0005),
	    ID_MDIG_DiafrWidth		= MAKE_SETTING_ID (CLSID_MDIG, 0x0006),
	    ID_MDIG_ScanTime		= MAKE_SETTING_ID (CLSID_MDIG, 0x0007),

	    ID_MDIG_RadOutput		= MAKE_SETTING_ID (CLSID_MDIG, 0x0008),
	    ID_MDIG_EngVersion		= MAKE_SETTING_ID (CLSID_MDIG, 0x0009),

	    ID_MDIG_FocusLength		= MAKE_SETTING_ID (CLSID_MDIG, 0x000A),
	//
	//.....................................................................
	//
	    ID_URP_SHOT_UAnodeMin	= MAKE_SETTING_ID (CLSID_URP , 0x0001),
	    ID_URP_SHOT_UAnodeMax	= MAKE_SETTING_ID (CLSID_URP , 0x0002),
	    ID_URP_SHOT_IFilamentMin	= MAKE_SETTING_ID (CLSID_URP , 0x0003),
	    ID_URP_SHOT_IFilamentMax	= MAKE_SETTING_ID (CLSID_URP , 0x0004),
	    ID_URP_RadOutput		= MAKE_SETTING_ID (CLSID_URP,  0x0005),

	    ID_URP_SF_UAnodeMin		= MAKE_SETTING_ID (CLSID_URP , 0x0101),
	    ID_URP_SF_UAnodeMax		= MAKE_SETTING_ID (CLSID_URP , 0x0102),
	    ID_URP_SF_IFilamentPreMin	= MAKE_SETTING_ID (CLSID_URP , 0x0103),
	    ID_URP_SF_IFilamentPreMax	= MAKE_SETTING_ID (CLSID_URP , 0x0104),
	    ID_URP_SF_IFilamentPreDef	= MAKE_SETTING_ID (CLSID_URP , 0x0105),
	    ID_URP_SF_IFilamentMax	= MAKE_SETTING_ID (CLSID_URP , 0x0106),
	    ID_URP_SF_IAnodeTable	= MAKE_SETTING_ID (CLSID_URP , 0xFF00),

	    ID_URP_BF_UAnodeMin		= MAKE_SETTING_ID (CLSID_URP , 0x0201),
	    ID_URP_BF_UAnodeMax		= MAKE_SETTING_ID (CLSID_URP , 0x0202),
	    ID_URP_BF_IFilamentPreMin	= MAKE_SETTING_ID (CLSID_URP , 0x0203),
	    ID_URP_BF_IFilamentPreMax	= MAKE_SETTING_ID (CLSID_URP , 0x0204),
	    ID_URP_BF_IFilamentPreDef	= MAKE_SETTING_ID (CLSID_URP , 0x0205),
	    ID_URP_BF_IFilamentMax	= MAKE_SETTING_ID (CLSID_URP , 0x0206),
	    ID_URP_BF_IAnodeTable	= MAKE_SETTING_ID (CLSID_URP , 0xFF01),

	    ID_URP_UForceMin		= MAKE_SETTING_ID (CLSID_URP , 0x0301),
	    ID_URP_TForceMax		= MAKE_SETTING_ID (CLSID_URP , 0x0302),
	    ID_URP_TTubeMax		= MAKE_SETTING_ID (CLSID_URP , 0x0303),
	    ID_URP_HeatForceLo		= MAKE_SETTING_ID (CLSID_URP , 0x0304),
	    ID_URP_HeatForceHi		= MAKE_SETTING_ID (CLSID_URP , 0x0305),
	    ID_URP_HeatForceK		= MAKE_SETTING_ID (CLSID_URP , 0x0306),
	    ID_URP_HeatTubeLo		= MAKE_SETTING_ID (CLSID_URP , 0x0307),
	    ID_URP_HeatTubeHi		= MAKE_SETTING_ID (CLSID_URP , 0x0308),
	    ID_URP_HeatTubeK		= MAKE_SETTING_ID (CLSID_URP , 0x0309),

	    ID_URP_UACorrection	    	= MAKE_SETTING_ID (CLSID_URP , 0x030A),
	    ID_URP_IARetention		= MAKE_SETTING_ID (CLSID_URP , 0x030B),
	    ID_URP_HeatDelay		= MAKE_SETTING_ID (CLSID_URP , 0x030C),
	    ID_URP_TExposeMax		= MAKE_SETTING_ID (CLSID_URP , 0x030D),
	    ID_URP_UACorrectionTable	= MAKE_SETTING_ID (CLSID_URP , 0xFF02),

	    ID_URP_DelayParam_Full	= MAKE_SETTING_ID (CLSID_URP , 0x0401),
	    ID_URP_DelayParam_21x30	= MAKE_SETTING_ID (CLSID_URP , 0x0402),
	    ID_URP_DelayParam_18x24	= MAKE_SETTING_ID (CLSID_URP , 0x0403),
	    ID_URP_DelayParam_12x12	= MAKE_SETTING_ID (CLSID_URP , 0x0404),
	//
	//.....................................................................
	//
	    ID_MSC_Version		= MAKE_SETTING_ID (CLSID_MSC , 0x0001),

	    ID_MSC_Scan_IAnodeMax	= MAKE_SETTING_ID (CLSID_MSC , 0xFF00),

	    ID_MSC_Scan_12_Param	= MAKE_SETTING_ID (CLSID_MSC , 0x0201),
	    ID_MSC_Scan_23_Param	= MAKE_SETTING_ID (CLSID_MSC , 0x0202),
	    ID_MSC_ImageProcMask	= MAKE_SETTING_ID (CLSID_MSC , 0x0203),
	    ID_MSC_ImageKX		= MAKE_SETTING_ID (CLSID_MSC , 0x0204),
	    ID_MSC_ImageKY		= MAKE_SETTING_ID (CLSID_MSC , 0x0205),

	//
	//.....................................................................
	//
	    ID_MSC_Diafr_SyncDelay	= MAKE_SETTING_ID (CLSID_IMG , 0x0101),
	    ID_MSC_Diafr_DefCoeff	= MAKE_SETTING_ID (CLSID_IMG , 0x0102),
	    ID_MSC_Diafr_AccelTime	= MAKE_SETTING_ID (CLSID_IMG , 0x0103),
	    ID_MSC_Diafr_Velocity	= MAKE_SETTING_ID (CLSID_IMG , 0x0104),
	    ID_MSC_Diafr_StepPerDec	= MAKE_SETTING_ID (CLSID_IMG , 0x0105),

	    ID_MSC_Scan_SyncDelay	= MAKE_SETTING_ID (CLSID_IMG , 0x0201),
	    ID_MSC_Scan_DefCoeff	= MAKE_SETTING_ID (CLSID_IMG , 0x0202),
	    ID_MSC_Scan_AccelTime	= MAKE_SETTING_ID (CLSID_IMG , 0x0203),
	    ID_MSC_Scan_Velocity	= MAKE_SETTING_ID (CLSID_IMG , 0x0204),
	    ID_MSC_Scan_StepPerDec	= MAKE_SETTING_ID (CLSID_IMG , 0x0205),

	    ID_MSC_Scan_CenterLine	= MAKE_SETTING_ID (CLSID_IMG , 0x0206),

	    ID_MSC_ImageDCBeg		= MAKE_SETTING_ID (CLSID_IMG , 0x0002),
	    ID_MSC_ImageDCEnd		= MAKE_SETTING_ID (CLSID_IMG , 0x0003),
	    ID_MSC_ImageActiveBeg	= MAKE_SETTING_ID (CLSID_IMG , 0x0004),
	    ID_MSC_ImageActiveEnd	= MAKE_SETTING_ID (CLSID_IMG , 0x0005),

	}   ParamId ;

    STDMETHOD_ (void,	      Close)()				  PURE;

    STDMETHOD_ (GResult,   GetValue)(ParamId, void  * pData	,
					      DWord   dDataSize	,
					      DWord * pDataLen	) PURE;

    STDMETHOD_ (GResult,   SetValue)(ParamId, void  * pData	,
					      DWord   dDataSize	) PURE;

    STDMETHOD_ (GResult,    Checkin)(ClassId)			  PURE;
    };
//
//[]------------------------------------------------------------------------[]

    DEFINE_GUID (IID_IRRPSerialBus  ,0x4e8254d8,0x12c6,0x4f96,0x97,0x09,0x04,0x76,0x9f,0x7e,0xda,0xf3);

//[]------------------------------------------------------------------------[]
//
    DECLARE_INTERFACE_ (IRRPSerialBus, IUnknown)
    {
    STDMETHOD_ (GResult,    CreateRRPDevice)(void **	pInterface    ,
					     REFCLSID	rCLSID	      ,
				       const void *	pInstanceId   ,
					     REFIID	rIID	      ) PURE;
    };
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
    DECLARE_INTERFACE_ (IRRPDeviceService, IUnknown)
    {

    STDMETHOD_ (GResult,    CreateRRPDevice)(void **	pInterface    ,
					     REFCLSID	DevClassId    ,
					     void *	DevInstanceId ,
					     REFIID	InterfaceId   )  PURE;

    STDMETHOD_ (void,	     SetEquCallBack)(void (GAPI *) (_U32, _U32)) PURE;

    STDMETHOD_ (GResult,  CreateRRPSettings)(IRRPSettings *&)		 PURE;

#if FALSE

    STDMETHOD_ (GResult, CreateRRPSerialBus)(void **	pInterface    ,
					     REFCLSID	rCLSID	      ,
				       const void *	pInstanceId   )  PURE;

#endif
    };
//
//[]------------------------------------------------------------------------[]
































//[]------------------------------------------------------------------------[]
//
    DECLARE_INTERFACE_ (IRegister, IUnknown)
    {
    DECLARE_INTERFACE  (Handle)
    {
    STDMETHOD_ (void,		 Revoke)() PURE;
    };

    STDMETHOD  (	 RegisterObject)(Handle   *& hRegister	 ,
					 REFCLSID    rClassId	 ,
					 void	  *  pInstanceId ,
					 IUnknown *  pIUnknown	 ) PURE;
    };
//
//[]------------------------------------------------------------------------[]

#if FALSE

    IRRPDEVS_EXPORT HRESULT    GAPI CreateRRPDeviceService  (IRRPDeviceService *&,

From UrpHF Original :

#define REG_GET_ENV_TEMP	0xf8	//���. ����.  ���. �����
#define REG_GET_PWR_TEMP	0xf9	//���. ����.  ������� ��.
#define REG_GET_OIL_TEMP	0xfa	//���. ����. �/� ����� 

#define REG_GET_ANODE_SPIN_FR 0xfb	//���. ������� ����.��.

#define REG_GET_DOSE		0xfc	//���. ����

#define REG_GET_USUPP		0xfe	//���������� ����. ~220�(���)

#define REG_GET_KKM_PWR_TEMP	0xff	//���. ����.  ������� ��.(���)

#endif


    Bool			GAPI ClearShotCount ();
    Bool			GAPI GetShotCount   (FILETIME & fTime, int & cGood, int & cBad, DWord & cQ, DWord & cMAS);

extern "C"
{
    IRRPDEVS_EXPORT GResult    GAPI CreateRRPDeviceService  (IRRPDeviceService *&,
							     LPCTSTR	pHostName);

//    IRRPDEVS_EXPORT GResult    GAPI CreateRRPCallBack	    (IRRPCallBack *&	 );
}

#endif//__R_DEVDEFS_H

/*
//.................

    Scanner :

	Write :	    D6,(m_Status & 0xFE) | 0x00 [, ...]
	Read  :	    81,	D6

	Anser :	    D6, ln, D6, xx, D7, xx, D8, xx, ..., BE, ..., lorc, hics,

//.................

    Calimator :

	Write :	    D6,(m_Status & 0xFE) | 0x01 [, ...]
	Read  :	    81, D7

	Anser :	    D7, ln, D6, xx, D7, xx, D8, xx, ..., BE, ..., lorc, hics,


    [D6,01],[81,D6],...
    [D6,00],[81,D6],...


    Scanner :		    Collimator :

    D6, xxxx VVV0	    D6, xxxx Vxx1
*/


/*
	typedef enum
	{
	    II_ImageSizeX	= 1,
	    II_ImageSizeY	= 2,

	}   ImageInfoId	 ;

	typedef union ImageInfo
	{
	    _I32	    i32Value ;
	    _U32	    u32Value ;
	    float	    r32Value ;
	    void  *	    pValue   ;
	    char  *	    sbValue  ;
	    WCHAR *	    swValue  ;

	}   ImageInfo	 ;



    STDMETHOD_ (DWord,	SelectImageRepairer)(LPCTSTR = NULL)		 PURE;

    STDMETHOD_ (DWord,	       GetImageInfo)(ImageInfoId, ImageInfo *  ) PURE;

    STDMETHOD_ (Bool,	     GetImageStatus)(int *  nPhaseId  ,
					     int *  nPhase    ,
					     int *  nPhaseNum ,
					     int *  nStep     ,
					     int *  nSetpNum  ) PURE;

    STDMETHOD_ (DWord,		  GrabImage)(void * pDst      ,
					     int    nFromRow  ,
					     int    nNumRows  ) PURE;
*/