//[]------------------------------------------------------------------------[]
// RRP Serial interface devices declarations
//
//
// Created by GSh.
//[]------------------------------------------------------------------------[]

#include "g_winfile.h"

#include <stdio.h>
#include <shlwapi.h>
#if (1310 > _MSC_VER)
#include <largeint.h>
#endif

#include "r_rrpdevs.h"
#include "r_ftdibus.h"

#define USE_INDICATOR_STAT  1

#define EMULATE_HEATS	    0

#define TRACE_REQUEST	    0
#define	TRACE_SEND	    1
#define TRACE_SEND_L	    0
#define TRACE_RECV	    0
#define TRACE_RECV_V	    0

static void GetBasePath (TCHAR * pPath)
{
    TCHAR   pBase [MAX_PATH] = {0};


//<<
    {{
	LPCH pEnvString = ::GetEnvironmentStrings ();

	if (pEnvString)
	{
	    pEnvString = (::FreeEnvironmentStrings (pEnvString), NULL);
	}
    }}
//>>





    ::GetEnvironmentVariable (_T("XPROM_ROOT"), pBase, sizeof (pBase) / sizeof (TCHAR));
    strcat (pBase, _T("\\bin"));

    strcpy (pPath, pBase);
};

static void GetNVCalibratePath (TCHAR * pPath)
{
    GetBasePath (pPath);

    strcat (pPath, "\\NVCalibrate");
};


static int getNVCalibrateSet (int iAngle)
{
static const int sLat [19] =
{
    -2,				    // -90
    -2,				    // -80
    -2,				    // -70
    -1,				    // -60
    -1,				    // -50
    -1,				    // -40
    -1,				    // -30
     0,				    // -20
     0,				    // -10
     0,				    //   0
     0,				    //  10
     0,				    //  20
     1,				    //  30
     1,				    //  40
     1,				    //  50
     1,				    //  60
     2,				    //  70
     2,				    //  80
     2,				    //  90
};
    if (iAngle < -90) iAngle = -90;
    if ( 90 < iAngle) iAngle =  90;

    return sLat [(iAngle + 90) / 10];
};

static const TCHAR * getNVCalibrateName (int iSet)
{
static const TCHAR * sLat [5] =
{
    "\\_L90","\\_L45","","\\_R45","\\_R90"
};
    return sLat [iSet + 2];
};

Bool checkNVCalibrateName (const TCHAR * pPath)
{
    WIN32_FIND_DATA finddata; HANDLE hFind; TCHAR pName [MAX_PATH];

    strcpy (pName, pPath);
    strcat (pName, "\\*.CFF");

    if (INVALID_HANDLE_VALUE ==

       (hFind = ::FindFirstFile (pName, & finddata)))
    {	
	return False;
    }
    return (::FindClose (hFind), True);
};

static void GetNVCalibrateUAPath (TCHAR * pPath, Bool bFocus, int iAngle = 0)
{
    int i; iAngle;

    for (i = getNVCalibrateSet (iAngle); i != 0; i += ((i < 0) ? 1 : -1))
    {
	GetNVCalibratePath (pPath);
	strcat (pPath, getNVCalibrateName (i));
	strcat (pPath, (bFocus) ? "\\BF" : "\\SF");

	if (checkNVCalibrateName (pPath))
	{   return; }
    }

    GetNVCalibratePath (pPath);
    strcat  (pPath, getNVCalibrateName (i));
    strcat  (pPath, (bFocus) ? "\\BF" : "\\SF");
};

static Bool BuildFileName (TCHAR * pFileName, LPCTSTR pName)
{
    GetBasePath (pFileName);

    strcat	(pFileName, "\\" );
    strcat	(pFileName, pName);

    return True;
};

void GetFgxOutFileName (TCHAR * pPath)
{
    GetBasePath (pPath);

    strcat (pPath, "\\Logs\\FgxOutRow.dat");
};

/*
static Bool BuildFileName (TCHAR * pFileName, DWord sFileName, LPCTSTR pName)
{
//  ::GetModuleFileName   (NULL, pFileName, sFileName);

    ::GetWindowsDirectory (	 pFileName, sFileName);

    for (TCHAR * p = pFileName + ::lstrlen (pFileName); pFileName < p; p --)
    {
	if (* p == '\\')
	{
	    ::StrCpy  (p + 1, _T("Program Files\\RoentgenProm\\UrpHF Next\\"));

	    ::StrCat  (p    , pName);

//	    ::lstrcpy (p + 1, pName);

	    return True ;
	}
    }
	    return False;
};
*/

#include "r_rrplog.h"

//[]------------------------------------------------------------------------[]
//
class CRRPConfigFile
{
public :
		    CRRPConfigFile ()
		    {
			m_hFile = NULL;
		    };

		   ~CRRPConfigFile ()
		    {
			Close ();
		    };

static	GResult	    LoadUrpParam	    (FILETIME &	fTime	  ,
					     int      & QTubeCur  ,
					     int      & QForceCur );

static	GResult	    SaveUrpParam	    (int        QTubeCur  ,
					     int	QForceCur );

static  GResult	    LoadMM2Param	    (int      & PressHigh );
static	GResult	    SaveMM2Param	    (int        PressHigh );

protected :

	GResult	    Create		    ();

	GResult	    Save		    ();

	void	    Close		    ();

protected :

	HANDLE		    m_hFile	;

#pragma pack (_M_PACK_LONG)

	struct
	{
	struct
	{
	    FILETIME	    fTime	;

	    int		    QTubeCur    ;
	    int		    QForceCur   ;

	}		    Urp_Sav	;

	struct
	{
	    int		    PressHigh	;

	}		    MM2_Sav	;

	}		    m_Body	;

#pragma pack ()
};

GResult CRRPConfigFile::Create ()
{
    GResult dResult		;
    TCHAR   pFileName [MAX_PATH];

    BuildFileName (pFileName, "CRRPConfig.cfg");

    HANDLE  hFile = ::CreateFile (pFileName,
				  GENERIC_READ | GENERIC_WRITE,
				  0,
				  NULL,
				  OPEN_ALWAYS,
				  FILE_ATTRIBUTE_NORMAL,
				  NULL);

    if (INVALID_HANDLE_VALUE == hFile)
    {
	return ::GetLastError ();
    }
	dResult = ERROR_SUCCESS;

	m_hFile = hFile;

	::memset (& m_Body, 0, sizeof (m_Body));
	m_Body.MM2_Sav.PressHigh = (5 * 2);

    if (ERROR_ALREADY_EXISTS != ::GetLastError ())
    {
    if (ERROR_SUCCESS != 
       (dResult	       = Save ()))
    {
	Close  ();
	return dResult;
    }
    }
	       ::SetFilePointer (m_hFile, 0, NULL, FILE_BEGIN);	
	return ::ReadFile	(m_hFile, & m_Body, sizeof (m_Body), & dResult, NULL) ? ERROR_SUCCESS
										      : ::GetLastError ();
};

GResult CRRPConfigFile::Save ()
{
    GResult dResult;

	   ::SetFilePointer (m_hFile, 0, NULL, FILE_BEGIN);
    return ::WriteFile	    (m_hFile, & m_Body, sizeof (m_Body), & dResult, NULL) ? ERROR_SUCCESS
										  : ::GetLastError ();
};

void CRRPConfigFile::Close ()
{
    if (m_hFile)
    {
	::CloseHandle (m_hFile);
		       m_hFile = NULL;
    }
};

GResult CRRPConfigFile::LoadUrpParam (FILETIME & fTime	   ,
				      int      & QTubeCur  ,
				      int      & QForceCur )
{
    GResult	    dResult ;
    CRRPConfigFile  cfg	    ;

    if (ERROR_SUCCESS ==
       (dResult	       = cfg.Create ()))
    {
	fTime	  = cfg.m_Body.Urp_Sav.fTime     ;
	QTubeCur  = cfg.m_Body.Urp_Sav.QTubeCur  ;
	QForceCur = cfg.m_Body.Urp_Sav.QForceCur ;

		    cfg.Close ();
    }
	return dResult;
};

GResult CRRPConfigFile::SaveUrpParam (int QTubeCur, int QForceCur)
{
    GResult	    dResult ;
    CRRPConfigFile  cfg	    ;

    if (ERROR_SUCCESS ==
       (dResult	       = cfg.Create ()))
    {
	::GetSystemTimeAsFileTime
     (& cfg.m_Body.Urp_Sav.fTime);

	cfg.m_Body.Urp_Sav.QTubeCur  = QTubeCur ;
	cfg.m_Body.Urp_Sav.QForceCur = QForceCur;

	dResult	= cfg.Save  ();
		  cfg.Close ();
    }
	return dResult;
};
//
//[]------------------------------------------------------------------------[]
//
GResult CRRPConfigFile::LoadMM2Param (int & PressHigh )
{
    GResult	    dResult ;
    CRRPConfigFile  cfg	    ;

    if (ERROR_SUCCESS ==
       (dResult	       = cfg.Create ()))
    {
	PressHigh = cfg.m_Body.MM2_Sav.PressHigh;

		    cfg.Close ();
    }
	return dResult;
};

GResult CRRPConfigFile::SaveMM2Param (int PressHigh)
{
    GResult	    dResult ;
    CRRPConfigFile  cfg	    ;

    if (ERROR_SUCCESS ==
       (dResult	       = cfg.Create ()))
    {
	cfg.m_Body.MM2_Sav.PressHigh = PressHigh;

	dResult	= cfg.Save  ();
		  cfg.Close ();
    }
	return dResult;
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
namespace LOCAL
{
void GAPI RRPSetParam (IRRPDevice * pInterface, DWord dParamId, DWord dParamValue)
{
    IRRPDevice::Param	Param;

    if (pInterface)
    {{
	Param.Init (dParamId, dParamValue);

	pInterface ->SetParam (1, & Param);
    }}
};
}
//
//[]------------------------------------------------------------------------[]
//
class ShotCountFile
{
public :
		    ShotCountFile ()
		    {
			m_hFile = NULL;

			memset (& m_fData, 0, sizeof (m_fData));
			m_fData.uSize	    = sizeof (m_fData);
		    };

		   ~ShotCountFile ()
		    {
			Close ();
		    };

	Bool	    Create	    ();

	void	    Close	    () 
	{ 
	    if (m_hFile)
	    {
		m_hFile = (::CloseHandle (m_hFile), NULL);
	    }
	};

	void	    Load	    ()
	{
	    DWord   dTemp;

	    ::SetFilePointer (m_hFile, 0, NULL, FILE_BEGIN);
	    ::ReadFile	     (m_hFile, & m_fData, sizeof (m_fData), & dTemp, NULL);
	};

	void	    Save	    ()
	{
	    DWord   dTemp;

	    ::SetFilePointer (m_hFile, 0, NULL, FILE_BEGIN);
	    ::WriteFile	     (m_hFile, & m_fData, sizeof (m_fData), & dTemp, NULL);
	};

#pragma pack (1)

	struct Data
	{
	    DWord	uSize	;
	    FILETIME	fTime	;
	    DWord	cGood	;
	    DWord	cBad	;
	    DWord	cQ	;
	    DWord	cMAS	;
	};

#pragma pack ()

	HANDLE	    m_hFile	;
	Data	    m_fData	;
};

Bool ShotCountFile::Create ()
{
    TCHAR   pFileName [MAX_PATH];

    BuildFileName (pFileName, "CRRPShotCnt.cfg");

    m_hFile	  = ::CreateFile (pFileName,
				  GENERIC_READ | GENERIC_WRITE,
				  FILE_SHARE_READ | FILE_SHARE_WRITE,
				  NULL,
				  OPEN_ALWAYS,
				  FILE_ATTRIBUTE_NORMAL,
				  NULL);

    if (INVALID_HANDLE_VALUE == m_hFile)
    {
	return (m_hFile = NULL, False);
    }

    if (ERROR_ALREADY_EXISTS != ::GetLastError ())
    {
	::GetSystemTimeAsFileTime (& m_fData.fTime);

	Save ();
    }
    else
    {
	Load ();
    }
	return True;
};

Bool GAPI GetShotCount (FILETIME & fTime, int & cGood, int & cBad, DWord & cMAS, DWord & cQ)
{
    ShotCountFile   cnt ;

    cnt.Create ();

    fTime = cnt.m_fData.fTime;
    cGood = cnt.m_fData.cGood;
    cBad  = cnt.m_fData.cBad ;
    cQ	  = cnt.m_fData.cQ   ;
    cMAS  = cnt.m_fData.cMAS ;

    return True;
};

Bool GAPI ClearShotCount ()
{
    DWord		tmp  ;
    ShotCountFile	cnt  ;
    ShotCountFile::Data fData;
    SYSTEMTIME		tTime;

    TCHAR   pBuf    [MAX_PATH];

    cnt.Create ();

    if ((0 < cnt.m_fData.cGood) || (0 < cnt.m_fData.cBad))
    {
	memcpy (& fData, & cnt.m_fData, sizeof (cnt.m_fData));

	memset (& cnt.m_fData,	     0, sizeof (cnt.m_fData));
	cnt.m_fData.uSize	      = sizeof (cnt.m_fData) ;

	::GetSystemTimeAsFileTime  (& cnt.m_fData.fTime);

	cnt.Save   ();

	::FileTimeToLocalFileTime (& fData.fTime, & fData.fTime);
	::FileTimeToSystemTime	  (& fData.fTime, & tTime);

	::wsprintf (pBuf, "\r\n%02d/%02d/%02d %02d:%02d.%02d : %d/%d MAS = %d, Q = %d",

	(tTime.wYear % 100), tTime.wMonth, tTime.wDay, tTime.wHour, tTime.wMinute, tTime.wSecond,

	 fData.cGood, fData.cBad, fData.cMAS, fData.cQ);

	::SetFilePointer (cnt.m_hFile, 0, NULL, FILE_END);
	::WriteFile	 (cnt.m_hFile, pBuf, (DWord) ::strlen (pBuf), & tmp, NULL);
    }
    return True;
};

static Bool GetShotCount (int & cGood, int & cBad)
{
    FILETIME	fTime;
    DWord	cQ   ;
    DWord	cMAS ;

    return GetShotCount (fTime, cGood, cBad, cQ, cMAS);
};

static Bool IncShotCount (Bool bGood, DWord dIncMAS, DWord dIncQ)
{
    ShotCountFile   cnt ;

    cnt.Create ();

    if (bGood) cnt.m_fData.cGood ++;
    else       cnt.m_fData.cBad  ++;

    cnt.m_fData.cQ   += dIncQ	;
    cnt.m_fData.cMAS += dIncMAS ;

    cnt.Save   ();

    return True;
};

using LOCAL::RRPSetParam;
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
class CRRPSerialBus
{
public :
			CRRPSerialBus	();

	void		MM2IndEnable	(Bool bEnable);

	void		MM2IndSetValue	(DWord dValue);

	typedef enum
	{
	    Off		    = 0,
	    On		    = 1,

	}   LineStatus ;

	typedef enum
	{
	    RecvData	    =  0,
	    RecvTimeout	    =  1,

	}   RecvReason ;

	typedef enum
	{
	    RecvBreak	    =  0,
	    RecvContinue    =  1,
	
	}   RecvResult ;

	typedef enum
	{
	    SendOOB	    =  0,
	    SendData	    =  1,

	}   SendReason ;

	typedef enum
	{
	    NoMoreData	    =  0,	// Continue build send packet
	    QueueForSend    =  1,	// Flush all output
	    QueueForRecv    =  2,	// Flush all output and wait for recv

	}   SendResult ;

//	struct DeviceEntry;

	struct DeviceEntry
	{
	typedef void	    (GAPI * LineCallBack)(DeviceEntry *	     ,
						  LineStatus Status  ,
						  GResult	     );

	typedef SendResult  (GAPI * SendCallBack)(DeviceEntry *	     ,
						  SendReason Reason  ,
						  Byte  *&   pTail   ,
					    const Byte  *    pHigh   );

	typedef RecvResult  (GAPI * RecvCallBack)(DeviceEntry *	     ,
						  RecvReason Reason  ,
					    const Byte  *&   pHead   ,
					    const Byte  *    pTail   );

	typedef void	    (GAPI * IdleCallBack)(DeviceEntry *	     );

	    typedef TSLink <DeviceEntry, 0>    Link;
	    typedef TXList <DeviceEntry,
		    TSLink <DeviceEntry, 0> >  List;

	    Link	    LLink	    ;

	    _U32	    uIdleFlags	    ;

	    DWord	    uOrderId	    ;
	    void *	    pContext	    ;
	    LineCallBack    pLineCallBack   ;
	    SendCallBack    pSendCallBack   ;
	    RecvCallBack    pRecvCallBack   ;
	    IdleCallBack    pIdleCallBack   ;
	};

	void		Connect		(DeviceEntry *);

	void		Disconnect	(DeviceEntry *);

	void		On_Idle		();

	void		On_Indicator	(Byte *&);

private :

	GIrp *		Co_Recv		(GIrp *, void *);

	GIrp *		Co_Send		(GIrp *, void *);

	void		Do_IoTransact	(GIrp *);

	GIrp *		Co_Work		(GIrp *, void *);

	GIrp *		Co_Create	(GIrp *, void *);

	GIrp *		Co_StartIo	(GIrp *, void *);

	void		StartIo		();
//
//.......................................................
//
private :

	TInterlocked <ULONG>  m_MM2IndRefCount	;
	TInterlocked <ULONG>  m_MM2IndValue	;

	GFtdiFile	    m_FtdiFile		;

	GIrp *		    m_pIoIrp		;
	DWord		    m_uBusStatus	;

	Bool		    m_DeSendOOB		;
	DeviceEntry *	    m_DeSendCur		;
	DeviceEntry *	    m_DeRecvCur		;
	DeviceEntry::List   m_DeList		;

	DWord		    m_dRecvTimeout	;
	GTimeout	    m_tRecvTimeout	;

	Byte *		    m_pRecvHead		;
	Byte *		    m_pRecvTail		;

	Byte *		    m_pSendTail		;

	Byte		    m_pRecvBuf [256 + 4];
	Byte		    m_pSendBuf [512 + 8];
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
CRRPSerialBus::CRRPSerialBus ()
{
    m_pIoIrp	    = NULL ;

    m_uBusStatus    = False;

    m_DeSendOOB	    = True ;
    m_DeSendCur     =
    m_DeRecvCur     = NULL ;

    m_dRecvTimeout  = 40   ;

    m_pRecvHead	    =
    m_pRecvTail	    = m_pRecvBuf ;
    m_pSendTail	    = m_pSendBuf ;
};

void CRRPSerialBus::Connect (CRRPSerialBus::DeviceEntry * e)
{
    TXList_Iterator <DeviceEntry> I;

    for (I  = m_DeList.head (); I.next ();)
    {
	if (e ->uOrderId < I.next () ->uOrderId)
	{
	    break;
	}
	I <<= m_DeList.getnext (I.next ());
    }
	      m_DeList.insert  (I,	e);

    if (NULL == m_pIoIrp)
    {
	StartIo ();

printf ("CRRPSerialBus->StartIo...\n");
    }
    else
    if (m_uBusStatus & 0x01)
    {
       (e ->pLineCallBack)(e, On, ERROR_SUCCESS);
    }
};

void CRRPSerialBus::Disconnect (CRRPSerialBus::DeviceEntry * e)
{
    TXList_Iterator <DeviceEntry> I;

    if (m_DeList.contains  (e))
    {
	if ((m_DeSendCur == e) && ((m_DeSendCur = m_DeSendCur ->LLink.next ()) == e))
	{
	     m_DeSendCur = NULL;
	}
	if ((m_DeRecvCur == e))
	{
	     m_DeRecvCur = NULL;
	}

	m_DeList.extract (  e);
    }

    if (m_DeList.empty () && m_pIoIrp)
    {
printf ("CRRPSerialBus->CancelIo...\n");

	while ((volatile DWord &) m_uBusStatus && (0x80000000 != m_MM2IndValue.GetValue ()))
	{
 	    if (TCurrent () ->GetCurApartment () ->PumpInputWork ())
	    {
		TCurrent () ->GetCurApartment () ->Work ();

		continue;
	    }
		TCurrent () ->GetCurApartment () ->WaitInputWork (20);
	}

	CancelIrp (m_pIoIrp);

	while (NULL != (volatile void *) m_pIoIrp)
	{
 	    if (TCurrent () ->GetCurApartment () ->PumpInputWork ())
	    {
		TCurrent () ->GetCurApartment () ->Work ();

		continue;
	    }
		TCurrent () ->GetCurApartment () ->WaitInputWork (20);
	}
    }
};
//
//[]------------------------------------------------------------------------[]
//
GIrp * CRRPSerialBus::Co_Recv (GIrp * pIrp, void *)
{
    if (ERROR_SUCCESS != pIrp ->GetCurResult ())
    {
	m_DeRecvCur = NULL;

	return pIrp;
    }

    if (m_DeRecvCur)
    {
	    m_pRecvTail  += (DWord) pIrp ->GetCurResultInfo ();

	if (RecvContinue == (m_DeRecvCur ->pRecvCallBack)(

		 m_DeRecvCur		 ,

	       ((0 == (DWord) pIrp ->GetCurResultInfo ()) && m_tRecvTimeout.Occured ()) 

		? RecvTimeout : RecvData , 
		(const Byte *&) m_pRecvHead, (const Byte *) m_pRecvTail))
	{
	    if (m_pRecvBuf  < m_pRecvHead)
	    {
	    if (m_pRecvHead < m_pRecvTail)
	    {
		::memmove (m_pRecvBuf, m_pRecvHead, m_pRecvTail - m_pRecvHead);
	    }
		m_pRecvTail = m_pRecvBuf  +
			     (m_pRecvTail - m_pRecvHead);
		m_pRecvHead = m_pRecvBuf ;
	    }
	    goto lContinueRead	    ;
	}
	    goto lStartNextTransact ;
    }
    else
    {
	if (! m_tRecvTimeout.Occured ())
	{
	    goto lContinueRead	    ;
	}
	    goto lStartNextTransact ;
    }

lContinueRead :

    SetCurCoProc <CRRPSerialBus, void *>
    (pIrp, & CRRPSerialBus::Co_Recv, this, NULL);

    m_FtdiFile.QueueForRecv (pIrp, m_pRecvTail, (DWord)(m_pRecvHead + sizeof (m_pRecvBuf) - m_pRecvTail));

    return NULL;

lStartNextTransact :

    m_DeRecvCur = NULL;

    m_tRecvTimeout.Disactivate ();

    Do_IoTransact (pIrp);

    return NULL;
};

GIrp * CRRPSerialBus::Co_Send (GIrp * pIrp, void *)
{
    if (ERROR_SUCCESS != pIrp ->GetCurResult ())
    {
	return pIrp;
    }

    m_tRecvTimeout.Activate ((m_DeRecvCur) ? m_dRecvTimeout : 10);

    SetCurCoProc <CRRPSerialBus, void *>
    (pIrp, & CRRPSerialBus::Co_Recv, this, NULL);

    m_FtdiFile.QueueForRecv (pIrp, m_pRecvTail, (DWord)(m_pRecvHead + sizeof (m_pRecvBuf) - m_pRecvTail));

    return NULL;
};

void CRRPSerialBus::MM2IndEnable (Bool bEnable)
{
    if (bEnable)
    {
    if (0 == m_MM2IndRefCount ++)
    {
	m_MM2IndValue.Exchange (0x00);
    }
    }
    else
    {
    if (0 == -- m_MM2IndRefCount)
    {
	m_MM2IndValue.Exchange (0x00);
    }
    }
};

void CRRPSerialBus::MM2IndSetValue (DWord dValue)
{
    if (0 < m_MM2IndRefCount)
    {{

    if (0x80000000 & dValue)
    {
	dValue = 0x04;
    }
    else
    if (0x40000000 & dValue)
    {
	dValue = 0x02;
    }
    else
    {
	dValue = (0 == (0x3FFFFFFF & dValue) ) ? 0x01 : 0x00;
    }
	m_MM2IndValue.Exchange (dValue);
    }}
};

void CRRPSerialBus::On_Indicator (Byte *& pTail)
{
    UINT  uValue;

    if (0x80000000 != (uValue = m_MM2IndValue.Exchange (0x80000000)))
    {
	* pTail ++ = 0xC7;
	* pTail ++ = LOBYTE(LOWORD(uValue));
    }
};

void CRRPSerialBus::Do_IoTransact (GIrp * pIrp)
{
    if (! pIrp ->Cancelled ())
    {
	DeviceEntry * e;

	m_pRecvHead    =
	m_pRecvTail    = m_pRecvBuf ;
	m_pSendTail    = m_pSendBuf ;

      * m_pSendTail ++ = 0x00;
      * m_pSendTail ++ = 0x00;

    if (NULL != (e = m_DeSendCur))
    {
    if (	     m_DeSendOOB)
    {
	On_Indicator (m_pSendTail);

lRepeatSendOOB :

	switch ((e ->pSendCallBack)(e, SendOOB , m_pSendTail, m_pSendBuf + sizeof (m_pSendBuf)))
	{
	case QueueForSend :
	    m_DeSendCur  = e;
	    goto lComplete  ;

	case QueueForRecv :
	    m_DeSendCur  =
	    m_DeRecvCur  = e;
	    goto lComplete  ;

//	case NoMoreData :
	default :
	    e		 = e ->LLink.next ();
	if (m_DeSendCur != e)
	{
	    goto lRepeatSendOOB;
	}
	    break;
	};
    }

lRepeatSendData :

	switch ((e ->pSendCallBack)(e, SendData, m_pSendTail, m_pSendBuf + sizeof (m_pSendBuf)))
	{
	case QueueForSend :
	    m_DeSendCur	 = e;
	    break;

	case QueueForRecv :
	    m_DeSendCur	 = e ->LLink.next ();
	    m_DeRecvCur	 = e;
	    break;

//	case NoMoreData :
	default :
	    e		 = e ->LLink.next ();
        if (m_DeSendCur != e)
	{
	    goto lRepeatSendData;
	}
	    break;
	};

lComplete :

	if (m_pSendTail == m_pSendBuf + 2)
	{
	    m_pSendTail	 = m_pSendBuf;
	}

	SetCurCoProc  <CRRPSerialBus, void *>
       (pIrp, & CRRPSerialBus::Co_Send, this, NULL);

#if (TRACE_SEND_L)
{{  const Byte * p = m_pSendBuf;
printf ("...Send (%d){", (int)(m_pSendTail - m_pSendBuf));
    for (; p < m_pSendTail; p ++)
    {
printf (" %02X", * p);
    }
printf ("}\n");
}}
#endif

	m_FtdiFile.QueueForSend (pIrp, m_pSendBuf, (DWord)(m_pSendTail - m_pSendBuf));

	return;
    }
    else
    {
	On_Indicator (m_pSendTail);

	m_FtdiFile.QueueForSend (pIrp, m_pSendBuf, (DWord)(m_pSendTail - m_pSendBuf));

	return;
    }
    }
	CompleteIrp (pIrp);
};
//
//[]------------------------------------------------------------------------[]
//
GIrp * CRRPSerialBus::Co_Work (GIrp * pIrp, void *)
{
    {
	m_uBusStatus	&= (~0x01);

	m_FtdiFile.Close ();

    for (DeviceEntry * e = m_DeSendCur = m_DeList.first (); e; e = m_DeList.getnext (e))
    {
       (e ->pLineCallBack)(e, Off, pIrp ->GetCurResult ());
    }
    }
	return pIrp;
};

GIrp * CRRPSerialBus::Co_Create  (GIrp * pIrp, void *)
{
    if (pIrp ->Cancelled ())
    {
	return pIrp;
    }

    if ((ERROR_SUCCESS != m_FtdiFile.Create ("USB <-> Serial"	, True))
    &&  (ERROR_SUCCESS != m_FtdiFile.Create ("FT230X Basic UART", True)))
    {

    for (DeviceEntry * e = m_DeSendCur = m_DeList.first (); e; e = m_DeList.getnext (e)) 
    {
	(e ->pLineCallBack)(e, Off, ERROR_FILE_NOT_FOUND);
    }

printf ("CRRPSerialBus::CreateLoop\n");

	SetCurCoProc <CRRPSerialBus, void *>
       (pIrp, & CRRPSerialBus::Co_Create, this, NULL);

	QueueIrpForComplete (pIrp, 5000);
	return NULL;
    }

printf ("CRRPSerialBus->Work\n");

	m_uBusStatus	|= ( 0x01);

    for (DeviceEntry * e = m_DeSendCur = m_DeList.first (); e; e = m_DeList.getnext (e)) 
    {
	(e ->pLineCallBack)(e, On, ERROR_SUCCESS);
    }

    SetCurCoProc <CRRPSerialBus, void *>
   (pIrp, & CRRPSerialBus::Co_Create, this, NULL);

    SetCurCoProc <CRRPSerialBus, void *>
   (pIrp, & CRRPSerialBus::Co_Work  , this, NULL);

    Do_IoTransact (pIrp);
    return NULL;
};

GIrp * CRRPSerialBus::Co_StartIo (GIrp * pIrp, void *)
{
printf ("CRRPSerialBus->Break...\n");

    if (m_pIoIrp == pIrp)
    {
	m_pIoIrp  = NULL;
    }
	return pIrp;
};

void CRRPSerialBus::StartIo ()
{
    m_pIoIrp	       = new (4096 * 1) GIrp;

    SetCurCoProc <CRRPSerialBus, void *>
   (m_pIoIrp, & CRRPSerialBus::Co_StartIo, this, NULL);

    SetCurCoProc <CRRPSerialBus, void *>
   (m_pIoIrp, & CRRPSerialBus::Co_Create , this, NULL);

    QueueIrpForComplete (m_pIoIrp);
};

void CRRPSerialBus::On_Idle ()
{
    for (DeviceEntry * e = m_DeList.first (); e; e = m_DeList.getnext (e))
    {
       (e ->pIdleCallBack)(e);
    }
};
//
//[]------------------------------------------------------------------------[]


class CRRPDeviceService;

static void MM2IndEnable    (Bool );

static void MM2IndSetValue  (DWord);

	CRRPDeviceService * s_pRRPService   = NULL;

static	IRRPSettings *	    GetIRRPSettings ();

//[]------------------------------------------------------------------------[]
//
#define	_WREG_MASK(him,hir,lom,lor) ((((_U32)him) << 24) |  \
				     (((_U32)lom) << 16) |  \
				     (((_U32)hir) <<  8) |  \
				     (((_U32)lor) <<  0))

#define	_WREG_HIREG(d)		    HIBYTE(LOWORD(d))
#define	_WREG_LOREG(d)		    LOBYTE(LOWORD(d))
#define	_WREG_HIMASK(d)		    HIBYTE(HIWORD(d))
#define _WREG_LOMASK(d)		    LOBYTE(HIWORD(d))
//
//[]------------------------------------------------------------------------[]


//[]------------------------------------------------------------------------[]
//
class CRRPCallBack : public IRRPCallBack
{
public :
			CRRPCallBack ()
			{
			    m_pCallBack = NULL;
			    m_pContext  = NULL;
			};

		       ~CRRPCallBack ()
			{};

	void		SetHandler  (void (GAPI * pCallBack)(void *, const void *, DWord, DWord), void * pContext)
			{
			    m_pCallBack = pCallBack;
			    m_pContext  = pContext;			    
			};

	LONG		GetRefCount () const { return m_cRefCount;};

protected :

    STDMETHOD  (     QueryInterface)(REFIID, void **);

    STDMETHOD_ (ULONG,	     AddRef)();

    STDMETHOD_ (ULONG,	    Release)();

    STDMETHOD_ (void,	     Invoke)(const void * hICallBack ,
				     const 
				     IRRPProtocol::DataPack *);
protected :

	TInterlocked <LONG>	    m_cRefCount;

	void		    (GAPI * m_pCallBack)(void *, const void *, DWord, DWord);
	void *			    m_pContext ;
};
//
//[]------------------------------------------------------------------------[]
//
HRESULT CRRPCallBack::QueryInterface (REFIID, void ** pi)
{
    if (pi)
    {
      * pi = NULL;
    }
	return ERROR_NOT_SUPPORTED;
};

ULONG CRRPCallBack::AddRef ()
{
	return ++ m_cRefCount;
};

ULONG CRRPCallBack::Release ()
{
	return -- m_cRefCount;;
};

void CRRPCallBack::Invoke (const void * hICallBack	 ,
			   const IRRPProtocol::DataPack *
					pDataPack	 )
{
    Word    uLength = getBigEndianW (pDataPack, offsetof (IRRPProtocol::DataPack, uLength));
    Word    uType   = getBigEndianW (pDataPack, offsetof (IRRPProtocol::DataPack, uType  ));
    IRRPProtocol::ParamPack::Entry *
	    pEntry;

    if (0 == uType)
    {
	pEntry = (IRRPProtocol::ParamPack::Entry *)(pDataPack + 1);

	while ((Byte *) pEntry < ((Byte *) pDataPack + uLength))
	{
	    (m_pCallBack)(m_pContext, hICallBack, 
				      getBigEndianD (pEntry, offsetof (IRRPProtocol::ParamPack::Entry, uParamId)),
				      getBigEndianD (pEntry, offsetof (IRRPProtocol::ParamPack::Entry, uValue  )));
	    pEntry ++;
	}
    }
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
#define	FREG_ReqEnable	    0x0001  // uValue can  be send to device
#define FREG_AckEnable	    0x0002  // uValue can  be recv from device
#define FREG_Param	    0x0004  // uValue is some value 0 -> mask

#define FREG_ReqToSync	    0x0100  // uValue must be synced
#define FREG_ReqToSend	    0x0200  // uValue must be send to device
#define FREG_Changed	    0x0400  // uValue has been changed by AckValue
#define FREG_Duty	    0x0800  // uValue undefined

//	FREG_AcqEnable | FREG_ReqEnable mask :
//
//	00 : software virtual register
//	01 : hardware write only command register
//	10 : hardware read  only status  register
//	11 : hardware rdwr	 status  register
//
class CRRPSerialDevice
{
	friend class CRRPSerialBus;

public :
		 CRRPSerialDevice (_U32 uDeviceId);

	operator CRRPSerialBus::DeviceEntry * () { return & m_BusEntry;};

	_U32		GetRegStatus	    () const { return m_RegStatus.uValue;};

virtual	void		Reset		    ();

virtual	void		Initialize	    ();

	void		Connect		    (CRRPSerialBus &);

	void		Disconnect	    (CRRPSerialBus &);

protected :

virtual	void		DispatchAPC	    (DWord dCtrlCode, int nArgs, void ** pArgs, HANDLE hResume);

public :

static	void	 GAPI	DispatchAPC	    (		      int nArgs, void ** pArgs, HANDLE hResume);

protected :

	struct Register;

public :

virtual	void	        OnAckValue	    (Register &, _U32 v);

	void		OnAckBits	    (Register &, _U32 c, _U32 s);

	void		SetRegValue	    (Register &, _U32 v);

	void		SetRegBits	    (Register &, _U32 c, _U32 s);

	void		SetRegStatus	    (_U32 c, _U32 s) { SetRegBits (m_RegStatus, c, s);};

	void		AckRegStatus	    (_U32 c, _U32 s) 
			{
			    OnAckBits  (m_RegStatus, c, s);

//printf ("%08X<-%08X.%08X\n", m_RegStatus.uValue, c, s);
			};

protected :

	Bool		LockAcquire	    () { return m_PLock.LockAcquire ();};

	void	        LockRelease	    () {	m_PLock.LockRelease ();};

#pragma pack (_M_PACK_VPTR)

	struct XferBuf
	{
#pragma pack (_M_PACK_BYTE)

	struct Pack
	{
	    Byte	    bPackLen	    ;
	    Byte	    bPackId	    ;
	//  Byte	    bPack [bPackLen];
	};

#pragma pack (_M_PACK_VPTR)

	union
	{
	    Byte *	    pHead	    ;
	    Pack *	    pHeadPack	    ;
	};
	union
	{
	    Byte *	    pTail	    ;
	    Pack *	    pTailPack	    ;
	};
	    Byte	    pBase [64]	    ;


			    XferBuf		()
			    {
				Purge ();
			    };

	    void	    Purge		()
			    {
				pHead =
				pTail = pBase;
			    };

	    void	    Put			(const Pack *);
	    void	    PutOver		(const Pack *);

	    const Pack *    Get			(const Pack *);
	};

	struct Register
	{
	    typedef TSLink <Register, 0>    Link;
	    typedef TXList <Register,
		    TSLink <Register, 0> >  List;

	    Link	    LLink	    ;
	    _U32	    uFlags	    ;
	    _U32	    uCodeMask_1_0   ;	// Encode/Decode serial mask 1,0 bytes
	    _U32	    uCodeMask_3_2   ;	// Encode/Decode serial mask 3,2 bytes
	    _U32	    uValue	    ;

	    Register *	    DefineSWReg_16	(_U32 uCodeMask)
			    {
				uFlags	      =	FREG_Duty;
				uCodeMask_1_0 = uCodeMask;
				uCodeMask_3_2 =		0;
				uValue	      =		0;
				return this;
			    };

	    Register *	    DefineSWReg_32	(_U32 uCodeMask, _U32 uCodeMaskHi)
			    {
				uFlags	      =	FREG_Duty  ;
				uCodeMask_1_0 = uCodeMask  ;
				uCodeMask_3_2 =	uCodeMaskHi;
				uValue	      =		  0;
				return this;
			    };

	    Register *	    DefineWOReg_16	(_U32 uCodeMask)
			    {
				uFlags	      = FREG_Duty				| FREG_ReqEnable;
				uCodeMask_1_0 = uCodeMask;
				uCodeMask_3_2 =		0;
				uValue	      =		0;
				return this;
			    };

	    Register *	    DefineWOParam_16	(_U32 uCodeMask)
			    {
				uFlags	      = FREG_Duty | FREG_Param			| FREG_ReqEnable;
				uCodeMask_1_0 = uCodeMask;
				uCodeMask_3_2 =		0;
				uValue	      =		0;
				return this;
			    };

	    Register *	    DefineRWReg_16	(_U32 uCodeMask)
			    {
				uFlags	      = FREG_Duty	       | FREG_AckEnable | FREG_ReqEnable;
				uCodeMask_1_0 = uCodeMask;
				uCodeMask_3_2 =		0;
				uValue	      =		0;
				return this;
			    };

	    Register *	    DefineROReg_16	(_U32 uCodeMask)
			    {
				uFlags	      = FREG_Duty	       | FREG_AckEnable;
				uCodeMask_1_0 = uCodeMask;
				uCodeMask_3_2 =		0;
				uValue	      =		0;
				return this;
			    };

	    Register *	    DefineROReg_32	(_U32 uCodeMask, _U32 uCodeMaskHi)
			    {
				uFlags	      = FREG_Duty	       | FREG_AckEnable;
				uCodeMask_1_0 = uCodeMask  ;
				uCodeMask_3_2 =	uCodeMaskHi;
				uValue	      =		  0;
				return this;
			    };

	    Register *	    DefineRWParam_16	(_U32 uCodeMask)
			    {
				uFlags	      = FREG_Duty | FREG_Param | FREG_AckEnable | FREG_ReqEnable;
				uCodeMask_1_0 = uCodeMask;
				uCodeMask_3_2 =		0;
				uValue	      =		0;
				return this;
			    };

	    Register *	    DefineROParam_16	(_U32 uCodeMask)
			    {
				uFlags	      = FREG_Duty | FREG_Param | FREG_AckEnable;
				uCodeMask_1_0 = uCodeMask;
				uCodeMask_3_2 =		0;
				uValue	      =		0;
				return this;
			    };

	    Register *	    DefineROParam_32	(_U32 uCodeMask, _U32 uCodeMaskHi)
			    {
				uFlags	      = FREG_Duty | FREG_Param | FREG_AckEnable;
				uCodeMask_1_0 = uCodeMask  ;
				uCodeMask_3_2 =	uCodeMaskHi;
				uValue	      =		  0;
				return this;
			    };

	    _U32	    OnAckValue	    (_U32 v);

//	    _U32	    SetAckValue	    (_U32 v) { return OnAckValue (v);};

//	    _U32	    SetReqValue	    (_U32 v);

//	    _U32	    SetReqBits	    (_U32 c, _U32 s);

	    _U32	    Refresh	    ();

	    void	    PutReqValue	    (	   Byte *& pTail);

	    Bool	    DecodeAckValue  (	   _U32 &  Value,
					     const Byte *  pHead,
					     const Byte *  pTail);
	};

#pragma pack ()

virtual	void			    OnIdle  ();

static	void GAPI		    OnIdle  (CRRPSerialBus::DeviceEntry *);

virtual void			    OnLine  (CRRPSerialBus::LineStatus	 ,
					     GResult	   Result	 );

static	void GAPI		    OnLine  (CRRPSerialBus::DeviceEntry *,
					     CRRPSerialBus::LineStatus	 ,
					     GResult	   Result	 );

virtual CRRPSerialBus::SendResult   PutSendOOBRequest
					    (	   Byte *& pTail	 ,
					     const Byte *  pHigh	 );

virtual CRRPSerialBus::SendResult   PutSendRequest
					    (	   Byte *& pTail	 ,
					     const Byte *  pHigh	 );

virtual	CRRPSerialBus::SendResult   PutRecvRequest
					    (	   Byte *& pTail	 ,
					     const Byte *  pHigh	 );

virtual CRRPSerialBus::SendResult   OnSend  (CRRPSerialBus::SendReason	 ,
						   Byte *& pTail	 ,
					     const Byte *  pHigh	 );
static	CRRPSerialBus::SendResult 
	GAPI			    OnSend  (CRRPSerialBus::DeviceEntry *,
					     CRRPSerialBus::SendReason	 ,
						   Byte *& pTail	 ,
					     const Byte *  pHigh	 );

virtual	void			    OnRecvRegPack
					    (const Byte *  pHead	 ,
					     const Byte *  pTail	 );

virtual	CRRPSerialBus::RecvResult   OnRecv  (CRRPSerialBus::RecvReason	 ,
					     const Byte *& pHead	 ,
					     const Byte *  pTail	 );

static	CRRPSerialBus::RecvResult
	GAPI			    OnRecv  (CRRPSerialBus::DeviceEntry *,
					     CRRPSerialBus::RecvReason   ,
					     const Byte *& pHead	 ,
					     const Byte *  pTail	 );

virtual	void		On_IRRPCallBack	    (	     const void *, _U32, _U32);

static	void GAPI	On_IRRPCallBack	    (void *, const void *, _U32, _U32);

protected :

	GCriticalSection    m_PLock	    ;

	_U32		    m_uDeviceId	    ;

	DWord		    m_BusTrMask	    ;
	GTimeout	    m_BusTrTimeout  ;

	CRRPSerialBus::DeviceEntry
			    m_BusEntry	    ;

	Register *	    m_RegCur	    ;
	Register::List	    m_RegList	    ;

	Register	    m_RegStatus	    ;

	XferBuf		    m_OOBSendBuf    ;

public :

	CRRPCallBack	    m_sIRRPCallBack ;
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
void CRRPSerialDevice::XferBuf::Put (const CRRPSerialDevice::XferBuf::Pack * pPack)
{
	if (pBase + sizeof (pBase) < pTail + pPack ->bPackLen)
	{   return;}

	memcpy (pTail, pPack, pPack ->bPackLen);

		pTail +=      pPack ->bPackLen ;
};

void CRRPSerialDevice::XferBuf::PutOver (const CRRPSerialDevice::XferBuf::Pack * pPack)
{
    for (const Pack *  p = pHeadPack; p < pTailPack; p += p ->bPackLen)
    {
	if  (p ->bPackId == pPack ->bPackId )
	{
	if  (pBase + sizeof (pBase) < pTail + pPack ->bPackLen - p ->bPackLen)
	{   return;}
							  pTail -= p ->bPackLen;

	memmove ((Byte *) p, ((Byte *) p) + p ->bPackLen, pTail - (Byte *) p);
	    break ;
	}
    }
	Put (pPack);

	    return;
};

const CRRPSerialDevice::XferBuf::Pack *
      CRRPSerialDevice::XferBuf::Get (const CRRPSerialDevice::XferBuf::Pack * pPack)
{
    if (pPack)
    {
	((const Byte *&) pPack) += pPack ->bPackLen;

	return (pPack < pTailPack) ? pHeadPack = (Pack *) pPack : ((pHead = pTail = pBase), NULL);
    }
	return (pHead < pTail	 ) ? pHeadPack : NULL;
};
//
//[]------------------------------------------------------------------------[]
//
_U32 CRRPSerialDevice::Register::OnAckValue (_U32 v)
{
    Bool    bDuty = (uFlags & FREG_Duty) ? True : False;

    switch (uFlags & (FREG_AckEnable | FREG_ReqEnable))
    {
    case (0		 | FREG_ReqEnable) :	// 01 : hardware write only command register
	    break;

    case (0				 ) :	// 00 : software virtual register
    case (FREG_AckEnable |		0) :	// 10 : hardware read  only status  register

	if (uFlags & FREG_Duty)
	{
	    uFlags &= (~FREG_Duty     );
	    uFlags |= ( FREG_Changed  );

	    uValue  =  v;
	    break;
	}
	if (uValue != v)
	{
	    uFlags |= ( FREG_Changed  );

	    uValue  =  v;
	}
	    break;

    case (FREG_AckEnable | FREG_ReqEnable) :	// 11 : hardware rdwr status register

	if (uFlags & FREG_Duty)
	{
	    uFlags &= (~FREG_Duty     );
	    uFlags |= ( FREG_Changed  );

	    uValue  =  v;
	    break;
	}
	if (uFlags & FREG_ReqToSend)
	{
	    break;
	}
	if (uValue != v)
	{
	    uFlags |= ( FREG_ReqToSend);
//	    uFlags |= ( FREG_ReqToSync);

	    break;
	}
	if (uFlags & FREG_ReqToSync)
	{
	    uFlags &= (~FREG_ReqToSync);
	    uFlags |= ( FREG_Changed  );
	}
	    break;
    }
	return uFlags & (FREG_ReqToSync
		      |  FREG_ReqToSend
		      |  FREG_Changed  );
};
/*
_U32 CRRPSerialDevice::Register::SetReqValue (_U32 v)
{
    if (uFlags &    FREG_ReqEnable)
    {
	uValue  = v;

	uFlags &= (~FREG_Duty     );
	uFlags |= ( FREG_ReqToSend);

    if (uFlags &    FREG_AckEnable)
    {
	uFlags |= ( FREG_ReqToSync);
    }
    }
    else
    {
	return OnAckValue (v);
    }
	return uFlags & (FREG_ReqToSync
		      |  FREG_ReqToSend
		      |  FREG_Changed  );
};

_U32 CRRPSerialDevice::Register::SetReqBits (_U32 c, _U32 s)
{
    return SetReqValue ((uValue & (~c)) | s);
};
*/
_U32 CRRPSerialDevice::Register::Refresh ()
{
    if (uFlags & FREG_Duty)
    {}
    else
    {
    switch (uFlags & (FREG_AckEnable | FREG_ReqEnable))
    {
    case (0		 | FREG_ReqEnable) :	// 01 : hardware write only command register
	break;

    case (0				 ) :	// 00 : software virtual register
    case (FREG_AckEnable |		0) :	// 10 : hardware read  only status  register
	uFlags |= FREG_Changed;
	break;

    case (FREG_AckEnable | FREG_ReqEnable) :	// 11 : hardware rdwr status register
	if (uFlags & FREG_ReqToSync)
	{}
	else
	{
	    uFlags |= FREG_Changed;
	}
	break;

    default :
	break;
    };
    }
	return uFlags & (FREG_ReqToSync
		      |  FREG_ReqToSend
		      |  FREG_Changed  );
};
//
//[]------------------------------------------------------------------------[]
//
void CRRPSerialDevice::Register::PutReqValue (Byte *& pTail)
{
	_U32	m = 0;
	_U32	v = 0;
	_U32	t = 0;

    if	   (uFlags & FREG_ReqToSend)
    {
	if (uFlags & FREG_Param	   )
	{
		m  = (((_U32) _WREG_HIMASK (uCodeMask_3_2)) << 24)
		   | (((_U32) _WREG_LOMASK (uCodeMask_3_2)) << 16)
		   | (((_U32) _WREG_HIMASK (uCodeMask_1_0)) <<  8)
		   | (((_U32) _WREG_LOMASK (uCodeMask_1_0)) <<  0);

		t = uValue;

	    for (_U32 b = 0x00000001; b; b <<= 1)
	    {
		if (m & b)
		{
		v |= ((t & 0x1) ? b : 0x0); t >>= 1;
		}
	    }
	}
	else
	{
		v = uValue;
	}

	if (_WREG_LOREG (uCodeMask_1_0))
	{
	    * pTail ++ = _WREG_LOREG (uCodeMask_1_0);
	    * pTail ++ = LOBYTE ( LOWORD (v));
	}
	if (_WREG_HIREG (uCodeMask_1_0))
	{
	    * pTail ++ = _WREG_HIREG (uCodeMask_1_0);
	    * pTail ++ = HIBYTE ( LOWORD (v));
	}
	if (_WREG_LOREG (uCodeMask_3_2))
	{
	    * pTail ++ = _WREG_LOREG (uCodeMask_3_2);
	    * pTail ++ = LOBYTE ( HIWORD (v));
	}
	if (_WREG_HIREG (uCodeMask_3_2))
	{
	    * pTail ++ = _WREG_HIREG (uCodeMask_3_2);
	    * pTail ++ = HIBYTE ( HIWORD (v));
	}

//	if ((uFlags & (FREG_AckEnable | FREG_ReqEnable)) == FREG_ReqEnable)
	{
	     uFlags &= (~FREG_ReqToSend);
	}
    }
};

static const Byte * ScanXPack (const Byte * pHead,
			       const Byte * pTail, Byte Addr)
{
    for (const Byte * p = pHead; p < pTail; p += 2)
    {
    if  ((* p == Addr) && (((* (p + 1) & 0x80) == 0)))
    {
	return p   ;
    }
    }
	return NULL;
};

Bool CRRPSerialDevice::Register::DecodeAckValue (      _U32 & Value ,
						 const Byte * pHead ,
						 const Byte * pTail )
{
	  _U32	 m = 0;
	  _U32	 v = 0;
	  _U32	 t = 0;

	  Bool	 bDecode = False;

    const Byte * pReg;

    if (uFlags & FREG_AckEnable)
    {{
    if (uFlags & FREG_Param    )
    {
	    m  = (((_U32) _WREG_HIMASK (uCodeMask_3_2)) << 24)
	       | (((_U32) _WREG_LOMASK (uCodeMask_3_2)) << 16)
	       | (((_U32) _WREG_HIMASK (uCodeMask_1_0)) <<  8)
	       | (((_U32) _WREG_LOMASK (uCodeMask_1_0)) <<  0);

	if (_WREG_HIREG (uCodeMask_3_2))
	{
	if ( NULL ==    (pReg = ScanXPack (pHead, pTail, 
	    _WREG_HIREG (uCodeMask_3_2))))
	{
	    return False;
	}
	    t |= (((_U32) * (pReg + 1)) << 24);
	}

	if (_WREG_LOREG (uCodeMask_3_2))
	{
	if ( NULL ==    (pReg = ScanXPack (pHead, pTail,
	    _WREG_LOREG (uCodeMask_3_2))))
	{
	    return False;
	}
	    t |= (((_U32) * (pReg + 1)) << 16);
	}

	if (_WREG_HIREG (uCodeMask_1_0))
	{
	if ( NULL ==    (pReg = ScanXPack (pHead, pTail, 
	    _WREG_HIREG (uCodeMask_1_0))))
	{
	    return False;
	}
	    t |= (((_U32) * (pReg + 1)) <<  8);
	}

	if (_WREG_LOREG (uCodeMask_1_0))
	{
	if ( NULL ==    (pReg = ScanXPack (pHead, pTail,
	    _WREG_LOREG (uCodeMask_1_0))))
	{
	    return False;
	}
	    t |= (((_U32) * (pReg + 1)) <<  0);
	}

	for (_U32 b = 0x80000000; b; b >>= 1)
	{
	    if (m & b)
	    {
	    v <<= 1; v |= (t & b) ? 1 : 0;
	    }
	}
	    bDecode = True  ;
    }
    else
    {
	    v	    = uValue;

	if (_WREG_HIREG (uCodeMask_3_2))
	{
	if ( NULL !=	(pReg = ScanXPack (pHead, pTail,
	    _WREG_HIREG (uCodeMask_3_2))))
	{
	    v	&= 0x00FFFFFF;
	    v	|= (((_U32) (* (pReg + 1) & _WREG_HIMASK (uCodeMask_3_2))) << 24);

	    bDecode = True;
	}
	}

	if (_WREG_LOREG (uCodeMask_3_2))
	{
	if ( NULL !=    (pReg = ScanXPack (pHead, pTail,
	    _WREG_LOREG (uCodeMask_3_2))))
	{
	    v	&= 0xFF00FFFF;
	    v	|= (((_U32) (* (pReg + 1) & _WREG_LOMASK (uCodeMask_3_2))) << 16);

	    bDecode = True;
	}
	}

	if (_WREG_HIREG (uCodeMask_1_0))
	{
	if ( NULL !=    (pReg = ScanXPack (pHead, pTail,
	    _WREG_HIREG (uCodeMask_1_0))))
	{
	    v	&= 0xFFFF00FF;		
	    v	|= (((_U32) (* (pReg + 1) & _WREG_HIMASK (uCodeMask_1_0))) <<  8);

	    bDecode = True;
	}
	}

	if (_WREG_LOREG (uCodeMask_1_0))
	{
	if ( NULL !=    (pReg = ScanXPack (pHead, pTail,
	    _WREG_LOREG (uCodeMask_1_0))))
	{
	    v	&= 0xFFFFFF00;
	    v	|= (((_U32) (* (pReg + 1) & _WREG_LOMASK (uCodeMask_1_0))) <<  0);

	    bDecode = True;
	}
	}
    }
    }}

    if (bDecode)
    {
	Value = v;
    }
	return bDecode;
};
//
//[]------------------------------------------------------------------------[]
//
CRRPSerialDevice::CRRPSerialDevice (_U32 uDeviceId)
{ 
    m_uDeviceId		     = uDeviceId ;

    m_BusTrMask		     =	       0 ;
    m_BusTrTimeout	  .Disactivate ();

    m_BusEntry.uIdleFlags     =	       0 ;

    m_BusEntry.uOrderId      = uDeviceId ;
    m_BusEntry.pContext	     = this	 ;
    m_BusEntry.pLineCallBack = OnLine    ;
    m_BusEntry.pSendCallBack = OnSend    ;
    m_BusEntry.pRecvCallBack = OnRecv    ;
    m_BusEntry.pIdleCallBack = OnIdle	 ;

    m_RegCur		     = NULL	 ;

    m_RegList.append (m_RegStatus.
		      DefineSWReg_32 (_WREG_MASK (0xFF,0x00, 0xFF,0x00),
				      _WREG_MASK (0xFF,0x00, 0xFF,0x00)));

    m_sIRRPCallBack.SetHandler (On_IRRPCallBack, this);
};
//
//[]------------------------------------------------------------------------[]
//
void CRRPSerialDevice::Reset ()
{
    for (Register * r = m_RegList.first (); r; r = m_RegList.getnext (r))
    {
    if (  (r ->uFlags & FREG_AckEnable))
    {
	r ->uFlags &= 0x00FF	;
	r ->uFlags |= FREG_Duty	;

    if (! (r ->uFlags & FREG_ReqEnable))
    {
	SetRegValue (* r, 0);
    }}
    }
	m_BusTrMask = 0x00000000;

printf ("\n\n\t\t\t\t...Reset device %08X...\n\n", m_uDeviceId);
};

void CRRPSerialDevice::Initialize ()
{
    SetRegStatus (0xFFFFFFFF, 0);

    Reset ();
};

void CRRPSerialDevice::Connect (CRRPSerialBus & Bus)
{
    Bus.Connect	   (& m_BusEntry);
};

void CRRPSerialDevice::Disconnect (CRRPSerialBus & Bus)
{
    Bus.Disconnect (& m_BusEntry);
};
//
//[]------------------------------------------------------------------------[]
//
void CRRPSerialDevice::OnIdle ()
{};

void CRRPSerialDevice::OnLine (
CRRPSerialBus::LineStatus Status, GResult dResult)
{
    AckRegStatus (IRRPDevice::IRRPDevice_BusOnline | IRRPDevice::IRRPDevice_DevOnline,
		 (Status == CRRPSerialBus::On)
		? IRRPDevice::IRRPDevice_BusOnline : 0x00);
		  
printf ("%02X::Line {%s}\n", HIBYTE(HIWORD(m_uDeviceId)), (Status == CRRPSerialBus::On) ? "Online" : "Offline");
};

CRRPSerialBus::SendResult
CRRPSerialDevice::PutSendOOBRequest (Byte *& pTail, const Byte * pHigh)
{
    for (const XferBuf::Pack * pPack = m_OOBSendBuf.Get (NULL); pPack; pPack = m_OOBSendBuf.Get (pPack))
    {
    if  (pHigh - pTail	    < pPack ->bPackLen - (Byte) sizeof (XferBuf::Pack))
    {	break;}

    memcpy (pTail, pPack + 1, pPack ->bPackLen - (Byte) sizeof (XferBuf::Pack));

	    pTail +=	      pPack ->bPackLen - (Byte) sizeof (XferBuf::Pack) ;

#if TRACE_SEND
    {{	const Byte * pMarker = (const Byte *)(pPack + 1);

printf ("...OOB  [");

	for (; pMarker < ((const Byte *) pPack) + pPack ->bPackLen; pMarker ++)
	{
	    printf (" %02X", * pMarker);
	}
printf ("]\n");
    }}
#endif

    };
	return CRRPSerialBus::NoMoreData;
};

CRRPSerialBus::SendResult
CRRPSerialDevice::PutRecvRequest (Byte *& pTail, const Byte * pHigh)
{
    if (pHigh < pTail + 2)
    {
	return CRRPSerialBus::NoMoreData  ;
    }
      * pTail ++ = 0x81;
      * pTail ++ = HIBYTE (HIWORD (m_uDeviceId));

	m_BusTrMask <<= 1;

#if TRACE_REQUEST
printf ("...Request {%02X %02X}\n", * (pTail - 2), * (pTail - 1));
#endif

	return CRRPSerialBus::QueueForRecv;
};

CRRPSerialBus::SendResult
CRRPSerialDevice::PutSendRequest (Byte *& pTail, const Byte * pHigh)
{
	if (m_RegCur == NULL)
	{
	    m_RegCur  = m_RegList.first ();
	}

#if TRACE_SEND
    {{	Byte * pMarker = pTail;
#endif

    for (;  m_RegCur; m_RegCur = m_RegList.getnext (m_RegCur))
    {
	if (pHigh < pTail + 8)
	{
	    return CRRPSerialBus::QueueForSend;
	}
	    m_RegCur ->PutReqValue (pTail);
    }

#if TRACE_SEND
	if (pMarker < pTail)
	{
printf ("...Send [");

	for (;pMarker < pTail; pMarker += 2)
	{
	    printf (" %02X %02X", * (pMarker + 0), * (pMarker + 1));
	}
printf ("]\n");
	}
    }}
#endif
	    return CRRPSerialBus::NoMoreData ;
};

CRRPSerialBus::SendResult CRRPSerialDevice::OnSend (
CRRPSerialBus::SendReason Reason, Byte *& pTail, const Byte * pHigh)
{
    if (! (GetRegStatus () & 0x02))
    {
	if (CRRPSerialBus::SendOOB == Reason)
	{
        return CRRPSerialBus::NoMoreData;
	}
	if (m_BusTrTimeout.NotInfinite () && ! m_BusTrTimeout.Occured ())
	{
        return CRRPSerialBus::NoMoreData;
	};
	    m_BusTrTimeout.Activate (500);

	return PutRecvRequest	 (pTail, pHigh);
    }

    if (CRRPSerialBus::SendOOB == Reason)
    {
	return PutSendOOBRequest (pTail, pHigh);
    }

	CRRPSerialBus::SendResult    res =
	       PutSendRequest	 (pTail, pHigh);

    if (CRRPSerialBus::NoMoreData == res)
    {
	return PutRecvRequest	 (pTail, pHigh);
    }
	return res;
};
//
//[]------------------------------------------------------------------------[]
//
/*
printf ("Changed %08X = %08X (%d)\n", i ->uCodeMask_1_0, i ->uValue, i ->uValue);

if (0xD6 == _WREG_LOREG (i ->uCodeMask_1_0))
{
if (0xD7 == HIBYTE (HIWORD (m_uDeviceId)))
{
printf ("\t\t\t\t");
}
printf ("[%02X%02X] Changed = [%04X]\n", _WREG_HIREG (i ->uCodeMask_1_0),
					 _WREG_LOREG (i ->uCodeMask_1_0), LOWORD (i ->uValue));
}
*/

void CRRPSerialDevice::OnAckValue (CRRPSerialDevice::Register & r, _U32 uValue)
{
    _U32	    uIdleFlags  = 
	 m_BusEntry.uIdleFlags;

	 m_BusEntry.uIdleFlags |= r.OnAckValue (uValue);

    if ((m_BusEntry.uIdleFlags ^ uIdleFlags) & FREG_Changed)
    {
	QueueUserAPC (((GApartment *) s_pRRPService) ->GetCoThread () ->GetHandle (),
		      NULL, 0, NULL, NULL);
    }
};

void CRRPSerialDevice::SetRegValue (CRRPSerialDevice::Register & r, _U32 uValue)
{
    if (r.uFlags &    FREG_ReqEnable)
    {
	r.uValue  = uValue;

	r.uFlags &= (~FREG_Duty     );
	r.uFlags |= ( FREG_ReqToSend);

    if (r.uFlags &    FREG_AckEnable)
    {
	r.uFlags |= ( FREG_ReqToSync);
    }
    }
    else
    {
	r.uFlags |= ( FREG_Duty	    );

	OnAckValue  (r, uValue);
    }
};

void CRRPSerialDevice::OnAckBits (CRRPSerialDevice::Register & r, _U32 c, _U32 s)
{
    OnAckValue  (r, (r.uValue & (~c)) | s);
};

void CRRPSerialDevice::SetRegBits  (CRRPSerialDevice::Register & r, _U32 c, _U32 s)
{
    SetRegValue (r, (r.uValue & (~c)) | s);
};

void CRRPSerialDevice::OnRecvRegPack (const Byte * pHead, const Byte * pTail)
{
    _U32 uValue ;

    for (Register * r = m_RegList.first (); r; r = m_RegList.getnext (r))
    {
    if  (r ->DecodeAckValue (uValue, pHead, pTail))
    {
#if FALSE

if ((0xBF == _WREG_LOREG (r ->uCodeMask_1_0)) && (0x07 == _WREG_LOMASK (r ->uCodeMask_1_0)))
{
    printf ("\t\t\t....%02X\n", uValue);
};

#endif
	OnAckValue (* r, uValue);
    }
    }
};

static Bool CheckRecvRegPack (const Byte * pHead,
			      const Byte * pTail)
{
    if (pHead + 4 > pTail)
    {
	return False;
    }

    _U16    uCRC  = 0;

    for (;  pHead < pTail - 2; pHead += 2)
    {
//
// Remove this check to the grab section
//
//  if ((!(* (pHead + 0) & 0x80))
//  ||  ( (* (pHead + 1) & 0x80)))
//  {
//	return False;
//  }
        uCRC += * (pHead + 0);
        uCRC += * (pHead + 1);
    }
	return (uCRC == getLitEndianW (pTail - 2, 0)) ? True : False;
};

CRRPSerialBus::RecvResult CRRPSerialDevice::OnRecv (
CRRPSerialBus::RecvReason Reason, const Byte *& pHead, const Byte * pTail)
{
    if (CRRPSerialBus::RecvTimeout == Reason)
    {
	if((m_BusTrMask & 0x07) == 0)
	{
	if (m_RegStatus.uValue & IRRPDevice::IRRPDevice_DevOnline)
	{
	    Reset ();
	}
	    AckRegStatus (IRRPDevice::IRRPDevice_DevOnline, 0x00);
	}
	    return CRRPSerialBus::RecvBreak;
    }

    for (;  pHead < pTail;)
    {
	if (* pHead != HIBYTE (HIWORD (m_uDeviceId)))
	{
	    pHead ++;

	    continue;
	}
	if (  pTail < pHead + (* (pHead + 1)))
	{
	    return CRRPSerialBus::RecvContinue;
	}

	if (CheckRecvRegPack (pHead,  pHead + ((* (pHead + 1) * 2) / 2))) 
	{
	if (0 == (IRRPDevice::IRRPDevice_DevOnline & GetRegStatus ()))
	{
#if FALSE
	    CRRPSerialDevice::Reset ();
#else
//New	    Reset ();
#endif
	}
	    m_BusTrTimeout.Disactivate ();
	    m_BusTrMask  |= 1;

	    AckRegStatus (0x00, IRRPDevice::IRRPDevice_DevOnline);

#if TRACE_RECV
{{  const Byte * p = pHead;
printf ("...Recv [");
    for (; p < pHead + ((* (pHead + 1) * 2) / 2) - 2; p += 2)
    {
printf (" %02X %02X", * p, * (p + 1));
    }
printf ("\n");
}}
#endif

	    OnRecvRegPack (pHead + 2, pHead + ((* (pHead + 1) * 2) / 2) - 2);

     				      pHead += (* (pHead + 1) * 2) / 2;
	}
	    return CRRPSerialBus::RecvBreak;
    };
	    return CRRPSerialBus::RecvContinue;
};

void GAPI CRRPSerialDevice::OnIdle (
CRRPSerialBus::DeviceEntry * e)
{
    ((CRRPSerialDevice *) e ->pContext) ->OnIdle ();
};

void GAPI CRRPSerialDevice::OnLine (
CRRPSerialBus::DeviceEntry * e, CRRPSerialBus::LineStatus Status, GResult dResult)
{
    ((CRRPSerialDevice *) e ->pContext) ->OnLine (Status, dResult);
};

CRRPSerialBus::SendResult GAPI CRRPSerialDevice::OnSend (
CRRPSerialBus::DeviceEntry * e, CRRPSerialBus::SendReason Reason,
				Byte *& pTail, const Byte * pHigh)
{
#if TRUE

  ((CRRPSerialDevice *) e ->pContext) ->LockAcquire ();

    CRRPSerialBus::SendResult r =
  ((CRRPSerialDevice *) e ->pContext) ->OnSend (Reason, pTail, pHigh);

  ((CRRPSerialDevice *) e ->pContext) ->LockRelease ();

    return r;
#else
    return ((CRRPSerialDevice *) e ->pContext) ->OnSend (Reason, pTail, pHigh);
#endif
};

CRRPSerialBus::RecvResult GAPI CRRPSerialDevice::OnRecv (
CRRPSerialBus::DeviceEntry * e, CRRPSerialBus::RecvReason Reason,
				const Byte *& pHead, const Byte * pTail)
{
#if TRUE

  ((CRRPSerialDevice *) e ->pContext) ->LockAcquire ();

    CRRPSerialBus::RecvResult r =
  ((CRRPSerialDevice *) e ->pContext) ->OnRecv (Reason, pHead, pTail);

  ((CRRPSerialDevice *) e ->pContext) ->LockRelease ();

    return r;
#else
    return ((CRRPSerialDevice *) e ->pContext) ->OnRecv (Reason, pHead, pTail);
#endif
};

void CRRPSerialDevice::On_IRRPCallBack (const void *, _U32, _U32)
{};

void GAPI CRRPSerialDevice::On_IRRPCallBack (void * pContext	 ,
				       const void * hIRRPCallBack, 
					     _U32   uParamId	 ,
					     _U32   uValue	 )
{
    ((CRRPSerialDevice *) pContext) ->On_IRRPCallBack (hIRRPCallBack, uParamId, uValue);
};
//
//[]------------------------------------------------------------------------[]
//
void CRRPSerialDevice::DispatchAPC (DWord dCtrlCode,
				    int	  nArgs, void ** pArgs, HANDLE hResume)
{
    if (hResume)
    {
	::SetEvent (hResume);
    }
};

void GAPI CRRPSerialDevice::DispatchAPC (int nArgs, void ** pArgs, HANDLE hResume)
{
//  GASSERT (2 <= nArgs)

    ((CRRPSerialDevice *) pArgs [0]) ->DispatchAPC 
    ((DWord	        ) pArgs [1], nArgs - 2, pArgs + 2, hResume);
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
class CRRPDevice : public CRRPSerialDevice
{
	friend class CRRPSerialBus;
	friend class CRRPUrp;

public :

		    CRRPDevice	(_U32 uDeviceId) : CRRPSerialDevice (uDeviceId)
		    {};

	void	    OnIdleRegList	();

	void	    OnIdle		();

protected :
#ifdef _MSC_BUG_C2248
public :
#endif
#pragma pack (_M_PACK_VPTR)

	struct CallBackEntry : public IRRPCallBack::Handle
	{
	STDMETHOD_ (GResult,  Duplicate)(IRRPCallBack::Handle *&, IRRPCallBack *);

	STDMETHOD_ (void,	  Close)();

	STDMETHOD_ (void,	 Select)(_U32 *);

	STDMETHOD_ (void,      UnSelect)(_U32 *);

	STDMETHOD_ (void,	Refresh)(_U32 *);

			    CallBackEntry ()
			    {
				pRRPDevice = NULL;

				uFlags	   = 0	 ;
				pICallBack = NULL;
				nParamIds  = 0   ;
			    };

	    typedef TSLink <CallBackEntry, sizeof (IRRPCallBack::Handle)>   Link;
	    typedef TXList <CallBackEntry,
		    TSLink <CallBackEntry, sizeof (IRRPCallBack::Handle)> > List;

	    Link	    LLink	    ;
	    CRRPDevice *    pRRPDevice	    ;

	    _U32	    uFlags	    ;
	    IRRPCallBack *  pICallBack	    ;

	    int		    nParamIds	    ;
	    _U32	    pParamIds [128] ;
	};

#pragma pack ()


	CallBackEntry *	CreateCallBack	(IRRPCallBack  *);
	void		CloseCallBack	(CallBackEntry *);

public :

	void	    IRRPCallBack_Refresh    ()
		    {
			printf ("...[%08X] : Refresh request\n", m_uDeviceId);

			m_BusEntry.uIdleFlags |= FREG_Changed;
		    };
public :

virtual	HRESULT	    XUnknown_QueryInterface	(REFIID, void **);

virtual	void	    XUnknown_FreeInterface	(void *);

	friend
	class TXUnknown_Entry <CRRPDevice>;
	      TXUnknown_Entry <CRRPDevice>
				m_XUnknown  ;

	GResult	    IRRPDevice_Connect		(IRRPCallBack::Handle *&  ,
						 IRRPCallBack *	pICallBack);

	void	    IRRPDevice_SetParam		(int, const IRRPDevice::Param *);

	void	    IRRPDevice_GetParam		(int,       IRRPDevice::Param *);

	friend
	class IRRPDevice_Entry;
	friend
	class TInterface_Entry <IRRPDevice, CRRPDevice>;
	class IRRPDevice_Entry : public
	      TInterface_Entry <IRRPDevice, CRRPDevice>
	{
	STDMETHOD_ (GResult,	    Connect)(IRRPCallBack::Handle *&
							    hICallBack ,
					     IRRPCallBack * pICallBack )
	{
	    return GetT () ->IRRPDevice_Connect	  (hICallBack ,
						   pICallBack );
	};
	STDMETHOD_ (void,	   SetParam)(	   int	    nParams,
					     const Param *  pParams)
	{
		   GetT () ->IRRPDevice_SetParam  (nParams, pParams);
	};
	STDMETHOD_ (void,	   GetParam)(	   int	    nParams,
						   Param *  pParams)
	{
		   GetT () ->IRRPDevice_GetParam  (nParams, pParams);
	};
	}		    m_IRRPDevice    ;


protected :

	void		    DispatchAPC		(DWord dCtrlCode, int, void **, HANDLE);

protected :

	_U32		    GetDeviceId		() const { return m_uDeviceId;};

virtual	void		    SetParam		(const IRRPDevice::Param * pParam );

virtual	void		    OnIdleRegPack	(IRRPProtocol::ParamPack::Entry *&, Register *);

protected :

	CallBackEntry::List m_CallBackList  ;
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
GResult CRRPDevice::CallBackEntry::Duplicate (IRRPCallBack::Handle *&, IRRPCallBack *)
{
    return ERROR_NOT_SUPPORTED;
};

void CRRPDevice::CallBackEntry::Close ()
{
    IRRPCallBack * pI = pICallBack   ;


    void *  pArgs [3] = {pRRPDevice, (void *) 0, (void *) this};

    ::QueueUserAPC (((GApartment *) s_pRRPService) ->GetCoThread () ->GetHandle (),
		    CRRPSerialDevice::DispatchAPC, 3, pArgs,
		    TCurrent ()	    ->GetResume ());
    if (pI)
    {
	pI ->Release ();
    }
};

void CRRPDevice::CallBackEntry::Select (_U32 *)
{
};

void CRRPDevice::CallBackEntry::UnSelect (_U32 *)
{
};

void CRRPDevice::CallBackEntry::Refresh (_U32 *)
{
    uFlags |= 0x01;

    pRRPDevice ->IRRPCallBack_Refresh ();
};

struct IRRPProtocolBuffer
{
    union
    {
	IRRPProtocol::ParamPack::Entry *
		pItem	    ;
	Byte *	pTail	    ;
    };
	Byte	pBase [4096];

	IRRPProtocolBuffer () { pTail = pBase; };
};

void CRRPDevice::OnIdleRegPack (IRRPProtocol::ParamPack::Entry *& pItem, Register * r)
{
    putBigEndianD (pItem, offsetof (IRRPProtocol::ParamPack::Entry, uParamId),
		   m_uDeviceId
		| (0x00000000)
		| (((_U32) _WREG_HIREG (r ->uCodeMask_1_0)) << 8)
		| (((_U32) _WREG_LOREG (r ->uCodeMask_1_0)) << 0));

    putBigEndianD (pItem, offsetof (IRRPProtocol::ParamPack::Entry, uValue  ),
			  r ->uValue);

    pItem ++;
};

void CRRPDevice::OnIdleRegList ()
{
    Bool		bChangedInited = 0x0;
    Bool		bRefreshInited = 0x0;
    IRRPProtocolBuffer	bChanged ;
    IRRPProtocolBuffer	bRefresh ;

    m_BusEntry.uIdleFlags &= (~FREG_Changed);

    if (True)
    {
	{{
	    putBigEndianW (bChanged.pTail, offsetof (IRRPProtocol::ParamPack, uLength   ), 0);
	    putBigEndianW (bChanged.pTail, offsetof (IRRPProtocol::ParamPack, uType     ), 0);
	    putBigEndianD (bChanged.pTail, offsetof (IRRPProtocol::ParamPack, uRequestId), 0);

	    bChanged.pTail += sizeof (IRRPProtocol::DataPack);

	    for (Register * i = m_RegList.first (); i; i = m_RegList.getnext (i))
	    {
	    if  (i ->uFlags &	 FREG_Changed)
	    {
		 i ->uFlags &= (~FREG_Changed);
#if TRUE
		OnIdleRegPack (bChanged.pItem, i);
#else
		putBigEndianD (bChanged.pItem, offsetof (IRRPProtocol::ParamPack::Entry, uParamId),
				       m_uDeviceId
				    | (0x00000000)
				    | (((_U32) _WREG_HIREG (i ->uCodeMask_1_0)) << 8)
				    | (((_U32) _WREG_LOREG (i ->uCodeMask_1_0)) << 0));

		putBigEndianD (bChanged.pItem, offsetof (IRRPProtocol::ParamPack::Entry, uValue  ),
						i ->uValue);
		bChanged.pItem ++;
#endif
	    }
	    }

	    if (sizeof (IRRPProtocol::DataPack)	      < (bChanged.pTail - bChanged.pBase))
	    {
		putBigEndianW (bChanged.pBase, offsetof (IRRPProtocol::ParamPack, uLength),
					       WORD	(bChanged.pTail - bChanged.pBase));
		bChangedInited |= 0x2;
	    }
		bChangedInited |= 0x1;
	}}
    }

    for (CallBackEntry * e = m_CallBackList.first (); e; e = m_CallBackList.getnext (e))
    {
	if (e ->uFlags & 0x01)
	{
	if (! bRefreshInited)
	{{
	    putBigEndianW (bRefresh.pTail, offsetof (IRRPProtocol::ParamPack, uLength   ), 0);
	    putBigEndianW (bRefresh.pTail, offsetof (IRRPProtocol::ParamPack, uType     ), 0);
	    putBigEndianD (bRefresh.pTail, offsetof (IRRPProtocol::ParamPack, uRequestId), 0);

	    bRefresh.pTail += sizeof (IRRPProtocol::DataPack);

	    for (Register * i = m_RegList.first (); i; i = m_RegList.getnext (i))
	    {
#if TRUE
		OnIdleRegPack (bRefresh.pItem, i);
#else
		putBigEndianD (bRefresh.pItem, offsetof (IRRPProtocol::ParamPack::Entry, uParamId),
				       m_uDeviceId
				    | (0x00000000)
				    | (((_U32) _WREG_HIREG (i ->uCodeMask_1_0)) << 8)
				    | (((_U32) _WREG_LOREG (i ->uCodeMask_1_0)) << 0));

		putBigEndianD (bRefresh.pItem, offsetof (IRRPProtocol::ParamPack::Entry, uValue  ),
						i ->uValue);
		bRefresh.pItem ++;
#endif
	    }

	    if (sizeof (IRRPProtocol::DataPack)	      < (bRefresh.pTail - bRefresh.pBase))
	    {
		putBigEndianW (bRefresh.pBase, offsetof (IRRPProtocol::ParamPack, uLength),
					       WORD	(bRefresh.pTail - bRefresh.pBase));
		bRefreshInited |= 0x2;
	    }
		bRefreshInited |= 0x1;
	}}

	if (bRefreshInited == 0x3)
	{
//printf ("\t\t\t%02X::Invoke (Refresh) ...............\n", HIBYTE(HIWORD(m_uDeviceId)));

	    e ->pICallBack ->Invoke (e, (IRRPProtocol::ParamPack *) bRefresh.pBase);
	}
	    e ->uFlags &= (~0x01);

	    continue;
	}

	if (bChangedInited == 0x3)
	{
//printf ("\t\t\t%02X::Invoke (Changed) ...............\n", HIBYTE(HIWORD(m_uDeviceId)));

	    e ->pICallBack ->Invoke (e, (IRRPProtocol::ParamPack *) bChanged.pBase);
	}
    }
};

void CRRPDevice::OnIdle ()
{
    if (m_BusEntry.uIdleFlags & FREG_Changed)
    {
	OnIdleRegList ();
    }
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
void CRRPDevice::SetParam (const IRRPDevice::Param * pParam)
{};

void CRRPDevice::CloseCallBack (CallBackEntry * e)
{
    delete (m_CallBackList.extract (e));
};

CRRPDevice::CallBackEntry * 
CRRPDevice::CreateCallBack (IRRPCallBack * pICallBack)
{
	CallBackEntry *  e = new CallBackEntry;

    if (e)
    {
	e ->pRRPDevice = this;
       (e ->pICallBack = pICallBack) ->AddRef ();

	m_CallBackList.append (e);

	e ->Refresh (NULL);
    }	
	return e;
};

GResult CRRPDevice::IRRPDevice_Connect (IRRPCallBack::Handle *& hICallBack,
					IRRPCallBack *		pICallBack)
{
		hICallBack = NULL;

    if (NULL == pICallBack)
    {
	return ERROR_INVALID_PARAMETER;
    }

    void *  pArgs [4] = { this, (void *) 1, (void *) NULL, (void *) pICallBack};

    ::QueueUserAPC (((GApartment *) s_pRRPService) ->GetCoThread () ->GetHandle (),
		    CRRPSerialDevice::DispatchAPC, 4, pArgs,
		    TCurrent ()	  ->GetResume   ());

    if (pArgs [2])
    {
	hICallBack = (IRRPCallBack::Handle *)(pArgs [2]);

	return ERROR_SUCCESS  ;
    }
	return ERROR_NO_SYSTEM_RESOURCES;
};

void CRRPDevice::IRRPDevice_SetParam (int nParams, const IRRPDevice::Param * pParams)
{
    void *  pArgs [4] = { this, (void *) 2, (void *) nParams, (void *) pParams };

#if True

    if (((GApartment *) s_pRRPService) ->GetCoThread () ->GetId () ==
	GetCurThread () ->GetId ())
    {
	CRRPSerialDevice::DispatchAPC (4, pArgs, NULL);

	return;
    }

#endif

    ::QueueUserAPC (((GApartment *) s_pRRPService) ->GetCoThread () ->GetHandle (),
		    CRRPSerialDevice::DispatchAPC, 4, pArgs,
		    TCurrent ()	  ->GetResume   ());
};

void CRRPDevice::IRRPDevice_GetParam (int nParams, IRRPDevice::Param * pParams)
{
};

HRESULT CRRPDevice::XUnknown_QueryInterface (REFIID iid, void ** pi)
{
    HRESULT Result = ERROR_NOT_SUPPORTED;

    LockAcquire ();

	if (m_XUnknown.GetRefCount () == 0)
	{
	    m_XUnknown.Init (this);
	}
	else
	{
	    m_XUnknown.AddRef ();
	}

	if (IID_IUnknown == iid)
	{
	    (* (XUnknown **) pi = & m_XUnknown) ->AddRef ();

	    Result = ERROR_SUCCESS;
	}

    LockRelease ();

	    m_XUnknown.Release ();

    return Result;
};

#if FALSE

void CRRPDevice::XUnknown_FreeInterface (void * pi)
{
    if (pi == & m_IRRPDevice)
    {
    }
    else
    if (pi == & m_XUnknown)
    {
    }
};

#endif

void CRRPDevice::DispatchAPC (DWord dCtrlCode,
			      int   nArgs, void ** pArgs, HANDLE hResume)
{
    if (0 == dCtrlCode)
    {
	CloseCallBack ((CRRPDevice::CallBackEntry *) pArgs [0]);
    }
    else
    if (1 == dCtrlCode)
    {
	pArgs [0] = CreateCallBack ((IRRPCallBack *) pArgs [1]);
    }
    else
    if (2 == dCtrlCode)
    {{
	for (int i = 0; i < (int) pArgs [0]; i ++)
	{
	    SetParam (((const IRRPDevice::Param *) pArgs [1]) + i);
	}
    }}
    else
    if (3 == dCtrlCode)
    {{
    }}

    if (hResume)
    {
	::SetEvent (hResume);
    }
};
//
//[]------------------------------------------------------------------------[]

static CRRPDevice *	    GetCRRPMScan ();

//[]------------------------------------------------------------------------[]
//
#define	KKM_DEVICE_ID	    0xCC000000

class CRRPKKM : public CRRPDevice
{
public :
		    CRRPKKM ();

	void		    SetParam	(const IRRPDevice::Param *);

	friend
	class IRRPKKM_Entry;
	friend
	class TInterface_Entry <IRRPKKM, CRRPKKM>;
	class IRRPKKM_Entry : public
	      TInterface_Entry <IRRPKKM, CRRPKKM>

	{
	STDMETHOD_ (GResult,	    Connect)(IRRPCallBack::Handle *&
							    hICallBack ,
					     IRRPCallBack * pICallBack )
	{
	    return GetT () ->IRRPDevice_Connect	  (hICallBack ,
						   pICallBack );
	};
	STDMETHOD_ (void,	   SetParam)(	   int	    nParams,
					     const Param *  pParams)
	{
		   GetT () ->IRRPDevice_SetParam  (nParams, pParams);
	};
	STDMETHOD_ (void,	   GetParam)(	   int	    nParams,
						   Param *  pParams)
	{
		   GetT () ->IRRPDevice_GetParam  (nParams, pParams);
	};
	};

protected :

	CRRPSerialDevice::Register	m_RegGetPower		;

	CRRPSerialDevice::Register	m_RegGetU220		;
	CRRPSerialDevice::Register	m_RegGetUForce		;
	CRRPSerialDevice::Register	m_RegGetICharging	;
	CRRPSerialDevice::Register	m_RegGetTForceItems	;
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
CRRPKKM::CRRPKKM () : CRRPDevice (KKM_DEVICE_ID)
{
    IRRPKKM_Entry     IEntry;

    * ((void **) & m_IRRPDevice) = * ((void **) & IEntry);

    m_RegList.append (m_RegGetPower.
		      DefineROReg_16	(_WREG_MASK (0x00,0x00, 0x11,0xCC)));

    m_RegList.append (m_RegGetU220.
		      DefineROParam_16	(_WREG_MASK (0x7F,0xFE, 0x01,0x00)));
    m_RegList.append (m_RegGetUForce.
		      DefineROParam_16	(_WREG_MASK (0x7F,0xF7, 0x07,0xF6)));

    m_RegList.append (m_RegGetICharging.
		      DefineROParam_16	(_WREG_MASK (0x7F,0xCD, 0x00,0x00)));
    m_RegList.append (m_RegGetTForceItems.
		      DefineROParam_16	(_WREG_MASK (0x7F,0xFF, 0x00,0x00)));
};

void CRRPKKM::SetParam (const IRRPDevice::Param * pParam)
{
printf ("[%08X] <- %08X (%d)\n", pParam ->uParamId, pParam ->u32Value, pParam ->u32Value);

/*
    switch (pParam ->uParamId)
    {
    case IRRPKKM::ID_RegSetPower :
	SetRegValue (m_RegSetPower, pParam ->u32Value);
	break;
    };
*/
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
#define URP_DEVICE_ID	    0x80000000

class CRRPUrp : public CRRPDevice
{
private :

	void	    LogHV		();

public :
		    CRRPUrp ();

	void	    Reset		();

	void	    Initialize		();

	void	    OnAckValue		(Register &, _U32 v);

protected :

	void	    SetExposeParam	(int	iUAnode10     ,
					 int	iIFilamentPre ,
					 int	iIFilament    ,
					 int	= 0	      );

	void	    SetParam		(const IRRPDevice::Param *);

	CRRPSerialBus::SendResult   PutRecvRequest
					(      Byte *& pTail  ,
					 const Byte *  pHigh  );

public :

	void	    LoadConfigInfo	();

	void	    SaveConfigInfo	();

	void	    IRRPUrp_SetHV	    (IRRPUrp::HVCommand);

	void	    IRRPUrp_RunHV	    (GIrp *);

	void	    IRRPUrp_CheckExposition (Bool, int *, int *);

	int	    IRRPUrp_GetUAnodeX10    (int nUAnodeR10);

	int	    IRRPUrp_GetUAnodeR10    (int nUAnodeX10);

	friend
	class TInterface_Entry <IRRPUrp, CRRPUrp>;
	class IRRPUrp_Entry : public
	      TInterface_Entry <IRRPUrp, CRRPUrp>
	{
	STDMETHOD_ (GResult,	    Connect)(IRRPCallBack::Handle *&
							    hICallBack ,
					     IRRPCallBack * pICallBack )
	{
	    return GetT () ->IRRPDevice_Connect	  (hICallBack ,
						   pICallBack );
	};
	STDMETHOD_ (void,	   SetParam)(	   int	    nParams,
					     const Param *  pParams)
	{
		   GetT () ->IRRPDevice_SetParam  (nParams, pParams);
	};
	STDMETHOD_ (void,	   GetParam)(	   int	    nParams,
						   Param *  pParams)
	{
		   GetT () ->IRRPDevice_GetParam  (nParams, pParams);
	};
	STDMETHOD_ (void,	      SetHV)(HVCommand	uCommand)
	{
		   GetT () ->IRRPUrp_SetHV (uCommand);
	};
	STDMETHOD_ (void,	      RunHV)(	   GIrp *   pIrp   )
	{
		   GetT () ->IRRPUrp_RunHV (pIrp);
	};
	STDMETHOD_ (void,   CheckExposition)(Bool bFocus, int * pUAnode ,
							  int * pIAnode )
	{
		   GetT () ->IRRPUrp_CheckExposition (bFocus, pUAnode, pIAnode);
	};
	STDMETHOD_ (int,       GetUAnodeX10)(int fUAnodeR10)
	{
	    return GetT () ->IRRPUrp_GetUAnodeX10 (fUAnodeR10);
	};
	STDMETHOD_ (int,       GetUAnodeR10)(int nUAnodeX10)
	{
	    return GetT () ->IRRPUrp_GetUAnodeR10 (nUAnodeX10);
	};
	};
//
//--------------------------------------------------------
//
	struct IFilamentEntry
	{
	    int	    nIFilamentX	    ;
	    int	    nIAnodeR	    ;
	};

	struct IFilamentRow
	{
	    int	    nUAnodeX	    ;
	    int	    nEntries	    ;
	    IFilamentEntry 
		    pEntries [50]   ;

	    int	    GetIAnodeR	    (int nIFilamentX);

	    int	    GetIFilamentX   (int nIAnodeR);
	};

	struct IFilamentTable
	{
	    int	    nRows	    ;
	    IFilamentRow
		    pRows    [50]   ;

	    int	    GetIAnodeR	    (int   nUAnodeX, int   nIFilamentX);

	    int	    GetIFilamentX   (int   nUAnodeX, int   nIAnodeR);

	    void    SetExposition   (int * pUAnodeX, int * pIAnodeR);
	};

	int		GetIAnodeR	    (int nUAnodeX, int nIFilamentX);

	int		GetIFialmentX	    (int nUAnodeX, int nIAnodeR	  );
//
//--------------------------------------------------------

	void		LoadIFilamentTable  (char *, Bool);
//
//--------------------------------------------------------
//
	struct UAnodeTable
	{
	    int	    nEntries	    ;

	struct	    Entry
	{
	    int	    nUAnodeX	    ;
	    float   fUAnodeR	    ;

	}	    pEntries [50]   ;

	    int	    GetUAnodeR10	    (int nUAnodeX10);
	    int	    GetUAnodeX10	    (int fUAnodeR10);
	};

	int		GetUAnodeR	    (int nUAnodeX10);
	int		GetUAnodeR10	    (int nUAnodeX10);
	int		GetUAnodeX	    (int nUAnodeR10);


	void		LoadUAnodeTable	    (char *);
//
//--------------------------------------------------------

	void		LoadIniParam	    ();

	GIrp *		Co_RunHVWaitOn	    (GIrp *, void *);

	GIrp *		Co_RunHVWaitReady   (GIrp *, void *);

	GIrp *		Co_RunHVStart	    (GIrp *, void *);

	GIrp *		Co_RunHVDone	    (GIrp *, void *);

	void		On_IRRPCallBack	    (	     const void *, _U32, _U32);

	Bool		On_SecondTimer	    ();

static	Bool GAPI	On_SecondTimer	    (IWakeUp *,  IWakeUp::Reason 
							       eOnReason,
							void * pContext ,
							void * pArgument);

	void		OnIdleRegPack	    (IRRPProtocol::ParamPack::Entry *&, Register *);

protected :

	DWord				m_ParSetMode		;

	CRRPSerialDevice::Register	m_RegGetHVReady		;
	CRRPSerialDevice::Register	m_RegGetStatusHV	;

	CRRPSerialDevice::Register	m_regSetUAnode		;
//	CRRPSerialDevice::Register	m_RegSetUAnode		;
//	CRRPSerialDevice::Register	m_RegSetUAnode10	;
	int				m_RegSetUAnode		;
	int				m_RegSetUAnode10	;
	int				m_ParSetUAnode10	;
	CRRPSerialDevice::Register	m_RegSetUAnodeMax	;

	CRRPSerialDevice::Register	m_RegSetIAnode		;

	CRRPSerialDevice::Register	m_RegSetFocus		;

	CRRPSerialDevice::Register	m_RegSetIFilamentPreSF	;
	CRRPSerialDevice::Register	m_RegSetIFilamentSF	;

	CRRPSerialDevice::Register	m_RegSetIFilamentPreBF	;
	CRRPSerialDevice::Register	m_RegSetIFilamentBF	;

	CRRPSerialDevice::Register	m_ParSetIAnode		;
	CRRPSerialDevice::Register	m_ParSetIAnodeMode	;

	CRRPSerialDevice::Register	m_RegSetMAS		;
	CRRPSerialDevice::Register	m_RegSetExposeTime	;

	CRRPSerialDevice::Register	m_RegGetBlk		;
	CRRPSerialDevice::Register	m_RegGetStatus		;

	CRRPSerialDevice::Register	m_RegGetUAnodePos	;
	int				m_RegGetUAnodePos10	;

	CRRPSerialDevice::Register	m_RegGetUAnodeNeg	;

	CRRPSerialDevice::Register	m_RegGetIAnodePos	;
	CRRPSerialDevice::Register	m_RegGetIAnodeNeg	;

	CRRPSerialDevice::Register	m_RegGetExposeTime	;
	CRRPSerialDevice::Register	m_RegGetMAS		;

	CRRPSerialDevice::Register	m_RegGetIFilamentPre	;
	CRRPSerialDevice::Register	m_RegGetIFilament	;

	CRRPSerialDevice::Register	m_RegGetTForce		;
	CRRPSerialDevice::Register	m_RegGetTTube		;

	CRRPSerialDevice::Register	m_RegGetDevType		;
	CRRPSerialDevice::Register	m_RegSetHVDelay		;

	CRRPSerialDevice::Register	m_RegSetFieldSize	;

	CRRPSerialDevice::Register	m_RegSetAnodeSpin	;
	CRRPSerialDevice::Register	m_RegSetAnodeSpinMin	;
	CRRPSerialDevice::Register	m_RegGetAnodeSpin	;
	CRRPSerialDevice::Register	m_RegMaxTForce		;

// KKM Reflector :
//
	CRRPSerialDevice::Register	m_RegKKMStatus		;

	CRRPSerialDevice::Register	m_RegGetPower		;
	CRRPSerialDevice::Register	m_RegGetU220		;
	CRRPSerialDevice::Register	m_RegGetUForce		;
	CRRPSerialDevice::Register	m_RegGetICharging	;
//
// -------------

	CRRPSerialDevice::Register	m_RegHeatTubeTimeout	;
	CRRPSerialDevice::Register	m_RegHeatForceTimeout	;

	CRRPSerialDevice::Register	m_RegGetShotCount	;
	CRRPSerialDevice::Register	m_RegGetShotCountBad	;

	TInterlockedPtr <GIrp *>	m_pShotIrp		;

public :

	IWakeUp *			m_wSecondTimer		;

	IRRPKKM *			m_pIRRPKKM		;
	IRRPCallBack::Handle *		m_hIRRPKKMCallBack	;

protected :

	int				m_iUForceLo		;

	int				m_iQForceCur		;
	int				m_iQForceHi		;
	int				m_iQForceLo		;
	int				m_iQForceK		;

	int				m_iQTubeCur		;
	int				m_iQTubeHi		;
	int				m_iQTubeLo		;
	int				m_iQTubeK		;

	int				m_iHeatDelay		;
	int				m_tExposeMax		;

	int				m_iUAnodeSFMin		;
	int				m_iUAnodeSFMax		;

	int				m_iUAnodeBFMin		;
	int				m_iUAnodeBFMax		;

	int				m_iIFilamentPreSFMin	;
	int				m_iIFilamentPreSFMax	;
	int				m_iIFilamentPreSFDef	;

	int				m_iIFilamentPreBFMin	;
	int				m_iIFilamentPreBFMax	;
	int				m_iIFilamentPreBFDef	;

	int				m_iIFilamentSFMax	;
	int				m_iIFilamentBFMax	;

	_U32				m_uDelay_Full		;
	_U32				m_uDelay_21x30		;
	_U32				m_uDelay_18x24		;
	_U32				m_uDelay_12x12		;

	_U32				m_uUACorrection		;

	UAnodeTable			m_UAnodeTable		;

	IFilamentTable			m_IFilamentTableSF	;
	IFilamentTable			m_IFilamentTableBF	;
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
CRRPUrp::CRRPUrp () : CRRPDevice (URP_DEVICE_ID)
{
    IRRPUrp_Entry    IEntry;

    * ((void **) & m_IRRPDevice) = * ((void **) & IEntry);

    m_ParSetMode =    0x0000;

    m_RegList.append (m_RegGetHVReady.
		      DefineSWReg_32	(_WREG_MASK (0xFF,0x00, 0xFF,0x7D),
					 _WREG_MASK (0xFF,0x00, 0xFF,0x00)));
    m_RegList.append (m_RegGetStatusHV.
		      DefineSWReg_32	(_WREG_MASK (0xFF,0x00, 0xFF,0x7F),
					 _WREG_MASK (0x00,0x00, 0xFF,0x00)));
    m_RegList.append (m_RegGetDevType.
		      DefineROReg_16	(_WREG_MASK (0x00,0x00, 0x7F,0x98)));

    m_RegList.append (m_regSetUAnode.
		      DefineRWParam_16	(_WREG_MASK (0x7F,0x9E, 0x07,0x9D)));
//  m_RegList.append (m_RegSetUAnode.
//		      DefineSWReg_16	(_WREG_MASK (0x00,0x00, 0x00,0x0B)));
//  m_RegList.append (m_RegSetUAnode10.
//		      DefineSWReg_16	(_WREG_MASK (0x00,0x00, 0x00,0x0C)));
    m_RegSetUAnode    =
    m_RegSetUAnode10  = 0;
    m_ParSetUAnode10  = 0;
    m_RegList.append (m_RegSetIAnode.
		      DefineRWParam_16	(_WREG_MASK (0X7f,0xA0, 0x07,0x9F)));

    m_RegList.append (m_RegSetFocus.
		      DefineRWReg_16	(_WREG_MASK (0x00,0x00, 0x01,0x97)));	// Set this to default on start.

    m_RegList.append (m_RegSetIFilamentPreSF.
		      DefineRWParam_16	(_WREG_MASK (0x7F,0xA2, 0x07,0xA1)));
    m_RegList.append (m_RegSetIFilamentSF.
		      DefineRWParam_16	(_WREG_MASK (0x7F,0xA4, 0x07,0xA3)));
    m_RegList.append (m_RegSetIFilamentPreBF.
		      DefineRWParam_16	(_WREG_MASK (0x7F,0xA6,	0x07,0xA5)));
    m_RegList.append (m_RegSetIFilamentBF.
		      DefineRWParam_16	(_WREG_MASK (0x7F,0xA8, 0x07,0xA7)));
    m_RegList.append (m_ParSetIAnode.
		      DefineSWReg_16	(_WREG_MASK (0x00,0x00, 0xFF,0x07)));
    m_RegList.append (m_ParSetIAnodeMode.
		      DefineSWReg_16	(_WREG_MASK (0x00,0x00, 0xFF,0x08)));

    m_RegList.append (m_RegSetHVDelay.
		      DefineRWParam_16	(_WREG_MASK (0x00,0x00, 0x7F,0xB7)));

    m_RegList.append (m_RegSetMAS.
		      DefineRWParam_16	(_WREG_MASK (0x7F,0xAA, 0x7F,0xA9)));
    m_RegList.append (m_RegSetExposeTime.
		      DefineRWParam_16	(_WREG_MASK (0x7F,0xAC, 0x7F,0xAB)));

    m_RegList.append (m_RegMaxTForce.
		      DefineRWParam_16	(_WREG_MASK (0x00,0x00, 0x7F,0xB5)));

    m_RegList.append (m_RegGetBlk.
		      DefineROReg_32	(_WREG_MASK (0x7F,0xD1, 0x7F,0xD0),
					 _WREG_MASK (0x00,0x00, 0x7F,0xD2)));
    m_RegList.append (m_RegGetStatus.
		      DefineROReg_16	(_WREG_MASK (0x07,0xD4,	0x03,0xD3)));
    m_RegList.append (m_RegGetUAnodePos.
		      DefineROParam_16	(_WREG_MASK (0x7F,0xE1, 0x07,0xE0)));
    m_RegGetUAnodePos10 = 0;
    m_RegList.append (m_RegGetUAnodeNeg.
		      DefineROParam_16	(_WREG_MASK (0x7F,0xE3, 0x07,0xE2)));
    m_RegList.append (m_RegGetIAnodePos.
		      DefineROParam_16	(_WREG_MASK (0x7F,0xE5, 0x07,0xE4)));
    m_RegList.append (m_RegGetIAnodeNeg.
		      DefineROParam_16	(_WREG_MASK (0x7F,0xE7, 0x07,0xE6)));
    m_RegList.append (m_RegGetExposeTime.
		      DefineROParam_16	(_WREG_MASK (0x7F,0xE9, 0x7F,0xE8)));
    m_RegList.append (m_RegGetMAS.
		      DefineROParam_16	(_WREG_MASK (0x7F,0xEB, 0x7F,0xEA)));
    m_RegList.append (m_RegGetTForce.
		      DefineROParam_16	(_WREG_MASK (0x00,0x00, 0x7F,0xF9)));
    m_RegList.append (m_RegGetTTube.
		      DefineROParam_16	(_WREG_MASK (0x00,0x00, 0x7F,0xFA)));
    m_RegList.append (m_RegGetIFilamentPre.
		      DefineROParam_16	(_WREG_MASK (0x7F,0xEF, 0x07,0xEE)));
    m_RegList.append (m_RegGetIFilament.
		      DefineROParam_16	(_WREG_MASK (0x7F,0xF1, 0x07,0xF0)));

    m_RegList.append (m_RegSetAnodeSpin.
		      DefineROParam_16	(_WREG_MASK (0x7F,0xBA,	0x01,0x00)));
    m_RegList.append (m_RegSetAnodeSpinMin.
		      DefineROParam_16	(_WREG_MASK (0x7F,0xBB,	0x01,0x00)));
    m_RegList.append (m_RegGetAnodeSpin.
		      DefineROParam_16	(_WREG_MASK (0x7F,0xFB, 0x01,0x00)));

// KKM Reflector :
//

    m_RegList.append (m_RegKKMStatus.
		      DefineSWReg_16	(_WREG_MASK (0xFF,0x01, 0xFF,0x08)));
    m_RegList.append (m_RegGetPower.
		      DefineSWReg_16	(_WREG_MASK (0xFF,0x01, 0xFF,0x09)));
    m_RegList.append (m_RegGetU220.
		      DefineSWReg_16	(_WREG_MASK (0x03,0x01, 0xFF,0x02)));
    m_RegList.append (m_RegGetUForce.
		      DefineSWReg_16	(_WREG_MASK (0x03,0x01, 0xFF,0x03)));
    m_RegList.append (m_RegGetICharging.
		      DefineSWReg_16	(_WREG_MASK (0x03,0x01, 0xFF,0x04)));
//
// --------------

    m_RegList.append (m_RegHeatTubeTimeout.
		      DefineSWReg_16	(_WREG_MASK (0xFF,0x01, 0xFF,0x06)));
    m_RegList.append (m_RegHeatForceTimeout.
		      DefineSWReg_16	(_WREG_MASK (0xFF,0x01, 0xFF,0x07)));

    m_RegList.append (m_RegSetFieldSize.
		      DefineSWReg_16	(_WREG_MASK (0x00,0x01, 0x0F,0x01)));

    m_RegList.append (m_RegGetShotCount.
		      DefineSWReg_32	(_WREG_MASK (0xFF,0x01, 0xFF,0x0C),
					 _WREG_MASK (0x7F,0x00, 0xFF,0x00)));
    m_RegList.append (m_RegGetShotCountBad.
		      DefineSWReg_32	(_WREG_MASK (0xFF,0x01, 0xFF,0x0D),
					 _WREG_MASK (0x7F,0x00, 0xFF,0x00)));

    CreateWakeUp     (m_wSecondTimer);

    m_pIRRPKKM		= NULL;
    m_hIRRPKKMCallBack	= NULL;

    m_iUForceLo		 =   200;

    m_iQTubeCur		 =     0;
    m_iQTubeHi		 = 46000;
    m_iQTubeLo		 = 26000;
    m_iQTubeK		 =   400;

    m_iQForceCur	 =     0;
    m_iQForceHi		 = 46000;
    m_iQForceLo		 = 26000;
    m_iQForceK		 =   200;

    m_iHeatDelay	 =     0;
    m_tExposeMax	 =  8000;

    m_iUAnodeSFMin	 =   
    m_iUAnodeBFMin	 =   0;
    m_iUAnodeSFMax	 =
    m_iUAnodeSFMax	 =  35;

    m_iIFilamentPreSFMin = 180;
    m_iIFilamentPreSFMax = 220;
    m_iIFilamentPreSFDef = 201;

    m_iIFilamentSFMax	 = 420;

    m_iIFilamentPreBFMin = 180;
    m_iIFilamentPreBFMax = 220;
    m_iIFilamentPreBFDef = 202;

    m_iIFilamentBFMax	 = 460;

    m_uDelay_Full	 = 0x0000007F;
    m_uDelay_21x30	 = 0x0000007F;
    m_uDelay_18x24	 = 0x0000007F;
    m_uDelay_12x12	 = 0x0000007F;

    m_uUACorrection	 = 0x00000000;

    m_UAnodeTable.nEntries   =
    m_IFilamentTableSF.nRows =
    m_IFilamentTableBF.nRows = 0;
};

void CRRPUrp::LoadConfigInfo ()
{
    FILETIME	fTimeCur  ;
    FILETIME	fTime	  ;
    int		QTubeCur  ;
    int		QForceCur ;
    Bool	bNegative ;

    if (ERROR_SUCCESS == CRRPConfigFile::LoadUrpParam (fTime, QTubeCur, QForceCur))
    {
    if ((0 != fTime.dwLowDateTime) || (0 != fTime.dwHighDateTime))
    {
	::GetSystemTimeAsFileTime (& fTimeCur);

#if (1310 > _MSC_VER)

	(LARGE_INTEGER &) fTime = LargeIntegerSubtract
				((LARGE_INTEGER &) fTimeCur, (LARGE_INTEGER &) fTime);

	(LARGE_INTEGER &) fTime = ExtendedLargeIntegerDivide 
				((LARGE_INTEGER &) fTime, 10 * 1000 * 1000, NULL);
#else
	((LARGE_INTEGER &) fTime).QuadPart = 
				((LARGE_INTEGER &) fTimeCur).QuadPart -
				((LARGE_INTEGER &) fTime   ).QuadPart;

	((LARGE_INTEGER &) fTime).QuadPart =
				((LARGE_INTEGER &) fTime).QuadPart / 10 * 1000 * 1000;
#endif

	if ((0 == fTime.dwHighDateTime) && ((60 * 60) > fTime.dwLowDateTime))
	{

    printf ("\t\t\t...Last work %d sec.\n", fTime.dwLowDateTime);
    printf ("\t\t\t...F  %d.\n", QForceCur);
    printf ("\t\t\t...T  %d.\n", QTubeCur );

				       bNegative = False;
	    if (QTubeCur < 0)
	    {	QTubeCur = - QTubeCur; bNegative = True;}

	    if (0 < (QTubeCur   -= (fTime.dwLowDateTime * m_iQTubeK)))
	    {
	    if ((m_iQTubeLo  < (m_iQTubeCur = QTubeCur)) && bNegative)
	    {
		SetRegValue (m_RegHeatTubeTimeout , ((m_iQTubeCur  - m_iQTubeLo ) * 64)/m_iQTubeK /64);
	    }
	    }
					 bNegative = False;
	    if (QForceCur < 0)
	    {	QForceCur = - QForceCur; bNegative = True;}

	    if (0 < (QForceCur  -= (fTime.dwLowDateTime * m_iQForceK)))
	    {
	    if ((m_iQForceLo < (m_iQForceCur = QForceCur)) && bNegative)
	    {
		SetRegValue (m_RegHeatForceTimeout, ((m_iQForceCur - m_iQForceLo) * 64)/m_iQForceK/64);
	    }
	    }
	}
    printf ("\t\t\t...Ft %d\n", m_RegHeatForceTimeout.uValue);
    printf ("\t\t\t...Tt %d\n", m_RegHeatTubeTimeout .uValue);
    }
    }

    printf ("\t\t\t...CRRPUrp::Load ()\n");
};

void CRRPUrp::SaveConfigInfo ()
{
    CRRPConfigFile::SaveUrpParam ((0 == m_RegHeatTubeTimeout .uValue) ? m_iQTubeCur  : - m_iQTubeCur, 
				  (0 == m_RegHeatForceTimeout.uValue) ? m_iQForceCur : - m_iQForceCur);

    printf ("\t\t\t...CRRPUrp::Save ()\n");

};

void CRRPUrp::Reset ()
{
    DWord	dTemp	;
    CRRPDevice::Reset ();

    m_RegGetDevType.uFlags |= FREG_Duty;

    SetRegValue (m_RegSetFieldSize , 0x00);
    SetRegValue (m_RegSetHVDelay   , LOBYTE(HIWORD(m_uDelay_Full)) & 0x7F);

    IRRPDevice::Param		     Param;
    Param.Init (IRRPMScan::ID_RegSetShotRows,
				     LOBYTE(LOWORD(m_uDelay_Full)) & 0x7F);
    GetCRRPMScan () ->SetParam	  (& Param);

    GetIRRPSettings () ->GetValue (IRRPSettings::ID_URP_TForceMax, & dTemp, sizeof (DWord), NULL);
    SetRegValue (m_RegMaxTForce	   , dTemp);

    SetRegBits (m_RegGetHVReady, 0, IRRPUrp::HVReady_NotConnect);
};

void CRRPUrp::Initialize ()
{
    CRRPSerialDevice::Initialize ();

    m_iQTubeCur	 = 
    m_iQForceCur = 0;

    SetRegValue (m_RegHeatTubeTimeout , 0);
    SetRegValue (m_RegHeatForceTimeout, 0);

    LoadIniParam   ();

    LoadConfigInfo ();

    int cGood, cBad;

    GetShotCount (cGood, cBad);

    SetRegValue  (m_RegGetShotCount   , cGood);
    SetRegValue  (m_RegGetShotCountBad, cBad );
};

static GResult GAPI createIRRPKKM (IRRPKKM **);

void CRRPUrp::SetExposeParam (int  iUAnode10	 ,
			      int  iIFilamentPre ,
			      int  iIFilament	 ,
			      int  		 )
{
    int iUAnode = ((iUAnode10 + 5) / 10);

    if (0x01 & m_RegSetFocus.uValue)
    {
    if (iUAnode	      < m_iUAnodeBFMin)
	iUAnode	      = m_iUAnodeBFMin;
    else
    if (iUAnode	      > m_iUAnodeBFMax)
	iUAnode	      = m_iUAnodeBFMax;

    if (iIFilamentPre < m_iIFilamentPreBFMin)
	iIFilamentPre = m_iIFilamentPreBFMin;
    else
    if (iIFilamentPre > m_iIFilamentPreBFMax)
	iIFilamentPre = m_iIFilamentPreBFMax;

    if (iIFilament    < m_iIFilamentPreBFMax)
	iIFilament    = m_iIFilamentPreBFMax;

    if (iIFilament    > m_iIFilamentBFMax)
	iIFilament    = m_iIFilamentBFMax;
    }
    else
    {
    if (iUAnode	      < m_iUAnodeSFMin)
	iUAnode	      = m_iUAnodeSFMin;
    else
    if (iUAnode	      > m_iUAnodeSFMax)
	iUAnode	      = m_iUAnodeSFMax;

    if (iIFilamentPre < m_iIFilamentPreSFMin)
	iIFilamentPre = m_iIFilamentPreSFMin;
    else
    if (iIFilamentPre > m_iIFilamentPreSFMax)
	iIFilamentPre = m_iIFilamentPreSFMax;

    if (iIFilament    < m_iIFilamentPreSFMax)
	iIFilament    = m_iIFilamentPreSFMax;

    if (iIFilament    > m_iIFilamentSFMax)
	iIFilament    = m_iIFilamentSFMax;
    }

    if (((iUAnode10 + 5) / 10) != iUAnode)
    {
	iUAnode10 = iUAnode * 10;
    }
	m_RegSetUAnode   = iUAnode  ;
	m_RegSetUAnode10 = iUAnode10;

	SetRegValue (m_regSetUAnode  , (0x16 != m_RegGetDevType.uValue) 
	
		   ? m_RegSetUAnode : m_RegSetUAnode10);

    if (0x01 & m_RegSetFocus.uValue)
    {
	SetRegValue (m_RegSetIFilamentPreBF, iIFilamentPre);
	SetRegValue (m_RegSetIFilamentBF   , iIFilament	  );
    }
    else
    {
	SetRegValue (m_RegSetIFilamentPreSF, iIFilamentPre);
	SetRegValue (m_RegSetIFilamentSF   , iIFilament	  );
    }
};

void CRRPUrp::OnAckValue (CRRPSerialDevice::Register & r, _U32 uValue)
{
    CRRPDevice::OnAckValue (r, uValue);

    if ((& r == & m_RegStatus) && (r.uFlags & FREG_Changed))
    {
	SetRegBits (m_RegGetHVReady, (r.uValue & 0x02) ? IRRPUrp::HVReady_NotConnect : 0,
				     (r.uValue & 0x02) ? 0 : IRRPUrp::HVReady_NotConnect);

	SetRegBits (m_RegKKMStatus , 0x01, (r.uValue & 0x01));
    }

    if ((& r == & m_RegGetBlk) && (r.uFlags & FREG_Changed))
    {
	SetRegBits (m_RegGetHVReady, IRRPUrp::HVReady_Block, (r.uValue & 0x007F7F7F) ? IRRPUrp::HVReady_Block : 0);

//printf ("\t\t\t\t...(U) %08X...\n", m_RegGetHVReady.uValue);
    }

    if (
       ((& r == & m_RegHeatTubeTimeout ) && (r.uFlags & FREG_Changed))
    || ((& r == & m_RegHeatForceTimeout) && (r.uFlags & FREG_Changed))
       )
    {
	SetRegBits (m_RegGetHVReady, IRRPUrp::HVReady_HeatTimeout, ((0 != m_RegHeatTubeTimeout .uValue)
								 || (0 != m_RegHeatForceTimeout.uValue)) ? IRRPUrp::HVReady_HeatTimeout : 0);

//printf ("\t\t\t\t...(T) %08X...\n", m_RegGetHVReady.uValue);
    }
    if ((& r == & m_RegGetUForce) && (r.uFlags & FREG_Changed))
    {
	SetRegBits (m_RegGetHVReady, IRRPUrp::HVReady_NotCharge, (m_pIRRPKKM && (m_iUForceLo > (int) r.uValue)) ? IRRPUrp::HVReady_NotCharge : 0);

//printf ("\t\t\t\t...(C) %08X...%d\n", m_RegGetHVReady.uValue, r.uValue);
    }
    if ((& r == & m_RegGetPower) && (r.uFlags & FREG_Changed))
    {
	SetRegBits (m_RegGetHVReady, IRRPUrp::HVReady_RedButton, (m_pIRRPKKM && (r.uValue & 0x10)) ? IRRPUrp::HVReady_RedButton : 0);
    }

    if ((& r == & m_RegSetIFilamentBF) && ( (0x01 & m_RegSetFocus.uValue)) && (r.uFlags & FREG_Changed))
    {
	SetRegValue (m_ParSetIAnode, m_IFilamentTableBF.GetIAnodeR (m_RegSetUAnode, r.uValue));
	SetRegValue (m_RegSetIAnode, ((0 == (m_ParSetMode & 0x01)) && m_ParSetIAnodeMode.uValue) ? m_ParSetIAnode.uValue : 0);
    }
    else
    if ((& r == & m_RegSetIFilamentSF) && (!(0x01 & m_RegSetFocus.uValue)) && (r.uFlags & FREG_Changed))
    {
	SetRegValue (m_ParSetIAnode, m_IFilamentTableSF.GetIAnodeR (m_RegSetUAnode, r.uValue));
	SetRegValue (m_RegSetIAnode, ((0 == (m_ParSetMode & 0x01)) && m_ParSetIAnodeMode.uValue) ? m_ParSetIAnode.uValue : 0);
    }

    if ((0x02 == (0x06 & m_RegGetStatusHV.uValue))
    && True /*(
       ((& r == & m_RegGetUAnodePos) && (0 < r.uValue))
    || ((& r == & m_RegGetIAnodeNeg) && (0 < r.uValue))
    || ((& r == & m_RegGetMAS	   ) && (1 < r.uValue))
       )*/)
    {
	SetRegBits (m_RegGetStatusHV, 0x0000, 0x0004);
    }

    if ((& r == & m_RegGetUAnodePos) && (r.uFlags & FREG_Changed))
    {
	m_RegGetUAnodePos10 = r.uValue * ((0x16 != m_RegGetDevType.uValue) ? 10 : 1);
    }

    if (	 (0x04 & m_RegGetStatusHV.uValue)
    && (
       ((& r == & m_RegGetStatus) && ((0x0000 == (r.uValue & 0x0700))
				  ||  (		 (r.uValue & 0x0400))))
       ))
    {{
	SetRegBits (m_RegGetStatusHV, 0x0006, 0x0000);

	int	  Q  = ((m_RegGetUAnodePos10 + 5) / 10) * m_RegGetMAS.uValue;
//
//	int	  Q  = m_RegGetUAnodePos.uValue * m_RegGetMAS.uValue;
 
	if (m_iQTubeHi	< (m_iQTubeCur  += Q))
	{
	    SetRegValue (m_RegHeatTubeTimeout ,  0 + ((m_iQTubeCur  - m_iQTubeLo ) * 64)/m_iQTubeK /64);
	}
	if (m_iQForceHi < (m_iQForceCur += Q))
	{
	    SetRegValue (m_RegHeatForceTimeout, m_iHeatDelay + ((m_iQForceCur - m_iQForceLo) * 64)/m_iQForceK/64);
	}
	else
	{
	    SetRegValue (m_RegHeatForceTimeout, m_iHeatDelay);
	}

	printf ("......Add Q = %d (QForceCur = %d, QTubeCur = %d)\n", Q, m_iQForceCur, m_iQTubeCur);

	Q = (0 < m_RegGetIAnodeNeg.uValue) ? Q : 0;

	IncShotCount (((0 < Q) && (0 == m_RegGetBlk.uValue)), Q, (0 < Q) ? m_RegGetMAS.uValue : 0);

	if ((0 < Q) && (0 == m_RegGetBlk.uValue))
	{
	    SetRegValue (m_RegGetShotCount   , m_RegGetShotCount   .uValue + 1);
	}
	else
	{
	    SetRegValue (m_RegGetShotCountBad, m_RegGetShotCountBad.uValue + 1);
	}
    }}
};

CRRPSerialBus::SendResult 
CRRPUrp::PutRecvRequest (Byte *& pTail, const Byte * pHigh)
{
    if (pHigh < pTail + 4)
    {
	return CRRPSerialBus::NoMoreData  ;
    }

    if (m_RegGetDevType.uFlags & FREG_Duty)
    {
#if True
      * pTail ++ = 0x98;
      * pTail ++ = 0x7F;
#else
      * pTail ++ = 0x98;
      * pTail ++ = 0x00;
#endif
    }
    else
    if ((m_RegGetDevType.uValue != 0x16)
    &&	(m_RegGetDevType.uValue != 0x42))

    {
      * pTail ++ = 0x98;
      * pTail ++ = 0x42;
    }
	return CRRPDevice::PutRecvRequest (pTail, pHigh);
};


int CRRPUrp::GetUAnodeX (int fUAnodeR10)
{
    return ((m_uUACorrection) ? (m_UAnodeTable.GetUAnodeX10 (fUAnodeR10) +  5)
			      :				     fUAnodeR10) / 10;
};

int CRRPUrp::GetUAnodeR (int nUAnodeX10)
{
    return ((m_uUACorrection) ? (m_UAnodeTable.GetUAnodeR10 (nUAnodeX10) +  5)
			      :				     nUAnodeX10) / 10;
};

int CRRPUrp::GetUAnodeR10 (int nUAnodeX10)
{
    return (m_uUACorrection) ? (m_UAnodeTable.GetUAnodeR10 (nUAnodeX10))
			     :				    nUAnodeX10;
};


void CRRPUrp::OnIdleRegPack (IRRPProtocol::ParamPack::Entry *& pItem, Register * r)
{
	CRRPDevice::OnIdleRegPack (pItem, r);

    if (r == & m_regSetUAnode )
    {{
	int UAnodeX10 = r ->uValue * ((0x16 != m_RegGetDevType.uValue) ? 10 : 1);

	m_RegSetUAnode	 = 
      ((m_RegSetUAnode10 =  UAnodeX10) + 5) / 10;

printf ("\t\tSetUA [%3d (%2d.%1d)] ->Show [%2d.%1d]\n", r ->uValue, m_ParSetUAnode10  / 10,
								    m_ParSetUAnode10  % 10, 
								    GetUAnodeR10 
								   (m_ParSetUAnode10) / 10,
								    GetUAnodeR10
								   (m_ParSetUAnode10) % 10);

	if (((m_ParSetUAnode10 + 5) / 10) != UAnodeX10 / 10)
	{     m_ParSetUAnode10		   = UAnodeX10;}

	putBigEndianD (pItem, offsetof (IRRPProtocol::ParamPack::Entry, uParamId),
					IRRPUrp::ID_ParSetUAnode10);
	putBigEndianD (pItem, offsetof (IRRPProtocol::ParamPack::Entry, uValue  ), GetUAnodeR10 (m_ParSetUAnode10));

	pItem ++;

	putBigEndianD (pItem, offsetof (IRRPProtocol::ParamPack::Entry, uParamId),
					IRRPUrp::ID_ParSetUAnode);
	putBigEndianD (pItem, offsetof (IRRPProtocol::ParamPack::Entry, uValue  ), GetUAnodeR   (m_ParSetUAnode10));

printf ("\t\tSetUA [%3d (%2d.%1d)] ->Show [%2d.%1d]\n", r ->uValue, m_ParSetUAnode10  / 10,
								    m_ParSetUAnode10  % 10, 
								    GetUAnodeR10 
								   (m_ParSetUAnode10) / 10,
								    GetUAnodeR10
								   (m_ParSetUAnode10) % 10);
	pItem ++;
    }}
    else
    if (r == & m_RegGetUAnodePos)
    {{
	int UAnodeR10 = GetUAnodeR10 (m_RegGetUAnodePos10);

	putBigEndianD (pItem, offsetof (IRRPProtocol::ParamPack::Entry, uParamId),
					IRRPUrp::ID_ParGetUAnode10);
	putBigEndianD (pItem, offsetof (IRRPProtocol::ParamPack::Entry, uValue  ),  UAnodeR10);

	pItem ++;

	putBigEndianD (pItem, offsetof (IRRPProtocol::ParamPack::Entry, uParamId),
					IRRPUrp::ID_ParGetUAnode);
	putBigEndianD (pItem, offsetof (IRRPProtocol::ParamPack::Entry, uValue  ), (UAnodeR10 + 5) / 10);

printf ("\t\tGetUA [%3d] ->Show [%2d.%1d]\n", r ->uValue, UAnodeR10 / 10, UAnodeR10 % 10);

	pItem ++;
    }}
};

void CRRPUrp::SetParam (const IRRPDevice::Param * pParam)
{
printf ("[%08X] <- %08X (%d)\n", pParam ->uParamId, pParam ->u32Value, pParam ->u32Value);

    switch (pParam ->uParamId)
    {
    case IRRPUrp::ID_ParSetEquMask :

	    m_RegKKMStatus	.Refresh ();
	    m_RegGetPower	.Refresh ();
	    m_RegGetU220	.Refresh ();
	    m_RegGetUForce	.Refresh ();
	    m_RegGetICharging	.Refresh ();

	if (( (IRRPUrp::EquMask_KKM & pParam ->u32Value)) && (NULL == m_pIRRPKKM))
	{
	    if (ERROR_SUCCESS == createIRRPKKM (& m_pIRRPKKM))
	    {
		m_pIRRPKKM ->Connect (m_hIRRPKKMCallBack ,
				    & m_sIRRPCallBack	 );
	    }
	}
	else
	if ((!(IRRPUrp::EquMask_KKM & pParam ->u32Value)) && (NULL != m_pIRRPKKM))
	{
	    if (m_hIRRPKKMCallBack)
	    {
		m_hIRRPKKMCallBack ->Close ();
		m_hIRRPKKMCallBack = NULL;
	    }
		m_pIRRPKKM ->Release ();
		m_pIRRPKKM = NULL;

	    SetRegValue (m_RegGetUForce, 0);
	}
	break;

    case IRRPUrp::ID_ParSetMode :
			m_ParSetMode &= (~(HIWORD (pParam ->u32Value)));
			m_ParSetMode |= ( (LOWORD (pParam ->u32Value)));
	break;

    case IRRPUrp::ID_RegSetUAnode :

	SetExposeParam (pParam ->u32Value * 10	     ,
		(0x01 & m_RegSetFocus	      .uValue)
		      ? m_RegSetIFilamentPreBF.uValue
		      : m_RegSetIFilamentPreSF.uValue,
		(0x01 & m_RegSetFocus	      .uValue)
		      ? m_RegSetIFilamentBF   .uValue
		      : m_RegSetIFilamentSF   .uValue);
	break;

    case IRRPUrp::ID_ParSetUAnode :

			m_ParSetUAnode10 =
    (m_uUACorrection) ? m_UAnodeTable.GetUAnodeX10 (pParam ->u32Value * 10)
		      :				    pParam ->u32Value * 10;

        SetExposeParam (m_ParSetUAnode10	     ,
		(0x01 & m_RegSetFocus	      .uValue)
		      ? m_RegSetIFilamentPreBF.uValue
		      : m_RegSetIFilamentPreSF.uValue,
		(0x01 & m_RegSetFocus	      .uValue)
		      ? m_RegSetIFilamentBF   .uValue
		      : m_RegSetIFilamentSF   .uValue);
	break;

    case IRRPUrp::ID_ParSetUAnode10 :

			m_ParSetUAnode10 =
    (m_uUACorrection) ? m_UAnodeTable.GetUAnodeX10 (pParam ->u32Value)
		      :				    pParam ->u32Value;

        SetExposeParam (m_ParSetUAnode10	     ,
		(0x01 & m_RegSetFocus	      .uValue)
		      ? m_RegSetIFilamentPreBF.uValue
		      : m_RegSetIFilamentPreSF.uValue,
		(0x01 & m_RegSetFocus	      .uValue)
		      ? m_RegSetIFilamentBF   .uValue
		      : m_RegSetIFilamentSF   .uValue);
	break;

    case IRRPUrp::ID_Focus :

	SetRegValue (m_RegSetFocus,  pParam ->u32Value);

	SetExposeParam (m_RegSetUAnode10	     ,
		(0x01 & m_RegSetFocus	      .uValue)
		      ? m_RegSetIFilamentPreBF.uValue
		      : m_RegSetIFilamentPreSF.uValue,
		(0x01 & m_RegSetFocus	      .uValue)
		      ? m_RegSetIFilamentBF   .uValue
		      : m_RegSetIFilamentSF   .uValue);
	break;

    case IRRPUrp::ID_IFilamentPre :

	SetExposeParam (m_RegSetUAnode10	     ,
	     (0 < (int) pParam ->u32Value)
		      ? pParam ->u32Value
		      :
	     (	(0x01 & m_RegSetFocus	      .uValue)
		      ? m_iIFilamentPreBFDef
		      : m_iIFilamentPreSFDef
	     )					     ,
		(0x01 & m_RegSetFocus	      .uValue)
		      ? m_RegSetIFilamentBF   .uValue
		      : m_RegSetIFilamentSF   .uValue);
	break;

    case IRRPUrp::ID_IFilament :

	SetExposeParam (m_RegSetUAnode10	     ,
		(0x01 & m_RegSetFocus	      .uValue)
		      ? m_RegSetIFilamentPreBF.uValue
		      : m_RegSetIFilamentPreSF.uValue,
			pParam ->u32Value	     );
	break;

    case IRRPUrp::ID_ParSetIAnode :

	SetExposeParam (m_RegSetUAnode10	     ,
		(0x01 & m_RegSetFocus	      .uValue)
		      ? m_RegSetIFilamentPreBF.uValue
		      : m_RegSetIFilamentPreSF.uValue,
		(0x01 & m_RegSetFocus	      .uValue)
		      ? m_IFilamentTableBF.GetIFilamentX
		       (m_RegSetUAnode, pParam ->u32Value)
		      : m_IFilamentTableSF.GetIFilamentX
		       (m_RegSetUAnode, pParam ->u32Value));
	break;

    case IRRPUrp::ID_MAS :
	SetRegValue (m_RegSetMAS, pParam ->u32Value);
	break;

    case IRRPUrp::ID_ExposeTime :
	SetRegValue (m_RegSetExposeTime, (pParam ->i32Value < m_tExposeMax) ? pParam ->u32Value : m_tExposeMax);
	break;

    case IRRPUrp::ID_ParSetFieldSize :
    {{
	SetRegValue (m_RegSetFieldSize , pParam ->u32Value);

	    IRRPDevice::Param Param;
	    Param.Init (IRRPMScan::ID_RegSetShotRows, 0);

	switch (m_RegSetFieldSize.uValue)
	{
	default :
	case 0  : 
	    SetRegValue (m_RegSetHVDelay, LOBYTE(HIWORD(m_uDelay_Full )) & 0x7F);
	    Param.u32Value =		  LOBYTE(LOWORD(m_uDelay_Full )) & 0x7F ;
	    break;
	case 1  :
	    SetRegValue (m_RegSetHVDelay, LOBYTE(HIWORD(m_uDelay_21x30)) & 0x7F);
	    Param.u32Value =		  LOBYTE(LOWORD(m_uDelay_21x30)) & 0x7F ;
	    break;
	case 2	:
	    SetRegValue (m_RegSetHVDelay, LOBYTE(HIWORD(m_uDelay_18x24)) & 0x7F);
	    Param.u32Value =		  LOBYTE(LOWORD(m_uDelay_18x24)) & 0x7F ;
	    break;
	case 3	:
	    SetRegValue (m_RegSetHVDelay, LOBYTE(HIWORD(m_uDelay_12x12)) & 0x7F);
	    Param.u32Value =		  LOBYTE(LOWORD(m_uDelay_12x12)) & 0x7F ;
	    break;
	};
	    GetCRRPMScan () ->SetParam (& Param);
    }} 
	break;

    case IRRPUrp::ID_ParGetShotCount :
	SetRegValue (m_RegGetShotCount	 , 0);
	SetRegValue (m_RegGetShotCountBad, 0);
	break;
    };
};

void CRRPUrp::LogHV ()
{
    LogPrintf ("NotReady (%08Xh), ",		m_RegGetHVReady	.uValue);
    LogPrintf ("FOCUS %s, ",		       (m_RegSetFocus	.uValue & 0x01) ? "Large" : "Small");
    LogPrintf ("HVDelay %d, ",			m_RegSetHVDelay .uValue);
    LogPrintf ("UInp %d, UForce %d, ",		m_RegGetU220	.uValue, m_RegGetUForce	.uValue);
    LogPrintf ("TTube %d, TForce %d\r\n",	m_RegGetTTube	.uValue, m_RegGetTForce .uValue);
    LogPrintf ("BLK      (%08Xh)\r\n",		m_RegGetBlk	.uValue);

    LogPrintf (	       "\t\t        Spin     = %d/%d/%d\r\n",
						m_RegSetAnodeSpin   .uValue,
						m_RegSetAnodeSpinMin.uValue,
						m_RegGetAnodeSpin   .uValue);

    LogPrintf ("    SetUa    = %3d/%2d.%1d",	m_regSetUAnode	    .uValue, 
						GetUAnodeR10 (m_RegSetUAnode10) / 10,
						GetUAnodeR10 (m_RegSetUAnode10) % 10);

    LogPrintf (			 "\tGetUa    = %3d/%d.%d\r\n",
						m_RegGetUAnodePos.uValue, 
						GetUAnodeR10 (m_RegGetUAnodePos10) / 10,
						GetUAnodeR10 (m_RegGetUAnodePos10) % 10);

    LogPrintf ("    SetPFC   = %3d\r\n",       (m_RegSetFocus.uValue & 0x01) ? m_RegSetIFilamentPreBF.uValue
									     : m_RegSetIFilamentPreSF.uValue);
    LogPrintf ("    SetFC    = %3d\r\n",       (m_RegSetFocus.uValue & 0x01) ? m_RegSetIFilamentBF.uValue
									     : m_RegSetIFilamentSF.uValue   );
    LogPrintf ("    SetAC    = %3d\tGetAC    = +%d/-%d\r\n",
						m_RegSetIAnode	    .uValue,
						m_RegGetIAnodePos   .uValue,
						m_RegGetIAnodeNeg   .uValue);
    LogPrintf ("    SetMAS   = %d\tGetMAS   = %d\r\n",
						m_RegSetMAS	    .uValue,
						m_RegGetMAS	    .uValue);
    LogPrintf ("    SetTexp  = %d\tGetTexp  = %d\r\n",
						m_RegSetExposeTime  .uValue,
						m_RegGetExposeTime  .uValue);
};

void CRRPUrp::IRRPUrp_SetHV (IRRPUrp::HVCommand nCommand)
{
    TStreamBuf <64> CmdBuf  ;

    switch (nCommand)
    {
    case IRRPUrp::HVPrepare :

if (m_ParSetMode & 0x01)
{
	CmdBuf.putBigEndianW (0x0A90);
	CmdBuf.putBigEndianW (0xD602);
	CmdBuf.putBigEndianW (0xD702);
}
else
{
	CmdBuf.putBigEndianW (0x0690);
}
	CmdBuf.putBigEndianW (0x9001);
	CmdBuf.putBigEndianW (0x9004);

	{{
	    LogClear	();
	    LogPrintf	("\r\n=================================================\r\n");
	    LogPrintd	();
	    LogPrintf	("\r\n");
	    LogPrintt	(True);
	    LogPrintf	(" : ������� ���������� ������ HV :\r\n\r\n");
	    LogHV	();
	    LogPrintf	("\r\n");
	    LogFlush	();
	}}

#if EMULATE_HEATS

    if (m_iQTubeHi  < (m_iQTubeCur  += 10000))
    {
	SetRegValue (m_RegHeatTubeTimeout , ((m_iQTubeCur  - m_iQTubeLo ) * 64)/m_iQTubeK /64);
    }

    if (m_iQForceHi < (m_iQForceCur += 10000))
    {
	SetRegValue (m_RegHeatForceTimeout, ((m_iQForceCur - m_iQForceLo) * 64)/m_iQForceK/64);
    }

#endif
	break;

    case IRRPUrp::HVOn :
	CmdBuf.putBigEndianW (0x0490);
	CmdBuf.putBigEndianW (0x9003);

				    SetRegBits (m_RegGetStatusHV, 0x0000, 0x0002);

	{{
	    LogPrintt	(True);
	    LogPrintf	(" : ������� ������ HV :\r\n\r\n");
	    LogHV	();
	    LogPrintf	("\r\n");
	    LogFlush	();
	}}
	break;

    case IRRPUrp::HVRelease :

if (m_ParSetMode & 0x01)
{
	CmdBuf.putBigEndianW (0x0C90);
	CmdBuf.putBigEndianW (0xD60A);
	CmdBuf.putBigEndianW (0xD70A);
}
else
{
	CmdBuf.putBigEndianW (0x0890);
}
	CmdBuf.putBigEndianW (0x9000);
	CmdBuf.putBigEndianW (0x9000);
	CmdBuf.putBigEndianW (0x9000);

				    SetRegBits (m_RegGetStatusHV, 0x0002, 0x0000);
	{{
	    LogPrintt	(True);
	    LogPrintf	(" : ������� c����� HV :\r\n\r\n");
	    LogHV	();
	    LogPrintf	("\r\n=================================================\r\n");
	    LogFlush	();
	}}
	break;
    };

    if (CmdBuf.sizeforget ())
    {
    LockAcquire ();

	m_OOBSendBuf.PutOver ((XferBuf::Pack *) CmdBuf.head ());

    LockRelease ();
    }
};
//
//[]--------------------------------------------------------------------------
//
static int SInterpolate (int X, int Xh, int Yh,
				int Xl, int Yl)
{
#if TRUE
    return		      Yl + ((((	      (X - Xl) * (Yh - Yl)) * 64) /	   (Xh - Xl) + 32) / 64);
#else
    return round_I32 ((float) Yl + (((((float)(X - Xl) * (Yh - Yl))	) / (float)(Xh - Xl)	 )     ));
#endif
};

int CRRPUrp::IFilamentRow::GetIAnodeR (int nIFilamentX)
{
    if (0 < nEntries)
    {{
	IFilamentEntry * le, * he;

	int i;

	for (i = 0; (i < nEntries) && (pEntries [i].nIFilamentX < nIFilamentX); i ++)
	{}

	if (i == nEntries)
	{   le =  he = & pEntries [i - 1];}
	else
	if (i ==	0)
	{   le =  he = & pEntries [i    ];}
	else
	{   le = (he = & pEntries [i    ]) - ((pEntries [i].nIFilamentX == nIFilamentX) ? 0 : 1);}

	if (le ->nIFilamentX != he ->nIFilamentX)
	{
//	    return le ->nIAnodeR + (((((nIFilamentX   - le ->nIFilamentX)	 * 64 *
//				    (he ->nIAnodeR    - le ->nIAnodeR   ))
//				  / (he ->nIFilamentX - le ->nIFilamentX)) + 32) / 64);

	    return SInterpolate (nIFilamentX, he ->nIFilamentX, he ->nIAnodeR,
					      le ->nIFilamentX, le ->nIAnodeR);
	}
	    return ((le ->nIAnodeR * 64) + (he ->nIAnodeR * 64) + 32)/64/2;
    }}
	    return 0;
};

int CRRPUrp::IFilamentTable::GetIAnodeR (int nUAnodeX, int nIFilamentX)
{
    if (0 < nRows)
    {{
	IFilamentRow * lr, * hr;
	int  i, lI, hI;

	for (i = 0; (i < nRows) && (pRows [i].nUAnodeX < nUAnodeX); i ++)
	{}

	if (i == nRows)
	{   lr =  hr = & pRows [i - 1];}
	else
	if (i ==     0)
	{   lr =  hr = & pRows [0];}
	else
	{   lr = (hr = & pRows [i]) - ((pRows [i].nUAnodeX == nUAnodeX) ? 0 : 1);}

	lI = lr ->GetIAnodeR (nIFilamentX);
	hI = hr ->GetIAnodeR (nIFilamentX);

	if (lr ->nUAnodeX != hr ->nUAnodeX)
	{
//	    return lI + (((((nUAnodeX   - lr ->nUAnodeX)	* 64 *
//			 (hI	        - lI	       ))
//		       / (hr ->nUAnodeX - lr ->nUAnodeX)) + 32) / 64);

	    return SInterpolate (nUAnodeX, hr ->nUAnodeX, hI,
					   lr ->nUAnodeX, lI);

	}
	    return ((lI * 64) + (hI * 64) + 32)/64/2;
    }}
	    return 0;
};
//
//[]--------------------------------------------------------------------------
//
int CRRPUrp::IFilamentRow::GetIFilamentX (int nIAnodeR)
{
    if (0 < nEntries)
    {{
	IFilamentEntry * le, * he;

	int i;

	for (i = 0; (i < nEntries) && (pEntries [i].nIAnodeR < nIAnodeR); i ++)
	{}

	if (i == nEntries)
	{   le =  he = & pEntries [i - 1];}
	else
	if (i ==	0)
	{   le =  he = & pEntries [i    ];}
	else
	{   le = (he = & pEntries [i    ]) - ((pEntries [i].nIAnodeR == nIAnodeR) ? 0 : 1);}

	if (le ->nIAnodeR != he ->nIAnodeR)
	{
//	    return le ->nIFilamentX + (((((nIAnodeR	 - le ->nIAnodeR   )	    * 64 *
//				       (he ->nIFilamentX - le ->nIFilamentX))
//				     / (he ->nIAnodeR    - le ->nIAnodeR   )) + 32) / 64);

	    return SInterpolate (nIAnodeR, he ->nIAnodeR, he ->nIFilamentX,
					   le ->nIAnodeR, le ->nIFilamentX);
	}
	    return ((le ->nIFilamentX * 64) + (he ->nIFilamentX * 64) + 32)/64/2;
    }}
	    return 0;
};

#if TRUE

int CRRPUrp::IFilamentTable::GetIFilamentX (int nUAnodeX, int nIAnodeR)
{
    if (0 < nRows)
    {{
	IFilamentRow * lr, * hr;

	int  i, lF, hF, F, P, C;

	for (i = 0; (i < nRows) && (pRows [i].nUAnodeX < nUAnodeX); i ++)
	{}

	if (i == nRows)
	{   lr =  hr = & pRows [i - 1];}
	else
	if (i ==     0)
	{   lr =  hr = & pRows [0];}
	else
	{   lr = (hr = & pRows [i]) - ((pRows [i].nUAnodeX == nUAnodeX) ? 0 : 1);}

	lF = lr ->GetIFilamentX (nIAnodeR);
	hF = hr ->GetIFilamentX (nIAnodeR);

	if (lF <  hF)
	{
	    for (F = i = lF, P = 1000; i <= hF; i ++)
	    {
		if (0 > (C = GetIAnodeR (nUAnodeX, i) - nIAnodeR)) C = -C;

		if (P > C)
		{
		    P = C; F = i;

		    continue;
		}
		    break;
	    }
	}
	else
//	if (hF <= lF)
	{
	    for (F = i = hF, P = 1000; i <= lF; i ++)
	    {
		if (0 > (C = GetIAnodeR (nUAnodeX, i) - nIAnodeR)) C = -C;

		if (P > C)
		{
		    P = C; F = i;

		    continue;
		}
		    break;
	    }
	}
	    return F;
/*
	if (lr ->nUAnodeX != hr ->nUAnodeX)
	{
//	    return lF + (((((nUAnodeX   - lr ->nUAnodeX)       * 64 *
//			(hF	        - lF	       ))
//		      / (hr ->nUAnodeX  - lr ->nUAnodeX)) + 32)/ 64);

	    return SInterpolate (nUAnodeX, hr ->nUAnodeX, hF,
					   lr ->nUAnodeX, lF);
	}
	    return ((lF * 64) + (hF * 64) + 32)/64/2;
*/
    }}
	    return 0;
};

#else

int CRRPUrp::IFilamentTable::GetIFilamentX (int nUAnodeX, int nIAnodeR)
{
    if (0 < nRows)
    {{
	IFilamentRow * lr, * hr;

	int  i, lF, hF;

	for (i = 0; (i < nRows) && (pRows [i].nUAnodeX < nUAnodeX); i ++)
	{}

	if (i == nRows)
	{   lr =  hr = & pRows [i - 1];}
	else
	if (i ==     0)
	{   lr =  hr = & pRows [0];}
	else
	{   lr = (hr = & pRows [i]) - ((pRows [i].nUAnodeX == nUAnodeX) ? 0 : 1);}

	lF = lr ->GetIFilamentX (nIAnodeR);
	hF = hr ->GetIFilamentX (nIAnodeR);

	if (lr ->nUAnodeX != hr ->nUAnodeX)
	{
//	    return lF + (((((nUAnodeX   - lr ->nUAnodeX)       * 64 *
//			(hF	        - lF	       ))
//		      / (hr ->nUAnodeX  - lr ->nUAnodeX)) + 32)/ 64);

	    return SInterpolate (nUAnodeX, hr ->nUAnodeX, hF,
					   lr ->nUAnodeX, lF);
	}
	    return ((lF * 64) + (hF * 64) + 32)/64/2;
    }}
	    return 0;
};

#endif

//
//[]--------------------------------------------------------------------------
//
void CRRPUrp::IFilamentTable::SetExposition (int * pUAnodeX, int * pIAnodeR)
{
    if (0 < nRows)
    {{
	IFilamentRow * lr, * hr;

	int  i, nIAnodeR;

	for (i = 0; (i < nRows) && (pRows [i].nUAnodeX < * pUAnodeX); i ++)
	{}

	if (i == nRows)
	{   lr =  hr = & pRows [i - 1];}
	else
	if (i ==     0)
	{   lr =  hr = & pRows [0];}
	else
	{   lr = (hr = & pRows [i]) - ((pRows [i].nUAnodeX == * pUAnodeX) ? 0 : 1);}

	if (* pUAnodeX < lr ->nUAnodeX)
	{   * pUAnodeX = lr ->nUAnodeX;}
	if (* pUAnodeX > hr ->nUAnodeX)
	{   * pUAnodeX = hr ->nUAnodeX;}

	if (* pIAnodeR < (nIAnodeR = GetIAnodeR (* pUAnodeX, 0)))
	{   * pIAnodeR =  nIAnodeR;}
	if (* pIAnodeR > (nIAnodeR = GetIAnodeR (* pUAnodeX, 1024)))
	{   * pIAnodeR =  nIAnodeR;}

	return;
    }}

    * pUAnodeX = * pIAnodeR = 0;
};
//
//[]--------------------------------------------------------------------------
//
int CRRPUrp::UAnodeTable::GetUAnodeR10 (int nUAnodeX)
{
    if (0 < nEntries)
    {{
	Entry * le, * he;

	int  i; float lU, hU;

	for (i = 0; (i < nEntries) && (pEntries [i].nUAnodeX < nUAnodeX); i ++)
	{}

	if (i == nEntries)
	{   le =  he = & pEntries [i - 1];}
	else
	if (i ==        0)
	{   le =  he = & pEntries [0];}
	else
	{   le = (he = & pEntries [i]) - ((pEntries [i].nUAnodeX == nUAnodeX) ? 0 : 1);}

	lU = le ->fUAnodeR;
	hU = he ->fUAnodeR;

	if (le ->nUAnodeX != he ->nUAnodeX)
	{
	    return round_I32 (lU + ((((nUAnodeX   - le ->nUAnodeX) *
				   (hU		  - lU		 ))
				 / (he ->nUAnodeX - le ->nUAnodeX))));
	}
	if (nUAnodeX < le ->nUAnodeX)
	{
	    return round_I32 ((lU / le ->nUAnodeX) * nUAnodeX);
	}
	if (he ->nUAnodeX < nUAnodeX)
	{
	    return round_I32 ((hU / he ->nUAnodeX) * nUAnodeX);
	}
//	if ((le ->nUAnodeX <= nUAnodeX) && (nUAnodeX <= he ->nUAnodeX))
	{
	    return round_I32 ((lU + hU) / 2);
	}
    }}

    return nUAnodeX;
};

int CRRPUrp::UAnodeTable::GetUAnodeX10 (int fUAnodeR)
{
    if (0 < nEntries)
    {{
	Entry * le, * he;

	int  i; int lU, hU;

	for (i = 0; (i < nEntries) && (pEntries [i].fUAnodeR < fUAnodeR); i ++)
	{}

	if (i == nEntries)
	{   le =  he = & pEntries [i - 1];}
	else
	if (i ==	0)
	{   le =  he = & pEntries [0];}
	else
	{   le = (he = & pEntries [i]) - ((pEntries [i].fUAnodeR == fUAnodeR) ? 0 : 1);}

	lU = le ->nUAnodeX;
	hU = he ->nUAnodeX;

	if (le ->fUAnodeR != he ->fUAnodeR)
	{
	    return round_I32 (lU + ((((fUAnodeR   - le ->fUAnodeR) *
				   (hU		  - lU		 ))
				 / (he ->fUAnodeR - le ->fUAnodeR))));
	}
	if (fUAnodeR < le ->fUAnodeR)
	{
	    return round_I32 ((((float) lU) / le ->fUAnodeR) * fUAnodeR);
	}
	if (he ->fUAnodeR < fUAnodeR)
	{
	    return round_I32 ((((float) hU) / he ->fUAnodeR) * fUAnodeR);
	}
	if ((le ->fUAnodeR <= fUAnodeR) && (fUAnodeR <= he ->fUAnodeR))
	{
	    return round_I32 ((float)((lU * 16) + (hU * 16))/16/2);
	}
    }}

    return fUAnodeR;
};
//
//[]--------------------------------------------------------------------------
//
void CRRPUrp::LoadIFilamentTable (char * pBuf, Bool bBigFocus)
{
    GStreamBuf	s     (pBuf, ::lstrlen (pBuf));
		s.put (	     ::lstrlen (pBuf));

    int		nUA, nIF, nIA, c;

    IFilamentRow   * r;
    IFilamentEntry * e;

lNextUAnode :
	     s.ScanfA (" \t\r\n", NULL);
    if (1 != s.ScanfA (" \t", "<%d, ", & nUA))
	return;

    r = (bBigFocus) ? & m_IFilamentTableBF.pRows [m_IFilamentTableBF.nRows ++]
		    : & m_IFilamentTableSF.pRows [m_IFilamentTableSF.nRows ++];
    r ->nUAnodeX = nUA;
    r ->nEntries =   0;

lNextIF :
	     s.ScanfA (" \t\r\n", NULL);
    if (2 != s.ScanfA (" \t", "<%d,%d>", & nIF, & nIA))
	return;

    e = & r ->pEntries [r ->nEntries ++];
    e ->nIFilamentX = nIF;
    e ->nIAnodeR    = nIA;

	     s.ScanfA (" \t", NULL);
    if (',' == (c = s.GetChrA ()))
	goto lNextIF;
    else
    if ('>' !=  c)
	return;

	     s.ScanfA (" \t", NULL);

    if (',' == s.GetChrA ())
	goto lNextUAnode;

	     s.ScanfA (" \t\r\n", NULL);

    if (0 != s.sizeforget ())
    {
	return;
    }
	{{
	    IFilamentTable &	t = (bBigFocus) ? m_IFilamentTableBF
					        : m_IFilamentTableSF;

	    printf ("IFilament Table [%s] (%d) rows :\n", (bBigFocus) ? "BF" : "SF", t.nRows);

	for (int i = 0; i < t.nRows; i ++)
	{
	    printf ("%2d (%2d)", t.pRows [i].nUAnodeX,
				 t.pRows [i].nEntries);

	for (int j = 0; j < t.pRows [i].nEntries; j ++)
	{
	    printf ("{%d %d}",	 t.pRows [i].pEntries [j].nIFilamentX,
				 t.pRows [i].pEntries [j].nIAnodeR   );
	}
	    printf ("\n");
	}
	}}
};

void CRRPUrp::LoadUAnodeTable (char * pBuf)
{
    GStreamBuf	s     (pBuf, ::lstrlen (pBuf));
		s.put (	     ::lstrlen (pBuf));

    int		nUA, c;
    float	fUA;

lNextUAnode :
	     s.ScanfA (" \t\r\n", NULL);
    if (2 != s.ScanfA (" \t", "<%d,%f>", & nUA, & fUA))
	return;

    m_UAnodeTable.pEntries [m_UAnodeTable.nEntries].nUAnodeX = nUA * 10;
    m_UAnodeTable.pEntries [m_UAnodeTable.nEntries].fUAnodeR = fUA * 10;
			    m_UAnodeTable.nEntries ++;

	     s.ScanfA (" \t", NULL);
    if (',' == (c = s.GetChrA ()))
	goto lNextUAnode;

	{{
	    printf ("UAnode Table (%d) entries\n", m_UAnodeTable.nEntries);

	for (int i = 0; i < m_UAnodeTable.nEntries; i ++)
	{
	    printf ("\t%2d - %4.2f\n", m_UAnodeTable.pEntries [i].nUAnodeX,
				       m_UAnodeTable.pEntries [i].fUAnodeR);
	}
	    printf ("\n");
	}}
};

void CRRPUrp::LoadIniParam ()
{
    DWord   dBufSize ;
    char    pBuf [16 * 1024];

    GetIRRPSettings () ->GetValue (IRRPSettings::ID_URP_SF_UAnodeMin, & m_iUAnodeSFMin, sizeof (int), NULL);
    GetIRRPSettings () ->GetValue (IRRPSettings::ID_URP_SF_UAnodeMax, & m_iUAnodeSFMax, sizeof (int), NULL);

    GetIRRPSettings () ->GetValue (IRRPSettings::ID_URP_BF_UAnodeMin, & m_iUAnodeBFMin, sizeof (int), NULL);
    GetIRRPSettings () ->GetValue (IRRPSettings::ID_URP_BF_UAnodeMax, & m_iUAnodeBFMax, sizeof (int), NULL);

    GetIRRPSettings () ->GetValue (IRRPSettings::ID_URP_SF_IFilamentPreMin, & m_iIFilamentPreSFMin, sizeof (int), NULL);
    GetIRRPSettings () ->GetValue (IRRPSettings::ID_URP_SF_IFilamentPreMax, & m_iIFilamentPreSFMax, sizeof (int), NULL);
    GetIRRPSettings () ->GetValue (IRRPSettings::ID_URP_SF_IFilamentPreDef, & m_iIFilamentPreSFDef, sizeof (int), NULL);
    GetIRRPSettings () ->GetValue (IRRPSettings::ID_URP_SF_IFilamentMax	  , & m_iIFilamentSFMax	  , sizeof (int), NULL);

    GetIRRPSettings () ->GetValue (IRRPSettings::ID_URP_BF_IFilamentPreMin, & m_iIFilamentPreBFMin, sizeof (int), NULL);
    GetIRRPSettings () ->GetValue (IRRPSettings::ID_URP_BF_IFilamentPreMax, & m_iIFilamentPreBFMax, sizeof (int), NULL);
    GetIRRPSettings () ->GetValue (IRRPSettings::ID_URP_BF_IFilamentPreDef, & m_iIFilamentPreBFDef, sizeof (int), NULL);
    GetIRRPSettings () ->GetValue (IRRPSettings::ID_URP_BF_IFilamentMax	  , & m_iIFilamentBFMax	  , sizeof (int), NULL);

    GetIRRPSettings () ->GetValue (IRRPSettings::ID_URP_HeatTubeLo, & m_iQTubeLo, sizeof (int), NULL); m_iQTubeLo *= 1000;
    GetIRRPSettings () ->GetValue (IRRPSettings::ID_URP_HeatTubeHi, & m_iQTubeHi, sizeof (int), NULL); m_iQTubeHi *= 1000;
    GetIRRPSettings () ->GetValue (IRRPSettings::ID_URP_HeatTubeK , & m_iQTubeK , sizeof (int), NULL);

    GetIRRPSettings () ->GetValue (IRRPSettings::ID_URP_HeatDelay , & m_iHeatDelay, sizeof (int), NULL);
    GetIRRPSettings () ->GetValue (IRRPSettings::ID_URP_TExposeMax, & m_tExposeMax, sizeof (int), NULL);

    GetIRRPSettings () ->GetValue (IRRPSettings::ID_URP_HeatForceLo, & m_iQForceLo, sizeof (int), NULL); m_iQForceLo *= 1000;
    GetIRRPSettings () ->GetValue (IRRPSettings::ID_URP_HeatForceHi, & m_iQForceHi, sizeof (int), NULL); m_iQForceHi *= 1000;
    GetIRRPSettings () ->GetValue (IRRPSettings::ID_URP_HeatForceK , & m_iQForceK , sizeof (int), NULL);

    GetIRRPSettings () ->GetValue (IRRPSettings::ID_URP_UForceMin  , & m_iUForceLo, sizeof (int), NULL);

    GetIRRPSettings () ->GetValue (IRRPSettings::ID_URP_DelayParam_Full , & m_uDelay_Full , sizeof (_U32), NULL);
    GetIRRPSettings () ->GetValue (IRRPSettings::ID_URP_DelayParam_21x30, & m_uDelay_21x30, sizeof (_U32), NULL);
    GetIRRPSettings () ->GetValue (IRRPSettings::ID_URP_DelayParam_18x24, & m_uDelay_18x24, sizeof (_U32), NULL);
    GetIRRPSettings () ->GetValue (IRRPSettings::ID_URP_DelayParam_12x12, & m_uDelay_12x12, sizeof (_U32), NULL);

    GetIRRPSettings () ->GetValue (IRRPSettings::ID_URP_UACorrection	, & m_uUACorrection,sizeof (_U32), NULL);
    GetIRRPSettings () ->GetValue (IRRPSettings::ID_URP_IARetention     , & m_ParSetIAnodeMode.uValue,
											    sizeof (_U32), NULL);
							       SetRegValue (m_ParSetIAnodeMode	     , 
								            m_ParSetIAnodeMode.uValue);

    m_UAnodeTable.nEntries   =
    m_IFilamentTableSF.nRows =
    m_IFilamentTableBF.nRows = 0;

    if (ERROR_SUCCESS == GetIRRPSettings () ->GetValue
			(IRRPSettings::ID_URP_UACorrectionTable,
			 pBuf, sizeof (pBuf), & dBufSize))
    {
	LoadUAnodeTable (pBuf);
    }
    if (ERROR_SUCCESS == GetIRRPSettings () ->GetValue 
			(IRRPSettings::ID_URP_SF_IAnodeTable,
			 pBuf, sizeof (pBuf), & dBufSize))
    {
	LoadIFilamentTable (pBuf, False);
    }
    if (ERROR_SUCCESS == GetIRRPSettings () ->GetValue 
			(IRRPSettings::ID_URP_BF_IAnodeTable,
			 pBuf, sizeof (pBuf), & dBufSize))
    {
	LoadIFilamentTable (pBuf, True );
    }

    IRRPDevice::Param	Param; 
    Param.Init (IRRPUrp::ID_Focus	    , m_RegSetFocus     .uValue); SetParam (& Param);
    Param.Init (IRRPUrp::ID_ParSetFieldSize , m_RegSetFieldSize .uValue); SetParam (& Param);
//  Param.Init (IRRPUrp::ID_ParSetIAnodeMode, m_ParSetIAnodeMode.uValue); SetParam (& Param);
};
//
//[]------------------------------------------------------------------------[]
//
GIrp * CRRPUrp::Co_RunHVWaitOn (GIrp * pIrp, void *)
{
    if (pIrp ->Cancelled () || (ERROR_SUCCESS != pIrp ->GetCurResult ()))
    {	return pIrp;}

    if (0 == (m_RegStatus  .uValue & 0x0002))
    {
	pIrp ->SetResultInfo (-1, NULL);
	return pIrp;
    }
    if (     (m_RegGetBlk  .uValue & 0x007F7F7F))
    {
	pIrp ->SetResultInfo (-1, NULL);
	return pIrp;
    }
//
//............
//
    if ((0x0000 == (m_RegGetStatus.uValue & 0x0700))
    ||  (	   (m_RegGetStatus.uValue & 0x0400)))
    {
	return pIrp;
    }
	SetCurCoProc <CRRPUrp, void *>
       (pIrp, & CRRPUrp::Co_RunHVWaitOn, this, NULL);

//	printf ("...SetSetHVDone\n");

	QueueIrpForComplete (pIrp, 20);

	return NULL;
};

GIrp * CRRPUrp::Co_RunHVWaitReady (GIrp * pIrp, void *)
{
    if (pIrp ->Cancelled () || (ERROR_SUCCESS != pIrp ->GetCurResult ()))
    {	return pIrp;}

    if (0 == (m_RegStatus  .uValue & 0x0002))
    {
	pIrp ->SetResultInfo (-1, NULL);
	return pIrp;
    }
    if (     (m_RegGetBlk  .uValue & 0x007F7F7F))
    {
	pIrp ->SetResultInfo (-1, NULL);
	return pIrp;
    }
//
//............
//
    if ((0x0000 == (m_RegGetStatus.uValue & 0x0700)))
    {
//	printf ("...WaitSetHVReady %08X\n", m_RegGetStatus.uValue);

	SetCurCoProc <CRRPUrp, void *>
       (pIrp, & CRRPUrp::Co_RunHVWaitReady, this, NULL);

	QueueIrpForComplete (pIrp, 20);
	return NULL;
    }
    if (	   (m_RegGetStatus.uValue & 0x0100))
    {
	IRRPUrp_SetHV (IRRPUrp::HVOn);

	SetCurCoProc <CRRPUrp, void *>
       (pIrp, & CRRPUrp::Co_RunHVWaitOn	  , this, NULL);

	return pIrp;
    }
	pIrp ->SetResultInfo (-1, NULL);

	return pIrp;
};

GIrp * CRRPUrp::Co_RunHVStart (GIrp * pIrp, void *)
{
/* !!! */ //			    SetRegBits (m_RegGetStatusHV, 0x0000, 0x0001);

	SetCurCoProc <CRRPUrp, void *>
       (pIrp, & CRRPUrp::Co_RunHVWaitReady, this, NULL);

	IRRPUrp_SetHV (IRRPUrp::HVPrepare);

	printf ("[%08X] ...SetHV Started...\n", TCurrent () ->GetId ());

	QueueIrpForComplete (pIrp, 100);

	return NULL;
};

GIrp * CRRPUrp::Co_RunHVDone (GIrp * pIrp, void *)
{
    if (pIrp ->Cancelled ()  && (ERROR_SUCCESS == pIrp ->GetCurResult ()))
    {
	pIrp ->SetResultInfo (ERROR_CANCELLED, NULL);
    }
    if (ERROR_SUCCESS == pIrp ->GetCurResult ())
    {
	pIrp ->SetCurResultInfo (ERROR_SUCCESS, (void *) m_RegGetIAnodeNeg.uValue);
    }
	IRRPUrp_SetHV (IRRPUrp::HVRelease);

				    SetRegBits (m_RegGetStatusHV, 0x0001, 0x0000);

	printf ("[%08X] ...SetHV Done... with %08X\n", TCurrent () ->GetId (), pIrp ->GetCurResult ());

	m_pShotIrp.Exchange		 (NULL);

	return pIrp;
};

void CRRPUrp::IRRPUrp_RunHV (GIrp * pIrp)
{
    if (m_pShotIrp.CompareExchange (pIrp, NULL) != NULL)
    {
	pIrp ->SetCurResultInfo (ERROR_BUSY   , NULL);

        QueueIrpForComplete (pIrp);

	return;
    }
	pIrp ->SetCurResultInfo (ERROR_SUCCESS, NULL);

    	SetCurCoProc <CRRPUrp, void *>
       (pIrp, & CRRPUrp::Co_RunHVDone     , this, NULL);

	SetCurCoProc <CRRPUrp, void *>
       (pIrp, & CRRPUrp::Co_RunHVStart	, this, NULL);

/* !!! */			    SetRegBits (m_RegGetStatusHV, 0x0000, 0x0001);

       ((GApartment *) s_pRRPService) ->IApartment_QueueIrpForComplete (m_pShotIrp, 500);
};

void CRRPUrp::IRRPUrp_CheckExposition (Bool bBigFocus, int * pUAnodeR, int * pIAnodeR)
{
    int	  nUAnodeX10 = 0;
    int	  nUAnodeX   = 0;
    int * pUAnodeX   = NULL;

    if (pUAnodeR)
    {
	nUAnodeX   =
      ((nUAnodeX10 = (m_uUACorrection) ? m_UAnodeTable.GetUAnodeX10 (* pUAnodeR * 10)
				       :			    (* pUAnodeR * 10)) + 5) / 10;
        pUAnodeX   = & nUAnodeX;
    }

    (bBigFocus) ? m_IFilamentTableBF.SetExposition (pUAnodeX, pIAnodeR)
		: m_IFilamentTableSF.SetExposition (pUAnodeX, pIAnodeR);

    if (pUAnodeR)
    {
	if (((nUAnodeX10 + 5) / 10) != nUAnodeX)
	{    nUAnodeX10		    =  nUAnodeX * 10;}

      * pUAnodeR   = GetUAnodeR (nUAnodeX10);
    }
};

int CRRPUrp::IRRPUrp_GetUAnodeX10 (int fUAnodeR10)
{
    return (m_uUACorrection) ? m_UAnodeTable.GetUAnodeX10 (fUAnodeR10)
			     :				   fUAnodeR10;

//  return GetUAnodeX (fUAnodeR * 10);
/*
    return (m_uUACorrection) ? m_UAnodeTable.GetUAnodeX (fUAnodeR * 10)
			     :				 fUAnodeR;
*/
};

int CRRPUrp::IRRPUrp_GetUAnodeR10 (int nUAnodeX10)
{
    return (m_uUACorrection) ? m_UAnodeTable.GetUAnodeR10 (nUAnodeX10)
			     :				   nUAnodeX10;

//  return GetUAnodeR (nUAnodeX * 10);
/*
    return (m_uUACorrection) ? round_I32 (m_UAnodeTable.GetUAnodeR 
							(nUAnodeX))
			     :				 nUAnodeX;
*/
};

void CRRPUrp::On_IRRPCallBack (const void * hIRRPCallBack, _U32 uParamId, _U32 uValue)
{
// Proxy notifications :
//
    switch (uParamId)
    {
    case KKM_DEVICE_ID :
	OnAckValue (m_RegKKMStatus	,(uValue & (~ IRRPDevice::IRRPDevice_BusOnline)) 
		 | (m_RegStatus		 .uValue &    IRRPDevice::IRRPDevice_BusOnline));
	break;

    case IRRPKKM::ID_RegGetPower :
	OnAckValue (m_RegGetPower	, uValue);
	break;

    case IRRPKKM::ID_RegGetU220 :
	OnAckValue (m_RegGetU220	, uValue);
	break;

    case IRRPKKM::ID_RegGetUForce :
	OnAckValue (m_RegGetUForce	, uValue);
	break;

    case IRRPKKM::ID_RegGetICharging :
	OnAckValue (m_RegGetICharging	, uValue);
	break;
    
    case IRRPKKM::ID_RegGetTForceItems :
	OnAckValue (m_RegGetTForce	, uValue);
	break;
    };
};

Bool CRRPUrp::On_SecondTimer ()
{
    if (0 < m_iQTubeCur)
    {
	m_iQTubeCur -= m_iQTubeK;
    }
    if (m_RegHeatTubeTimeout.uValue)
    {
	SetRegValue (m_RegHeatTubeTimeout,
	m_RegHeatTubeTimeout.uValue  - 1);
    }

    if (0 < m_iQForceCur)
    {
	m_iQForceCur -= m_iQForceK;
    }
    if (m_RegHeatForceTimeout.uValue)
    {
	SetRegValue (m_RegHeatForceTimeout,
        m_RegHeatForceTimeout.uValue - 1);
    }

	return True;
};

Bool GAPI CRRPUrp::On_SecondTimer (IWakeUp *,  IWakeUp::Reason 
						     eOnReason,
					      void * pContext ,
					      void * pArgument)
{
    if (eOnReason == IWakeUp::OnTimeout)
    {
	return ((CRRPUrp *) pContext) ->On_SecondTimer ();
    }
	return False;
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
#define MM2_DEVICE_ID	    0x91000000

class CRRPMM2 : public CRRPDevice
{
public :
		    CRRPMM2 ();

	void	    Initialize		();

	void		    SetParam	    (const IRRPDevice::Param *);

	void		    OnAckValue	    (Register &, _U32);

	friend
	class IRRPMM2_Entry;
	friend
	class TInterface_Entry <IRRPMM2, CRRPMM2>;
	class IRRPMM2_Entry : public
	      TInterface_Entry <IRRPMM2, CRRPMM2>
	{
	STDMETHOD_ (GResult,	    Connect)(IRRPCallBack::Handle *&
							    hICallBack ,
					     IRRPCallBack * pICallBack )
	{
	    return GetT () ->IRRPDevice_Connect	  (hICallBack ,
						   pICallBack );
	};
	STDMETHOD_ (void,	   SetParam)(	   int	    nParams,
					     const Param *  pParams)
	{
		   GetT () ->IRRPDevice_SetParam  (nParams, pParams);
	};
	STDMETHOD_ (void,	   GetParam)(	   int	    nParams,
						   Param *  pParams)
	{
		   GetT () ->IRRPDevice_GetParam  (nParams, pParams);
	};
	};

	void		    Reset	    ();

protected :

	CRRPSerialBus::SendResult   PutSendOOBRequest
					    (	   Byte *& pTail    ,
					     const Byte *  pHigh    );
protected :

	CRRPSerialDevice::Register	m_RegGetThick	    ;
	CRRPSerialDevice::Register	m_RegGetPress	    ;
	CRRPSerialDevice::Register	m_RegGetRotate	    ;

	CRRPSerialDevice::Register	m_RegGetMoveStatus  ;
	CRRPSerialDevice::Register	m_RegGetPressStatus ;

	CRRPSerialDevice::Register	m_RegSetMotors	    ;

	CRRPSerialDevice::Register	m_RegSetMove	    ;
	CRRPSerialDevice::Register	m_RegSetRotate	    ;

	CRRPSerialDevice::Register	m_RegSetPress	    ;

	CRRPSerialDevice::Register	m_RegSetPressHigh   ;

	_U32				m_ValGetPress	    ;

public :
	Bool				m_bValSetPressHigh  ;
	int				m_dValSetPressHigh  ;
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
CRRPMM2::CRRPMM2 () : CRRPDevice (MM2_DEVICE_ID)
{
    IRRPMM2_Entry     IEntry;

    * ((void **) & m_IRRPDevice) = * ((void **) & IEntry);

    m_RegList.append (m_RegGetThick.
		      DefineROParam_16	(_WREG_MASK (0x7F,0x93, 0x01,0x00)));
    m_RegList.append (m_RegGetPress.
		      DefineROParam_16	(_WREG_MASK (0x7F,0x94, 0x00,0x00)));
    m_RegList.append (m_RegGetRotate.
		      DefineROReg_16	(_WREG_MASK (0x7F,0xBD, 0x43,0xBC)));

    m_RegList.append (m_RegSetMotors.
		      DefineSWReg_16	(_WREG_MASK (0x00,0x00, 0x3F,0x01)));
    
    m_RegList.append (m_RegGetMoveStatus.
		      DefineROReg_16	(_WREG_MASK (0x1F,0x92, 0x0F,0x91)));
    m_RegList.append (m_RegGetPressStatus.
		      DefineROReg_16	(_WREG_MASK (0x00,0x00, 0x37,0xBF)));

//  m_RegList.append (m_RegSetMove.
//		      DefineRWReg_16	(_WREG_MASK (0x00,0x00, 0x03,0x91)));
//  m_RegList.append (m_RegSetRotate.
//		      DefineRWReg_16	(_WREG_MASK (0x00,0x00, 0x03,0x92)));

    m_RegList.append (m_RegSetPress.
		      DefineRWReg_16	(_WREG_MASK (0x00,0x00, 0x04,0xBF)));

    m_RegList.append (m_RegSetPressHigh.
		      DefineRWParam_16	(_WREG_MASK (0x00,0x00, 0x7F,0xC3)));

    m_ValGetPress = 0x00000000;

    m_bValSetPressHigh = True;
    m_dValSetPressHigh = (5 * 2);
};

void CRRPMM2::Initialize ()
{
    CRRPConfigFile::LoadMM2Param (m_dValSetPressHigh);
};

void CRRPMM2::Reset ()
{
    SetRegValue (m_RegSetMotors, 0x00);

    m_bValSetPressHigh = True;

    CRRPDevice::Reset ();
};

CRRPSerialBus::SendResult
CRRPMM2::PutSendOOBRequest (Byte *& pTail, const Byte * pHigh)
{
    TStreamBuf <64> CmdBuf  ;

    if (m_RegSetMotors.uValue & 0x01)
    {	
	CmdBuf.putBigEndianW (0x0401);
	CmdBuf.putBigEndianW (0x9101);
	m_OOBSendBuf.Put     ((XferBuf::Pack *) CmdBuf.head ());

	CmdBuf.clear	     ();
    }
    else 
    if (m_RegSetMotors.uValue & 0x02)
    {
	CmdBuf.putBigEndianW (0x0401);
	CmdBuf.putBigEndianW (0x9102);
	m_OOBSendBuf.Put     ((XferBuf::Pack *) CmdBuf.head ());

	CmdBuf.clear	     ();
    }
//
//.....................................
//
    if (m_RegSetMotors.uValue & 0x04)
    {	
	CmdBuf.putBigEndianW (0x0402);
	CmdBuf.putBigEndianW (0x9202);
	m_OOBSendBuf.Put     ((XferBuf::Pack *) CmdBuf.head ());

	CmdBuf.clear	     ();
    }
    else 
    if (m_RegSetMotors.uValue & 0x08)
    {
	CmdBuf.putBigEndianW (0x0402);
	CmdBuf.putBigEndianW (0x9201);
	m_OOBSendBuf.Put     ((XferBuf::Pack *) CmdBuf.head ());

	CmdBuf.clear	     ();
    }
//
//.....................................
//
    if (m_RegSetMotors.uValue & 0x10)
    {	
	CmdBuf.putBigEndianW (0x0402);
	CmdBuf.putBigEndianW (0xBF02);
	m_OOBSendBuf.Put     ((XferBuf::Pack *) CmdBuf.head ());

	CmdBuf.clear	     ();
    }
    else 
    if (m_RegSetMotors.uValue & 0x20)
    {
	CmdBuf.putBigEndianW (0x0402);
	CmdBuf.putBigEndianW (0xBF01);
	m_OOBSendBuf.Put     ((XferBuf::Pack *) CmdBuf.head ());

	CmdBuf.clear	     ();
    }

    return CRRPDevice::PutSendOOBRequest (pTail, pHigh);
};

//static const Byte _C701 [] = {0x04, 0xC7, 0xC7, 0x01};	// Led Ready
//static const Byte _C702 [] = {0x04, 0xC7, 0xC7, 0x02};	// Led Alarm
//static const Byte _C704 [] = {0x04, 0xC7, 0xC7, 0x04};	// Led Shot
//static const Byte _C700 [] = {0x04, 0xC7, 0xC7, 0x00};	// Led normal

static const Byte _BF01 [] = {0x04, 0x02, 0xBF, 0x01};	// Press Dn
static const Byte _BF02 [] = {0x04, 0x02, 0xBF, 0x02};	// Press Up
static const Byte _BF00 [] = {0x04, 0x02, 0xBF, 0x00};	// Press Stop.

static const Byte _9204 [] = {0x04, 0x02, 0x92, 0x04};	// Rot Left 90
static const Byte _9220 [] = {0x04, 0x02, 0x92, 0x20};	// Rot Left 45
static const Byte _9208 [] = {0x04, 0x02, 0x92, 0x08};	// Rot Midle
static const Byte _9240 [] = {0x04, 0x02, 0x92, 0x40};	// Rot Right 45
static const Byte _9210 [] = {0x04, 0x02, 0x92, 0x10};	// Rot Right 90

void CRRPMM2::SetParam (const IRRPDevice::Param * pParam)
{
printf ("[%08X] <- %08X (%d)\n", pParam ->uParamId, pParam ->u32Value, pParam ->u32Value);

    const Byte * pOOB = NULL;

    switch (pParam ->uParamId)
    {
    case IRRPMM2::ID_SetPressMode :
	SetRegBits (m_RegSetPress,  0x04, (0 == pParam ->u32Value) ? 0x00 : 0x04);
	break;

    case IRRPMM2::ID_RunMove :
	SetRegBits (m_RegSetMotors, 0x03,((0 == pParam ->i32Value) ? 0x00 :
					 ((0 <  pParam ->i32Value) ? 0x01 : 0x02)));
	break;

    case IRRPMM2::ID_RunRotate :
	SetRegBits (m_RegSetMotors, 0x0C,((0 == pParam ->i32Value) ? 0x00 :
					 ((0 <  pParam ->i32Value) ? 0x04 : 0x08)));
	break;

    case IRRPMM2::ID_RunRotateFix :
	switch (pParam ->i32Value)
	{
	case IRRPMM2::RotLt90 : pOOB = _9204; break;
	case IRRPMM2::RotLt45 : pOOB = _9220; break;
	case IRRPMM2::RotMd   : pOOB = _9208; break;
	case IRRPMM2::RotRt45 : pOOB = _9240; break;
	case IRRPMM2::RotRt90 : pOOB = _9210; break;
	};
	if (pOOB)
	    m_OOBSendBuf.PutOver ((XferBuf::Pack *) pOOB);

	break;

    case IRRPMM2::ID_RunPress :
/* No pulse control !!!
	m_RegSetMotors.SetReqBits  (0x30,((0 == pParam ->i32Value) ? 0x00 :
					 ((0 <	pParam ->i32Value) ? 0x10 : 0x20)));
*/
/*
	m_OOBSendBuf.PutOver ((XferBuf::Pack *)
					 ((0 == pParam ->i32Value) ? _BF00 :
					 ((0 <  pParam ->i32Value) ? _BF02 : _BF01)));
*/
	if	(pParam ->i32Value == 0) pOOB = _BF00;
	else if (pParam ->i32Value  > 0) pOOB = _BF02;
	else 				 pOOB = _BF01;
	
	m_OOBSendBuf.PutOver ((XferBuf::Pack *)	pOOB);

	break;

    case IRRPMM2::ID_RegSetPressHigh :
	SetRegValue (m_RegSetPressHigh, m_dValSetPressHigh = pParam ->i32Value);
	break;
    };
};

void CRRPMM2::OnAckValue (CRRPSerialDevice::Register & r, _U32 uValue)
{
    if (& r == & m_RegGetPress)
    {
	if (0 == (0x40 & uValue))
	{
	    m_ValGetPress &=  0x80000FC0;
	    m_ValGetPress |= (0x40000000 | ((uValue & 0x3F) << 0));
	}
	else
	if (m_ValGetPress &   0x40000000)
	{
	    m_ValGetPress &=  0x4000003F;
	    m_ValGetPress |= (0x80000000 | ((uValue & 0x3F) << 4)); // * 16 ??? (Sheet !!!)
	}

	if (((m_BusTrMask   & 0x03	) != 0x03      )
	||  ((m_ValGetPress & 0xC0000000) != 0xC0000000))
	{
	    return;
	}

	    uValue	   =
	   (m_ValGetPress &=  0x00000FFF);
    }
    if (& r == & m_RegGetRotate)
    {
	uValue = ((int)(((_U32)HIBYTE(LOWORD(uValue))) <<   2) 
		 |     (((_U32)LOBYTE(LOWORD(uValue))) & 0x03)) - ((uValue & 0x0040) ? 180 : 135);
    }
#if True

    if (& r == & m_RegSetPressHigh)
    {
    if (m_dValSetPressHigh != uValue)
    {
	if (m_bValSetPressHigh && (uValue == (5 * 2)))
	{
	    SetRegValue (m_RegSetPressHigh, m_dValSetPressHigh);
	}
	else
	{
	    m_dValSetPressHigh = uValue;
	}
    }
	    m_bValSetPressHigh = False ;
    } 

#else
//	Has been commented
    if ((& r == & m_RegSetPressHigh) && (m_RegSetPressHigh.uValue != uValue))
    {
printf ("m_RegSetPress %04X != %04X\n", m_RegSetPressHigh.uValue, uValue);
    }
//
#endif
    CRRPDevice::OnAckValue (r, uValue);
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
class CRRPStepMotor : public CRRPDevice
{
public :
		    CRRPStepMotor (_U32 uDegiceId);

	friend
	class IRRPMMotor_Entry;
	friend
	class TInterface_Entry <IRRPMMotor, CRRPStepMotor>;
	class IRRPMMotor_Entry : public
	      TInterface_Entry <IRRPMMotor, CRRPStepMotor>
	{
	STDMETHOD_ (GResult,	    Connect)(IRRPCallBack::Handle *&
							    hICallBack ,
					     IRRPCallBack * pICallBack )
	{
	    return GetT () ->IRRPDevice_Connect	  (hICallBack ,
						   pICallBack );
	};
	STDMETHOD_ (void,	   SetParam)(	   int	    nParams,
					     const Param *  pParams)
	{
		   GetT () ->IRRPDevice_SetParam  (nParams, pParams);
	};
	STDMETHOD_ (void,	   GetParam)(	   int	    nParams,
						   Param *  pParams)
	{
		   GetT () ->IRRPDevice_GetParam  (nParams, pParams);
	};
	STDMETHOD_ (void,	        Run)(IRRPMMotor::RunCommand cmd, int nStep)
	{
	    GetT () ->IRRPMMotor_Run (cmd, nStep);
	};
	STDMETHOD_ (void,      SetParamBits)(_U32 uParamId, _U32 ClrMask,
							    _U32 SetMask)
	{
	    GetT () ->IRRPMMotor_SetParamBits (uParamId, ClrMask, SetMask);
	};
	STDMETHOD_ (_U32,	   GetParam)(_U32 uParamId)
	{
	    return GetT () ->IRRPMMotor_GetParam (uParamId);
	};
	};


	void	    IRRPMMotor_Run		(IRRPMMotor::RunCommand, int);

	void	    IRRPMMotor_SetParamBits   (_U32, _U32, _U32);

virtual	_U32	    IRRPMMotor_GetParam	(_U32);	

	void	    Reset		();

protected :

	CRRPSerialBus::SendResult   PutSendRequest
					    (	   Byte *& pTail	 ,
					     const Byte *  pHigh	 );

	void	    LoadIniParam	();
/*
	CRRPSerialBus::SendResult   
			    OnSend	(CRRPSerialBus::SendReason	 ,
						   Byte *& pTail	 ,
					     const Byte *  pHigh	 );
*/
	void		    SetParam	(const IRRPDevice::Param *);

protected :

	CRRPSerialDevice::Register	m_RegGetStatus	    ;
	CRRPSerialDevice::Register	m_RegGetPosition    ;

	CRRPSerialDevice::Register	m_RegSetControl	    ;
	CRRPSerialDevice::Register	m_RegSetStep	    ;
	CRRPSerialDevice::Register	m_RegSetVelocity    ;
	CRRPSerialDevice::Register	m_RegSetAccelTime   ;
	CRRPSerialDevice::Register	m_RegSetDefCoeff    ;
	CRRPSerialDevice::Register	m_RegSetSyncDelay   ;
	CRRPSerialDevice::Register	m_RegSetShotRows    ;

	CRRPSerialDevice::Register	m_RegRunCommand	    ;

	int				m_uSetSyncDelay	    ;
	int				m_uSetDefCoeff	    ;
	int				m_uSetAccelTime	    ;
	int				m_uSetVelocity	    ;
	int				m_uSetStepPerDec    ;
};
//
//[]------------------------------------------------------------------------[]
//
CRRPSerialBus::SendResult
CRRPStepMotor::PutSendRequest (Byte *& pTail, const Byte * pHigh)
{
    Bool    bSetAddr  = False ;

	if (m_RegCur == NULL)
	{
	    m_RegCur  = m_RegList.first ();
	}

#if TRACE_SEND
    {{	Byte * pMarker = pTail;
#endif

    for (;  m_RegCur; m_RegCur = m_RegList.getnext (m_RegCur))
    {
	if (pHigh < pTail + 8)
	{
	    return CRRPSerialBus::QueueForSend;
	}
	if (m_RegCur ->uFlags & FREG_ReqToSend)
	{
	    if (! bSetAddr)
	    {
	      * pTail ++ = 0xD6;
	      * pTail ++ = LOBYTE (LOWORD (m_RegSetControl.uValue |
					 ((0xD6000000 == m_uDeviceId) ? 0x00 : 0x01)));
		bSetAddr = True;
	    }

	    if ((m_RegCur == & m_RegSetControl))
	    {
	      * pTail ++ = 0xD6;
	      * pTail ++ = LOBYTE (LOWORD (m_RegSetControl.uValue |
					 ((0xD6000000 == m_uDeviceId) ? 0x00 : 0x01)));

	        m_RegSetControl.uFlags &= (~FREG_ReqToSend);
	    }
	    else
	    if (m_RegCur == & m_RegRunCommand)
	    {
	      * pTail ++ = 0xD6;
	      * pTail ++ = LOBYTE (LOWORD (m_RegSetControl.uValue |
					   m_RegRunCommand.uValue |
					 ((0xD6000000 == m_uDeviceId) ? 0x00 : 0x01)));

	        m_RegRunCommand.uFlags &= (~FREG_ReqToSend);
	    }
	    else
	    {
		m_RegCur ->PutReqValue (pTail);
	    }
	}
    }

#if TRACE_SEND
	if (pMarker < pTail)
	{
printf ("...Send [");

	for (;pMarker < pTail; pMarker += 2)
	{
	    printf (" %02X %02X", * (pMarker + 0), * (pMarker + 1));
	}
printf ("]\n");
	}
    }}
#endif
	    return CRRPSerialBus::NoMoreData ;
};

_U32 CRRPStepMotor::IRRPMMotor_GetParam (_U32 uParamId)
{
    switch (uParamId)
    {
    case IRRPMMotor::ID_RegSetVelocity :
	return m_uSetVelocity;
	break;

    case IRRPMMotor::ID_RegSetAccelTime :
	return m_uSetAccelTime;
	break;

    case IRRPMMotor::ID_RegSetDefCoeff :
	return m_uSetDefCoeff;
	break;

    case IRRPMMotor::ID_RegSetSyncDelay :
	return m_uSetSyncDelay;
	break;

    case 0x0000DEDF :
	return m_uSetStepPerDec;
    };
	return 0xFFFFFFFF;
};

CRRPStepMotor::CRRPStepMotor (_U32 uDeviceId) : CRRPDevice (uDeviceId)
{
    IRRPMMotor_Entry	IEntry;

    * ((void **) & m_IRRPDevice) = * ((void **) & IEntry);

    m_RegList.append (m_RegGetStatus.
		      DefineROReg_16	(_WREG_MASK (0x07,0xD7, 0x1E,0xD6)));
    m_RegList.append (m_RegGetPosition.
		      DefineROParam_32	(_WREG_MASK (0x7F,0xDE, 0x7F,0xDF),
					 _WREG_MASK (0x00,0x00, 0x30,0xD7)));
    m_RegList.append (m_RegSetControl.
		      DefineRWReg_16	(_WREG_MASK (0x00,0x00, 0x1E,0xD6)));
    m_RegList.append (m_RegSetStep.
		      DefineWOParam_16	(_WREG_MASK (0x7F,0xD8, 0x7F,0xD9)));
    m_RegList.append (m_RegSetVelocity.
		      DefineRWParam_16	(_WREG_MASK (0x7F,0xDA, 0x7F,0xDB)));
    m_RegList.append (m_RegSetAccelTime.
		      DefineRWParam_16  (_WREG_MASK (0x7F,0xDC, 0x7F,0xDD)));
    m_RegList.append (m_RegSetDefCoeff.
		      DefineRWParam_16	(_WREG_MASK (0x00,0x00, 0x7F,0xBD)));
    m_RegList.append (m_RegSetSyncDelay.
		      DefineRWParam_16	(_WREG_MASK (0x00,0x00, 0x7F,0xBE)));
    m_RegList.append (m_RegSetShotRows.
		      DefineRWParam_16	(_WREG_MASK (0x00,0x00, 0x7F,0xB8)));

    m_RegList.append (m_RegRunCommand.
		      DefineWOReg_16	(_WREG_MASK (0x00,0x00, 0x60,0xD6)));

    m_uSetSyncDelay  =
    m_uSetDefCoeff   =
    m_uSetAccelTime  =
    m_uSetVelocity   =
    m_uSetStepPerDec = 0;

    m_RegSetShotRows.uValue = 0x7F  ;
};

void CRRPStepMotor::Reset ()
{
    CRRPDevice::Reset ();

    SetRegValue (m_RegSetSyncDelay, m_uSetSyncDelay);
    SetRegValue (m_RegSetDefCoeff , m_uSetDefCoeff );
    SetRegValue (m_RegSetAccelTime, m_uSetAccelTime);
    SetRegValue (m_RegSetVelocity , m_uSetVelocity );
    SetRegValue (m_RegSetShotRows , 
		 m_RegSetShotRows . uValue	   );

    SetRegValue (m_RegSetControl  , 0x08 | 0x02);

    SetRegValue (m_RegSetStep	  , 15000);
};

void CRRPStepMotor::SetParam (const IRRPDevice::Param * pParam)
{
printf ("[%08X] <- %08X (%d)\n", pParam ->uParamId, pParam ->u32Value, pParam ->u32Value);

    switch (pParam ->uParamId)
    {
    case IRRPMMotor::ID_RegSetSyncDelay :
	SetRegValue (m_RegSetSyncDelay, m_uSetSyncDelay = pParam ->i32Value);
	break;

    case IRRPMMotor::ID_RegSetDefCoeff :
	SetRegValue (m_RegSetDefCoeff , m_uSetDefCoeff  = pParam ->i32Value);
	break;

    case IRRPMMotor::ID_RegSetAccelTime :
	SetRegValue (m_RegSetAccelTime, m_uSetAccelTime = pParam ->i32Value);
	break;

    case IRRPMMotor::ID_RegSetVelocity :
	SetRegValue (m_RegSetVelocity , m_uSetVelocity  = pParam ->i32Value);
	break;

    case IRRPMMotor::ID_RegSetShotRows :
	SetRegValue (m_RegSetShotRows ,			  pParam ->i32Value);
	break;

    case 0x0000DEDF :
	m_uSetStepPerDec = pParam ->i32Value;
	{{
	    DWord   pCfgBuf [8] = {8 * sizeof (DWord), m_uDeviceId, m_uSetSyncDelay ,
								    m_uSetDefCoeff  ,
								    m_uSetAccelTime ,
								    m_uSetVelocity  ,
								    m_uSetStepPerDec,
								    0x7F	    };
	}}
	break;
    };
};

void CRRPStepMotor::IRRPMMotor_Run (IRRPMMotor::RunCommand dir, int nStep)
{
#if FALSE

    TStreamBuf <64> CmdBuf ;

    switch (dir)
    {
    case IRRPMMotor::Stop :

	CmdBuf._PutB (0x04);
	CmdBuf._PutB (0x01);	

	CmdBuf._PutB (HIBYTE(HIWORD(m_uDeviceId)));
	CmdBuf._PutB (0x00);

	m_OOBSendBuf.Put    ((XferBuf::Pack *) CmdBuf.Head ());

//	m_RegRunCommand.SetReqValue (0x00 | m_RegSetControl.uValue);
	break;

    case IRRPMMotor::RunForward :

	CmdBuf._PutB (0x08);
	CmdBuf._PutB (0x01);

	CmdBuf._PutB (0xD9);
	CmdBuf._PutB (LOBYTE(LOWORD(nStep)));

	CmdBuf._PutB (0xD8);
	CmdBuf._PutB (HIBYTE(LOWORD(nStep)));

	CmdBuf._PutB (HIBYTE(HIWORD(m_uDeviceId)));
	CmdBuf._PutB (0x20);

	m_OOBSendBuf.Put    ((XferBuf::Pack *) CmdBuf.Head ());

//	m_RegSetStep   .SetReqValue (nStep);
//	m_RegRunCommand.SetReqBits  (0x60, 0x20);
	break;

    case IRRPMMotor::RunBackward :

//	m_RegSetStep   .SetReqValue (nStep);
//	m_RegRunCommand.SetReqBits  (0x60, 0x40);
	break;
    };

#else

    switch (dir)
    {
    case IRRPMMotor::Stop :
	SetRegValue (m_RegRunCommand, 0x00 | m_RegSetControl.uValue);
	break;

    case IRRPMMotor::RunForward :
	SetRegValue (m_RegSetStep   , nStep);
	SetRegBits  (m_RegRunCommand, 0x60, 0x20);
	break;

    case IRRPMMotor::RunBackward :
	SetRegValue (m_RegSetStep   , nStep);
	SetRegBits  (m_RegRunCommand, 0x60, 0x40);
	break;
    };

#endif
};

void CRRPStepMotor::IRRPMMotor_SetParamBits (_U32 uParamId, _U32 ClrMask,
							    _U32 SetMask)
{
    if (uParamId == 0x000000D6)
    {
	SetRegBits  (m_RegSetControl, ClrMask & 0x0000001E,
				      SetMask & 0x0000001E);
    }
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
#define MDIAFR_DEVICE_ID    0xD7000000

class CRRPMDiafr : public CRRPStepMotor
{
public :
		    CRRPMDiafr ();

	void		LoadIniParam	    ();

	void		Initialize	    ();
};
//
//[]------------------------------------------------------------------------[]
//
CRRPMDiafr::CRRPMDiafr () : CRRPStepMotor (MDIAFR_DEVICE_ID)
{};

void CRRPMDiafr::LoadIniParam ()
{
    GetIRRPSettings () ->GetValue (IRRPSettings::ID_MSC_Diafr_SyncDelay, & m_uSetSyncDelay , sizeof (int), NULL);
    GetIRRPSettings () ->GetValue (IRRPSettings::ID_MSC_Diafr_DefCoeff , & m_uSetDefCoeff  , sizeof (int), NULL);
    GetIRRPSettings () ->GetValue (IRRPSettings::ID_MSC_Diafr_AccelTime, & m_uSetAccelTime , sizeof (int), NULL);
    GetIRRPSettings () ->GetValue (IRRPSettings::ID_MSC_Diafr_Velocity,  & m_uSetVelocity  , sizeof (int), NULL);
    GetIRRPSettings () ->GetValue (IRRPSettings::ID_MSC_Diafr_StepPerDec,& m_uSetStepPerDec, sizeof (int), NULL);

    IRRPDevice::Param	Param;

    Param.Init (IRRPMMotor::ID_RegSetSyncDelay, m_uSetSyncDelay); SetParam (& Param);
    Param.Init (IRRPMMotor::ID_RegSetDefCoeff , m_uSetDefCoeff ); SetParam (& Param);
    Param.Init (IRRPMMotor::ID_RegSetAccelTime, m_uSetAccelTime); SetParam (& Param);
    Param.Init (IRRPMMotor::ID_RegSetVelocity , m_uSetVelocity ); SetParam (& Param);
};

void CRRPMDiafr::Initialize ()
{
    CRRPStepMotor::Initialize ();

    LoadIniParam ();
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
#define	MSCAN_DEVICE_ID	    0xD6000000

class CRRPMScan : public CRRPStepMotor
{
public :
		    CRRPMScan ();

	void		LoadIAnodeMax	    ();

	void		LoadIniParam	    ();

	void		Initialize	    ();

	void		CheckExposition	    (int * nUAnode, int * nIAnode);

protected :

	void		SetParam	    (const IRRPDevice::Param *);


	CRRPSerialBus::SendResult   PutSendRequest
					    (	   Byte *& pTail      ,
					     const Byte *  pHigh      );

	CRRPSerialBus::SendResult   PutRecvRequest
					    (	   Byte *&  pTail     ,
					     const Byte *   pHigh     );

	CRRPSerialBus::RecvResult   OnRecv  (CRRPSerialBus::RecvReason,
					     const Byte *&  pHead     ,
					     const Byte *   pTail     );

public :

	void		Reset		    ();

	friend
	class IRRPMScan_Entry;
	friend
	class TInterface_Entry <IRRPMScan, CRRPMScan>;
	class IRRPMScan_Entry : public
	      TInterface_Entry <IRRPMScan, CRRPMScan>
	{
	STDMETHOD_ (GResult,	    Connect)(IRRPCallBack::Handle *&
							    hICallBack ,
					     IRRPCallBack * pICallBack )
	{
	    return GetT () ->IRRPDevice_Connect	  (hICallBack ,
						   pICallBack );
	};
	STDMETHOD_ (void,	   SetParam)(	   int	    nParams,
					     const Param *  pParams)
	{
		   GetT () ->IRRPDevice_SetParam  (nParams, pParams);
	};
	STDMETHOD_ (void,	   GetParam)(	   int	    nParams,
						   Param *  pParams)
	{
		   GetT () ->IRRPDevice_GetParam  (nParams, pParams);
	};
	STDMETHOD_ (void,	        Run)(IRRPMMotor::RunCommand cmd, int nStep)
	{
	    GetT () ->IRRPMMotor_Run (cmd, nStep);
	};
	STDMETHOD_ (void,      SetParamBits)(_U32 uParamId, _U32 ClrMask,
							    _U32 SetMask)
	{
	    GetT () ->IRRPMScan_SetParamBits (uParamId, ClrMask, SetMask);
	};
	STDMETHOD_ (_U32,	   GetParam)(_U32 uParamId)
	{
	    return GetT () ->IRRPMScan_GetParam (uParamId);
	};
	STDMETHOD_ (DWord,	 GetDataPtr)(void *& pHead, DWord dOffset,
							    DWord dLength)
	{
	    return GetT () ->IRRPMScan_GetDataPtr (pHead, dOffset, dLength);
	};
	STDMETHOD_ (void,      GetTestImage)()
	{
	    return GetT () ->IRRPMScan_GetTestImage ();
	};
	};

static	void GAPI   XPGrabberCallBack		(void * , DWord, DWord);

	void	    XUnknown_FreeInterface	(void *);

public :

	void		IRRPMScan_SetParamBits  (_U32, _U32, _U32);

	_U32		IRRPMScan_GetParam	(_U32);

	DWord		IRRPMScan_GetDataPtr	(void *&, DWord, DWord);

	void		IRRPMScan_GetTestImage	();


	struct IAnodeMaxTable
	{
	    int	    nEntries	    ;

	struct Entry
	{
	    int	    nUAnodeX	    ;
	    int	    nIAnodeRMax	    ;
	};   
	    Entry   pEntries [20]   ;
	};

public :

	DWord				m_MSCVersion	  ;

	union
	{
	struct
	{
	    Word			l_RegRecvCtrl	  ;
	    Word			h_RegRecvCtrl	  ;
	};
	    DWord			__RegRecvCtrl	  ;
	};
	Word				m_RegRecvLoop	  ;

	DWord				m_RegVersion	  ;
	DWord				m_RegVersion2	  ;
	DWord				m_RegVersion2a	  ;
	DWord				m_RegVersion3	  ;
	DWord				m_RegVersion4	  ;
	DWord				m_RegVersion5	  ;
	DWord				m_RegVersion5a	  ;
	DWord				m_RegVersion6	  ;

	CRRPSerialDevice::Register	m_RegScanStatus	  ;
	CRRPSerialDevice::Register	m_RegScanOffset	  ;
	CRRPSerialDevice::Register	m_RegScanOffsetEx ;
	CRRPSerialDevice::Register	m_RegScanVersion  ;

	DWord				m_RegScan20Req	  ;
	CRRPSerialDevice::Register	m_RegScan20Acq	  ;

	IAnodeMaxTable			m_IAnodeMaxTable  ;

	XPGrabber			m_XPGrabber	  ;
};
//
//[]------------------------------------------------------------------------[]
//
static Byte getCRC_96 (const Byte * pHead, const Byte * pTail)
{
    DWord  uCRC  = 0;

    for (; pHead < pTail;) { uCRC += * pHead ++;}

    return LOBYTE (((uCRC >> 7) + uCRC) & 0x7F);
};

static void PutBeg_96 (Byte * pTail, Byte bAddress, Byte bCommand)
{
    * (pTail + 0) = 0x96     ;
    * (pTail + 1) = bAddress ;
    * (pTail + 2) = bCommand ;
    * (pTail + 3) = 5	     ;
    * (pTail + 4) = 0	     ;
};

static void PutRegB_96 (Byte * pTail, Byte bValue)
{
    * (pTail + * (pTail + 3) + 0) = bValue;
	       * (pTail + 3)	 += 1;
};

static void PutRegW_96 (Byte * pTail, Word wValue)
{
    * (pTail + * (pTail + 3) + 0) = (LOBYTE(wValue >> 0) & 0x7F);
    * (pTail + * (pTail + 3) + 1) = (LOBYTE(wValue >> 7) & 0x7F);
	       * (pTail + 3)	 += 2;
};

static void PutEnd_96 (Byte *& pTail)
{
    if (5 <  * (pTail + 3))
    {
	PutRegB_96 (pTail,   getCRC_96 (pTail + 5, pTail + * (pTail + 3)));
    }
	     * (pTail + 4) = getCRC_96 (pTail    , pTail + 4		 );

    pTail += * (pTail + 3);
};
//
//[]------------------------------------------------------------------------[]
//
static GFORCEINLINE Byte GetB_8  (const Byte * p, int packoffset)
{
    return (((Byte)(* (p + 5 + packoffset))) << 0);
};

static GFORCEINLINE Word GetW_14 (const Byte * p, int packoffset)
{
    return (((Word)(* (p + 5 + packoffset + 0))) << 0)
	 | (((Word)(* (p + 5 + packoffset + 1))) << 7);
};

static void OutALTERA (const Byte * p)
{
    char pBuf [512]; * pBuf = 0;

    LogPrintf ("\r\n");
    LogPrintt (True);

    LogPrintf (" ---- ALTERA Regs ----\r\n\r\n");
    sprintf   (pBuf, "     0 : %3d %3d %7d %7d %7d %7d %7d",

			    GetB_8  (p,  0),
			    GetB_8  (p,  1),
			    GetW_14 (p,  2),
			    GetW_14 (p,  4),
			    GetW_14 (p,  6),
			    GetW_14 (p,  8),
			    GetW_14 (p, 10));
    LogPrintf ("%s\r\n", pBuf);

    sprintf   (pBuf, "     7 : %3d %3d %3d %3d %3d %3d %3d %3d",

			    GetB_8  (p, 12),
			    GetB_8  (p, 13),
			    GetB_8  (p, 14),
			    GetB_8  (p, 15),
			    GetB_8  (p, 16),
			    GetB_8  (p, 17),
			    GetB_8  (p, 18),
			    GetB_8  (p, 19));
    LogPrintf ("%s\r\n", pBuf);

    sprintf   (pBuf, "    15 : %3d %3d %3d %3d %3d %3d %3d     %7d %7d",

			    GetB_8  (p, 20),
			    GetB_8  (p, 21),
			    GetB_8  (p, 22),
			    GetB_8  (p, 23),
			    GetB_8  (p, 24),
			    GetB_8  (p, 25),
			    GetB_8  (p, 26),
			    GetW_14 (p, 27),
			    GetW_14 (p, 29));
    LogPrintf ("%s\r\n\r\n", pBuf);
    LogFlush  ();

//printf ("\t\t.................... ALTERA .........................\n");
};
//
//[]------------------------------------------------------------------------[]
//
static const Byte MScan_40 [] = {0x81, 0x96, 0x03, 0x40, 0x05, 0x5F};
static const Byte MScan_46 [] = {0x81, 0x96, 0x03, 0x46, 0x05, 0x65};
static const Byte MScan_44 [] = {0x81, 0x96, 0x03, 0x44, 0x05, 0x63};
static const Byte MScan_45 [] = {0x81, 0x96, 0x03, 0x45, 0x05, 0x64};
static const Byte MScan_7F [] = {0x81, 0x96, 0x03, 0x7F, 0x05, 0x1F};
static const Byte MScan_47 [] = {0x81, 0x96, 0x03, 0x47, 0x05, 0x66};

static const Byte MScan_49 [] = {0x81, 0x96, 0x03, 0x49, 0x05, 0x68};
static const Byte MScan_5C [] = {0x81, 0x96, 0x03, 0x5C, 0x05, 0x7B};

static const Byte MScan_41 [] = {0x81, 0x96, 0x03, 0x41, 0x05, 0x60};

static	DWord s_dMScanCM  = 0x0000;

#define	MScanCM_SetMode0    0x0001	// 0x61 (0) Set Work  mode 
#define MScanCM_SetMode3    0x0002	// 0x61 (3) Set Debug mode
#define MScanCM_GetImage3   0x0004	// 0x64 ( ) Get Debug image
#define MScanCM_ResetARegs  0x0008	// 0x60 ( ) Reset ALTERA regs
#define MScanCM_GetARegs    0x0010	// 0x4F ( ) Get ALTERA regs

static	Bool  s_bMScanI3  = False;

static _U16 getMScanCRC (const Byte * pHead, 
			 const Byte * pTail)
{
    _U16   uCRC  = 0;

    for (; pHead < pTail;) { uCRC += * pHead ++;}

    return (((uCRC >> 7) & 0x7F) + (uCRC & 0x7F)) & 0x7F;
};

static Bool putMScanCMD (Byte *& pTail, const Byte * pHigh, Byte bCmd, Byte * pData, DWord dDataLen)
{
    if (pHigh - pTail > 5 + ((0 < dDataLen) ? (LOBYTE(LOWORD(dDataLen))) + 1 : 0))
    {
	DWord  crc =     0;
	Byte *   p = pTail;

	    * p ++ = 0x81;
	    * p ++ = 0x96;
	    * p ++ = 0x03;
	    * p ++ = bCmd;
	    * p ++ = 5 + ((0 < dDataLen) ? (LOBYTE(LOWORD(dDataLen))) + 1 : 0);
	    * p    = LOBYTE (getMScanCRC (pTail + 1, p));
	      p ++;

	if (0 < dDataLen)
	{
	while (0 < dDataLen)
	{
	    * p ++ = * pData ++; dDataLen --;
	}
	    * p    = LOBYTE (getMScanCRC (pTail + 6, p));
	      p ++;
	}

	return (pTail = p, True);
    }
	return False;
};

CRRPSerialBus::SendResult 
CRRPMScan::PutRecvRequest (Byte *& pTail, const Byte * pHigh)
{
    if (pHigh < pTail +  6)
    {
        return CRRPSerialBus::NoMoreData  ;
    }

    if (__RegRecvCtrl == 0)
    {
    if (m_RegRecvLoop & 0x0001)
    {
	m_RegRecvLoop = ((m_RegRecvLoop << 1) | (m_RegRecvLoop >> 7));

	return CRRPStepMotor::PutRecvRequest (pTail, pHigh);
    }
    else
    {
	m_RegRecvLoop = ((m_RegRecvLoop << 1) | (m_RegRecvLoop >> 7));

	if (0 < s_dMScanCM && (0x0104 <= getBigEndianW (& m_RegVersion2, 2)))
	{
	    if (s_dMScanCM & MScanCM_SetMode0)
	    {
		PutBeg_96  (pTail, 0x03, 0x61);
		PutRegB_96 (pTail, 0x00);
		PutEnd_96  (pTail);
	    }
	    else
	    if (s_dMScanCM & MScanCM_SetMode3)
	    {
		s_dMScanCM &= (~MScanCM_SetMode3);

		PutBeg_96  (pTail, 0x03, 0x61);
		PutRegB_96 (pTail, 0x03);
		PutEnd_96  (pTail);
	    }
	    else
	    if (s_dMScanCM & MScanCM_GetImage3)
	    {
		s_dMScanCM &= (~MScanCM_GetImage3);
		s_bMScanI3  = True;

		PutBeg_96  (pTail, 0x03, 0x64);
		PutEnd_96  (pTail);
	    }
	    else
	    if (s_dMScanCM & MScanCM_GetARegs)
	    {
		s_dMScanCM &= (~MScanCM_GetARegs);

		PutBeg_96  (pTail, 0x03, 0x4F);
		PutEnd_96  (pTail);
	    }
	    else
	    if (s_dMScanCM & MScanCM_ResetARegs)
	    {
		s_dMScanCM &= (~MScanCM_ResetARegs);

		PutBeg_96  (pTail, 0x03, 0x60);
		PutEnd_96  (pTail);
	    }
	}
	else
	{
	    memcpy (pTail, MScan_41, 6); pTail += 6;
	}

#if TRACE_REQUEST
printf ("...MScan {%02X %02X}\n", * (pTail - 5), * (pTail - 3));
#endif
	return CRRPSerialBus::QueueForRecv;
    }
    }

    if (l_RegRecvCtrl == 0)
    {
	l_RegRecvCtrl = h_RegRecvCtrl;
    }

printf ("...Version %08X <<\n", __RegRecvCtrl);

    if (l_RegRecvCtrl &    0x0001)
    {	l_RegRecvCtrl &= (~0x0001); memcpy (pTail, MScan_46, 6); pTail += 6;}
    else
    if (l_RegRecvCtrl &	   0x0002)
    {	l_RegRecvCtrl &= (~0x0002); memcpy (pTail, MScan_44, 6); pTail += 6;}
    else
    if (l_RegRecvCtrl &	   0x0004)
    {	l_RegRecvCtrl &= (~0x0004); memcpy (pTail, MScan_45, 6); pTail += 6;}
    else
    if (l_RegRecvCtrl &	   0x0008)
    {	l_RegRecvCtrl &= (~0x0008); memcpy (pTail, MScan_7F, 6); pTail += 6;}
    else
    if (l_RegRecvCtrl &	   0x0010)
    {	l_RegRecvCtrl &= (~0x0010); memcpy (pTail, MScan_47, 6); pTail += 6;}
    else
    if (l_RegRecvCtrl &	   0x0020)
    {	l_RegRecvCtrl &= (~0x0020); memcpy (pTail, MScan_40, 6); pTail += 6;}
    else
    if (l_RegRecvCtrl &	   0x0040)
    {	l_RegRecvCtrl &= (~0x0040); memcpy (pTail, MScan_49, 6); pTail += 6;}
    else
    if (l_RegRecvCtrl &	   0x0080)
    {	l_RegRecvCtrl &= (~0x0080); memcpy (pTail, MScan_5C, 6); pTail += 6;}

//#if TRACE_REQUEST
printf ("...Version %08X {%02X %02X} >>\n", __RegRecvCtrl, * (pTail - 5), * (pTail - 3));
//#endif

	return CRRPSerialBus::QueueForRecv;
};


CRRPSerialBus::RecvResult CRRPMScan::OnRecv (
CRRPSerialBus::RecvReason Reason, const Byte *& pHead, const Byte * pTail)
{
    if (CRRPSerialBus::RecvTimeout == Reason)
    {
printf ("................Problem Timeout............\n");

//	Reset ();	Do normal 3-time resend condition !!!

    if (m_RegRecvLoop & 0x0001)
    {}
    else
    {
	m_RegRecvLoop = ((m_RegRecvLoop << 1) | (m_RegRecvLoop >> 7));
    }
        return CRRPStepMotor::OnRecv (Reason, pHead, pTail);
    }

    if ((__RegRecvCtrl == 0) && (0x96 != * pHead))
    {
	return CRRPStepMotor::OnRecv (Reason, pHead, pTail);
    }

    for (;  pHead < pTail;)
    {
	if (* pHead != 0x96)
	{
	    pHead ++;

	    continue;
	}

	if (   pTail < pHead + 5)
	{
	    return CRRPSerialBus::RecvContinue;
	}

	if (* (pHead + 4) != getMScanCRC (pHead, pHead + 4))
	{
	    return CRRPSerialBus::RecvBreak;
	}

	if (   pTail < pHead + (* (pHead + 3)))
	{
	    return CRRPSerialBus::RecvContinue;
	}

	if ((5 < * (pHead + 3)) && (* (pHead + * (pHead + 3) - 1) != getMScanCRC (pHead + 5, pHead + * (pHead + 3) - 1)))
	{
	    return CRRPSerialBus::RecvBreak;
	}

	    m_BusTrTimeout.Disactivate ();
	    m_BusTrMask  |= 1;

	if (0x46 == * (pHead + 2))
	{
	    m_RegVersion4  = ((DWord)(* (pHead +  6)) <<  7) + ((* (pHead + 5)) <<  0);
//	    m_RegVersion4 |= ((DWord)(* (pHead +  8)) << 23) + ((* (pHead + 7)) << 16);
	    m_RegVersion4 |= ((DWord)(* (pHead + 10)) << 24) + ((* (pHead + 9)) << 16);

	    h_RegRecvCtrl &= (~0x0001);
	}
	else
	if (0x44 == * (pHead + 2))
	{
	    m_RegVersion5  = (((DWord)(* (pHead + 5)) & 0x0F) * 16);

	    h_RegRecvCtrl &= (~0x0002);
	}
	else
	if (0x45 == * (pHead + 2))
	{
	    m_RegVersion5a = (((DWord)(* (pHead + 5)) & 0x0F) * HIBYTE (HIWORD (m_RegVersion4)));//16);

	    h_RegRecvCtrl &= (~0x0004);
	}
	else
	if (0x7F == * (pHead + 2))
	{
	    m_RegVersion3  = ((DWord)(* (pHead +  7)) <<  7) + ((* (pHead + 6)) <<  0);

	    h_RegRecvCtrl &= (~0x0008);
	}
	else
	if (0x47 == * (pHead + 2))
	{
	    m_RegVersion6  = (DWord)(* (pHead + 5));

	    h_RegRecvCtrl &= (~0x0010);
	}
	else
	if (0x40 == * (pHead + 2))
	{
	    memcpy (& m_RegVersion, pHead + 5, 8);

// Debug to -> SW 1.3 --------
//
//	    m_RegVersion2  = (m_RegVersion2 & 0x0000FFFF) | 0x03010000;
//
// Debug to -> SW 1.3 --------

	    h_RegRecvCtrl &= (~0x0020);

	    if (0x0104 <= getBigEndianW (& m_RegVersion2, 2))
	    {
		__RegRecvCtrl |= ( 0x00C000C0);	   // Continue read Novik's - �� ���� !!!

		s_dMScanCM = MScanCM_SetMode0 | MScanCM_ResetARegs; // Turn Work mode !!!
	    }
	    else
	    {
		SetRegValue (m_RegScanVersion, (m_RegScanVersion.uValue & 0xFFFF0000) | LOWORD(m_RegVersion4));
	    }
	}
	else
	if (0x49 == * (pHead + 2))			// Novik's CRC mode ???
	{
	    XPGrabber::s_bCRCMode = (0 != * (pHead + 5)) ? True : False;

	    h_RegRecvCtrl &= (~0x0040);
	}
	else
	if (0x5C == * (pHead + 2))			// Novik's Altera version
	{
	    m_RegVersion2a = ((DWord)(* (pHead +  6)) <<  7) + ((* (pHead + 5)) <<  0);

    	    SetRegValue (m_RegScanVersion, (m_RegScanVersion.uValue & 0xFFFF0000) | LOWORD(m_RegVersion4));

	    h_RegRecvCtrl &= (~0x0080);
	}
	else
	if (0x61 == * (pHead + 2))
	{
printf ("...Scan Mode %d\n", * (pHead + 5));

	    if (0 == * (pHead + 5))
	    {
		s_dMScanCM &= (~MScanCM_SetMode0);
	    }
	}
	else
	if (0x64 == * (pHead + 2))
	{
printf ("...Grab debug image\n");
	}
	else
	if (0x60 == * (pHead + 2))
	{
printf ("...Reset ARegs\n");
	}
	else
	if (0x4F == * (pHead + 2))
	{
printf ("...Grab  ARegs\n");

	    OutALTERA (pHead);
	}
	else
	if ((0x41 != * (pHead + 2)) && True)
	{
{{  const Byte * p = pHead;
printf ("...0x96 [", (int)(pTail - pHead));
    for (; (p < pTail) && (p < pHead + 10); p ++)
    {
printf (" %02X", * p);
    }
printf ("]\n");
}}
	}
	
	if (h_RegRecvCtrl == 0)
	{
	    __RegRecvCtrl  = 0;
	}

	if (0x41 == * (pHead + 2))
	{
	    SetRegValue (m_RegScan20Acq, (DWord)(* (pHead + 5)));

	    if (LOBYTE(LOWORD(m_RegScan20Acq.uValue))
	    !=  LOBYTE(LOWORD(m_RegScan20Req	   )))
	    {
		m_RegScan20Req |= 0x80000000;
	    }
	}

#if TRACE_RECV_V
{{  const Byte * p = pHead;
printf ("...Version (%d)[", (int)(pTail - pHead));
    for (; p < pTail; p ++)
    {
printf (" %02X", * p);
    }
printf ("]\n");
}}
#endif
	    return CRRPSerialBus::RecvBreak;
    };
	    return CRRPSerialBus::RecvContinue;
};

void CRRPMScan::Reset ()
{
    m_MSCVersion	  = 0x0100;
//  XPGrabber::s_bCRCMode =  False;		// Do not toch this !!!
	       s_dMScanCM = 0x0000;
	       s_bMScanI3 =  False;

    GetIRRPSettings () ->GetValue (IRRPSettings::ID_MSC_Version	      , & m_MSCVersion	  , sizeof (DWord), NULL);

	m_RegVersion   =
	m_RegVersion2  =
	m_RegVersion2a =
	m_RegVersion3  =
	m_RegVersion4  =
	m_RegVersion5  =
	m_RegVersion5a =
	m_RegVersion6  = 0xFFFFFFFF;

    if (0x0100 < m_MSCVersion)
    {
//	__RegRecvCtrl  = 0x003F003F;
	__RegRecvCtrl  = 0x003F003F;
	m_RegRecvLoop  = 0x5555;

	m_RegScan20Req = 0x80000001;

	SetRegValue (m_RegScanVersion, (m_MSCVersion << 16) + 0xFFFF);
    }
    else
    {
	__RegRecvCtrl  = 0x00000000;
	m_RegRecvLoop  = 0xFFFF;

	m_RegScan20Req = 0x00000000;

	m_RegVersion   = '\0\0��';
	m_RegVersion2  = 0x00010001;
//	m_RegVersion2a =
//	m_RegVersion3  =
	m_RegVersion4  = 6450 + (0xFFFF << 16);
//	m_RegVersion5  = 
//	m_RegVersion5a =
//	m_RegVersion6  =;

	SetRegValue (m_RegScanVersion, (m_MSCVersion << 16) + 6450);
    }

    CRRPStepMotor::Reset ();
};
//
//[]------------------------------------------------------------------------[]
//
#pragma warning (push)
#pragma warning (disable : 4355)

CRRPMScan::CRRPMScan () : CRRPStepMotor (MSCAN_DEVICE_ID),

			  m_XPGrabber   (XPGrabberCallBack, (void *) this)
{
    m_MSCVersion   = 0xFFFFFFFF;
    __RegRecvCtrl  = 0xFFFFFFFF;
    m_RegRecvLoop  = 0xFFFF;
    
    m_RegVersion   =
    m_RegVersion2  =
    m_RegVersion3  =
    m_RegVersion4  =
    m_RegVersion5  =
    m_RegVersion6  = 0xFFFFFFFF;

    m_RegScan20Req = 0x00000000;

    IRRPMScan_Entry	IEntry;

    * ((void **) & m_IRRPDevice) = * ((void **) & IEntry);

    memset (& m_IAnodeMaxTable, 0, sizeof (m_IAnodeMaxTable));

    m_RegList.append (m_RegScanStatus.
		      DefineSWReg_16	(_WREG_MASK (0x00,0x00, 0x0F,0x57)));
    m_RegList.append (m_RegScanOffset.
		      DefineSWReg_32	(_WREG_MASK (0xFF,0x00, 0xFF,0x58),
					 _WREG_MASK (0xFF,0x00, 0xFF,0x00)));
    m_RegList.append (m_RegScanOffsetEx.
		      DefineSWReg_32	(_WREG_MASK (0xFF,0x00, 0xFF,0x59),
					 _WREG_MASK (0xFF,0x00, 0xFF,0x00)));
    m_RegList.append (m_RegScanVersion.
		      DefineSWReg_32	(_WREG_MASK (0xFF,0x00, 0xFF,0x66),
					 _WREG_MASK (0xFF,0x00, 0xFF,0x00)));
    m_RegList.append (m_RegScan20Acq.
		      DefineSWReg_32	(_WREG_MASK (0xFF,0x00, 0xFF,0x67),
					 _WREG_MASK (0xFF,0x00, 0xFF,0x00)));
};

#pragma warning (pop)

void CRRPMScan::LoadIAnodeMax ()
{
    DWord		    t;
    TStreamBuf <1 * 1024>   s;

    m_IAnodeMaxTable.nEntries = 0;

    if (ERROR_SUCCESS == 
    GetIRRPSettings () ->GetValue (IRRPSettings::ID_MSC_Scan_IAnodeMax, s.head (), (DWord) s.sizeforput (), & t))
    {{
	s.put (::lstrlen ((char *) s.head ()));

    for (;m_IAnodeMaxTable.nEntries < (sizeof (m_IAnodeMaxTable.pEntries)
				     / sizeof (  IAnodeMaxTable::Entry));)
    {
		 s.ScanfA (" \t\r\n", NULL);
	if (2 != s.ScanfA (" \t", "<%d,%d>", & m_IAnodeMaxTable.pEntries 
					      [m_IAnodeMaxTable.nEntries].nUAnodeX, 
					     & m_IAnodeMaxTable.pEntries
					      [m_IAnodeMaxTable.nEntries].nIAnodeRMax))
	    {break;}

	m_IAnodeMaxTable.nEntries ++;

	if (',' != s.GetChrA ())
	    {break;}
    }
    }}
};

void CRRPMScan::CheckExposition (int * pUAnode, int * pIAnode)
{
    if (0 < m_IAnodeMaxTable.nEntries)
    {{
	IAnodeMaxTable::Entry * lr, * hr;

	int  i, nIAnodeRMax;

	for (i = 0; (i < m_IAnodeMaxTable.nEntries) && (m_IAnodeMaxTable.pEntries [i].nUAnodeX < * pUAnode); i ++)
	{}

	if (i == m_IAnodeMaxTable.nEntries)
	{   lr =  hr = & m_IAnodeMaxTable.pEntries [i - 1];}
	else
	if (i == 0)
	{   lr =  hr = & m_IAnodeMaxTable.pEntries [0];}
	else
	{   lr = (hr = & m_IAnodeMaxTable.pEntries [i]) - ((m_IAnodeMaxTable.pEntries [i].nUAnodeX == * pUAnode) ? 0 : 1);}

	if (lr == hr)
	{
	    nIAnodeRMax = lr ->nIAnodeRMax;
	}
	else
	{
	    nIAnodeRMax = lr ->nIAnodeRMax + ( (((hr ->nIAnodeRMax - lr ->nIAnodeRMax) * (* pUAnode - lr ->nUAnodeX) * 64)
					       / (hr ->nUAnodeX    - lr ->nUAnodeX				   ))/ 64);
	}

	if (nIAnodeRMax < * pIAnode)
	{
	    * pIAnode = nIAnodeRMax;
	}
    }}
};

void CRRPMScan::LoadIniParam ()
{
    GetIRRPSettings () ->GetValue (IRRPSettings::ID_MSC_Scan_SyncDelay, & m_uSetSyncDelay , sizeof (int), NULL);
    GetIRRPSettings () ->GetValue (IRRPSettings::ID_MSC_Scan_DefCoeff , & m_uSetDefCoeff  , sizeof (int), NULL);
    GetIRRPSettings () ->GetValue (IRRPSettings::ID_MSC_Scan_AccelTime, & m_uSetAccelTime , sizeof (int), NULL);
    GetIRRPSettings () ->GetValue (IRRPSettings::ID_MSC_Scan_Velocity,  & m_uSetVelocity  , sizeof (int), NULL);
    GetIRRPSettings () ->GetValue (IRRPSettings::ID_MSC_Scan_StepPerDec,& m_uSetStepPerDec, sizeof (int), NULL);

    LoadIAnodeMax ();

    IRRPDevice::Param	Param;

    Param.Init (IRRPMMotor::ID_RegSetSyncDelay, m_uSetSyncDelay); SetParam (& Param);
    Param.Init (IRRPMMotor::ID_RegSetDefCoeff , m_uSetDefCoeff ); SetParam (& Param);
    Param.Init (IRRPMMotor::ID_RegSetAccelTime, m_uSetAccelTime); SetParam (& Param);
    Param.Init (IRRPMMotor::ID_RegSetVelocity , m_uSetVelocity ); SetParam (& Param);
};

void CRRPMScan::Initialize ()
{
    CRRPStepMotor::Initialize ();

    LoadIniParam ();
};

void CRRPMScan::SetParam (const IRRPDevice::Param * pParam)
{
    switch (pParam ->uParamId)
    {
    case IRRPMScan::ID_RegGetFGStatus :
	SetRegBits  (m_RegScanStatus  , 0x04, 0x00);
	SetRegValue (m_RegScanOffset  , 0);
	SetRegValue (m_RegScanOffsetEx, 0);
	return;
    }
    CRRPStepMotor::SetParam (pParam);
};

void GAPI CRRPMScan::XPGrabberCallBack (void * pContext, DWord dFlags, DWord dSize)
{
    CRRPMScan *  pMScan   =
   (CRRPMScan *) pContext;

    switch (dFlags & 0x1F)
    {
    case 0x00 :
	    pMScan ->SetRegBits
	   (pMScan ->m_RegScanStatus, 0x1F, 0x00);
	    break;

    case 0x02 : 
	    pMScan ->SetRegBits 
	   (pMScan ->m_RegScanStatus, 0x1E, 0x02);

	    pMScan ->m_XPGrabber.ContinueImage 
				 (pMScan ->m_RegScanOffset   .uValue);
	    pMScan ->SetRegValue (pMScan ->m_RegScanOffset  , 0     );
	    pMScan ->SetRegValue (pMScan ->m_RegScanOffsetEx, dSize );
	    break;

    case 0x03 : 
	    if ((256 * 1024) >= (dSize - pMScan ->m_RegScanOffsetEx.uValue))
	    {	return;}

	    pMScan ->SetRegValue (pMScan ->m_RegScanOffsetEx, dSize );
	    break;

    case 0x04 :
	    pMScan ->SetRegBits
	   (pMScan ->m_RegScanStatus, 0x10, 0x08);
	    break;

    case 0x01 : 
	    pMScan ->SetRegBits
	   (pMScan ->m_RegScanStatus, 0x12, 0x04);

	    pMScan ->SetRegValue (pMScan ->m_RegScanOffset  , dSize );
	    pMScan ->SetRegValue (pMScan ->m_RegScanOffsetEx, 0     );

	    if (s_bMScanI3)	     // If Debug image, set back Work mode
	    {
		s_bMScanI3  = False;
		s_dMScanCM |= MScanCM_SetMode0;
	    }

    if (XPGrabber::s_bCRCMode)	    // Log <- Out getimage results !!!
    {{
		s_dMScanCM |= (MScanCM_GetARegs | MScanCM_ResetARegs);

	LogPrintf ("\r\n");
	LogPrintt (True);
	LogPrintf (" : %c%c %1d.%1d, %1d.%1d, %d, SN %05d %s",
		   LOBYTE(LOWORD(pMScan ->m_RegVersion )), HIBYTE(LOWORD(pMScan ->m_RegVersion )),
		   LOBYTE(LOWORD(pMScan ->m_RegVersion2)), HIBYTE(LOWORD(pMScan ->m_RegVersion2)),
		   LOBYTE(HIWORD(pMScan ->m_RegVersion2)), HIBYTE(HIWORD(pMScan ->m_RegVersion2)),
		  (0xFFFF != LOWORD(pMScan ->m_RegVersion2a)) 
			  ?  LOWORD(pMScan ->m_RegVersion2a) : 0, LOWORD(pMScan ->m_RegVersion3),

		  (pMScan ->m_XPGrabber.m_sRecvResult.nErrCRC2	   |
		   pMScan ->m_XPGrabber.m_sRecvResult.nErrCRC1	   |
		   pMScan ->m_XPGrabber.m_sRecvResult.nErrCRC3	   |
		   pMScan ->m_XPGrabber.m_sRecvResult.nErrDataCRC2 |
		   pMScan ->m_XPGrabber.m_sRecvResult.nErrDataCRC1 |
		   pMScan ->m_XPGrabber.m_sRecvResult.nErrDataCRC3 |
		   pMScan ->m_XPGrabber.m_sRecvResult.nErrDataSync ) ? " ----- Image Error -----\r\n\r\n" : "\r\n\r\n");

	LogPrintf ("    Full %d - Accepted %d - Rejected %d = %d bytes\r\n", 
		   pMScan ->m_XPGrabber.m_sRecvResult.dRecvSize	    ,
		   pMScan ->m_XPGrabber.m_sRecvResult.dRecvAccepted ,
		   pMScan ->m_XPGrabber.m_sRecvResult.dRecvRejected ,

		   pMScan ->m_XPGrabber.m_sRecvResult.dRecvSize	    -
		   pMScan ->m_XPGrabber.m_sRecvResult.dRecvAccepted -
		   pMScan ->m_XPGrabber.m_sRecvResult.dRecvRejected );

	LogPrintf ("    Rows %d/%d {%d/%d}, Syn %d, Hdr {%d.%d.%d}, Dat {%d.%d.%d}\r\n\r\n",

		   pMScan ->m_XPGrabber.m_sRecvResult.nRowAccepted +
		   pMScan ->m_XPGrabber.m_sRecvResult.nSkiped	    ,
		   pMScan ->m_XPGrabber.m_sRecvResult.nRowOk	    ,
		   pMScan ->m_XPGrabber.m_sRecvResult.nRowAccepted -
		   pMScan ->m_XPGrabber.m_sRecvResult.nRowOk	    ,
		   pMScan ->m_XPGrabber.m_sRecvResult.nSkiped	    ,

		   pMScan ->m_XPGrabber.m_sRecvResult.nErrDataSync  ,
		   pMScan ->m_XPGrabber.m_sRecvResult.nErrCRC2	    ,
		   pMScan ->m_XPGrabber.m_sRecvResult.nErrCRC1	    ,
		   pMScan ->m_XPGrabber.m_sRecvResult.nErrCRC3	    ,

		   pMScan ->m_XPGrabber.m_sRecvResult.nErrDataCRC2  ,
		   pMScan ->m_XPGrabber.m_sRecvResult.nErrDataCRC1  ,
		   pMScan ->m_XPGrabber.m_sRecvResult.nErrDataCRC3  );

	LogFlush  ();
    }}
	    break;

    case 0x08 :
	    pMScan ->SetRegBits
	   (pMScan ->m_RegScanStatus, 0x1F, 0x01);
	    break;

    case 0x10 :
	    dSize = pMScan ->m_RegScanOffset.uValue;
	    pMScan ->SetRegBits
	   (pMScan ->m_RegScanStatus, 0x02, 0x10);
    if (! XPGrabber::s_bCRCMode)
    {{
	LogPrintf ("\r\n\r\n");
	LogPrintt (True);
	LogPrintf (" : �������� ����������� %9d bytes (%4d.%d) rows\r\n\r\n", dSize, dSize / ((1537 * 3) * 2),
										     dSize % ((1537 * 3) * 2));
	LogFlush  ();
    }}
	    break;
    };
};

DWord CRRPMScan::IRRPMScan_GetDataPtr (void *& pHead, DWord dOffset, DWord dLength)
{
    void *	pSrcBuf = m_XPGrabber.GetImageBuf ();

    if (NULL == pSrcBuf)
    {	return (pHead = NULL, 0);}

    DWord dImageOff = (m_RegScanOffset.uValue > m_RegScanOffsetEx.uValue) 
			   ? m_RegScanOffset.uValue : m_RegScanOffsetEx.uValue;

    if (  dImageOff < dOffset)
    {	return (pHead = NULL, 0);}

	   dLength  = (	        (dOffset + dLength) <= dImageOff)
		    ? dLength
		    :(dLength - (dOffset + dLength   - dImageOff));

	return (((Byte *&) pHead) = ((Byte *) pSrcBuf) + dOffset), dLength;
};

_U32 CRRPMScan::IRRPMScan_GetParam (_U32 uParamId)
{
    switch (uParamId)
    {
    case IRRPMScan::ID_RegGetVersion  :
	return m_RegVersion ;
    case IRRPMScan::ID_RegGetVersion2 :
	return m_RegVersion2;
    case IRRPMScan::ID_RegGetVersion2a :
	return m_RegVersion2a;
    case IRRPMScan::ID_RegGetVersion3 :
	return m_RegVersion3;
    case IRRPMScan::ID_RegGetVersion4 :
	return m_RegVersion4;
    case IRRPMScan::ID_RegGetVersion5 :
	return m_RegVersion5;
    case IRRPMScan::ID_RegGetVersion5a:
	return m_RegVersion5a;
    case IRRPMScan::ID_RegGetVersion6 :
	return m_RegVersion6;
    default :
	break;
    };
	return IRRPMMotor_GetParam (uParamId);
};

CRRPSerialBus::SendResult
CRRPMScan::PutSendRequest (Byte *& pTail, const Byte * pHigh)
{
    if (m_RegScan20Req  &   0x80000000)
    {
    if (putMScanCMD (pTail, pHigh, 0x01, (Byte *) & m_RegScan20Req, 1))
    {
	m_RegScan20Req &= (~0x80000000);

	CRRPStepMotor::PutSendRequest (pTail, pHigh);

	return CRRPSerialBus::QueueForSend;
    }
    }
	return CRRPStepMotor::PutSendRequest (pTail, pHigh);
}

void CRRPMScan::IRRPMScan_SetParamBits (_U32 uParamId, _U32 ClrMask,
						       _U32 SetMask)
{
    if (uParamId == 0x00000096)
    {
	m_RegScan20Req = ((m_RegScan20Req & (~ClrMask)) | SetMask) ^ 0x80000001;

	return;
    }

    return IRRPMMotor_SetParamBits (uParamId, ClrMask, SetMask);
};

void CRRPMScan::IRRPMScan_GetTestImage ()
{
    s_dMScanCM = MScanCM_SetMode3 | MScanCM_GetImage3;
};
//
//[]------------------------------------------------------------------------[]


//[]------------------------------------------------------------------------[]
//
#include "RRPCalibrate.h"

#define MDIG_DEVICE_ID	    0x50000000

#define	IMAGE_RULE_WIDTH (1537		   * 1)
#define IMAGE_LINE_WIDTH (IMAGE_RULE_WIDTH * 3)

class CRRPMDigit : public CRRPDevice
{
public :
		    CRRPMDigit (IRRPDeviceService *);

		   ~CRRPMDigit ();

static	HRESULT	    CreateInstance		(void ** pInterface, IRRPDeviceService *);

	HRESULT	    Create			(void ** pInterface);

virtual	HRESULT	    XUnknown_QueryInterface	(REFIID, void **);

virtual	void	    XUnknown_FreeInterface	(void *);

//	GResult	    IRRPDevice_Connect		(IRRPCallBack::Handle *& hICallBack,
//						 IRRPCallBack *		 pICallBack);

//	void	    IRRPDevice_SetParam		(int			   nParams,
//						 const IRRPDevice::Param * pParams);

	void	    SetParam			(const IRRPDevice::Param * pParam );

	void	    OnAckValue			(Register &, _U32 v);

//	void	    OnIdleRegPack		(IRRPProtocol::ParamPack::Entry *&, Register *);
//
public :

	friend
	class TXUnknown_Entry <CRRPMDigit>;
	      TXUnknown_Entry <CRRPMDigit>
				m_XUnknown  ;

	friend
	class IRRPMDIG_Entry;
	friend
	class TInterface_Entry <IRRPMDIG, CRRPMDigit>;
	class IRRPMDIG_Entry : public
	      TInterface_Entry <IRRPMDIG, CRRPMDigit>
	{
	STDMETHOD_ (GResult,	    Connect)(IRRPCallBack::Handle *&
							    hICallBack ,
					     IRRPCallBack * pICallBack )
	{
	    return GetT () ->IRRPDevice_Connect	  (hICallBack ,
						   pICallBack );
	};
	STDMETHOD_ (void,	   SetParam)(	   int	    nParams,
					     const Param *  pParams)
	{
		   GetT () ->IRRPDevice_SetParam  (nParams, pParams);
	};
	STDMETHOD_ (void,	   GetParam)(	   int	    nParams,
						   Param *  pParams)
	{
		   GetT () ->IRRPDevice_GetParam  (nParams, pParams);
	};
	STDMETHOD_ (_U32,	   GetParam)(_U32 uParamId)
	{
	    return GetT () ->IRRPMDIG_GetParam  (uParamId);
	};
	STDMETHOD_ (GResult,	      SetHV)(HVCommand Command)
	{
	    LogPrintf ("\n<<<<<<<< IRRPMDIG::SetHV - (%s)\n", (HVOn == Command) ? "HVOn" : "HVRelease");
	    LogFlush  ();

	    return (HVOn == Command) ? GetT () ->IRRPMDIG_SetHV () : (GetT () ->IRRPMDIG_ReleaseHV (), ERROR_SUCCESS);
	};
	STDMETHOD_ (GResult, PeekImageStatus)(int * pPhase, int * pStep, int * pStepNum)
	{
	    return GetT () ->IRRPMDIG_PeekImageStatus (pPhase, pStep, pStepNum);
	};
	STDMETHOD_ (int,	   GrabImage)(void * pDstBuf, DWord dDstPitch, int nFromRow, int nNumRows)
	{
	    return GetT () ->IRRPMDIG_GrabImage (pDstBuf, dDstPitch, nFromRow, nNumRows);
	};
	}		    m_IRRPDevice    ;



#pragma pack (_M_PACK_VPTR)

	struct GetShotRequest : public GIrp_Request
	{
	    GTimeout		tTimeout;

	    GetShotRequest *	Init ()
			    {
				GIrp_Request::Init (0x0000);

				tTimeout.Disactivate ();

				return this;
			    };
	};

#pragma pack ()

	_U32		IRRPMDIG_GetParam   (_U32);

	void		IRRPMDIG_ReleaseHV  ();

	GResult		IRRPMDIG_SetHV	    ();

	GResult		IRRPMDIG_PeekImageStatus    (int *, int *, int *);

	int		IRRPMDIG_GrabImage	    (void *, DWord, int, int);

protected :

	void		SetIAnode	    (int    nIAnodeR  );
	void		SetUAnode10	    (int    nUAnodeR10);
	void		SetThick	    (int    nThick    );

	DWord		GetSetHVReady	    ();

	GIrp *		Co_WaitEof	    (GIrp *, void *);

	GIrp *		Co_WaitSof	    (GIrp *, void *);

	GIrp *		Co_SetHVOn	    (GIrp *, void *);

	GIrp *		Co_SetHVWaitOn	    (GIrp *, void *);

	GIrp *		Co_SetHVWaitPrepare (GIrp *, void *);

	GIrp *		Co_SetHVRelease	    (GIrp *, void *);

	GIrp *		Co_SetHVReady	    (GIrp *, void *);

	GIrp *		Co_SetHVWaitRelease (GIrp *, void *);

	GIrp *		Co_SetHVWaitReady   (GIrp *, void *);

	GIrp *		Co_RepairImageEx    (GIrp *, void *);

	GIrp *		Co_RepairImage	    (GIrp *, void *);

	GIrp *		Co_SetHVDone	    (GIrp *, void *);

	Bool		   LockAcquire	    () { return m_PLock.LockAcquire ();};

	void		   LockRelease	    () {	m_PLock.LockRelease ();};

	void		On_IRRPCallBack	    (const void *, _U32, _U32);

protected :

	int	    GetUAnode		    (int uUAnode);

	int	    GetIFilament	    (int uIAnode);

//	int	    GetUAnodeXFromThickR    (int uThickR);

protected :

	GCriticalSection	m_PLock			;

	IRRPDeviceService *	m_pIRRPDevService	;

	IRRPUrp	*		m_pIRRPUrp		;
	IRRPCallBack::Handle *	m_hIRRPUrpCallBack	;

	IRRPMScan *		m_pIRRPMScan		;
	IRRPCallBack::Handle *	m_hIRRPMScanCallBack	;
	GTimeout		m_tScanBackTimeout	;
	Bool			m_bGrab			;
	DWord			m_dGrabOffset		;
	IRRPRepairer *		m_pIRRPRepairer		;

	IRRPMMotor *		m_pIRRPMDiafr		;
	IRRPCallBack::Handle *	m_hIRRPMDiafrCallBack	;
	GTimeout		m_tDiafrBackTimeout	;

	IRRPMM2 *		m_pIRRPMM2		;
	IRRPCallBack::Handle *  m_hIRRPMM2CallBack	;

	GIrp *			m_pShotIrp		;

	int			m_uDiafrWidth		;
	int			m_uScanTime		;

	int			m_nActiveBeg		;
	int			m_nActiveEnd		;
	int			m_nDCBeg		;
	int			m_nDCEnd		;

//--------------------------------------------------------
//
	struct UThickEntry
	{
	    int	    uType	    ;
	    int	    nUAnodeR	    ;
	    int	    nIAnodeR	    ;
	};

	struct UThickRow
	{
	    int	    nUThickR	    ;
	    int	    nEntries	    ;
	    UThickEntry
		    pEntries [64]   ;

	Bool	    GetUThickEntry  (UThickEntry *& pLo,
				     UThickEntry *& pHi, int nUAnodeR);
	};

	struct UThickTable
	{
	    int	    nRows	    ;
	    UThickRow
		    pRows    [64]   ;

	Bool	    GetUThickRow    (UThickRow *& pLo  ,
				     UThickRow *& pHi  , int nThickR );

	int	    GetUThickCol    (UThickEntry **    , 
				     int	  *    , int nUAnodeR);
	};

//
//--------------------------------------------------------
/*
	struct EquMask
	{
	_U32	bKKM		: 1 ;
	_U32	bUrp		: 1 ;
	_U32	bMScan		: 1 ;
	_U32	bMDiafr		: 1 ;
	_U32	bMM2		: 1 ;

		EquMask		     ()
		{
		    * this = 0x00  ;
		}

		EquMask		     (_U32 dValue)
		{
		    * this = dValue;
		};

		EquMask & operator = (_U32 dValue)
		{
		    * ((DWord *) this) = dValue;
		    return    *  this;
		};
	};
*/
	void		LoadThickTable	    (char *);

	void		LoadIniParam	    ();

	void		SetEquMask	    (DWord);

static	void GAPI	SetEquMask	    (int, void **, HANDLE);

	void	   Xxxx_SetEquMask	    (IRRPMDIG::EquMask);

	struct KKMStatus
	{
	_U32	bConnected	: 1 ;

		KKMStatus & operator = (_U32 dValue)
		{
		    * ((DWord *) this) = dValue;
		    return    *  this;
		};
	};

	struct UrpStatus
	{
	_U32	bConnected	: 1 ;
	_U32	bKKMConnected	: 1 ;
	_U32	bKKMCharge	: 1 ;
	_U32	bHVPrepared	: 1 ;
	_U32	bHVOn		: 1 ;
	_U32	bHVBlocked	: 1 ;

		UrpStatus & operator = (_U32 dValue)
		{
		    * ((DWord *) this) = dValue;
		    return    *  this;
		};
	};

	struct MMotorStatus
	{
	_U32	bConnected	: 1 ;
	_U32	bMove		: 1 ;
	_U32	bBegPin		: 1 ;
	_U32	bEndPin		: 1 ;

		MMotorStatus & operator = (_U32 dValue)
		{
		    * ((DWord *) this) = dValue;
		    return    *  this;
		};
	};

	struct MScanStatus : public MMotorStatus
	{
	_U32	bFGConnected	: 1 ;
	_U32	bFGDataFlow	: 1 ;
	_U32	bFGDataOverflow	: 1 ;
	_U32	bFGDataReady	: 1 ;

		MScanStatus & operator = (_U32 dValue)
		{
		    * ((DWord *) this) = dValue;
		    return    *  this;
		};
	};

	struct MM2Status
	{
	_U32	bConnected	: 1 ;

		MM2Status & operator = (_U32 dValue)
		{
		    * ((DWord *) this) = dValue;
		    return    *  this;
		};
	};

	DWord				m_dEquMask	    ;
	DWord				m_dHVReady	    ;
	Bool				m_bPressDetector    ;

	KKMStatus			m_KKMStatus	    ;
	UrpStatus			m_UrpStatus	    ;
	MScanStatus			m_MScanStatus	    ;
	MMotorStatus			m_MDiafrStatus	    ;
	MM2Status			m_MM2Status	    ;

	CRRPSerialDevice::Register	m_RegGetEquStatus   ;

	CRRPSerialDevice::Register	m_RegGetStatusHV    ;

	CRRPSerialDevice::Register	m_ParSetFieldSize   ;
	CRRPSerialDevice::Register	m_ParSetPressMode   ;
	CRRPSerialDevice::Register	m_ParSetFocusSize   ;
	CRRPSerialDevice::Register	m_ParSetExposeMode  ;

	CRRPSerialDevice::Register	m_ParSetUAnode	    ;
	CRRPSerialDevice::Register	m_ParSetUAnode10    ;
	CRRPSerialDevice::Register	m_ParSetIAnode	    ;

	CRRPSerialDevice::Register	m_ParGetRotPos	    ;
	CRRPSerialDevice::Register	m_ParGetPress	    ;
	CRRPSerialDevice::Register	m_ParGetThick	    ;

	CRRPSerialDevice::Register	m_ParGetUAnode	    ;
	CRRPSerialDevice::Register	m_ParGetUAnode10    ;
	CRRPSerialDevice::Register	m_ParGetIAnode	    ;
	CRRPSerialDevice::Register	m_ParGetExposeTime  ;

	_U32				m_uParGetHeatID	    ;
	CRRPSerialDevice::Register	m_ParGetHeatTimeout ;

	CRRPSerialDevice::Register	m_ParSetPressHigh   ;

	CRRPSerialDevice::Register	m_ParGetShotCount   ;
	CRRPSerialDevice::Register	m_ParGetShotCountBad;

	UThickRow *			m_pThick	    ;
	UThickRow *			m_pThickLo	    ;
	UThickRow *			m_pThickHi	    ;

	UThickTable			m_UThickTable	    ;
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
/*
void CRRPMDigit::OnIdleRegPack (IRRPProtocol::ParamPack::Entry *& pItem, Register * r)
{
    if (r == & m_ParSetUAnode)
    {
	if (((m_ParSetUAnode10 + 5) / 10) != (int) r ->uValue)
	{     m_ParSetUAnode10		   = (int) r ->uValue * 10;}

	putBigEndianD (pItem, offsetof (IRRPProtocol::ParamPack::Entry, uParamId),
					IRRPMDIG::ID_ParSetUAnode);
	putBigEndianD (pItem, offsetof (IRRPProtocol::ParamPack::Entry, uValue  ),
		      ((((m_pIRRPUrp) ? m_pIRRPUrp ->GetUAnodeR10 (m_ParSetUAnode10)
				     :				   m_ParSetUAnode10)) + 5) / 10);
	pItem ++;

	return;
    }

	CRRPDevice::OnIdleRegPack (pItem, r);
};
*/

void CRRPMDigit::OnAckValue (CRRPSerialDevice::Register & r, _U32 uValue)
{
    if (FREG_Changed & r.OnAckValue (uValue))
    {
#if USE_INDICATOR_STAT

    if (& r == & m_RegStatus)
    {{
	MM2IndSetValue
    (((m_UrpStatus.bHVPrepared | m_UrpStatus.bHVOn) ? 0x80000000 : 0x00000000) | r.uValue);

    }}

#endif
/*
printf ("CRRPMDigit::Changed (%08X) <-%08X\n", r.uCodeMask_1_0, r.uValue);
*/
	OnIdleRegList ();
    }
};

int CRRPMDigit::GetUAnode (int nUAnodeR)
{
/*
    int	 i;

    for (i = 0; i < m_IFilamentTable.nRows; i ++)
    {
    if (m_IFilamentTable.pRows [i].nUAnodeX >= nUAnodeR)
    {	break;}
    }
    }
	return m_IFilamentTable.pRows [i].nUAnodeX

    if (m_IFilamentTable.pRows [0			  ].nUAnodeX > nUAnodeR)
    {
	nUAnodeR =
	m_IFilamentTable.pRows [0			  ].nUAnodeX;
    }
    else
    if (m_IFilamentTable.pRows [m_IFilamentTable.nRows - 1].nUAnodeX < nUAnodeR)
    {
	nUAnodeR =
	m_IFilamentTable.pRows [m_IFilamentTable.nRows - 1].nUAnodeX;
    }
    else
    {
    }

    for (int i = 0; i < m_IFilamentTable.nRows; i ++)
    {
	if 
    }
*/
	return nUAnodeR;
};

/*
int CRRPMDigit::GetIAnodeRFromThickR (int uThickR)
{
    return uThickR;
};
*/

/*
#define	INT_SCALE   256

struct ZPoint
{
    int	    x;
    int	    y;
    int	    z;
};

static void __interpolate (ZPoint & p, const ZPoint & p11, const ZPoint & p21,
				       const ZPoint & p12, const ZPoint & p22)
{
    int u, v, z;

	u = ((p11.z * INT_SCALE * (p12.y - p11.y)) + (p12.z * INT_SCALE * (p.y - p11.y))) / (p12.y - p11.y);
	v = ((p21.z * INT_SCALE * (p12.y - p11.y)) + (p22.z * INT_SCALE * (p.y - p11.y))) / (p12.y - p11.y);

	z = ((u * (p21.x - p.x)) + (v * (p.x - p11.x))) / (p21.x - p11.x);

      p.z =  (z / INT_SCALE) + ((z % INT_SCALE) ? ((0 < z) ? 1 : -1) : 0);
};
*/
Bool CRRPMDigit::UThickRow::GetUThickEntry (CRRPMDigit::UThickEntry *& pLo,
					    CRRPMDigit::UThickEntry *& pHi, int nUAnodeR)
{
    int	 n, i;

    if ((n = nEntries) == 0)
    {	return 0;}

	 n --;

    if (nUAnodeR < pEntries [0].nUAnodeR	   )
    {
	return (pLo = pHi = & pEntries [0], 2);
    }
    else
    if (	   pEntries [n].nUAnodeR < nUAnodeR)
    {
	return (pLo = pHi = & pEntries [n], 1);
    }

    for (i = 0; (i < n) && (pEntries [i].nUAnodeR < nUAnodeR); i ++)
    {}

	pHi =   pEntries + i;
	pLo =  (pEntries < pHi) ? (pHi - 1) : pHi;

	return 3;
};

Bool CRRPMDigit::UThickTable::GetUThickRow (CRRPMDigit::UThickRow *& pLo,
					    CRRPMDigit::UThickRow *& pHi, int nThickR)
{
    int	 n, i;

    if ((n = nRows) == 0)
    {	return 0;}

	 n --;

    if (nThickR < pRows [0].nUThickR	      )
    {	
	return (pLo = pHi = & pRows [0], 2);
    }
    else
    if (	  pRows [n].nUThickR < nThickR)
    {
	return (pLo = pHi = & pRows [n], 1);
    }

    for (i = 0; (i < n) && (pRows [i].nUThickR < nThickR); i ++)
    {}

	pHi =   pRows + i;
	pLo =  (pRows < pHi) ? (pHi - 1) : pHi;

	return 3;
};

int CRRPMDigit::UThickTable::GetUThickCol (CRRPMDigit::UThickEntry ** pEntries, int * pThicks, int nUAnodeR)
{
    int	 n, i, j;

    for (n = 0, i = 0; i < nRows; i ++)
    {
	for    (j = 0; j < pRows [i].nEntries; j ++)
	{
	    if (nUAnodeR == pRows [i].pEntries [j].nUAnodeR)
	    {
		pThicks  [n   ] =   pRows [i].nUThickR	  ;
		pEntries [n ++] = & pRows [i].pEntries [j];
		break;
	    }
	}
    }
    return n;
};
//
//[]------------------------------------------------------------------------[]
//
void CRRPMDigit::LoadThickTable (char * pBuf)
{
    GStreamBuf	s     (pBuf, ::lstrlen (pBuf));
		s.put (	     ::lstrlen (pBuf));

    int		uThick, nUA, nIA, uType, c;

    UThickRow	* r;
    UThickEntry	* e;

lNextThick :
	     s.ScanfA (" \t\r\n", NULL);
    if (1 != s.ScanfA (" \t", "<%d, ", & uThick))
	return;

    r = & m_UThickTable.pRows [m_UThickTable.nRows ++];
    r ->nUThickR = uThick;
    r ->nEntries = 0	 ;

lNextUAnode :
	     s.ScanfA (" \t\r\n", NULL);
    if (3 != s.ScanfA (" \t", "<%d,%d,%d>", & nUA, & nIA, & uType))
	return;

    e = & r ->pEntries [r ->nEntries ++];

    e ->uType	 = uType;
    e ->nUAnodeR = nUA	;
    e ->nIAnodeR = nIA  ;

	     s.ScanfA (" \t", NULL);
    if (',' == (c = s.GetChrA ()))
	goto lNextUAnode;
    else
    if ('>' !=  c)
	return;

	     s.ScanfA (" \t", NULL);

    if (',' == s.GetChrA ())
	goto lNextThick;

	     s.ScanfA (" \t\r\n", NULL);

    if (0 != s.sizeforget ())
    {
	return;
    }

#if TRUE

	{{
	    printf ("UThick Table (%d) rows :\n", m_UThickTable.nRows);

	for (int i = 0; i < m_UThickTable.nRows; i ++)
	{
	    printf ("%2d (%2d)",  m_UThickTable.pRows [i].nUThickR,
				  m_UThickTable.pRows [i].nEntries);

	for (int j = 0; j < m_UThickTable.pRows [i].nEntries; j ++)
	{
	    printf ("{%d %d %d}", m_UThickTable.pRows [i].pEntries [j].nUAnodeR,
				  m_UThickTable.pRows [i].pEntries [j].nIAnodeR,
				  m_UThickTable.pRows [i].pEntries [j].uType   );
	}
	    printf ("\n");
	}
	}}

#endif
};

void CRRPMDigit::LoadIniParam ()
{
    DWord   dBufSize ;
    char    pBuf [4 * 1024];

    m_UThickTable.nRows = 0;

    if (ERROR_SUCCESS  == GetIRRPSettings () ->GetValue 
			 (IRRPSettings::ID_MDIG_ThickTable,
			  pBuf, sizeof (pBuf), & dBufSize))
    {
	LoadThickTable   (pBuf);
    }
};
//
//[]------------------------------------------------------------------------[]
//
CRRPMDigit::CRRPMDigit (IRRPDeviceService * pIRRPDevService) : CRRPDevice (MDIG_DEVICE_ID)
{
    IRRPMDIG_Entry   IEntry;

    * ((void **) & m_IRRPDevice) = * ((void **) & IEntry);

   if (   pIRRPDevService)
   {
	MM2IndEnable (True);

       (m_pIRRPDevService  = pIRRPDevService) ->AddRef ();
   }

    m_pIRRPUrp		    = NULL;
    m_hIRRPUrpCallBack	    = NULL;

    m_pIRRPMScan	    = NULL;
    m_hIRRPMScanCallBack    = NULL;
    m_bGrab		    =	 0;
    m_dGrabOffset	    =	 0;
    m_pIRRPRepairer	    = NULL;

    m_pIRRPMDiafr	    = NULL;
    m_hIRRPMDiafrCallBack   = NULL;

    m_pIRRPMM2		    = NULL;
    m_hIRRPMM2CallBack	    = NULL;

    m_pShotIrp		    = NULL;
    m_uDiafrWidth	    =    0;

    m_nActiveBeg	    =
    m_nActiveEnd	    =	 0;
    m_nDCBeg		    =
    m_nDCEnd		    =	 0;

    m_dHVReady		    =	 0;
    m_dEquMask		    =	 0;
    m_bPressDetector	    = True;

    m_KKMStatus		    = 0x00;
    m_UrpStatus		    = 0x00;
    m_MScanStatus	    = 0x00;
    m_MDiafrStatus	    = 0x00;
    m_MM2Status		    = 0x00;

    m_RegList.append (m_RegGetEquStatus.
		      DefineSWReg_32	(_WREG_MASK (0xFF,0x00, 0xFF,0x7C),
					 _WREG_MASK (0xFF,0x00, 0xFF,0x00)));
    m_RegList.append (m_RegGetStatusHV.
		      DefineSWReg_32	(_WREG_MASK (0xFF,0x00, 0xFF,0x7F),
					 _WREG_MASK (0x00,0x00, 0xFF,0x00)));

    m_RegList.append (m_ParSetFieldSize.
		      DefineSWReg_16	(_WREG_MASK (0x00,0x00, 0x03,0x01)));
    m_RegList.append (m_ParSetPressMode.
		      DefineSWReg_16	(_WREG_MASK (0x00,0x00, 0x01,0x02)));
    m_RegList.append (m_ParSetFocusSize.
		      DefineSWReg_16	(_WREG_MASK (0x00,0x00, 0x01,0x03)));
    m_RegList.append (m_ParSetExposeMode.
		      DefineSWReg_16	(_WREG_MASK (0x00,0x00, 0x03,0x04)));

    m_RegList.append (m_ParSetUAnode.
		      DefineSWReg_16	(_WREG_MASK (0x00,0x00, 0xFF,0x06)));
    m_RegList.append (m_ParSetUAnode10.
		      DefineSWReg_16	(_WREG_MASK (0x00,0x00, 0xFF,0x0A)));

    m_RegList.append (m_ParSetIAnode.
		      DefineSWReg_16	(_WREG_MASK (0x00,0x00, 0xFF,0x07)));

    m_RegList.append (m_ParGetRotPos.
		      DefineSWReg_16	(_WREG_MASK (0x03,0x01, 0xFF,0x01)));
    m_RegList.append (m_ParGetPress.
		      DefineSWReg_16	(_WREG_MASK (0x03,0x01, 0xFF,0x02)));
    m_RegList.append (m_ParGetThick.
		      DefineSWReg_16	(_WREG_MASK (0x03,0x01, 0xFF,0x03)));

    m_RegList.append (m_ParGetUAnode10.
		      DefineSWReg_16	(_WREG_MASK (0x03,0x01, 0xFF,0x08)));
    m_RegList.append (m_ParGetUAnode.
		      DefineSWReg_16	(_WREG_MASK (0x03,0x01, 0xFF,0x04)));
    m_RegList.append (m_ParGetIAnode.
		      DefineSWReg_16	(_WREG_MASK (0x03,0x01, 0xFF,0x05)));
    m_RegList.append (m_ParGetExposeTime.
		      DefineSWReg_16	(_WREG_MASK (0X03,0X01, 0xFF,0x07)));

    m_uParGetHeatID	= 0;
    m_RegList.append (m_ParGetHeatTimeout.
		      DefineSWReg_16	(_WREG_MASK (0x7F,0x01, 0xFF,0x06)));

    m_RegList.append (m_ParSetPressHigh.
		      DefineSWReg_16	(_WREG_MASK (0x00,0x00, 0xFF,0x09)));

    m_RegList.append (m_ParGetShotCount.
		      DefineSWReg_32	(_WREG_MASK (0xFF,0x02, 0xFF,0x04),
					 _WREG_MASK (0x7F,0x00, 0xFF,0x00)));
    m_RegList.append (m_ParGetShotCountBad.
		      DefineSWReg_32	(_WREG_MASK (0xFF,0x02, 0xFF,0x05),
					 _WREG_MASK (0x7F,0x00, 0xFF,0x00)));

    m_pThick		=
    m_pThickLo		=
    m_pThickHi		= m_UThickTable.pRows;

    m_UThickTable.nRows	= 0;
    
    LoadIniParam ();

    if (ERROR_SUCCESS == CreateRRPRepairer (m_pIRRPRepairer, NULL))
    {}

//  m_RegList.append (m_ParImageHeightFlow.
//		      DefineSWReg_16	(_WREG_MASK (0x7F,0x00, 0xFF, 0x0A)));
//
//  m_RegList.append (m_ParSetIFilament.
//		      DefineSWReg_16	(_WREG_MASK (0x03,0x00, 0xFF, 0x0B)));
};

CRRPMDigit::~CRRPMDigit ()
{
    if (m_pIRRPRepairer	 )
    {
	m_pIRRPRepairer ->Release ();
	m_pIRRPRepairer = NULL;
    }
    if (m_pIRRPDevService)
    {
	m_pIRRPDevService ->Release ();
	m_pIRRPDevService = NULL;
    }
};

void CRRPMDigit::SetEquMask (DWord dEquMask)
{
    if ( ( (dEquMask & IRRPMDIG::EquMask_Urp)) && (m_pIRRPUrp == NULL))
    {
	if (ERROR_SUCCESS == m_pIRRPDevService ->CreateRRPDevice 
			    ((void **) & m_pIRRPUrp	,
			      IID_IUnknown		,
			     (void  *) URP_DEVICE_ID	,
			      IID_IUnknown		))
	{
	    m_pIRRPUrp ->Connect (m_hIRRPUrpCallBack	,
				& m_sIRRPCallBack	);
	}
	else
	{
	    dEquMask &= (~IRRPMDIG::EquMask_Urp);
	}
    }
//............................................................
//
	RRPSetParam (m_pIRRPUrp, IRRPUrp::ID_ParSetEquMask,
		    (dEquMask  & IRRPMDIG::EquMask_KKM) ? IRRPUrp::EquMask_KKM : 0);
//
//............................................................
    if ( (!(dEquMask & IRRPMDIG::EquMask_Urp)) && m_pIRRPUrp)
    {
	m_uParGetHeatID	    = 0;
	SetRegValue (m_ParGetHeatTimeout, 0);

	m_hIRRPUrpCallBack  ->Close ();
	m_hIRRPUrpCallBack  = NULL;

	m_pIRRPUrp  ->Release ();
	m_pIRRPUrp  = NULL;

	m_UrpStatus = 0x00;
    }
    if ( ( (dEquMask & IRRPMDIG::EquMask_MScan)) && (m_pIRRPMScan == NULL))
    {
	if (ERROR_SUCCESS == m_pIRRPDevService ->CreateRRPDevice 
			    ((void **) & m_pIRRPMScan	,
			      IID_IUnknown		,
			     (void  *) MSCAN_DEVICE_ID	,
			      IID_IUnknown		))
	{
	    m_pIRRPMScan ->Connect (m_hIRRPMScanCallBack,
				  & m_sIRRPCallBack	);
	}
	else
	{
	    dEquMask &= (~IRRPMDIG::EquMask_MScan);
	}
    }
    if ( (!(dEquMask & IRRPMDIG::EquMask_MScan)) && m_pIRRPMScan)
    {
	m_hIRRPMScanCallBack  ->Close ();
	m_hIRRPMScanCallBack  = NULL;

	m_pIRRPMScan  ->Release ();
	m_pIRRPMScan  = NULL;

	m_MScanStatus = 0x00;
    }
//............................................................
    if ( ( (dEquMask & IRRPMDIG::EquMask_MDiafr)) && (m_pIRRPMDiafr == NULL))
    {
	if (ERROR_SUCCESS == m_pIRRPDevService ->CreateRRPDevice 
			    ((void **) & m_pIRRPMDiafr  ,
			      IID_IUnknown		,
			     (void  *) MDIAFR_DEVICE_ID ,
			      IID_IUnknown		))
	{
	    m_pIRRPMDiafr ->Connect (m_hIRRPMDiafrCallBack,
				   & m_sIRRPCallBack    );
	}
	else
	{
	    dEquMask &= (~IRRPMDIG::EquMask_MDiafr);
	}
    }
    if ( (!(dEquMask & IRRPMDIG::EquMask_MDiafr)) && m_pIRRPMDiafr)
    {
	m_hIRRPMDiafrCallBack ->Close ();
	m_hIRRPMDiafrCallBack = NULL;

	m_pIRRPMDiafr  ->Release ();
	m_pIRRPMDiafr  = NULL;

	m_MDiafrStatus = 0x00;
    }
//............................................................
    if ( ( (dEquMask & IRRPMDIG::EquMask_MM2)) && (m_pIRRPMM2 == NULL))
    {
	if (ERROR_SUCCESS == m_pIRRPDevService ->CreateRRPDevice 
			    ((void **) & m_pIRRPMM2	,
			      IID_IUnknown		,
			     (void  *) MM2_DEVICE_ID	,
			      IID_IUnknown		))
	{
	    m_pIRRPMM2 ->Connect (m_hIRRPMM2CallBack	,
				& m_sIRRPCallBack	);
	}
	else
	{
	    dEquMask &= (~IRRPMDIG::EquMask_MM2);
	}
    }
    if ( (!(dEquMask & IRRPMDIG::EquMask_MM2)) && m_pIRRPMM2)
    {
	m_hIRRPMM2CallBack  ->Close ();
	m_hIRRPMM2CallBack  = NULL;

	m_pIRRPMM2  ->Release ();
	m_pIRRPMM2  = NULL;

	m_MM2Status = 0x00;
    }
//............................................................
	m_dEquMask  = dEquMask;

	SetRegStatus (0x00000001
		    | 0x00000002
		    | 0x00000010
		    | 0x00000020
		    | 0x00000040
		    | 0x00000100
		    | 0x00000200
		    | 0x00000400
		    | 0x00000800
		    | 0x00001000
		    | 0x00012000, GetSetHVReady ());
};

void GAPI CRRPMDigit::SetEquMask (int nArgs, void ** pArgs, HANDLE hResume)
{
    ((CRRPMDigit *) pArgs [0]) ->SetEquMask ((DWord) pArgs [1]);

    ::SetEvent (hResume);
};

HRESULT CRRPMDigit::Create (void ** pInterface)
{
    m_XUnknown  .Init (this);

    * pInterface = m_IRRPDevice.Init (this, IID_NULL, & m_XUnknown);

    m_XUnknown	.Release ();

    GetIRRPSettings () ->GetValue (IRRPSettings::ID_MDIG_EquMask,
				 & m_dEquMask, sizeof (DWord), NULL);
    SetEquMask (m_dEquMask);

    GetIRRPSettings () ->GetValue (IRRPSettings::ID_MDIG_DiafrWidth,
				 & m_uDiafrWidth, sizeof (int), NULL);
    GetIRRPSettings () ->GetValue (IRRPSettings::ID_MDIG_ScanTime,
				 & m_uScanTime, sizeof (int), NULL);

    GetIRRPSettings () ->GetValue (IRRPSettings::ID_MSC_ImageActiveBeg,
				 & m_nActiveBeg, sizeof (int), NULL);
    GetIRRPSettings () ->GetValue (IRRPSettings::ID_MSC_ImageActiveEnd,
				 & m_nActiveEnd, sizeof (int), NULL);

    GetIRRPSettings () ->GetValue (IRRPSettings::ID_MSC_ImageDCBeg,
				 & m_nDCBeg, sizeof (int), NULL);
    GetIRRPSettings () ->GetValue (IRRPSettings::ID_MSC_ImageDCEnd,
				 & m_nDCEnd, sizeof (int), NULL);

    return ERROR_SUCCESS;
};

HRESULT	CRRPMDigit::CreateInstance (void ** pInterface, IRRPDeviceService * pIRRPDevService)
{
    return (new CRRPMDigit (pIRRPDevService)) ->Create  (pInterface);
};

HRESULT CRRPMDigit::XUnknown_QueryInterface (REFIID iid, void ** pi)
{
    HRESULT Result = ERROR_NOT_SUPPORTED;

    LockAcquire ();

	if (m_XUnknown.GetRefCount () == 0)
	{
	    m_XUnknown.Init (this);
	}
	else
	{
	    m_XUnknown.AddRef ();
	}

	if (IID_IUnknown == iid)
	{
	    (* (XUnknown **) pi = & m_XUnknown) ->AddRef ();

	    Result = ERROR_SUCCESS;
	}

    LockRelease ();

	    m_XUnknown.Release ();

    return Result;
};

void CRRPMDigit::XUnknown_FreeInterface (void * pi)
{
    if (pi == & m_XUnknown)
    {
	MM2IndEnable (False);
    {{
	SetEquMask (0x0000);

	printf ("Destroy MDigit object CallBackCount %d\n", m_sIRRPCallBack.GetRefCount ());

	delete this;
    }}
    }
};

void CRRPMDigit::SetIAnode (int nIAnodeR)
{
    int	nUAnodeR = (int) m_ParSetUAnode.uValue;

    if (m_ParSetExposeMode.uValue == IRRPMDIG::ExposeMode_Auto)
    {{
	for (int i = 0; i < m_pThick ->nEntries; i ++)
	{
	    if (m_pThick ->pEntries [i].uType == 2)
	    {
		nIAnodeR = 
		m_pThick ->pEntries [i].nIAnodeR;

		break;
	    }
	}
    }}
    else
    if (m_ParSetExposeMode.uValue == IRRPMDIG::ExposeMode_AutoHalf)
    {{
	for (int i = 0; i < m_pThick ->nEntries; i ++)
	{
	    if (m_pThick ->pEntries [i].nUAnodeR == nUAnodeR)
	    {
		nIAnodeR =
		m_pThick ->pEntries [i].nIAnodeR;
	    }
	}
    }}
// Add min/max UAnode/IAnode correction !!!!
//
    if (m_pIRRPUrp)
    {
	m_pIRRPUrp ->CheckExposition 
      ((m_ParSetFocusSize.uValue == IRRPMDIG::FocusSize_BF) ? True : False, & nUAnodeR, & nIAnodeR);
    }

    if ((m_ParSetExposeMode.uValue < IRRPMDIG::ExposeMode_Manual) && GetCRRPMScan ())
    {
	((CRRPMScan *) GetCRRPMScan ()) ->CheckExposition (& nUAnodeR, & nIAnodeR);
    }

    SetRegValue (m_ParSetUAnode, nUAnodeR);
    SetRegValue (m_ParSetIAnode, nIAnodeR);
};

void CRRPMDigit::SetUAnode10 (int nUAnodeR10)
{
    int nUAnodeR = (nUAnodeR10 + 5) / 10;

    if (m_ParSetExposeMode.uValue != IRRPMDIG::ExposeMode_Manual)
    {
	if (nUAnodeR < m_pThick ->pEntries [0].nUAnodeR)
	{   nUAnodeR = m_pThick ->pEntries [0].nUAnodeR;}
	if (nUAnodeR > m_pThick ->pEntries [m_pThick ->nEntries - 1].nUAnodeR)
	{   nUAnodeR = m_pThick ->pEntries [m_pThick ->nEntries - 1].nUAnodeR;}

	if (m_ParSetExposeMode.uValue == IRRPMDIG::ExposeMode_Auto)
	{
	    for (int i = 0; i < m_pThick ->nEntries; i ++)
	    {
		if (m_pThick ->pEntries [i].uType == 2)
		{
		    nUAnodeR = 
		    m_pThick ->pEntries [i].nUAnodeR;

		    break;
		}
	    }
	}
    }

// Add min/max UAnode/IAnode correction !!!!
//
    int	n = 0;

    if (m_pIRRPUrp)
    {
	m_pIRRPUrp ->CheckExposition 
      ((m_ParSetFocusSize.uValue == IRRPMDIG::FocusSize_BF) ? True : False, & nUAnodeR, & n);
    }

    if (((nUAnodeR10 + 5) / 10) != nUAnodeR)
    {
	nUAnodeR10 = nUAnodeR * 10;
    }

    SetRegValue (m_ParSetUAnode  , nUAnodeR  );
    SetRegValue (m_ParSetUAnode10, nUAnodeR10);

    SetIAnode	(m_ParSetIAnode .uValue  );
};

void CRRPMDigit::SetThick (int nThickR)
{
				SetRegValue (m_ParGetThick, nThickR);
    m_UThickTable.GetUThickRow (m_pThickLo,  m_pThickHi	  , nThickR);

    m_pThick = ((nThickR - m_pThickLo ->nUThickR) < (m_pThickHi ->nUThickR - nThickR)) 
			      ? m_pThickLo : m_pThickHi;

    SetUAnode10 (m_ParSetUAnode10.uValue);
};

void CRRPMDigit::SetParam (const IRRPDevice::Param * pParam)
{
printf ("[%08X] <- %08X (%d)\n", pParam ->uParamId, pParam ->u32Value, pParam ->u32Value);

//    _U32    uIFilament;

    switch (pParam ->uParamId)
    {
    case IRRPMDIG::ID_ParSetEquMask :
	Xxxx_SetEquMask ((IRRPMDIG::EquMask) pParam ->u32Value);
	break;

    case IRRPMDIG::ID_ParSetFieldSize :
	SetRegValue (m_ParSetFieldSize, pParam ->u32Value);
	break;

    case IRRPMDIG::ID_ParSetPressMode :
	SetRegValue (m_ParSetPressMode, pParam ->u32Value);
	break;

    case IRRPMDIG::ID_ParSetFocusSize :

// Add min/max UAnode/IAnode correction !!!!
//

    if (m_ParSetExposeMode.uValue != IRRPMDIG::ExposeMode_Manual)
    {
	SetRegValue (m_ParSetFocusSize, IRRPMDIG::FocusSize_BF);
	SetUAnode10 (m_ParSetUAnode10.uValue);
	break;
    }
	SetRegValue (m_ParSetFocusSize, pParam ->u32Value);
	SetUAnode10 (m_ParSetUAnode10.uValue);
	break;

    case IRRPMDIG::ID_ParSetExposeMode :
    if (pParam ->u32Value != IRRPMDIG::ExposeMode_Manual)
    {
	SetRegValue (m_ParSetFocusSize,  IRRPMDIG::FocusSize_BF);
    }
	SetRegValue (m_ParSetExposeMode, pParam ->u32Value);

	SetThick    (m_ParGetThick.uValue);
	break;

    case IRRPMDIG::ID_ParSetThick :

	if (IRRPMDIG::PressDetector_Off == (IRRPMDIG::PressDetector) pParam ->u32Value)
	{
	    m_bPressDetector = False;
	}
	else
	if (IRRPMDIG::PressDetector_On  == (IRRPMDIG::PressDetector) pParam ->u32Value)
	{
	    m_bPressDetector = True ;

	    if (m_hIRRPMM2CallBack)
		m_hIRRPMM2CallBack ->Refresh (NULL);
	}
	else
	if (! m_bPressDetector)
	{
	    SetThick (pParam ->u32Value);
	}
	break;

    case IRRPMDIG::ID_ParSetUAnode :

	SetUAnode10 (pParam ->u32Value * 10);
	break;

    case IRRPMDIG::ID_ParSetUAnode10 :

	SetUAnode10 (pParam ->u32Value);
	break;

    case IRRPMDIG::ID_ParSetIAnode :
	SetIAnode (pParam ->u32Value);
	break;

    case IRRPMDIG::ID_ParDecompress :
	RRPSetParam (m_pIRRPMM2, IRRPMM2::ID_RunPress, INT_MAX);
	break;

    case IRRPMDIG::ID_ParSetPressHigh :
	RRPSetParam (m_pIRRPMM2, IRRPMM2::ID_RegSetPressHigh,
		    (20 > pParam ->u32Value) ? pParam ->u32Value * 2 : 20 * 2);
	break;
    };
};

void CRRPMDigit::On_IRRPCallBack (const void * hIRRPCallBack, _U32 uParamId, _U32 uValue)
{
// Proxy notifications :
//
    if ((0 == HIBYTE(HIWORD(uParamId))) && (hIRRPCallBack == m_hIRRPMScanCallBack ))
    {
	uParamId |= 0xD6000000;
    }
    if ((0 == HIBYTE(HIWORD(uParamId))) && (hIRRPCallBack == m_hIRRPMDiafrCallBack))
    {
	uParamId |= 0xD7000000;
    }

    switch (uParamId)
    {
    case 0x80000000 :
	m_UrpStatus	.bConnected	= (uValue & IRRPUrp::IRRPDevice_DevOnline) ? 1 : 0;

	OnAckBits      (m_RegGetEquStatus, 0x80000000, (uValue & IRRPUrp::IRRPDevice_BusOnline) 
					 ? 0x80000000		 : 0);
	OnAckBits      (m_RegGetEquStatus, IRRPMDIG::EquMask_Urp,
       (m_UrpStatus	.bConnected)	 ? IRRPMDIG::EquMask_Urp : 0);
	break;

    case IRRPUrp::ID_RegGetHVReady :
	m_dHVReady = uValue;
	break;

//  case 0x80000108 :
    case IRRPUrp::ID_RegKKMStatus :
	m_UrpStatus	.bKKMConnected  = (uValue & IRRPKKM::IRRPDevice_DevOnline) ? 1 : 0;

	OnAckBits      (m_RegGetEquStatus, 0x80000000, (uValue & IRRPUrp::IRRPDevice_BusOnline) 
					 ? 0x80000000		 : 0);
	OnAckBits      (m_RegGetEquStatus, IRRPMDIG::EquMask_KKM, 
       (m_UrpStatus	.bKKMConnected)  ? IRRPMDIG::EquMask_KKM : 0x00);
	break;

    case 0x8000D1D0 :
	m_UrpStatus	.bHVBlocked	= (uValue & 0x007F7F7F)			   ? 1 : 0;
	break;

    case 0x8000D4D3 :
	m_UrpStatus	.bHVPrepared = (uValue & 0x0100) ? 1 : 0;
	m_UrpStatus	.bHVOn	     = (uValue & 0x0200) ? 1 : 0;
	break;

    case IRRPUrp::ID_ParGetUAnode10 :
	OnAckValue (m_ParGetUAnode10,  uValue	       );
	OnAckValue (m_ParGetUAnode  , (uValue + 5) / 10);
	break;

//  case IRRPUrp::ID_ParGetUAnode :
//	OnAckValue (m_ParGetUAnode, uValue);
//	break;
#if TRUE
    case 0x8000E7E6 :
#else
    case 0x8000E5E4 :
#endif
	OnAckValue (m_ParGetIAnode, uValue);
	break;

    case 0x8000E9E8 :
	OnAckValue (m_ParGetExposeTime, uValue);
	break;

    case IRRPUrp::ID_RegHeatTubeTimeout  :
    case IRRPUrp::ID_RegHeatForceTimeout :
	if ((m_uParGetHeatID == uParamId) || (m_ParGetHeatTimeout.uValue < uValue))
	{
	     m_uParGetHeatID  = uParamId;

	     OnAckValue (m_ParGetHeatTimeout, uValue);
	}
	break;

    case IRRPUrp::ID_ParGetShotCount	:
	OnAckValue (m_ParGetShotCount, uValue);
	break;

    case IRRPUrp::ID_ParGetShotCountBad :
	OnAckValue (m_ParGetShotCountBad, uValue);
	break;

    case 0xD6000000 :
	m_MScanStatus	.bConnected = (uValue & 0x0002) ? 1 : 0;

	OnAckBits      (m_RegGetEquStatus, 0x80000000, (uValue & IRRPUrp::IRRPDevice_BusOnline) 
					 ? 0x80000000		   : 0);
	OnAckBits      (m_RegGetEquStatus, IRRPMDIG::EquMask_MScan,
       (m_MScanStatus	.bConnected)	 ? IRRPMDIG::EquMask_MScan : 0);
	break;

    case 0xD600D7D6 :
	m_MScanStatus	.bMove	    = (uValue & 0x0100) ? 1 : 0;
	m_MScanStatus	.bBegPin    = (uValue & 0x0200) ? 1 : 0;
	m_MScanStatus	.bEndPin    = (uValue & 0x0400) ? 1 : 0;
	break;

    case IRRPMScan::ID_RegGetFGStatus :
	m_MScanStatus	.bFGConnected = (uValue & IRRPMScan::FGStatus_Online   ) ? 1 : 0;
	m_MScanStatus	.bFGDataFlow  = (uValue & IRRPMScan::FGStatus_DataSof  ) ? 1 : 0;
	m_MScanStatus	.bFGDataReady = (uValue & IRRPMScan::FGStatus_DataReady) ? 1 : 0;
//	m_MScanStatus	.bFGDataReady = (uValue & IRRPMScan::FGStatus_DataEof  ) ? 1 : 0;
	m_MScanStatus	.bFGDataOverflow
				      = (uValue & IRRPMScan::FGStatus_Overflow) ? 1 : 0;

	OnAckBits      (m_RegGetEquStatus, IRRPMDIG::EquMask_MFG,
       (m_MScanStatus	.bFGConnected)	 ? IRRPMDIG::EquMask_MFG : 0);
	break;

    case 0xD7000000 :
	m_MDiafrStatus	.bConnected = (uValue & 0x0002) ? 1 : 0;

	OnAckBits      (m_RegGetEquStatus, 0x80000000, (uValue & IRRPUrp::IRRPDevice_BusOnline) 
					 ? 0x80000000		   : 0);
	OnAckBits      (m_RegGetEquStatus, IRRPMDIG::EquMask_MDiafr,
       (m_MDiafrStatus	.bConnected)	 ? IRRPMDIG::EquMask_MDiafr : 0);
	break;

    case 0xD700D7D6 :
	m_MDiafrStatus	.bMove	    = (uValue & 0x0100) ? 1 : 0;
	m_MDiafrStatus	.bBegPin    = (uValue & 0x0200) ? 1 : 0;
	m_MDiafrStatus	.bEndPin    = (uValue & 0x0400) ? 1 : 0;
	break;

    case 0x91000000 :
	m_MM2Status	.bConnected = (uValue & 0x0002) ? 1 : 0;

	OnAckBits      (m_RegGetEquStatus, 0x80000000, (uValue & IRRPUrp::IRRPDevice_BusOnline) 
					 ? 0x80000000		 : 0);
	OnAckBits      (m_RegGetEquStatus, IRRPMDIG::EquMask_MM2,
       (m_MM2Status	.bConnected)	 ? IRRPMDIG::EquMask_MM2 : 0);
	break;

    case IRRPMM2::ID_RegGetRotPos :
	OnAckValue (m_ParGetRotPos, uValue);
	break;

    case IRRPMM2::ID_RegGetThick :
	if (m_bPressDetector)
	{
	    SetThick (uValue);
	}
	break;

    case IRRPMM2::ID_RegGetPress :
	OnAckValue (m_ParGetPress,  uValue);
	break;

    case IRRPMM2::ID_RegSetPressHigh :
	OnAckValue (m_ParSetPressHigh, uValue / 2);
	break;

    case IRRPMScan::ID_RegGetFGOffsetEx :
	if (m_bGrab)
	{{
	    void *  pSrcData;
	    DWord   dLength = m_pIRRPMScan ->GetDataPtr
		   (pSrcData, m_dGrabOffset, 64 * (1024 * 1024));

	    if (0 < dLength)
	    {
		m_dGrabOffset += m_pIRRPRepairer ->PushData (pSrcData, dLength);
	    }
	}}
	break;

// Self  notifications :
//
//  case IRRPMDIG::ID_ParSetExposeMode :
//	printf ("Control Point...\n");
//	break;
//
// Self  notifications
    };

    Bool    bReady = True;

    if (m_pShotIrp)
    {	    bReady = False;}

    if (m_dEquMask & IRRPMDIG::EquMask_Urp)
    {
	if ((! m_UrpStatus.bConnected) || False)
	{   bReady = False;}
    }
    if (m_dEquMask & IRRPMDIG::EquMask_MScan)
    {
	if ((! m_MScanStatus .bConnected) || (m_MScanStatus .bMove) /*|| (! m_MScanStatus .bBegPin)*/) // Not check Beg Pin
	{   bReady = False;}
	if ((! m_MScanStatus .bFGConnected))
	{   bReady = False;}
    }
    if (m_dEquMask & IRRPMDIG::EquMask_MDiafr)
    {
	if ((! m_MDiafrStatus.bConnected) || (m_MDiafrStatus.bMove) /*|| (! m_MDiafrStatus.bBegPin)*/) // Not check Beg Pin
	{   bReady = False;}
    }
    if (m_dEquMask & IRRPMDIG::EquMask_MM2)
    {
	if ((! m_MM2Status.bConnected) || False)
	{   bReady = False;}
    }

	OnAckBits   (m_RegStatus,
		      0x00000001
		    | 0x00000002
		    | 0x00000010
		    | 0x00000020
		    | 0x00000040
		    | 0x00000100
		    | 0x00000200
		    | 0x00000400
		    | 0x00000800
		    | 0x00001000
		    | 0x00012000, GetSetHVReady ());
};
//
//[]------------------------------------------------------------------------[]
//
DWord CRRPMDigit::GetSetHVReady ()
{
    DWord   dNotReady  = 0x00000000;

    if (m_dEquMask & IRRPMDIG::EquMask_Urp)	    // Check Urp status 
    {
	    dNotReady |= (m_UrpStatus.bConnected    ) ? 0x00000000 : 0x00000001;
	    dNotReady |= 
		   (0 == m_ParGetHeatTimeout.uValue ) ? 0x00000000 : 0x00000002;

    if (m_dEquMask & IRRPMDIG::EquMask_KKM)
    {
	    dNotReady |= (m_UrpStatus.bKKMConnected ) ? 0x00000000 : 0x00000010;

	    dNotReady |= (m_dHVReady & IRRPUrp::HVReady_NotCharge) 
						      ? 0x00000020 : 0x00000000;

	    dNotReady |= (m_dHVReady & IRRPUrp::HVReady_RedButton)
						      ? 0x00000040 : 0x00000000;
    }
    }

    if (m_dEquMask & IRRPMDIG::EquMask_MScan)
    {
	    dNotReady |= (m_MScanStatus.bConnected  ) ? 0x00000000 : 0x00000100;

	if (! m_MScanStatus.bBegPin || m_MScanStatus.bMove)
	{
	    dNotReady |= 0x00000200;
	}
	if ((dNotReady & 0x00000200) && ! (m_MScanStatus.bMove) && ! (m_MScanStatus.bFGDataFlow))
	{
	    if (m_tScanBackTimeout.Infinite () || m_tScanBackTimeout.Occured ())
	    {
		m_tScanBackTimeout.Activate (2000);
	    if (m_pIRRPMScan)
		m_pIRRPMScan ->Run (IRRPMMotor::RunBackward , 15000);
	    }
	}
	    dNotReady |= (m_MScanStatus.bFGConnected) ? 0x00000000 : 0x00000400;
	    dNotReady |= (m_MScanStatus.bFGDataFlow ) ? 0x00000800 : 0x00000000;
    }

    if (m_dEquMask & IRRPMDIG::EquMask_MDiafr)
    {
	    dNotReady |= (m_MDiafrStatus.bConnected ) ? 0x00000000 : 0x00001000;

	if (! m_MDiafrStatus.bBegPin || m_MDiafrStatus.bMove)
	{
	    dNotReady |= 0x00002000;
	}
	if ((dNotReady & 0x00002000) && ! (m_MDiafrStatus.bMove))
	{
	    if (m_tDiafrBackTimeout.Infinite () || m_tDiafrBackTimeout.Occured ())
	    {
		m_tDiafrBackTimeout.Activate (2000);
	    if (m_pIRRPMDiafr)
		m_pIRRPMDiafr ->Run (IRRPMMotor::RunBackward, 15000);
	    }
	}
    }

    if (m_dEquMask & IRRPMDIG::EquMask_MM2)
    {
	    dNotReady |= (m_MM2Status.bConnected    ) ? 0x00000000 : 0x00010000;
    }

    return  dNotReady;
};
//
//[]------------------------------------------------------------------------[]
//
void CRRPMDigit::IRRPMDIG_ReleaseHV ()
{
    if (m_pShotIrp)
    {
	CancelIrp (m_pShotIrp);

	while (NULL != (volatile void *) m_pShotIrp)
	{   ::Sleep (20);}

	printf ("...Done\n");
    }
};

GIrp * CRRPMDigit::Co_RepairImageEx (GIrp * pIrp, void * pShitLock)
{
    IRRPRepairer::Phase 
    iPhase = m_pIRRPRepairer ->PeekImageStatus (NULL, NULL);

    if (IRRPRepairer::Done != iPhase)
    {
	SetCurCoProc <CRRPMDigit, void *>
       (pIrp, & CRRPMDigit::Co_RepairImageEx, this, pShitLock);

	pIrp ->pCancelProcLock.Exchange (NULL);
	QueueIrpForComplete (pIrp, 20);

	printf ("[]");

	return NULL;
    }
//				    SetRegValue (m_RegGetStatusHV, 0x00040000 | LOWORD (m_pIRRPRepairer ->PeekImageResult ()));
//
	printf ("...RepairImage Cancelled\n");

	m_bGrab	    = False;

	pIrp ->pCancelProcLock.Exchange ((GAccessLock *) pShitLock);

	return pIrp;
};

GIrp * CRRPMDigit::Co_RepairImage (GIrp * pIrp, void *)
{
    if (m_bGrab)
    {{
	int	     iStep    ;
	int	     iStepNum ;

    if (pIrp ->Cancelled () || (ERROR_SUCCESS != pIrp ->GetCurResult ()))
    {
	m_pIRRPRepairer ->CancelRepairing ();

	SetCurCoProc <CRRPMDigit, void *>
       (pIrp, & CRRPMDigit::Co_RepairImageEx, this, pIrp ->pCancelProcLock);

	pIrp ->pCancelProcLock.Exchange (NULL);
	QueueIrpForComplete (pIrp, 20);

	return NULL;
    }

	IRRPRepairer::Phase 
	iPhase = m_pIRRPRepairer ->PeekImageStatus (& iStep, & iStepNum);

    if (IRRPRepairer::Done	!= iPhase)
    {
	SetCurCoProc <CRRPMDigit, void *>
       (pIrp, & CRRPMDigit::Co_RepairImage, this, pIrp ->pCancelProcLock);

	QueueIrpForComplete (pIrp, 20);

	printf ("{}");
				    OnAckValue (m_RegGetStatusHV, 0x00040000 | 0);
	return NULL;
    }
				    OnAckValue (m_RegGetStatusHV, 0x00040000 | LOWORD (m_pIRRPRepairer ->PeekImageResult ()));

    if (0 != LOWORD (m_RegGetStatusHV.uValue))
    {
	pIrp ->SetResultInfo (5014, LOWORD (m_RegGetStatusHV.uValue));
    }
	printf ("...RepairImage Done with %08X\n", LOWORD (m_RegGetStatusHV.uValue));

	m_bGrab	   = False;
    }}
	return pIrp;
};

GIrp * CRRPMDigit::Co_SetHVDone (GIrp * pIrp, void *)
{
/*
    if (m_pIRRPMScan)
    {   m_pIRRPMScan  ->Run (IRRPMMotor::RunBackward, 15000);}
    if (m_pIRRPMDiafr)
    {   m_pIRRPMDiafr ->Run (IRRPMMotor::RunBackward, 15000);}
*/
    if (pIrp ->Cancelled () && (ERROR_SUCCESS == pIrp ->GetCurResult ()))
    {
	pIrp ->SetResultInfo (ERROR_CANCELLED, NULL);
    }
    if (ERROR_SUCCESS != pIrp ->GetCurResult ())
    {
	RRPSetParam (m_pIRRPMM2, IRRPMM2::ID_RunPress, INT_MAX);
    }

    if (ERROR_CANCELLED		== pIrp ->GetCurResult ())
    {
	pIrp ->SetResultInfo (1, NULL);
    }
    else
    if (ERROR_OPERATION_ABORTED == pIrp ->GetCurResult ())
    {
	pIrp ->SetResultInfo (2, NULL);
    }

    GetShotRequest * rq = GetCurRequest 
   <GetShotRequest> (pIrp);

	printf ("...SetHV Done with %08X\n", (LOWORD(pIrp ->GetCurResult ())));

	m_pShotIrp = NULL ;

    DWord StatusHV = m_RegGetStatusHV.uValue;

				    OnAckValue (m_RegGetStatusHV, 0x00000000 | (DWORD)(LOWORD(pIrp ->GetCurResult ())));
    if (True)
    {
	LogPrintf ("\r\n");
	LogPrintt (True);
	LogPrintf (" : ��������� ��������� ��������� ������\r\n");
	LogPrintf ("\t%d.%d (%08Xh)\r\n", HIWORD (StatusHV), LOWORD(pIrp ->GetCurResult ()), pIrp ->GetCurResultInfo ());
	LogFlush  ();
    }
	return pIrp;
};
//
//[]------------------------------------------------------------------------[]
//
GIrp * CRRPMDigit::Co_WaitEof (GIrp * pIrp, void *)
{
    if (pIrp ->Cancelled () || (ERROR_SUCCESS != pIrp ->GetCurResult ()))
    {	    return pIrp;}

    if (m_pIRRPMScan)
    {
	if (! m_MScanStatus.bFGConnected  )
	{
	    printf ("...Read Image Fail (FG not connected)\n");
	    pIrp ->SetResultInfo (5001, NULL);
	    return pIrp;
	}
	if (  m_MScanStatus.bFGDataOverflow)
	{
	    printf ("...Read Image Fail (FG overflow)\n");
	    pIrp ->SetResultInfo (5002, NULL);
	    return pIrp;
	}
	if (  m_MScanStatus.bFGDataReady)
	{
//	    if ((m_nDCEnd     <= m_pIRRPRepairer ->GetSrcSizeY ())
//	    &&  (m_nActiveEnd <= m_pIRRPRepairer ->GetSrcSizeY ()))
	    {
		m_pIRRPRepairer ->PushData (NULL, 0);
		printf ("...Read Image Done %d !!!\n", m_pIRRPRepairer ->GetSrcSizeY ());
		return pIrp;
	    }
	      m_MScanStatus.bFGDataReady = 0;
	}
//	if (! m_MScanStatus.bFGDataFlow )
//	{
//	    printf ("...Read Image Faile (FG no data)\n");
//	    pIrp ->SetResultInfo (5003, NULL);
//	    return pIrp;
//	}
	    SetCurCoProc  <CRRPMDigit, void *>
	   (pIrp, & CRRPMDigit::Co_WaitEof, this, NULL);

	    QueueIrpForComplete (pIrp, 20);
	    return NULL;
    }
	    return pIrp;
};

GIrp * CRRPMDigit::Co_WaitSof (GIrp * pIrp, void * bFirstEntry)
{
	if (bFirstEntry )
	{
//GSh ---   if (m_pIRRPMScan)
//GSh ---   {   m_pIRRPMScan  ->Run (IRRPMMotor::RunBackward, 15000);}
//GSh ---   if (m_pIRRPMDiafr)
//GSh ---   {   m_pIRRPMDiafr ->Run (IRRPMMotor::RunBackward, 15000);}
	}

    if (pIrp ->Cancelled () || (ERROR_SUCCESS != pIrp ->GetCurResult ()))
    {	return pIrp;}

    if (m_pIRRPMScan)
    {
	if (! m_MScanStatus.bFGConnected)
	{
	    printf ("...Read Image Fail (FG not connected)\n");
	    pIrp ->SetResultInfo (5001, NULL);
	    return pIrp;
	}
	if (bFirstEntry)
	{
	    GetCurRequest <CRRPMDigit::GetShotRequest> (pIrp) ->tTimeout.Activate (2000);
	}
	if (m_MScanStatus.bFGDataReady || m_MScanStatus.bFGDataFlow)
	{
	    GetCurRequest <CRRPMDigit::GetShotRequest> (pIrp) ->tTimeout.Disactivate ();
	    return pIrp;
	}
	if (! GetCurRequest <CRRPMDigit::GetShotRequest> (pIrp) ->tTimeout.Occured  ())
	{
	    SetCurCoProc  <CRRPMDigit, void *>
	   (pIrp, & CRRPMDigit::Co_WaitSof, this, (void *) 0);

	    QueueIrpForComplete (pIrp, 20);
	    return NULL;
	}
	    printf ("...Fail Read Image\n");
	    pIrp ->SetResultInfo (5003, NULL);
    }
	    return pIrp;
};

GIrp * CRRPMDigit::Co_SetHVOn (GIrp * pIrp, void *)
{
    if (pIrp ->Cancelled ())
    {	return pIrp;}

    if (m_pIRRPMScan)
    {
	if ((! m_MScanStatus.bConnected) || (! m_MScanStatus.bFGConnected))
	{
	    printf ("...Fail (Scanner or FG, not connected)\n"); 

	    pIrp ->SetResultInfo (5001, NULL);
	    return pIrp;
	}
	if (m_MScanStatus.bEndPin && (m_pIRRPUrp && m_UrpStatus.bHVOn))
	{
	    m_pIRRPUrp ->SetHV (IRRPUrp::HVRelease);
	}
	if (m_MScanStatus.bEndPin)
	{
	    printf ("...SetHV Done with success (1) !!!\n");

	    pIrp ->SetResultInfo (ERROR_SUCCESS, NULL);
	    return pIrp;
	}
	if (! m_MScanStatus.bEndPin && ! m_MScanStatus.bMove)
	{
	    printf ("...Fail (Scanner, not moved)\n");

	    pIrp ->SetResultInfo (5007, NULL);
	    return pIrp;
	}
    }
    if (m_pIRRPMDiafr)
    {
	if (! m_MDiafrStatus.bConnected)
	{
	    printf ("...Fail (Diafr, not connected)\n");

	    pIrp ->SetResultInfo (5001, NULL);
	    return pIrp;
	}
	if (m_MDiafrStatus.bEndPin && (m_pIRRPUrp && m_UrpStatus.bHVOn))
	{
	    m_pIRRPUrp ->SetHV (IRRPUrp::HVRelease);
	}
	if (! m_MDiafrStatus.bEndPin && ! m_MDiafrStatus.bMove)
	{
	    printf ("...Fail (Diafr not moved)\n");

	    pIrp ->SetResultInfo (5008, NULL);
	    return pIrp;
	}
    }
    if (m_pIRRPUrp)
    {
	if (! m_UrpStatus.bConnected)
	{
	    printf ("...Fail (Urp, not connected)\n");

	    pIrp ->SetResultInfo (5001, NULL);
	    return pIrp;
	}
	if (m_UrpStatus.bHVBlocked )
	{
	    printf ("...Fail (Urp blocked)\n");

	    pIrp ->SetResultInfo (5010, NULL);
	    return pIrp;
	}
	if ((NULL == m_pIRRPMScan) && (NULL == m_pIRRPMDiafr) && (! m_UrpStatus.bHVOn))
	{
	    printf ("...SetHV Done with success (3) !!!\n");

	    pIrp ->SetResultInfo (ERROR_SUCCESS, NULL);
	    return pIrp;
	}
    }
	if (GetCurRequest <CRRPMDigit::GetShotRequest> (pIrp) ->tTimeout.Occured ())
	{
	    printf ("...Fail (Wait Timeout)\n");

	    pIrp ->SetResultInfo (5012, NULL);
	    return pIrp;
	}

	    SetCurCoProc  <CRRPMDigit, void *>
	   (pIrp, & CRRPMDigit::Co_SetHVOn, this, NULL);

	    QueueIrpForComplete (pIrp, 20);
	    return NULL;
};

GIrp * CRRPMDigit::Co_SetHVWaitOn (GIrp * pIrp, void *)
{
    if (pIrp ->Cancelled ())
    {	return pIrp;}

    if ((m_pIRRPUrp    && (! m_UrpStatus   .bConnected))
    ||	(m_pIRRPMScan  && (! m_MScanStatus .bConnected))
    ||	(m_pIRRPMDiafr && (! m_MDiafrStatus.bConnected)))
    {
	    printf ("...Fail (Connection)\n");

	    pIrp ->SetResultInfo (5001, NULL);
	    return pIrp;
    }

    if (m_pIRRPUrp)
    {
	if (m_UrpStatus.bHVBlocked )
	{
	    printf ("...Fail (Urp blocked 2)\n");

	    pIrp ->SetResultInfo (5010, NULL);
	    return pIrp;
	}
//	if (! m_UrpStatus.bHVOn)		    // ?????????????? Look !!!!!!!!!!!!!!!
//	{
//	    printf ("...Fail (Urp no HV)\n");
//
//	    pIrp ->SetResultInfo (-1, NULL);
//	    return pIrp;
//	}
    }
    if ((m_pIRRPMScan  && ! m_MScanStatus .bMove)
    ||  (m_pIRRPMDiafr && ! m_MDiafrStatus.bMove))
    {
	    printf ("...Fail (Scan or Diafr Not started\n");

	    pIrp ->SetResultInfo (5015, NULL);
	    return pIrp;
    }
	    GetCurRequest <CRRPMDigit::GetShotRequest> (pIrp) ->tTimeout.Activate (8000);

	    SetCurCoProc  <CRRPMDigit, void *>
	   (pIrp, & CRRPMDigit::Co_WaitEof, this, NULL);

	    SetCurCoProc  <CRRPMDigit, void *>
	   (pIrp, & CRRPMDigit::Co_WaitSof, this, (void *) 1);

	    SetCurCoProc  <CRRPMDigit, void *>
	   (pIrp, & CRRPMDigit::Co_SetHVOn, this, NULL);

	    QueueIrpForComplete (pIrp, 20);
	    return NULL;
};

GIrp * CRRPMDigit::Co_SetHVWaitPrepare (GIrp * pIrp, void *)
{
    if (pIrp ->Cancelled ())
    {	return pIrp;}

    DWord   dReady;

    if (0x00000000 != (dReady = GetSetHVReady ()))
    {
	printf ("...Fail Wait Prepare %08X\n", dReady);

	pIrp ->SetResultInfo (5017, dReady);

	return pIrp;
    }

    if (m_pIRRPUrp)
    {   
	if (m_UrpStatus.bHVBlocked )
	{
#if FALSE
	    printf ("...Fail Urp condtion ...\n");
#else
	    printf ("...Fail Urp\n");
	    pIrp ->SetResultInfo (5010, NULL);
#endif
	    return pIrp;
	}
	if (GetCurRequest <CRRPMDigit::GetShotRequest> (pIrp) ->tTimeout.Occured ())
	{
	    printf ("...Wait Prepare Timeout\n");

	    pIrp ->SetResultInfo (5016, NULL);
	    return pIrp;
	}
	if (m_UrpStatus.bHVPrepared)
	{
	    m_pIRRPUrp ->SetHV (IRRPUrp::HVOn);

				    OnAckValue (m_RegGetStatusHV, 0x00030000);

	    SetCurCoProc  <CRRPMDigit, void *>
	   (pIrp, & CRRPMDigit::Co_SetHVWaitOn, this, NULL);

	    QueueIrpForComplete (pIrp, 4000);
	    return NULL;
	}
	    SetCurCoProc  <CRRPMDigit, void *>
	   (pIrp, & CRRPMDigit::Co_SetHVWaitPrepare, this, NULL);

	    QueueIrpForComplete (pIrp,  20);
	    return NULL;
    }
	    if (m_pIRRPMScan )
	    {   m_pIRRPMScan   ->Run (IRRPMMotor::RunForward, 15000);}
	    if (m_pIRRPMDiafr)
	    {   m_pIRRPMDiafr  ->Run (IRRPMMotor::RunForward, 15000);}

	    SetCurCoProc <CRRPMDigit, void *>
	   (pIrp, & CRRPMDigit::Co_SetHVWaitOn, this, NULL);

	    QueueIrpForComplete (pIrp, 200);
	    return NULL;
};

GIrp * CRRPMDigit::Co_SetHVRelease (GIrp * pIrp, void *)
{
    if (m_pIRRPUrp)
    {   m_pIRRPUrp ->SetHV (IRRPUrp::HVRelease);};

	return pIrp;
};

GIrp * CRRPMDigit::Co_SetHVReady (GIrp * pIrp, void *)
{
    if (pIrp ->Cancelled ())
    {	return pIrp;}

    DWord   dReady;

    if (0x00000000 != (dReady = GetSetHVReady ()))
    {
	printf ("...Fail Ready %08X\n", dReady);

	pIrp ->SetResultInfo (5017, dReady);

	return pIrp;
    }

    GetCurRequest <CRRPMDigit::GetShotRequest> (pIrp) ->tTimeout.Activate (8000);

    if (m_pIRRPUrp)
    {   m_pIRRPUrp ->SetHV (IRRPUrp::HVPrepare);}

				    OnAckValue (m_RegGetStatusHV, 0x00020000);

    SetCurCoProc  <CRRPMDigit, void *>
   (pIrp, & CRRPMDigit::Co_SetHVRelease, this, NULL);

    SetCurCoProc  <CRRPMDigit, void *>
   (pIrp, & CRRPMDigit::Co_SetHVWaitPrepare, this, NULL);

    QueueIrpForComplete (pIrp, 20);
    return NULL;
};

GIrp * CRRPMDigit::Co_SetHVWaitReady (GIrp * pIrp, void *)
{
    if (pIrp ->Cancelled ())
    {	return pIrp;}

    DWord  dReady;

    if (0x00000000 != (dReady = GetSetHVReady ()))
    {
	printf ("...WaitSetHVReady %08X\n", dReady);

	SetCurCoProc <CRRPMDigit, void *>
       (pIrp, & CRRPMDigit::Co_SetHVWaitReady, this, NULL);

	QueueIrpForComplete (pIrp, 20);

	return NULL;
    }
    if (m_pIRRPRepairer)
    {{
	IRRPRepairer::Phase
	iPhase = m_pIRRPRepairer ->PeekImageStatus (NULL, NULL);

    if (IRRPRepairer::Capturing > iPhase)
    {
	return pIrp;
    }
    else
    if (IRRPRepairer::Capturing < iPhase)
    {
	printf ("...WaitReparing ready\n");

	SetCurCoProc <CRRPMDigit, void *>
       (pIrp, & CRRPMDigit::Co_SetHVWaitReady, this, NULL);

	QueueIrpForComplete (pIrp, 20);

	return NULL;
    }
//  else
//  {}

    }}

	SetCurCoProc <CRRPMDigit, void *>
       (pIrp, & CRRPMDigit::Co_SetHVReady, this, NULL);

	QueueIrpForComplete (pIrp, 500);
	return NULL;
};
//
//[]------------------------------------------------------------------------[]





















#include "r_rrpini.cpp"

//[]------------------------------------------------------------------------[]
//
class CRRPDeviceService : public GApartment
{
friend	IRRPSettings *	    GetIRRPSettings ();

friend	CRRPDevice *	    GetCRRPMScan    ();

public :
			    CRRPDeviceService ()
			    {
				m_uEquMask	= 0x00;
				m_pEquCallBack	= NULL;
			    };

friend	void		    MM2IndEnable      (Bool bEnable) { if (s_pRRPService) { s_pRRPService ->m_SerialBus.MM2IndEnable (bEnable); } };

friend	void		    MM2IndSetValue    (DWord dValue) { if (s_pRRPService) { s_pRRPService ->m_SerialBus.MM2IndSetValue (dValue);} };

static	GResult GAPI	    CreateInstance  (int, void **, HANDLE);

	void		    Work	    ();

protected :

	GResult		    InitInstance    (int, void **, HANDLE hResume);

	void		    SetEquMask	    (_U32 c, _U32 s);

#pragma pack (_M_PACK_VPTR)

	struct NotificationEntry
	{
	    typedef TSLink <NotificationEntry, 0>   Link;
	    typedef TXList <NotificationEntry,
		    TSLink <NotificationEntry, 0> > List;

	    void *	    pContext ;
	};

#pragma pack ()

protected :
#ifdef _MSC_BUG_C2248
public :
#endif
public :

	HRESULT	    XUnknown_QueryInterface		(REFIID, void **);

	void	    XUnknown_FreeInterface		(void *);

	void	    DestroyRRPDevice			(CRRPDevice *);

static	void GAPI   DestroyRRPDevice			(int	   nArgs	 ,
							 void **   pArgs	 ,
							 HANDLE	   hResume	 );

	GResult	    CreateRRPDevice			(void **   pInterface	 ,
							 void *	   DevInstanceId );

static	void GAPI   CreateRRPDevice			(int	   nArgs	 ,
							 void **   pArgs	 ,
							 HANDLE	   hResume	 );

	GResult	    IRRPDeviceService_CreateRRPDevice	(void **   pInterface    ,
							 REFCLSID  DevClassId    ,
							 void *    DevInstanceId ,
							 REFIID    InterfaceId   );

	void	    IRRPDeviceService_SetEquCallBack	(void (GAPI * pEquCallBack)(_U32,_U32))
		    {
			m_pEquCallBack = pEquCallBack;
		    };

	GResult	    IRRPDeviceService_CreateRRPSettings	(IRRPSettings *&);

	void	    OnSettingsChanged			(IRRPSettings::ClassId);

static	void GAPI   OnSettingsChanged			(int	   nArgs    ,
							 void **   pArgs    ,
							 HANDLE	   hResume  );

friend	void	    OnSettingsChanged			(IRRPSettings::ClassId);

protected :

//	friend
//	class TXUnknown_Entry <CRRPDeviceService>;
//	      TXUnknown_Entry <CRRPDeviceService>
//				m_XUnknown	    ;

	friend
	class IRRPDeviceService_Entry;
	friend
	class TInterface_Entry <IRRPDeviceService, CRRPDeviceService>;
	class IRRPDeviceService_Entry : public 
	      TInterface_Entry <IRRPDeviceService, CRRPDeviceService>
	{
	STDMETHOD_ (GResult,    CreateRRPDevice)(void **    pInterface    ,
						 REFCLSID   DevClassId    ,
						 void *	    DevInstanceId ,
						 REFIID	    InterfaceId   )
	{
	    return GetT () ->IRRPDeviceService_CreateRRPDevice
							   (pInterface	  ,
							    DevClassId	  ,
							    DevInstanceId ,
							    InterfaceId	  );
	};
	STDMETHOD_ (void,	 SetEquCallBack)(void (GAPI * pEquCallBack)(_U32, _U32))
	{
	    GetT () ->IRRPDeviceService_SetEquCallBack (pEquCallBack);
	};
	STDMETHOD_ (GResult,  CreateRRPSettings)(IRRPSettings *& pInterface)
	{
	    return GetT () ->IRRPDeviceService_CreateRRPSettings(pInterface);
	};
	}		    m_IRRPDevService ;

	_U32		    m_uEquMask	;
	void (GAPI *	    m_pEquCallBack)(_U32, _U32);

	CRRPSerialBus	    m_SerialBus	;

	CRRPKKM		    m_KKM	;
	CRRPUrp		    m_Urp	;
	CRRPMScan	    m_MScan	;
	CRRPMDiafr	    m_MDiafr	;
	CRRPMM2		    m_MM2	;

	CRRPIniFile	    m_IniFile	;
};

HRESULT CRRPDeviceService::XUnknown_QueryInterface (REFIID iid, void ** pi)
{
    if (iid == IID_NULL)
    {
    if (0 < m_IRRPDevService.GetRefCount ())
    {
	return m_IRRPDevService.QueryInterface (iid, pi);
    }
	* pi = m_IRRPDevService.Init (this, IID_NULL, & m_XUnknown);

	return ERROR_SUCCESS;
    }
	return GApartment::XUnknown_QueryInterface (iid, pi);

//	return ERROR_NOT_SUPPORTED;
};

void CRRPDeviceService::XUnknown_FreeInterface (void * pi)
{
    if (pi == & m_IRRPDevService)
    {
printf ("\n\n\t...Free CRRPDeviceService::IRRPDeviceService\n\n");
    }
    else
    if (pi == & m_XUnknown)
    {
printf ("\n\n\t...Free CRRPDeviceService::XUnknown\n\n");
    }
};

void CRRPDeviceService::SetEquMask (_U32 c, _U32 s)
{
    _U32    uEquMask = m_uEquMask;
		       m_uEquMask = (m_uEquMask & (~c)) | s;

    if (m_pEquCallBack && (uEquMask ^ m_uEquMask))
    {
       (m_pEquCallBack)(m_uEquMask, uEquMask ^ m_uEquMask);
    }
};

void CRRPDeviceService::DestroyRRPDevice (CRRPDevice * pDevice)
{
    DWord   dEquMask = 0;

    LockAcquire ();

    if (pDevice == & m_KKM)
    {
	dEquMask = IRRPMDIG::EquMask_KKM;
    }
    else
    if (pDevice == & m_Urp  )
    {
	dEquMask = IRRPMDIG::EquMask_Urp;

	m_Urp.SaveConfigInfo ();

	m_Urp.m_wSecondTimer ->CancelWaitFor ();

	if (m_Urp.m_pIRRPKKM)
	{
	    m_Urp.m_pIRRPKKM ->Release ();
	    m_Urp.m_pIRRPKKM = NULL;
	}
    }
    else
    if (pDevice == & m_MDiafr)
    {
	dEquMask = IRRPMDIG::EquMask_MDiafr;
    }
    else
    if (pDevice == & m_MScan)
    {
	dEquMask = IRRPMDIG::EquMask_MScan | IRRPMDIG::EquMask_MFG;

		   m_MScan.m_XPGrabber.Close ();
    }
    else
    if (pDevice == & m_MM2)
    {
	dEquMask = IRRPMDIG::EquMask_MM2;

	CRRPConfigFile::SaveMM2Param (m_MM2.m_dValSetPressHigh);
    }
	m_SerialBus.Disconnect (* pDevice);

printf ("Destroy CRRPDevice\n");

    LockRelease ();

	SetEquMask (dEquMask, 0);
};

GResult CRRPDeviceService::CreateRRPDevice (void ** pInterface	  ,
					    void *  DevInstanceId )
{
	* pInterface = NULL;

    if (DevInstanceId == (void *) KKM_DEVICE_ID)
    {
	if (0 == m_KKM.m_XUnknown.GetRefCount ())
	{   m_KKM.m_XUnknown.Init (& m_KKM);}
	else
	{   m_KKM.m_XUnknown.AddRef ();}

	if (0 == m_KKM.m_IRRPDevice.GetRefCount ())
	{
	    m_KKM.m_IRRPDevice.Init (& m_KKM, IID_NULL, & m_KKM.m_XUnknown);

	    m_KKM.Initialize	();

	LockAcquire ();

	    m_SerialBus.Connect (m_KKM);

	LockRelease ();

	    SetEquMask (0, IRRPMDIG::EquMask_KKM);
	}
	else
	{   m_KKM.m_IRRPDevice.AddRef ();}

	    m_KKM.m_XUnknown.Release  ();

	  * pInterface = & m_KKM.m_IRRPDevice;

	    return ERROR_SUCCESS;
    }
    else
    if (DevInstanceId == (void *) URP_DEVICE_ID)
    {
	if (0 == m_Urp.m_XUnknown.GetRefCount ())
	{   m_Urp.m_XUnknown.Init (& m_Urp);}
	else
	{   m_Urp.m_XUnknown.AddRef ();}

	if (0 == m_Urp.m_IRRPDevice.GetRefCount ())
	{
	    m_Urp.m_IRRPDevice.Init (& m_Urp, IID_NULL, & m_Urp.m_XUnknown);

	    m_Urp.Initialize ();

	LockAcquire ();

	    m_SerialBus.Connect (m_Urp);

	LockRelease ();

	    m_Urp.m_wSecondTimer ->QueueWaitFor (NULL, CRRPUrp::On_SecondTimer, & m_Urp, NULL, 0, 1000);

	    SetEquMask (0, IRRPMDIG::EquMask_Urp);
	}
	else
	{   m_Urp.m_IRRPDevice.AddRef ();}

	    m_Urp.m_XUnknown.Release  ();

	  * pInterface = & m_Urp.m_IRRPDevice;

	    return ERROR_SUCCESS;
    }
    else
    if (DevInstanceId == (void *) MSCAN_DEVICE_ID)
    {
	if (0 == m_MScan.m_XUnknown.GetRefCount ())
	{   m_MScan.m_XUnknown.Init (& m_MScan);}
	else
	{   m_MScan.m_XUnknown.AddRef ();}

	if (0 == m_MScan.m_IRRPDevice.GetRefCount ())
	{
	    m_MScan.m_IRRPDevice.Init (& m_MScan, IID_NULL, & m_MScan.m_XUnknown);

	    m_MScan.Initialize ();

	    m_MScan.m_XPGrabber.Create ();

	LockAcquire ();

	    m_SerialBus.Connect (m_MScan);

	LockRelease ();

	    SetEquMask (0, IRRPMDIG::EquMask_MScan | IRRPMDIG::EquMask_MFG);
	}
	else
	{   m_MScan.m_IRRPDevice.AddRef ();}

	    m_MScan.m_XUnknown.Release ();

	  * pInterface = & m_MScan.m_IRRPDevice;

	    return ERROR_SUCCESS;
    }
    else
    if (DevInstanceId == (void *) MDIAFR_DEVICE_ID)
    {
	if (0 == m_MDiafr.m_XUnknown.GetRefCount ())
	{   m_MDiafr.m_XUnknown.Init (& m_MDiafr);}
	else
	{   m_MDiafr.m_XUnknown.AddRef ();}

	if (0 == m_MDiafr.m_IRRPDevice.GetRefCount ())
	{
	    m_MDiafr.m_IRRPDevice.Init (& m_MDiafr, IID_NULL, & m_MDiafr.m_XUnknown);

	    m_MDiafr.Initialize ();

	LockAcquire ();

	    m_SerialBus.Connect (m_MDiafr);

	LockRelease ();

	    SetEquMask (0, IRRPMDIG::EquMask_MDiafr);
	}
	else
	{   m_MDiafr.m_IRRPDevice.AddRef ();}

	    m_MDiafr.m_XUnknown.Release ();

	  * pInterface = & m_MDiafr.m_IRRPDevice;

	    return ERROR_SUCCESS;
    }
    else
    if (DevInstanceId == (void *) MM2_DEVICE_ID)
    {
	if (0 == m_MM2.m_XUnknown.GetRefCount ())
	{   m_MM2.m_XUnknown.Init (& m_MM2);}
	else
	{   m_MM2.m_XUnknown.AddRef ();}

	if (0 == m_MM2.m_IRRPDevice.GetRefCount ())
	{
	    m_MM2.m_IRRPDevice.Init (& m_MM2, IID_NULL, & m_MM2.m_XUnknown);

	    m_MM2.Initialize ();

	LockAcquire ();

	    m_SerialBus.Connect (m_MM2);

	LockRelease ();

	    SetEquMask (0, IRRPMDIG::EquMask_MM2);
	}
	else
	{   m_MM2.m_IRRPDevice.AddRef ();}

	    m_MM2.m_XUnknown.Release ();

	  * pInterface = & m_MM2.m_IRRPDevice;

	    return ERROR_SUCCESS;
    }
    else
    if (DevInstanceId == (void *) MDIG_DEVICE_ID)
    {{
	return CRRPMDigit::CreateInstance (pInterface,  & m_IRRPDevService);
    }}
	return ERROR_NOT_SUPPORTED ;
};

void GAPI CRRPDeviceService::CreateRRPDevice (int nArgs, void ** pArgs, HANDLE hResume)
{
    * ((GResult *) pArgs [0]) = ((CRRPDeviceService *) pArgs [1]) ->CreateRRPDevice 
				((void **)	       pArgs [2],
				 (void  *)	       pArgs [3]);
    ::SetEvent (hResume);
};

GResult	CRRPDeviceService::IRRPDeviceService_CreateRRPDevice
				        (void **   pInterface    ,
					 REFCLSID  DevClassId    ,
					 void *    DevInstanceId ,
					 REFIID    InterfaceId   )
{
    GResult dResult   = ERROR_NOT_SUPPORTED;

    void *  pArgs [4] = {& dResult, this, pInterface, DevInstanceId};

    ::QueueUserAPC (m_pCoThread ->GetHandle (), CreateRRPDevice, 4, pArgs, TCurrent () ->GetResume ());

    return  dResult;
};

GResult	CRRPDeviceService::InitInstance (int, void **, HANDLE hResume)
{
    return ERROR_SUCCESS;
};

GResult CRRPDeviceService::CreateInstance (int nArgs, void ** pArgs, HANDLE hResume)
{
printf ("\n\n\t...Run CRRPDeviceService %d\n\n", sizeof (CRRPDeviceService));

    CRRPDeviceService	 s ;

    s_pRRPService =    & s ;

    s.m_XUnknown.Init (& s);

    s.m_XUnknown.QueryInterface (IID_NULL, (void **) pArgs [0]);

    s.m_IApartment.Init (& s, IID_IApartment, & s.m_XUnknown);

    s.m_XUnknown.Release ();

    if (hResume)
    {
	::SetEvent (hResume);
    }

	RunApartment (& s, 0, NULL, NULL);

printf ("\n\n\t...End CRRPDeviceService\n\n");

	return ERROR_SUCCESS;
};

void CRRPDeviceService::Work ()
{
    m_SerialBus.On_Idle ();
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
IRRPDEVS_EXPORT GResult GAPI CreateRRPDeviceService (IRRPDeviceService *&
							 pIRRPDevService ,
						     LPCTSTR		 )
{
    void * pInterface = NULL;
    void * pArgs [1]  = {& pInterface};

    if (ERROR_SUCCESS == 

	CreateThread  <TPrivateWakeUpThread <GThread, 32> > 
		      (NULL, CRRPDeviceService::CreateInstance, 1, pArgs))
    {
	pIRRPDevService = (IRRPDeviceService *) pInterface;

	return ERROR_SUCCESS;
    }
	return ERROR_NO_SYSTEM_RESOURCES;
};
//
//[]------------------------------------------------------------------------[]


void GAPI CRRPDeviceService::DestroyRRPDevice (int nArgs, void ** pArgs, HANDLE hResume)
{
    ((CRRPDeviceService *) pArgs [1]) ->DestroyRRPDevice 
    ((CRRPDevice	*) pArgs [2]);

    ::SetEvent (hResume);
};

void CRRPDevice::XUnknown_FreeInterface (void * pi)
{
    if (pi == & m_XUnknown)
    {
    {{
	void * pArgs [3] = {NULL, s_pRRPService, this};

	::QueueUserAPC (s_pRRPService ->GetCoThread () ->GetHandle (), 
			CRRPDeviceService::DestroyRRPDevice, 3, pArgs, 
			TCurrent () ->GetResume ());

	printf ("Break from FreeInterface\n");
    }}
    }
};

void CRRPMScan::XUnknown_FreeInterface (void * pi)
{
    if (pi == & m_XUnknown)
    {
	CRRPStepMotor::XUnknown_FreeInterface (pi);
    }
};
//
//[]------------------------------------------------------------------------[]
//
GResult CRRPMDigit::IRRPMDIG_SetHV ()
{
    if (m_pShotIrp)
    {
	return ERROR_ALREADY_EXISTS;
    }

    m_pShotIrp	    = new GIrp ();

    SetCurRequest    (m_pShotIrp,
    CreateRequest 
    <GetShotRequest> (m_pShotIrp) ->Init ());

    SetCurCoProc <CRRPMDigit, void *>
   (m_pShotIrp, & CRRPMDigit::Co_SetHVDone     , this, NULL);

    SetCurCoProc <CRRPMDigit, void *>
   (m_pShotIrp, & CRRPMDigit::Co_RepairImage   , this, NULL);

    SetCurCoProc <CRRPMDigit, void *>
   (m_pShotIrp, & CRRPMDigit::Co_SetHVWaitReady, this, NULL);

    if (m_pIRRPMM2)
    {
	RRPSetParam (m_pIRRPMM2, IRRPMM2::ID_SetPressMode ,(m_ParSetPressMode .uValue == 0x00) ? 0x00 : 0x01);
    }
    if (m_pIRRPUrp)
    {
	RRPSetParam (m_pIRRPUrp, IRRPUrp::ID_ParSetFieldSize, m_ParSetFieldSize .uValue);
	RRPSetParam (m_pIRRPUrp, IRRPUrp::ID_Focus	    , m_ParSetFocusSize .uValue);
	RRPSetParam (m_pIRRPUrp, IRRPUrp::ID_ParSetUAnode10 , m_ParSetUAnode10  .uValue);
	RRPSetParam (m_pIRRPUrp, IRRPUrp::ID_IFilamentPre   ,	-1);
	RRPSetParam (m_pIRRPUrp, IRRPUrp::ID_ParSetIAnode   , m_ParSetIAnode    .uValue);
	RRPSetParam (m_pIRRPUrp, IRRPUrp::ID_MAS	    , 1500);
	RRPSetParam (m_pIRRPUrp, IRRPUrp::ID_ExposeTime	    , 8000);
    }
    if (m_pIRRPMScan)
    {
	RRPSetParam (m_pIRRPMScan, IRRPMScan::ID_RegGetFGStatus, 0x00);
    }

    if (m_pIRRPRepairer)
    {{
	m_bGrab	    	= True;
	m_dGrabOffset	=    0;

	DWord dScan_12_Param,
	      dScan_23_Param,
	      dImageProcMask;

	TCHAR pPath [MAX_PATH];

printf ("\n=========================== m_ParGetRotPos %d\n\n", m_ParGetRotPos.uValue);

	GetNVCalibrateUAPath  (pPath, m_ParSetFocusSize.uValue);

	GetIRRPSettings () ->GetValue (IRRPSettings::ID_MSC_Scan_12_Param, & dScan_12_Param, sizeof (DWord), NULL);
	GetIRRPSettings () ->GetValue (IRRPSettings::ID_MSC_Scan_23_Param, & dScan_23_Param, sizeof (DWord), NULL);
	GetIRRPSettings () ->GetValue (IRRPSettings::ID_MSC_ImageProcMask, & dImageProcMask, sizeof (DWord), NULL);

	m_pIRRPRepairer ->StartRepairing (m_ParSetUAnode   .uValue,
					  m_ParSetIAnode   .uValue,
					  m_nActiveBeg	,
					  m_nActiveEnd	,
					  m_nDCBeg	,
					  m_nDCEnd	,
					 (m_ParSetFieldSize.uValue & 0x03) | (dImageProcMask << 8),
					 (((DWord) LOBYTE(HIWORD(dScan_12_Param))) << 24)
				       | (((DWord) LOBYTE(LOWORD(dScan_12_Param))) << 16)
				       | (((DWord) LOBYTE(HIWORD(dScan_23_Param))) <<  8)
				       | (((DWord) LOBYTE(LOWORD(dScan_23_Param))) <<  0),
					  pPath			  );
    }}

				    OnAckValue (m_RegGetStatusHV, 0x00010000);

	s_pRRPService ->IApartment_QueueIrpForComplete (m_pShotIrp, 0);

	return ERROR_SUCCESS;
};

void CRRPMDigit::Xxxx_SetEquMask (IRRPMDIG::EquMask uEquMask)
{
    void * pArgs [2] = {this, (void *) uEquMask};

    ::QueueUserAPC (s_pRRPService ->GetCoThread () ->GetHandle (),
		    CRRPMDigit::SetEquMask, 2, pArgs,
		    TCurrent () ->GetResume	());
};

_U32 CRRPMDigit::IRRPMDIG_GetParam (_U32 _uParamId)
{
    IRRPRepairer::ImageInfo iInfo;

    switch (_uParamId)
    {
    case IRRPMDIG::ID_ParImageWidth  :
	if (m_pIRRPRepairer && (ERROR_SUCCESS == m_pIRRPRepairer ->GetImageInfo (IRRPRepairer::II_ImageSizeX, & iInfo)))
	{
	    return iInfo.u32Value;
	}
	    break;

	    return IMAGE_LINE_WIDTH;

    case IRRPMDIG::ID_ParImageHeight :
	if (m_pIRRPRepairer && (ERROR_SUCCESS == m_pIRRPRepairer ->GetImageInfo (IRRPRepairer::II_ImageSizeY, & iInfo)))
	{
	    return iInfo.u32Value;
	}
	    break;

    case IRRPMDIG::ID_ParImageExposeTimeEff :

	    return ((int) m_uDiafrWidth * m_uScanTime) / 330;	// 330 mm - Scan length.
    };

	return 0xFFFFFFFF;
};

GResult CRRPMDigit::IRRPMDIG_PeekImageStatus (int * pPhase, int * pStep, int * pStepNum)
{
    if (m_pIRRPRepairer)
    {
	return (* pPhase = m_pIRRPRepairer ->PeekImageStatus (pStep, pStepNum), ERROR_SUCCESS);
    }
	return (GResult) -1;
};

int CRRPMDigit::IRRPMDIG_GrabImage (void * pDstBuf, DWord dDstPitch, int nFromRow, int nNumRows)
{
    if (m_pIRRPRepairer)
    {
	return m_pIRRPRepairer ->GrabImage (pDstBuf, dDstPitch, nFromRow, nNumRows);
    }
	return (GResult) -1;

/*
    if (nFromRow	    > IMAGE_HEIGHT)
    {	return 0;}

    if (nFromRow + nNumRows > IMAGE_HEIGHT)
    {
	nNumRows = IMAGE_HEIGHT - nFromRow;
    }

    Word * pSrcBuf = ((Word *) GetFGImageBuf ()) + (nFromRow * IMAGE_WIDTH);
    Word * pDstBuf =  (Word *) pBuf;

    for (_U32 i = 0; i < nNumRows; i ++)
    {
    for (int  j = 0; j < RULE_WIDTH; j ++)
    {
	pDstBuf [j + (RULE_WIDTH * 1)] = * pSrcBuf ++;
	pDstBuf [j + (RULE_WIDTH * 0)] = * pSrcBuf ++;
	pDstBuf [(RULE_WIDTH * 3) - j] = * pSrcBuf ++;
    }
	pDstBuf += IMAGE_WIDTH;
    }
*/
    return nNumRows;
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
static GResult GAPI createIRRPKKM (IRRPKKM ** pIRRPKKM)
{
    return 
    s_pRRPService ->IRRPDeviceService_CreateRRPDevice ((void **) pIRRPKKM     ,
						       IID_IUnknown	      ,
						       (void  *) KKM_DEVICE_ID,
						       IID_IUnknown	      );
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
void CRRPDeviceService::OnSettingsChanged (IRRPSettings::ClassId uClassId)
{
    if ((IRRPSettings::CLSID_URP == uClassId)
    && (0 < m_Urp.m_IRRPDevice.GetRefCount ()))
    {
	    m_Urp   .LoadIniParam ();
    }
    else
    if ((IRRPSettings::CLSID_MSC == uClassId))
    {
    if (0 < m_MDiafr.m_IRRPDevice.GetRefCount ())
    {
	    m_MDiafr.LoadIniParam ();
    }
    if (0 < m_MScan .m_IRRPDevice.GetRefCount ())
    {
	    m_MScan .Reset ();

	    m_MScan .LoadIniParam ();
    }
    }
};

void GAPI CRRPDeviceService::OnSettingsChanged (int nArgs, void ** pArgs, HANDLE hResume)
{
    ((CRRPDeviceService   *)	  pArgs [1]) ->OnSettingsChanged
    ((IRRPSettings::ClassId)(int) pArgs [2]);

    ::SetEvent (hResume);
};

void OnSettingsChanged (IRRPSettings::ClassId uClassId)
{
    void *  pArgs [3] = {NULL, s_pRRPService, (void *) uClassId};

    ::QueueUserAPC (s_pRRPService ->m_pCoThread ->GetHandle (),
		    CRRPDeviceService::OnSettingsChanged, 3, pArgs, TCurrent () ->GetResume ());
};

GResult CRRPDeviceService::IRRPDeviceService_CreateRRPSettings
			  (IRRPSettings *& pISettings)
{
#if FALSE

    return CRRPIniFile::CreateInstance (pISettings);
#else

    pISettings = & m_IniFile;

    return ERROR_SUCCESS;

#endif
};

static IRRPSettings * GetIRRPSettings ()
{
    return & s_pRRPService ->m_IniFile;
};

static CRRPDevice * GetCRRPMScan ()
{
    return (s_pRRPService) ? & s_pRRPService ->m_MScan : NULL;
};
//
//[]------------------------------------------------------------------------[]


























/*
Bool LoadMotorParam (DWord dDeviceId, void * pBuf)
{
    DWORD  dFileBufSize		;
    BYTE   pFileBuf  [4096]	;
    TCHAR  pFileName [MAX_PATH]	;

    if (! BuildFileName (pFileName, _T("MDIGConsole.cfg")))
    {	return False;}

    HANDLE hFile = ::CreateFile (pFileName,
				 GENERIC_READ,
				 0,
				 NULL,
				 OPEN_EXISTING,
				 FILE_ATTRIBUTE_NORMAL,
				 NULL);

    if (INVALID_HANDLE_VALUE == hFile)
    {
	return False;
    }    

	Bool   bResult = False;

    if (::ReadFile (hFile, pFileBuf, ::GetFileSize (hFile, NULL), & dFileBufSize, NULL))
    {
	for (Byte * p = pFileBuf; p < pFileBuf + dFileBufSize; p += * ((DWord *) p))
	{
	    if (* (((DWord *) p) + 1) == dDeviceId)
	    {
		memcpy (pBuf, p, * ((DWord *) p));

		bResult = True;

		break; 
	    }
	}
    }

    ::CloseHandle (hFile);

	return bResult;
};

Bool SaveMotorParam (void * pBuf)
{
    DWord  dTemp		;
    DWord  dFileBufSize		;
    BYTE   pFileBuf  [4096]	;
    TCHAR  pFileName [MAX_PATH] ;

    if (! BuildFileName (pFileName, _T("MDIGConsole.cfg")))
    {	return False;};

    HANDLE hFile = ::CreateFile (pFileName,
				 GENERIC_READ | GENERIC_WRITE,
				 0,
				 NULL,
				 OPEN_ALWAYS,
				 FILE_ATTRIBUTE_NORMAL,
				 NULL);

    if (INVALID_HANDLE_VALUE == hFile)
    {
	return False;
    }

	Bool   bResult = False;

    if (::ReadFile (hFile, pFileBuf, ::GetFileSize (hFile, NULL), & dFileBufSize, NULL))
    {
	for (Byte * p = pFileBuf; p < pFileBuf + dFileBufSize; p += * ((DWord *) p))
	{
	    if (* (((DWord *) p) + 1) == * (((DWord *) pBuf) + 1))
	    {
		dTemp = * ((DWord *) p);

		memmove (p, p + dTemp, (pFileBuf + dFileBufSize) - (p + dTemp));

		dFileBufSize -= dTemp;

		break;
	    }
	}
		memcpy  (pFileBuf + dFileBufSize, pBuf, dTemp = * ((DWord *) pBuf));

		dFileBufSize += dTemp;

	::SetFilePointer (hFile, 0, 0, FILE_BEGIN);

	bResult = 

	::WriteFile	 (hFile, pFileBuf, dFileBufSize, & dTemp, NULL);
	::SetEndOfFile   (hFile);
    }

    ::CloseHandle (hFile);

	return bResult;
};
*/



































#if FALSE

float CRRPUrp::UAnodeTable::getUAnodeR (float fUAnodeX)
{
    if (0 < nEntries)
    {{
	Entry * le, * he;

	int  i; float lU, hU;

	for (i = 0; (i < nEntries) && (pEntries [i].nUAnodeX < fUAnodeX); i ++)
	{}

	if (i == nEntries)
	{   le =  he = & pEntries [i - 1];}
	else
	if (i ==        0)
	{   le =  he = & pEntries [0];}
	else
	{   le = (he = & pEntries [i]) - ((pEntries [i].nUAnodeX == round_I32 (fUAnodeX)) ? 0 : 1);}

	lU = le ->fUAnodeR;
	hU = he ->fUAnodeR;

	if (le ->nUAnodeX != he ->nUAnodeX)
	{
	    return lU + ((((fUAnodeX   - le ->nUAnodeX) * 64 *
			(hU	       - lU	      ))
		      / (he ->nUAnodeX - le ->nUAnodeX))/ 64);
	}
	if (fUAnodeX < le ->nUAnodeX)
	{
	    return (lU / le ->nUAnodeX) * fUAnodeX;
	}
	if (he ->nUAnodeX < fUAnodeX)
	{
	    return (hU / he ->nUAnodeX) * fUAnodeX;
	}
//	if ((le ->nUAnodeX <= fUAnodeX) && (fUAnodeX <= he ->nUAnodeX))
	{
	    return (lU + hU) / 2;
	}
    }}

    return (float) fUAnodeX;
};

float CRRPUrp::UAnodeTable::GetUAnodeR (int nUAnodeX)
{
    if (0 < nEntries)
    {{
	Entry * le, * he;

	int  i; float lU, hU;

	for (i = 0; (i < nEntries) && (pEntries [i].nUAnodeX < nUAnodeX); i ++)
	{}

	if (i == nEntries)
	{   le =  he = & pEntries [i - 1];}
	else
	if (i ==        0)
	{   le =  he = & pEntries [0];}
	else
	{   le = (he = & pEntries [i]) - ((pEntries [i].nUAnodeX == nUAnodeX) ? 0 : 1);}

	lU = le ->fUAnodeR;
	hU = he ->fUAnodeR;

	if (le ->nUAnodeX != he ->nUAnodeX)
	{
	    return lU + ((((nUAnodeX   - le ->nUAnodeX) * 64 *
			(hU		- lU	      ))
		      / (he ->nUAnodeX - le ->nUAnodeX))/ 64);
	}
	if (nUAnodeX < le ->nUAnodeX)
	{
	    return (lU / le ->nUAnodeX) * nUAnodeX;
	}
	if (he ->nUAnodeX < nUAnodeX)
	{
	    return (hU / he ->nUAnodeX) * nUAnodeX;
	}
//	if ((le ->nUAnodeX <= nUAnodeX) && (nUAnodeX <= he ->nUAnodeX))
	{
	    return (lU + hU) / 2;
	}
    }}

    return (float) nUAnodeX;
};

float CRRPUrp::UAnodeTable::getUAnodeX (int fUAnodeR)
{
    if (0 < nEntries)
    {{
	Entry * le, * he;

	int  i; int lU, hU;

	for (i = 0; (i < nEntries) && (pEntries [i].fUAnodeR < fUAnodeR); i ++)
	{}

	if (i == nEntries)
	{   le =  he = & pEntries [i - 1];}
	else
	if (i ==	0)
	{   le =  he = & pEntries [0];}
	else
	{   le = (he = & pEntries [i]) - ((pEntries [i].fUAnodeR == fUAnodeR) ? 0 : 1);}

	lU = le ->nUAnodeX;
	hU = he ->nUAnodeX;

	if (le ->fUAnodeR != he ->fUAnodeR)
	{
	    return lU + ((((fUAnodeR   - le ->fUAnodeR) * 64 *
		        (hU	       - lU	      ))
		      / (he ->fUAnodeR - le ->fUAnodeR))/ 64);
	}
	if (fUAnodeR < le ->fUAnodeR)
	{
	    return ((((float) lU) / le ->fUAnodeR) * fUAnodeR);
	}
	if (he ->fUAnodeR < fUAnodeR)
	{
	    return ((((float) hU) / he ->fUAnodeR) * fUAnodeR);
	}
	if ((le ->fUAnodeR <= fUAnodeR) && (fUAnodeR <= he ->fUAnodeR))
	{
	    return (float)((lU * 16) + (hU * 16))/16/2;
	}
    }}
    return (float) fUAnodeR;
};

int CRRPUrp::UAnodeTable::GetUAnodeX (int fUAnodeR)
{
    float  fUAnodeX = getUAnodeX (fUAnodeR);

    printf ("\tUser %d ->Set %2.2f ->Show %2.2f [%d]\n", fUAnodeR, fUAnodeX, getUAnodeR (fUAnodeX), round_I32 (GetUAnodeR (round_I32 (fUAnodeX))));

    return round_I32 (fUAnodeX);
/*
    if (0 < nEntries)
    {{
	Entry * le, * he;

	int  i; int   lU, hU;

	for (i = 0; (i < nEntries) && (pEntries [i].fUAnodeR < fUAnodeR); i ++)
	{}

	if (i == nEntries)
	{   le =  he = & pEntries [i - 1];}
	else
	if (i ==	0)
	{   le =  he = & pEntries [0];}
	else
	{   le = (he = & pEntries [i]) - ((pEntries [i].fUAnodeR == fUAnodeR) ? 0 : 1);}

	lU = le ->nUAnodeX;
	hU = he ->nUAnodeX;

	if (le ->fUAnodeR != he ->fUAnodeR)
	{
	    return round_I32 (lU + ((((fUAnodeR   - le ->fUAnodeR) * 64 *
				   (hU		  - lU		 ))
				 / (he ->fUAnodeR - le ->fUAnodeR))/ 64));
	}
	if (fUAnodeR < le ->fUAnodeR)
	{
	    return round_I32 (((((float) lU) / le ->fUAnodeR) * fUAnodeR));
	}
	if (he ->fUAnodeR < fUAnodeR)
	{
	    return round_I32 (((((float) hU) / he ->fUAnodeR) * fUAnodeR));
	}
	if ((le ->fUAnodeR <= fUAnodeR) && (fUAnodeR <= he ->fUAnodeR))
	{
	    return ((lU * 16) + (hU * 16))/16/2;
	}
    }}

    return fUAnodeR;
*/
};

#endif

