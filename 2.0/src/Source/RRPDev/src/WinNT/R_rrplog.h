
//[]------------------------------------------------------------------------[]
//
class CRRPLogFile
{
public :

friend	Bool		LogCreate	();
friend	void		LogPrintd	();
friend	void		LogPrintt	(Bool = False);
friend	void		LogPrintf	(const char * fmt, ...);
friend	void		LogClear	();
friend	void		LogFlush	();


	void		Flush		();

private :

static	HANDLE		create		();

private :

static	CRRPLogFile *		m_sLog	;

	TStreamBuf <4 * 4096>	m_sBuf	;
};

CRRPLogFile * CRRPLogFile::m_sLog = NULL;

Bool LogCreate ()
{
    if ((NULL == CRRPLogFile::m_sLog) && (NULL == (CRRPLogFile::m_sLog = new CRRPLogFile)))
    {	return False;}

	return True ;
};

void LogPrintd ()
{
    if (! LogCreate ()) return;

    SYSTEMTIME	     sTime ;
    GetLocalTime  (& sTime);

    CRRPLogFile::m_sLog ->m_sBuf.printfA ("%02d/%02d/%02d", sTime.wDay, sTime.wMonth, (sTime.wYear % 100));
};

void LogPrintt (Bool bMilliseconds)
{
    if (! LogCreate ()) return;

    SYSTEMTIME	     sTime ;
    GetLocalTime  (& sTime);

    CRRPLogFile::m_sLog ->m_sBuf.printfA ("%02d:%02d:%02d", sTime.wHour, sTime.wMinute, sTime.wSecond);

    if (bMilliseconds)
    CRRPLogFile::m_sLog ->m_sBuf.printfA (".%03d", sTime.wMilliseconds);
};

void LogPrintf (const char * fmt, ...)
{
    if (! LogCreate ()) return;

    int	      out;
    va_list   arg;

    va_start (arg, fmt);

    if (0 < (out = ::_vsnprintf ((char *) CRRPLogFile::m_sLog ->m_sBuf.tail	  (), 
					  CRRPLogFile::m_sLog ->m_sBuf.sizeforput (), fmt, arg)))
    {
	CRRPLogFile::m_sLog ->m_sBuf.put (out);
    }

    va_end   (arg);
};

void LogClear ()
{
    if (! LogCreate ()) return;

    CRRPLogFile::m_sLog ->m_sBuf.clear ();
};

void LogFlush ()
{
    if (! LogCreate ()) return;

    CRRPLogFile::m_sLog ->Flush ();
};

static void CleanupLogDir ()
{};

HANDLE CRRPLogFile::create ()
{
    HANDLE	hFile ;
    SYSTEMTIME	sTime ;

    TCHAR   pTime     [64];
    TCHAR   pFileName [MAX_PATH];

    GetLocalTime  (& sTime);

    wsprintf	  (pTime, "logs\\%02d_%02d_%02d.log", sTime.wDay, sTime.wMonth, (sTime.wYear) % 100);

    BuildFileName (pFileName, pTime);

    hFile = ::CreateFile (pFileName,
			  GENERIC_READ | GENERIC_WRITE,
			  FILE_SHARE_READ,
			  NULL,
			  OPEN_ALWAYS,
			  FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN,
			  NULL);

    if (INVALID_HANDLE_VALUE == hFile)
    {
	return NULL;
    }
    if (ERROR_ALREADY_EXISTS == ::GetLastError ())
    {}
    else
    {
	CleanupLogDir ();
    }

    ::SetFilePointer (hFile, 0, NULL, FILE_END);    

    return  hFile;
};

void CRRPLogFile::Flush ()
{
    HANDLE	 hFile ;

    if (NULL == (hFile = create ()))
    {
	return;
    }

    DWORD	 dTmp  ;

    ::WriteFile	  (hFile, m_sBuf.head (), (DWord) m_sBuf.sizeforget (), & dTmp, NULL);

    m_sBuf.clear  ();

    ::CloseHandle (hFile);
};

//
//[]------------------------------------------------------------------------[]
