
//[]------------------------------------------------------------------------[]
//
static char * findChr (char * pStr, char cChr)
{
    for (; * pStr && (* pStr != cChr); pStr ++)
    {}
    return pStr;
};

static char sscanChr (char *& pStr, char cChr, const char * pDelPtr = NULL)
{
    for (; * pStr && (pDelPtr) ? pStr < pDelPtr : True; pStr ++)
    {
	if (cChr == * pStr)
	{
	    break;
	}
    }
    return (pDelPtr && (pDelPtr <= pStr)) ? '\0' : * pStr;
};

static char sscanChr (char *& pStr, const char * pDelStr, const char * pDelPtr = NULL)
{
    for (; * pStr && (pDelPtr) ? pStr < pDelPtr : True; pStr ++)
    {
	if ('\0' != * findChr ((char *) pDelStr, * pStr))
	{
	    break;
	}
    }
    return (pDelPtr && (pDelPtr <= pStr)) ? '\0' : * pStr;
};

static char sskipChr (char *& pStr, const char * pDelStr, const char * pDelPtr = NULL)
{
    for (; * pStr && (pDelPtr) ? pStr < pDelPtr : True; pStr ++)
    {
	if ('\0' == * findChr ((char *) pDelStr, * pStr))
	{
	    break;
	}
    }
    return (pDelPtr && (pDelPtr <= pStr)) ? '\0' : * pStr;
};
//
//[]------------------------------------------------------------------------[]

//[]------------------------------------------------------------------------[]
//
    struct CIniSection
    {
	char *	    m_pBase;
	char *	    m_pTail;
	char *	    m_pHigh;
//
//  ........................................................................
//
	void	    clear	() { m_pTail = m_pBase;};

	DWord	    size	() { return (DWord)(m_pHigh - m_pBase);};

	DWord	    filled	() { return (DWord)(m_pTail - m_pBase);};
//
//  ........................................................................
//
	void	    insertStr	(      char *&  pTail	,
				 const void *	pData	,
				       int	nBytes	);

	void	    insertStr	(      char *&  pTail	,
				 const char *	pString	);

	char *	    findParam	(      char *&  pTail	,
				 const char *	pName	);

	Bool	    GetParam	(const char *	pName	,
				       char *	pValue	,
				       DWord	dValue	);

	Bool	    PutParam	(const char *	pName	,
				 const char *	pValue	,
				       DWord	dValeu	);

	Bool	    PutParamf	(const char *	pName	,
				 const char *	fmt	, ...);
    };

    template <size_t S> struct TIniSection : public CIniSection
    {
		    TIniSection () 
		    {
			m_pHigh =
		       (m_pTail =
			m_pBase = pReserved) + sizeof (pReserved);
		    }

	char 	    pReserved [S - sizeof (CIniSection)];
    };
//
//[]------------------------------------------------------------------------[]
//
void CIniSection::insertStr (char *& pTail, const void * pString, int nBytes)
{
    memmove (pTail + nBytes, pTail, m_pTail - pTail);
    memcpy  (pTail, pString, nBytes);

    m_pTail += nBytes;
};

void CIniSection::insertStr (char *& pTail, const char * pString)
{
    insertStr (pTail, pString, (DWord) strlen (pString));
};

char * CIniSection::findParam (char *& pTail, const char * pName)
{
    char *  p;
    char *  pHead = m_pBase;

    for (;;)
    {
	if ('\0' == sskipChr (pHead, " \t\r\n"))
	{   break;}

	if ('"'  == * pHead ++)
	{
	    for (p = (char *) pName; * p && * pName; p ++, pHead ++)
	    {
		if (* p == * pHead)
		{   continue;}

		    break   ;
	    }
	    if (('\0' == * p) && ('"' == * pHead))
	    {
	    if  ('='  == sskipChr (++ pHead, " \t"))
		{
	    if  ('\0' != sskipChr (++ pHead, " \t"))
		    {
	    if  (';'  == * (pTail = findChr (pHead, ';')))
			{
			    return pHead;
			}
		    }
		}
	    }
	}
	pHead = findChr (pHead, '\n');
    }
	return pTail = NULL;
};

Bool CIniSection::GetParam (const char * pName   ,
				  char * pBuf    ,
				  DWord  dBufSize)
{
    char *  pTail ;
    char *  pHead ;

    if (NULL ==
       (pHead = findParam (pTail, pName)))
    {	return False;}

    if (dBufSize < (DWord)(pTail - pHead + 1))
    {	return False;}

    memcpy  (pBuf, pHead, (pTail - pHead));

    * (pBuf + (pTail - pHead)) = '\0';

	return True;
};

Bool CIniSection::PutParam (const char * pName ,
			    const char * pValue,
				  DWord  dValue)
{
    int	    lN	   ;
    int	    lV	   ;
    int	    lE	   ;

    char *  pTail  ;
    char *  pHead  ;

    char    fName [128];

    if (NULL == 
       (pHead = findParam (pTail, pName)))
    {
    if (0 > (  lN = ::_snprintf (fName, sizeof (fName), "\t\"%s\" = ", pName)))
    {   return False;}

	pHead = 
	pTail = m_pTail;

	       lE = 3;
    }
    else
    {
	fName [lN = lE = 0] = '\0';
    }
	       lV = dValue;

    if ((m_pTail - (pTail - pHead) + lN + lV + lE) >= m_pHigh)
    {	return False;}

    memmove (pHead + lN + lV + lE, pTail , m_pTail - pTail);

    memcpy  (pHead		, fName  , lN);
    memcpy  (pHead + lN		, pValue , lV);
    if (lE)
    {
    memcpy  (pHead + lN + lV	, ";\r\n", lE);
    }

	m_pTail -= (pTail - pHead);
	m_pTail += (lN + lV + lE );

	return True;
};

Bool CIniSection::PutParamf (const char * pName, const char * fmt, ...)
{
    va_list   arg;
    va_start (arg, fmt);

    char      pValue [256];

    va_start (arg, fmt);

    if (0 > ::_vsnprintf (pValue, sizeof (pValue), fmt, arg))
    {	return False;}

    va_end   (arg);

	return PutParam (pName, pValue, (DWord) ::strlen (pValue));
};
//
//[]------------------------------------------------------------------------[]

static GResult GetParam (CIniSection & s, const char  * pName	  ,
						void  * pData	  ,
						DWord   dDataSize ,
						DWord * pDataLen  )
{
    if (! s.GetParam (pName, (char *) pData, dDataSize))
    {
      * ((char *) pData)   = '\0';
    }
    return (* pDataLen = (DWord) ::strlen ((char *) pData), ERROR_SUCCESS);
}

static GResult PutParam (CIniSection & s, const char  * pName	 ,
						void  * pData	 ,
						DWord	dDataLen )
{
    if (s.PutParam (pName, (char *) pData, dDataLen))
    {
	return ERROR_SUCCESS;
    }
	return ERROR_INVALID_DATA;
};

static GResult GetParam_X (CIniSection & s, const char  * pName	,
						  void  * pData	)
{
    char      pValue [256];

    if (s.GetParam  (pName, pValue,  sizeof  (pValue))
    && (1 == sscanf (pValue,  "%x", (DWord *) pData )))
    {}
    else
    {
      * ((DWord *) pData)  = 0;
    }
	return ERROR_SUCCESS;
};

static GResult PutParam_X (CIniSection & s, const char  * pName	,
						  void  * pData	)
{
    if (s.PutParamf (pName, "0x%08X", * ((DWord *) pData)))
    {
	return ERROR_SUCCESS;
    }
	return ERROR_INVALID_DATA;
};

static GResult GetParam_d (CIniSection & s, const char  * pName	,
						  void  * pData	)
{
    char      pValue [256];

    if (s.GetParam  (pName, pValue,  sizeof  (pValue))
    && (1 == sscanf (pValue,  "%d", (DWord *) pData )))
    {}
    else
    {
      * ((DWord *) pData)  = 0;
    }
	return ERROR_SUCCESS;
};

static GResult PutParam_d (CIniSection & s, const char  * pName	,
						  void  * pData	)
{
    if (s.PutParamf (pName, "%d", * ((DWord *) pData)))
    {
	return ERROR_SUCCESS;
    }
	return ERROR_INVALID_DATA;
};

static GResult GetParam_s_s (CIniSection & s, const char  * pName ,
						    void  * pData )
{
    DWord     dHiValue	  ;
    DWord     dLoValue	  ;
    char      pValue [256];

    if (s.GetParam  (pName, pValue,   sizeof (pValue))
    && (2 == sscanf (pValue, "%d, %d", & dHiValue, & dLoValue)))
    {}
    else
    {
	dHiValue = dLoValue = 0;
    }
	return (* ((DWord *) pData) = (dHiValue << 16) | (dLoValue << 0), ERROR_SUCCESS);
};

static GResult PutParam_s_s (CIniSection & s, const char  * pName ,
						    void  * pData )
{
    if (s.PutParamf (pName, "%d, %d", HIWORD (* ((DWord *) pData)),
				      LOWORD (* ((DWord *) pData))))
    {
	return ERROR_SUCCESS;
    }
	return ERROR_INVALID_DATA;
};

//[]------------------------------------------------------------------------[]
//
#define	INI_FILE_SIZE	(32 * 1024)

class CRRPIniFile : public IRRPSettings
{
    friend class CRRPDeviceService;

protected :

    STDMETHOD_ (void,	      Close)();

    STDMETHOD_ (GResult,   GetValue)(ParamId, void  * pData	,
					      DWord   dDataSize	,
					      DWord * pDataLen	);

    STDMETHOD_ (GResult,   SetValue)(ParamId, void  * pData	,
					      DWord   dDataSize	);
    STDMETHOD_ (GResult,    Checkin)(ClassId);

public :

static	GResult		    CreateInstance	(IRRPSettings *& pISettings);

protected :

			    CRRPIniFile		()
			    {
				TCHAR pFileName [MAX_PATH];

				BuildFileName (pFileName, "MDIGSetup.ini");

				if (INVALID_HANDLE_VALUE ==
				   (m_hFile		  = ::CreateFile 
							    (pFileName,
							     GENERIC_READ | GENERIC_WRITE,
							     FILE_SHARE_READ | FILE_SHARE_WRITE,
							     NULL,
							     OPEN_ALWAYS,
							     FILE_ATTRIBUTE_NORMAL,
							     NULL		  )))
				{
				    m_hFile = NULL;
				}
				else
				{
				    Create (pFileName);
				}
			    };

			   ~CRRPIniFile		()
			    {
				if (m_hFile)
				{
				    ::CloseHandle (m_hFile);
						   m_hFile = NULL;
				}
			    };

	void		Create		    (LPCTSTR);

	const char *	GetSectionInfo	    (ClassId, CIniSection *&);

	Bool		LoadSection	    (ClassId, Bool bVerExt = True);

	Bool		SaveSection	    (ClassId, Bool bVerExt = True);

	Bool		LoadFile	    (char * pBuf, DWord, DWord &);

	Bool		SaveFile	    (char * pBuf, DWord);

	Bool		LoadSection	    (CIniSection &, const char * pName);

	Bool		SaveSection	    (CIniSection &, const char * pName);

protected :

	HANDLE			m_hFile	;

	TIniSection <4 * 1024>	m_MDIG_Section	;
	TIniSection <8 * 1024>	m_URP_Section	;
	TIniSection <8 * 1024>	m_MSC_Section	;
	TIniSection <4 * 1024>	m_IMG_Section	;
};
//
//[]------------------------------------------------------------------------[]
//
GResult CRRPIniFile::CreateInstance (IRRPSettings *& pISettings)
{
#if FALSE

    CRRPIniFile * pIniFile = new CRRPIniFile;

    pISettings	= pIniFile;

    return ERROR_SUCCESS;

#else

    pISettings  = NULL;

    return ERROR_NOT_SUPPORTED;

#endif
};

void CRRPIniFile::Close ()
{
#if FALSE

    delete this;

#endif
};

static DWord GetEngHashVersion ()
{
    HMODULE	hModule	;
    union
    {
    void *	pv	;
    VS_FIXEDFILEINFO *
		fv	;
    };
    UINT	sv	;
    TCHAR	pModule  [256 ];

    BYTE	pVersion [4096];

    if (NULL != (hModule = g_GWinModule.hExeDll))
    {
    if (::GetModuleFileName  (hModule, pModule, sizeof (pModule)/sizeof (TCHAR)))
    {
    if (::GetFileVersionInfo (pModule, NULL, sizeof (pVersion), pVersion))
    {
    if (::VerQueryValue	     (pVersion, "\\", & pv, & sv))
    {
	return (((DWord) HIWORD(fv ->dwFileVersionMS)) << 3) +
	       (((DWord) LOWORD(fv ->dwFileVersionMS)) << 2) +
	       (((DWord) HIWORD(fv ->dwFileVersionLS)) << 1) +
	       (((DWord) LOWORD(fv ->dwFileVersionLS)) << 0);
    }
    }
    }
    }
	return 0;
};

static GResult GetEngVersion (void * pData, DWord dDataSize, DWord * pDataLen)
{
    * ((char *) pData)	= '\0';

    HMODULE	hModule	;
    union
    {
    void *	pv	;
    VS_FIXEDFILEINFO *
		fv	;
    };
    UINT	sv	;
    TCHAR	pModule  [256 ];

    BYTE	pVersion [4096];

    if (NULL != (hModule = g_GWinModule.hExeDll))
    {
    if (::GetModuleFileName  (hModule, pModule, sizeof (pModule)/sizeof (TCHAR)))
    {
    if (::GetFileVersionInfo (pModule, NULL, sizeof (pVersion), pVersion))
    {
    if (::VerQueryValue	     (pVersion, "\\", & pv, & sv))
    {
	::wsprintf ((char *) pData, "\"Engine %d.%d.%d.%d\"", HIWORD(fv ->dwFileVersionMS),
							      LOWORD(fv ->dwFileVersionMS),
							      HIWORD(fv ->dwFileVersionLS),
							      LOWORD(fv ->dwFileVersionLS));
    }
    }
    }
    }

    return (* pDataLen = (DWord) ::strlen ((char *) pData), ERROR_SUCCESS);
};

void OnSettingsChanged (IRRPSettings::ClassId);

GResult CRRPIniFile::GetValue (ParamId uParamId, void  * pData	   ,
						 DWord   dDataSize ,
						 DWord * pDataLen  )
{
    switch (uParamId)
    {
    case ID_MDIG_SerialNum :
	return GetParam	    (m_MDIG_Section, "SerialNum", pData, dDataSize, pDataLen);
    case ID_MDIG_EquMask :
	return GetParam_X   (m_MDIG_Section, "EquMask", pData);
    case ID_MDIG_ThickTable :
	return GetParam	    (m_MDIG_Section, "ThickR, (UAnodeX, IAnodeR, Type)", pData, dDataSize, pDataLen);

    case ID_MDIG_AnodeFilter	:
	return GetParam	    (m_MDIG_Section, "AnodeFilter", pData, dDataSize, pDataLen);
    case ID_MDIG_SFocusSize	:
	return GetParam	    (m_MDIG_Section, "SFocusSize", pData, dDataSize, pDataLen);
    case ID_MDIG_BFocusSize	:
	return GetParam	    (m_MDIG_Section, "BFocusSize", pData, dDataSize, pDataLen);
    case ID_MDIG_DiafrWidth	:
	return GetParam_d   (m_MDIG_Section, "DiafrWidth", pData);
    case ID_MDIG_ScanTime	:
	return GetParam_d   (m_MDIG_Section, "ScanTime", pData);

    case ID_MDIG_RadOutput	:
	return GetParam	    (m_MDIG_Section, "RadOutput", pData, dDataSize, pDataLen);

    case ID_MDIG_EngVersion	:
	return GetEngVersion (pData, dDataSize, pDataLen);

    case ID_MDIG_FocusLength	:
	return GetParam	    (m_MDIG_Section, "FocusLength", pData, dDataSize, pDataLen);
//
//..............................................................
//
    case ID_URP_SHOT_UAnodeMin :
	return GetParam_d   (m_URP_Section, "SHOT_UAnodeMin", pData);
    case ID_URP_SHOT_UAnodeMax :
	return GetParam_d   (m_URP_Section, "SHOT_UAnodeMax", pData);
    case ID_URP_SHOT_IFilamentMin :
	return GetParam_d   (m_URP_Section, "SHOT_IFilamentMin", pData);
    case ID_URP_SHOT_IFilamentMax :
	return GetParam_d   (m_URP_Section, "SHOT_IFilamentMax", pData);

    case ID_URP_SF_UAnodeMin :
	return GetParam_d   (m_URP_Section, "SF_UAnodeMin", pData);
    case ID_URP_SF_UAnodeMax :
	return GetParam_d   (m_URP_Section, "SF_UAnodeMax", pData);
    case ID_URP_SF_IFilamentPreMin :
	return GetParam_d   (m_URP_Section, "SF_IFilamentPreMin", pData);
    case ID_URP_SF_IFilamentPreMax :
	return GetParam_d   (m_URP_Section, "SF_IFilamentPreMax", pData);
    case ID_URP_SF_IFilamentPreDef :
	return GetParam_d   (m_URP_Section, "SF_IFilamentPreDef", pData);
    case ID_URP_SF_IFilamentMax :
	return GetParam_d   (m_URP_Section, "SF_IFilamentMax", pData);
    case ID_URP_SF_IAnodeTable :
	return GetParam	    (m_URP_Section, "SF_UAnodeX, (IFilamentX, IAnodeR)", pData, dDataSize, pDataLen);

    case ID_URP_BF_UAnodeMin :
	return GetParam_d   (m_URP_Section, "BF_UAnodeMin", pData);
    case ID_URP_BF_UAnodeMax :
	return GetParam_d   (m_URP_Section, "BF_UAnodeMax", pData);
    case ID_URP_BF_IFilamentPreMin :
	return GetParam_d   (m_URP_Section, "BF_IFilamentPreMin", pData);
    case ID_URP_BF_IFilamentPreMax :
	return GetParam_d   (m_URP_Section, "BF_IFilamentPreMax", pData);
    case ID_URP_BF_IFilamentPreDef :
	return GetParam_d   (m_URP_Section, "BF_IFilamentPreDef", pData);
    case ID_URP_BF_IFilamentMax :
	return GetParam_d   (m_URP_Section, "BF_IFilamentMax", pData);
    case ID_URP_BF_IAnodeTable :
	return GetParam	    (m_URP_Section, "BF_UAnodeX, (IFilamentX, IAnodeR)", pData, dDataSize, pDataLen);

    case ID_URP_UForceMin :
	return GetParam_d   (m_URP_Section, "UForceMin", pData);
    case ID_URP_TForceMax :
	return GetParam_d   (m_URP_Section, "TForceMax", pData);
    case ID_URP_TTubeMax :
	return GetParam_d   (m_URP_Section, "TTubeMax",  pData);

    case ID_URP_HeatDelay :
	return GetParam_d   (m_URP_Section, "HeatDelay", pData);
    case ID_URP_TExposeMax :
	return GetParam_d   (m_URP_Section, "TExposeMax",  pData);

    case ID_URP_HeatForceLo :
	return GetParam_d   (m_URP_Section, "HeatForceLo", pData);
    case ID_URP_HeatForceHi :
	return GetParam_d   (m_URP_Section, "HeatForceHi", pData);
    case ID_URP_HeatForceK :
	return GetParam_d   (m_URP_Section, "HeatForceK",  pData);

    case ID_URP_HeatTubeLo :
	return GetParam_d   (m_URP_Section, "HeatTubeLo", pData);
    case ID_URP_HeatTubeHi :
	return GetParam_d   (m_URP_Section, "HeatTubeHi", pData);
    case ID_URP_HeatTubeK :
	return GetParam_d   (m_URP_Section, "HeatTubeK",  pData);

    case ID_URP_DelayParam_Full :
	return GetParam_s_s (m_URP_Section, "DelayParam_Full", pData);
    case ID_URP_DelayParam_21x30 :
	return GetParam_s_s (m_URP_Section, "DelayParam_21x30", pData);
    case ID_URP_DelayParam_18x24 :
	return GetParam_s_s (m_URP_Section, "DelayParam_18x24", pData);
    case ID_URP_DelayParam_12x12 :
	return GetParam_s_s (m_URP_Section, "DelayParam_12x12", pData);

    case ID_URP_UACorrection :
	return GetParam_d   (m_URP_Section, "UAnodeX_Correction", pData);
    case ID_URP_IARetention :
	return GetParam_d   (m_URP_Section, "IAnodeX_Retention", pData);

    case ID_URP_UACorrectionTable :
	return GetParam	    (m_URP_Section, "UAnodeX, (UAnodeR)", pData, dDataSize, pDataLen);
//
//..............................................................
//
    case ID_MSC_Version :
	return GetParam_X   (m_MSC_Section, "Version", pData);

    case ID_MSC_Scan_IAnodeMax :
	return GetParam	    (m_MSC_Section, "Scan_IAnodeMax", pData, dDataSize, pDataLen);

    case ID_MSC_Scan_12_Param :
	return GetParam_s_s (m_MSC_Section, "Scan_12_Param", pData);
    case ID_MSC_Scan_23_Param :
	return GetParam_s_s (m_MSC_Section, "Scan_23_Param", pData);

    case ID_MSC_ImageProcMask :
	return GetParam_X   (m_MSC_Section, "ImageProcMask", pData);

    case ID_MSC_ImageKX :
	return GetParam_d   (m_MSC_Section, "ImageKX", pData);
    case ID_MSC_ImageKY :
	return GetParam_d   (m_MSC_Section, "ImageKY", pData);
//
//..............................................................
//
    case ID_MSC_Diafr_SyncDelay :
	return GetParam_d (m_IMG_Section, "Diafr_SyncDelay", pData);
    case ID_MSC_Diafr_DefCoeff :
	return GetParam_d (m_IMG_Section, "Diafr_DefCoeff", pData);
    case ID_MSC_Diafr_AccelTime :
	return GetParam_d (m_IMG_Section, "Diafr_AccelTime", pData);
    case ID_MSC_Diafr_Velocity :
	return GetParam_d (m_IMG_Section, "Diafr_Velocity", pData);
    case ID_MSC_Diafr_StepPerDec :
	return GetParam_d (m_IMG_Section, "Diafr_StepPerDec", pData);

    case ID_MSC_Scan_SyncDelay :
	return GetParam_d (m_IMG_Section, "Scan_SyncDelay", pData);
    case ID_MSC_Scan_DefCoeff :
	return GetParam_d (m_IMG_Section, "Scan_DefCoeff", pData);
    case ID_MSC_Scan_AccelTime :
	return GetParam_d (m_IMG_Section, "Scan_AccelTime", pData);
    case ID_MSC_Scan_Velocity :
	return GetParam_d (m_IMG_Section, "Scan_Velocity", pData);
    case ID_MSC_Scan_StepPerDec :
	return GetParam_d (m_IMG_Section, "Scan_StepPerDec", pData);

    case ID_MSC_Scan_CenterLine :
	return GetParam_d (m_IMG_Section, "Scan_CenterLine", pData);

    case ID_MSC_ImageDCBeg :
	return GetParam_d (m_IMG_Section, "ImageDCBeg", pData);
    case ID_MSC_ImageDCEnd :
	return GetParam_d (m_IMG_Section, "ImageDCEnd", pData);
    case ID_MSC_ImageActiveBeg :
	return GetParam_d (m_IMG_Section, "ImageActiveBeg", pData);
    case ID_MSC_ImageActiveEnd :
	return GetParam_d (m_IMG_Section, "ImageActiveEnd", pData);
//
//..............................................................
//
    };
	return ERROR_INVALID_PARAMETER;
};

GResult CRRPIniFile::SetValue (ParamId uParamId, void  * pData	  ,
						 DWord   dDataSize)
{
    switch (uParamId)
    {
    case ID_MDIG_SerialNum :
	return PutParam	  (m_MDIG_Section, "SerialNum", pData, dDataSize);
    case ID_MDIG_EquMask :
	return PutParam_X (m_MDIG_Section, "EquMask", pData);
//
//..............................................................
//
    case ID_URP_SF_UAnodeMin :
	return PutParam_d   (m_URP_Section, "SF_UAnodeMin", pData);
    case ID_URP_SF_UAnodeMax :
	return PutParam_d   (m_URP_Section, "SF_UAnodeMax", pData);
    case ID_URP_SF_IFilamentPreMin :
	return PutParam_d   (m_URP_Section, "SF_IFilamentPreMin", pData);
    case ID_URP_SF_IFilamentPreMax :
	return PutParam_d   (m_URP_Section, "SF_IFilamentPreMax", pData);
    case ID_URP_SF_IFilamentPreDef :
	return PutParam_d   (m_URP_Section, "SF_IFilamentPreDef", pData);
    case ID_URP_SF_IFilamentMax :
	return PutParam_d   (m_URP_Section, "SF_IFilamentMax", pData);
    case ID_URP_SF_IAnodeTable :
	return PutParam	    (m_URP_Section, "SF_UAnodeX, (IFilamentX, IAnodeR)", pData, dDataSize);

    case ID_URP_BF_UAnodeMin :
	return PutParam_d   (m_URP_Section, "BF_UAnodeMin", pData);
    case ID_URP_BF_UAnodeMax :
	return PutParam_d   (m_URP_Section, "BF_UAnodeMax", pData);
    case ID_URP_BF_IFilamentPreMin :
	return PutParam_d   (m_URP_Section, "BF_IFilamentPreMin", pData);
    case ID_URP_BF_IFilamentPreMax :
	return PutParam_d   (m_URP_Section, "BF_IFilamentPreMax", pData);
    case ID_URP_BF_IFilamentPreDef :
	return PutParam_d   (m_URP_Section, "BF_IFilamentPreDef", pData);
    case ID_URP_BF_IFilamentMax :
	return PutParam_d   (m_URP_Section, "BF_IFilamentMax", pData);
    case ID_URP_BF_IAnodeTable :
	return PutParam	    (m_URP_Section, "BF_UAnodeX, (IFilamentX, IAnodeR)", pData, dDataSize);

    case ID_URP_UForceMin :
	return PutParam_d   (m_URP_Section, "UForceMin", pData);
    case ID_URP_TForceMax :
	return PutParam_d   (m_URP_Section, "TForceMax", pData);
    case ID_URP_TTubeMax :
	return PutParam_d   (m_URP_Section, "TTubeMax",  pData);

    case ID_URP_HeatDelay :
	return PutParam_d   (m_URP_Section, "HeatDelay", pData);
    case ID_URP_TExposeMax :
	return PutParam_d   (m_URP_Section, "TExposeMax",  pData);

    case ID_URP_HeatForceLo :
	return PutParam_d   (m_URP_Section, "HeatForceLo", pData);
    case ID_URP_HeatForceHi :
	return PutParam_d   (m_URP_Section, "HeatForceHi", pData);
    case ID_URP_HeatForceK :
	return PutParam_d   (m_URP_Section, "HeatForceK",  pData);

    case ID_URP_HeatTubeLo :
	return PutParam_d   (m_URP_Section, "HeatTubeLo", pData);
    case ID_URP_HeatTubeHi :
	return PutParam_d   (m_URP_Section, "HeatTubeHi", pData);
    case ID_URP_HeatTubeK :
	return PutParam_d   (m_URP_Section, "HeatTubeK",  pData);

    case ID_URP_DelayParam_Full :
	return PutParam_s_s (m_URP_Section, "DelayParam_Full", pData);
    case ID_URP_DelayParam_21x30 :
	return PutParam_s_s (m_URP_Section, "DelayParam_21x30", pData);
    case ID_URP_DelayParam_18x24 :
	return PutParam_s_s (m_URP_Section, "DelayParam_18x24", pData);
    case ID_URP_DelayParam_12x12 :
	return PutParam_s_s (m_URP_Section, "DelayParam_12x12", pData);

    case ID_URP_UACorrection :
	return PutParam_d   (m_URP_Section, "UAnodeX_Correction", pData);
    case ID_URP_IARetention :
	return PutParam_d   (m_URP_Section, "IAnodeX_Retention", pData);
//
//..............................................................
//
    case ID_MSC_Version :
    {{
	GResult dResult;

	if (ERROR_SUCCESS == (dResult = PutParam_X (m_MSC_Section, "Version", pData)))
	{
	    dResult = SaveSection (CLSID_MSC, False);

		      LoadSection (CLSID_MSC);

		OnSettingsChanged (CLSID_MSC);
	}
	return dResult;
    }}

    case ID_MSC_Scan_12_Param :
	return PutParam_s_s (m_MSC_Section, "Scan_12_Param", pData);
    case ID_MSC_Scan_23_Param :
	return PutParam_s_s (m_MSC_Section, "Scan_23_Param", pData);

    case ID_MSC_ImageProcMask :
	return PutParam_X   (m_MSC_Section, "ImageProcMask", pData);
//
//..............................................................
//
    case ID_MSC_Diafr_SyncDelay :
	return PutParam_d (m_IMG_Section, "Diafr_SyncDelay", pData);
    case ID_MSC_Diafr_DefCoeff :
	return PutParam_d (m_IMG_Section, "Diafr_DefCoeff", pData);
    case ID_MSC_Diafr_AccelTime :
	return PutParam_d (m_IMG_Section, "Diafr_AccelTime", pData);
    case ID_MSC_Diafr_Velocity :
	return PutParam_d (m_IMG_Section, "Diafr_Velocity", pData);
    case ID_MSC_Diafr_StepPerDec :
	return PutParam_d (m_IMG_Section, "Diafr_StepPerDec", pData);

    case ID_MSC_Scan_SyncDelay :
	return PutParam_d (m_IMG_Section, "Scan_SyncDelay", pData);
    case ID_MSC_Scan_DefCoeff :
	return PutParam_d (m_IMG_Section, "Scan_DefCoeff", pData);
    case ID_MSC_Scan_AccelTime :
	return PutParam_d (m_IMG_Section, "Scan_AccelTime", pData);
    case ID_MSC_Scan_Velocity :
	return PutParam_d (m_IMG_Section, "Scan_Velocity", pData);
    case ID_MSC_Scan_StepPerDec :
	return PutParam_d (m_IMG_Section, "Scan_StepPerDec", pData);

    case ID_MSC_Scan_CenterLine :
	return PutParam_d (m_IMG_Section, "Scan_CenterLine", pData);
//
//..............................................................
//
    };
	return ERROR_INVALID_PARAMETER;
};

const char * CRRPIniFile::GetSectionInfo (ClassId uClassId, CIniSection *& pSection)
{
    switch (uClassId)
    {
    case CLSID_MDIG    : return (pSection = & m_MDIG_Section, "MDIG_1.0");
    case CLSID_URP     : return (pSection = & m_URP_Section , "URP_CC5_2.0");
    case CLSID_MSC     : return (pSection = & m_MSC_Section , "MSC_1.0");
    }
			 return (pSection = NULL, NULL);
};

GResult CRRPIniFile::Checkin (ClassId uClassId)
{
    GResult dResult = SaveSection (uClassId);

		OnSettingsChanged (uClassId);

    return  dResult;
};


static char * __findSection (char *& pTail)
{
    char * pHead;

    for (;;)
    {
	if ('\0' == sskipChr (pTail, " \t\r\n"))
	{   break;}

	if ('{'  == * pTail ++)
	{
	if ('"'  == * pTail ++)
	{
	    if ('"' == (* (pTail = findChr (pHead = pTail, '"'))))
	    {
	      * pTail ++ = '\0';

		return pHead;
	    }
	}
	}
    }
    return NULL;
};

static char * __findParam (char *& pTail, char *& pValue)
{
    char * pHead;

    for (;;)
    {
	if ('\0' == sskipChr (pTail, " \t\r\n"))
	{   break;}

	if ('"'  == * pTail ++)
	{
	    pHead = pTail;

	    for (; * pTail && ('"' != * pTail); pTail ++) {}

	    if ('"' == * pTail)
	    {
	      * pTail ++ = '\0';

		if ('='  == sskipChr (   pTail, " \t"))
		{
		if ('\0' != sskipChr (++ pTail, " \t"))
		    {
			pValue = pTail;

		if (';'  == * (pTail = findChr (pTail, ';')))
			{
			    return pHead;
			}
		    }
		}
	    }
	}
	break;
    }
    return NULL;
};

void CRRPIniFile::Create (LPCTSTR pFileName)
{
    char * pTail;
    
    DWord  dVersion  = 0; 
    DWord  hVersion  = GetEngHashVersion ();

    Bool   bExisting = LoadSection (CLSID_MDIG);

    union
    {
    TCHAR  pTempName [1];
    char   pVersion  [1];
    char   pFileBuf  [INI_FILE_SIZE];
    };

    if (bExisting)
    {
	GetParam_X (m_MDIG_Section, "Version", & dVersion);

	if (dVersion == hVersion)
	{
	    LoadSection (CLSID_URP );
	    LoadSection (CLSID_MSC );
	    return;
/*
goto lMergin;
*/
	}

	BuildFileName (pTempName, "MDIGSetup.sav" );
	::CopyFile    (pFileName, pTempName, False);    // ini -> old
    }
	BuildFileName (pTempName, "MDIGSetup.def" );
	::CopyFile    (pTempName, pFileName, False);	// def -> ini

    LoadSection (CLSID_MDIG);
    {
	::wsprintf  (pVersion, "\r\n\r\n\t\"Version\"\t= 0x%08X;", hVersion);

	pTail = (char *) m_MDIG_Section.m_pBase   ;
	m_MDIG_Section.insertStr (pTail, pVersion);
    }
    SaveSection (CLSID_MDIG);

    if (! bExisting)
    {{
	LoadSection (CLSID_URP );
	LoadSection (CLSID_MSC );
	return;
    }}

//
//  Do merging !!!
/*
lMergin :
*/
    HANDLE hSavFile; DWord dSavSize; 
    
    char * pHead; char * pSrcName; char * pSrcParam; char * pSrcValue; char * pDstBeg; char * pDstEnd;

    TIniSection <8 * 1024> DstSection  ;
    TIniSection <4 * 1024> DstSection2 ;

    BuildFileName (pTempName, "MDIGSetup.sav" );

    if (INVALID_HANDLE_VALUE ==
       (hSavFile	      = ::CreateFile 
			        (pTempName,
				 GENERIC_READ,
				 FILE_SHARE_READ | FILE_SHARE_WRITE,
				 NULL,
				 OPEN_EXISTING,
				 FILE_ATTRIBUTE_NORMAL,
				 NULL		      )))
    {
	return;
    }
    if (! ::ReadFile (hSavFile, pFileBuf, sizeof (pFileBuf), & dSavSize, NULL))
    {
	CloseHandle (hSavFile);
	return;
    }
    CloseHandle (hSavFile);

    * ((pHead = pFileBuf) + dSavSize) = '\0';

    for (;NULL != (pSrcName = __findSection (pHead));)
    {
//	printf ("Section <%s>\n", pSrcName);
//
	if (LoadSection (DstSection, pSrcName))
	{
	    if (0 == strcmp (pSrcName, "MSC_3x1537_1.0"))
	    {
		LoadSection (DstSection2, "MSC_1.0");
	    }
	    else
	    {	DstSection2.clear (); }

	    for (;NULL != (pSrcParam = __findParam (pHead, pSrcValue));)
	    {
	    if  (0 != strcmp (pSrcParam, "Version"))
	    {
	    if  ( NULL != (pDstBeg   = DstSection.findParam (pDstEnd, pSrcParam)))
	    {
//		printf ("\tParameter  <%s> [%c,%c] <- [%c,%c]\n", pSrcParam, * pSrcValue, * pHead, * pDstBeg, * pDstEnd);
//
		DstSection.PutParam  (pSrcParam, pSrcValue, (DWord)(pHead - pSrcValue));
	    }
	    else
	    if  ( NULL != (pDstBeg   = DstSection2.findParam (pDstEnd, pSrcParam)))
	    {
//		printf ("\tParameter2 <%s> [%c,%c] <- [%c,%c]\n", pSrcParam, * pSrcValue, * pHead, * pDstBeg, * pDstEnd);
//
		DstSection2.PutParam (pSrcParam, pSrcValue, (DWord)(pHead - pSrcValue));
	    }
	    }
		pHead ++;
	    }
/*
printf ("\n\n=============\n%s\n=============\n\n", DstSection.m_pBase);
*/
	    SaveSection (DstSection, pSrcName);
	    DstSection.clear ();

	    if (DstSection2.filled ())
	    {
		SaveSection (DstSection2, "MSC_1.0");
		DstSection2.clear ();
	    }
	}
	else
	{
	    continue;
	}
    }

    LoadSection (CLSID_MDIG);
    LoadSection (CLSID_URP );
    LoadSection (CLSID_MSC );
};
//
//[]------------------------------------------------------------------------[]
//
static char * findSection (char *& pTail, const char * pName)
{
    char *  p;
    char *  pHead = pTail;

    for (;;)
    {
	if ('\0' == sskipChr (pHead, " \t\r\n"))
	{   break;}

	if ('{'  == * pHead ++)
	{
	if ('"'  == * pHead ++)
	{
	    for (p = (char *) pName; * p && * pName; p ++, pHead ++)
	    {
		if (* p == * pHead)
		{   continue;}

		    break   ;
	    }
	    if (('\0' == * p) && ('"' == * pHead))
	    {
	    if  ('}'  == * (pTail = findChr (++ pHead, '}')))
		{
		    return pHead;
		}
	    }
	}
	}
	pHead = findChr (pHead, '\n');
    }
	return pTail = NULL;
};

Bool CRRPIniFile::LoadSection (ClassId uClassId, Bool bVerExt)
{
    CIniSection * s;
    const char  * sName;

    if (NULL ==  (sName = GetSectionInfo (uClassId, s)))
    {	return False;}

    DWord   dFileSize;
    char    pFileBuf [INI_FILE_SIZE];

    if (! LoadFile (pFileBuf, sizeof (pFileBuf) - 1, dFileSize))
    {	return False;}

lVerExt :

    * (pFileBuf + dFileSize) = '\0';

    char * pHead;
    char * pTail = pFileBuf;

	s ->clear ();

    if (NULL ==
       (pHead = findSection (pTail, sName)))
    {	return False;}

    if (s ->size () < (DWord)(pTail - pHead + 1))
    {	return False;}

    memcpy (s ->m_pBase, pHead, (pTail - pHead));

	    s ->m_pTail	     += (pTail - pHead);
	  * s ->m_pTail	      = '\0';
// <<
//
    if ((CLSID_MSC == uClassId) && bVerExt)
    {{
	  uClassId = CLSID_IMG;
	  s	   = & m_IMG_Section ;

    DWord dVersion =  0x0100;
	  sName	   = "MSC_3x1537_1.0";

	  GetValue (IRRPSettings::ID_MSC_Version, & dVersion, sizeof (DWord), NULL);

    if	 (dVersion == 0x0200)
    {	  sName	   = "MSC_3x1537_2.0";}

	  goto lVerExt;
    }}
//
// >>
	return True;
};

Bool CRRPIniFile::SaveSection (ClassId uClassId, Bool bVerExt)
{
    CIniSection	* s; 
    const char  * sName;

    if (NULL ==  (sName = GetSectionInfo (uClassId, s)))
    {	return False;}

    DWord dFileSize;
    char  pFileBuf [INI_FILE_SIZE];

    if (! LoadFile (pFileBuf, sizeof (pFileBuf) - 1, dFileSize))
    {	return False;}

lVerExt :

    * (pFileBuf + dFileSize) = '\0';

    char * pHead;
    char * pTail = pFileBuf;

    if (NULL ==
       (pHead = findSection (pTail, sName)))
    {	return False;}

    if ((dFileSize - (pTail - pHead) + s ->filled ()) > INI_FILE_SIZE)
    {	return False;}

    memmove (pHead + s ->filled (), pTail, (pFileBuf + dFileSize) - pTail);
    memcpy  (pHead,  s ->m_pBase  , s ->filled ());

	dFileSize -= ((DWord)(pTail - pHead));
	dFileSize += s ->filled ();
// <<
//
    if ((CLSID_MSC == uClassId) && bVerExt)
    {{
	  uClassId = CLSID_IMG;
	  s	   = & m_IMG_Section ;

    DWord dVersion =  0x0100;
	  sName	   = "MSC_3x1537_1.0";

	  GetValue (IRRPSettings::ID_MSC_Version, & dVersion, sizeof (DWord), NULL);

    if	 (dVersion == 0x0200)
    {	  sName	   = "MSC_3x1537_2.0";}

	  goto lVerExt;
    }}
//
// >>
	return SaveFile (pFileBuf, dFileSize);
};
//
//[]------------------------------------------------------------------------[]
//
Bool CRRPIniFile::LoadSection (CIniSection & s, const char * pName)
{
    DWord   dFileSize;
    char    pFileBuf [INI_FILE_SIZE];

    if (! LoadFile (pFileBuf, sizeof (pFileBuf) - 1, dFileSize))
    {	return False;}

    * (pFileBuf + dFileSize) = '\0';

    char * pHead;
    char * pTail = pFileBuf;

	s.clear ();

    if (NULL ==
       (pHead = findSection (pTail, pName)))
    {	return False;}

    if (s.size () < (DWord)(pTail - pHead + 1))
    {	return False;}

    memcpy (s.m_pBase, pHead, (pTail - pHead));

	    s.m_pTail	     += (pTail - pHead);
	  * s.m_pTail	      = '\0';

	return True;
};

Bool CRRPIniFile::SaveSection (CIniSection & s, const char * pName)
{
    DWord dFileSize;
    char  pFileBuf [INI_FILE_SIZE];

    if (! LoadFile (pFileBuf, sizeof (pFileBuf) - 1, dFileSize))
    {	return False;}

    * (pFileBuf + dFileSize) = '\0';

    char * pHead;
    char * pTail = pFileBuf;

    if (NULL ==
       (pHead = findSection (pTail, pName)))
    {	return False;}

    if ((dFileSize - (pTail - pHead) + s.filled ()) > INI_FILE_SIZE)
    {	return False;}

    memmove (pHead + s.filled (), pTail, (pFileBuf + dFileSize) - pTail);
    memcpy  (pHead,  s.m_pBase  , s.filled ());

	dFileSize -= ((DWord)(pTail - pHead));
	dFileSize += s.filled ();

	return SaveFile (pFileBuf, dFileSize);
};
//
//[]------------------------------------------------------------------------[]
//
Bool CRRPIniFile::LoadFile (char * pBuf, DWord dBufSize, DWord & dFileSize)
{
    if (NULL == m_hFile)
    {	return False;}

    DWord   dTemp    ;

    if (dBufSize < (dFileSize = ::GetFileSize (m_hFile, NULL)))
    {	return False;}

    ::SetFilePointer (m_hFile, 0, NULL, FILE_BEGIN);
    if (! ::ReadFile (m_hFile, pBuf, dFileSize, & dTemp, NULL) || (dTemp != dFileSize))
    {	return False;}

	return True ;
};

Bool CRRPIniFile::SaveFile (char * pBuf, DWord dBufSize)
{
    if (NULL == m_hFile)
    {	return False;}

    DWord   dTemp    ;

    ::SetFilePointer  (m_hFile, 0, NULL, FILE_BEGIN);
    if (! ::WriteFile (m_hFile, pBuf, dBufSize, & dTemp, NULL) || (dTemp != dBufSize))
    {	return False;}

    ::SetEndOfFile    (m_hFile);

	return True ;
};
//
//[]------------------------------------------------------------------------[]


#if FALSE

//[]------------------------------------------------------------------------[]
//
class CIniFile
{
public :
		    CIniFile	()
		    {
			m_pBufBase = NULL;
			m_dBufSize = 0	 ;
		    };

		   ~CIniFile	()
		    {
			Free ();
		    };

	Bool		    Load	    (LPTSTR pFileName);

	void		    Free	    ()
			    {
				if (m_pBufBase)
				{
				    free 
				   (m_pBufBase);
				    m_pBufBase = NULL;
				    m_dBufSize = 0   ;
				}
			    };

	Bool		    FindSection	    (LPTSTR & pHead,
					     LPTSTR & pTail, LPTSTR pName);

protected :

	DWord	    m_dBufSize	    ;
	char *	    m_pBufBase	    ;
};
//
//[]--------------------------------------------------------------------------
//
Bool ReadIniRecord (LPTSTR pString, LPTSTR & pHead,
				    LPTSTR   pTail)
{
    int	   nTab = 0	 ;
    LPTSTR p	= pString;

    for ( ; pHead < pTail; pHead ++)
    {
	if (* pHead == '[')
	{{
    for ( ; pHead < pTail; pHead ++, p ++)
    {
	    * p = * pHead;

	if (* pHead == '[')
	{
	    nTab ++;
	}
	if (* pHead == ']')
	{
	if (0 == -- nTab  )
	{
	    return (pHead ++, * (p + 1) = 0, True);
	}
	}
    }
	}}
    }
    return (* pString = 0, False);
};

Bool sscanIniDelim (LPTSTR & pString, LPCTSTR pDelim, LPCTSTR pBreakDelim)
{
    LPCTSTR  p;

    for (; * pString; * pString ++)
    {
	for (p = pDelim; * p; p ++)
	{
	    if (* pString == * p)
	    {
		  pString ++;

		  return (* pString) ? True : False;
	    }
	}
	for (p = pBreakDelim; * p; p ++)
	{
	    if (* pString == * p)
	    {
		  return False;
	    }
	}
    }
		  return False;
};


int sscanIniValue (LPTSTR & pString, const char * fmt, ...)
{
    int	      n;
    va_list   args;
    va_start (args, pString);

    for (n = 0; * fmt; fmt ++)
    {
	if (* fmt == '%')
	{
	    if (1 != sscanf (pString, "%d", /*va_arg (args, void *)*/ args))
	    {	break;}

	    fmt ++;
	    n   ++;
	}
	if (NULL    == 
	   (pString =  StrChr (pString, * fmt)))
	{	break;}

	    pString ++;
    }

    return n;    
}

Bool CIniFile::Load (LPTSTR pName)
{
    HANDLE	hFile;
    TCHAR	pFileName [MAX_PATH];

    if (! BuildFileName (pFileName, pName))
    {	return False;}

    if (INVALID_HANDLE_VALUE == 
       (hFile = CreateFile	(pFileName	    ,
				 GENERIC_READ	    ,
				 FILE_SHARE_READ    ,
				 NULL		    ,
				 OPEN_EXISTING	    ,
				 FILE_ATTRIBUTE_NORMAL,
				 NULL		    )))
    {
    return False;
    }

    m_pBufBase = (char *) malloc 
   (m_dBufSize = ::GetFileSize (hFile, NULL));
		 ::ReadFile    (hFile, m_pBufBase, 
				       m_dBufSize, 
				     & m_dBufSize, NULL);
    ::CloseHandle (hFile);

    return True ;
};

Bool CIniFile::FindSection (LPTSTR & pHead,
			    LPTSTR & pTail, LPTSTR pName)
{
    int	   nTab = 0;
    LPTSTR pBeg = StrStr (m_pBufBase, pName);
    LPTSTR pEnd;

    for (pEnd = pBeg; pEnd; pEnd ++)
    {
	if ((m_pBufBase + m_dBufSize) < pEnd)
	{
	    break;
	}

	if (* pEnd == '[')
	{
	    nTab ++;
	}
	if (* pEnd == ']')
	{
	if (0 == -- nTab )
	{
	    pHead = pBeg    ;
	    pTail = pEnd + 1;

	    return True ;
	}
	}
    };
	    pHead =
	    pTail = NULL;

	    return False;
};
//
//[]------------------------------------------------------------------------[]

#endif
