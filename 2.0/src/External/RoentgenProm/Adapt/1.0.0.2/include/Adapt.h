//#ifdef __cplusplus

enum enAdaptParam
{
    adParProscan = 263
};


#ifdef ADAPT_EXPORTS
#define PDIFF ptrdiff_t

template <class _Ty>
class CDS
{
public:
	CDS(_Ty *one, PDIFF rowsize)
		:one_(one), rowsize_(rowsize){}
	_Ty* operator[] (PDIFF i) 
	{
		return one_+ rowsize_*i;
	}

private:
	_Ty* one_;
	PDIFF rowsize_;
};
#endif // EOF: ADAPT_EXPORTS

extern "C" {
//#endif

//	massin		������� ������ , 
//	massout		�������� ������ ;
//	j1			������ ������� � "����"-�������, ������� ���������� ������������
//	s			������,���������������� �������,
//	width		������ �������,
//	height		������ �������

//	������ ������� ���������� � �������� ������ ����, ������ - ����(Proscan)
//  � ������ dll ������ ������� ���������� � �������� ������ ����, ����� - �������

	void  _add_interp(
				   unsigned short				*massin,
				   unsigned short				*massout,  
				   int				   			j1,
				   int							s,			   
				   int						    width,
				   int							height

				  
	);
	void add_interp(unsigned short *in_mass, int in_j1, int in_s, int in_nn, int in_mm);


//	massin		������� ������ , 
//	massout		�������� ������ ,
//	width		������ �������,
//	height		������ �������

//	������ ������� ���������� � �������� ������ ����, ������ - ����
//  � ������ dll ������ ������� ���������� � �������� ������ ����, ����� - �������


void  Adapt(
			   unsigned short		*massin,
			   unsigned short		*massout,  
			   int					 width,
			   int					 height


);


//	massin		������� ������ , 
//	massout		�������� ������ ,
//	width		������ �������,
//	height		������ �������
//	������� ����������� "������", ��������� �������
void PaintHole(
				
				unsigned short		*massin,
				unsigned short		*massout,
				int					 width,
				int					 heigh
	 );
//	massin		������� ������ , 
//	massout		�������� ������ ,
//	j1			�������,����� "�����"-�������, ������� ���������� ������������
//	s			������,���������������� �������,
//	width		������ �������,
//	height		������ �������,
//  EndIm		��������� ������� � ��������������� ������� �����������

 void Alignment(
				
				unsigned short		*massin, 
				unsigned short		*massout,
				int					j1,
				int					s,
				int					width,
				int					height,
				int					EndIm
);
//#ifdef __cplusplus
}
//#endif